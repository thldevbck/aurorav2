'' updates
''--------------------------------------------------------------
''rev:mia jan 16 session state to viewstate
''        all codes affected were labeled. 
''June 1 2010 - fixes for blocking rule messages that
''              are not displaying in bookingprocess.aspx
''
''rev:mia may 26 2011-remove sorting when page loading
''rev:mia May 30 2011 - check if vehicle is available
''              RES_validateBookingRequest
''              RES_manageBookingRequestForOslo_POS
''rev:mia june 2 2011 this is not required DDLREQUESTSOURCE
''              RES_manageBookingRequestForOslo_POS
''rev:mia june 2 2011 Add default line to the DDLRES in the SUMMARYPAGE
''rev:mia june 14 2011 Started working on the EXTEnsions
''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
''rev:mia Oct 16 2014 - addition of PromoCode
''                    - RES_manageBookingRequestForOslo_POS                 - modified sp to pass PromoCode
''                    - RES_CreateAvailableVehiclePackage_OSLO              - modified sp to pass PromoCode
''                    - VehicleRequest                                      - addition of PromoCode (varchar(30))
''                    - Aurora.Booking.Services\BookingQuickAvail.vb        - Addition of an Empty PromoCode to NewRequestXml function
''                    - Aurora.Reservations.Services\ManageAvailability.vb  - Addition of a PromoCode to CreateAvailableVehiclePackage
''                    - VehicleAvailExtension.js                            - addition of PromoCode variable
''                    - ExtensionListener.aspx.vb                           - addition of PromoCode variable

''rev:mia Nov 19 2014 - addition of Slot Availability
''                    - RES_SummaryOfChosenVehicles
''                    - SaveRentalSlot
''                    - Created SlotAvailableControl
''                    - Modified Aurora.Booking.Data
''                    - Modified Aurora.Booking.Services
''--------------------------------------------------------------


Imports Aurora.Common
Imports Aurora.Booking.Services.BookingProcessUtility
Imports Aurora.Booking.Services.BookingProcess
Imports Aurora.Reservations.Services
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports Aurora.Booking.Services


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_BookingProcess
    Inherits AuroraPage
    Implements ICallbackEventHandler


#Region " Constant"

    Const NEWBOOKINGS As String = "NEWBOOKING|"
    Const DUPLICATE As String = "DUPLICATEBOOKING|"
    Const VEHICLEDISPLAY As String = "VEHICLEDISPLAY|"
    Const SUMMARYDISPLAY As String = "VEHICLESUMMARY|"

    ''Private Const FINISHED_PROCESS As String = "Finished"
    Private Const QS_RS_VEHREQMGTNBR As String = "BookingProcess.aspx"
    Private Const QS_RS_CUSTOMERFIND As String = "CustomerFind.aspx"
    Private Const SESSION_XMLVehicleRequest As String = "XMLVehicleRequest"
    Private Const SESSION_hashproperties As String = "hashproperties"
    Private Const BUTTON_VALUE As String = "Display Availability With Cost"
    Private Const SCRIPT_DOFOCUS As String = "window.setTimeout('DoFocus()', 1); function DoFocus(){  try { document.getElementById('REQUEST_LASTFOCUS').focus(); } catch (ex) {}    };"
    Private Const PARAMTHREE_FORMAT As String = "@agentid,@productid,@ckodate,@ckoloc ,@ckidate ,1,0,0, @ckiloc"
    Private Const CKO_ERROR_MSG As String = "The Check-Out date should be less than or equal to the Check-In date"
    Private Const CKO_ERROR_INVALID_MSG As String = "The Check-Out date is invalid"
    Private Const CKI_ERROR_INVALID_MSG As String = "The Check-In date is invalid"
    Private Const AGENTID_MSG As String = "Agent ID is mandatory"
    Private Const PRODUCTID_MSG As String = "Product is mandatory"
    Private Const PRODUCTID_INVALID_MSG As String = "Product is invalid"
    Private Const REQUESTSOURCE_MSG As String = "Request Source is mandatory"
    Private Const CKO_REQUIRED_MSG As String = "Check-Out Location is mandatory"
    Private Const CKO_INVALID_MSG As String = "Check-Out Location is invalid"
    Private Const CKI_REQUIRED_MSG As String = "Check-In Location is mandatory"
    Private Const CKI_INVALID_MSG As String = "Check-In Location is invalid"
    Private Const CKODATE_REQUIRED_MSG As String = "Check-Out Date is mandatory"
    Private Const CKIDATE_REQUIRED_MSG As String = "Check-In Date is mandatory"
    Private Const LASTNAME_REQUIRED_MSG As String = "Lastname is mandatory"
    Private Const RESUBMIT_TABLE As String = "Please Resubmit to update table"
    Private Const BUTTON_VALUE_RELOC As String = "No Availability Required"
    ''POS
    Private Const BLOCKING_CODE As String = "GEN172"
    ''invalid location
    Private Const INVALID_LOCATION As String = "Found no match for Location, refine your selection"
    ''rev:mia nov25
    Private Const CLOSED_BRANCH_LOCATION As String = "branch is closed on "
#End Region

#Region " Variables"


    Private pageValid As Boolean
    Dim sc As ScriptManager
    Private Property paramThree() As String
        Get
            Return ViewState("paramThree")
        End Get
        Set(ByVal value As String)
            ViewState("paramThree") = value
        End Set
    End Property

    Dim StepNextButton As Button
    Dim Submit As Button
    Dim SaveNext As Button
    Dim SaveNextSummary As Button
    Dim SubmitSummary As Button
    Dim btnNewBooking As Button
    Dim btnRequestBrochure As Button
    Dim btnReloc As Button
    Dim StartNextButton As Button
    Dim AddToBooking As Button
    Dim Unsetbtn As Button
    Dim index As Integer
    ''Dim btnQuickAvail As Button

#End Region

#Region " Enum"
    Enum BookingProcess
        NewBooking = 0
        CheckDuplicate = 1
        DisplayAvailablity = 2
        Summary = 3
    End Enum
#End Region

#Region " Hidden Ids"

    Private Property BPDid() As String
        Get
            Return CType(ViewState("BPDid"), String)
        End Get
        Set(ByVal value As String)
            ViewState("BPDid") = value
        End Set
    End Property

    Private Property VhrID() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("VhrID")), "", CType(ViewState("VhrID"), String))
        End Get
        Set(ByVal value As String)
            ViewState("VhrID") = value
        End Set
    End Property

    Private Property BooID() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("BooID")), "", CType(ViewState("BooID"), String))
        End Get
        Set(ByVal value As String)
            ViewState("BooID") = value
        End Set
    End Property

    Private Property BkrID() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("BkrID")), "", CType(ViewState("BkrID"), String))
        End Get
        Set(ByVal value As String)
            ViewState("BkrID") = value
        End Set
    End Property

#End Region

#Region " Global function"

    Private Sub SetUpdateAgentButtonVisible(ByVal visible As Boolean)
        If visible AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance) Then
            updateAgentButton.Visible = True
            updateAgentButton.Text = "Edit Agent"
        ElseIf visible AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentEnquiry) Then
            updateAgentButton.Visible = True
            updateAgentButton.Text = "View Agent"
        Else
            updateAgentButton.Visible = False
        End If
    End Sub

    Private Sub SetAddAgentButtonVisible(ByVal visible As Boolean)
        If visible AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance) Then
            addAgentButton.Visible = True
        Else
            addAgentButton.Visible = False
        End If
    End Sub

    Protected Function ParseRemainingDay(ByVal strdate As String) As String
        Try
            Return DateDiff(DateInterval.Day, ParseDateTime(DateTime.Now, SystemCulture), ParseDateTime(strdate, SystemCulture))
        Catch
        End Try
    End Function


    ''Private Shared notToskipLocationUpdate As Boolean
    Private Property notToskipLocationUpdate() As Boolean
        Get
            Return CType(ViewState("notToskipLocationUpdate"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState("notToskipLocationUpdate") = value
        End Set
    End Property
    Function CheckDateFormatting(Optional ByVal flagForStepOne As Boolean = True) As Boolean
        If flagForStepOne Then

            If (String.IsNullOrEmpty(Me.BkrID) AndAlso _
                String.IsNullOrEmpty(Me.BooID) AndAlso _
                String.IsNullOrEmpty(Me.VhrID)) Then

                ''rev:mia nov18
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
                SetLocationDateTimeV2()

            Else
                If (Request.QueryString("agentId") IsNot Nothing Or Request.QueryString("agentText") IsNot Nothing) _
                   And notToskipLocationUpdate = False Then
                    ''rev:mia nov18
                    ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
                    ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
                    SetLocationDateTimeV2()
                End If
            End If
            Try
                If Me.DateControlForCheckOut.Text <> "" Then
                    Dim temp As String = Convert.ToDateTime(Me.DateControlForCheckOut.Text)
                End If

            Catch ex As Exception
                Me.SetErrorShortMessage(CKO_ERROR_INVALID_MSG)
                sc.SetFocus(Me.DateControlForCheckOut)
                Return False
            End Try

            Try
                If Me.DateControlForCheckIn.Text <> "" Then
                    Dim temp As String = Convert.ToDateTime(Me.DateControlForCheckIn.Text)
                End If
            Catch ex As Exception
                Me.SetErrorShortMessage(CKI_ERROR_INVALID_MSG)
                sc.SetFocus(Me.DateControlForCheckIn)
                Return False
            End Try

            If Me.DateControlForCheckOut.Date <> Date.MinValue AndAlso Me.DateControlForCheckIn.Date <> Date.MinValue Then
                If Date.Compare(Me.DateControlForCheckOut.Date, Me.DateControlForCheckIn.Date) = 1 Then
                    Me.SetInformationShortMessage(CKO_ERROR_MSG)
                    sc.SetFocus(Me.DateControlForCheckOut)
                    txtHirePeriod.Text = ""
                    Me.lblDaysMessage.Text = ""
                    Return False
                End If
            End If

        Else

            Try

                If Step3_DateControlForCKI.Text <> "" Then
                    Dim temp As String = Convert.ToDateTime(Step3_DateControlForCKI.Text)
                End If

            Catch ex As Exception
                Me.SetErrorShortMessage(CKO_ERROR_INVALID_MSG)
                sc.SetFocus(Step3_DateControlForCKI)
                Exit Function
            End Try

            Try
                If Step3_DateControlForCKO.Text <> "" Then
                    Dim temp As String = Convert.ToDateTime(Step3_DateControlForCKO.Text)
                End If
            Catch ex As Exception
                Me.SetErrorShortMessage(CKI_ERROR_INVALID_MSG)
                sc.SetFocus(Step3_DateControlForCKO)
                Exit Function
            End Try
        End If

        Return True
    End Function


    Private Sub LoadPage(ByVal proc As BookingProcess, Optional ByVal MBR As Boolean = False)
        Select Case proc
            Case BookingProcess.NewBooking

                If MBR = False Then
                    If Not String.IsNullOrEmpty(Request.QueryString("vhrid")) AndAlso Me.BackUrl.Contains("package.aspx") Then
                        Me.wizBookingProcess.ActiveStepIndex = 2
                        Me.VhrID = Request.QueryString("vhrid")
                    Else
                        Step1_NewBooking()
                    End If
                Else
                    Step1_ModifyBookingMBR()
                End If

            Case BookingProcess.CheckDuplicate
                Step2_CheckDuplicate()
            Case BookingProcess.DisplayAvailablity
                Step3_VehicleRequest()
            Case BookingProcess.Summary
                Step4_Summary()
        End Select
    End Sub

    Private Sub DisableButtonsInNavigationTemplate(ByVal proc As BookingProcess)
        Dim StepNextButton As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("StepNextButton"), Button)
        Dim AddToBooking As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnAddToBooking"), Button)
        Dim Unset As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnUnset"), Button)
        Dim Submit As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSubmit"), Button)
        Dim SaveNext As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSaveNext"), Button)

        Dim SaveNextSummary As Button = CType(Me.wizBookingProcess.FindControl("FinishNavigationTemplateContainerId").FindControl("BtnSaveNextSummary"), Button)


        Dim SubmitSummary As Button = CType(Me.wizBookingProcess.FindControl("FinishNavigationTemplateContainerId").FindControl("BtnSaveSummary"), Button)

        Select Case proc
            Case BookingProcess.CheckDuplicate
                Unset.Visible = True
                AddToBooking.Visible = True
                Submit.Visible = False
                SaveNext.Visible = False
                StepNextButton.Visible = True

            Case BookingProcess.DisplayAvailablity
                Unset.Visible = False
                AddToBooking.Visible = False
                Submit.Visible = True
                SaveNext.Visible = True
                StepNextButton.Visible = False

            Case BookingProcess.Summary
                SaveNextSummary.Visible = True
                SubmitSummary.Visible = True

        End Select
    End Sub

    Private Function DateTimeXMLSplitter(ByVal datestring As String) As String
        Dim aryDate() As String = datestring.Split("/")
        If aryDate.Length = 3 And aryDate IsNot Nothing Then
            If aryDate(0).Length = 1 Then
                aryDate(0) = "0" & aryDate(0)
            End If

            If aryDate(1).Length = 1 Then
                aryDate(1) = "0" & aryDate(1)
            End If
            Return String.Concat(aryDate(0), "/", aryDate(1), "/", aryDate(2))
        Else
            Return datestring
        End If
    End Function

    Private Function NewDateTimeXMLSplitter(ByVal datestring As String) As String
        Dim aryDate() As String = datestring.Split("/")
        If aryDate.Length = 3 And aryDate IsNot Nothing Then
            If aryDate(0).Length = 1 Then
                aryDate(0) = "0" & aryDate(0)
            End If

            If aryDate(1).Length = 1 Then
                aryDate(1) = "0" & aryDate(1)
            End If
            Return String.Concat(aryDate(2), "-", aryDate(1), "-", aryDate(0))
        Else
            Return datestring
        End If
    End Function

    Private Sub CalculateRequestPeriod(ByVal proc As BookingProcess, Optional ByVal validate As Boolean = True)
        Try
            Dim xmldoc As New XmlDocument
            Dim xmlstring As String


            Dim tempCki As String = ""
            Dim mode As Integer = 1
            Select Case proc
                Case BookingProcess.NewBooking

                    If Me.DateControlForCheckOut.Text = "" Then
                        If validate Then
                            Me.SetInformationShortMessage(CKODATE_REQUIRED_MSG)
                        End If
                        Exit Sub
                    Else

                    End If

                    If DateControlForCheckIn.Text = "" Then
                        If validate Then
                            Me.SetInformationShortMessage(CKIDATE_REQUIRED_MSG)
                        End If
                        Exit Sub
                    End If

                    If Me.HidDMW.Value = "1" Then

                    ElseIf Me.HidDMW.Value = "7" Then
                        mode = 7
                    ElseIf Me.HidDMW.Value = "30" Then
                        mode = 30
                    End If

                    xmlstring = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                           ParseDateTime(Me.DateControlForCheckOut.Date, SystemCulture), _
                                           GetAMPM(Me.ddlAMPMcheckout.Text), _
                                           tempCki, _
                                           GetAMPM(Me.ddlAMPMcheckin.Text), _
                                           HirePeriod, _
                                           mode, _
                                           0)

                    If IsValidXMLstring(xmlstring) = False Then Exit Sub
                    xmldoc.LoadXml(xmlstring)
                    Dim xmlelem As XmlElement = xmldoc.DocumentElement

                    ''rev:mia 14July
                    Me.DateControlForCheckOut.Date = ParseDateTime(xmlelem.SelectSingleNode("//Root/D1").InnerText, SystemCulture)
                    Me.DateControlForCheckIn.Date = ParseDateTime(xmlelem.SelectSingleNode("//Root/D2").InnerText, SystemCulture)

                    Me.txtHirePeriod.Text = xmlelem.SelectSingleNode("//Root/Dif").InnerText


                Case BookingProcess.CheckDuplicate
                Case BookingProcess.DisplayAvailablity

                    If Step3_DateControlForCKO.Text = "" Then
                        Me.SetInformationShortMessage(CKODATE_REQUIRED_MSG)
                        Exit Sub
                    End If

                    If Step3_DateControlForCKI.Text = "" Then
                        Me.SetInformationShortMessage(CKIDATE_REQUIRED_MSG)
                        Exit Sub
                    End If

                    If Me.HidDMW.Value = "1" Then
                    ElseIf Me.HidDMW.Value = "7" Then
                        mode = 7
                    ElseIf Me.HidDMW.Value = "30" Then
                        mode = 30
                    End If
                    ''rev:mia 14july
                    xmlstring = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                      ParseDateTime(Me.Step3_DateControlForCKO.Date, SystemCulture), _
                                                      IIf(Me.Step3_ddlamppmCKO.Text.Contains("AM"), "AM", "PM"), _
                                                      ParseDateTime(Me.Step3_DateControlForCKI.Date, SystemCulture), _
                                                      IIf(Me.Step3_ddlAMPMcki.Text.Contains("AM"), "AM", "PM"), _
                                                      Step3_HirePeriod, _
                                                      Me.Step3_ddldays.SelectedValue, _
                                                      0)



                    xmldoc.LoadXml(xmlstring)
                    Dim xmlelem As XmlElement = xmldoc.DocumentElement

                    Me.Step3_DateControlForCKO.Date = ParseDateTime(xmlelem.SelectSingleNode("//Root/D1").InnerText, SystemCulture)
                    Me.Step3_DateControlForCKI.Date = ParseDateTime(xmlelem.SelectSingleNode("//Root/D2").InnerText, SystemCulture)
                    Me.Step3_txthireperiod.Text = xmlelem.SelectSingleNode("//Root/Dif").InnerText


                Case BookingProcess.Summary
            End Select
            xmldoc = Nothing
        Catch ex As Exception
            MyBase.SetErrorMessage("CalculateRequestPeriod > " & ex.Message)
        End Try
    End Sub

    Private Sub BindData(ByVal proc As BookingProcess, Optional ByVal blnBind As Boolean = True)
        Select Case proc
            Case BookingProcess.NewBooking
                Step1_BindData()
            Case BookingProcess.CheckDuplicate
            Case BookingProcess.DisplayAvailablity
            Case BookingProcess.Summary
        End Select
    End Sub

    Private Sub SplitData(ByVal proc As BookingProcess)

        Try

            Select Case proc
                Case BookingProcess.NewBooking
                    SetAddAgentButtonVisible(String.IsNullOrEmpty(Me.BooID) AndAlso String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText))
                    SetUpdateAgentButtonVisible(String.IsNullOrEmpty(Me.BooID) AndAlso updateAgentButton.Visible)

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/productid").InnerText) Then
                        gProductID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/productid").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agnId").InnerText) Then
                        gAgentID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agnId").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText) Then
                        gAgentCode = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agentIsMisc").InnerText) Then
                        gAgentIsMisc = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agentIsMisc").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/contact").InnerText) Then
                        gMiscAgnContact = XMLVehicleRequest.SelectSingleNode("VehicleRequest/contact").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText) Then
                        gMiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
                    End If

                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/pkgid").InnerText) Then
                        gPackageId = XMLVehicleRequest.SelectSingleNode("VehicleRequest/pkgid").InnerText
                    End If

                    If Me.PickerControlForAgent.ReadOnly = False Then
                        If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText) Then
                            Me.VhrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText
                        End If

                        If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText) Then
                            Me.BkrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText
                        End If

                        If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText) Then
                            Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
                        End If

                    End If
                    
                    If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bookingnum").InnerText) Then

                        ''rev:mia July 2 2013 - back to the office. 
                        Dim rentalIdQS As String = "&hdRentalId=" & BooID
                        If Not Request.QueryString("hdRentalId") Is Nothing Then
                            rentalIdQS = "&hdRentalId=" & Request.QueryString("hdRentalId")
                        End If
                        Dim tempURL As String = "~/Booking/Booking.aspx?activeTab=7&isFromSearch=0&hdBookingId=" & BooID & rentalIdQS

                        MyBase.SetValueLink(BooID, tempURL)

                    Else
                        MyBase.SetValueLink("", "")
                    End If

                Case BookingProcess.CheckDuplicate
                    Me.Step2_LoadxmlVhrHeader()
                    Me.Step2_LoadXMLBooHeader()
                    Me.Step2_LoadXMLdetails()
                Case BookingProcess.DisplayAvailablity
                Case BookingProcess.Summary
                    xmlChosenVehicle = New XmlDocument
                    Me.AgnDir = xmlRequest.SelectSingleNode("//Data/AgnDir").InnerText
                    Me.Msg = xmlRequest.SelectSingleNode("//Data/Msg").InnerText
                    Me.HeaderMsg = xmlRequest.SelectSingleNode("//Data/Header[@value]").Attributes(2).Value
                    Me.lblselectedmsg.Text = Me.HeaderMsg


                    Dim xmlstring As String = xmlRequest.SelectSingleNode("//Data/SelectedVehicles").OuterXml
                    xmlstring = AddIDinChosenVehicle(xmlstring, "/SelectedVehicles/SelectedVehicle", "id")

                    xmlChosenVehicle.LoadXml(xmlstring)
                    Logging.LogDebug("xmlChosenVehicle - manny", xmlChosenVehicle.OuterXml)
                    If xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle/SlvId").InnerText = "" Then
                        xmlChosenVehicle = Nothing
                        gRecExist = False
                    End If
            End Select



        Catch ex As Exception
            MyBase.SetErrorMessage("SplitData > " & ex.Message)
        End Try
    End Sub

    Sub RefreshContactID()
        If Me.hidContactID.Value <> "" AndAlso Me.hidContactID.Value.Contains("|") Then
            Dim ary() As String = Me.hidContactID.Value.Split("|")
            If ary(1) IsNot Nothing AndAlso ary(1).Length > 0 Then
                Me.ddlContact.SelectedItem.Text = ary(1)
            End If
        End If
    End Sub

    Private Sub RemoveSessionForNBR()
        ''-----------------------------------------
        ''rev:mia jan 16 session state to viewstate
        ''-----------------------------------------
        'If Session(SESSION_VehicleRequestMgt) IsNot Nothing Then
        '    Session.Remove(SESSION_VehicleRequestMgt)
        'End If
        'Session("agentId") = Nothing
        'Session("agentText") = Nothing
        ''Session("VhrID") = Nothing
        ''-----------------------------------------
    End Sub

    Private Sub GetFromSession(ByVal proc As BookingProcess)
        Try
            Select Case proc

                Case BookingProcess.CheckDuplicate

                    If Session(SESSION_rentalduplicatelist) IsNot Nothing Then
                        hashXMLdocument = CType(Session(SESSION_rentalduplicatelist), Hashtable)
                    Else
                        Exit Sub
                    End If


                    Me.xmlHeader = CType(hashXMLdocument(SESSION_xmlHeader), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlHeader.OuterXml) Then

                        Me.xmlHeader.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_xmlHeader), String)))
                    End If


                    Me.xmlVhrHeader = CType(hashXMLdocument(SESSION_xmlVhrHeader), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlVhrHeader.OuterXml) Then

                        xmlVhrHeader.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_xmlVhrHeader), String)))
                    End If


                    Me.xmlBooHeader = CType(hashXMLdocument(SESSION_xmlBooHeader), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlBooHeader.OuterXml) Then

                        Me.xmlBooHeader.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_xmlBooHeader), String)))
                    End If


                    Me.xmlDuplicateRental = CType(hashXMLdocument(SESSION_xmlDuplicateRental), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlDuplicateRental.OuterXml) Then

                        Me.xmlDuplicateRental.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_xmlDuplicateRental), String)))
                    End If


                    Me.xmlDoc = CType(hashXMLdocument(SESSION_xmlDoc), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlDoc.OuterXml) Then

                        Me.xmlDoc.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_xmlDoc), String)))
                    End If

                    hashXMLdocument(SESSION_xmlDetails) = Me.xmlDetails

                Case BookingProcess.Summary
                    
                    Me.xmlCur = CType(Session(SESSION_selectedVehicleList_xmlCur), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlCur.OuterXml) Then
                        Me.xmlCur.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_selectedVehicleList_xmlCur), String)))
                    End If

                    Me.xmlChosenVehicle = CType(Session(SESSION_selectedVehicleList_xmlChosenVehicle), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlChosenVehicle.OuterXml) Then
                        Me.xmlChosenVehicle.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_selectedVehicleList_xmlChosenVehicle), String)))
                    End If

                    Me.xmlErrorMsg = CType(Session(SESSION_selectedVehicleList_xmlErrorMsg), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlErrorMsg.OuterXml) Then
                        Me.xmlErrorMsg.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_selectedVehicleList_xmlErrorMsg), String)))
                    End If

                    Me.xmlRequest = CType(Session(SESSION_selectedVehicleList_xmlRequest), XmlDocument)
                    If String.IsNullOrEmpty(Me.xmlRequest.OuterXml) Then
                        Me.xmlRequest.LoadXml(Server.HtmlDecode(CType(ViewState(SESSION_selectedVehicleList_xmlRequest), String)))
                    End If


            End Select
        Catch ex As Exception
            ''  Me.AddToSession(proc)
        End Try
    End Sub

    Private Sub SessionToViewState(ByVal proc As BookingProcess)
        Select Case proc
            Case BookingProcess.CheckDuplicate
                ViewState(SESSION_xmlHeader) = Server.HtmlEncode(Me.xmlHeader.OuterXml)
                ViewState(SESSION_xmlVhrHeader) = Server.HtmlEncode(Me.xmlVhrHeader.OuterXml)
                ViewState(SESSION_xmlBooHeader) = Server.HtmlEncode(Me.xmlBooHeader.OuterXml)
                ViewState(SESSION_xmlDuplicateRental) = Server.HtmlEncode(Me.xmlDuplicateRental.OuterXml)
                ViewState(SESSION_xmlDetails) = Server.HtmlEncode(Me.xmlDetails.OuterXml)
                ViewState(SESSION_xmlDoc) = Server.HtmlEncode(Me.xmlDoc.OuterXml)

            Case BookingProcess.Summary
                ViewState(SESSION_selectedVehicleList_xmlCur) = Server.HtmlEncode(Me.xmlCur.OuterXml)
                ViewState(SESSION_selectedVehicleList_xmlChosenVehicle) = Server.HtmlEncode(xmlChosenVehicle.OuterXml)
                ViewState(SESSION_selectedVehicleList_xmlErrorMsg) = Server.HtmlEncode(xmlErrorMsg.OuterXml)
                ViewState(SESSION_selectedVehicleList_xmlRequest) = Server.HtmlEncode(Me.xmlRequest.OuterXml)
        End Select
    End Sub

    Private Sub AddToSession(ByVal proc As BookingProcess)
        Try
            Select Case proc
                Case BookingProcess.CheckDuplicate
                    If Session(SESSION_rentalduplicatelist) IsNot Nothing Then
                        Session.Remove(SESSION_rentalduplicatelist)
                    End If
                    If hashXMLdocument IsNot Nothing Then
                        hashXMLdocument.Clear()
                    End If
                    hashXMLdocument(SESSION_xmlHeader) = Me.xmlHeader
                    hashXMLdocument(SESSION_xmlVhrHeader) = Me.xmlVhrHeader
                    hashXMLdocument(SESSION_xmlBooHeader) = Me.xmlBooHeader
                    hashXMLdocument(SESSION_xmlDuplicateRental) = Me.xmlDuplicateRental
                    hashXMLdocument(SESSION_xmlDetails) = Me.xmlDetails
                    hashXMLdocument(SESSION_xmlDoc) = Me.xmlDoc
                    Session.Add(SESSION_rentalduplicatelist, hashXMLdocument)

                Case BookingProcess.Summary

                    Session(SESSION_selectedVehicleList_xmlCur) = Me.xmlCur
                    Session(SESSION_selectedVehicleList_xmlChosenVehicle) = xmlChosenVehicle
                    Session(SESSION_selectedVehicleList_xmlErrorMsg) = xmlErrorMsg
                    Session(SESSION_selectedVehicleList_xmlRequest) = Me.xmlRequest

            End Select
            SessionToViewState(proc)
        Catch ex As Exception
            MyBase.SetErrorMessage("AddToSession(" & proc & ") > " & ex.Message & ex.StackTrace)
        End Try
    End Sub

    Protected Function ParseEvalItemToBoolean(ByVal objCurr As Object, Optional ByVal currentrequest As Object = Nothing) As Boolean
        Try
            If objCurr Is DBNull.Value Then
                Return False
            End If
            If objCurr Is Nothing Then
                Return False
            End If
            If objCurr = 0 Then
                If CType(currentrequest, String).StartsWith("Current") And objCurr = 0 Then
                    Return False
                End If
                Return True
            End If
            If objCurr = 1 Then
                Return False
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage("ParseEvalItemToBoolean(" & objCurr & ") > " & ex.Message)
        End Try
    End Function

    Protected Function GetPackageUrl(ByVal packageId As Object) As String

        Dim packageIdString As String
        packageIdString = Convert.ToString(packageId)

        Return "../Package/Package.aspx?pkgId=" & packageIdString & "&vhrId=" & VhrID
    End Function

#End Region

#Region " New Booking"

    Private SESSION_VehicleRequestMgt As String
    Private dictObject As New StringDictionary
    Private sbFeatures As New StringBuilder
    Private strButtonListValue As String
    Private strGridItemValue As String
    Protected productid As String
    Protected XMLVehicleRequest As New XmlDocument
    ''Protected Shared gProductID As String
    Private Property gProductID() As String
        Get
            Return ViewState("gProductID")
        End Get
        Set(ByVal value As String)
            ViewState("gProductID") = value
        End Set
    End Property
    Protected gAgentID As String
    Protected gAgentCode As String
    Protected gAgentIsMisc As String
    Protected gMiscAgnContact As String
    Protected gMiscAgnName As String
    Protected gPackageId As String
    Protected gDdToday As String
    Protected ginitialValue As String
    Protected gNewValue As String
    Protected hashProperties As New Hashtable

    Sub Step1_ModifyBookingMBR()

        Try
            If Page.IsPostBack Then
                Call Me.BindData(BookingProcess.NewBooking, True)
                Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
                paramThree = GetPackageNewValues
                Me.PickerControlForPackage.Param1 = Splitter(AgentParameter)
            End If
        Catch ex As Exception
            Me.SetErrorMessage("Step1_ModifyBookingMBR > " & ex.Message)
        End Try

    End Sub

    Private Function GetAgentID(ByVal contactName As String) As String
        Dim agentid As String
        Dim id As String = Me.UserCode
        Dim contact As String
        If (contactName.Contains("-") = True AndAlso contactName <> "") Then
            contact = contactName.Substring(0, contactName.IndexOf("-"))
        Else
            If contactName <> "" Then
                contact = contactName
            Else
                Return String.Empty
            End If

        End If

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "AGENT", contact, "", "", "", "", id)

        Try
            agentid = xmlDoc.GetElementsByTagName("ID").Item(0).InnerText
        Catch ex As Exception
            Return ""
        End Try

        Return agentid
    End Function
    Sub Step1_NewBooking()
        Try
            If Not Page.IsPostBack Then


                Me.HidTemp.Value = ""
                Me.Step1_PageHookedOnFocus()
                Step1_SetFocusToControl(Me.PickerControlForAgent)
                Call Me.Step1_FillControls()


                ''------------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''commented
                ''------------------------------------------
                Me.VhrID = ""
                'If Session("VhrID") IsNot Nothing Then
                '    Me.VhrID = Session("VhrID")
                'Else
                '    Me.VhrID = ""
                'End If
                ''------------------------------------------


                Me.BooID = ""
                Me.BkrID = ""
                notToskipLocationUpdate = False

                'get bookingId for queryString
                If Not String.IsNullOrEmpty(Request.QueryString("booId")) Then
                    Me.BooID = Request.QueryString("booId")
                Else
                    ''------------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''commented
                    ''------------------------------------------
                    ''If Session("booid") IsNot Nothing Then
                    ''    Me.BooID = Session("booid")
                    ''End If
                    ''------------------------------------------
                End If

                ''rev:mia nov24
                ViewState("skipDefaultAgent") = False
                If Not Request.QueryString("agentId") Is Nothing Then
                    Me.PickerControlForAgent.DataId = Request.QueryString("agentId").ToString()
                    ViewState("skipDefaultAgent") = True ''rev:mia nov24
                Else
                    ''------------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''commented
                    ''------------------------------------------
                    'If Session("agentId") IsNot Nothing Then
                    '    Me.PickerControlForAgent.DataId = Session("agentId")
                    'End If
                    ''------------------------------------------
                End If

                If Not Request.QueryString("agentText") Is Nothing Then
                    Me.PickerControlForAgent.Text = Request.QueryString("agentText").ToString()
                    ViewState("skipDefaultAgent") = True ''rev:mia nov24
                Else
                    ''------------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''commented
                    ''------------------------------------------
                    'If Session("agentText") IsNot Nothing Then
                    '    Me.PickerControlForAgent.Text = Session("agentText")
                    'End If
                    ''------------------------------------------
                End If

                ''from AddRental Button coming in the Booking.aspx
                If Not Request.QueryString("agentId") Is Nothing And Request.QueryString("agentText") Is Nothing Then
                    Me.PickerControlForAgent.Text = Request.QueryString("agentId").ToString()
                    Me.PickerControlForAgent.DataId = GetAgentID(Request.QueryString("agentId").ToString())

                    Me.PickerControlForAgent.ReadOnly = True
                    SetUpdateAgentButtonVisible(False)
                    SetAddAgentButtonVisible(False)
                Else
                    Me.PickerControlForAgent.ReadOnly = False
                    SetUpdateAgentButtonVisible(True)
                    SetAddAgentButtonVisible(True)
                End If


                Me.CacheContactID = ""
                Me.Step1_LoadScreen()

                ''-----------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''-----------------------------------------
                'If hashProperties Is Nothing Then
                '    hashProperties = New Hashtable
                'End If
                'hashProperties(SESSION_XMLVehicleRequest) = XMLVehicleRequest
                'If Session(SESSION_hashproperties) IsNot Nothing Then
                '    Session.Remove(SESSION_hashproperties)
                'End If
                'Session(SESSION_hashproperties) = hashProperties

                Me._SESSION_XMLVehicleRequest = XMLVehicleRequest.OuterXml
                ''-----------------------------------------

                ''rev:mia dec11
                If (Request.QueryString("toggleon") IsNot Nothing) Then
                    Dim value As String = Request.QueryString("toggleon")
                    If value = "1" Then
                        Master.ToggleCompanyInServer(Me.UserCode, Me.CompanyName)
                    ElseIf value = "0" Then
                        If sc.AsyncPostBackSourceElementID = "" AndAlso Not String.IsNullOrEmpty(PostBackValue) Then
                            RetrievePostBackValue()
                        End If
                        Master.ToggleCompanyInServer(Me.UserCode, Me.CompanyName)
                    End If
                End If
            End If
        Catch ex As Exception
            Me.SetErrorMessage("Step1_NewBooking > " & ex.Message)
        End Try


    End Sub

    Private Sub Step1_GetContactInformation(ByVal agentid As String)
        Dim xmlstring As String = "<Contacts>" & Aurora.Common.Data.ExecuteScalarSP("RES_getContact", "AGENT", agentid, "Reservation", "") & "</Contacts>"
        If xmlstring = "<Contacts></Contacts>" Then
            Me.ddlContact.DataSource = Nothing
            Exit Sub
        End If
        xmlstring = RemoveUnwantedChar(xmlstring)
        Dim xmlReader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        Dim ds As New DataSet
        ds.ReadXml(xmlReader)


        Me.ddlContact.DataSource = ds.Tables("Contact")
        Me.ddlContact.DataTextField = "ConName"
        Me.ddlContact.DataValueField = "ConId"
        Me.ddlContact.DataBind()
    End Sub

    Private Sub Step1_SetControlFocus(ByVal controlid As String, ByVal sc As ScriptManager)
        System.Diagnostics.Debug.WriteLine("controlid: " & controlid)
        Select Case controlid

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForPackage$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox"
                Me.SetLocationDateTimeV2()
                sc.SetFocus(Me.PickerControlForPackage)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$StartNavigationTemplateContainerID$btnNewBooking"
                paramThree = ""
                Me.HidTemp.Value = ""


            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckIn$pickerTextBox", _
                 "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox"
                sc.SetFocus(Me.DateControlForCheckIn)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckOut$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox"
                sc.SetFocus(Me.DateControlForCheckOut)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForAgent$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox"
                sc.SetFocus(Me.ddlContact)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForProduct$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox"
                sc.SetFocus(Me.ddlRequestSource)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckOut$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox"
                sc.SetFocus(Me.ddlAMPMcheckout)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlAMPMcheckout"
                sc.SetFocus(Me.DateControlForCheckOut)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckOut$dateTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox"
                sc.SetFocus(Me.PickerControlForCheckIn)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckIn$pickerTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox"
                sc.SetFocus(Me.ddlAMPMcheckin)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlAMPMcheckIn"
                sc.SetFocus(Me.DateControlForCheckIn)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckIn$dateTextBox", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox"
                sc.SetFocus(Me.txtHirePeriod)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckIn", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn"
                ''rev:mia nov18
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
                Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckIn.Text, Me.DateControlForCheckIn.Text, Me.ddlAMPMcheckin)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckOut", _
                "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut"
                ''rev:mia nov18
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
                Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckOut.Text, Me.DateControlForCheckOut.Text, Me.ddlAMPMcheckout)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$txtSurname"
                sc.SetFocus(Me.txtfirstName)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$btnCalculate"
                ''rev:mia nov18
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
                ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
                Me.SetLocationDateTimeV2()
                sc.SetFocus(Me.txtSurname)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlAdultNo"
                sc.SetFocus(Me.ddlChildNO)
            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlChildNO"
                sc.SetFocus(Me.ddlInfantsNO)
            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlInfantsNO"
                sc.SetFocus(Me.PickerControlForPackage)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$btnNewBooking"
                Me.RemoveSessionForNBR()
                Response.Redirect(QS_RS_VEHREQMGTNBR)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlAdultNO"
                sc.SetFocus(Me.ddlChildNO)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlChildNO"
                sc.SetFocus(Me.ddlInfantsNO)

            Case "ctl00$ContentPlaceHolder$wizBookingProcess$ddlInfantsNO"
                sc.SetFocus(Me.PickerControlForPackage)

            Case "ctl00$lnkToggleCompany" ''rev:mie dec10
                ToggleButtonsClearingValue()

        End Select


    End Sub

    Private Sub Step1_SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, Optional ByVal ddlControl As DropDownList = Nothing)
        Try

            Dim index As Integer
            Dim indexText As String = ""
            If Page.IsPostBack Then
                Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
                Select Case sc.AsyncPostBackSourceElementID
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckOut$dateTextBox", _
                        "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox"
                        If HidCKO.Value = Me.PickerControlForCheckOut.Text Then
                            index = Me.ddlAMPMcheckout.SelectedIndex
                            indexText = Me.ddlAMPMcheckout.Text
                        End If
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckIn$dateTextBox", _
                    "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox"
                        If HidCKI.Value = Me.PickerControlForCheckIn.Text Then
                            index = Me.ddlAMPMcheckin.SelectedIndex
                            indexText = Me.ddlAMPMcheckin.Text
                        End If
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckOut$PickerTextbox", _
                        "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_PickerTextbox"
                        HidCKO.Value = Me.PickerControlForCheckOut.Text

                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckIn$PickerTextbox", _
                    "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_PickerTextbox"
                        HidCKI.Value = Me.PickerControlForCheckIn.Text

                    Case "ctl00_ContentPlaceHolder_wizardControl_itemDropDownList", _
                    "ctl00$ContentPlaceHolder$wizBookingProcess$StepNavigationTemplateContainerID$StepPreviousButton"
                    Case Else

                End Select
            End If

            Dim temp As String = ""
            If ddlControl.Text <> "AM" And ddlControl.Text <> "PM" Then
                temp = ddlControl.Text
            End If

            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If

            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, MyBase.UserCode)
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                MyBase.SetInformationShortMessage("No available time for location code '" & locationCode & "' and userCode '" & MyBase.UserCode & "'")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)(0).ToString
            Dim endtime As String = ds.Tables(0).Rows(0)(1).ToString

            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If

            ddlControl.Items.Clear()

            ' fix by shoel to break out of blackhole loops :)
            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":")(0)), CInt(endtime.Split(":")(1)), CInt(endtime.Split(":")(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":")(0)), CInt(startTime.Split(":")(1)), CInt(startTime.Split(":")(2)))

            While tmTempTimeValue < tmEndTime
                ddlControl.Items.Add(tmTempTimeValue.Hours.ToString().PadLeft(2, "0") & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While

            ' when tmTempTimeValue = tmEndTime
            ddlControl.Items.Add(tmEndTime.Hours.ToString().PadLeft(2, "0") & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"))

            ' fix by shoel to break out of blackhole loops :)

            ddlControl.SelectedIndex = index

            If ddlControl.ID = Me.ddlAMPMcheckout.ID Then
                If Me.HidCKOampm.Value = "" Then
                    ddlControl.SelectedIndex = 0
                Else
                    ''rev:mia sept 29
                    ''ddlControl.SelectedItem.Text = Me.HidCKOampm.Value
                    If Me.HidCKOampm.Value.Length = 8 Then
                        ddlControl.SelectedValue = Me.HidCKOampm.Value.Substring(0, 5)
                    ElseIf Me.HidCKOampm.Value.Length = 5 Then
                        ddlControl.SelectedValue = Me.HidCKOampm.Value
                    End If

                End If

            Else
                If Me.HidCKIampm.Value = "" Then
                    ddlControl.SelectedIndex = 0
                Else
                    ''rev:mia sept 29
                    ''ddlControl.SelectedItem.Text = Me.HidCKIampm.Value
                    If Me.HidCKIampm.Value.Length = 8 Then
                        ddlControl.SelectedValue = Me.HidCKIampm.Value.Substring(0, 5)
                    ElseIf Me.HidCKIampm.Value.Length = 5 Then
                        ddlControl.SelectedValue = Me.HidCKIampm.Value
                    End If

                End If

            End If

        Catch ex As Exception
            ''   Me.SetErrorMessage("Step1_SelectLocationBasedOnCodeAndUser -> " & ex.Message)
        End Try
    End Sub

    Private Sub Step1_ChildHookedOnFocus(ByVal currentcontrol As Control)

        If TypeOf currentcontrol Is TextBox OrElse _
           TypeOf currentcontrol Is DropDownList OrElse _
           TypeOf currentcontrol Is UserControls_PickerControl OrElse _
           TypeOf currentcontrol Is UserControls_DateControl OrElse _
           TypeOf currentcontrol Is Button Then

            If Not TypeOf currentcontrol Is UserControls_PickerControl AndAlso Not TypeOf currentcontrol Is UserControls_DateControl Then
                CType(currentcontrol, WebControl).Attributes.Add("onfocus", "document.getElementById('__LASTFOCUS').value=this.id")

            Else
                CType(currentcontrol, UserControl).Attributes.Add("onfocus", "document.getElementById('__LASTFOCUS').value=this.id")
            End If

            If currentcontrol.HasControls Then
                For Each mycontrol As Control In currentcontrol.Controls
                    Step1_ChildHookedOnFocus(mycontrol)
                Next
            End If
        Else
            If currentcontrol.HasControls Then
                For Each mycontrol As Control In currentcontrol.Controls
                    Step1_ChildHookedOnFocus(mycontrol)
                Next
            End If
        End If

    End Sub

    Private Sub Step1_PageHookedOnFocus()

        Dim currentcontrol As Control
        For Each currentcontrol In Me.StepOne_panelbranchCheckout.Controls
            Step1_ChildHookedOnFocus(currentcontrol)
        Next

    End Sub

    Private Sub Step1_SetFocusToControl(ByVal controlToFocus As Control)
        Dim scriptBlock As String = "document.getElementById('" & controlToFocus.ClientID & "').focus()"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetFocusScript", scriptBlock, True)
    End Sub

    Private Sub Step1_AddJSAttributes()
        ''rev:mia oct15
        Const invalid As String = "<>"
        Dim INVALIDCHARS As String

        If String.IsNullOrEmpty(ConfigurationManager.AppSettings("INVALIDCHARS")) Then
            INVALIDCHARS = "%^*+=!~`<>"
        Else
            INVALIDCHARS = ConfigurationManager.AppSettings("INVALIDCHARS") & invalid
        End If

        Filter_TxtRef.InvalidChars = INVALIDCHARS
        Filter_txtSurname.InvalidChars = INVALIDCHARS
        Filter_txtfirstName.InvalidChars = INVALIDCHARS
        Filter_txtthirdParty.InvalidChars = INVALIDCHARS
        ''rev:mia oct.8
        Dim setParamOne As String = ""
        Dim setParamTwo As String = ""
        Dim setParamThree As String = ""
        Dim setParamFour As String = ""
        Dim setParamFive As String = ""

        txtHirePeriod.AutoPostBack = False
        Dim callbackref As String = Me.Page.ClientScript.GetCallbackEventReference(Me, "'Agent'", "refreshPackageCallBack", "null", True)

        callbackref = ""
        StepOne_panelPackage.Attributes.Add("onmouseup", callbackref)


        CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "getContacts('" + Me.PickerControlForAgent.DataId + "');")
        callbackref = Me.Page.ClientScript.GetCallbackEventReference(Me, "'Product'", "refreshPackageCallBack", "null", True)
        callbackref = ""


        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetProductID('" & Me.CurrentUserCode & "');")
        Dim script As New StringBuilder



        CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "setTillDays();GetPackageXML();GetAvailableTimeCKOv2();") ''rev:mia dec3
        CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "setTillDays();GetPackageXML();GetAvailableTimeCKIv2();")  ''rev:mia dec3


        ''-----------------------------------------------
        '' REV:MIA OCT.16
        '' START: set of params for GETVALIDLOCATION
        '' Fixing defect on LIVE
        ''-----------------------------------------------
        ''For Step # 1
        script = Nothing : script = New StringBuilder
        With script
            .Append("function CKOscriptStep1(){")
            ''.Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString() != '')")
            .Append("{")


            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString());")
            .Append("}")

            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 2, '');")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString());")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("}")
            .Append("}")
        End With
        If ClientScript.IsStartupScriptRegistered(Me.GetType, "CKOScript") = False Then
            ClientScript.RegisterStartupScript(Me.GetType, "CKOScript", script.ToString, True)
        End If


        setParamOne = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 1, '');"
        setParamThree = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').value.toString().indexOf(' -')));"

        CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).Attributes.Add("onchange", "GetPackageXML();GetAvailableTimeCKOv2(); ") ''rev:mia dec17 
        CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();  GetAvailableTimeCKOv2();")



        tdCheckOutLocation.Attributes.Add("onkeyup", setParamOne & setParamTwo & setParamThree & "CKOscriptStep1();")
        tdCheckOutLocation.Attributes.Add("onmouseup", setParamOne & setParamTwo & setParamThree & "CKOscriptStep1();")

        script = Nothing : script = New StringBuilder
        With script
            .Append("function CKIscriptStep1(){")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString() != '')")
            .Append("{")


            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString());")
            .Append("}")

            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 2, '');")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox').value.toString());")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")

            .Append("}")
            .Append("}")
        End With
        If ClientScript.IsStartupScriptRegistered(Me.GetType, "CKIScript") = False Then
            ClientScript.RegisterStartupScript(Me.GetType, "CKIScript", script.ToString, True)
        End If

        CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); GetAvailableTimeCKIv2();") ''rev:mia dec17
        CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).Attributes.Add("onchange", "GetPackageXML(); GetAvailableTimeCKIv2();") ''rev:mia dec17


        setParamOne = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox').value.toString().indexOf(' -')));"
        setParamThree = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').value.toString().indexOf(' -')));"
        tdCheckinLocation.Attributes.Add("onkeyup", setParamOne & setParamTwo & setParamThree & "CKIscriptStep1();")
        tdCheckinLocation.Attributes.Add("onmouseup", setParamOne & setParamTwo & setParamThree & "CKIscriptStep1();")


        setParamOne = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox').value.toString().indexOf(' -')));"
        setParamTwo = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox').value);"
        setParamThree = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo').value);"
        setParamFour = ""
        setParamFive = ""

        StepOne_panelPackage.Attributes.Add("onmouseup", "paramThreeDynamic();")
        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onfocus", "paramThreeDynamic();")
        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onkeypress", "paramThreeDynamic();")
        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();")
        ''rev:mia Oct 9
        StepOne_panelPackage.Attributes.Add("onkeyup", setParamOne & setParamTwo & setParamThree)
        StepOne_panelPackage.Attributes.Add("onmouseup", setParamOne & setParamTwo & setParamThree)
        CType(Me.PickerControlForPackage.FindControl("pickerImage"), Image).Attributes.Add("onclick", "GetPackageXML();")
        CType(Me.PickerControlForPackage.FindControl("pickerImage"), Image).Attributes.Add("onmouseover", "GetPackageXML();")
        StepOne_panelPackage.Attributes.Add("onmousedown", "GetPackageXML()")


        Me.ddlContact.Attributes.Add("onchange", "document.getElementById('ctl00_ContentPlaceHolder_hidContactID').value= this.value + '|' + document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlContact').options[document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlContact').options.selectedIndex].text;")
        Me.ddlAdultNO.Attributes.Add("onchange", "GetPackageXML();")
        Me.ddlChildNO.Attributes.Add("onchange", "GetPackageXML();")
        Me.ddlInfantsNO.Attributes.Add("onchange", "GetPackageXML();")

        Me.ddlAdultNO.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlChildNO.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlInfantsNO.Attributes.Add("onblur", "GetPackageXML();" + callbackref)

        Me.ddlNoOfVehicles.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlNoOfVehicles.Attributes.Add("onchange", "GetPackageXML();")

        ''rev:mia june 2 2011 this is not required
        ''Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")
        ''Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")


        Me.ddlAMPMcheckout.Attributes.Add("onchange", "GetPackageXML();")
        Me.ddlAMPMcheckin.Attributes.Add("onchange", "GetPackageXML();")
        StepOne_panelbranchCheckout.Attributes.Add("onmouseout", "GetPackageXML();")

        Me.ddlAMPMcheckout.Attributes.Add("onblur", "document.getElementById('ctl00_ContentPlaceHolder_HidCKOampm').value = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout').value;")
        Me.ddlAMPMcheckin.Attributes.Add("onblur", "document.getElementById('ctl00_ContentPlaceHolder_HidCKIampm').value = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin').value;")

        Me.ddlDays.Attributes.Add("onblur", "GetPackageXML();document.getElementById('ctl00_ContentPlaceHolder_HidDMW').value = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlDays').value;")
        Me.txtSurname.Attributes.Add("onblur", "GetPackageXML();")
        ''Me.txtSurname.Attributes.Add("onchange", "ReplaceHiddenCompleteData('17');")

        Me.btnCalculate.Attributes.Add("onblur", "document.getElementById('" & Me.btnQuickAvail.ClientID & "').focus();")

        ''Me.btnQuickAvail.Attributes.Add("onclick", "GetPackageXML();GetAvailableTimeCKIv2();")
        Me.btnQuickAvail.Attributes.Add("onclick", "GetPackageXML();")
        Me.btnQuickAvail.Attributes.Add("onblur", "document.getElementById('" & Me.txtSurname.ClientID & "').focus();")
        Me.btnCalculate.Attributes.Add("onclick", "clearMessages();GetAvailableTimeCKIv2();")
        Me.Step3_ddldays.Attributes.Add("onblur", "document.getElementById('ctl00_ContentPlaceHolder_HidDMW').value = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddldays').value;")
        Me.txtHirePeriod.Attributes.Add("onblur", "clearMessages()")
        Me.txtHirePeriod.Attributes.Add("onchange", "clearMessages()")


    End Sub

    Private Sub Step1_NavigationTab()
        Dim index As Integer = 1

        CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).TabIndex = index

        If Me.updateAgentButton.Visible Then
            Me.updateAgentButton.TabIndex = index + 1
        End If

        If Me.addAgentButton.Visible Then
            Me.addAgentButton.TabIndex = index + 1
        End If


        Me.ddlContact.TabIndex = index + 1

        If Me.agentContactImageButton.Visible Then
            Me.agentContactImageButton.TabIndex = index + 1
        End If

        Me.txtRef.TabIndex = index + 1
        Me.txtthirdParty.TabIndex = index + 1

        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        Me.ddlNoOfVehicles.TabIndex = index + 1
        Me.ddlRequestSource.TabIndex = index + 1

        CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        Me.ddlAMPMcheckout.TabIndex = index + 1

        CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        Me.ddlAMPMcheckin.TabIndex = index + 1

        Me.txtHirePeriod.TabIndex = index + 1
        Me.ddlDays.TabIndex = index + 1
        Me.btnCalculate.TabIndex = index + 1
        Me.btnQuickAvail.TabIndex = index + 1


        Me.txtSurname.TabIndex = index + 1
        Me.txtfirstName.TabIndex = index + 1

        ''rev:mia Sept.2 2013 - Customer Title from query
        ''Me.ddltitle.TabIndex = index + 1
        Me.customertitleDropdown.TabIndex = index + 1

        Me.ddlAdultNO.TabIndex = index + 1
        Me.ddlChildNO.TabIndex = index + 1
        Me.ddlInfantsNO.TabIndex = index + 1

        ''reV:mia May 18 2011 addred emailaddress
        txtEmailAddress.TabIndex = index + 1
        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1

        StepNextButton = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("StepNextButton"), Button)
        Submit = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSubmit"), Button)
        SaveNext = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSaveNext"), Button)
        SaveNextSummary = CType(Me.wizBookingProcess.FindControl("FinishNavigationTemplateContainerId").FindControl("BtnSaveNextSummary"), Button)
        '' SaveNextSummary.UseSubmitBehavior = False
        SubmitSummary = CType(Me.wizBookingProcess.FindControl("FinishNavigationTemplateContainerId").FindControl("BtnSaveSummary"), Button)
        btnNewBooking = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("btnNewBooking"), Button)
        btnRequestBrochure = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("btnRequestBrochure"), Button)
        btnReloc = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("btnReloc"), Button)
        StartNextButton = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("StartNextButton"), Button)
        AddToBooking = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnAddToBooking"), Button)
        Unsetbtn = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnUnset"), Button)

        ''btnQuickAvail = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("btnQuickAvail"), Button)

        btnNewBooking.TabIndex = index + 1
        ''btnQuickAvail.TabIndex = index + 1
        btnRequestBrochure.TabIndex = index + 1
        btnReloc.TabIndex = index + 1
        StartNextButton.TabIndex = index + 1


    End Sub

    Private Sub Step1_SetXmlString(ByVal xmlstring As String)
        Try

            If IsValidXMLstring(xmlstring) Then
                Dim xmldoc As New XmlDocument
                xmldoc.LoadXml(xmlstring)
                Me.VhrID = xmldoc.SelectSingleNode("//Root/NewBookingRequest/VhrId").InnerText
                Me.BkrID = xmldoc.SelectSingleNode("//Root/NewBookingRequest/BkrId").InnerText
                Trace.Warn("bkrid was loaded in Step1_SetXmlString : " & xmldoc.SelectSingleNode("//Root/NewBookingRequest/BkrId").InnerText)
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText = Me.VhrID
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText = Me.BkrID
                xmldoc = Nothing
            End If

        Catch ex As Exception
            ''   MyBase.SetErrorMessage(ex.Message)
        End Try

    End Sub

    Private Sub Step1_CalculateDays(ByVal ddate As String, ByVal strCKO As String)
        If ddate <> "" Or Char.IsDigit(ddate) Then
            ''rev:mia 14July
            Me.DateControlForCheckIn.Date = ParseDateTime(DateTime.Parse(strCKO).AddDays(ddate - 1), SystemCulture)
        End If
    End Sub

    Private Sub Step1_FillControls()

        Dim xmlString As String = "<CommonFeatures>" & Aurora.Common.Data.ExecuteScalarSP("RES_getFeatures", "", 1) & "</CommonFeatures>"
        Dim xmlReader As New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        Dim ds As New DataSet
        ds.ReadXml(xmlReader)

        Me.chkFeatures.DataSource = ds
        Me.chkFeatures.DataTextField = "FtrDesc"
        Me.chkFeatures.DataValueField = "FtrId"
        Me.chkFeatures.DataBind()


        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        xmlString = "<UnCommonFeatures>" & Aurora.Common.Data.ExecuteScalarSP("RES_getFeatures", "", 0) & "</UnCommonFeatures>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)
        Me.ddlUncommonFeature1.DataSource = ds
        Me.ddlUncommonFeature1.DataTextField = "FtrDesc"
        Me.ddlUncommonFeature1.DataValueField = "FtrId"
        Me.ddlUncommonFeature1.DataBind()
        Me.ddlUncommonFeature1.SelectedIndex = 0

        Me.ddlUncommonFeature2.DataSource = ds
        Me.ddlUncommonFeature2.DataTextField = "FtrDesc"
        Me.ddlUncommonFeature2.DataValueField = "FtrId"
        Me.ddlUncommonFeature2.DataBind()
        Me.ddlUncommonFeature2.SelectedIndex = 0

        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        xmlString = "<Brand>" & Aurora.Common.Data.ExecuteScalarSP("GEN_getComboData", "BRAND", "", "", CurrentUserCode) & "</Brand>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)
        Me.ddlBrand.DataSource = ds
        Me.ddlBrand.DataTextField = "Description"
        Me.ddlBrand.DataValueField = "ID"
        Me.ddlBrand.DataBind()
        Me.ddlBrand.SelectedIndex = 0

        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        ''rev:mia june 2 2011 this is not required
        'Dim xmldoc As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "11", "", "")
        'ds.ReadXml(New XmlNodeReader(xmldoc))
        'Me.ddlRequestSource.AppendDataBoundItems = True
        'Me.ddlRequestSource.Items.Add(New ListItem("", "0"))
        'Me.ddlRequestSource.DataSource = ds
        'Me.ddlRequestSource.DataTextField = "Code"
        'Me.ddlRequestSource.DataValueField = "ID"
        'Me.ddlRequestSource.DataBind()
        'Me.ddlRequestSource.SelectedIndex = 0

        Me.ddlContact.Items.Add(" ")
        Me.ddlContact.Items.Add(" ")


        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet
        xmlString = "<Type>" & Aurora.Common.Data.ExecuteScalarSP("RES_getClass", "", 1) & "</Type>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)


        Me.rdlACorAV.DataSource = ds
        Me.rdlACorAV.DataTextField = "ClaCode"
        Me.rdlACorAV.DataValueField = "ClaID"
        Me.rdlACorAV.DataBind()
        xmlReader = Nothing

    End Sub

    Private Sub Step1_LoadScreen(ByVal strXML As String)

        Try

            XMLVehicleRequest.LoadXml(ChangeRootElement(strXML))
            Trace.warn("Step1_LoadScreen : " & strXML)

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/today").InnerText) Then
                gDdToday = XMLVehicleRequest.SelectSingleNode("VehicleRequest/today").InnerText
            End If

            If Not String.IsNullOrEmpty(Me.HidAgentCode.Value) Then
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText = Me.HidAgentCode.Value
            End If

            Call Me.SplitData(BookingProcess.NewBooking)

            If Page.IsPostBack Then
                Me.ginitialValue = String.Concat(gProductID, _
                                                             Me.PickerControlForCheckOut.Text, _
                                                             Me.DateControlForCheckOut.Text, _
                                                             IIf(Me.ddlAMPMcheckout.Text.Contains("A"), "AM", "PM"), _
                                                             Me.PickerControlForCheckIn.Text, _
                                                             Me.DateControlForCheckIn.Text, _
                                                             IIf(Me.ddlAMPMcheckin.Text.Contains("A"), "AM", "PM"))

                Me.HidGinitialValue.Value = Me.ginitialValue
            Else
                Me.HidGinitialValue.Value = ""
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub Step1_LoadScreen()
        Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequestForOslo", Me.VhrID, Me.BooID, Me.CurrentUserCode)
        Me.Step1_LoadScreen(xmlstring)
        Me.Step1_BindDataOnFirstLoad(xmlstring)
    End Sub

    Sub DisplayTestData(ByVal desc As String, ByVal testvalue As String)
        If Me.UserCode = "ma2" Or Me.UserCode = "RS3" Then
            displayvalue.Text = displayvalue.Text & "<b>" & desc.ToUpper & "</b> : " & testvalue & "<br/>"
        End If
    End Sub

    Sub Step1_BindDataOnFirstLoad(ByVal xmlstring As String)
        Try

            Dim ds As New DataSet
            xmlstring = RemoveUnwantedChar(xmlstring)
            Dim reader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            ds.ReadXml(reader)

            If String.IsNullOrEmpty(Me.PickerControlForAgent.Text) AndAlso String.IsNullOrEmpty(Me.PickerControlForAgent.DataId) Then
                Me.PickerControlForAgent.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agent")
                Me.PickerControlForAgent.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agnId")
            End If

            ''rev:mia nov24
            If ViewState("skipDefaultAgent") = False Then
                GetDefaultAgent()
                If Not String.IsNullOrEmpty(DefaultAgent) Then
                    Me.PickerControlForAgent.Text = DefaultAgent
                    Me.PickerControlForAgent.DataId = DefaultAgent.Split("-")(0).TrimEnd
                End If
            End If

            Me.PickerControlForProduct.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productname")
            Me.PickerControlForProduct.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid")
            ddlNoOfVehicles.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).numvehicle")


            Me.PickerControlForCheckOut.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch")
            
            ''rev:jan.12 some xml doesnt have coDw..trapped it
            Try
                Me.lbldayCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).coDw")
            Catch ex As Exception
            End Try

            ''rev:jan.12 some xml have invalid date
            Dim ckoTime As String = ""
            Dim ckoDate As String = ""
            Try
                ckoDate = (DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")), ckoTime))
                Me.DateControlForCheckOut.Date = ParseDateTime(ckoDate, SystemCulture)
            Catch ex As Exception
            End Try

            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckOut.Text, Me.DateControlForCheckOut.Text, Me.ddlAMPMcheckout)
            Dim tempCKO As String = ""
            Try
                tempCKO = ParseDateTime(ckoTime, SystemCulture).ToString("hh:mm:ss tt")
                Me.ddlAMPMcheckout.Text = ckoTime.Remove(ckoTime.Length - 2, 2) & DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString
            Catch ex As Exception

                If ckoTime.Equals("12:00 AM") Then
                Else
                    If Not String.IsNullOrEmpty(Step3_ddlamppmCKO.Text) Then
                        If Step3_ddlamppmCKO.Text.Length > 2 Then
                            Me.ddlAMPMcheckout.SelectedItem.Value = ckoTime
                        End If
                    End If

                End If

            End Try
           
            Me.PickerControlForCheckIn.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch")

            ''rev:jan.12 some xml doesnt have ciDw..trapped it
            Try
                Me.lblDayCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).ciDw")
            Catch ex As Exception
            End Try

            ''rev:jan.12 some xml have invalid date
            Dim ckiTime As String = ""
            Dim ckiDate As String = ""
            Try
                ckiDate = DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")), ckiTime)
                Me.DateControlForCheckIn.Date = ParseDateTime(ckiDate, SystemCulture)
            Catch ex As Exception
            End Try

            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckIn.Text, Me.DateControlForCheckIn.Text, Me.ddlAMPMcheckin)
            Dim tempCKI As String = ""
            Try
                tempCKI = ParseDateTime(ckiDate, SystemCulture).ToString("hh:mm:ss tt")
                Me.ddlAMPMcheckin.Text = ckiTime.Remove(ckiTime.Length - 2, 2) & DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString

            Catch ex As Exception
                If ckiTime.Equals("12:00 AM") Then

                Else
                    If Not String.IsNullOrEmpty(Step3_ddlAMPMcki.Text) Then
                        If Step3_ddlAMPMcki.Text.Length > 2 Then

                            Me.ddlAMPMcheckin.SelectedItem.Value = ckiTime
                        End If
                    End If
                End If
            End Try

            Try

                ''-----------------------------------------------------------------------------------
                ''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
                ''-----------------------------------------------------------------------------------
                If (String.IsNullOrEmpty(Me.PickerControlForCheckOut.Text) And Not String.IsNullOrEmpty(Request.QueryString("outL"))) Then
                    Me.PickerControlForCheckOut.Text = GetLocation(Request.QueryString("outL").ToString)
                End If
                'If ((Me.ddlAMPMcheckout.Text = "AM" Or Me.ddlAMPMcheckout.Text = "PM") And Not String.IsNullOrEmpty(Request.QueryString("outT"))) Then
                '    Me.ddlAMPMcheckout.Text = Request.QueryString("outT").ToString
                'End If
                If (String.IsNullOrEmpty(Me.DateControlForCheckOut.Text) And Not String.IsNullOrEmpty(Request.QueryString("outD"))) Then
                    Me.DateControlForCheckOut.Date = Request.QueryString("outD").ToString
                End If

                If (String.IsNullOrEmpty(Me.PickerControlForCheckIn.Text) And Not String.IsNullOrEmpty(Request.QueryString("inL"))) Then
                    Me.PickerControlForCheckIn.Text = GetLocation(Request.QueryString("inL").ToString)
                End If
                'If ((Me.ddlAMPMcheckin.Text = "AM" Or Me.ddlAMPMcheckin.Text = "PM") And Not String.IsNullOrEmpty(Request.QueryString("inT"))) Then
                '    Me.ddlAMPMcheckin.Text = Request.QueryString("inT").ToString
                'End If
                If (String.IsNullOrEmpty(Me.DateControlForCheckIn.Text) And Not String.IsNullOrEmpty(Request.QueryString("inD"))) Then
                    Me.DateControlForCheckIn.Text = Request.QueryString("inD").ToString
                End If

                ''rev:mia June 7 2013 - addition of Vehicle and AgentReference
                If (String.IsNullOrEmpty(Me.txtRef.Text) And Not String.IsNullOrEmpty(Request.QueryString("ar"))) Then
                    Me.txtRef.Text = Request.QueryString("ar").ToString
                End If

                If (String.IsNullOrEmpty(Me.PickerControlForProduct.Text) And Not String.IsNullOrEmpty(Request.QueryString("veh"))) Then
                    Me.PickerControlForProduct.Text = GetProduct(Request.QueryString("veh").ToString)
                    ''rev:mia Dec.20 2013 - Fixes for filtering of packages in BookingProcess after adding a new rental
                    Me.hidPackageID.Value = Me.PickerControlForProduct.DataId

                    If (Me.PickerControlForPackage.DataId = "" Or String.IsNullOrEmpty(Me.PickerControlForPackage.DataId)) Then
                        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onfocus", "paramThreeDynamic();GetPackageXML();")
                    End If
                End If


                Me.lblDayCKI.Text = ParseDateAsDayWord(DateControlForCheckIn.Text)
            Catch ex As Exception

            End Try


            Me.txtHirePeriod.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")

            Me.VhrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).VhrId")
            Me.BooID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).booid")
            Me.BkrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid")
            ''Trace.Warn("bkrid was loaded in Step1_BindDataOnFirstLoad : " & DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid"))


            Me.txtSurname.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirersurname")
            Me.txtfirstName.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirerfirstname")

            ''rev:mia Sept.2 2013 - Customer Title from query
            ''Me.ddltitle.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirertitle")
            Try
                customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DecodeText(DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirertitle"))))
            Catch ex As Exception
                customertitleDropdown.SelectedIndex = 0
            End Try

            Try
                ''-----------------------------------------------------------------------------------
                ''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
                ''-----------------------------------------------------------------------------------
                If (String.IsNullOrEmpty(txtSurname.Text) And Not String.IsNullOrEmpty(Request.QueryString("ln"))) Then
                    txtSurname.Text = Request.QueryString("ln").ToString
                End If
                If (String.IsNullOrEmpty(txtfirstName.Text) And Not String.IsNullOrEmpty(Request.QueryString("fn"))) Then
                    txtfirstName.Text = Request.QueryString("fn").ToString
                End If
                ''rev:mia Sept.2 2013 - Customer Title from query
                ''If (String.IsNullOrEmpty(ddltitle.Text) And Not String.IsNullOrEmpty(Request.QueryString("tit"))) Then
                If (String.IsNullOrEmpty(customertitleDropdown.SelectedItem.Text) And Not String.IsNullOrEmpty(Request.QueryString("tit"))) Then
                    ''ddltitle.Text = Request.QueryString("tit").ToString
                    Try
                        customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(Request.QueryString("tit").ToString))
                    Catch ex As Exception
                        customertitleDropdown.SelectedIndex = 0
                    End Try
                End If

            Catch ex As Exception

            End Try

            Me.ddlAdultNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).adult")
            Me.ddlChildNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).childern")
            Me.ddlInfantsNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).infants")

            Try
                ''-----------------------------------------------------------------------------------
                ''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
                ''-----------------------------------------------------------------------------------
                If (Not String.IsNullOrEmpty(Request.QueryString("ad"))) Then
                    ddlAdultNO.SelectedItem.Text = Request.QueryString("ad").ToString
                End If
                If (Not String.IsNullOrEmpty(Request.QueryString("ch"))) Then
                    ddlChildNO.SelectedItem.Text = Request.QueryString("ch").ToString
                End If
                If (Not String.IsNullOrEmpty(Request.QueryString("in"))) Then
                    ddlInfantsNO.SelectedItem.Text = Request.QueryString("in").ToString
                End If

            Catch ex As Exception

            End Try



            Me.PickerControlForPackage.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).pkgid")
            Me.PickerControlForPackage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).package")

            ''rev:mia june 2 2011 this is not required
            'Me.ddlRequestSource.SelectedValue = DataBinder.Eval(ds, "Tables(0).DefaultView(0).requestsource")

            Me.lblDaysMessage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).tillDays")

            ''rev:mia may 18 2011 - add email address
            GetConfirmationEmail(Me.BooID)

            
            reader = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Step1_BindData()
        Try

            Dim strcontact() As String = Me.hidContactID.Value.ToString.Split("|")

            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequestForOslo", Me.VhrID, Me.BooID, Me.CurrentUserCode)
            xmlstring = RemoveUnwantedChar(xmlstring)
            Dim tempContactxml As String

            If strcontact.Length > 1 Then
                tempContactxml = "<contactname>" & strcontact(1) & "</contactname></VehicleRequest>"
            Else
                If String.IsNullOrEmpty(Me.CacheContactID) = False Then
                    strcontact = Me.CacheContactID.Split("|")
                    tempContactxml = "<contactname>" & strcontact(1) & "</contactname></VehicleRequest>"
                Else
                    tempContactxml = "<contactname></contactname></VehicleRequest>"
                End If

            End If

            xmlstring = xmlstring.Replace("</VehicleRequest>", tempContactxml)
            Me.Step1_LoadScreen(xmlstring)

            Dim ds As New DataSet
            Dim reader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            ds.ReadXml(reader)

            Me.PickerControlForAgent.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agent")
            Me.PickerControlForAgent.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agnId")

            Me.PickerControlForProduct.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productname")
            Me.PickerControlForProduct.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid")
            ddlNoOfVehicles.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).numvehicle")


            Me.PickerControlForCheckOut.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch")
            Me.lbldayCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).coDw")

            Dim ckoTime As String = ""
            Dim ckoDate As String = DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")), ckoTime)
            Me.DateControlForCheckOut.Date = ParseDateTime(ckoDate, SystemCulture)

            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckOut.Text, Me.DateControlForCheckOut.Text, Me.ddlAMPMcheckout)

            Dim tempCKO As String = ParseDateTime(ckoTime, SystemCulture).ToString("hh:mm:ss tt")

            Dim item As ListItem

            Try

                ckoTime = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")
                ckoTime = ckoTime.Split("T")(1)
                ckoTime = ckoTime.Split(" ")(0)
                If ckoTime.Length = 4 Then
                    ckoTime = "0" & ckoTime
                ElseIf ckoTime.Length = 8 Then
                    ckoTime = ckoTime.Substring(0, 5)
                End If
                item = Me.ddlAMPMcheckout.Items.FindByText(ckoTime)
                If item IsNot Nothing Then
                    ''rev:mia sept 29
                    ''Me.ddlAMPMcheckout.SelectedItem.Text = item.Text
                    Me.ddlAMPMcheckout.SelectedValue = item.Text
                Else
                    Me.ddlAMPMcheckout.Text = ckoTime
                End If
                Me.HidCKOampm.Value = Me.ddlAMPMcheckout.Text

            Catch ex As Exception

                If ckoTime.Equals("12:00 AM") Then
                Else
                    If Not String.IsNullOrEmpty(Step3_ddlamppmCKO.Text) Then
                        If Step3_ddlamppmCKO.Text.Length > 2 Then
                            Me.ddlAMPMcheckout.SelectedItem.Value = ckoTime
                        End If
                    End If

                End If

            End Try

            Me.PickerControlForCheckIn.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch")
            Me.lblDayCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).ciDw")
            Dim ckiTime As String = ""
            Dim ckiDate As String = DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")), ckiTime)

            Me.DateControlForCheckIn.Date = ParseDateTime(ckiDate, SystemCulture)
            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckIn.Text, Me.DateControlForCheckIn.Text, Me.ddlAMPMcheckin)

            Dim tempCKI As String = ParseDateTime(ckiDate, SystemCulture).ToString("hh:mm:ss tt")

            Try

                ckiTime = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")
                ckiTime = ckiTime.Split("T")(1)
                ckiTime = ckiTime.Split(" ")(0)
                If ckiTime.Length = 4 Then
                    ckiTime = "0" & ckiTime
                ElseIf ckiTime.Length = 8 Then
                    ckiTime = ckiTime.Substring(0, 5)
                End If

                item = Me.ddlAMPMcheckin.Items.FindByText(ckiTime)
                If item IsNot Nothing Then
                    ''rev:mia sept 29
                    '' Me.ddlAMPMcheckin.SelectedItem.Text = item.Text
                    Me.ddlAMPMcheckin.SelectedValue = item.Value
                Else
                    Me.ddlAMPMcheckin.Text = ckoTime
                End If
                Me.HidCKIampm.Value = Me.ddlAMPMcheckin.Text

            Catch ex As Exception
                If ckiTime.Equals("12:00 AM") Then
                Else
                    If Not String.IsNullOrEmpty(Step3_ddlAMPMcki.Text) Then
                        If Step3_ddlAMPMcki.Text.Length > 2 Then
                            Me.ddlAMPMcheckin.SelectedItem.Value = ckiTime
                        End If
                    End If
                End If


            End Try



            Me.txtHirePeriod.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")

            Me.VhrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).VhrId")
            Me.BooID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).booid")
            Me.BkrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid")
            Trace.warn("bkrid was loaded in Step1_BindData : " & DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid"))


            Me.txtSurname.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirersurname")
            Me.txtfirstName.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirerfirstname")

            ''rev:mia Sept.2 2013 - Customer Title from query
            ''Me.ddltitle.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirertitle")
            Try
                customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirertitle")))
            Catch ex As Exception
                customertitleDropdown.SelectedIndex = 0
            End Try


            Me.ddlAdultNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).adult")
            Me.ddlChildNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).childern")
            Me.ddlInfantsNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).infants")


            Me.PickerControlForPackage.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).pkgid")
            Me.PickerControlForPackage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).package")

            ''rev:mia june 2 2011 this is not required
            ''Me.ddlRequestSource.SelectedValue = DataBinder.Eval(ds, "Tables(0).DefaultView(0).requestsource")
            Me.lblDaysMessage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).tillDays")


            reader = Nothing

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub Step1_GetCommonFeatures()

        With sbFeatures
            For Each chkitems As ListItem In chkFeatures.Items
                If chkitems.Selected Then
                    sbFeatures.Append("," & chkitems.Value)
                End If
            Next
        End With

    End Sub

    Private Sub Step1_GetSelectedFeatures()
        If Me.ddlUncommonFeature1.SelectedItem IsNot "" Then
            If Me.ddlUncommonFeature1.SelectedValue IsNot "" Then
                sbFeatures.Append("," & Me.ddlUncommonFeature1.SelectedValue)
            End If
        End If

        If Me.ddlUncommonFeature2.SelectedItem IsNot "" Then
            If Me.ddlUncommonFeature2.SelectedValue IsNot "" Then
                sbFeatures.Append("," & Me.ddlUncommonFeature2.SelectedValue)
            End If
        End If
        If sbFeatures.ToString.StartsWith(",") Then
            sbFeatures.Remove(0, 1)
        End If
    End Sub

    Private Sub Step1_GetVehicleTypes(ByVal VehicleTypeID As String)
        If VehicleTypeID IsNot "NA" Then
            Dim xmlstring As String = "<VehicleTypes>" & Aurora.Common.Data.ExecuteScalarSP("RES_getType", VehicleTypeID, "") & "</VehicleTypes>"
            Dim xmlreader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            xmlreader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(xmlreader)

            Me.GridView1.DataSource = ds
            Me.GridView1.DataBind()
            Me.GridView1.SelectedIndex = 0
            xmlreader = Nothing
        End If
    End Sub

    Private Sub Step1_SplitDataOnPostBack()
        Try
            hashProperties = CType(Session(SESSION_hashproperties), Hashtable)
            XMLVehicleRequest = CType(hashProperties(SESSION_XMLVehicleRequest), XmlDocument)
            Dim xmlstring As String = XMLVehicleRequest.OuterXml

            If IsValidXMLstring(xmlstring) Then

                Dim xmlelem As XmlElement = XMLVehicleRequest.DocumentElement
                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid") IsNot Nothing Then
                    Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand") IsNot Nothing Then
                    Me.Brand = XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand").InnerText
                End If

                If (XMLVehicleRequest.SelectSingleNode("VehicleRequest/class") IsNot Nothing) Then
                    Me.Class = XMLVehicleRequest.SelectSingleNode("VehicleRequest/class").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/type") IsNot Nothing Then
                    Me.TypeID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/type").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo") IsNot Nothing Then
                    Me.BKRIntegrityNo = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName") IsNot Nothing Then
                    Me.MiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype") IsNot Nothing Then
                    Me.HireOUM = XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype").InnerText
                End If

            End If



        Catch ex As Exception
            Logging.LogException("Step1_SplitDataOnPostBack", ex)
        End Try

    End Sub

    Private Sub Step1_CompareValues()
        Me.gNewValue = String.Concat(Me.PickerControlForProduct.DataId, _
                                         Me.PickerControlForCheckOut.Text, _
                                         Me.DateControlForCheckOut.Text, _
                                         IIf(Me.ddlAMPMcheckout.Text.Contains("A"), "AM", "PM"), _
                                         Me.PickerControlForCheckIn.Text, _
                                         Me.DateControlForCheckIn.Text, _
                                         IIf(Me.ddlAMPMcheckin.Text.Contains("A"), "AM", "PM"))


        If String.IsNullOrEmpty(Me.HidGinitialValue.Value) Then
            Me.HidGinitialValue.Value = Me.gNewValue
            Me.VhrID = ""
        Else
            If Not Me.HidGinitialValue.Value.ToString.Equals(Me.gNewValue) Then
                Me.VhrID = ""
            End If
        End If

    End Sub

    Private Sub Step1_DefaultContent()
        Me.ddlNoOfVehicles.Text = 1
        Me.ddlChildNO.SelectedIndex = 0
        Me.ddlInfantsNO.SelectedIndex = 0
        Me.ddlAdultNO.SelectedIndex = 1
        Me.wizBookingProcess.ActiveStepIndex = 0

        Me.Step3_txthireperiod.ReadOnly = True
    End Sub

    Private Function Step1_GetProductID() As String
        Try


            Call Step1_GetCommonFeatures()
            Call Step1_GetSelectedFeatures()

            Dim paramOne_ProductCode As String = Splitter(Me.PickerControlForProduct.Text)
            Dim paramTwo_Brand As String = Me.ddlBrand.SelectedValue

            Dim paramThree_Class As String = ""
            If strButtonListValue <> "" Then
                paramThree_Class = strButtonListValue
            Else
                ''-----------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''-----------------------------------------
                'If Session("listvalue") IsNot Nothing Then
                '    paramThree_Class = CType(Session("listvalue"), String)
                'End If
                paramThree_Class = ListValue
                ''-----------------------------------------
            End If

            Dim paramFour_Type As String = ""
            If strGridItemValue <> "" Then
                paramFour_Type = strGridItemValue
            Else
                ''-----------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''-----------------------------------------
                'If Session("GridItemValue") IsNot Nothing Then
                '    paramFour_Type = CType(Session("GridItemValue"), String)
                'End If
                ''-----------------------------------------
            End If

            Dim paramFive As String = ""
            Dim paramSix_Features As String = sbFeatures.ToString
            Dim paramSeven As String = ""


            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", _
                                                       paramOne_ProductCode, _
                                                       paramTwo_Brand, _
                                                       paramThree_Class, _
                                                       paramFour_Type, _
                                                       paramFive, _
                                                       paramSix_Features, _
                                                       paramSeven, _
                                                       CurrentUserCode)


            If xmlstring <> "" Then

                If IsValidXMLstring(xmlstring) Then
                    Dim xmldoc As New XmlDocument
                    xmldoc.LoadXml(xmlstring)
                    Dim xmlelem As XmlElement = xmldoc.DocumentElement
                    Return xmlelem.SelectSingleNode("Id").InnerText
                End If
            Else
                Return ""
            End If

        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
        End Try
    End Function

    Private Function Step1_GetPackageXML() As String

        Dim agentid As String = IIf(Me.PickerControlForAgent.DataId <> "", Me.PickerControlForAgent.DataId, Me.hidAgentID.Value) & ","
        If agentid = "," Then
            agentid = Me.hidAgentID.Value & ","
        End If

        productid = Step1_GetProductID() & ","
        If productid = "," Then Return ""


        Dim ckoDate As String
        If Me.ddlAMPMcheckout.Text.Equals("AM") Then
            ckoDate = Me.ddlAMPMcheckout.Text & " " & "07:00:00" & ","
        Else
            If Me.ddlAMPMcheckout.Text.Equals("PM") Then
                ckoDate = Me.DateControlForCheckOut.Text & " " & "13:00:00" & ","
            Else
                ckoDate = Me.DateControlForCheckOut.Text & " " & Me.ddlAMPMcheckout.Text & ","
            End If
        End If


        Dim ckoBranch As String = Splitter(Me.PickerControlForCheckOut.Text) & ","
        Dim ckiBranch As String = Splitter(Me.PickerControlForCheckIn.Text) & ","


        Dim adultNo As String = Me.NumberOfAdults & ","
        Dim ChildNo As String = Me.NumberOfChildren & ","
        Dim InfantNo As String = Me.NumberOfInfants & ","

        Dim ckiDate As String
        If Me.ddlAMPMcheckin.Text.Equals("AM") Then
            ckiDate = Me.DateControlForCheckIn.Text & " " & "07:00:00"
        Else
            If Me.ddlAMPMcheckin.Text.Equals("PM") Then
                ckiDate = Me.DateControlForCheckIn.Text & " " & "13:00:00"
            Else
                ckiDate = Me.DateControlForCheckIn.Text & " " & Me.ddlAMPMcheckin.Text
            End If
        End If


        Return String.Concat(agentid, productid, ckoDate, ckoBranch, ckiBranch, adultNo, ChildNo, InfantNo, ckiDate)
    End Function



    Private Function Step1_IsRequiredFieldHaveValues() As Boolean

        If Me.PickerControlForAgent.Text = "" Then
            Me.SetInformationShortMessage(AGENTID_MSG)
            sc.SetFocus(Me.PickerControlForAgent)

            Return False
        End If

        If Me.PickerControlForProduct.Text = "" Then
            Me.SetInformationShortMessage(PRODUCTID_MSG)
            sc.SetFocus(Me.PickerControlForProduct)
            Return False
        Else
            Dim temp As String = GetProductID(Me.PickerControlForProduct.Text, MyBase.UserCode)
            If String.IsNullOrEmpty(temp) Then
                Me.SetInformationShortMessage(PRODUCTID_INVALID_MSG)
                sc.SetFocus(Me.PickerControlForProduct)
                Return False
            End If
        End If

        ''rev:mia june 2 2011 this is not required
        'If Me.ddlRequestSource.SelectedItem.Text.Trim = "" Then
        '    Me.SetInformationShortMessage(REQUESTSOURCE_MSG)
        '    sc.SetFocus(Me.ddlRequestSource)
        '    Return False
        'End If

        If PickerControlForCheckOut.Text = "" Then
            Me.SetInformationShortMessage(CKO_REQUIRED_MSG)
            sc.SetFocus(PickerControlForCheckOut)
            Return False
        ElseIf (PickerControlForCheckOut.Text.Contains("-") = False) Then
            Me.SetInformationShortMessage(CKO_INVALID_MSG)
            sc.SetFocus(PickerControlForCheckOut)
            Return False
        End If

        If DateControlForCheckOut.Text = "" Then
            Me.SetInformationShortMessage(CKODATE_REQUIRED_MSG)
            sc.SetFocus(DateControlForCheckOut)
            Return False
        End If

        If PickerControlForCheckIn.Text = "" Then
            Me.SetInformationShortMessage(CKO_REQUIRED_MSG)
            sc.SetFocus(PickerControlForCheckIn)
            Return False
        ElseIf (PickerControlForCheckIn.Text.Contains("-") = False) Then
            Me.SetInformationShortMessage(CKI_INVALID_MSG)
            sc.SetFocus(PickerControlForCheckIn)
            Return False
        End If

        If DateControlForCheckIn.Text = "" Then
            Me.SetInformationShortMessage(CKIDATE_REQUIRED_MSG)
            sc.SetFocus(DateControlForCheckIn)
            Return False
        End If

        If txtSurname.Text = "" Then
            ''this is disabled if quick avail is click
            If rfv_Surname.Enabled = True Or IsquickAvail = False Then
                Me.SetInformationShortMessage(LASTNAME_REQUIRED_MSG)
                sc.SetFocus(txtSurname)
                Return False
            End If
        End If

        ''rev":mia nov25
        'If IsBranchLocationClosed(True) = False Then
        '    ''Return False
        'End If


        If Not CheckDateFormatting() Then Return False

        ''reV:mia may 18 2011 email address
        If Not (String.IsNullOrEmpty(txtEmailAddress.text)) Then
            If (CheckEmailAddress(txtEmailAddress) = True) Then
                emailaddress = txtEmailAddress.text
            Else
                Return False
            End If

        End If

       

        Return True

    End Function

    Private Function Step1_GetXMLString(Optional ByVal getnewcontact As Boolean = False, _
                                        Optional ByVal ForRelocation As Boolean = False, _
                                        Optional ByVal productId As String = "") As String
        Try


            Dim strcontact() As String = Me.CacheContactID.ToString.Split("|") '' Me.hidContactID.Value.ToString.Split("|")

            If sbFeatures IsNot Nothing Then sbFeatures = Nothing : sbFeatures = New StringBuilder

            With sbFeatures
                .Append("<Root>")
                .Append("<NewBookingRequest>")

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/BooId") Is Nothing Then
                    .AppendFormat("<BooId>{0}</BooId>", Me.BooID)
                Else
                    .AppendFormat("<BooId>{0}</BooId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BooId").InnerText)
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/BkrId") Is Nothing Then
                    .AppendFormat("<BkrId>{0}</BkrId>", Me.BkrID)
                Else
                    .AppendFormat("<BkrId>{0}</BkrId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BkrId").InnerText)
                End If


                ''for every request..we should have an empty booking

                If IsquickAvail Then
                    .Append("<VhrId/>")
                Else
                    If String.IsNullOrEmpty(Me.VhrID) Then
                        .Append("<VhrId/>")
                    Else
                        .AppendFormat("<VhrId>{0}</VhrId>", Me.VhrID)
                    End If
                End If
                



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/AgnCode") Is Nothing Then
                    .AppendFormat("<AgnCode>{0}</AgnCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/AgnCode").InnerText)
                Else
                    .AppendFormat("<AgnCode>{0}</AgnCode>", Splitter(Me.PickerControlForAgent.Text))
                End If

                ''fixed for the contactid not being passed in the xml
                If Not String.IsNullOrEmpty(Me.hidContactID.Value) AndAlso (Me.hidContactID.Value.Contains("|")) Then
                    If Not String.IsNullOrEmpty(Me.hidContactID.Value) AndAlso XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode") Is Nothing Then
                        .AppendFormat("<ConCode>{0}</ConCode>", Me.hidContactID.Value.Split("|")(0))
                    Else
                        .AppendFormat("<ConCode>{0}</ConCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode").InnerText)
                    End If
                Else
                    .AppendFormat("<ConCode>{0}</ConCode>", "")
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrAgnRef") Is Nothing Then
                    .AppendFormat("<VhrAgnRef>{0}</VhrAgnRef>", Me.Reference)
                Else
                    .AppendFormat("<VhrAgnRef>{0}</VhrAgnRef>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrAgnRef").InnerText)
                End If

                'rev:mia July 2 2012 - addition of 3rd party reference
                '                            - first day of work after two weeks holiday
                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrThirdPartyRef") Is Nothing Then
                    .AppendFormat("<VhrThirdPartyRef>{0}</VhrThirdPartyRef>", Me.ThirdParyReference)
                Else
                    .AppendFormat("<VhrThirdPartyRef>{0}</VhrThirdPartyRef>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrThirdPartyRef").InnerText)
                End If

                .Append("<SapId/>")


                ''rev:mia june 14 2011 Started working on the EXTEnsions
                If Not String.IsNullOrEmpty(productId) Then
                    .AppendFormat("<PrdId>{0}</PrdId>", productId)
                Else
                    If XMLVehicleRequest.SelectSingleNode("VehicleRequest/PrdId") Is Nothing Then
                        If Me.productid = "," Or Me.productid = "" Then
                            Me.productid = Me.hidPackageID.Value
                        End If
                        Me.productid = Me.productid.Replace(",", "")
                        .AppendFormat("<PrdId>{0}</PrdId>", Me.productid)
                    Else
                        .AppendFormat("<PrdId>{0}</PrdId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PrdId").InnerText)
                    End If
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/BrdCode") Is Nothing Then
                    .AppendFormat("<BrdCode>{0}</BrdCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BrdCode").InnerText)
                Else
                    .AppendFormat("<BrdCode>{0}</BrdCode>", Me.Brand)
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ClaId") Is Nothing Then
                    .AppendFormat("<ClaId>{0}</ClaId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ClaId").InnerText)
                Else
                    .AppendFormat("<ClaId>{0}</ClaId>", Me.Class)
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgCode") Is Nothing Then
                    .AppendFormat("<PkgCode>{0}</PkgCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgCode").InnerText)
                Else
                    .AppendFormat("<PkgCode>{0}</PkgCode>", Splitter(Me.PickerControlForPackage.Text))
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgId") Is Nothing Then
                    .AppendFormat("<PkgId>{0}</PkgId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgId").InnerText)
                Else
                    .AppendFormat("<PkgId>{0}</PkgId>", Me.PickerControlForPackage.DataId)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/TypeId") Is Nothing Then
                    .AppendFormat("<TypeId>{0}</TypeId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/TypeId").InnerText)
                Else
                    .AppendFormat("<TypeId>{0}</TypeId>", Me.TypeID)
                End If


                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/IntegrityNo") Is Nothing Then
                    .AppendFormat("<IntegrityNo>{0}</IntegrityNo>", Me.BKRIntegrityNo)
                Else
                    .AppendFormat("<IntegrityNo>{0}</IntegrityNo>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/IntegrityNo").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentName") Is Nothing Then
                    .AppendFormat("<MiscAgentName>{0}</MiscAgentName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentName").InnerText)
                Else
                    .AppendFormat("<MiscAgentName>{0}</MiscAgentName>", Me.MiscAgnName)
                End If



                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentContact") Is Nothing Then
                    .AppendFormat("<MiscAgentContact>{0}</MiscAgentContact>", Me.MiscAgentContact)
                Else
                    .AppendFormat("<MiscAgentContact>{0}</MiscAgentContact>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentContact").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfVehicles") Is Nothing Then
                    .AppendFormat("<NumberOfVehicles>{0}</NumberOfVehicles>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfVehicles").InnerText)
                Else
                    .AppendFormat("<NumberOfVehicles>{0}</NumberOfVehicles>", Me.NumVehicle)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfAdults") Is Nothing Then
                    .AppendFormat("<NumberOfAdults>{0}</NumberOfAdults", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfAdults").InnerText)
                Else
                    .AppendFormat("<NumberOfAdults>{0}</NumberOfAdults>", Me.NumberOfAdults)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfChildren") Is Nothing Then
                    .AppendFormat("<NumberOfChildren>{0}</NumberOfChildren>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfChildren").InnerText)
                Else
                    .AppendFormat("<NumberOfChildren>{0}</NumberOfChildren>", Me.NumberOfChildren)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfInfants") Is Nothing Then
                    .AppendFormat("<NumberOfInfants>{0}</NumberOfInfants>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfInfants").InnerText)
                Else
                    .AppendFormat("<NumberOfInfants>{0}</NumberOfInfants>", Me.NumberOfInfants)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoLocation") Is Nothing Then
                    .AppendFormat("<ckoLocation>{0}</ckoLocation>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoLocation").InnerText)
                Else
                    .AppendFormat("<ckoLocation>{0}</ckoLocation>", Me.CKOLocation)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDate") Is Nothing Then
                    .AppendFormat("<ckoDate>{0}</ckoDate>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDate").InnerText)
                Else
                    .AppendFormat("<ckoDate>{0}</ckoDate>", Me.CKODate)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDayPart") Is Nothing Then
                    .AppendFormat("<ckoDayPart>{0}</ckoDayPart>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDayPart").InnerText)
                Else
                    .AppendFormat("<ckoDayPart>{0}</ckoDayPart>", Me.CKODayPart)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiLocation") Is Nothing Then
                    .AppendFormat("<ckiLocation>{0}</ckiLocation>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiLocation").InnerText)
                Else
                    .AppendFormat("<ckiLocation>{0}</ckiLocation>", Me.CKILocation)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDate") Is Nothing Then
                    .AppendFormat("<ckiDate>{0}</ckiDate>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDate").InnerText)
                Else
                    .AppendFormat("<ckiDate>{0}</ckiDate>", Me.CKIDate)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDayPart") Is Nothing Then
                    .AppendFormat("<ckiDayPart>{0}</ckiDayPart>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDayPart").InnerText)
                Else
                    .AppendFormat("<ckiDayPart>{0}</ckiDayPart>", Me.CKIDayPart)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/HirePeriod") Is Nothing Then
                    .AppendFormat("<HirePeriod>{0}</HirePeriod>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/HirePeriod").InnerText)
                Else
                    .AppendFormat("<HirePeriod>{0}</HirePeriod>", Me.HirePeriod)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/HireUOM") Is Nothing Then
                    .AppendFormat("<HireUOM>{0}</HireUOM>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/HireUOM").InnerText)
                Else
                    .AppendFormat("<HireUOM>{0}</HireUOM>", Me.HireOUM)
                End If

                If IsquickAvail Then
                    'this is a required field..lets put some flag and 
                    ''when the validation encountered it, then allow it
                    If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName") Is Nothing Then
                        .AppendFormat("<LastName>{0}</LastName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName").InnerText)
                    Else
                        If Me.LastName <> "" Then
                            .AppendFormat("<LastName>{0}</LastName>", Me.LastName)
                        Else
                            .AppendFormat("<LastName>{0}</LastName>", "QuickAvail")
                        End If

                    End If

                Else
                    If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName") Is Nothing Then
                        .AppendFormat("<LastName>{0}</LastName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName").InnerText)
                    Else
                        .AppendFormat("<LastName>{0}</LastName>", IIf(IsquickAvail, "QuickAvail", Me.LastName))
                    End If
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/FirstName") Is Nothing Then
                    .AppendFormat("<FirstName>{0}</FirstName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/FirstName").InnerText)
                Else
                    .AppendFormat("<FirstName>{0}</FirstName>", Me.FirstName)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/Title") Is Nothing Then
                    .AppendFormat("<Title>{0}</Title>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/Title").InnerText)
                Else
                    .AppendFormat("<Title>{0}</Title>", Me.Titles)
                End If


                ''rev:mia june 2 2011 this is not required
                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/RequestSource") Is Nothing Then
                    .AppendFormat("<RequestSource>{0}</RequestSource>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/RequestSource").InnerText)
                Else
                    .AppendFormat("<RequestSource>{0}</RequestSource>", IIf(String.IsNullOrEmpty(Me.RequestSource), "", ""))
                End If



                If ForRelocation = False Then
                    .AppendFormat("<ButClick>{0}</ButClick>", BUTTON_VALUE)
                Else
                    .AppendFormat("<ButClick>{0}</ButClick>", BUTTON_VALUE_RELOC)
                End If

                ''rev:mia 16oct2014 - cleaning out
                'For Each item As ListItem In Me.chkFeatures.Items
                '    If item.Selected Then
                '        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", item.Value)
                '    End If

                'Next

                'If Me.ddlUncommonFeature1.SelectedItem.Text <> "" Then
                '    If Me.ddlUncommonFeature1.SelectedValue <> 0 Then
                '        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", Me.ddlUncommonFeature1.SelectedValue)
                '    End If
                'End If


                'If Me.ddlUncommonFeature2.SelectedItem.Text <> "" Then
                '    If Me.ddlUncommonFeature2.SelectedValue <> 0 Then
                '        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", Me.ddlUncommonFeature2.SelectedValue)
                '    End If
                'End If

                ''rev:mia 16oct2014 - addition of PromoCode
                If (Not String.IsNullOrEmpty(promocodeTextbox.Text)) Then
                    Me.PromoCode = HttpUtility.HtmlEncode(promocodeTextbox.Text)
                    .AppendFormat("<PromoCode>{0}</PromoCode>", Me.PromoCode)
                Else
                    'If (Not String.IsNullOrEmpty(Me.PromoCode)) Then
                    '    promocodeTextbox.Text = HttpUtility.HtmlDecode(Me.PromoCode)
                    '    .AppendFormat("<PromoCode>{0}</PromoCode>", Me.PromoCode)
                    'Else
                    '    .AppendFormat("<PromoCode/>")
                    'End If
                    .AppendFormat("<PromoCode/>")
                End If

                .Append("</NewBookingRequest>")
                .AppendFormat("<Aurora><AddModUserId>{0}</AddModUserId></Aurora>", Me.CurrentUserCode)
                .AppendFormat("<Aurora><AddProgName>{0}</AddProgName></Aurora>", "BookingProcess.aspx")
                .Append("</Root>")

            End With

            ''rev:mia dec11
            Dim temp As String = sbFeatures.ToString
            Dim tempNewElement As String = ""
            tempNewElement = "<loccko>" & Me.PickerControlForCheckOut.Text & "</loccko>"
            tempNewElement = tempNewElement & "<loccki>" & Me.PickerControlForCheckIn.Text & "</loccki>"
            tempNewElement = tempNewElement & "<packageText>" & Me.PickerControlForPackage.Text & "</packageText>"
            tempNewElement = tempNewElement & "<productText>" & Me.PickerControlForProduct.Text & "</productText>"
            tempNewElement = tempNewElement & "<AgnCodeText>" & Me.PickerControlForAgent.Text & "</AgnCodeText></NewBookingRequest>"
            temp = temp.Replace("</NewBookingRequest>", tempNewElement)
            Me.PostBackValue = temp

        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
        End Try

        Return sbFeatures.ToString
    End Function


#End Region

#Region " CheckDuplicates"

    Protected xmlHeader As New XmlDocument
    Protected xmlVhrHeader As New XmlDocument
    Protected xmlBooHeader As New XmlDocument
    Protected xmlDuplicateRental As New XmlDocument
    Protected xmlDetails As New XmlDocument
    Protected xmlDoc As New XmlDocument

    Protected SESSION_rentalduplicatelist As String
    Protected SESSION_xmlHeader As String = "xmlHeader"
    Protected SESSION_xmlVhrHeader As String = "xmlVhrHeader"
    Protected SESSION_xmlBooHeader As String = "xmlBooHeader"
    Protected SESSION_xmlDuplicateRental As String = "xmlDuplicateRental"
    Protected SESSION_xmlDetails As String = "xmlDetails"
    Protected SESSION_xmlDoc As String = "xmlDoc"

    Protected hashXMLdocument As New Hashtable
    ''Private Shared unset As Boolean ''= False
    Private Property unset() As Boolean
        Get
            Return CType(ViewState("unset"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState("unset") = value
        End Set
    End Property


    Sub Step2_CheckDuplicate()

        Try
            Me.xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_manageDupRentals", Me.VhrID, Me.BooID, Me.BPDid, "")
            Dim strXML As String = xmlDoc.OuterXml.Replace("^", " ")

            strXML = ChangeRootElement(strXML)
            xmlDoc.LoadXml(strXML)

            If Me.xmlDuplicateRental IsNot Nothing Then
                Me.xmlDuplicateRental = New XmlDocument
            End If

            Me.xmlDuplicateRental.LoadXml(strXML)

            If (xmlDoc.SelectSingleNode("//Data/Details").ChildNodes.Count = 1) Then
                Me.wizBookingProcess.MoveTo(DisplayAvailableStep3)
                Exit Sub
            Else
                Call Step2_BindData(xmlDoc)
                Call Me.SplitData(BookingProcess.CheckDuplicate)

                ''-----------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''-----------------------------------------
                ''Call Me.AddToSession(BookingProcess.CheckDuplicate)
                Call Me.AddToSessionR2(BookingProcess.CheckDuplicate)
                ''-----------------------------------------

            End If

            If Not Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum") Is Nothing Then
                If Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText <> "" Then
                    ''rev:mia July 2 2013 - back to the office. 
                    Dim booIdQS As String = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
                    Dim rentalIdQS As String = "&hdRentalId=" & booIdQS

                    If Not Request.QueryString("hdRentalId") Is Nothing Then
                        rentalIdQS = "&hdRentalId=" & Request.QueryString("hdRentalId")
                    End If
                    Dim tempURL As String = "~/Booking/Booking.aspx?activeTab=7&isFromSearch=0&hdBookingId=" & booIdQS & rentalIdQS
                    Me.SetValueLink(booIdQS, tempURL)

                Else
                    MyBase.SetValueLink("", "")
                End If
            End If

            pageValid = True

        Catch ex As Exception
            Me.SetErrorMessage("Step2_CheckDuplicate > " & ex.Message)
            pageValid = False
        End Try
    End Sub

    Sub Step2_BindData(ByVal xmldoc As XmlDocument)
        Dim ds As DataSet = New DataSet
        Try


            ds.ReadXml(New XmlNodeReader(xmldoc))
            Step2_GridView1.DataSource = ds.Tables("Rental")
            Step2_GridView1.DataBind()

            If ds.Tables IsNot Nothing Then
                If ds.Tables(0).Rows.Count = 0 Then
                    Me.wizBookingProcess.MoveTo(DisplayAvailableStep3)
                End If

                Me.txtHirer.Value = ds.Tables("VhrHeader").Rows(0).Item("Hirer").ToString.ToUpper
                Me.txtagent.Value = ds.Tables("VhrHeader").Rows(0).Item("Agent").ToString.ToUpper
            Else
                Me.wizBookingProcess.MoveTo(DisplayAvailableStep3)
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_BindData  > " & ex.Message)
            LogError("Step2_BindData  > " & ex.Source & ", " & ex.Message & ", " & ex.StackTrace)
        End Try
    End Sub

    Sub Step2_LoadxmlVhrHeader()
        Try
            If Me.xmlVhrHeader Is Nothing Then
                Me.xmlVhrHeader = New XmlDocument
            End If
            Me.xmlVhrHeader.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/VhrHeader").OuterXml)
        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_LoadxmlVhrHeader  > " & ex.Message)
        End Try

    End Sub

    Sub Step2_LoadXMLBooHeader()
        Try
            If Me.xmlBooHeader Is Nothing Then
                Me.xmlBooHeader = New XmlDocument
            End If
            Me.xmlBooHeader.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/BooHeader").OuterXml)

            Dim booid As String = ""
            If Not Me.xmlBooHeader.SelectSingleNode("BooId") Is Nothing Then
                booid = Me.xmlBooHeader.SelectSingleNode("BooId").InnerText
            End If

            If Not String.IsNullOrEmpty(booid) Then
                Me.Step2_LoadXMLheader(1)
            Else
                Me.Step2_LoadXMLheader(0)
            End If
        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_LoadXMLBooHeader  > " & ex.Message)
        End Try

    End Sub

    Sub Step2_LoadXMLheader(ByVal mode As Integer)
        Try
            If Me.xmlHeader Is Nothing Then
                Me.xmlHeader = New XmlDocument
            End If
            If mode = 1 Then
                Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)
            Else
                Me.xmlHeader.LoadXml(Me.xmlVhrHeader.OuterXml)
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_LoadXMLheader(" & mode & ") > " & ex.Message)
        End Try

    End Sub

    Sub Step2_LoadXMLdetails()
        Try
            If Me.xmlDetails Is Nothing Then
                Me.xmlDetails = New XmlDocument
            End If
            Me.xmlDetails.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/Details").OuterXml)
        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_LoadXMLdetails > " & ex.Message)
        End Try

    End Sub

    Sub Step2_AddToBooking()

        Dim checked As Boolean = False
        Dim chkError As CheckBox = Nothing

        Try


            Dim i As Integer = 0
            Dim row As GridViewRow = Nothing
            Dim temp As String = String.Empty
            If Not Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum") Is Nothing Then
                temp = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
            End If
            For i = 0 To Me.xmlDoc.DocumentElement("Details").ChildNodes.Count - 1
                row = Step2_GridView1.Rows(i)
                chkError = CType(row.FindControl("chkError"), CheckBox)
                If chkError IsNot Nothing Then
                    If chkError.Checked = True Then
                        checked = True
                        Me.Hidbooid.Value = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooId").InnerText
                        Me.lblbooid.Text = Me.Hidbooid.Value
                        Me.BooID = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooId").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooId").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooNum").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("AgnId").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("AgnId").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("Agent").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("Agent").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("Hirer").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("Hirer").InnerText
                        Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)

                        chkError.Checked = False
                        ''rev:mia July 2 2013 - back to the office. 
                        Dim booIdQS As String = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
                        Dim rentalIdQS As String = "&hdRentalId=" & booIdQS

                        If Not Request.QueryString("hdRentalId") Is Nothing Then
                            rentalIdQS = "&hdRentalId=" & Request.QueryString("hdRentalId")
                        End If
                        Dim tempURL As String = "~/Booking/Booking.aspx?activeTab=7&isFromSearch=0&hdBookingId=" & booIdQS & rentalIdQS
                        Me.SetValueLink(booIdQS, tempURL)
                        

                    End If
                End If
            Next

            If checked = False Then
                MyBase.SetInformationShortMessage("Please select the booking")
            Else

                If (Not String.IsNullOrEmpty(Me.Hidbooid.Value) Or (Not String.IsNullOrEmpty(temp))) Then
                    If (String.IsNullOrEmpty(temp)) Then temp = Me.Hidbooid.Value

                    MyBase.SetInformationShortMessage("Booking # " & Me.BooID & " was linked in '" & temp & "'")
                End If
            End If



        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_AddToBooking > " & ex.Message)
        End Try


    End Sub

    Sub Step2_UnsetToBooking()
        Try


            Dim temp As String = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
            Me.Hidbooid.Value = ""
            Me.BooID = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("AgnId").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("Agent").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("Hirer").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = ""
            Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)

            Dim chkError As CheckBox = Nothing
            Dim row As GridViewRow = Nothing
            For i As Integer = 0 To Me.xmlDoc.DocumentElement("Details").ChildNodes.Count - 1
                row = Step2_GridView1.Rows(i)
                chkError = CType(row.FindControl("chkError"), CheckBox)
                If chkError IsNot Nothing Then
                    chkError.Checked = False
                End If
                MyBase.SetValueLink("", "")
            Next
            If Not String.IsNullOrEmpty(temp) Then
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Booking Request unlinked from " & temp)
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage("Step2_UnsetToBooking > " & ex.Message)
        End Try
    End Sub

    Protected Sub Step2_GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step2_GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            Try
                Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
                If rowview("BookingRental").ToString = "Current Request" Then
                    e.Row.BackColor = Drawing.Color.Ivory
                    e.Row.Font.Bold = True
                End If
            Catch
            End Try

        End If
    End Sub

    Protected Sub Step2_GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step2_GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim hplink As HyperLink
            If Not rowview("BookingRental").ToString = "Current Request" Then
                hplink = CType(e.Row.FindControl("hpLink"), HyperLink)
                If hplink IsNot Nothing Then
                    hplink.NavigateUrl = "Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & rowview("BooId") & "&hdRentalId=" & rowview("RntId")
                End If

                Dim rowAgentHirer As String = rowview("AgentHirer")
                rowAgentHirer = rowAgentHirer.Replace("^", " ")
                rowAgentHirer = rowAgentHirer.ToLower
                Dim intLocation As Integer = rowAgentHirer.IndexOf(",")

                ''rev:mia Nov.23 2011 - related to helpDesk call 21523	aliment of Agent Hirer
                Dim agent As String = ""
                If (intLocation <> -1) Then
                    agent = rowAgentHirer.Substring(0, intLocation)
                End If

                intLocation = agent.LastIndexOf(" ")
                If (intLocation <> -1) Then
                    agent = rowAgentHirer.Substring(0, intLocation)
                End If

                Dim hirer As String = rowAgentHirer.Substring(intLocation + 1)
                rowAgentHirer = agent.TrimEnd.ToUpper & "<br/><b>" & hirer.TrimEnd.ToLower & "</b>"

                Dim litAgentHirer As Literal = CType(e.Row.FindControl("litAgentHirer"), Literal)
                If litAgentHirer IsNot Nothing Then
                    litAgentHirer.Text = rowAgentHirer
                End If

            End If

        Else
            If e.Row.RowType = DataControlRowType.Header Then
                Dim col As DataControlField = Nothing
                Dim litHeaderAgent As Literal
                For i As Integer = 0 To Me.Step2_GridView1.Columns.Count - 1
                    col = Me.Step2_GridView1.Columns(i)
                    If col.HeaderText = "Agent Hirer" Then
                        litHeaderAgent = New Literal
                        litHeaderAgent.Text = "Agent" & "<br/>" & "Hirer"
                        litHeaderAgent.Visible = True
                        e.Row.Cells(i).Controls.Add(litHeaderAgent)
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Protected Sub Step2_Unset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        unset = True
        Me.Step2_UnsetToBooking()

        ''-----------------------------------------
        ''rev:mia jan 16 session state to viewstate
        ''-----------------------------------------
        ''Me.AddToSession(BookingProcess.CheckDuplicate)
        Me.AddToSessionR2(BookingProcess.CheckDuplicate)
        ''-----------------------------------------
    End Sub

    Protected Sub Step2_Set_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        unset = False
        Me.Step2_AddToBooking()

        ''-----------------------------------------
        ''rev:mia jan 16 session state to viewstate
        ''-----------------------------------------
        ''Me.AddToSession(BookingProcess.CheckDuplicate)
        Me.AddToSessionR2(BookingProcess.CheckDuplicate)
        ''-----------------------------------------
    End Sub

#End Region

#Region " Display Availability"

    Protected xmlstring As String
    ''Protected Shared mCostIndex As String
    Private Property mCostIndex() As String
        Get
            Return ViewState("mCostIndex")
        End Get
        Set(ByVal value As String)
            ViewState("mCostIndex") = value
        End Set
    End Property
    Protected reader As XmlTextReader
    Protected sbglobal As StringBuilder
    Protected XMLdocVehicle As New XmlDocument
    ''Private Shared blnresubmit As Boolean = False
    Private Property blnresubmit() As Boolean
        Get
            Return CType(ViewState("blnresubmit"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState("blnresubmit") = value
        End Set
    End Property

    Private ReadOnly Property HirePeriodDiff() As String
        Get
            Try
                Return DateDiff(DateInterval.Day, CDate(Me.Step3_DateControlForCKO.Text), CDate(Me.Step3_DateControlForCKI.Text)).ToString
            Catch ex As Exception
                MyBase.SetErrorMessage("HirePeriodDiff > " & ex.Message)
            End Try
        End Get
    End Property

    Private Property TotalQTY() As String
        Get
            Return CType(ViewState("TotalQTY"), String)
        End Get
        Set(ByVal value As String)
            ViewState("TotalQTY") = value
        End Set
    End Property

    Private Property veh_ID() As String
        Get
            Return CType(ViewState("veh_ID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("veh_ID") = value
        End Set
    End Property

    Private Property DaysMode() As String
        Get
            Return CType(ViewState("DaysMode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("DaysMode") = value
        End Set
    End Property

    Private Property Qty() As String
        Get
            Return CType(ViewState("Qty"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Qty") = value
        End Set
    End Property

    Private Property PKGid() As String
        Get
            Return CType(ViewState("PKGid"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PKGid") = value
        End Set
    End Property

    Private Property VCvalue() As String
        Get
            Return CType(ViewState("VCvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("VCvalue") = value
        End Set
    End Property

    Private Property TCvalue() As String
        Get
            Return CType(ViewState("TCvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("TCvalue") = value
        End Set
    End Property

    Private Property PPvalue() As String
        Get
            Return CType(ViewState("PPvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PPvalue") = value
        End Set
    End Property

    Private Property PDvalue() As String
        Get
            Return CType(ViewState("PDvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PDvalue") = value
        End Set
    End Property

    Private Property VDNvalue() As String
        Get
            Return CType(ViewState("VDNvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("VDNvalue") = value
        End Set
    End Property

    Private Property VDGvalue() As String
        Get
            Return CType(ViewState("VDGvalue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("VDGvalue") = value
        End Set
    End Property

    Private Property ProgramID() As String
        Get
            Return CType(ViewState("ProgramID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ProgramID") = value
        End Set
    End Property

    Private Property COampm() As String
        Get
            Return CType(ViewState("COampm"), String)
        End Get
        Set(ByVal value As String)
            ViewState("COampm") = value
        End Set
    End Property

    Private Property CIampm() As String
        Get
            Return CType(ViewState("CIampm"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CIampm") = value
        End Set
    End Property

    Private Property Step3_ProductID() As String
        Get
            If String.IsNullOrEmpty(CType(ViewState("Step3_ProductID"), String)) Then
                Return Me.Step3_PickerControlForProduct.DataId
            Else

                Return CType(ViewState("Step3_ProductID"), String)

            End If
        End Get
        Set(ByVal value As String)
            ViewState("Step3_ProductID") = value
        End Set
    End Property

    Private Property CityCode() As String
        Get
            Return CType(ViewState("CityCode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CityCode") = value
        End Set
    End Property

    Protected Property BranchCKO() As String
        Get

            If Me.Step3_PickerControlForBranchCKO.Text <> "" Then
                If Me.Step3_PickerControlForBranchCKO.Text.IndexOf("-") <> -1 Then
                    Return Me.Step3_PickerControlForBranchCKO.Text.Substring(0, Me.Step3_PickerControlForBranchCKO.Text.IndexOf("-")).TrimEnd.ToUpper
                Else
                    Return Me.Step3_PickerControlForBranchCKO.Text.ToUpper
                End If

            Else
                Return CType(ViewState("BranchCKO"), String)
            End If

        End Get

        Set(ByVal value As String)
            If value.Contains("-") Then
                ViewState("BranchCKO") = value.Substring(0, value.IndexOf("-"))
            Else
                ViewState("BranchCKO") = value
            End If
        End Set
    End Property

    Private Property BranchCKI() As String
        Get



            If Me.Step3_PickerControlForBranchCKI.Text <> "" Then
                If Me.Step3_PickerControlForBranchCKI.Text.IndexOf("-") <> -1 Then
                    Return Me.Step3_PickerControlForBranchCKI.Text.Substring(0, Me.Step3_PickerControlForBranchCKI.Text.IndexOf("-")).TrimEnd.ToUpper
                Else
                    Return Me.Step3_PickerControlForBranchCKI.Text.ToUpper
                End If
            Else
                Return CType(ViewState("BranchCKI"), String)
            End If
        End Get

        Set(ByVal value As String)
            If value.Contains("-") Then
                ViewState("BranchCKI") = value.Substring(0, value.IndexOf("-"))
            Else
                ViewState("BranchCKI") = value
            End If
        End Set
    End Property

    Private Property CostDef() As String
        Get
            Return CType(ViewState("CostDef"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CostDef") = value
        End Set
    End Property

    Private Property BranchCKOwhen() As String
        Get
            Return CType(ViewState("BranchCKOwhen"), String)
        End Get
        Set(ByVal value As String)
            ViewState("BranchCKOwhen") = value
        End Set
    End Property

    Private Property BranchCKIwhen() As String
        Get
            Return CType(ViewState("BranchCKIwhen"), String)
        End Get
        Set(ByVal value As String)
            ViewState("BranchCKIwhen") = value
        End Set
    End Property

    Private Property Step3_HirePeriod() As String
        Get
            Return CType(ViewState("Step3_HirePeriod"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Step3_HirePeriod") = value
        End Set
    End Property

    Private Property RentalID() As String
        Get
            Return Step3_HidRentalID.Value
        End Get
        Set(ByVal value As String)
            Step3_HidRentalID.Value = value
        End Set
    End Property

    Protected Property Package() As String
        Get
            Return CType(ViewState("Package"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Package") = value
        End Set
    End Property

    Protected Property PP() As String
        Get
            Return CType(ViewState("PP"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PP") = value
        End Set
    End Property

    Protected Property UOM() As String
        Get
            Return CType(ViewState("UOM"), String)
        End Get
        Set(ByVal value As String)
            ViewState("UOM") = value
        End Set
    End Property

    Protected Property CUR() As String
        Get
            Return CType(ViewState("CUR"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CUR") = value
        End Set
    End Property

    Protected Property Val() As String
        Get
            Return CType(ViewState("Val"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Val") = value
        End Set
    End Property

    Protected Property ByPass() As String
        Get
            Return CType(ViewState("ByPass"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ByPass") = value
        End Set
    End Property

    Protected ReadOnly Property CKOandCKI() As String
        Get
            Return Me.BranchCKO & " - " & Me.BranchCKI
        End Get

    End Property

    Protected Property Vehicles() As String
        Get
            Return CType(ViewState("Vehicles"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Vehicles") = value
        End Set
    End Property

    Protected Property Availabilitynumber() As String
        Get
            If String.IsNullOrEmpty(Me.ByPass) = False Then
                If Me.ByPass.ToString = "1" Or Me.ByPass = 1 Then
                    Return "Bypass"
                Else
                    Return CType(ViewState("Availabilitynumber"), String)
                End If
            Else
                Return CType(ViewState("Availabilitynumber"), String)
            End If

        End Get
        Set(ByVal value As String)

            ViewState("Availabilitynumber") = value
        End Set
    End Property

    Protected Property CriteriaMatch() As String
        Get
            Return CType(ViewState("CriteriaMatch"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CriteriaMatch") = value
        End Set
    End Property

    Protected Property ProductName() As String
        Get
            Return CType(ViewState("ProductName"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ProductName") = value
        End Set
    End Property

    Private Property DoNavigate() As Boolean
        Get
            Return ViewState("DoNavigate")
        End Get
        Set(ByVal value As Boolean)
            ViewState("DoNavigate") = value
        End Set
    End Property

    Function Step3_RetrieveValueFromGrid() As String
        Dim mtotalqty As Integer = CInt(Me.TotalQTY)
        Dim sb As New StringBuilder
        Dim txtvalue As TextBox
        Dim litpackage As Label = Nothing
        Dim litvehID As Label = Nothing
        Dim temp As String = ""
        DoNavigate = False
        Try

            Dim i As Integer = 0
            If Step3_GridView1.Rows.Count > 0 Then


                For Each griditem As GridViewRow In Step3_GridView1.Rows
                    If griditem.RowType = DataControlRowType.DataRow Then
                        sb.Append("<Avail>")
                        litvehID = CType(griditem.FindControl("lblvehID"), Label)

                        If litvehID IsNot Nothing Then
                            sb.AppendFormat("<AvvId>{0}</AvvId>", litvehID.Text)
                        Else
                            sb.AppendFormat("<AvvId>{0}</AvvId>", Me.veh_ID)
                        End If


                        litpackage = CType(griditem.FindControl("lblpackageID"), Label)
                        If litpackage IsNot Nothing Then
                            sb.AppendFormat("<PkgId>{0}</PkgId>", litpackage.Text)
                        Else
                            sb.AppendFormat("<PkgId>{0}</PkgId>", Me.PKGid)
                        End If

                        txtvalue = CType(griditem.FindControl("txtvalue"), TextBox)
                        ''rev:mia POC
                        If txtvalue IsNot Nothing AndAlso txtvalue.Enabled Then
                            If String.IsNullOrEmpty(txtvalue.Text) = False Then
                                If txtvalue.Text <> 0 And txtvalue.Text <> "0" Then
                                    If Char.IsDigit(txtvalue.Text) Then
                                        mtotalqty += CInt(txtvalue.Text)

                                        sb.AppendFormat("<Qty>{0}</Qty>", txtvalue.Text)

                                        If DoNavigate = False Then
                                            DoNavigate = True
                                        End If

                                    Else
                                        sb.AppendFormat("<Qty>{0}</Qty>", "")
                                    End If
                                Else
                                    sb.AppendFormat("<Qty>{0}</Qty>", "")
                                    txtvalue.Text = "0"

                                    ''DoNavigate = False
                                    mtotalqty = 0
                                End If

                            Else
                                sb.AppendFormat("<Qty>{0}</Qty>", "")
                                mtotalqty = 0
                            End If
                        Else
                            ''rev:mia POC
                            If Not txtvalue.Enabled Then
                                sb.AppendFormat("<Qty>{0}</Qty>", 1)
                            Else
                                sb.AppendFormat("<Qty>{0}</Qty>", "")
                            End If

                        End If
                        sb.Append("</Avail>")
                        i = i + 1
                    End If
                Next

            Else
                ''poc 
                Dim ds As DataSet = CType(ViewState("dsavailability"), DataSet)
                For rowctr As Integer = 0 To ds.Tables("VehicleRequest").Rows.Count - 1
                    sb.Append("<Avail>")
                    sb.AppendFormat("<AvvId>{0}</AvvId>", ds.Tables("VehicleRequest").Rows(rowctr)("VhrId"))
                    sb.AppendFormat("<PkgId>{0}</PkgId>", ds.Tables("VehicleRequest").Rows(rowctr)("pkgid"))
                    If ViewState("flagforBlockingRule") = True Then
                        sb.AppendFormat("<Qty>{0}</Qty>", 1)
                    Else
                        sb.AppendFormat("<Qty>{0}</Qty>", "")
                    End If

                    sb.Append("</Avail>")
                Next



            End If
            Me.TotalQTY = mtotalqty
            ViewState("summaryrow") = i


            Return sb.ToString
        Catch ex As Exception
            MyBase.SetErrorMessage("Step3_RetrieveValueFromGrid > " & ex.Message)
        End Try
    End Function

    Sub Step3_getDisplayAvailableVehiclesWithCost()

        Try

            Dim rntid As String = ""
            Dim butSel As String = Me.hid_html_butSel.Value


            Dim xmltemp As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getDisplayAvailableVehiclesWithCostOSLO", Me.VhrID, Me.BooID, rntid, MyBase.UserCode, butSel)

            If Not xmltemp.SelectSingleNode("data/VehicleRequest/bookingnum") Is Nothing Then
                If xmltemp.SelectSingleNode("data/VehicleRequest/bookingnum").InnerText <> "" Then

                    Dim booIdQS As String = xmltemp.SelectSingleNode("data/VehicleRequest/bookingnum").InnerText
                    Dim rentalIdQS As String = "&hdRentalId=" & booIdQS

                    If Not Request.QueryString("hdRentalId") Is Nothing Then
                        rentalIdQS = "&hdRentalId=" & Request.QueryString("hdRentalId")
                    End If
                    Dim tempURL As String = "~/Booking/Booking.aspx?activeTab=7&isFromSearch=0&hdBookingId=" & booIdQS & rentalIdQS
                    Me.SetValueLink(booIdQS, tempURL)

                Else
                    Me.SetValueLink("", "")
                End If
            Else
                Me.SetValueLink("", "")
            End If
            ''rev:mia jan 23
            Me.AgentID = xmltemp.SelectSingleNode("data/VehicleRequest/agnId").InnerText
            Me.ProductIDForBypassRecalculation = xmltemp.SelectSingleNode("data/VehicleRequest/productid").InnerText

            xmlstring = xmltemp.OuterXml
            xmlstring = RemoveUnwantedChar(xmlstring)
            ''poc
            reader = New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(reader)
            ViewState("dsavailability") = ds


            If ds.Tables("VehicleRequest").Rows.Count = -1 Then
                Exit Sub
            End If

            Me.Step3_GetTable(ds)

            ds = Nothing
            Me.Step3_BindGrid()

            ''REV:MIA OCT.16
            Step3_PickerControlForBranchCKO.Param2 = Step3_PickerControlForBranchCKI.Text.Split(" ")(0)
            Step3_PickerControlForBranchCKO.Param3 = Me.Step3_PickerControlForProduct.Text.Split(" ")(0)

            Step3_PickerControlForBranchCKI.Param2 = Step3_PickerControlForBranchCKO.Text.Split(" ")(0)
            Step3_PickerControlForBranchCKI.Param3 = Me.Step3_PickerControlForProduct.Text.Split(" ")(0)

            ''REV:MIA OCT.20
            Step3_AddJSAttributes()

            ''rev:mia jan 19
            'Step3_PickerControlForBranchCKI.AutoPostBack = False
            'Step3_PickerControlForBranchCKO.AutoPostBack = False
            ''Me.Step3_DateControlForCKO.AutoPostBack = False
            ''Me.Step3_DateControlForCKI.AutoPostBack = False

        Catch ex As Exception
            MyBase.SetErrorMessage("Step3_getDisplayAvailableVehiclesWithCost > " & ex.Message)
        End Try
    End Sub

    Sub Step3_BindGrid(ByVal mBKRid As String, ByVal mBranchCKO As String, ByVal mBranchCKOwhen As String, ByVal mCOampm As String, ByVal mBranchCKI As String, ByVal mBranchCKIwhen As String, ByVal mCIampm As String, ByVal mHirePeriod As String, ByVal mProductID As String, ByVal mBooID As String, ByVal mPrgmName As String, ByVal mCityCode As String)
        Dim xmldoc As New XmlDocument

        ''rev:mia dec2-use the function to retrieve country
        ''-----------------------------------------------------------------
        Dim BranchCKOtext As String = ""
        If Not String.IsNullOrEmpty(BranchCKO) Then
            If BranchCKO.Contains("-") Then
                BranchCKOtext = BranchCKO.Split("-")(0)
            Else
                BranchCKOtext = BranchCKO.TrimEnd
            End If
        End If

        Dim qrytext As String = "select dbo.getCountryforlocation('" & BranchCKOtext & "',NULL,NULL)"
        Dim countrytext As String = Aurora.Common.Data.ExecuteScalarSQL(qrytext)

        ''just incase there are no data returned...trapped it
        If String.IsNullOrEmpty(countrytext) Then
            If BranchCKO.Contains("AKL") Or _
                       BranchCKO.Contains("AKX") Or _
                       BranchCKO.Contains("CHC") Or _
                       BranchCKO.Contains("TEU") Or _
                       BranchCKO.Contains("WKA") Or _
                       BranchCKO.Contains("WLG") Or _
                       BranchCKO.Contains("CH1") Or _
                       BranchCKO.Contains("AK1") Or _
                       BranchCKO.Contains("AK2") Or _
                       BranchCKO.Contains("CH2") Or _
                       BranchCKO.Contains("ZQN") Then
                mCityCode = "NZ"
            Else
                mCityCode = "AU"
            End If
        Else
            mCityCode = countrytext
        End If
        ''-----------------------------------------------------------------

        Dim blockingMessage As String = "" ''rev:mia nov.14 - this will the blocking message 
        Try

            LogInformation("Step3_Bindgrid: " _
                        & vbCrLf & "BkrID: " & mBKRid _
                        & vbCrLf & " ,branchOut: " & mBranchCKO _
                        & vbCrLf & " ,dteCKO: " & mBranchCKOwhen _
                        & vbCrLf & " ,ampmCKO: " & mCOampm _
                        & vbCrLf & " ,branchIn: " & mBranchCKI _
                        & vbCrLf & " ,dteCKI: " & mBranchCKIwhen _
                        & vbCrLf & " ,ampmCKI: " & mCIampm _
                        & vbCrLf & " ,txtHirePeriod: " & mHirePeriod _
                        & vbCrLf & " ,BooID: " & mBooID _
                        & vbCrLf & " ,CountryCode: " & mCityCode _
                        & vbCrLf & " ,product: " & mProductID _
                        & vbCrLf & " ,PromoCode: " & Me.PromoCode & vbCrLf & vbCrLf)



            xmlstring = ManageAvailability.GetAvailabilityWithCost _
                                                    (mBKRid, _
                                                     mBranchCKO, _
                                                     mBranchCKOwhen, _
                                                     mCOampm, _
                                                     mBranchCKI, _
                                                     mBranchCKIwhen, _
                                                     mCIampm, _
                                                     mHirePeriod, _
                                                     mProductID, _
                                                     "0", _
                                                     mBooID, _
                                                     "", _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     UserCode, _
                                                     mPrgmName, _
                                                     IIf(String.IsNullOrEmpty(mCityCode), MyBase.CountryCode, mCityCode), _
                                                     blockingMessage, _
                                                     False, _
                                                     Me.PromoCode) ''rev:mia Oct 16 2014 - addition of PromoCode


            ''rev:mia June 1 2010
            If String.IsNullOrEmpty(blockingMessage) And String.IsNullOrEmpty(ViewState("flagforBlockingRuleMessage")) Then
                ViewState("flagforBlockingRule") = False
                ViewState("flagforBlockingRuleMessage") = ""
                Me.HidWarningMessage.Value = ""
            Else
                ViewState("flagforBlockingRule") = True
                ViewState("flagforBlockingRuleMessage") = IIf(Not String.IsNullOrEmpty(blockingMessage), blockingMessage, Me.HidWarningMessage.Value)
                Me.HidWarningMessage.Value = ViewState("flagforBlockingRuleMessage")
            End If


            xmlstring = xmlstring.Replace("<Data><data>", "<Data>")
            xmlstring = xmlstring.Replace("</data></Data>", "</Data>")



            xmldoc.LoadXml(xmlstring)

            If Not (xmldoc.DocumentElement.SelectSingleNode("Error/ErrDescription/Aurora/Error/Description") Is Nothing) Then
                Dim errorMsg As String = xmldoc.DocumentElement.SelectSingleNode("Error/ErrDescription/Aurora/Error/Description").InnerText
                If Not String.IsNullOrEmpty(errorMsg) Then
                    Throw New Exception(errorMsg)
                End If
            Else
                If xmlstring.Equals("<data></data>") Or xmlstring.Equals("<Data></Data>") Then
                    Throw New Exception("Invalid XML: &lt;Data&gt;&lt;/Data&gt;")
                Else
                    If xmldoc.SelectSingleNode("Data/Error/ErrDescription") IsNot Nothing Then

                        Dim errorMsg As String = xmldoc.SelectSingleNode("Data/Error/ErrDescription").InnerText
                        If Not String.IsNullOrEmpty(errorMsg) Then
                            Throw New Exception(errorMsg)
                        End If
                    End If
                End If
            End If


            Me.ByPass = xmldoc.SelectSingleNode("//Data/Forward[@BP]").Attributes(0).Value
            Me.ProductName = xmldoc.SelectSingleNode("//Data/Forward/Vehicle/veh").InnerText
            Me.Availabilitynumber = xmldoc.SelectSingleNode("//Data/Forward/Avail/av").InnerText
            Me.CriteriaMatch = xmldoc.SelectSingleNode("//Data/Forward/Crit/cr").InnerText.Trim
            Me.Package = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pkg").InnerText
            Me.PP = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pp").InnerText
            Me.Brand = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/br").InnerText

            Me.UOM = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/uom").InnerText
            If mCostIndex = Nothing Then mCostIndex = "@tc"

            Me.Val = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[" & mCostIndex & "]").Attributes(0).Value
            Me.TCvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@tc]").Attributes(0).Value
            Me.PPvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@pp]").Attributes(1).Value
            Me.PDvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@pd]").Attributes(2).Value
            Me.VCvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vc]").Attributes(3).Value
            Me.VDNvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vdn]").Attributes(4).Value
            Me.VDGvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vdg]").Attributes(5).Value

            Me.CUR = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/cur").InnerText

            ''use in getxmlfor save
            Me.PKGid = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pkg[@pkgid]").Attributes(1).Value
            Me.veh_ID = xmldoc.SelectSingleNode("//Data/Forward/Vehicle/veh[@ID]").Attributes(0).Value


            Dim ds As New DataSet
            xmlstring = ExtractGetAvailXML(xmlstring)
            xmlstring = RemoveUnwantedChar(xmlstring)

            reader = New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            ds.ReadXml(reader)


            Dim view As New DataView(ds.Tables(0))
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC "
            End If

            ''------------------------------------------
            ''rev:mia jan 16 session state to viewstate
            Me.HidDS.Value = Server.HtmlEncode(ds.GetXml.ToString)
            ''Session("dssorted") = ds
            ''------------------------------------------

            ''rev:mia may 26 2011-remove sorting when page loading
            ''view.Sort = ViewState("sort")
            Step3_GridView1.DataSource = view

            Step3_GridView1.DataBind()
            trdataTableDefault.Visible = False

            hasDvassError = False
            ''POC enhancement
            Dim Submit As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSubmit"), Button)
            If CType(ViewState("flagforBlockingRule"), Boolean) = True Or Not String.IsNullOrEmpty(blockingMessage) Then ''rev:mia nov.14 - this will check the blocking message 
                Me.Step3_rdList.Enabled = False
                If Not String.IsNullOrEmpty(blockingMessage) Then
                    MyBase.SetWarningShortMessage(blockingMessage)
                    Me.SetErrorShortMessage(blockingMessage)
                End If

            Else
                Submit.Enabled = True
                Me.Step3_btnOneDayBack.Enabled = True
                Me.Step3_btnOneDayForward.Enabled = True
                Me.Step3_btnResubmit.Enabled = True
                Me.Step3_rdList.Enabled = True
            End If
            Step3_GridView1.Enabled = True
        Catch ex As Exception

            ''POC enhancement
            If String.IsNullOrEmpty(Me.HidWarningMessage.Value) Then
                MyBase.SetErrorShortMessage(ex.Message)
            Else
                Dim Submit As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSubmit"), Button)
                Step3_GridView1.Enabled = True
                Submit.Enabled = True
                Me.Step3_btnOneDayBack.Enabled = True
                Me.Step3_btnOneDayForward.Enabled = True
                Me.Step3_btnResubmit.Enabled = True
                Me.Step3_rdList.Enabled = True
            End If

            Me.trdataTableDefault.Visible = False
            hasDvassError = True
        Finally
            xmldoc = Nothing
        End Try

    End Sub

    Function RemoveUnwantedChar(ByVal xmlstring As String) As String
        xmlstring = xmlstring.Replace("%", " percent")
        xmlstring = xmlstring.Replace("&", " and")
        ''xmlstring = xmlstring.Replace("<", "&lt;")
        ''xmlstring = xmlstring.Replace(">", "&gt;")
        xmlstring = xmlstring.Replace("''", "")
        xmlstring = xmlstring.Replace("'", "")
        Return xmlstring
    End Function

    Dim hasDvassError As Boolean
    Sub Step3_BindGrid()
        Try
            Dim prodID As String = ""
            If blnresubmit = True Then
                prodID = Aurora.Booking.Services.BookingProcess.GetProductID(Me.Step3_PickerControlForProduct.DataId, MyBase.UserCode)
            Else
                prodID = Me.Step3_ProductID
            End If
            Dim ampmCKO As String = IIf(Me.Step3_ddlamppmCKO.Text.Contains("AM"), "AM", "PM")
            Dim ampmCKI As String = IIf(Me.Step3_ddlAMPMcki.Text.Contains("AM"), "AM", "PM")

            Dim dteCKO As String = DateTimeToString(Me.Step3_DateControlForCKO.Date, SystemCulture).Split(" "c)(0) & " " & Me.Step3_ddlamppmCKO.Text
            Dim dteCKI As String = DateTimeToString(Me.Step3_DateControlForCKI.Date, SystemCulture).Split(" "c)(0) & " " & Me.Step3_ddlAMPMcki.Text


            Me.Step3_BindGrid(Me.BkrID, _
                                    Me.BranchCKO, _
                                    dteCKO, _
                                    ampmCKO, _
                                    Me.BranchCKI, _
                                    dteCKI, _
                                    ampmCKI, _
                                    Me.Step3_txthireperiod.Text, _
                                    prodID, _
                                    Me.BooID, _
                                    Me.PrgmName, _
                                    Me.CityCode)
            blnresubmit = False


        Catch ex As Exception
            MyBase.AddErrorMessage("Step3_BindGrid() > " & ex.Message)
        End Try

    End Sub

    Sub Step3_BindGridSorted()
        Try

            ''------------------------------------------
            ''rev:mia jan 16 session state to viewstate
            ''------------------------------------------
            If String.IsNullOrEmpty(Me.HidDS.Value) Then Exit Sub
            Dim tempxml As String = Server.HtmlDecode(Me.HidDS.Value)
            Dim xmlReader As New XmlTextReader(tempxml, System.Xml.XmlNodeType.Document, Nothing)
            xmlReader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(xmlReader)



            Dim view As New DataView(ds.Tables(0))
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC "
            End If
            Me.HidDS.Value = Server.HtmlEncode(ds.GetXml.ToString)
            view.Sort = ViewState("sort")
            Step3_GridView1.DataSource = view
            Step3_GridView1.DataBind()

            'If CType(Session("dssorted"), DataSet) IsNot Nothing Then
            '    Dim view As New DataView(CType(Session("dssorted"), DataSet).Tables(0))
            '    If ViewState("sort") Is Nothing Then
            '        ViewState("sort") = "pkg_text ASC "
            '    End If
            '    Session("dssorted") = CType(Session("dssorted"), DataSet)
            '    view.Sort = ViewState("sort")
            '    Step3_GridView1.DataSource = view
            '    Step3_GridView1.DataBind()
            'End If
            ''------------------------------------------
        Catch ex As Exception
            MyBase.AddErrorMessage("Step3_BindGridSorted() > " & ex.Message)
        End Try

    End Sub

    Sub Step3_Resubmit(Optional ByVal withMsg As Boolean = False)
        If Not ValidateVehicleAvailability() Then
            Me.SetInformationShortMessage("All fields are required.")
            Exit Sub
        End If
        Dim sb As New StringBuilder
        Try
            Dim tempCKOdate As String = DateTimeToString(Me.Step3_DateControlForCKO.Date, SystemCulture).Split(" "c)(0) & " " & ParsetimeToString(Me.Step3_ddlamppmCKO.Text.ToString)
            Dim tempCKIdate As String = DateTimeToString(Me.Step3_DateControlForCKI.Date, SystemCulture).Split(" "c)(0) & " " & ParsetimeToString(Me.Step3_ddlAMPMcki.Text.ToString)
            Me.ProductIDForBypassRecalculation = GetProductID(Me.Step3_PickerControlForProduct.Text, MyBase.UserCode, Me.Step3_PickerControlForProduct.DataId)
            blnresubmit = True
            With sb
                .Append("<Root>")
                .AppendFormat("<BkrId>{0}</BkrId>", Me.BkrID)
                .AppendFormat("<vhrId>{0}</vhrId>", Me.VhrID)
                .AppendFormat("<choBranch>{0}</choBranch>", Me.BranchCKO)
                .AppendFormat("<choDate>{0}</choDate>", tempCKOdate)
                .AppendFormat("<choAmPm>{0}</choAmPm>", IIf(Me.Step3_ddlamppmCKO.Text.Contains("AM"), "AM", "PM"))
                .AppendFormat("<chiBranch>{0}</chiBranch>", Me.BranchCKI)
                .AppendFormat("<chiDate>{0}</chiDate>", tempCKIdate)
                .AppendFormat("<chiAmPm>{0}</chiAmPm>", IIf(Me.Step3_ddlAMPMcki.Text.Contains("AM"), "AM", "PM"))
                .AppendFormat("<prdId>{0}</prdId>", Me.ProductIDForBypassRecalculation)
                .AppendFormat("<hirPrd>{0}</hirPrd>", Me.txtHirePeriod.Text)
                .Append("</Root>")
            End With



            Dim strValue As String = SelectedVehicle.manageDuplicateVehicleRequest(sb.ToString)
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(strValue)

            Dim retvalue As Boolean = SelectedVehicle.VehicleRequestUpdateBypassValue(Me.UserCode, _
                                                                                                          Me.AgentID, _
                                                                                                          Me.Step3_DateControlForCKO.Text, _
                                                                                                          Me.Step3_DateControlForCKI.Text, _
                                                                                                          Me.txtHirePeriod.Text, _
                                                                                                          Me.BranchCKO, _
                                                                                                          Me.BranchCKI, _
                                                                                                          DateTime.Now, _
                                                                                                          Me.ProductIDForBypassRecalculation, _
                                                                                                          "", _
                                                                                                          xmldoc.SelectSingleNode("Root/Data").InnerXml) ''get the newly created vhrid



            If xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "False" Then
                hashProperties("VHRid") = xmldoc.SelectSingleNode("//Root/Data").InnerText
                Me.VhrID = xmldoc.SelectSingleNode("//Root/Data").InnerText
                Me.Step3_BindGrid()
                If withMsg Then
                    If hasDvassError = False Then
                        ''rev:mia nov14
                        If ViewState("flagforBlockingRule") = True AndAlso CType(ViewState("flagforBlockingRuleMessage"), String) <> "" Then
                            Me.SetErrorShortMessage(CType(ViewState("flagforBlockingRuleMessage"), String))
                        Else
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Your revised request has been resubmitted")
                        End If

                        Me.HidCKIampm.Value = Me.Step3_ddlAMPMcki.Text
                        Me.HidCKOampm.Value = Me.Step3_ddlamppmCKO.Text
                    Else
                        MyBase.SetWarningShortMessage("Your revised request has not been resubmitted")
                    End If
                End If
            Else
                MyBase.SetShortMessage(AuroraHeaderMessageType.Error, xmldoc.SelectSingleNode("//Root/Error/ErrDescription").InnerText)
            End If
            xmldoc = Nothing


        Catch ex As Exception
            MyBase.SetErrorMessage("Step3_Resubmit > " & ex.Message)
            Logging.LogError("Step3_Resubmit > ", ex.Message & vbCrLf & ex.StackTrace)
        End Try
    End Sub

    Sub Step3_OneDayBackAndForward(ByVal isOneDayBack As Boolean)
        If Not ValidateVehicleAvailability() Then
            Me.SetInformationShortMessage("All fields are required.")
            Exit Sub
        End If
        Dim inOnedayback As Integer
        Try


            If isOneDayBack = False Then
                inOnedayback = -1
            Else
                inOnedayback = 1
            End If


            Dim xmlstring As String = Aurora.Booking.Services.BookingProcess.CalculateRequestPeriod(DateTimeToString(Me.Step3_DateControlForCKO.Date, SystemCulture).Split(" "c)(0), _
                        IIf(Me.Step3_ddlamppmCKO.Text.Contains("AM"), "AM", "PM"), _
                        DateTimeToString(Me.Step3_DateControlForCKI.Date, SystemCulture).Split(" "c)(0), _
                        IIf(Me.Step3_ddlAMPMcki.Text.Contains("AM"), "AM", "PM"), _
                        Me.Step3_txthireperiod.Text, _
                        Me.Step3_ddldays.SelectedValue, inOnedayback)

            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xmlstring)

            ''rev:mia 14July
            Me.Step3_DateControlForCKO.Date = ParseDateTime(xmldoc.SelectSingleNode("//Root/D1").InnerText, SystemCulture)
            Me.Step3_DateControlForCKI.Date = ParseDateTime(xmldoc.SelectSingleNode("//Root/D2").InnerText, SystemCulture)



            Me.Step3_txthireperiod.Text = xmldoc.SelectSingleNode("//Root/Dif").InnerText
            xmldoc = Nothing
            Step3_Resubmit(True)

            Me.Step3_lblckiDay.Text = ParseDateAsDayWord(Me.Step3_DateControlForCKI.Text)
            Me.Step3_lblckoDay.Text = ParseDateAsDayWord(Me.Step3_DateControlForCKO.Text)


        Catch ex As Exception
            MyBase.SetErrorMessage("Step3_OneDayBackAndForward > " & ex.Message)
        End Try
    End Sub

    Sub Step3_GetXMLforSave(ByVal navigate As Boolean)

        If Not ValidateVehicleAvailability() Then
            Me.SetInformationShortMessage("All fields are required.")
            Exit Sub
        End If

        Dim sb As New StringBuilder

        Try
            sb.Append("<Root>")
            sb.Append(Step3_RetrieveValueFromGrid)
            sb.AppendFormat("<UserCode>{0}</UserCode>", UserCode)
            sb.AppendFormat("<PrgName>{0}</PrgName>", Me.ProgramID)
            sb.Append("</Root>")



            Dim objXML As New XmlDocument
            objXML.LoadXml(ManageAvailability.manageAvailableVehicle(Me.VhrID, sb.ToString))



            Dim strMessage As String = objXML.SelectSingleNode("//Root/Error/ErrDescription").InnerText
            Dim strSplitMsg As String() = strMessage.Split("^")
            If strMessage <> "" Then
                If objXML.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "false" Then
                    MyBase.SetInformationShortMessage(strSplitMsg(1))
                Else
                    MyBase.SetInformationShortMessage(strMessage)
                End If
            End If


            If (navigate AndAlso DoNavigate = True) Then
                pageValid = True
            Else
                If navigate = True AndAlso DoNavigate = False Then

                    Dim hpLinkPackage As HyperLink = CType(Step3_GridView1.Rows(0).Cells(0).FindControl("hpLinkPackage"), HyperLink)
                    If String.IsNullOrEmpty(hpLinkPackage.Text) Then
                        MyBase.SetInformationShortMessage("No package available.")
                        pageValid = False
                    Else
                        ''poc 
                        If CType(ViewState("flagforBlockingRule"), Boolean) = False Then
                            MyBase.SetInformationShortMessage("Please select the vehicle.")
                            pageValid = False
                        Else
                            ''rev:mia nov24
                            For Each _row As GridViewRow In Step3_GridView1.Rows
                                Dim tempQTY As TextBox = CType(_row.FindControl("txtvalue"), TextBox)
                                If tempQTY IsNot Nothing Then
                                    If tempQTY.Text = "0" Or tempQTY.Text = "" Then
                                        tempQTY.Text = "0"
                                        pageValid = False
                                    Else
                                        pageValid = True
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                        If pageValid = False AndAlso ViewState("flagforBlockingRule") = True Then
                            MyBase.SetInformationMessage("Please select the vehicle.")
                            pageValid = False
                        ElseIf pageValid = False And ViewState("flagforBlockingRule") = False Then
                            For Each _row As GridViewRow In Step3_GridView1.Rows
                                Dim tempQTY As TextBox = CType(_row.FindControl("txtvalue"), TextBox)
                                If tempQTY IsNot Nothing Then
                                    If tempQTY.Text = "0" Or tempQTY.Text = "" Then
                                        tempQTY.Text = "0"
                                    End If
                                End If
                            Next

                        End If
                    End If
                End If
            End If
            objXML = Nothing

        Catch ex As Exception
            Response.Redirect(QS_RS_VEHREQMGTNBR & "?Err=3")
        End Try
    End Sub

    Sub Step3_SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, Optional ByVal ddlControl As DropDownList = Nothing, Optional ByVal usercode As String = "", Optional ByVal dateinterval As Integer = 15)
        Try

            Dim index As Integer


            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If



            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, UserCode)
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)(0).ToString
            Dim endtime As String = ds.Tables(0).Rows(0)(1).ToString

            Dim strtime As String = startTime.Substring(0, 5)
            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If

            ddlControl.Items.Clear()
            ' fix by shoel to break out of blackhole loops :)
            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":")(0)), CInt(endtime.Split(":")(1)), CInt(endtime.Split(":")(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":")(0)), CInt(startTime.Split(":")(1)), CInt(startTime.Split(":")(2)))

            While tmTempTimeValue < tmEndTime
                ddlControl.Items.Add(tmTempTimeValue.Hours.ToString().PadLeft(2, "0") & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While

            ' when tmTempTimeValue = tmEndTime
            ddlControl.Items.Add(tmEndTime.Hours.ToString().PadLeft(2, "0") & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"))

            ' fix by shoel to break out of blackhole loops :)
            ddlControl.SelectedIndex = index

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub Step3_GetTable(ByVal ds As DataSet)
        Try


            Me.Step3_PickerControlForProduct.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productname")
            Me.Step3_PickerControlForProduct.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid")
            Me.Step3_txthireperiod.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")
            Me.Step3_HirePeriod = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")
            Me.Step3_ddldays.SelectedValue = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hiretype")
            Me.DaysMode = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hiretype")
            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If

            ''rev:mia nov18
            ''Step3_SelectLocationBasedOnCodeAndUser(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch"), Me.Step3_ddlamppmCKO, MyBase.UserCode, locationinterval)
            Step3_SelectLocationBasedOnCodeAndUserV2(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch"), DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate"), Me.Step3_ddlamppmCKO, locationinterval)

            Me.Step3_PickerControlForBranchCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch")


            Me.Step3_lblckoDay.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).coDw")

            ''rev:mia 14July
            Me.Step3_DateControlForCKO.Date = ParseDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate"), SystemCulture)

            If Me.BackUrl.Contains("package.aspx") Then
                Me.HidCKOampm.Value = ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")))
                Me.HidCKIampm.Value = ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm")))

                If Me.HidCKOampm.Value = "12:00 AM" Then
                    Me.HidCKOampm.Value = "08:00 AM"
                End If

                If Me.HidCKIampm.Value = "12:00 AM" Then
                    Me.HidCKIampm.Value = "08:00 AM"
                End If
            End If

            Me.BranchCKOwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")

            Dim ampmCKO As String
            If DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString.Contains("00:00") Then
                ampmCKO = ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")))
                ampmCKO = ampmCKO.Replace("00:00 ", "")
            Else
                ampmCKO = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString
                If ampmCKO.Contains("AM") Or ampmCKO.Contains("PM") Then
                    ampmCKO = ampmCKO.Split(" ")(0)
                End If
            End If

            Try

                Me.Step3_ddlamppmCKO.Text = ampmCKO
                If ampmCKO.Contains("A") Or ampmCKO.Contains("P") Then
                    If ampmCKO = "12:00 AM" Or ampmCKO = "12:00 PM" Then
                        Me.Step3_ddlamppmCKO.Items.FindByText("12:00 PM").Selected = True
                    Else
                        Me.Step3_ddlamppmCKO.Text = Me.ddlAMPMcheckout.Text
                    End If
                Else
                    Me.Step3_ddlamppmCKO.Text = ampmCKO
                End If


            Catch ex As Exception
                If ampmCKO = "12:00 AM" Or ampmCKO = "12:00 PM" Then
                    ''Me.Step3_ddlamppmCKO.Items.FindByText("12:00 PM").Selected = True
                End If
            End Try


            ''rev:mia nov18
            ''Step3_SelectLocationBasedOnCodeAndUser(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch"), Me.Step3_ddlAMPMcki, MyBase.UserCode, locationinterval)
            Step3_SelectLocationBasedOnCodeAndUserV2(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch"), DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate"), Me.Step3_ddlAMPMcki, locationinterval)

            Me.Step3_PickerControlForBranchCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch")
            Me.Step3_lblckiDay.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).ciDw")

            Me.Step3_DateControlForCKI.Date = ParseDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate"), SystemCulture)
            Me.BranchCKIwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")

            Dim ampmCKI As String
            If DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString.Contains("00:00") Then
                ampmCKI = ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm")))
                ampmCKI = ampmCKI.Replace("00:00 ", "")
            Else
                ampmCKI = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString
                If ampmCKI.Contains("AM") Or ampmCKI.Contains("PM") Then
                    ampmCKI = ampmCKI.Split(" ")(0)
                End If
            End If


            Try
                Me.Step3_ddlAMPMcki.Text = ampmCKI
                If ampmCKI.Contains("A") Or ampmCKI.Contains("P") Then
                    If ampmCKI = "12:00 AM" Or ampmCKI = "12:00 PM" Then
                        Me.Step3_ddlAMPMcki.Items.FindByText("12:00 PM").Selected = True
                    Else
                        Me.Step3_ddlAMPMcki.Text = Me.ddlAMPMcheckin.Text
                    End If
                Else
                    Me.Step3_ddlAMPMcki.Text = ampmCKI
                End If


            Catch ex As Exception
                If ampmCKI = "12:00 AM" Or ampmCKI = "12:00 PM" Then
                    ''Me.Step3_ddlAMPMcki.Items.FindByText("12:00 PM").Selected = True
                End If
            End Try


            Me.BkrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid")
            Me.CostDef = DataBinder.Eval(ds, "Tables(0).DefaultView(0).costdef")
            Me.BranchCKO = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch")
            Me.BranchCKOwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")
            Me.COampm = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")
            Me.VhrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).vhrId")
            Me.BranchCKI = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch")
            Me.BranchCKIwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")
            Me.CIampm = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm")
            Me.Step3_HirePeriod = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")
            Me.Step3_ProductID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid")
            Me.CityCode = DataBinder.Eval(ds, "Tables(0).DefaultView(0).rntCty")
            Dim userid As String = MyBase.UserCode
            Me.ProgramID = "BookingProcess.aspx--Step3_GetTable"


        Catch ex As Exception
            MyBase.SetErrorMessage("Step3_GetTable > " & ex.Message)
        End Try
    End Sub

    Sub Step3_VehicleRequest()
        blnresubmit = False
        ''rev:mia nov18
        ''CType(Me.Step3_PickerControlForBranchCKO.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKO('" & Me.UserCode & "');")
        ''CType(Me.Step3_PickerControlForBranchCKI.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKI('" & Me.UserCode & "');")

        ''rev:mia jan 19
        CType(Me.Step3_PickerControlForBranchCKO.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKOv2();")
        CType(Me.Step3_PickerControlForBranchCKI.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKIv2();")
        CType(Me.Step3_DateControlForCKO.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKOv2();")
        CType(Me.Step3_DateControlForCKI.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKIv2();")




        ''rev:mia dec1
        Me.Step3_ddlAMPMcki.Attributes.Add("onchange", "setInformationShortMessage('Please  Resubmit to update table');")
        Me.Step3_ddlamppmCKO.Attributes.Add("onchange", "setInformationShortMessage('Please Resubmit to update table');")
        Me.Step3_getDisplayAvailableVehiclesWithCost()
    End Sub

    ''rev:mia oct.23
    Private Function isValidateLocation() As Boolean
        If Me.Step3_PickerControlForBranchCKO.Text = "" Then
            Me.SetInformationShortMessage(CKO_REQUIRED_MSG)
            Return False
        ElseIf (Me.Step3_PickerControlForBranchCKO.Text.Contains("-") = False) Then
            Me.SetInformationShortMessage(CKO_INVALID_MSG)
            Return False
        End If

        If Me.Step3_PickerControlForBranchCKI.Text = "" Then
            Me.SetInformationShortMessage(CKI_REQUIRED_MSG)
            Return False
        ElseIf (Me.Step3_PickerControlForBranchCKI.Text.Contains("-") = False) Then
            Me.SetInformationShortMessage(CKI_INVALID_MSG)
            Return False
        End If

        Return True
    End Function
    Protected Sub Step3_DateControlForCKI_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_DateControlForCKI.DateChanged

        If Not CheckDateFormatting(False) Then Exit Sub

        CalculateRequestPeriod(BookingProcess.DisplayAvailablity)
        Me.Step3_lblckiDay.Text = ParseDateAsDayWord(Me.Step3_DateControlForCKI.Text)
        ''rev:mia nov25
        Dim location As String = ""
        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKI.Text) Then
            location = Me.Step3_PickerControlForBranchCKI.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKI.Text, Me.Step3_ddlAMPMcki)

        If Me.Step3_ddlAMPMcki.Items.Count = 0 And Step3_PickerControlForBranchCKI.Text.IndexOf("-") = -1 Then
            Me.SetWarningShortMessage(INVALID_LOCATION)
            Exit Sub
        End If
        If Me.Step3_ddlAMPMcki.Items.Count = 0 Then Exit Sub

        If ViewState("branchisclosed") = False Then
            Me.SetInformationShortMessage(RESUBMIT_TABLE)
        End If
        ViewState("branchisclosed") = False
        ViewState("TriggerByDateChanged") = True ''rev:mia nov20
    End Sub

    Protected Sub Step3_DateControlForCKO_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_DateControlForCKO.DateChanged

        If Not CheckDateFormatting(False) Then Exit Sub

        CalculateRequestPeriod(BookingProcess.DisplayAvailablity)
        Me.Step3_lblckoDay.Text = ParseDateAsDayWord(Me.Step3_DateControlForCKO.Text)
        ''rev:mia nov25
        Dim location As String = ""
        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKO.Text) Then
            location = Me.Step3_PickerControlForBranchCKO.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKO.Text, Me.Step3_ddlamppmCKO)

        If Me.Step3_ddlamppmCKO.Items.Count = 0 And Step3_PickerControlForBranchCKO.Text.IndexOf("-") = -1 Then
            Me.SetWarningShortMessage(INVALID_LOCATION)
            Exit Sub
        End If

        If Me.Step3_ddlamppmCKO.Items.Count = 0 Then Exit Sub

        If ViewState("branchisclosed") = False Then
            Me.SetInformationShortMessage(RESUBMIT_TABLE)
        End If
        ViewState("branchisclosed") = False
        ViewState("TriggerByDateChanged") = True ''rev:mia nov20
    End Sub

    Protected Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_btnResubmit.Click
        ' clear errors
        Me.ClearMessages()
        Me.ClearShortMessage()
        ' clear errors

        ''rev":mia nov25
        If IsBranchLocationClosed(False) = False Then Exit Sub
        If Not isValidateLocation() Then Exit Sub
        Step3_Resubmit(True)
        ViewState("TriggerByDateChanged") = False ''rev:mia nov20
    End Sub

    Protected Sub Step3_SaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ''rev":mia nov25
        If IsBranchLocationClosed(False) = False Then Exit Sub
        If Not isValidateLocation() Then Exit Sub
        Step3_GetXMLforSave(True)
        ViewState("TriggerByDateChanged") = False ''rev:mia nov20
    End Sub

    Protected Sub btnOneDayForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_btnOneDayForward.Click
        ''rev":mia nov25
        ''If IsBranchLocationClosed(False) = False Then Exit Sub

        ''rev:mia oct.23
        If Not isValidateLocation() Then Exit Sub
        Step3_OneDayBackAndForward(True)
        ViewState("TriggerByDateChanged") = False ''rev:mia nov20

        ''rev:mia dec1
        RevalidateTimes()
    End Sub

    Protected Sub btnOneDayBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_btnOneDayBack.Click
        ''rev":mia nov25
        ''If IsBranchLocationClosed(False) = False Then Exit Sub

        ''rev:mia oct.23
        If Not isValidateLocation() Then Exit Sub
        Step3_OneDayBackAndForward(False)
        ViewState("TriggerByDateChanged") = False ''rev:mia nov20

        ''rev:mia dec1
        RevalidateTimes()
    End Sub

    Protected Sub Step3_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ''rev":mia nov25
        If IsBranchLocationClosed(False) = False Then Exit Sub

        ''rev:mia nov14
        If ViewState("flagforBlockingRule") = True AndAlso CType(ViewState("flagforBlockingRuleMessage"), String) <> "" Then
            MyBase.SetShortMessage(AuroraHeaderMessageType.Warning, CType(ViewState("flagforBlockingRuleMessage"), String))
            Exit Sub
        End If

        If Not isValidateLocation() Then Exit Sub
        Step3_GetXMLforSave(False)
        ''rev:mia nov17
        If ViewState("flagforBlockingRule") = False AndAlso CType(ViewState("flagforBlockingRuleMessage"), String) = "" Then
            Me.HidWarningMessage.Value = ""
            ViewState("flagforBlockingRuleMessage") = "" ''rev:mia nov21
        End If
        ViewState("TriggerByDateChanged") = False ''rev:mia nov20
    End Sub

    Protected Sub Step3_GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles Step3_GridView1.Sorting
        If e.SortExpression = "pkg_text" Then
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC"
            Else
                If ViewState("sort") = "pkg_text ASC" Then
                    ViewState("sort") = "pkg_text DESC"
                Else
                    ViewState("sort") = "pkg_text ASC"
                End If
            End If

        Else
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "cur ASC"
            Else
                If ViewState("sort") = "cur ASC" Then
                    ViewState("sort") = "cur DESC"
                Else
                    ViewState("sort") = "cur ASC"
                End If
            End If

        End If
        Step3_BindGridSorted()
    End Sub

    Protected Sub step3_GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step3_GridView1.RowDataBound
        Dim lblvalue As Label = Nothing
        If e.Row.RowType = DataControlRowType.DataRow Then
            Select Case mCostIndex
                Case "@pp"
                    lblvalue = CType(e.Row.FindControl("lblvaluePP"), Label)
                Case "@pd"
                    lblvalue = CType(e.Row.FindControl("lblvaluePD"), Label)
                Case "@tc"
                    lblvalue = CType(e.Row.FindControl("lblvalueTC"), Label)
                Case "@vc"
                    lblvalue = CType(e.Row.FindControl("lblvalueVC"), Label)
                Case "@vdn"
                    lblvalue = CType(e.Row.FindControl("lblvalueVDN"), Label)
                Case "@vdg"
                    lblvalue = CType(e.Row.FindControl("lblvalueVDG"), Label)
            End Select

            If lblvalue IsNot Nothing Then
                lblvalue.Visible = True
            End If


            If sbglobal IsNot Nothing Then
                sbglobal = Nothing
            End If
            sbglobal = New StringBuilder

            Dim txtValue As TextBox = CType(e.Row.FindControl("txtvalue"), TextBox)
            If String.IsNullOrEmpty(txtValue.Text) Then
                txtValue.Text = 0
            End If


            Dim Submit As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSubmit"), Button)
            Dim SaveNext As Button = CType(Me.wizBookingProcess.FindControl("StepNavigationTemplateContainerId").FindControl("btnSaveNext"), Button)

            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)


            If String.IsNullOrEmpty(rowview("pkg_text").ToString) Then
                txtValue.Enabled = False
                ''SaveNext.Enabled = False ''rev:mia nov18
                ''Submit.Enabled = False

            Else
                SaveNext.Enabled = True
                Submit.Enabled = True
                txtValue.Enabled = True
            End If


            Dim hplink As HyperLink
            If Not rowview("pkg_text").ToString = "" Then
                hplink = CType(e.Row.FindControl("hpLinkPackage"), HyperLink)
                If hplink IsNot Nothing Then
                    hplink.NavigateUrl = GetPackageUrl(rowview("pkg_pkgid"))
                    hplink.ToolTip = rowview("pkg_pkgcom").ToString
                End If
            End If

            Dim hplinkCur As LinkButton = Nothing
            If Not rowview("cur").ToString = "" Then
                hplinkCur = CType(e.Row.FindControl("curLink"), LinkButton)
                If hplinkCur IsNot Nothing Then
                    hplinkCur.OnClientClick = "openCurrencyExchangeWindow('" & rowview("cur").ToString & "');return false;"
                End If
            End If

        End If
    End Sub

    Protected Sub step3_GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step3_GridView1.RowCreated
        Dim col As DataControlField = Nothing
        Dim img As HtmlImage = Nothing
        If e.Row.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To Step3_GridView1.Columns.Count - 1
                col = Step3_GridView1.Columns(i)
                img = New HtmlImage
                img.Border = 0

                If col.HeaderText = "Package" Or col.HeaderText = "CUR" Then
                    If CType(ViewState("sort"), String).Contains("ASC") Then
                        img.Src = "~/images/sort_ascending.gif"
                        e.Row.Cells(i).Controls.Add(img)
                    Else
                        img.Src = "~/images/sort_descending.gif"
                        e.Row.Cells(i).Controls.Add(img)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub step3_rdList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_rdList.SelectedIndexChanged
        Select Case Step3_rdList.SelectedIndex
            Case 0 'perperson 2369
                mCostIndex = "@pp"
            Case 1 ' perday  189
                mCostIndex = "@pd"
            Case 2 ' total   4738
                mCostIndex = "@tc"
            Case 3  'vehtotal 4738
                mCostIndex = "@vc"
            Case 4  'vehdaynet 189
                mCostIndex = "@vdn"
            Case 5  'vehdaygross 252
                mCostIndex = "@vdg"
        End Select

        '' BindGrid()
        Step3_BindGridSorted()


    End Sub
#End Region

#Region " Summary"


    Private Const SESSION_selectedVehicleListProperties As String = "selectedVehicleListProperties"
    Private Const SESSION_selectedVehicleList_xmlChosenVehicle As String = "xmlChosenVehicle"
    Private Const SESSION_selectedVehicleList_xmlCur As String = "xmlCur"
    Private Const SESSION_selectedVehicleList_xmlErrorMsg As String = "xmlErrorMsg"
    Private Const SESSION_selectedVehicleList_xmlRequest As String = "xmlRequest"
    Private Const QS_BOOKING As String = "Booking.aspx?funCode=RS-BOKSUMMGT"
    Protected xmlRequest As XmlDocument
    Protected xmlRequestSource As XmlDocument
    Protected xmlRequestKnockBack As XmlDocument
    Protected xmlErrorMsg As XmlDocument
    Protected xmlChosenVehicle As XmlDocument
    Protected xmlCur As XmlDocument

    Protected xmlDefaultstring As String = "<xml><Data><Curr><code><value>0</value></code></Curr></Data></xml>"
    Protected gRecExist As Boolean = True
    Protected mTotalCheckItems As Decimal = 0.0


    Public Function FormattedDateAndTime(ByVal objDate As Object) As String
        Dim strDate As String() = Convert.ToString(objDate).Split(" ")
        Dim partialdate As String
        Try
            partialdate = DateTime.Parse(strDate(0))
            If strDate.Length > 1 Then
                Return Utility.DateDBUIConvert(partialdate) & " " & strDate(1)
            Else
                Return Utility.DateDBParse(partialdate).ToString("dd/MM/yyyy hh:mm tt")
            End If


        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Function FormattedDate(ByVal objDate As Object) As String
        Dim strDate As String() = Convert.ToString(objDate).Split(" ")
        Dim partialdate As String
        Try
            partialdate = DateTime.Parse(strDate(0))
            If strDate.Length > 1 Then
                Return Utility.DateDBUIConvert(partialdate) & " " & strDate(1)
            Else
                Return Utility.DateDBUIConvert(partialdate)
            End If


        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Property Msg() As String
        Get
            Return CType(ViewState("Msg"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Msg") = value
        End Set
    End Property

    Protected Property TotalCheckItems() As String
        Get
            Return FormatCurrency(CType(ViewState("TotalCheckItems"), String))
        End Get
        Set(ByVal value As String)
            ViewState("TotalCheckItems") = value
        End Set
    End Property

    Private Property AlertMessage() As String
        Get
            Return CType(ViewState("AlertMessage"), String)
        End Get
        Set(ByVal value As String)
            ViewState("AlertMessage") = value
        End Set
    End Property

    Private Property SLVid() As String
        Get
            Return CType(ViewState("SLVid"), String)
        End Get
        Set(ByVal value As String)
            ViewState("SLVid") = value
        End Set
    End Property

    Private Property AgnDir() As String
        Get
            Return CType(ViewState("AgnDir"), String)
        End Get
        Set(ByVal value As String)
            ViewState("AgnDir") = value
        End Set
    End Property

    Private Property Step4_BYpass() As String
        Get
            Return CType(ViewState("Step4_BYpass"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Step4_BYpass") = value
        End Set
    End Property

    Private Property HeaderMsg() As String
        Get
            Return CType(ViewState("HeaderMsg"), String)
        End Get
        Set(ByVal value As String)
            ViewState("HeaderMsg") = value
        End Set
    End Property

    Function Step4_checkVehicleSchedule(ByVal Parameter As String, Optional ByVal POS As Boolean = False) As Boolean
        Try

            If Not (Parameter.Equals("NN") OrElse Parameter.Equals("KK")) Then
                Return True
            End If

            Dim ChkValue As String = "-1"
            Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            Dim slvID As String = String.Empty
            Dim xmlstring As String = String.Empty
            Dim VehSel As String = String.Empty

            For i As Integer = 0 To ctr
                ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
                slvID = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("SlvId").InnerText
                VehSel = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("VehSel").InnerText
                If ChkValue.Equals("1") Then
                    If Not String.IsNullOrEmpty(slvID) Then
                        If Parameter.Equals(slvID) Then
                            xmlstring = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.RES_IsAuroraVehicleSchedule('" & Parameter & "')")
                        Else
                            xmlstring = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.RES_IsAuroraVehicleSchedule('" & slvID & "')")
                        End If

                        If Not String.IsNullOrEmpty(xmlstring) Then
                            xmlstring = xmlstring.Replace("<Data>", String.Empty)
                            xmlstring = xmlstring.Replace("</Data>", String.Empty).TrimEnd

                            ''REV:MIA POC
                            If Not POS Then
                                If xmlstring.Equals("0") = True OrElse xmlstring.Equals(0) Then

                                    If Trace.IsEnabled Then
                                        Trace.Warn("Step4_checkVehicleSchedule : RES_IsAuroraVehicleSchedule -- " & xmlstring)
                                    End If

                                    Me.AlertMessage = "Status for Product " & VehSel
                                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, Step4_DispClientMessage(xmlErrorMsg, "GEN095", "KK & NN," & Me.AlertMessage))
                                    Return False
                                End If
                            End If
                        End If
                    End If
                End If
            Next

            Return True

        Catch ex As Exception
            Me.SetErrorMessage("Step4_checkVehicleSchedule(" & Parameter & ") > " & ex.Message)
            Return False
        End Try

    End Function

    Private Function Step4_DispClientMessage(ByRef xmlErrorMsg As XmlDocument, ByVal msgcode As String, ByVal msgparam As String) As String
        Dim msg As String = String.Empty
        Dim retvalue As String = String.Empty
        Dim param As String = String.Empty
        Dim paramString() As String = Nothing
        Dim i As Integer

        Dim ctr As Integer = xmlErrorMsg.DocumentElement.ChildNodes(0).ChildNodes.Count - 1
        For i = 0 To ctr
            msg = xmlErrorMsg.DocumentElement.SelectSingleNode("Msg").InnerText
            retvalue = msg.IndexOf(msgcode)
            If retvalue <> -1 Then
                Exit For
            End If
        Next

        param = msgparam
        If Not String.IsNullOrEmpty(param) Then
            paramString = param.Split(",")
            For ii As Integer = 0 To paramString.Length - 1
                If Not String.IsNullOrEmpty(paramString(ii)) Then
                    msg = msg.Replace("xxx" & (ii + 1), paramString(ii))
                End If
            Next
        End If
        For iii As Integer = 0 To paramString.Length - 1
            msg = msg.Replace("xxx" & (iii + 1), "")
        Next
        Return msg
    End Function

    Function Step4_ButtonValidation(ByVal modetype As String, Optional ByVal POS As Boolean = False) As Boolean

        Try

            If modetype.Equals("IN") Or modetype.Equals("QN") Then Return True
            Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1

            Dim ChkValue As String = "-1"
            Dim avail As String = String.Empty
            Dim AvpBlocking As String = String.Empty


            For i As Integer = 0 To ctr
                ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
                avail = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Avail").InnerText
                AvpBlocking = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("AvpBlocking").InnerText

                If Not POS Then
                    If ChkValue.Equals("1") Then
                        If avail.Equals("NA") AndAlso AvpBlocking.Equals("Bypass") Then
                            Me.AlertMessage = "Option only available when vehicle availability has been checked or \n when processing a \'free sell\' request."
                            Return False
                        End If
                    End If
                End If
            Next
            Return True

        Catch ex As Exception
            Me.SetErrorMessage("Step4_ButtonValidation(" & modetype & ") > " & ex.Message)
            Return False
        End Try


    End Function

    Function Step4_ChangeChoosenType(ByVal myvalue As String, Optional ByVal POS As Boolean = False) As Boolean
        Try
            If Me.gRecExist = False Then Exit Function
            Dim bcheck As Boolean = False
            Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1

            Dim ChkValue As String = "-1"

            For i As Integer = 0 To ctr
                ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
                If ChkValue.Equals("1") Then
                    Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Status").InnerText = myvalue
                    bcheck = True
                End If
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
            Next
            If bcheck = False AndAlso Not POS Then
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select the record")
                Return False
            End If
            CType(Me.Step4_GridSummary.HeaderRow.Controls(10).FindControl("chkCheckHeader"), CheckBox).Checked = False
            CType(Me.Step4_GridSummary.FooterRow.FindControl("lblfootercost"), Label).Text = ""
            Return True
        Catch ex As Exception
            Me.SetErrorMessage("Step4_ChangeChoosenType(" & myvalue & ") > " & ex.Message)
            Return False
        End Try

    End Function

    Function Step4_ValidData() As Boolean

        Try
            Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            Dim status As String = String.Empty

            For i As Integer = 0 To ctr
                status = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Status").InnerText
                If String.IsNullOrEmpty(status) Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select the status")
                    Return False
                End If
                If status.Equals("KB") AndAlso (String.IsNullOrEmpty(Me.ddlKnockBack.SelectedItem.Text)) Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "For Status KB KnockBack type must be selected")
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Me.SetErrorMessage("Step4_ValidData > " & ex.Message)
            Return False
        End Try

    End Function

    Sub Step4_SubmitData(ByVal modeCase As String)
        Try

            ''rev:mia 19nov2014 start - part of  slot dropdown addition
            Dim avpId As String = ""
            ''rev:mia 19nov2014 end - part of  slot dropdown addition

            If (Me.gRecExist = False AndAlso modeCase.Equals("mbr") = False AndAlso modeCase.Equals("nbr")) Then
                Exit Sub
            End If

            Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            LogDebug("MANNY xmlChosenVehicle: " & xmlChosenVehicle.OuterXml)
            Dim ChkValue As String = "-1"
            For i As Integer = 0 To ctr
                ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
                If ChkValue.Equals("-1") Then
                    Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
                Else
                    ''rev:mia 19nov2014 start - part of  slot dropdown addition
                    Dim status As String = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Status").InnerText
                    If (String.IsNullOrEmpty(avpId) And ChkValue = "1" And (modeCase = "KK" Or modeCase = "NN" Or status = "KK" Or status = "NN")) Then
                        ''If (String.IsNullOrEmpty(avpId) And ChkValue = "1" And (status = "KK" Or status = "NN")) Then
                        avpId = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("AvpId").InnerText.TrimEnd
                    End If
                    ''rev:mia 19nov2014 end - part of  slot dropdown addition
                End If

                ' Fix for page not remembering status
                If UCase(Trim(modeCase)) = "KB" AndAlso CBool(ViewState("flagforBlockingRule")) AndAlso i = ctr Then
                    ' looking at status forced to KB, Blocking rule flag present, the latest vehicle has the problem
                    AddToListOfProblemVehicles(Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("SlvId").InnerText, ProblemsWithSelectedVehicle.SelectedVehicleIsKBOnly)
                End If
                ' Fix for page not remembering status

            Next

            Select Case modeCase
                Case "IN"

                    Step4_ButtonValidation("IN")
                    Me.Step4_ChangeChoosenType("IN")
                    ClearSlotDataForNonKKandNN()
                Case "QN", "NN", "KK"
                    If Me.Step4_ButtonValidation(modeCase) = False Then Exit Sub
                    If Me.Step4_checkVehicleSchedule(modeCase) = False Then
                        Exit Sub
                    End If

                    If Me.Step4_ChangeChoosenType(modeCase) = False Then Exit Sub
                    Me.Step4_RecalculateRates()


                    ''rev:mia 19nov2014 start - part of  slot dropdown addition
                    If (modeCase = "NN" Or modeCase = "KK" And Not String.IsNullOrEmpty(avpId)) Then
                        SlotAvpIDForKKandNN = avpId
                        TimeSlotAvailability.NoOfBulkBookings = NoOfBulkBookings
                        TimeSlotAvailability.SupervisorOverride = False
                        TimeSlotAvailability.Show(avpId)
                    Else
                        ClearSlotDataForNonKKandNN()
                    End If
                    ''rev:mia 19nov2014 end - part of  slot dropdown addition
                Case "KB"

                    ''rev:mia POC
                    If ViewState("flagforBlockingRule") = False Then
                        If String.IsNullOrEmpty(Me.ddlKnockBack.SelectedItem.Text) Then
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select a valid KnockBack type")
                            Exit Sub
                        End If
                        If Me.Step4_ButtonValidation(modeCase) = False Then Exit Sub
                        If Me.Step4_checkVehicleSchedule(modeCase) = False Then Exit Sub
                        If Me.Step4_ChangeChoosenType(modeCase) = False Then Exit Sub

                    Else
                        If Me.Step4_ButtonValidation(modeCase, True) = False Then Exit Sub
                        If Me.Step4_checkVehicleSchedule(modeCase, True) = False Then Exit Sub
                        If Me.Step4_ChangeChoosenType(modeCase, True) = False Then Exit Sub
                    End If
                    ClearSlotDataForNonKKandNN()
                Case "WL"

                    If Me.Step4_ButtonValidation(modeCase) = False Then Exit Sub
                    If Me.Step4_ChangeChoosenType(modeCase) = False Then Exit Sub
                    ClearSlotDataForNonKKandNN()
                Case "MBR"
                    ClearSlotDataForNonKKandNN()
                Case "NBR"

                    Me.RemoveSessionForNBR()
                    ClearSlotDataForNonKKandNN()
                    Response.Redirect(QS_RS_VEHREQMGTNBR)
                Case "SAVE", "SAVENEXT"
                    If Me.Step4_ValidData = False Then Exit Sub
                    If (modeCase = "SAVENEXT" And TimeSlotAvailability.IsThereAnySlot = True) Then
                        ''REV:19NOV2014 - SLOT MANAGEMENT
                        If (String.IsNullOrEmpty(SlotId) And String.IsNullOrEmpty(SlotDescription) And String.IsNullOrEmpty(SlotAvpID)) Then
                            SetErrorShortMessage("Time Slot is mandatory")
                            Exit Sub
                        End If
                    Else
                        ''double check
                        If (Not String.IsNullOrEmpty(SlotAvpIDForKKandNN)) Then
                            If (TimeSlotAvailability.IsSlotRequired(SlotAvpIDForKKandNN) = True And modeCase = "SAVENEXT") Then
                                If (String.IsNullOrEmpty(SlotId) And String.IsNullOrEmpty(SlotDescription) And String.IsNullOrEmpty(SlotAvpID)) Then
                                    SetErrorShortMessage("Time Slot is mandatory")
                                    Exit Sub
                                End If
                            End If
                        End If
                        
                    End If

                    Me.txtNewBookingNumber.Text = Me.txtNewBookingNumber.Text.Replace(" ", "")
                    Me.Step4_SaveAndNext(modeCase)
                Case "BACK"
            End Select
            Me.Step4_BindSummaryGrid()


            ''rev:mia sept21
            Me.Hidchosenvehicle.Text = Server.HtmlEncode(Me.xmlChosenVehicle.OuterXml)


            ''-----------------------------------------
            ''rev:mia jan 16 session state to viewstate
            ''-----------------------------------------
            ''Session(SESSION_selectedVehicleList_xmlChosenVehicle) = xmlChosenVehicle
            _SESSION_selectedVehicleList_xmlChosenVehicle = xmlChosenVehicle.OuterXml
            ''-----------------------------------------
        Catch ex As Exception
        End Try

    End Sub

    Sub Step4_RecalculateRates()

        Dim strBuilder As New StringBuilder

        Try
            With strBuilder
                .Append("<Root>")
                .Append(Me.xmlRequest.SelectSingleNode("/Data/Header").OuterXml)
                .Append(Me.xmlChosenVehicle.OuterXml)
                .AppendFormat("<BooId>{0}</BooId>", Me.BooID)
                .AppendFormat("<BpdId>{0}</BpdId>", Me.BPDid)
                .AppendFormat("<UsrCode>{0}</UsrCode>", UserCode)
                .Append("</Root>")
            End With
            Dim xmlstring As String = SelectedVehicle.manageRecalculateRates(strBuilder.ToString)
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xmlstring)
            If xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText.Equals("True") Then
                Dim msg As String = xmldoc.SelectSingleNode("//Root/Error/ErrDescription").InnerText
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, msg)
            End If
            xmlstring = xmldoc.OuterXml
            xmlstring = xmlstring.Replace("</data>", "")
            xmlstring = xmlstring.Replace("<data>", "")
            xmldoc = Nothing
            xmldoc = New XmlDocument
            xmldoc.LoadXml(xmlstring)
            xmldoc.LoadXml(xmldoc.SelectSingleNode("//Data/SelectedVehicles").OuterXml)
            For i As Integer = 0 To xmldoc.DocumentElement.ChildNodes.Count - 1
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Cost").InnerText = xmldoc.DocumentElement.ChildNodes(i).SelectSingleNode("Cost").InnerText
            Next
            xmldoc = Nothing
        Catch ex As Exception
            Me.SetErrorMessage("Step4_RecalculateRates > " & ex.Message)
        End Try


    End Sub

    Sub Step4_LoadXMLDocument(ByVal bkrid As String, ByVal booid As String, ByVal bpdid As String, ByVal butSel As String)

        Try
            Me.Step4_LoadXMLRequest(bkrid, booid, bpdid, butSel)
            Me.Step4_LoadXMLRequestKnockBack()
            Me.Step4_LoadXMLErrorMsg()
            Me.Step4_LoadXMLcur()

        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadXMLDocument > " & ex.Message)
        End Try

    End Sub

    Sub Step4_LoadXMLRequest(ByVal bkrid As String, ByVal booid As String, ByVal bpdid As String, ByVal butSel As String)
        Try

            xmlRequest = New XmlDocument
            xmlRequest = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_SummaryOfChosenVehicles", bkrid, booid, bpdid, butSel)

            Dim xmlstring As String = xmlRequest.OuterXml.ToString

            'Logging.LogDebug("xmlRequest - MANNY", xmlstring)

            'If Trace.IsEnabled And Me.UserCode = "ma2" Then
            '    Trace.Warn("Step4_LoadXMLRequest: xmlRequest : " & xmlstring)
            'End If
            'DisplayTestData("Step4_LoadXMLRequest: xmlRequest : ", Server.HtmlEncode(xmlstring))

            xmlstring = xmlstring.Replace("<data>", "<Data>")
            xmlstring = xmlstring.Replace("</data>", "</Data>")
            xmlstring = RemoveUnwantedChar(xmlstring)
            xmlRequest.LoadXml(xmlstring)

            Dim ds As DataSet
            ds = New DataSet
            ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))


        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadXMLRequest > " & ex.Message)
        End Try


    End Sub

    Sub Step4_LoadXMLRequestKnockBack()
        Try

            xmlRequestKnockBack = New XmlDocument
            xmlRequestKnockBack = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "30", "", "")

            Dim xmlstring As String = xmlRequestKnockBack.OuterXml.ToString
            xmlstring = RemoveUnwantedChar(xmlstring)
            Dim ds As DataSet
            ds = New DataSet
            ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

            Me.ddlKnockBack.DataSource = ds
            Me.ddlKnockBack.DataTextField = "CODE"
            Me.ddlKnockBack.DataValueField = "ID"
            Me.ddlKnockBack.DataBind()

            Me.ddlKnockBack.Items.Insert(0, String.Empty)
            Me.ddlKnockBack.SelectedIndex = -1


        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadXMLRequestKnockBack > " & ex.Message)
        End Try


    End Sub

    Sub Step4_LoadXMLErrorMsg()
        Try
            xmlErrorMsg = New XmlDocument
            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("GEN_getMsgToClient", "GEN095")
            xmlErrorMsg.LoadXml(xmlstring)

        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadXMLErrorMsg > " & ex.Message)
        End Try

    End Sub

    Sub Step4_LoadXMLcur()
        Try
            xmlCur = New XmlDocument
            xmlCur.LoadXml(Me.xmlDefaultstring)
        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadXMLcur > " & ex.Message)
        End Try

    End Sub

    Sub Step4_LoadSummaryScreen()

        Try

            If Trace.IsEnabled And Me.UserCode = "ma2" Then
                Trace.Warn("Step4_LoadSummaryScreen: " & Me.BkrID & " - " & Me.BooID)
            End If
            DisplayTestData("Step4_LoadSummaryScreen: bkrid ", Me.BkrID & " - " & Me.BooID)

            Step4_LoadXMLDocument(Me.BkrID, Me.BooID, Me.BPDid, Me.hid_html_butSel.Value)
            Me.CityCode = xmlRequest.SelectSingleNode("//Data/Cty").InnerText
            Me.Msg = xmlRequest.SelectSingleNode("//Data/Msg").InnerText
            Me.SplitData(BookingProcess.Summary)
            Me.Step4_GetCurrency()
            Me.Step4_ChangeData(False)

            If Me.xmlRequest.SelectSingleNode("//Data/Header[@BooNum]").Attributes(0).Value = "" Then
                MyBase.SetValueLink("", "")
            Else
                ''rev:mia July 2 2013 - back to the office. 
                Dim booIdQS As String = Me.xmlRequest.SelectSingleNode("//Data/Header[@BooNum]").Attributes(0).Value.ToString
                Dim rentalIdQS As String = "&hdRentalId=" & booIdQS
                If Not Request.QueryString("hdRentalId") Is Nothing Then
                    rentalIdQS = "&hdRentalId=" & Request.QueryString("hdRentalId")
                End If
                Dim tempURL As String = "~/Booking/Booking.aspx?activeTab=7&isFromSearch=0&hdBookingId=" & booIdQS & rentalIdQS
                Me.SetValueLink(booIdQS, tempURL)

            End If

            ''POC
            If xmlChosenVehicle IsNot Nothing Then
                Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
                For i As Integer = 0 To ctr
                    Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Poc").InnerText = "1"
                Next
            End If


            If Me.xmlRequest.SelectSingleNode("//Data/EnabNewBoo").InnerText = "0" Or Not Me.Hidbooid.Value = "" Then
                Me.tr.Visible = False
            End If

            ''-----------------------------------------
            ''rev:mia jan 16 session state to viewstate
            ''-----------------------------------------
            ''Me.AddToSession(BookingProcess.Summary)
            Me.AddToSessionR2(BookingProcess.Summary)
            ''-----------------------------------------

            ''rev:mia POC
            Dim SubmitSummary As Button = CType(Me.wizBookingProcess.FindControl("FinishNavigationTemplateContainerId").FindControl("BtnSaveSummary"), Button)
            LogDebug("ViewState('flagforBlockingRule') " & ViewState("flagforBlockingRule"))
            If CType(ViewState("flagforBlockingRule"), Boolean) = True Then
                POS_Forcecheck()
                Me.Step4_SubmitData("KB")

                Dim item As ListItem = ddlKnockBack.Items.FindByText("Blocking Rule")
                If item IsNot Nothing Then
                    ddlKnockBack.SelectedItem.Text = item.Text
                    ddlKnockBack.SelectedItem.Value = item.Value
                    ViewState("POS") = item.Value
                End If
                ' Fix for page not remembering status
                'Me.BtnInquiry.Enabled = False
                'Me.BtnQuote.Enabled = False
                'Me.BtnKnockBack.Enabled = False
                'Me.BtnWaitlist.Enabled = False
                'Me.BtnProvisional.Enabled = False
                'Me.BtnConfirm.Enabled = False

                'SubmitSummary.Enabled = False
                'Me.ddlKnockBack.Enabled = False
                'Me.Step4_GridSummary.Enabled = False
                'Else
                'Me.BtnInquiry.Enabled = True
                'Me.BtnQuote.Enabled = True
                'Me.BtnKnockBack.Enabled = True
                'Me.BtnWaitlist.Enabled = True
                'Me.BtnProvisional.Enabled = True
                'Me.BtnConfirm.Enabled = True
                'SubmitSummary.Enabled = True
                'Me.ddlKnockBack.Enabled = True
                'Me.Step4_GridSummary.Enabled = True
                ' Fix for page not remembering status
            End If

            ' Fix for page not remembering status
            ' check the status of all rows, if all are KB then do the following
            Dim bAllRowsAreKB As Boolean = True, bAtleastOneKBRowPresent As Boolean = False
            For Each rwVehicleRow As GridViewRow In Step4_GridSummary.Rows
                If DirectCast(rwVehicleRow.Cells(9), System.Web.UI.WebControls.DataControlFieldCell).Text.Trim().ToUpper() <> "KB" Then
                    bAllRowsAreKB = False
                Else
                    bAtleastOneKBRowPresent = True
                End If
            Next
            LogDebug("bAllRowsAreKB " & bAllRowsAreKB)
            If bAllRowsAreKB Then
                Me.BtnInquiry.Enabled = False
                Me.BtnQuote.Enabled = False
                Me.BtnKnockBack.Enabled = False
                Me.BtnWaitlist.Enabled = False
                Me.BtnProvisional.Enabled = False
                Me.BtnConfirm.Enabled = False
                SubmitSummary.Enabled = False
                Me.Step4_GridSummary.Enabled = False
            Else ' there is atleast one non-KB row

                ''rev:mia July 27 2012 - Fixed for all buttons that are being enabled even there is a blocking rul
                If CType(ViewState("flagforBlockingRule"), Boolean) = True Then
                    Me.BtnInquiry.Enabled = False
                    Me.BtnQuote.Enabled = False
                    Me.BtnKnockBack.Enabled = True
                    Me.BtnWaitlist.Enabled = False
                    Me.BtnProvisional.Enabled = False
                    Me.BtnConfirm.Enabled = False
                Else
                    Me.BtnInquiry.Enabled = True
                    Me.BtnQuote.Enabled = True
                    Me.BtnKnockBack.Enabled = True
                    Me.BtnWaitlist.Enabled = True
                    Me.BtnProvisional.Enabled = True
                    Me.BtnConfirm.Enabled = True
                    SubmitSummary.Enabled = True
                End If

                Me.Step4_GridSummary.Enabled = True
            End If

            ' if there is even 1 KB row, the drop down is to be disabled
            Me.ddlKnockBack.Enabled = Not bAtleastOneKBRowPresent
            ' Fix for page not remembering status

            ''rev:mia 19nov2014 start - part of  slot dropdown addition
            'Dim slotRentalId As String = Me.xmlRequest.SelectSingleNode("//Data/SelectedVehicles/SelectedVehicle/RentalId").InnerText
            'If (slotRentalId.Contains("/") = True) Then
            '    slotRentalId = slotRentalId.Replace("/", "") & "-1"
            'End If
            'PopulateAvailableSlot(slotRentalId)
            ''rev:mia 19nov2014 end - part of  slot dropdown addition


        Catch ex As Exception
            Me.SetErrorMessage("Step4_LoadSummaryScreen > " & ex.Message)
        End Try


    End Sub

    Sub Step4_BindSummaryGrid()
        Dim ds As New DataSet

        Try

            ds.ReadXml(New XmlTextReader(xmlChosenVehicle.OuterXml.ToString, System.Xml.XmlNodeType.Document, Nothing))

            Me.Step4_GridSummary.DataSource = ds
            Me.Step4_GridSummary.DataBind()
            Me.lblselectedmsg.Text = Me.HeaderMsg

            ''rev:mia 3dec2014 start - part of  TCX
            NoOfBulkBookings = ds.Tables(0).Rows.Count
            ''rev:mia 3dec2014 end   - part of  TCX


        Catch ex As Exception
            Me.SetErrorMessage("Step4_BindSummaryGrid > " & ex.Message)
        End Try

    End Sub

    Sub Step4_GetCurrency()
        Dim sb As New StringBuilder
        If gRecExist = False Then Exit Sub
        Dim bExist As Boolean = False

        Try

            Dim ctrChildnodes As Integer = xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            Dim ctrXmlcurnodes As Integer = xmlCur.DocumentElement.ChildNodes.Count - 1

            Dim Xmlcurdata As String = String.Empty
            Dim Xmlchosendata As String = String.Empty

            For i As Integer = 0 To ctrChildnodes
                For ii As Integer = 0 To ctrXmlcurnodes
                    Xmlcurdata = xmlCur.SelectSingleNode("//Data").ChildNodes(ii).SelectSingleNode("code").InnerText
                    Xmlchosendata = xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText
                    If String.IsNullOrEmpty(Xmlcurdata) = False Then
                        If Xmlcurdata.Equals(Xmlchosendata) Then
                            bExist = True
                        End If
                    Else
                        bExist = False
                    End If
                Next

                If bExist = False Then
                    With sb
                        .Append("<Curr>")
                        .AppendFormat("<code>{0}</code>", xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText)
                        .Append("<value>0</value>")
                        .Append("</Curr>")
                    End With
                End If
                If xmlCur IsNot Nothing Then
                    xmlCur = Nothing
                    xmlCur = New XmlDocument
                End If
                xmlCur.LoadXml("<Data>" & sb.ToString & "</Data>")
            Next

        Catch ex As Exception
            Me.SetErrorMessage("Step4_GetCurrency > " & ex.Message)
        End Try

    End Sub

    Sub Step4_ChangeData(ByVal chkValue As Boolean)
        If gRecExist = False Then Exit Sub

        Try
            Dim ctrNode As Integer = xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            For i As Integer = 0 To ctrNode
                xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
            Next

            If chkValue = False Then
                Step4_BindSummaryGrid()
            End If
        Catch ex As Exception
            Me.SetErrorMessage("Step4_ChangeData > " & ex.Message)
        End Try
    End Sub

    Sub Step4_CheckAll(ByVal controlname As String, ByVal check As Boolean)
        Dim chk As CheckBox = Nothing

        Try
            For Each row As GridViewRow In Me.Step4_GridSummary.Rows
                chk = row.FindControl(controlname)
                If chk IsNot Nothing Then
                    chk.Checked = check
                    If controlname = "chkCheck" Then
                        chk = Me.Step4_GridSummary.HeaderRow.Controls(10).FindControl("chkCheckHeader")
                        If chk IsNot Nothing Then
                            chk.Checked = check
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            Me.SetErrorMessage("Step4_CheckAll > " & ex.Message)
        End Try



    End Sub

    Sub Step4_DisplayPOC(ByVal check As Boolean)
        Dim col As DataControlField = Nothing
        Dim i As Integer

        Try
            For i = 0 To Me.Step4_GridSummary.Columns.Count - 1
                col = Me.Step4_GridSummary.Columns(i)
                If col.HeaderText.Contains("Part") Then
                    col.Visible = check
                    Step4_CheckAll("chkPOC", True)
                    Step4_CheckAll("chkCheck", False)
                End If
            Next

            ' Fix for page not remembering status
            For Each rwVehicleRow As GridViewRow In Step4_GridSummary.Rows
                Dim sCurrentRowSlvId As String = Step4_GridSummary.DataKeys(rwVehicleRow.DataItemIndex).Value.ToString()
                If ListOfProblemVehicles.Contains(sCurrentRowSlvId) Then
                    If ListOfProblemVehicles(sCurrentRowSlvId) = ProblemsWithSelectedVehicle.SelectedVehicleIsKBOnly Then
                        DirectCast(rwVehicleRow.Cells(9), System.Web.UI.WebControls.DataControlFieldCell).Text = "KB"
                        DirectCast(rwVehicleRow.FindControl("chkCheck"), System.Web.UI.WebControls.CheckBox).Enabled = False
                    End If
                End If
            Next
            ' Fix for page not remembering status

        Catch ex As Exception
            Me.SetErrorMessage("Step4_DisplayPOC > " & ex.Message)
        End Try


    End Sub

    Sub Step4_ClickCheck()
        If Me.gRecExist = False Then Exit Sub
        Dim curtotal As Decimal = 0
        Dim curStr As String = String.Empty
        Dim curr As String = String.Empty
        Dim cost As String = String.Empty
        Dim code As String = String.Empty
        Dim value As String = String.Empty

        Try


            Dim ctrChildnodes As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            Dim ctrXmlcurnodes As Integer = xmlCur.DocumentElement.ChildNodes.Count - 1

            Dim chk As String = String.Empty
            For i As Integer = 0 To ctrChildnodes
                chk = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText
                If chk.Equals("-1") Then
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
                ElseIf chk.Equals("1") Then
                    For ii As Integer = 0 To ctrXmlcurnodes
                        curr = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText
                        code = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("code").InnerText
                        cost = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Cost").InnerText
                        value = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("value").InnerText

                        If curr.Equals(code) AndAlso Not String.IsNullOrEmpty(cost) Then
                            curtotal = Decimal.Parse((value) + Decimal.Parse(cost))
                            Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("value").InnerText = curtotal

                        End If
                    Next
                End If
            Next

            ctrXmlcurnodes = xmlCur.DocumentElement.ChildNodes.Count - 1
            For i As Integer = 0 To ctrXmlcurnodes
                value = FormatCurrency(CDec(Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("value").InnerText))
                code = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("code").InnerText
                If Not value.Equals("0") Or Not value.Equals(0) Then
                    curStr = curStr & value & "&nbsp;" & code
                End If
                Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("value").InnerText = "0"
            Next
            If curStr.Length > 0 Then
                CType(Me.Step4_GridSummary.FooterRow.FindControl("lblfootercost"), Label).Text = curStr
            Else
                CType(Me.Step4_GridSummary.FooterRow.FindControl("lblfootercost"), Label).Text = ""
            End If

            ''rev:mia sept21
            Hidchosenvehicle.Text = Server.HtmlEncode(xmlChosenVehicle.OuterXml)

            ''-----------------------------------------
            ''rev:mia jan 16 session state to viewstate
            ''-----------------------------------------
            _SESSION_selectedVehicleList_xmlChosenVehicle = xmlChosenVehicle.OuterXml
            ''-----------------------------------------

            ' Fix for page not remembering status
            'Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString()
            For Each rwVehicleRow As GridViewRow In Step4_GridSummary.Rows
                Dim sCurrentRowSlvId As String = Step4_GridSummary.DataKeys(rwVehicleRow.DataItemIndex).Value.ToString()
                If ListOfProblemVehicles.Contains(sCurrentRowSlvId) Then
                    If ListOfProblemVehicles(sCurrentRowSlvId) = ProblemsWithSelectedVehicle.SelectedVehicleIsKBOnly Then
                        DirectCast(rwVehicleRow.Cells(9), System.Web.UI.WebControls.DataControlFieldCell).Text = "KB"
                        DirectCast(rwVehicleRow.FindControl("chkCheck"), System.Web.UI.WebControls.CheckBox).Enabled = False
                    End If
                End If

                Dim ddlRS As DropDownList = CType(rwVehicleRow.FindControl("ddlRS"), DropDownList)
                If ddlRS IsNot Nothing Then
                    ''rev:mia june 2 2011 Add default line to the DDLRES in the SUMMARYPAGE
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & sCurrentRowSlvId & "']/ReqSrc").InnerText = ddlRS.SelectedItem.Value
                End If

            Next
            ''-----------------------------------------
            ''rev:mia June 02 2011 session state to viewstate
            ''-----------------------------------------
            _SESSION_selectedVehicleList_xmlChosenVehicle = xmlChosenVehicle.OuterXml
            LogDebug("Step4_ClickCheck() ChosenVehicleXML: " & xmlChosenVehicle.OuterXml)
            ''-----------------------------------------
            ' Fix for page not remembering status
            ''rev:mia July 27 2012 - Fixed for all buttons that are being enabled even there is a blocking rul
            Try
                If CType(ViewState("flagforBlockingRule"), Boolean) = True Then
                    LogDebug("Disable other buttons if there is blocking rule")
                    Me.BtnInquiry.Enabled = False
                    Me.BtnQuote.Enabled = False
                    Me.BtnKnockBack.Enabled = True
                    Me.BtnWaitlist.Enabled = False
                    Me.BtnProvisional.Enabled = False
                    Me.BtnConfirm.Enabled = False
                Else
                    Me.BtnInquiry.Enabled = True
                    Me.BtnQuote.Enabled = True
                    Me.BtnKnockBack.Enabled = True
                    Me.BtnWaitlist.Enabled = True
                    Me.BtnProvisional.Enabled = True
                    Me.BtnConfirm.Enabled = True
                    SubmitSummary.Enabled = True
                End If
            Catch ex As Exception
                LogDebug("Step4_ClickCheck: " & ex.Message & ", " & ex.StackTrace & ", " & ex.Source)
            End Try
            
        Catch ex As Exception
            Me.SetErrorMessage("Step4_ClickCheck > " & ex.Message)
        End Try

    End Sub

    Sub Step4_SaveAndNext(ByVal modecase As String)

        Using otransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim sCitycode As String = IIf(String.IsNullOrEmpty(Me.CityCode), "", Me.CityCode)
                Dim parameter As String = "0"
                If modecase.Equals("SAVENEXT") Then
                    parameter = "1"
                End If
                Dim sb As New StringBuilder
                With sb
                    .Append("<Root>")
                    .Append(Me.xmlRequest.SelectSingleNode("/Data/Header").OuterXml)
                    .Append(Me.xmlChosenVehicle.OuterXml)
                    .AppendFormat("<NewBooNum>{0}</NewBooNum>", Me.txtNewBookingNumber.Text)
                    .AppendFormat("<BooId>{0}</BooId>", Me.BooID)
                    If CType(ViewState("flagforBlockingRule"), Boolean) = True Then
                        .AppendFormat("<KBType>{0}</KBType>", CType(ViewState("POS"), String))
                    Else
                        .AppendFormat("<KBType>{0}</KBType>", Me.ddlKnockBack.SelectedItem.Value)
                    End If
                    .AppendFormat("<UserCode>{0}</UserCode>", UserCode)
                    .AppendFormat("<PrgName>{0}</PrgName>", Me.PrgmName)
                    ''reV:mia may 18 2011 email address
                    .AppendFormat("<EmailAddress>{0}</EmailAddress>", Me.EmailAddress)
                    .Append("</Root>")
                End With


                Dim xmlstring As String = Aurora.ChkInOutRntCan.Services.BookingRentalMod.ManageSummaryOfChoosenVehicle(sb.ToString, sCitycode, parameter)
                LogDebug("Step4_SaveAndNext(ManageSummaryOfChoosenVehicle) > parameter: " & sb.ToString & vbCrLf & vbCrLf)
                LogDebug("Step4_SaveAndNext(ManageSummaryOfChoosenVehicle) > returned: " & xmlstring)

                Dim xmldoc As New XmlDocument
                xmldoc.LoadXml(xmlstring)
                Dim msg As String = xmldoc.SelectSingleNode("//Root/Error/ErrDescription").InnerText
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, msg)
                msg = xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText

                If msg.Equals("False") Then
                    If modecase.Equals("SAVENEXT") Then
                        Dim booid As String = xmldoc.SelectSingleNode("//Root/Id/BooId").InnerText
                        Dim rentid As String = xmldoc.SelectSingleNode("//Root/Id/RntId").InnerText
                        If Not String.IsNullOrEmpty(booid) Then
                            ''poc
                            Me.HidWarningMessage.Value = ""
                            If Not Me.Step4_GridSummary.Enabled Then
                                ''add note in the table
                                Dim pNteBooId As String = ""
                                Dim pNteBooNo As String = xmldoc.SelectSingleNode("Root/Data/SelectedVehicles/SelectedVehicle/RentalId").InnerText.Split("/")(0)
                                Dim pNteRntNo As Integer = xmldoc.SelectSingleNode("Root/Data/SelectedVehicles/SelectedVehicle/RentalId").InnerText.Split("/")(1)
                                Dim pNteCodTypId As String = "471AEF57-626D-434A-ACA8-9E5E9112AC3F"
                                Dim pNteDesc As String = "Blocking Rule: Request Declined.No Availability, check with the scheduling"
                                Dim pNteCodAudTypId As String = "FB4207C6-752C-4A04-BCEA-159E3B8E4BB1"
                                Dim pNtePriority As Integer = 3
                                Dim pNteIsActive As Boolean = True
                                Dim pIntegrityNo As Object = Nothing
                                Dim pUserName As String = Me.UserCode
                                Dim pAddPrgmName As String = "BookingProcess"

                                Dim returnMessage As String = BookingNotes.ManageNote(pNteBooId, _
                                                                                       pNteBooNo, _
                                                                                       pNteRntNo, _
                                                                                       pNteCodTypId, _
                                                                                       pNteDesc, _
                                                                                       pNteCodAudTypId, _
                                                                                       pNtePriority, _
                                                                                        pNteIsActive, _
                                                                                       pIntegrityNo, _
                                                                                       pUserName, _
                                                                                       pAddPrgmName)
                            End If

                            ''rev:mia 19nov2014 start - part of  slot dropdown addition
                            If modecase.Equals("SAVENEXT") And Not String.IsNullOrEmpty(SlotAvpID) And Not String.IsNullOrEmpty(SlotId) And Not String.IsNullOrEmpty(SlotDescription) Then

                                Dim rentalIdNodes As XmlNodeList = xmldoc.SelectNodes("//Root/Data/SelectedVehicles/SelectedVehicle/RentalId")
                                Try
                                    For Each rentalIdnode As XmlNode In rentalIdNodes
                                        Dim rentalnodeid As String = rentalIdnode.InnerText.Replace("/", "-")
                                        If (Not String.IsNullOrEmpty(rentalnodeid)) Then
                                            Logging.LogDebug("BookingProcess Step4_SaveAndNext(SaveRentalSlot) will EXECUTE.Parameters have data.", "RentalId: " & rentalnodeid & " SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
                                            Dim result As Boolean = Aurora.Booking.Services.Rental.SaveRentalSlot(rentalnodeid, SlotId, SlotDescription)
                                        End If
                                    Next
                                Catch ex As Exception
                                    Logging.LogError("BookingProcess Step4_SaveAndNext(SaveRentalSlot) ERROR:", ex.Message & "," & ex.StackTrace)
                                End Try
                                
                            Else
                                Logging.LogDebug("BookingProcess Step4_SaveAndNext(SaveRentalSlot) will not not be executed. Parameters are all empty", "SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
                            End If
                            ''ClearSlotDataForNonKKandNN()
                            ''rev:mia 19nov2014 end - part of  slot dropdown addition


                            otransaction.CommitTransaction()

                            Dim qstring As String = QS_BOOKING & "&hdBookingId=" & booid
                            qstring = qstring & "&hdRentalId=" & rentid
                            qstring = qstring & "&activeTab=9"


                            ''rev:mia dec11 
                            PostBackValue = Nothing
                            Response.Redirect(qstring)

                        Else
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Booking Not-Yet Created")
                            Throw New Exception("Booking Not-Yet Created")
                        End If
                    Else
                        otransaction.CommitTransaction()
                        Me.xmlRequest.LoadXml(xmldoc.SelectSingleNode("//Root/Data").OuterXml)
                        Me.SplitData(BookingProcess.Summary)
                    End If
                End If

            Catch exThread As System.Threading.ThreadAbortException
                otransaction.CommitTransaction()

            Catch ex As Exception
                otransaction.RollbackTransaction()
                Me.SetErrorMessage("Step4_SaveAndNext(" & modecase & ") > " & ex.Message)
                LogError("Step4_SaveAndNext(" & modecase & ") > " & ex.Message & vbCrLf & ex.StackTrace)
            End Try
        End Using
    End Sub

    Sub Step4_Summary()
        Try
            Me.Step4_LoadSummaryScreen()
            If Not String.IsNullOrEmpty(Me.Msg) Then
                Me.confirm_Inquiry.ConfirmText = Me.Msg
                Me.Confirm_Knockback.ConfirmText = Me.Msg
                Me.Confirm_Qoute.ConfirmText = Me.Msg
                Me.Confirm_Waitlisted.ConfirmText = Me.Msg
                Me.confirm_Inquiry.Enabled = True
                Me.Confirm_Knockback.Enabled = True
                Me.Confirm_Qoute.Enabled = True
                Me.Confirm_Waitlisted.Enabled = True
            Else
                Me.confirm_Inquiry.Enabled = False
                Me.Confirm_Knockback.Enabled = False
                Me.Confirm_Qoute.Enabled = False
                Me.Confirm_Waitlisted.Enabled = False
            End If


        Catch ex As Exception
            Me.SetErrorMessage("Step4_Summary > " & ex.Message)
        End Try

    End Sub

    Protected Sub Step4_chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)

        Try
            Step4_CheckAll("chkCheck", chk.Checked)

            For i As Integer = 0 To Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
                If chk.Checked = True Then
                    Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
                Else
                    Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
                End If
            Next
            Step4_ClickCheck()
        Catch ex As Exception
            Me.SetErrorMessage("Step4_chkAll_CheckedChanged > " & ex.Message)
        End Try

    End Sub

    Protected Sub Step4_GridSummary_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step4_GridSummary.RowCreated
        Dim index As Integer
        Dim col As DataControlField = Nothing
        Dim chkAll As CheckBox = Nothing

        Dim chk As CheckBox = Nothing

        Dim litControl As New Literal

        If e.Row.RowType = DataControlRowType.Header Then
            For index = 0 To Step4_GridSummary.Columns.Count - 1
                col = Step4_GridSummary.Columns(index)
                If (col.HeaderText = "Check") Or col.HeaderText = "" Then
                    col.HeaderText = "Check"
                    chkAll = New CheckBox
                    chkAll.ID = "chkCheckHeader"

                    chkAll.Visible = True
                    chkAll.AutoPostBack = "true"
                    litControl.Text = ("Check")
                    litControl.Visible = True
                    AddHandler chkAll.CheckedChanged, AddressOf Me.Step4_chkAll_CheckedChanged
                    e.Row.Cells(index).Controls.Add(litControl)
                    e.Row.Cells(index).Controls.Add(chkAll)
                Else
                    If Step4_BYpass <> "" Then
                        If col.HeaderText = "Bypass" Then
                            If col.Visible = False Then
                                col.Visible = True
                            End If
                        End If
                    Else
                        If col.HeaderText = "Bypass" Then
                            If col.Visible = True Then
                                col.Visible = False
                            End If
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub Step4_GridSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Step4_GridSummary.RowDataBound
        Dim ddlRS As DropDownList = Nothing
        If e.Row.RowType = DataControlRowType.DataRow Then

            ddlRS = CType(e.Row.FindControl("ddlRS"), DropDownList)
            ''rev:mia june 2 2011 Add default line to the DDLRES in the SUMMARYPAGE
            If Not ddlRS Is Nothing Then
                ddlRS.AppendDataBoundItems = True
                ddlRS.Items.Add(New ListItem("", ""))
                ddlRS.DataSource = Step4_XMLRequestSourceDataset()
                ddlRS.DataTextField = "CODE"
                ddlRS.DataValueField = "ID"
                ddlRS.DataBind()
                ddlRS.SelectedValue = DataBinder.Eval(e.Row.DataItem, "ReqSrc")
            End If



            ' Fix for page not remembering status
            Try
                Dim sCurrentRowSlvId As String = Step4_GridSummary.DataKeys(e.Row.DataItemIndex).Value.ToString
                Dim sCurrentRowAvailStatus As String = DirectCast(e.Row.Cells(5).Controls(0), System.Web.UI.DataBoundLiteralControl).Text.Trim().ToUpper()

                If sCurrentRowAvailStatus = "NO" Then
                    ' add to problems list
                    AddToListOfProblemVehicles(sCurrentRowSlvId, ProblemsWithSelectedVehicle.SelectedVehicleHasNoAvailability)
                End If

                ''rev:mia 19nov2014 start - part of  slot dropdown addition
                DirectCast(e.Row.FindControl("chkCheck"), System.Web.UI.WebControls.CheckBox).ToolTip = DataBinder.Eval(e.Row.DataItem, "AvpId")
                ''rev:mia 19nov2014 end - part of  slot dropdown addition

                If ListOfProblemVehicles.Contains(sCurrentRowSlvId) Then
                    DirectCast(Step4_GridSummary.HeaderRow.Cells(10).Controls(1), System.Web.UI.WebControls.CheckBox).Enabled = False

                    ' this vehicle has a problem
                    If ListOfProblemVehicles(sCurrentRowSlvId) = ProblemsWithSelectedVehicle.SelectedVehicleIsKBOnly Then
                        ' kb only row
                        DirectCast(e.Row.Cells(9), System.Web.UI.WebControls.DataControlFieldCell).Text = "KB"
                        DirectCast(e.Row.FindControl("chkCheck"), System.Web.UI.WebControls.CheckBox).Enabled = False

                        If xmlChosenVehicle IsNot Nothing Then
                            xmlChosenVehicle.DocumentElement.SelectSingleNode("SelectedVehicle[SlvId='" & sCurrentRowSlvId & "']/Status").InnerText = "KB"
                            _SESSION_selectedVehicleList_xmlChosenVehicle = xmlChosenVehicle.OuterXml
                            Dim item As ListItem = ddlKnockBack.Items.FindByText("Blocking Rule")
                            If item IsNot Nothing Then
                                ddlKnockBack.SelectedItem.Text = item.Text
                                ddlKnockBack.SelectedItem.Value = item.Value
                            End If
                            Me.ddlKnockBack.Enabled = False
                        End If


                    ElseIf ListOfProblemVehicles(sCurrentRowSlvId) = ProblemsWithSelectedVehicle.SelectedVehicleHasNoAvailability Then
                        ' no availability row
                    End If
                End If

            Catch ex As Exception

            End Try
            ' Fix for page not remembering status
        End If
    End Sub

    Function Step4_XMLRequestSourceDataset() As DataSet

        xmlRequestSource = New XmlDocument
        xmlRequestSource = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "11", "", "")

        Dim xmlstring As String = xmlRequestSource.OuterXml.ToString
        xmlstring = RemoveUnwantedChar(xmlstring)
        Dim ds As DataSet
        ds = New DataSet
        ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
        Return ds

    End Function

    Protected Sub chkconvoy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkconvoy.CheckedChanged
        Step4_DisplayPOC(chkconvoy.Checked)
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            Dim checkbox As CheckBox = CType(sender, CheckBox)
            Dim row As GridViewRow = CType(checkbox.NamingContainer, GridViewRow)
            Me.SLVid = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString
            Dim item As String = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString

            If Trace.IsEnabled And Me.UserCode = "ma2" Then
                Trace.Warn("CheckBox1_CheckedChanged :" & Me.SLVid)
            End If
            DisplayTestData("CheckBox1_CheckedChanged : Me.SLVid ", Me.SLVid)
            If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString Then

                If checkbox.Checked = True Then
                    If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString Then
                        Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "1"

                    End If
                Else
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "0"

                    CType(Step4_GridSummary.FooterRow.FindControl("lblfootercost"), Label).Text = ""
                End If

                Dim ddlRS As DropDownList = CType(row.FindControl("ddlRS"), DropDownList)
                If ddlRS IsNot Nothing Then
                    ''rev:mia june 2 2011 Add default line to the DDLRES in the SUMMARYPAGE
                    ''Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle/ReqSrc").InnerText = ddlRS.SelectedItem.Value
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/ReqSrc").InnerText = ddlRS.SelectedItem.Value
                End If

                Step4_ClickCheck()
            End If

        Catch ex As Exception
            MyBase.SetErrorShortMessage("CheckBox1_CheckedChanged:" & ex.Message)
            DisplayTestData("<i><u>CheckBox1_CheckedChanged : Me.SLVid  </i></u>", Me.SLVid & " Error: " & ex.Message & " ---> " & Server.HtmlEncode(Me.xmlChosenVehicle.OuterXml))
        End Try
    End Sub

    Protected Sub BtnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConfirm.Click
        Me.Step4_SubmitData("KK")
    End Sub

    Protected Sub BtnInquiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInquiry.Click
        Me.Step4_SubmitData("IN")
    End Sub

    Protected Sub BtnKnockBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnKnockBack.Click
        Me.Step4_SubmitData("KB")
    End Sub

    Protected Sub BtnWaitlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnWaitlist.Click
        Me.Step4_SubmitData("WL")
    End Sub

    Protected Sub BtnQuote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuote.Click
        Me.Step4_SubmitData("QN")
    End Sub

    Protected Sub BtnProvisional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnProvisional.Click
        Me.Step4_SubmitData("NN")
    End Sub

    Protected Sub Step4_BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Step4_SubmitData("SAVE")
    End Sub

    Protected Sub Step4_BtnSaveNextSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Step4_SubmitData("SAVENEXT")
    End Sub

#End Region

#Region " Private Properties"

#Region " Step One"

    Private _promocode As String
    Public Property PromoCode As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("promocode")), "", CType(ViewState("promocode"), String))
        End Get
        Set(value As String)
            ViewState("promocode") = value
        End Set
    End Property

    Private _references As String
    Private Property Reference() As String
        Get
            Return IIf(String.IsNullOrEmpty(_references), Me.txtRef.Text, _references)
        End Get
        Set(ByVal value As String)
            _references = value
        End Set
    End Property


    Private Property Brand() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("Brand")), "", CType(ViewState("Brand"), String))
        End Get
        Set(ByVal value As String)
            ViewState("Brand") = value
        End Set
    End Property


    Private Property [Class]() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("Class")), "", CType(ViewState("Class"), String))
        End Get
        Set(ByVal value As String)

            ViewState("Class") = value
        End Set
    End Property


    Private Property TypeID() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("TypeID")), "", CType(ViewState("TypeID"), String))
        End Get
        Set(ByVal value As String)
            ViewState("TypeID") = value
        End Set
    End Property


    Private Property BKRIntegrityNo() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("BKRIntegrityNo")), "", CType(ViewState("BKRIntegrityNo"), String))
        End Get
        Set(ByVal value As String)

            ViewState("BKRIntegrityNo") = value
        End Set
    End Property


    Private Property MiscAgnName() As String
        Get

            Return IIf(String.IsNullOrEmpty(ViewState("MiscAgnName")), "", CType(ViewState("MiscAgnName"), String))
        End Get
        Set(ByVal value As String)

            ViewState("MiscAgnName") = value
        End Set
    End Property


    Private Property MiscAgentContact() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("MiscAgentContact")), "", CType(ViewState("MiscAgentContact"), String))
        End Get
        Set(ByVal value As String)
            ViewState("MiscAgentContact") = value
        End Set
    End Property

    Private ReadOnly Property NumVehicle() As String
        Get
            Return Me.ddlNoOfVehicles.Text
        End Get

    End Property

    Private ReadOnly Property NumberOfAdults() As String
        Get
            Return Me.ddlAdultNO.Text
        End Get
    End Property

    Private ReadOnly Property NumberOfChildren() As String
        Get
            Return Me.ddlChildNO.Text
        End Get
    End Property

    Private ReadOnly Property NumberOfInfants() As String
        Get
            Return Me.ddlInfantsNO.Text
        End Get
    End Property

    Private ReadOnly Property CKOLocation() As String
        Get
            Return Splitter(Me.PickerControlForCheckOut.Text.ToUpper)
        End Get
    End Property

    Private ReadOnly Property CKODate() As String
        Get
            Dim tempTime As String = "08:00 AM"

            If Not String.IsNullOrEmpty(Me.HidCKOampm.Value) Then
                If Not Me.ddlAMPMcheckout.Text.Contains(Me.HidCKOampm.Value) Then
                    tempTime = Me.ddlAMPMcheckout.Text
                Else
                    If Not Me.HidCKOampm.Value.Equals("AM") Then
                        If Not Me.HidCKOampm.Value.Equals("PM") Then
                            tempTime = Me.HidCKOampm.Value
                        Else
                            tempTime = "PM"
                        End If
                    Else
                        tempTime = "AM"
                    End If
                End If
            Else
                ''rev:mia July 2 2013 - back to the office.
                If (String.IsNullOrEmpty(Me.HidCKOampm.Value) And Request.QueryString("outT") <> Nothing) Then
                    If (Me.ddlAMPMcheckout.Text <> "" And Me.ddlAMPMcheckout.Text <> "AM" And Me.ddlAMPMcheckout.Text <> "PM") Then
                        tempTime = HttpUtility.HtmlEncode(Request.QueryString("outT"))
                    End If
                End If
            End If
            Return DateTimeToString(Me.DateControlForCheckOut.Date, SystemCulture).Split(" "c)(0) & " " & IIf(Me.ddlAMPMcheckout.Text.Equals("AM") = False AndAlso Me.ddlAMPMcheckout.Text.Equals("PM") = False, tempTime, "")
        End Get
    End Property

    Private ReadOnly Property CKODayPart() As String
        Get

            If (Me.HidCKOampm.Value = "12:00 PM") Then
                Return "PM"

            Else
                Return GetAMPM(Me.ddlAMPMcheckout.Text)
            End If

        End Get
    End Property


    Private ReadOnly Property CKILocation() As String
        Get
            Return Splitter(Me.PickerControlForCheckIn.Text.ToUpper)
        End Get
    End Property

    Private ReadOnly Property CKIDate() As String
        Get

            Dim tempTime As String = "15:00"

            If Not String.IsNullOrEmpty(Me.HidCKIampm.Value) Then
                If Not Me.ddlAMPMcheckin.Text.Contains(Me.HidCKIampm.Value) Then
                    tempTime = Me.ddlAMPMcheckin.Text
                Else
                    If Not Me.HidCKIampm.Value.Equals("AM") Then
                        If Not Me.HidCKIampm.Value.Equals("PM") Then
                            tempTime = Me.HidCKIampm.Value
                        Else
                            tempTime = "PM"
                        End If
                    Else
                        tempTime = "AM"
                    End If
                End If
            Else
                ''rev:mia July 2 2013 - back to the office.
                If (String.IsNullOrEmpty(Me.HidCKIampm.Value) And Request.QueryString("inT") <> Nothing) Then
                    If (Me.ddlAMPMcheckin.Text <> "" And Me.ddlAMPMcheckin.Text <> "AM" And Me.ddlAMPMcheckin.Text <> "PM") Then
                        tempTime = HttpUtility.HtmlEncode(Request.QueryString("inT"))
                    End If
                End If
            End If
            Return DateTimeToString(Me.DateControlForCheckIn.Date, SystemCulture).Split(" "c)(0) & " " & IIf(Me.ddlAMPMcheckin.Text.Equals("AM") = False AndAlso Me.ddlAMPMcheckin.Text.Equals("PM") = False, tempTime, "")
        End Get
    End Property

    Private ReadOnly Property CKIDayPart() As String
        Get
            If (Me.HidCKIampm.Value = "12:00 PM") Then
                Return "PM"
            Else
                Return GetAMPM(Me.ddlAMPMcheckin.Text)
            End If

        End Get
    End Property

    Private ReadOnly Property LastName() As String
        Get
            Return Me.txtSurname.Text
        End Get
    End Property

    Private ReadOnly Property FirstName() As String
        Get
            Return Me.txtfirstName.Text
        End Get
    End Property

    Private ReadOnly Property Titles() As String
        Get
            '' Return Me.ddltitle.Text
            Return Me.customertitleDropdown.SelectedItem.Text
        End Get
    End Property

    Private ReadOnly Property RequestSource() As String
        Get
            If Me.ddlRequestSource.SelectedValue = "0" AndAlso Me.ddlRequestSource.SelectedItem.Text.Trim = "" Then
                Return ""
            Else
                Return Me.ddlRequestSource.SelectedValue
            End If
        End Get
    End Property

    Private ReadOnly Property HirePeriod() As String
        Get
            Return Me.txtHirePeriod.Text
        End Get
    End Property

    Private Property HireOUM() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("HireOUM")), "1", CType(ViewState("HireOUM"), String))
        End Get
        Set(ByVal value As String)
            ViewState("HireOUM") = value
        End Set
    End Property

    Private ReadOnly Property GetGriditemKey() As String
        Get
            Return HidGridViewItem.Value
        End Get
    End Property


    Private ReadOnly Property GetFullURLreferrer() As String
        Get
            Dim temp As String = ""
            Try
                temp = Request.UrlReferrer.Query
                If temp.Contains("isFromSearch") Then
                    temp = temp.Replace("&isFromSearch=0", "")
                    temp = temp.Replace("?", "")
                Else
                    If (temp.StartsWith("?") = True) Then
                        temp = temp.Remove(0, 1)
                    End If
                End If
                Return temp
            Catch ex As Exception
                Return temp
            End Try
        End Get
    End Property

    'rev:mia July 2 2012 - addition of 3rd party reference
    '                            - first day of work after two weeks holiday
    Private ReadOnly Property ThirdParyReference As String
        Get
            Return Me.txtthirdParty.Text.Trim
        End Get
    End Property

#End Region

#End Region

#Region " Public Properties"

    Private Property CacheContactID() As String
        Get
            Return IIf(String.IsNullOrEmpty(ViewState("CacheContactID")), "", CType(ViewState("CacheContactID"), String))
        End Get
        Set(ByVal value As String)
            ViewState("CacheContactID") = value
        End Set
    End Property

    Private Function GetProductID(ByVal productcode As String, ByVal paramuserid As String, Optional ByVal productGUID As String = "") As String

        Dim splitProductCode As String()
        If productcode.Contains("-") Then
            splitProductCode = productcode.Split("-")
            productcode = splitProductCode(0)
        Else
            'Return ""
        End If
        Dim tempReturn As String = Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", _
                                                  productcode, _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  paramuserid)

        Dim xmltemp As New XmlDocument
        Try
            If String.IsNullOrEmpty(tempReturn) Then Return ""
            xmltemp.LoadXml(tempReturn)
            Return xmltemp.SelectSingleNode("PRODUCT/Id").InnerText

        Catch ex As Exception
            xmltemp.LoadXml("<Products>" & tempReturn & "</Products>")
            Dim nodes As XmlNodeList = xmltemp.SelectNodes("Products/PRODUCT/Id")
            For Each mynode As XmlNode In nodes
                If mynode.Name = "Id" Then
                    If Not String.IsNullOrEmpty(mynode.InnerText) Then
                        If mynode.InnerText = productGUID Then
                            Return mynode.InnerText
                        End If
                    End If
                End If
            Next
        End Try
    End Function
    Protected ReadOnly Property GetPackageXMLinHiddenValues() As String
        Get
            Dim strpackage As String = Me.hidCKOandCKIinfo.Value
            If Me.PickerControlForAgent.DataId <> "" AndAlso Not String.IsNullOrEmpty(strpackage) Then

                Dim intfirstindex As Integer = strpackage.IndexOf(",")
                Dim agentid As String
                If intfirstindex <> -1 Then
                    ''extract agentID for any changes
                    agentid = strpackage.Substring(0, intfirstindex)
                    If Not agentid.Equals(Me.PickerControlForAgent.DataId) Then
                        strpackage = strpackage.Replace(agentid, Me.PickerControlForAgent.DataId.TrimEnd)
                    End If
                End If

                ''extract productID for any changes
                Dim productid As String = strpackage.Split(",")(1)
                If String.IsNullOrEmpty(productid) AndAlso Not productid.Equals(Me.hidPackageID.Value.TrimEnd) Then
                    strpackage = strpackage.Replace(productid, Me.hidPackageID.Value.TrimEnd)
                Else
                    If productid.Contains(",") Then
                        productid = productid.Substring(0, productid.IndexOf(",")).TrimEnd
                    End If
                    If productid <> "" Then
                        Dim tempProduct As String = GetProductID(Me.PickerControlForProduct.Text, MyBase.UserCode)
                        If String.IsNullOrEmpty(tempProduct) Then
                            MyBase.SetErrorShortMessage("Please check your product")
                        Else
                            strpackage = strpackage.Replace(productid, tempProduct)
                        End If

                    End If
                End If


            End If

            Dim intlastindex As Integer = strpackage.LastIndexOf(",")
            Dim ckiDate As String = ""

            If Me.DateControlForCheckIn.Text <> "" Then
                If Me.ddlAMPMcheckin.Text.Equals("AM") Then
                    ckiDate = DateTimeToString(Me.DateControlForCheckIn.Date, SystemCulture).Split(" "c)(0) & " " & "07:00:00"

                Else
                    If Me.ddlAMPMcheckin.Text.Equals("PM") Then
                        ckiDate = DateTimeToString(Me.DateControlForCheckIn.Date, SystemCulture).Split(" "c)(0) & " " & "07:00:00"
                    Else
                        ckiDate = DateTimeToString(Me.DateControlForCheckIn.Date, SystemCulture).Split(" "c)(0) & " " & Me.ddlAMPMcheckin.Text
                    End If
                End If
            End If

            If ckiDate <> "" AndAlso strpackage <> "" AndAlso intlastindex <> -1 Then
                strpackage = strpackage.Substring(0, intlastindex)
                strpackage = strpackage & "," & ckiDate
            End If
            Return strpackage
        End Get
    End Property

    Protected ReadOnly Property GetPackageNewValues() As String
        Get
            Return Me.HidTemp.Value
        End Get
    End Property


    Public ReadOnly Property CurrentUserCode() As String
        Get
            Return MyBase.UserCode
        End Get
    End Property

    Public ReadOnly Property AgentParameter() As String
        Get
            Return Me.PickerControlForAgent.Text
        End Get
    End Property


    Protected ReadOnly Property ParameterThree() As String
        Get
            Return Step1_GetPackageXML()
        End Get
    End Property



#End Region

#Region " Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        ''-----------------------------------------
        ''rev:mia jan 16 session state to viewstate
        ''-----------------------------------------
        ''SESSION_VehicleRequestMgt = "RS-VEHREQMGT" & MyBase.UserCode
        ''-----------------------------------------

        sc = ScriptManager.GetCurrent(Me)


        Try

            Call Me.Step1_AddJSAttributes()
            If Not Page.IsPostBack Then

                ''rev:mia Sept.2 2013 - Customer Title from query
                PopulateTitle()

                Call Me.Step1_NavigationTab()

                pageValid = True
                If (Not String.IsNullOrEmpty(Request.QueryString("vhrid"))) AndAlso (Me.BackUrl.Contains("Package.aspx") Or Not String.IsNullOrEmpty(Request.QueryString("aspBack"))) Then
                    Call Me.Step1_FillControls()
                    Me.VhrID = Request.QueryString("vhrId").ToString
                    Me.wizBookingProcess.ActiveStepIndex = 2
                Else
                    Me.wizBookingProcess.ActiveStepIndex = 0
                    LoadPage(BookingProcess.NewBooking)
                End If
            Else
                If Me.wizBookingProcess.ActiveStepIndex = 0 Then
                    ''-----------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''-----------------------------------------
                    ''Call Step1_SplitDataOnPostBack()
                    Call Step1_SplitDataOnPostBackR2()
                    ''-----------------------------------------
                    Me.PickerControlForPackage.Param3 = ParameterThree
                    Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
                    Me.PickerControlForPackage.Param1 = Splitter(AgentParameter.Trim)
                    Me.PickerControlForPackage.Param2 = Splitter(Me.PickerControlForPackage.Text.Trim)
                    Step1_SetControlFocus(sc.AsyncPostBackSourceElementID, sc)
                ElseIf Me.wizBookingProcess.ActiveStepIndex = 1 Then
                    If unset = False Then
                        Me.Hidbooid.Value = Me.lblbooid.Text
                    End If
                    ''-----------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''-----------------------------------------
                    ''Me.GetFromSession(BookingProcess.CheckDuplicate)
                    Me.GetFromSessionR2(BookingProcess.CheckDuplicate)
                    ''-----------------------------------------
                ElseIf Me.wizBookingProcess.ActiveStepIndex = 2 Then
                    Me.Step3_AddJSAttributes()
                ElseIf Me.wizBookingProcess.ActiveStepIndex = 3 Then
                    ''-----------------------------------------
                    ''rev:mia jan 16 session state to viewstate
                    ''-----------------------------------------
                    ''Me.GetFromSession(BookingProcess.Summary)
                    Me.GetFromSessionR2(BookingProcess.Summary)
                    ''-----------------------------------------
                Else
                End If
            End If

        Catch ex As Exception
            MyBase.SetInformationShortMessage("Error loading the page.")
            Logging.LogException("BookingProcess", ex)
        End Try

    End Sub



#End Region

#Region " Control Events"

    Protected Sub rdlACorAV_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlACorAV.DataBound
        Dim NAitemOntop As ListItem = New ListItem("NA")
        NAitemOntop.Selected = True
        rdlACorAV.Items.Insert(0, NAitemOntop)
    End Sub



    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.style.background='#BEBEBE';this.style.cursor='hand';")
            e.Row.Attributes.Add("onmouseout", "this.style.background='#ffffff';this.style.cursor='hand';")
            e.Row.Attributes.Add("onclick", "document.getElementById('ctl00_ContentPlaceHolder_HidGridViewItem').value =" & e.Row.DataItemIndex.ToString & ";")
        End If
    End Sub

    ''rev:mia june 2 2011 this is not required
    'Protected Sub ddlRequestSource_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRequestSource.DataBound
    '   '' BlankValueInDropdown(sender)
    'End Sub

    Protected Sub ddlUncommonFeature1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature1.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlUncommonFeature2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature2.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlBrand_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub DateControlForCheckIn_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCheckIn.DateChanged

        If Not CheckDateFormatting() Then Exit Sub

        Me.lblDayCKI.Text = ParseDateAsDayWord(DateControlForCheckIn.Text)
        Me.lblDaysMessage.Text = ParseRemainingDay(DateControlForCheckOut.Text)

        Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
        If (sc.AsyncPostBackSourceElementID = "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckIn$dateTextBox") Or _
           (sc.AsyncPostBackSourceElementID = "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox") Then
            If Me.PickerControlForCheckIn.Text <> "" Then
                If Me.HidCKI.Value = "" Then
                    Me.HidCKI.Value = Me.PickerControlForCheckIn.Text
                End If
            End If
            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckIn.Text, Me.DateControlForCheckIn.Text, Me.ddlAMPMcheckin)
        End If

        RefreshContactID()
        Step1_Recalculate()
        ''CheckInValidation()
    End Sub

    Protected Sub DateControlForCheckOut_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCheckOut.DateChanged

        If Not CheckDateFormatting() Then Exit Sub

        Me.lbldayCKO.Text = ParseDateAsDayWord(DateControlForCheckOut.Text)
        Me.lblDaysMessage.Text = ParseRemainingDay(DateControlForCheckOut.Text)

        Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
        If (sc.AsyncPostBackSourceElementID = "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckOut$dateTextBox") Or _
        (sc.AsyncPostBackSourceElementID = "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox") Then
            If Me.PickerControlForCheckOut.Text <> "" Then
                If Me.HidCKO.Value = "" Then
                    Me.HidCKO.Value = Me.PickerControlForCheckOut.Text
                End If

            End If
            ''rev:mia nov18
            ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
            Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckOut.Text, Me.DateControlForCheckOut.Text, Me.ddlAMPMcheckout)
        End If

        RefreshContactID()
        Step1_Recalculate()

    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click

        If Not Me.CheckDateFormatting Then Exit Sub
        Select Case Me.ddlDays.SelectedItem.Text
            Case "Days"
                Step1_CalculateDays(Me.txtHirePeriod.Text, Me.DateControlForCheckOut.Date.ToShortDateString)
                ''rev:mia Dec 10,2009 - Set checkin date to default 15:00
                Dim item As ListItem = Me.ddlAMPMcheckin.Items.FindByText("15:00")
                If Not item Is Nothing Then
                    Me.ddlAMPMcheckin.SelectedValue = "15:00"

                Else
                    If item Is Nothing And Me.ddlAMPMcheckin.Items.Count > 2 Then
                        Me.ddlAMPMcheckin.SelectedIndex = Me.ddlAMPMcheckin.Items.Count - 1
                    End If
                End If

            Case "Weeks"
            Case "Months"
        End Select
        Me.lblDayCKI.Text = ParseDateAsDayWord(DateControlForCheckIn.Text)
        CalculateRequestPeriod(BookingProcess.NewBooking)

        ''---------------------------------------------------
        ''rev:mia july 9 2013 - fixes for these issues
        ''---------------------------------------------------
        '>Create a new booking or Add rental.
        '>Enter location, dates and set the CI time to a non-default time say 14:00.
        '>Now change the hire period and hit the calculate button. You will see the CI time will set to defaults. Which is fine.
        '>Now hit next to the display vehicles page. You will notice that the CI time is no longer the default time but 14:00 which is the time you originally changed to.
        'Basically the problem is on Calculate Front end is displaying default time but carrying thru the last saved time.
        If (Not String.IsNullOrEmpty(Me.HidCKIampm.Value)) Then
            If (Me.HidCKIampm.Value <> Me.ddlAMPMcheckin.SelectedItem.Text) Then
                Me.HidCKIampm.Value = Me.ddlAMPMcheckin.SelectedItem.Text
            End If
        End If

        ''---------------------------------------------------

        ''rev:mia dec9
        Dim _btnReloc As Button = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("btnReloc"), Button)
        Dim _StartNextButton As Button = CType(Me.wizBookingProcess.FindControl("StartNavigationTemplateContainerId").FindControl("StartNextButton"), Button)

        If Me.ddlAMPMcheckout.Items.Count - 1 <= 1 AndAlso Me.ddlAMPMcheckin.Items.Count - 1 <= 1 Then
            _btnReloc.Enabled = False
            _StartNextButton.Enabled = False

        ElseIf Me.ddlAMPMcheckout.Items.Count - 1 <= 1 Then
            _btnReloc.Enabled = False
            _StartNextButton.Enabled = False
        ElseIf Me.ddlAMPMcheckin.Items.Count - 1 <= 1 Then
            _btnReloc.Enabled = False
            _StartNextButton.Enabled = False
        Else
            Me.ClearMessages()
            _btnReloc.Enabled = True
            _StartNextButton.Enabled = True
        End If


    End Sub

    ''REV:MIA - issue # 497 BookingProcess.aspx - Hire Period field not auto-calculating after dates have been entered
    Protected Sub Step1_Recalculate()
        If Not Me.CheckDateFormatting Then Exit Sub
        Me.lblDayCKI.Text = ParseDateAsDayWord(DateControlForCheckIn.Text)
        CalculateRequestPeriod(BookingProcess.NewBooking, False)
    End Sub

    Protected Sub CV_txthireperiod_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(txtHirePeriod.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Protected Sub Step1_DisplayAvailability(Optional ByVal ForRelocation As Boolean = False)
        Try
            If Page.IsValid Then
                Me.Step1_CompareValues()

                If Step1_IsRequiredFieldHaveValues() Then

                    ''Session("ExtensionXML") = Step1_GetXMLString(False, ForRelocation)
                    Dim strResult As String = Aurora.Booking.Services.BookingRequest.ManageBookingRequest(Step1_GetXMLString(False, ForRelocation, ))
                    ExtractNewVhrIDandBKRID(strResult)

                    If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnError) Then
                        If Not IsquickAvail Then
                            MyBase.SetInformationShortMessage(Aurora.Booking.Services.BookingRequest.ReturnError)
                            pageValid = False
                            Exit Sub
                        Else
                            HidWarningMessage.Value = Aurora.Booking.Services.BookingRequest.ReturnWarning
                        End If
                    Else
                        ''rev:mia June 1 2010-
                        Me.ClearMessages()
                        ViewState("flagforBlockingRuleMessage") = ""
                        ViewState("flagforBlockingRule") = False
                        Me.HidWarningMessage.Value = ""
                    End If

                    If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnWarning) Then
                        MyBase.SetInformationShortMessage(Aurora.Booking.Services.BookingRequest.ReturnWarning)
                        'POS enhancement
                        'TRAP any warning message that doesnt contains GEN172

                        If Not Aurora.Booking.Services.BookingRequest.ReturnWarning.Contains(BLOCKING_CODE) Then

                            If Not IsquickAvail Then
                                Me.HidWarningMessage.Value = ""
                            Else
                                HidWarningMessage.Value = Aurora.Booking.Services.BookingRequest.ReturnWarning
                            End If

                            ViewState("flagforBlockingRuleMessage") = "" ''rev:mia nov21
                            pageValid = False
                            Exit Sub
                        Else
                            HidWarningMessage.Value = Aurora.Booking.Services.BookingRequest.ReturnWarning

                            ViewState("flagforBlockingRuleMessage") = HidWarningMessage.Value ''rev:mia nov21
                            ViewState("flagforBlockingRule") = True
                        End If
                    Else
                        ''reV:mia oct.22
                        ViewState("flagforBlockingRule") = False
                        Me.HidWarningMessage.Value = ""

                        ViewState("flagforBlockingRuleMessage") = "" ''rev:mia nov21
                    End If
                    '' Logging.LogInformation("Step1_DisplayAvailability: ReturnWarningsss: ", HidWarningMessage.Value)
                    Call Me.Step1_SetXmlString(strResult)
                    Me.hid_html_butSel.Value = IIf(Not ForRelocation, BUTTON_VALUE, BUTTON_VALUE_RELOC)
                    pageValid = True
                    HidTemp.Value = paramThree

                    If (Request.QueryString("agentId") IsNot Nothing Or Request.QueryString("agentText") IsNot Nothing) Then
                        notToskipLocationUpdate = True
                    End If

                    txtSurname.BackColor = System.Drawing.Color.White

                Else
                    pageValid = False
                End If

            Else

                Dim validator As BaseValidator
                Dim objControl As Object
                For Each validator In Page.GetValidators("Book")
                    If Not validator.IsValid Then
                        validator.FindControl(validator.ControlToValidate).Focus()
                        ''rev:mia may 9 2011 - addition of highlighting of controls
                        objControl = validator.FindControl(validator.ControlToValidate)
                        objControl.backcolor = Aurora.Common.DataConstants.RequiredColor
                        MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)


                        ''Exit For
                    Else
                        ''rev:mia may 9 2011 - addition of highlighting of controls
                        objControl = validator.FindControl(validator.ControlToValidate)
                        objControl.backcolor = System.Drawing.Color.white
                    End If
                Next
                pageValid = Step1_IsRequiredFieldHaveValues()
            End If
            RefreshContactID()


        Catch ex As Exception
            '' Response.Redirect(QS_RS_VEHREQMGTNBR & "?Err=1")
        End Try
    End Sub

    Protected Sub btnNewBooking_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.RemoveSessionForNBR()
        Response.Redirect(QS_RS_VEHREQMGTNBR)
    End Sub

    Protected Sub StartNextButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        IsquickAvail = False
        rfv_Surname.Enabled = True
        Step1_DisplayAvailability()
    End Sub

    Protected Sub Relocation_Click(ByVal sender As Object, ByVal e As EventArgs)
        Step1_DisplayAvailability(True)
    End Sub

    Protected Sub Step1_RedirectCustomer_Click(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(QS_RS_CUSTOMERFIND & "?hdBookingId=" & Me.BooID & "&&hdRentalId=" & Me.RentalID)
    End Sub

    Protected Sub wizBookingProcess_ActiveStepChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wizBookingProcess.ActiveStepChanged
        Dim localIndex As Integer = 1
        Me.ClearShortMessage()
        Select Case Me.wizBookingProcess.ActiveStepIndex
            Case 0
                If Page.IsPostBack Then
                    Me.LoadPage(BookingProcess.NewBooking, True)
                End If
            Case 1
                DisableButtonsInNavigationTemplate(BookingProcess.CheckDuplicate)
                Me.LoadPage(BookingProcess.CheckDuplicate)
                sc.SetFocus(Me.btnBrochure)

            Case 2
                DisableButtonsInNavigationTemplate(BookingProcess.DisplayAvailablity)
                Me.LoadPage(BookingProcess.DisplayAvailablity)
            Case 3
                DisableButtonsInNavigationTemplate(BookingProcess.Summary)
                Me.LoadPage(BookingProcess.Summary)
        End Select

        wizardControl.SelectedIndex = Me.wizBookingProcess.ActiveStepIndex
    End Sub

    Protected Sub wizBookingProcess_NextButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles wizBookingProcess.NextButtonClick
        If Not Page.IsValid AndAlso Me.wizBookingProcess.ActiveStepIndex = 0 AndAlso pageValid = False Then
            e.Cancel = True

        ElseIf Me.wizBookingProcess.ActiveStepIndex = 2 Then
            If Not pageValid Then
                e.Cancel = True
            Else
                Me.wizBookingProcess.MoveTo(SummaryStep4)
            End If
        Else
            If Me.wizBookingProcess.ActiveStepIndex = 0 Then
                If Not pageValid Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Protected Sub wizardControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wizardControl.SelectedIndexChanged
        Me.wizBookingProcess.ActiveStepIndex = wizardControl.SelectedIndex
    End Sub

    Protected Sub ddlContact_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub Step3_PickerControlForBranchCKI_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_PickerControlForBranchCKI.TextChanged
        ''rev:mia nov28
        Dim location As String = ""
        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKI.Text) Then
            location = Me.Step3_PickerControlForBranchCKI.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKI.Text, Me.Step3_ddlAMPMcki)

        If Step3_PickerControlForBranchCKI.Text.TrimEnd.Length > 3 Then
            If Me.Step3_ddlAMPMcki.Items.Count = 0 And Step3_PickerControlForBranchCKI.Text.IndexOf("-") = -1 Then
                Me.SetWarningShortMessage(INVALID_LOCATION)
                Exit Sub
            End If
            If Me.Step3_ddlAMPMcki.Items.Count = 0 Then Exit Sub

            Me.SetInformationShortMessage(RESUBMIT_TABLE)
        Else
            Me.SetWarningShortMessage(INVALID_LOCATION)
        End If

    End Sub

    Protected Sub Step3_PickerControlForBranchCKO_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_PickerControlForBranchCKO.TextChanged
        ''rev:mia nov28
        Dim location As String = ""
        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKO.Text) Then
            location = Me.Step3_PickerControlForBranchCKO.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKO.Text, Me.Step3_ddlamppmCKO)

        If Step3_PickerControlForBranchCKO.Text.TrimEnd.Length > 3 Then
            If Me.Step3_ddlamppmCKO.Items.Count = 0 And Step3_PickerControlForBranchCKO.Text.IndexOf("-") = -1 Then
                Me.SetWarningShortMessage(INVALID_LOCATION)
                Exit Sub
            End If
            If Me.Step3_ddlamppmCKO.Items.Count = 0 Then Exit Sub

            Me.SetInformationShortMessage(RESUBMIT_TABLE)
        Else
            Me.SetWarningShortMessage(INVALID_LOCATION)
        End If
    End Sub

    Protected Sub Step3_PickerControlForProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_PickerControlForProduct.TextChanged
        Me.SetInformationShortMessage(RESUBMIT_TABLE)
    End Sub

    Function ValidateVehicleAvailability() As Boolean
        Return Not String.IsNullOrEmpty(Me.Step3_DateControlForCKI.Text) And _
               Not String.IsNullOrEmpty(Me.Step3_DateControlForCKO.Text) And _
               Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKI.Text) And _
               Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKO.Text) And _
               Not String.IsNullOrEmpty(Me.Step3_PickerControlForProduct.Text)
    End Function


    Sub Step3_AddJSAttributes()

        ''For Step # 3
        Dim setParamOne As String = ""
        Dim setParamTwo As String = ""
        Dim setParamThree As String = ""
        Dim setParamFour As String = ""
        Dim setParamFive As String = ""

        Dim script As New StringBuilder

        With script
            .Append("function CKOscriptStep3(){")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString() != '')")
            .Append("{")

            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString());")
            .Append("}")

            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 2, '');")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString());")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("}")


            .Append("}")
        End With
        If ClientScript.IsStartupScriptRegistered(Me.GetType, "CKOscriptStep3") = False Then
            ClientScript.RegisterStartupScript(Me.GetType, "CKOscriptStep3", script.ToString, True)
        End If


        setParamOne = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 1, '');"
        setParamThree = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForProduct_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForProduct_pickerTextBox').value.toString().indexOf(' -')));"

        td_step3_CKO.Attributes.Add("onkeyup", setParamOne & setParamThree & "CKOscriptStep3();")
        td_step3_CKO.Attributes.Add("onmouseup", setParamOne & setParamThree & "CKOscriptStep3();")




        script = Nothing : script = New StringBuilder
        With script
            .Append("function CKIscriptStep3(){")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString() != '')")
            .Append("{")

            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString());")
            .Append("}")

            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 2, '');")
            .Append("}")
            .Append("else")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString());")
            .Append("if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().indexOf('-') != -1)")
            .Append("{")
            .Append("PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox').value.toString().substring(0,3));")
            .Append("}")
            .Append("}")

            .Append("}")
        End With
        If ClientScript.IsStartupScriptRegistered(Me.GetType, "CKIscriptStep3") = False Then
            ClientScript.RegisterStartupScript(Me.GetType, "CKIscriptStep3", script.ToString, True)
        End If

        setParamOne = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox').value.toString().indexOf(' -')));"
        setParamThree = "PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForProduct_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForProduct_pickerTextBox').value.toString().indexOf(' -')));"
        td_step3_CKI.Attributes.Add("onkeyup", setParamOne & setParamThree & "CKIscriptStep3();")
        td_step3_CKI.Attributes.Add("onmouseup", setParamOne & setParamThree & "CKIscriptStep3();")

    End Sub
#End Region

#Region " popup postback"
    Protected Sub updateAgentButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateAgentButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then
            Me.SetErrorShortMessage("No Valid agent has been specified in the Agent field")
        Else

            Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
            Dim dt As New DataTable
            dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
            Dim agnIsMisc As Boolean = False
            If dt.Rows.Count > 0 And dt.Columns.Contains("ISMISC") Then
                agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
            End If

            If agnIsMisc Then
                bookingRequestAgentMgtPopupUserControl1.BookingId = BkrID
                bookingRequestAgentMgtPopupUserControl1.loadPopup()
            Else
                Dim qsRaw As String = Request.RawUrl


                ''------------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''commented
                ''------------------------------------------
                ''Session("VhrID") = Me.VhrID
                ''Session("agentId") = PickerControlForAgent.DataId.Trim()
                ''Session("agentText") = PickerControlForAgent.Text
                ''------------------------------------------

                If Not String.IsNullOrEmpty(Me.hidAgentID.Value) Then
                    Response.Redirect("~/../web/Agent/Maintenance.aspx?agnId=" & Me.hidAgentID.Value & "&agentText=" & Me.PickerControlForAgent.Text)
                Else
                    Dim agentid As String = GetAgentID(Me.PickerControlForAgent.Text)
                    Response.Redirect("~/../web/Agent/Maintenance.aspx?agnId=" & agentid & "&agentText=" & Me.PickerControlForAgent.Text)
                End If

            End If
        End If

    End Sub


    Protected Sub addAgentLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addAgentButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.Text) Then
            ''Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
            Response.Redirect("~/../web/Agent/search.aspx")
        Else
            If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then

                Dim agentName As String = ""

                agentName = PickerControlForAgent.Text
                Dim ds As DataSet
                ds = Booking.ShowBookingRequestAgent(agentName)

                If ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 OrElse ds.Tables(0).Rows(0)(0) = "Error" Then
                    ''Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
                    Response.Redirect("~/../web/Agent/search.aspx")
                Else
                    showBookingRequestAgentPopupUserControl1.loadPopup(PickerControlForAgent.Text)
                End If

            Else
                Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
                Dim dt As New DataTable
                dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
                Dim agnIsMisc As Boolean = False
                If dt.Rows.Count > 0 Then
                    agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
                End If

                If agnIsMisc Then
                    bookingRequestAgentMgtPopupUserControl1.BookingId = BkrID
                    bookingRequestAgentMgtPopupUserControl1.loadPopup()
                    Return
                Else
                    showBookingRequestAgentPopupUserControl1.loadPopup(PickerControlForAgent.Text)
                    Return
                End If
            End If

        End If

    End Sub


    Protected Sub agentContactImageButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentContactImageButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then

            'JL 2008/06/17
            'empty the ddlcontact items
            ddlContact.DataSource = Nothing
            ddlContact.DataBind()

            SetShortMessage(AuroraHeaderMessageType.Information, "No Valid agent has been specified in the Agent field")

        Else
            Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
            Dim dt As New DataTable
            dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
            Dim agnIsMisc As Boolean = False
            If dt.Rows.Count > 0 Then
                agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
            End If

            If agnIsMisc Then
                'JL 2008/06/17
                'empty the ddlcontact items
                ddlContact.DataSource = Nothing
                ddlContact.DataBind()

                SetShortMessage(AuroraHeaderMessageType.Information, "No list contact is available  for a miscellaneous agent.\nEnter or update the contact name via the Agent details(folder icon)")
            Else
                ContactAgentMgtPopupUserControl1.AgentId = PickerControlForAgent.DataId
                ContactAgentMgtPopupUserControl1.LoadPopup()
            End If
        End If

    End Sub

    Protected Sub showBookingRequestAgentPopupUserControl1_PostBack(ByVal sender As Object, ByVal addButton As Boolean, ByVal selectedAgent As Boolean, ByVal param As Object) Handles showBookingRequestAgentPopupUserControl1.PostBack

        If addButton Then
            ''Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
            Response.Redirect("~/../web/Agent/search.aspx")
        ElseIf selectedAgent Then

            Dim list As ArrayList
            list = CType(param, ArrayList)

            Dim agentId As String = Utility.ParseString(list.Item(0), "")
            Dim agentCode As String = Utility.ParseString(list.Item(1), "")
            Dim agentName As String = Utility.ParseString(list.Item(2), "")

            PickerControlForAgent.DataId = agentId
            PickerControlForAgent.Text = agentCode & " - " & agentName

        End If

    End Sub


    Protected Sub bookingRequestAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal saveButton As Boolean, ByVal backButton As Boolean, ByVal param As Object) Handles bookingRequestAgentMgtPopupUserControl1.PostBack
        If saveButton Then

        End If
    End Sub

    Protected Sub ContactAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles ContactAgentMgtPopupUserControl1.PostBack
        Dim requireRefresh As Boolean
        requireRefresh = Utility.ParseBoolean(param, True)

        If requireRefresh Then
            ddlContact_DataBinding(PickerControlForAgent.DataId)
        End If
    End Sub


    Private Sub ddlContact_DataBinding(ByVal agentid As String)

        If Not String.IsNullOrEmpty(agentid) Then
            Dim xmlDoc As XmlDocument
            xmlDoc = Data.ExecuteSqlXmlSPDoc("RES_getContact", "AGENT", agentid, "Reservation", "")
            Dim ds As DataSet = New DataSet()
            ds.ReadXml(New XmlNodeReader(xmlDoc))

            If ds.Tables.Count > 0 Then
                ddlContact.DataSource = ds.Tables(0)
                ddlContact.DataTextField = "ConName"
                ddlContact.DataValueField = "ConId"
                ddlContact.DataBind()

                ddlContact.Items.Add(New ListItem("", ""))
            End If
        Else
            ddlContact.DataSource = Nothing
            ddlContact.DataBind()
        End If

    End Sub

    Protected Sub wizBookingProcess_PreviousButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles wizBookingProcess.PreviousButtonClick
        ''If Me.BackUrl.Contains("package.aspx") AndAlso Me.wizBookingProcess.ActiveStepIndex = 2 Then
        If (Me.BackUrl.Contains("package.aspx") Or Not String.IsNullOrEmpty(Request.QueryString("aspBack"))) AndAlso Me.wizBookingProcess.ActiveStepIndex = 2 Then
            Me.wizBookingProcess.MoveTo(DuplicateRentalsStep2)
        ElseIf Me.wizBookingProcess.ActiveStepIndex = 1 Then
            Me.wizBookingProcess.MoveTo(bookingRequestStep1)
        End If
    End Sub


#End Region

#Region "Unused"

    Protected Sub btnBrochure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBrochure.Click
        Response.Redirect(QS_RS_CUSTOMERFIND & "?hdBookingId=" & Me.BooID & "&&hdRentalId=" & Me.RentalID)
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return Me._eventargument
    End Function

    Private _eventargument As String

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Me._eventargument = eventArgument
    End Sub

    Protected Sub PickerControlForProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForProduct.TextChanged
    End Sub

    Protected Sub PickerControlForCheckOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForCheckOut.TextChanged
    End Sub

    Protected Sub PickerControlForCheckIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForCheckIn.TextChanged
    End Sub

    Protected Sub ddlInfantsNO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInfantsNO.SelectedIndexChanged
    End Sub

    Protected Sub ddlChildNO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChildNO.SelectedIndexChanged
    End Sub

    Protected Sub ddlAdultNO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAdultNO.SelectedIndexChanged
    End Sub

    Protected Sub ddlAMPMcheckin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAMPMcheckin.SelectedIndexChanged
    End Sub

    Protected Sub ddlAMPMcheckout_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAMPMcheckout.SelectedIndexChanged
    End Sub

    Function PackageParameterThree(ByVal index As Integer, ByVal paramvalue As String) As String
    End Function

    Private Sub CheckOutValidation()
        Dim tempTime As String = "08:00 AM"
        If Me.ddlAMPMcheckout.Items.Count = 1 Then
            If Me.ddlAMPMcheckout.SelectedItem.Text = "AM" Then
                tempTime = "08:00"
            ElseIf Me.ddlAMPMcheckout.SelectedItem.Text = "PM" Then
                tempTime = "13:00"
            End If
        Else
            If Me.ddlAMPMcheckout.SelectedItem.Text = "12:00 PM" Then
                tempTime = "13:00:00 PM"
            Else
                tempTime = Me.ddlAMPMcheckout.SelectedItem.Text
            End If

        End If

        Dim temp As String = ParseDateTime(DateControlForCheckOut.Date) & " " & tempTime
        If String.IsNullOrEmpty(temp) = False Then
            paramThree = PackageParameterThree(2, temp)
        End If


    End Sub

    Private Sub CheckInValidation()
        Dim tempTime As String = "08:00 AM"
        If Me.ddlAMPMcheckin.Items.Count = 1 Then
            If Me.ddlAMPMcheckin.SelectedItem.Text = "AM" Then
                tempTime = "08:00"
            ElseIf Me.ddlAMPMcheckin.SelectedItem.Text = "PM" Then
                tempTime = "13:00"
            End If
        Else
            If Me.ddlAMPMcheckin.SelectedItem.Text = "12:00 PM" Then
                tempTime = "13:00:00 PM"
            Else
                tempTime = Me.ddlAMPMcheckin.SelectedItem.Text
            End If

        End If

        Dim temp As String = ParseDateTime(DateControlForCheckIn.Date) & " " & tempTime
        If String.IsNullOrEmpty(temp) = False Then
            paramThree = PackageParameterThree(8, temp)
        End If

    End Sub


    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        System.Diagnostics.Debug.WriteLine(ddlBrand.SelectedItem.Text & ":" & ddlBrand.SelectedItem.Value)
        If ddlBrand.SelectedItem.Value.ToString <> "0" Then
            ''PickerControlForProduct.Param2 = ddlBrand.SelectedItem.Value
        Else
            ''PickerControlForProduct.Param2 = ""
        End If

    End Sub



    Protected Sub rdlACorAV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlACorAV.SelectedIndexChanged
        If rdlACorAV.SelectedItem.Text <> "NA" Then
            strButtonListValue = rdlACorAV.SelectedValue
            ''-----------------------------------------
            ''rev:mia jan 16 session state to viewstate
            ''-----------------------------------------
            ''Session("listvalue") = strButtonListValue
            ListValue = strButtonListValue
            ''-----------------------------------------
            Call Step1_GetVehicleTypes(strButtonListValue)
            tableview.Visible = True
            tableviewDefault.Visible = False
        Else
            Me.GridView1.DataSource = Nothing
            Me.GridView1.DataBind()
            tableview.Visible = False
            tableviewDefault.Visible = True
        End If
    End Sub

    Dim UncommonFeature1 As String
    Protected Sub ddlUncommonFeature1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature1.SelectedIndexChanged
        DynamicParameterThree()
    End Sub

    Dim features As String
    Dim automaticOrManual As String = "09E27587-1B13-4F18-8CCA-0A275F47C933"
    Dim ManualTransmission As String = "22"
    Dim AirConditionedFrontCabOnly As String = "35DB2029-6E6C-4461-BDCB-24529171AFAD"
    Dim BabyBolt As String = "A6A55F74-9783-4B28-A32C-CA508069B1FA"
    Dim AirConditionedRearCabOnly As String = "A96FC84D-2E48-445A-9129-B8C6CABCAA66"
    Dim ColumnChangeAutomatic As String = "C3AA2338-6FEB-4124-9B92-7B471B74334B"
    Dim AirConditionedFrontRear As String = "DD01FCBE-2211-438E-8C87-3AEB6161ACEF"

    Protected Sub chkFeatures_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFeatures.SelectedIndexChanged
        DynamicParameterThree()
    End Sub

    Dim UncommonFeature2 As String
    Protected Sub ddlUncommonFeature2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature2.SelectedIndexChanged
        DynamicParameterThree()
    End Sub


    Sub DynamicParameterThree()
        UncommonFeature1 = ddlUncommonFeature1.SelectedItem.Value.ToString & ","
        UncommonFeature2 = ddlUncommonFeature2.SelectedItem.Value.ToString & ","
        If UncommonFeature1 = "0," Then
            UncommonFeature1 = ""
        End If

        If UncommonFeature2 = "0," Then
            UncommonFeature2 = ""
        End If

        For Each chkitems As ListItem In chkFeatures.Items
            If chkitems.Selected Then
                features &= chkitems.Value & ","
            End If
        Next
        Dim temp As String = String.Concat(features, UncommonFeature1, UncommonFeature2)
        If Not String.IsNullOrEmpty(temp) Then
            ''PickerControlForProduct.Param3 = IIf(temp.EndsWith(","), temp.Remove(temp.Length - 1), temp)

        Else
            ''PickerControlForProduct.Param3 = ""

        End If

    End Sub
    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Dim key As DataKey = GridView1.SelectedDataKey
        ''PickerControlForProduct.Param5 = key.Value
    End Sub
#End Region

#Region "Fix for page not remembering status"

    ReadOnly Property ListOfProblemVehicles() As Hashtable
        Get
            If ViewState("ListOfProblemVehicles") Is Nothing Then
                Return New Hashtable
            Else
                Return (CType(ViewState("ListOfProblemVehicles"), Hashtable))
            End If
        End Get
    End Property

    Sub AddToListOfProblemVehicles(ByVal strSlvId As String, ByVal problemWithThisSelectedVehicle As ProblemsWithSelectedVehicle)
        Dim htemp As New Hashtable
        htemp = ListOfProblemVehicles
        If Not htemp.Contains(strSlvId) Then
            htemp.Add(strSlvId, problemWithThisSelectedVehicle)
            ViewState("ListOfProblemVehicles") = htemp
        End If
    End Sub


    Public Enum ProblemsWithSelectedVehicle
        SelectedVehicleIsKBOnly = 0
        SelectedVehicleHasNoAvailability = 1
    End Enum

#End Region

#Region "POS enhancement"
    ''rev:mia POS
    Sub POS_Forcecheck()
        ' Fix for page not remembering status
        Return
        ' Fix for page not remembering status
        For Each row As GridViewRow In Step4_GridSummary.Rows
            Dim checkbox As CheckBox = CType(row.FindControl("chkCheck"), CheckBox)
            checkbox.Checked = True
            checkbox.Enabled = False
            Me.SLVid = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString
            Dim item As String = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString

            If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString Then

                If checkbox.Checked = True Then
                    If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = Step4_GridSummary.DataKeys(row.DataItemIndex).Value.ToString Then
                        Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "1"

                    End If
                Else
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "0"

                    CType(Step4_GridSummary.FooterRow.FindControl("lblfootercost"), Label).Text = ""
                End If

                Dim ddlRS As DropDownList = CType(row.FindControl("ddlRS"), DropDownList)
                If ddlRS IsNot Nothing Then
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle/ReqSrc").InnerText = ddlRS.SelectedItem.Value
                End If

                Step4_ClickCheck()
            End If
        Next
    End Sub

#End Region

#Region "TextChanges and SelectedIndex changes"
    Protected Sub Step3_ddlAMPMcki_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_ddlAMPMcki.SelectedIndexChanged
        Me.HidCKIampm.Value = Me.Step3_ddlAMPMcki.Text
    End Sub

    Protected Sub Step3_ddlAMPMcki_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_ddlAMPMcki.TextChanged
        Me.HidCKIampm.Value = Me.Step3_ddlAMPMcki.Text
    End Sub

    Protected Sub Step3_ddlamppmCKO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_ddlamppmCKO.SelectedIndexChanged
        Me.HidCKOampm.Value = Me.Step3_ddlamppmCKO.Text
    End Sub

    Protected Sub Step3_ddlamppmCKO_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Step3_ddlamppmCKO.TextChanged
        Me.HidCKOampm.Value = Me.Step3_ddlamppmCKO.Text
    End Sub
#End Region

#Region "After Releases Fixes - Oct.29"
    ''rev:mia oct.29
    Private Property DefaultAgent() As String
        Get
            Return CType(ViewState("DefaultAgent"), String).TrimEnd
        End Get
        Set(ByVal value As String)
            ViewState("DefaultAgent") = value
        End Set
    End Property
    Sub GetDefaultAgent()
        Dim xmltemp As XmlDocument
        Try
            xmltemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getDefaultAgent", Me.UserCode)
            DefaultAgent = xmltemp.DocumentElement.ChildNodes(0).ChildNodes(0).InnerText
        Catch ex As Exception
            DefaultAgent = ""
        Finally
            xmltemp = Nothing
        End Try

    End Sub
#End Region

#Region "Nov.11 Changes for Viewstate"
    Private Property _SESSION_hashproperties() As Hashtable
        Get
            Return CType(ViewState("_SESSION_hashproperties"), Hashtable)
        End Get
        Set(ByVal value As Hashtable)
            ViewState("_SESSION_hashproperties") = value
        End Set
    End Property

    Private Property _SESSION_XMLVehicleRequest() As String
        Get
            Return ViewState("_SESSION_XMLVehicleRequest")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_XMLVehicleRequest") = value
        End Set
    End Property

    Private Sub Step1_SplitDataOnPostBackR2()
        'XMLVehicleRequest = New XmlDocument
        'XMLVehicleRequest.LoadXml(Me._SESSION_XMLVehicleRequest)
        'Dim xmlstring As String = XMLVehicleRequest.OuterXml
        'If IsValidXMLstring(xmlstring) Then
        '    Dim xmlelem As XmlElement = XMLVehicleRequest.DocumentElement
        '    Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
        '    Me.Brand = XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand").InnerText
        '    Me.Class = XMLVehicleRequest.SelectSingleNode("VehicleRequest/class").InnerText
        '    Me.TypeID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/type").InnerText
        '    Me.BKRIntegrityNo = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo").InnerText
        '    Me.MiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
        '    Me.HireOUM = XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype").InnerText
        'End If

        Try
            XMLVehicleRequest = New XmlDocument
            XMLVehicleRequest.LoadXml(Me._SESSION_XMLVehicleRequest)
            Dim xmlstring As String = XMLVehicleRequest.OuterXml

            If IsValidXMLstring(xmlstring) Then

                Dim xmlelem As XmlElement = XMLVehicleRequest.DocumentElement
                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid") IsNot Nothing Then
                    Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand") IsNot Nothing Then
                    Me.Brand = XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand").InnerText
                End If

                If (XMLVehicleRequest.SelectSingleNode("VehicleRequest/class") IsNot Nothing) Then
                    Me.Class = XMLVehicleRequest.SelectSingleNode("VehicleRequest/class").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/type") IsNot Nothing Then
                    Me.TypeID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/type").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo") IsNot Nothing Then
                    Me.BKRIntegrityNo = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName") IsNot Nothing Then
                    Me.MiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype") IsNot Nothing Then
                    Me.HireOUM = XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype").InnerText
                End If

            End If
        Catch ex As Exception
            Logging.LogException("Step1_SplitDataOnPostBackR2", ex)
        End Try
    End Sub

    Private Property _SESSION_xmlHeader() As String
        Get
            Return ViewState("_SESSION_xmlHeader")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlHeader") = value
        End Set
    End Property

    Private Property _SESSION_xmlVhrHeader() As String
        Get
            Return ViewState("_SESSION_xmlVhrHeader")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlVhrHeader") = value
        End Set
    End Property

    Private Property _SESSION_xmlBooHeader() As String
        Get
            Return ViewState("_SESSION_xmlBooHeader")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlBooHeader") = value
        End Set
    End Property

    Private Property _SESSION_xmlDuplicateRental() As String
        Get
            Return ViewState("_SESSION_xmlDuplicateRental")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlDuplicateRental") = value
        End Set
    End Property

    Private Property _SESSION_xmlDetails() As String
        Get
            Return ViewState("_SESSION_xmlDetails")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlDetails") = value
        End Set
    End Property

    Private Property _SESSION_xmlDoc() As String
        Get
            Return ViewState("_SESSION_xmlDoc")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_xmlDoc") = value
        End Set
    End Property

    Private Property _SESSION_selectedVehicleList_xmlCur() As String
        Get
            Return ViewState("_SESSION_selectedVehicleList_xmlCur")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_selectedVehicleList_xmlCur") = value
        End Set
    End Property

    Private Property _SESSION_selectedVehicleList_xmlChosenVehicle() As String
        Get
            Return ViewState("_SESSION_selectedVehicleList_xmlChosenVehicle")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_selectedVehicleList_xmlChosenVehicle") = value
        End Set
    End Property

    Private Property _SESSION_selectedVehicleList_xmlErrorMsg() As String
        Get
            Return ViewState("_SESSION_selectedVehicleList_xmlErrorMsg")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_selectedVehicleList_xmlErrorMsg") = value
        End Set
    End Property

    Private Property _SESSION_selectedVehicleList_xmlRequest() As String
        Get
            Return ViewState("_SESSION_selectedVehicleList_xmlRequest")
        End Get
        Set(ByVal value As String)
            ViewState("_SESSION_selectedVehicleList_xmlRequest") = value
        End Set
    End Property

    Private Sub AddToSessionR2(ByVal proc As BookingProcess)
        Try
            Select Case proc
                Case BookingProcess.CheckDuplicate

                    _SESSION_xmlHeader = Me.xmlHeader.OuterXml
                    _SESSION_xmlVhrHeader = Me.xmlVhrHeader.OuterXml
                    _SESSION_xmlBooHeader = Me.xmlBooHeader.OuterXml
                    _SESSION_xmlDuplicateRental = Me.xmlDuplicateRental.OuterXml
                    _SESSION_xmlDetails = Me.xmlDetails.OuterXml
                    _SESSION_xmlDoc = Me.xmlDoc.OuterXml

                Case BookingProcess.Summary
                    _SESSION_selectedVehicleList_xmlCur = Me.xmlCur.OuterXml
                    _SESSION_selectedVehicleList_xmlChosenVehicle = Me.xmlChosenVehicle.OuterXml
                    _SESSION_selectedVehicleList_xmlErrorMsg = Me.xmlErrorMsg.OuterXml
                    _SESSION_selectedVehicleList_xmlRequest = Me.xmlRequest.OuterXml
            End Select

        Catch ex As Exception
            Logging.LogError("AddtosessionR2", ex.Message)
        End Try
    End Sub

    Private Sub GetFromSessionR2(ByVal proc As BookingProcess)
        Try
            Select Case proc
                Case BookingProcess.CheckDuplicate

                    Me.xmlHeader = New XmlDocument
                    Me.xmlHeader.LoadXml(Me._SESSION_xmlHeader)

                    Me.xmlVhrHeader = New XmlDocument
                    Me.xmlVhrHeader.LoadXml(_SESSION_xmlVhrHeader)

                    Me.xmlBooHeader = New XmlDocument
                    Me.xmlBooHeader.LoadXml(_SESSION_xmlBooHeader)

                    Me.xmlDuplicateRental = New XmlDocument
                    Me.xmlDuplicateRental.LoadXml(_SESSION_xmlDuplicateRental)

                    Me.xmlDoc = New XmlDocument
                    Me.xmlDoc.LoadXml(_SESSION_xmlDoc)


                Case BookingProcess.Summary

                    Me.xmlCur = New XmlDocument
                    Me.xmlCur.LoadXml(_SESSION_selectedVehicleList_xmlCur)

                    Me.xmlChosenVehicle = New XmlDocument
                    Me.xmlChosenVehicle.LoadXml(_SESSION_selectedVehicleList_xmlChosenVehicle)

                    Me.xmlErrorMsg = New XmlDocument
                    Me.xmlErrorMsg.LoadXml(_SESSION_selectedVehicleList_xmlErrorMsg)

                    Me.xmlRequest = New XmlDocument
                    Me.xmlRequest.LoadXml(_SESSION_selectedVehicleList_xmlRequest)

            End Select
        Catch ex As Exception
            Logging.LogError("GetFromSessionR2", ex.Message)
        End Try
    End Sub

#End Region

#Region "Nov.18 Changes for season"

    Private Sub Step1_SelectLocationBasedOnCodeAndUserV2(ByVal locationCode As String, ByVal traveldate As String, Optional ByVal ddlControl As DropDownList = Nothing)
        Dim iscko As Boolean = True
        If ddlControl.ID = "ddlAMPMcheckin" Then
            iscko = False
        End If

        Try

            Dim index As Integer
            Dim indexText As String = ""
            If Page.IsPostBack Then
                Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
                Select Case sc.AsyncPostBackSourceElementID
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckOut$dateTextBox", _
                        "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox"
                        If HidCKO.Value = Me.PickerControlForCheckOut.Text Then
                            index = Me.ddlAMPMcheckout.SelectedIndex
                            indexText = Me.ddlAMPMcheckout.Text
                        End If
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$DateControlForCheckIn$dateTextBox", _
                    "ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox"
                        If HidCKI.Value = Me.PickerControlForCheckIn.Text Then
                            index = Me.ddlAMPMcheckin.SelectedIndex
                            indexText = Me.ddlAMPMcheckin.Text
                        End If
                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckOut$PickerTextbox", _
                        "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_PickerTextbox"
                        HidCKO.Value = Me.PickerControlForCheckOut.Text

                    Case "ctl00$ContentPlaceHolder$wizBookingProcess$PickerControlForCheckIn$PickerTextbox", _
                    "ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_PickerTextbox"
                        HidCKI.Value = Me.PickerControlForCheckIn.Text

                    Case "ctl00_ContentPlaceHolder_wizardControl_itemDropDownList", _
                    "ctl00$ContentPlaceHolder$wizBookingProcess$StepNavigationTemplateContainerID$StepPreviousButton"
                    Case Else

                End Select
            End If

            Dim temp As String = ""
            If ddlControl.Text <> "AM" And ddlControl.Text <> "PM" Then
                temp = ddlControl.Text
            End If

            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If

            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationStartFinishTime", dsTemp, locationCode, traveldate, IIf(iscko = True, 0, 1))
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                Me.SetWarningShortMessage("No available time for location code '" & locationCode & "' and userCode '" & MyBase.UserCode & "'")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)("StartTime").ToString
            Dim endtime As String = ds.Tables(0).Rows(0)("FinishTime").ToString

            ''rev:mia nov.25 - just for error trapping just incase the first level of validation failed!!
            ddlControl.Items.Clear()
            Dim location As String = IIf(iscko = True, Me.PickerControlForCheckOut.Text, Me.PickerControlForCheckIn.Text)
            Dim day As String
            Dim mydate As String


            If iscko = True Then
                day = Me.lbldayCKO.Text
                mydate = Me.DateControlForCheckOut.Text
            Else
                day = Me.lblDayCKI.Text
                mydate = Me.DateControlForCheckIn.Text
            End If

            If String.IsNullOrEmpty(startTime) Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & mydate)
            End If
            If String.IsNullOrEmpty(endtime) Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & mydate)
            End If

            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If




            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":")(0)), CInt(endtime.Split(":")(1)), CInt(endtime.Split(":")(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":")(0)), CInt(startTime.Split(":")(1)), CInt(startTime.Split(":")(2)))

            While tmTempTimeValue < tmEndTime
                ddlControl.Items.Add(tmTempTimeValue.Hours.ToString().PadLeft(2, "0") & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While


            ddlControl.Items.Add(tmEndTime.Hours.ToString().PadLeft(2, "0") & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"))

            ddlControl.SelectedIndex = index

            If ddlControl.ID = Me.ddlAMPMcheckout.ID Then
                If Me.HidCKOampm.Value = "" Then
                    ddlControl.SelectedIndex = 0
                Else
                    ''rev:mia sept 29
                    ''ddlControl.SelectedItem.Text = Me.HidCKOampm.Value
                    If Me.HidCKOampm.Value.Length = 8 Then
                        ddlControl.SelectedValue = Me.HidCKOampm.Value.Substring(0, 5)
                    ElseIf Me.HidCKOampm.Value.Length = 5 Then
                        ddlControl.SelectedValue = Me.HidCKOampm.Value
                    End If

                End If

            Else
                If Me.HidCKIampm.Value = "" Then
                    ddlControl.SelectedIndex = 0
                Else
                    ''rev:mia sept 29
                    ''ddlControl.SelectedItem.Text = Me.HidCKIampm.Value
                    If Me.HidCKIampm.Value.Length = 8 Then
                        ddlControl.SelectedValue = Me.HidCKIampm.Value.Substring(0, 5)
                    ElseIf Me.HidCKIampm.Value.Length = 5 Then
                        ddlControl.SelectedValue = Me.HidCKIampm.Value
                    End If

                End If

            End If

        Catch ex As Exception

        End Try
    End Sub

    Sub SetLocationDateTimeV2()
        ''rev:mia nov18
        ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
        ''Me.Step1_SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
        Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckIn.Text, Me.DateControlForCheckIn.Text, Me.ddlAMPMcheckin)
        Me.Step1_SelectLocationBasedOnCodeAndUserV2(Me.PickerControlForCheckOut.Text, Me.DateControlForCheckOut.Text, Me.ddlAMPMcheckout)
    End Sub

    Sub Step3_SelectLocationBasedOnCodeAndUserV2(ByVal locationCode As String, ByVal traveldate As String, Optional ByVal ddlControl As DropDownList = Nothing, Optional ByVal dateinterval As Integer = 15)
        Try
            Dim locationcodehashypen As Boolean = False

            Dim iscko As Boolean = False
            If ddlControl.ID = "Step3_ddlamppmCKO" Then
                iscko = True
            End If
            Dim index As Integer

            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            ''rev:mia may 20 2011 - trapped empty traveldate
            If String.IsNullOrEmpty(traveldate) Then Exit Sub

            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
                locationcodehashypen = True
            End If



            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationStartFinishTime", dsTemp, locationCode, traveldate, IIf(iscko = True, 0, 1))
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)("StartTime").ToString
            Dim endtime As String = ds.Tables(0).Rows(0)("FinishTime").ToString

            ''rev:mia nov.25 - just for error trapping just incase the first level of validation failed!!
            ViewState("branchisclosed") = False
            ddlControl.Items.Clear()
            Dim location As String = IIf(iscko, Me.Step3_PickerControlForBranchCKO.Text, Me.Step3_PickerControlForBranchCKI.Text)
            Dim day As String
            Dim mydate As String

            If iscko = True Then
                day = Me.Step3_lblckoDay.Text
                mydate = Me.Step3_DateControlForCKO.Text
            Else
                day = Me.Step3_lblckiDay.Text
                mydate = Me.Step3_DateControlForCKI.Text
            End If

            If String.IsNullOrEmpty(startTime) Then
                If iscko Then
                    locationcodehashypen = Me.Step3_PickerControlForBranchCKO.Text.Contains("-")
                    If Not locationcodehashypen Then Exit Sub
                Else
                    locationcodehashypen = Me.Step3_PickerControlForBranchCKI.Text.Contains("-")
                    If Not locationcodehashypen Then Exit Sub
                End If

                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & mydate)
                ViewState("branchisclosed") = True
                Exit Sub
            End If
            If String.IsNullOrEmpty(endtime) Then
                If iscko Then
                    locationcodehashypen = Me.Step3_PickerControlForBranchCKO.Text.Contains("-")
                    If Not locationcodehashypen Then Exit Sub
                Else
                    locationcodehashypen = Me.Step3_PickerControlForBranchCKI.Text.Contains("-")
                    If Not locationcodehashypen Then Exit Sub
                End If
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & mydate)
                ViewState("branchisclosed") = True
                Exit Sub
            End If


            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If


            ' fix by shoel to break out of blackhole loops :)
            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":")(0)), CInt(endtime.Split(":")(1)), CInt(endtime.Split(":")(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":")(0)), CInt(startTime.Split(":")(1)), CInt(startTime.Split(":")(2)))

            While tmTempTimeValue < tmEndTime
                ddlControl.Items.Add(tmTempTimeValue.Hours.ToString().PadLeft(2, "0") & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While

            ' when tmTempTimeValue = tmEndTime
            ddlControl.Items.Add(tmEndTime.Hours.ToString().PadLeft(2, "0") & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"))

            ' fix by shoel to break out of blackhole loops :)
            ddlControl.SelectedIndex = index

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Function IsBranchLocationClosed(ByVal processStepOne As Boolean) As Boolean
        Dim location As String
        Dim day As String

        If processStepOne = True Then
            location = Me.PickerControlForCheckOut.Text ''.Split("-")(0)
            day = Me.lbldayCKO.Text

            If HidhasValueCKO.Value = False Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.DateControlForCheckOut.Text)
                sc.SetFocus(Me.DateControlForCheckOut)
                ddlAMPMcheckout.Items.Clear()
                Return False
            End If

            location = Me.PickerControlForCheckIn.Text ''.Split("-")(0)
            day = Me.lblDayCKI.Text

            If HidhasValueCKI.Value = False Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.DateControlForCheckIn.Text)
                sc.SetFocus(DateControlForCheckIn)
                Me.ddlAMPMcheckin.Items.Clear()
                Return False
            End If
        Else
            location = Me.Step3_PickerControlForBranchCKO.Text ''.Split("-")(0)
            day = Me.Step3_lblckoDay.Text

            If location.IndexOf("-") = -1 AndAlso Me.Step3_ddlamppmCKO.Items.Count = 0 Then
                Me.SetWarningShortMessage(INVALID_LOCATION)
                Exit Function
            End If
            If Me.Step3_ddlamppmCKO.Items.Count = 0 Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.Step3_DateControlForCKO.Text)
                Return False
            End If


            If Me.Step3_ddlamppmCKO.Text = "AM" Or Me.Step3_ddlamppmCKO.Text = "PM" Or Me.Step3_ddlamppmCKO.Text = "" Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.Step3_DateControlForCKO.Text)
                sc.SetFocus(Step3_DateControlForCKO)
                Return False
            End If

            location = Me.Step3_PickerControlForBranchCKI.Text ''.Split("-")(0)
            day = Me.Step3_lblckiDay.Text

            If location.IndexOf("-") = -1 AndAlso Me.Step3_ddlAMPMcki.Items.Count = 0 Then
                Me.SetWarningShortMessage(INVALID_LOCATION)
                Exit Function
            End If

            If Me.Step3_ddlAMPMcki.Items.Count = 0 Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.Step3_DateControlForCKI.Text)
                Exit Function
            End If


            If Me.Step3_ddlAMPMcki.Text = "AM" Or Me.Step3_ddlAMPMcki.Text = "PM" Or Me.Step3_ddlAMPMcki.Text = "" Then
                Me.SetWarningShortMessage(location & " " & CLOSED_BRANCH_LOCATION & " " & day & " " & Me.Step3_DateControlForCKI.Text)
                sc.SetFocus(Step3_DateControlForCKI)
                Return False
            End If
        End If
        Return True
    End Function


#End Region

#Region "Dec 1,2008 changes"
    ''rev:mia dec1
    Sub RevalidateTimes()
        Dim location As String = ""
        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKO.Text) Then
            location = Me.Step3_PickerControlForBranchCKO.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKO.Text, Me.Step3_ddlamppmCKO)

        If Not String.IsNullOrEmpty(Me.Step3_PickerControlForBranchCKI.Text) Then
            location = Me.Step3_PickerControlForBranchCKI.Text.Split("-")(0)
        End If
        Step3_SelectLocationBasedOnCodeAndUserV2(location, Me.Step3_DateControlForCKI.Text, Me.Step3_ddlAMPMcki)
    End Sub
#End Region

#Region "REV:MIA DECEMBER 10,2008 - WORKING ON TOGGLE FIXES"
    Public Sub ToggleButtonsClearingValue()
        Me.VhrID = ""
        Me.BooID = ""
        Me.BkrID = ""

        Me.PickerControlForAgent.ReadOnly = False
        SetUpdateAgentButtonVisible(True)
        SetAddAgentButtonVisible(True)
    End Sub

    Sub ToggleButtonsAction()
        Dim lnkToggleCompany As HtmlAnchor = CType(Master.FindControl("lnkToggleCompany"), HtmlAnchor)
        If lnkToggleCompany IsNot Nothing Then
            If lnkToggleCompany.InnerText.ToUpper.Contains("EXPLORE") Or lnkToggleCompany.InnerText.ToUpper.Contains("THL") Then
                ToggleButtonsClearingValue()
                ''-----------------------------------------
                ''rev:mia jan 16 session state to viewstate
                ''-----------------------------------------
                ''Session.Remove(SESSION_hashproperties)
                Me._SESSION_XMLVehicleRequest = ""
                ''-----------------------------------------

                If Me.wizBookingProcess.ActiveStepIndex <> 0 Then
                    Master.GotoDefaultPage("0")
                End If

            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        cbQuickAvail.Right_Button.Width = Unit.Pixel(70)

        If Not String.IsNullOrEmpty(ViewState("flagforBlockingRuleMessage")) AndAlso ViewState("TriggerByDateChanged") = False Then
            Me.SetInformationShortMessage(ViewState("flagforBlockingRuleMessage"))
        End If
        ViewState("TriggerByDateChanged") = False

        ''rev:mia dec11
        If sc.AsyncPostBackSourceElementID = "ctl00$lnkToggleCompany" Then
            Dim lnkToggleCompany As HtmlAnchor = CType(Master.FindControl("lnkToggleCompany"), HtmlAnchor)
            If lnkToggleCompany IsNot Nothing Then
                ToggleButtonsAction()
            End If
        End If

    End Sub

    ''rev:mia dont change this one...important when toggling company
    Private Property PostBackValue() As String
        Get
            Return Session("PostBackValuetoggle")
        End Get
        Set(ByVal value As String)
            Session("PostBackValuetoggle") = value
        End Set
    End Property

    Private Sub RetrievePostBackValue()
        If String.IsNullOrEmpty(PostBackValue) Then Exit Sub
        Dim temp As String = PostBackValue
        Dim xmltemp As New XmlDocument
        xmltemp.LoadXml(temp)

        Me.PickerControlForAgent.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/AgnCodeText").InnerText
        If Not String.IsNullOrEmpty(xmltemp.SelectSingleNode("Root/NewBookingRequest/ConCode").InnerText) Then
            Me.ddlContact.SelectedItem.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/ConCode").InnerText
        End If

        Me.txtRef.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/VhrAgnRef").InnerText
        Me.PickerControlForProduct.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/productText").InnerText
        Me.PickerControlForProduct.DataId = xmltemp.SelectSingleNode("Root/NewBookingRequest/PrdId").InnerText
        Me.PickerControlForPackage.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/packageText").InnerText
        Me.PickerControlForPackage.DataId = xmltemp.SelectSingleNode("Root/NewBookingRequest/PkgId").InnerText
        Me.ddlNoOfVehicles.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/NumberOfVehicles").InnerText
        Me.ddlAdultNO.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/NumberOfAdults").InnerText
        Me.ddlChildNO.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/NumberOfChildren").InnerText
        Me.ddlInfantsNO.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/NumberOfInfants").InnerText
        Me.PickerControlForCheckOut.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/loccko").InnerText
        Me.DateControlForCheckOut.Date = CDate(xmltemp.SelectSingleNode("Root/NewBookingRequest/ckoDate").InnerText)
        Me.PickerControlForCheckIn.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/loccki").InnerText
        Me.DateControlForCheckIn.Date = CDate(xmltemp.SelectSingleNode("Root/NewBookingRequest/ckiDate").InnerText)
        Me.txtHirePeriod.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/HirePeriod").InnerText
        Me.txtSurname.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/LastName").InnerText
        Me.txtfirstName.Text = xmltemp.SelectSingleNode("Root/NewBookingRequest/FirstName").InnerText


        ''rev:mia Sept.2 2013 - Customer Title from query
        ''Me.ddltitle.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/Title").InnerText
        Try
            customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(xmltemp.SelectSingleNode("Root/NewBookingRequest/Title").InnerText))
        Catch ex As Exception
            customertitleDropdown.SelectedIndex = 0
        End Try

        ''rev:mia june 2 2011 this is not required
        ''Me.ddlRequestSource.SelectedValue = xmltemp.SelectSingleNode("Root/NewBookingRequest/RequestSource").InnerText


    End Sub
#End Region

#Region "Jan 16 2009 related to Nov 18 changes"
    ''-----------------------------------------
    ''rev:mia jan 16 session state to viewstate
    ''-----------------------------------------
    Private Property ListValue() As String
        Get
            Return ViewState("ListValue")
        End Get
        Set(ByVal value As String)
            ViewState("ListValue") = value
        End Set
    End Property
#End Region

#Region "rev:mia jan 23 2009"
    Private Property AgentID() As String
        Get
            Return ViewState("AgentId")
        End Get
        Set(ByVal value As String)
            ViewState("AgentId") = value
        End Set
    End Property

    Private Property ProductIDForBypassRecalculation() As String
        Get
            Return ViewState("ProductIDForBypassRecalculation")
        End Get
        Set(ByVal value As String)
            ViewState("ProductIDForBypassRecalculation") = value
        End Set
    End Property
#End Region

#Region "rev:mia May 18 2011, EmailAddress added"
    Private Property EmailAddress As String
        Get
            Return viewstate("EmailAddress")
        End Get
        Set(ByVal value As String)
            viewstate("EmailAddress") = value
        End Set
    End Property

    Function CheckEmailAddress(ByVal emailaddress As TextBox) As Boolean
        Dim oRegex As Regex = New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
        If Not oRegex.IsMatch(emailaddress.Text.Trim) Then
            ScriptManager.GetCurrent(Page).SetFocus(emailaddress.Text)
            emailaddress.BackColor = Aurora.Common.DataConstants.RequiredColor
            CheckEmailAddress = False
            SetShortMessage(AuroraHeaderMessageType.Error, "Invalid Email address")
        Else
            emailaddress.BackColor = Drawing.Color.White
            Return True
        End If
    End Function



    Sub GetConfirmationEmail(ByVal bookingid As String)
        If String.IsNullOrEmpty(bookingid.Trim) Then Exit Sub

        Dim autoemail As String = Aurora.Common.Data.ExecuteScalarSP("GetConfirmationEmail", bookingid.Trim, bookingid.Trim.tostring & "-1")
        txtEmailAddress.text = autoemail
    End Sub



#End Region

#Region "rev:mia May 30 2011 - check if vehicle is available"

    Private _AvailExtensionParameter As ExtensionParemeter
    Private Property AvailExtensionParameter As ExtensionParemeter
        Get
            Return Session("AvailExtensionParameter")
        End Get
        Set(ByVal value As ExtensionParemeter)
            Session("AvailExtensionParameter") = value
        End Set
    End Property

    Function ImmediateAvailableChecking() As Boolean

        ''If UserCode.ToLower <> "ma2" Then Return False
        ImmediateAvailableChecking = False
        Dim sagent As String = Me.PickerControlForAgent.Text.Substring(0, PickerControlForAgent.Text.IndexOf("-")).TrimEnd.ToUpper
        Dim branchOut As String = PickerControlForCheckOut.Text.Substring(0, PickerControlForCheckOut.Text.IndexOf("-")).TrimEnd.ToUpper
        Dim branchIn As String = PickerControlForCheckIn.Text.Substring(0, PickerControlForCheckIn.Text.IndexOf("-")).TrimEnd.ToUpper


        ''rev:mia July 2 2013 - back to the office. 
        ''fixing problem in the addRental
        Dim dteCKO As String = DateTimeToString(DateControlForCheckOut.Date, SystemCulture).Split(" "c)(0) ''& " " & ddlAMPMcheckout.Text
        If (Request.QueryString("outT") <> Nothing) Then
            dteCKO = dteCKO & " " & Request.QueryString("outT")
        Else
            dteCKO = dteCKO & " " & ddlAMPMcheckout.Text
        End If
        ''rev:mia July 2 2013 - back to the office. 
        ''fixing problem in the addRental
        Dim dteCKI As String = DateTimeToString(DateControlForCheckIn.Date, SystemCulture).Split(" "c)(0) ''& " " & ddlAMPMcheckin.Text
        If (Request.QueryString("inT") <> Nothing) Then
            dteCKI = dteCKI & " " & Request.QueryString("inT")
        Else
            dteCKI = dteCKI & " " & ddlAMPMcheckin.Text
        End If

        Dim product As String = Aurora.Booking.Services.BookingProcess.GetProductID(PickerControlForProduct.DataId, MyBase.UserCode)

        Dim xmlstringAvailable As String = ""

        Dim qrytext As String = "select dbo.getCountryforlocation('" & branchOut & "',NULL,NULL)"
        Dim countrytext As String = Aurora.Common.Data.ExecuteScalarSQL(qrytext)


        Dim objExtension As New ExtensionParemeter
        objExtension.branchIn = branchIn
        objExtension.branchOut = branchOut
        objExtension.dteCKO = dteCKO
        objExtension.dteCKI = dteCKI
        objExtension.countrytext = countrytext
        objExtension.agentId = sagent
        objExtension.product = product
        objExtension.bkrid = Me.BkrID
        objExtension.hirePeriod = Me.txtHirePeriod.Text
        objExtension.Booid = Me.BooID
        objExtension.userCode = Me.UserCode
        objExtension.Newvhrid = VhrIDForExtension
        objExtension.Newbkrid = BkrIDForExtension


        AvailExtensionParameter = objExtension

        LogInformation("ImmediateAvailableChecking: " _
                       & vbCrLf & "BkrID: " & Me.BkrID _
                        & vbCrLf & " ,branchOut: " & branchOut _
                        & vbCrLf & " ,dteCKO: " & dteCKO _
                        & vbCrLf & " ,ampmCKO(PM): PM" _
                        & vbCrLf & " ,branchIn: " & branchIn _
                        & vbCrLf & " ,dteCKI: " & dteCKI _
                        & vbCrLf & " ,ampmCKI(PM): PM" _
                        & vbCrLf & " ,txtHirePeriod: " & txtHirePeriod.Text _
                        & vbCrLf & " ,BooID: " & BooID _
                        & vbCrLf & " ,CountryCode: " & countrytext _
                        & vbCrLf & " ,ProductID: " & productid _
                        & vbCrLf & " ,Product: " & product _
                        & vbCrLf & "PickerControlForProduct.text: " & PickerControlForProduct.Text & vbCrLf & vbCrLf)

        If Not productid.Equals(product) Then
            product = productid
        End If

        Try
            xmlstringAvailable = ManageAvailability.GetAvailabilityWithCost _
                                                  (Me.BkrID, _
                                                   branchOut, _
                                                   dteCKO, _
                                                   "PM", _
                                                   branchIn, _
                                                   dteCKI, _
                                                   "PM", _
                                                   txtHirePeriod.Text, _
                                                   product, _
                                                   "0", _
                                                   Me.BooID, _
                                                   "", _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   UserCode, _
                                                   "BookingProcess.aspx/ImmediateAvailableChecking", _
                                                   countrytext, _
                                                   String.Empty, _
                                                   True, _
                                                   Me.PromoCode) '' rev:mia Oct 16 2014 - addition of PromoCode

            Dim xmlAvail As New XmlDocument

            xmlAvail.LoadXml(xmlstringAvailable)

            Dim avs As String = xmlAvail.SelectSingleNode("//Data/AvailableVehicle/NumberOfVehicles").InnerText

            Dim blockingmessage As String = HidWarningMessage.Value

            If Not String.IsNullOrEmpty(blockingmessage) Then
                blockingmessage = String.Concat("<br/>Message: <b>", blockingmessage, "</b>")
            Else
                blockingmessage = String.Empty
            End If


            If avs.Equals("BYPASS") = True Then
                cbQuickAvail.Text = "Available: <b>YES</b></br>This vehicle is available as a <b><u>BYPASS</u></b>"
                ImmediateAvailableChecking = True
                IsVehicleReadyAndAvailable = True
            Else
                ''if avs is empty then possible that there is no record
                If String.IsNullOrEmpty(avs) Then
                    cbQuickAvail.Text = String.Concat("Available: <b>NO</b></br>", blockingmessage, "</br></br> Do you want to display other vehicles?")
                    IsVehicleReadyAndAvailable = False
                End If

                ''check if its a valid number
                Try
                    Dim intAvail As Integer = CInt(avs)
                    If intAvail > 1 Then
                        cbQuickAvail.Text = String.Concat("Available: <b>YES</b></br>There are <b>", avs, "</b> available vehicles.", blockingmessage)
                        ImmediateAvailableChecking = True
                        IsVehicleReadyAndAvailable = True
                    ElseIf intAvail = 1 Then
                        cbQuickAvail.Text = String.Concat("Available: <b>YES</b></br>There is <b>", avs, "</b> available vehicle.", blockingmessage)
                        ImmediateAvailableChecking = True
                        IsVehicleReadyAndAvailable = True
                    Else
                        cbQuickAvail.Text = String.Concat("Available: <b>NO</b>", blockingmessage, "</br></br> Do you want to display other vehicles?")
                        IsVehicleReadyAndAvailable = False
                    End If
                Catch ex As Exception
                    cbQuickAvail.Text = String.Concat("Available: <b>NO</b>", blockingmessage, "</br></br> Do you want to display other vehicles?")
                    IsVehicleReadyAndAvailable = False
                End Try
            End If


        Catch ex As Exception
            cbQuickAvail.Text = String.Concat("Available: <b>NO</b></br>Message: ", HidWarningMessage.Value, "</br></br> Do you want to display other vehicles?")
            IsVehicleReadyAndAvailable = False
            Logging.LogInformation("ImmediateAvailableChecking ERROR: ", xmlstringAvailable)
        End Try

        ''LogInformation("ImmediateAvailableChecking: " & xmlstringAvailable & vbCrLf & vbCrLf)
        Return ImmediateAvailableChecking
    End Function

    Protected Sub btnQuickAvail_Click(ByVal sender As Object, ByVal e As EventArgs)
        ''disabled the validation when its checking quick avail
        rfv_Surname.Enabled = False
        IsquickAvail = True

        HiddenFieldConfirmationBehaviorID.Value = cbQuickAvail.ConfirmationBehaviorID
        If Step1_IsRequiredFieldHaveValues() Then
            Step1_DisplayAvailability()
            ImmediateAvailableChecking()


            If IsVehicleReadyAndAvailable = False Then

                cbQuickAvail.Right_Button.Visible = True
                cbQuickAvail.Right_Button.Visible = True
                cbQuickAvail.HideOKButtonAndDisplaySearch(True)

                Dim objextenion As ExtensionParemeter = AvailExtensionParameter


                Dim sb As New StringBuilder
                sb.AppendFormat("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}", objextenion.branchIn, _
                                                                                 objextenion.branchOut, _
                                                                                 objextenion.dteCKO, _
                                                                                 objextenion.dteCKI, _
                                                                                 objextenion.countrytext, _
                                                                                 objextenion.agentId, _
                                                                                 objextenion.product, _
                                                                                 objextenion.bkrid, _
                                                                                 objextenion.hirePeriod, _
                                                                                 objextenion.Booid, _
                                                                                 objextenion.userCode, _
                                                                                 objextenion.Newvhrid, _
                                                                                 objextenion.Newbkrid)

                HiddenFieldVehicleExtension.Value = sb.ToString

            Else
                cbQuickAvail.HideOKButtonAndDisplaySearch(False)
                cbQuickAvail.Right_Button.Visible = False
                cbQuickAvail.Right_Button.Visible = False
            End If


            cbQuickAvail.Show()
        End If
        rfv_Surname.Enabled = True

    End Sub

    Private _isquickavail As Boolean
    Private Property IsquickAvail As Boolean
        Get
            Return _isquickavail
        End Get
        Set(ByVal value As Boolean)
            _isquickavail = value
        End Set
    End Property

#End Region

#Region "REV:Mia June 13 2011, Quick Avail Extensions (Fiest of Saint Anthony of Padua)"
    ''Private _IsVehicleReadyAndAvailable As Boolean
    Private Property IsVehicleReadyAndAvailable As Boolean
        Get
            Return ViewState("IsVehicleReadyAndAvailable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsVehicleReadyAndAvailable") = value
        End Set
    End Property


    Protected Sub cbQuickAvail_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cbQuickAvail.PostBack
        If leftButton And UserCode.ToLower = "ma2" Then
            'If IsVehicleReadyAndAvailable = False Then
            '    ''do nothing 
            'End If
        End If

    End Sub

    Private VhrIDForExtension As String
    Private BkrIDForExtension As String

    Sub ExtractNewVhrIDandBKRID(ByVal newxml As String)
        Dim xmlVhrIDandBKRID As New XmlDocument
        xmlVhrIDandBKRID.LoadXml(newxml)

        If Not xmlVhrIDandBKRID.SelectSingleNode("Op/D/Root/NewBookingRequest/VhrId") Is Nothing Then
            VhrIDForExtension = xmlVhrIDandBKRID.SelectSingleNode("Op/D/Root/NewBookingRequest/VhrId").InnerText
        End If

        If Not xmlVhrIDandBKRID.SelectSingleNode("Op/D/Root/NewBookingRequest/BkrId") Is Nothing Then
            BkrIDForExtension = xmlVhrIDandBKRID.SelectSingleNode("Op/D/Root/NewBookingRequest/BkrId").InnerText
        End If

        Session("ExtensionXML") = xmlVhrIDandBKRID.SelectSingleNode("Op/D/Root").OuterXml

    End Sub
#End Region

#Region "Extension Parameter"

    Private Class ExtensionParemeter
        Public branchOut As String
        Public branchIn As String
        Public dteCKO As String
        Public dteCKI As String
        Public product As String
        Public countrytext As String
        Public agentId As String
        Public bkrid As String
        Public hirePeriod As String
        Public Booid As String
        Public userCode As String
        Public Newvhrid As String
        Public Newbkrid As String
    End Class

    Protected Sub btnQuickAvailButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuickAvailButton.Click
        cbQuickAvail.Show()
        cbQuickAvail.Hide()
        sc.SetFocus(Me.PickerControlForProduct)
    End Sub
#End Region

#Region "rev:mia July 8 2013 - addrental for quick fix"
    Private Function GetLocation(locCode As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("Booking_AddRentalGetLocation", locCode)
    End Function
    Private Function GetProduct(PrdShortName As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("Booking_AddRentalGetProduct", PrdShortName)
    End Function

#End Region
    
#Region "rev:mia Sept.2 2013 - Customer Title from query"
    Sub PopulateTitle()
        customertitleDropdown.Items.Clear()
        customertitleDropdown.AppendDataBoundItems = True
        customertitleDropdown.Items.Add(New ListItem("", 0))
        customertitleDropdown.DataSource = Aurora.Booking.Services.BookingCustomer.GetCustomerTitle
        customertitleDropdown.DataTextField = "CodDesc"
        customertitleDropdown.DataValueField = "CodCode"
        customertitleDropdown.DataBind()
    End Sub
#End Region

#Region "REV:19NOV2014 - SLOT MANAGEMENT"


    Public Property NoOfBulkBookings As Integer
        Get
            Return ViewState("NoOfBulkBookings")
        End Get
        Set(value As Integer)
            ViewState("NoOfBulkBookings") = value
        End Set
    End Property


    Private _hasSlot As Boolean
    Private Property HasSlot As Boolean
        Get
            Return ViewState("_hasSlot")
        End Get
        Set(value As Boolean)
            ViewState("_hasSlot") = value
        End Set
    End Property



    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property


    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property


    Private Property SlotAvpID As String
        Get
            Return ViewState("_SlotAvpID")
        End Get
        Set(value As String)
            ViewState("_SlotAvpID") = value
        End Set
    End Property

    Private Property SlotAvpIDForKKandNN As String
        Get
            Return ViewState("_SlotAvpIDForKKandNN")
        End Get
        Set(value As String)
            ViewState("_SlotAvpIDForKKandNN") = value
        End Set
    End Property

    Protected Sub TimeSlotAvailability_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles TimeSlotAvailability.PostBack
        If (isSaveButton) Then
            SlotAvpID = param.Split("|")(0)
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)

            Logging.LogDebug("BookingProcess TimeSlotAvailability_PostBack", "SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
    End Sub

    Sub ClearSlotDataForNonKKandNN()
        Dim countNNorKK As Integer = 0
        Try
            countNNorKK = Me.xmlChosenVehicle.SelectNodes("SelectedVehicles/SelectedVehicle[Status='KK' or Status='NN']").Count

        Catch ex As Exception
            Logging.LogDebug("BookingProcess ERROR ClearSlotDataForNonKKandNN", "SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End Try

        If (countNNorKK = 0) Then
            SlotAvpID = ""
            SlotId = ""
            SlotDescription = ""
            Logging.LogDebug("BookingProcess AFTER ClearSlotDataForNonKKandNN", "SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        Else
            Logging.LogDebug("BookingProcess SKIP  ClearSlotDataForNonKKandNN because there is NN or KK selected", "CountNNorKK: " & countNNorKK & ", SlotAvpId: " & SlotAvpID & ",SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
        SlotAvpIDForKKandNN = ""
    End Sub



#End Region

End Class


