Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.Reservations.Services


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DuplicateRentalList)> _
Partial Class Booking_VehicleRequestResultList
    Inherits AuroraPage

#Region " Constant"
    Private Const SESSION_selectedVehicleList As String = "RS-SELVEHLST"

    ''rev:mia Feb.16
    Private Const QS_RS_VEHREQMGT As String = "VehicleRequestMgt.aspx?funCode=RS-VEHREQMGT"
    Private Const QS_RS_SELVEHLST As String = "SelectedVehicleList.aspx?funCode=RS-SELVEHLST"

#End Region

#Region " Variables"

    Protected xmlstring As String
    Protected mCostIndex As String
    Protected reader As XmlTextReader
    Protected sbglobal As StringBuilder
    Private SESSION_VehicleRequestMgt As String
    Protected HashProperties As New Hashtable
    Protected XMLdocVehicle As New XmlDocument
    Protected dictObject As New StringDictionary
    Private blnresubmit As Boolean = False
#End Region

#Region " Functions"

   
    ''transfered to BookingProcess
    Private Function GetProductID(ByVal productID As String) As String
        '@BrdCode varchar(64) = '',  
        '@ClsId  varchar(64) = '',  
        '@TypId  varchar(64) = '',  
        '--@ctyTravel varchar(64) = '',  
        '@sBranch varchar(64) = '',  
        '@ftrId  varchar(8000)='',  
        '@sCkiDate  VARCHAR(64),  
        '@userCode VARCHAR(64)  

        ''sfunctionName="RES_getProduct '" + param1 + "','" + param2 + "','" + param3 + "','" + param4 + "','" + param5 + "','" + param6 + "','" + param7 + "','" + loginUserCode + "'" 
        ''Return Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", productID, "", "", "", "", "", "", MyBase.UserCode)
        Dim xmldoc As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getProduct", productID, "", "", "", "", "", "", MyBase.UserCode)
        If xmldoc.OuterXml <> "<data></data>" Then
            Return xmldoc.SelectSingleNode("data/PRODUCT/Id").InnerText
        Else
            Return productID
        End If

    End Function

    ''transfered to utility
    Private Function ParseDateAsDayWord(ByVal strDate As String) As String
        Try

            Dim aryDay As String() = strDate.Split("/")
            Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            Return dt.ToString("dddd").Substring(0, 3)
        Catch
        End Try
    End Function

    ''transfered to BookingProcess
    Function CalculateRequestPeriod(ByVal cko As String, ByVal ckoampm As String, ByVal cki As String, ByVal ckiampm As String, ByVal hireperiod As String, ByVal daysmode As String, ByVal dataadd As Integer) As String

        Try
            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                       cko, _
                                                       ckoampm, _
                                                       cki, _
                                                       ckiampm, _
                                                       hireperiod, _
                                                       daysmode, _
                                                       dataadd)
            Return xmlstring

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Function

    ''transfered to BookingProcess
    Function ExtractGetAvailXML(ByVal xmlString As String) As String
        Dim sb As New StringBuilder
        Dim xmldoc As New XmlDocument

        Try

       
            xmldoc.LoadXml(xmlString)

            Dim nodes As XmlNodeList = xmldoc.GetElementsByTagName("Detail")
            sb.Append("<Package>")
            For Each node As XmlNode In nodes
                If node.Name = "Detail" Then
                    sb.Append("<Detail>")
                    For Each childnode As XmlNode In node.ChildNodes
                        Select Case childnode.Name
                            Case "pkg"
                                sb.AppendFormat("<pkg_id>{0}</pkg_id>", childnode.Attributes(0).Value)
                                sb.AppendFormat("<pkg_pkgid>{0}</pkg_pkgid>", childnode.Attributes(1).Value)
                                sb.AppendFormat("<pkg_pkgcom>{0}</pkg_pkgcom>", childnode.Attributes(2).Value)
                                sb.AppendFormat("<pkg_text>{0}</pkg_text>", childnode.InnerText)
                            Case "pp"
                                sb.AppendFormat("<pp>{0}</pp>", childnode.InnerText)
                            Case "br"
                                sb.AppendFormat("<br>{0}</br>", childnode.InnerText)
                            Case "uom"
                                sb.AppendFormat("<uom>{0}</uom>", childnode.InnerText)
                            Case "cur"
                                sb.AppendFormat("<cur>{0}</cur>", childnode.InnerText)
                            Case "Valqty"
                                sb.AppendFormat("<val_tc>{0}</val_tc>", childnode.ChildNodes(0).ChildNodes(0).Attributes(0).Value)
                                sb.AppendFormat("<val_pp>{0}</val_pp>", childnode.ChildNodes(0).ChildNodes(0).Attributes(1).Value)
                                sb.AppendFormat("<val_pd>{0}</val_pd>", childnode.ChildNodes(0).ChildNodes(0).Attributes(2).Value)
                                sb.AppendFormat("<val_vc>{0}</val_vc>", childnode.ChildNodes(0).ChildNodes(0).Attributes(3).Value)
                                sb.AppendFormat("<val_vdn>{0}</val_vdn>", childnode.ChildNodes(0).ChildNodes(0).Attributes(4).Value)
                                sb.AppendFormat("<val_vdg>{0}</val_vdg>", childnode.ChildNodes(0).ChildNodes(0).Attributes(5).Value)
                                sb.AppendFormat("<qty>{0}</qty>", childnode.ChildNodes(0).ChildNodes(1).InnerText)

                        End Select
                    Next
                    sb.Append("</Detail>")
                End If
            Next
            sb.Append("</Package>")
            Return sb.ToString

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Function

    Function RetrieveValueFromGrid() As String
        Dim mtotalqty As Integer = CInt(Me.TotalQTY)
        Dim sb As New StringBuilder
        Dim txtvalue As TextBox
        Try

        
            For Each griditem As GridViewRow In GridView1.Rows
                If griditem.RowType = DataControlRowType.DataRow Then
                    sb.Append("<Avail>")
                    sb.AppendFormat("<AvvId>{0}</AvvId>", Me.veh_ID)
                    sb.AppendFormat("<PkgId>{0}</PkgId>", Me.PKGid)

                    txtvalue = CType(griditem.FindControl("txtvalue"), TextBox)
                    If txtvalue IsNot Nothing Then
                        If txtvalue.Text <> "" Then
                            If Char.IsDigit(txtvalue.Text) Then
                                mtotalqty += CInt(txtvalue.Text)
                                sb.AppendFormat("<Qty>{0}</Qty>", txtvalue.Text)
                            Else
                                sb.AppendFormat("<Qty>{0}</Qty>", "")
                            End If
                        Else
                            sb.AppendFormat("<Qty>{0}</Qty>", "")
                        End If
                    Else
                        sb.AppendFormat("<Qty>{0}</Qty>", "")
                    End If
                    sb.Append("</Avail>")
                End If
            Next
            Me.TotalQTY = mtotalqty
            Return sb.ToString
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Function

#End Region

#Region " private Sub"

    Private Function AttachedEventHandler(ByVal controlName As Control) As String
        Dim sb As New StringBuilder
        With sb
            .Append("var obj = document.getElementById('" & controlName.ClientID & "');")
            .Append("var fnattach=function(){alert('hell0');};")
            .Append("obj.attachEvent('onchange','fnattach();');")

        End With
        Return sb.ToString
    End Function

    Private Sub CalculateRequestPeriod()
        Try


            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                       Me.DateControlForCKO.Text, _
                                                       IIf(Me.ddlamppmCKO.Text.Contains("AM"), "AM", "PM"), _
                                                       Me.DateControlForCKI.Text, _
                                                       IIf(Me.ddlAMPMcki.Text.Contains("AM"), "AM", "PM"), _
                                                       HirePeriod, _
                                                       Me.ddldays.SelectedValue, _
                                                       0)


            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xmlstring)
            Dim xmlelem As XmlElement = xmldoc.DocumentElement
            Me.DateControlForCKO.Text = xmlelem.SelectSingleNode("//Root/D1").InnerText
            Me.DateControlForCKI.Text = xmlelem.SelectSingleNode("//Root/D2").InnerText
            Me.txtHirePeriod.Text = xmlelem.SelectSingleNode("//Root/Dif").InnerText
            xmldoc = Nothing
            Me.SetInformationMessage("Please 'Resubmit' to update table")
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub


    Sub ToggleButton(ByVal bln As Boolean)
        If bln = True Then
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewBookingRequest.Enabled = True
            Me.btnOneDayBack.Enabled = True
            Me.btnOneDayForward.Enabled = True
            Me.btnResubmit.Enabled = True
            Me.btnSaveNext.Enabled = True
            Me.btnSubmit.Enabled = True
        Else
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewBookingRequest.Enabled = True
            Me.btnOneDayBack.Enabled = False
            Me.btnOneDayForward.Enabled = False
            Me.btnResubmit.Enabled = False
            Me.btnSaveNext.Enabled = False
            Me.btnSubmit.Enabled = False
        End If
    End Sub

    Sub NavigationTab()


        Dim index As Integer = 1

        ''txtBookingRef_onchange
        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).TabIndex = index
        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).Focus()
        ''   CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).Attributes.Add("onchange", "MessageOnchange();")


        CType(Me.PickerControlForBranchCKO.FindControl("pickerTextBox"), TextBox).TabIndex = index
        CType(Me.DateControlForCKO.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        ''CType(Me.DateControlForCKO.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "MessageOnchange();")
        Me.ddlamppmCKO.TabIndex = index + 1



        CType(Me.PickerControlForBranchCKI.FindControl("pickerTextBox"), TextBox).TabIndex = index
        '' CType(Me.DateControlForCKI.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "MessageOnchange();")
        CType(Me.DateControlForCKI.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        Me.ddlAMPMcki.TabIndex = index + 1

        Me.txthireperiod.TabIndex = index + 1
        Me.ddldays.TabIndex = index + 1

        Me.btnResubmit.TabIndex = index + 1
        Me.btnOneDayBack.TabIndex = index + 1
        Me.btnOneDayForward.TabIndex = index + 1

        Me.rdList.TabIndex = index + 1

        Me.btnSubmit.TabIndex = index + 1
        Me.btnSaveNext.TabIndex = index + 1
        Me.btnModifyBookingRequest.TabIndex = index + 1
        Me.btnNewBookingRequest.TabIndex = index + 1

        '' Me.ddlAMPMcki.Attributes.Add("onchange", "MessageOnchange();")
        '' Me.ddlamppmCKO.Attributes.Add("onchange", "MessageOnchange();")

    End Sub

    ''rev:mia Feb.16
    Sub RemoveSessionForNBR()
        If Session(SESSION_VehicleRequestMgt) IsNot Nothing Then
            Session.Remove(SESSION_VehicleRequestMgt)
        End If
    End Sub

    Sub AddSessionContent()
        ''rev:mia Feb.16
        If dictObject IsNot Nothing Then
            dictObject.Clear()
        End If

        With dictObject
            .Add("bkrid", Me.BKRid)
            .Add("booid", Me.BooID)
            .Add("bpdid", String.Empty)
            .Add("butSel", Me.ButtonSelection)

            If CType(Me.HashProperties("VHRid"), String) <> "" Then
                If Me.VHRid.Equals(CType(Me.HashProperties("VHRid"), String)) = True Then
                    dictObject("vhrId") = Me.VHRid
                Else
                    dictObject("vhrId") = CType(Me.HashProperties("VHRid"), String)
                End If
            Else
                dictObject("vhrId") = Me.VHRid
            End If
            dictObject("Contact") = Me.hidContactID.Value
        End With
        
        Session.Add(SESSION_VehicleRequestMgt, dictObject)

    End Sub

    ''rev:mia Feb.16
    Sub GetSessionForMBR()
        If Not String.IsNullOrEmpty(Request.QueryString("funcode")) Then
            If Request.QueryString("funcode").Equals("RS-VEHREQRES") Then
                dictObject = CType(Session(SESSION_VehicleRequestMgt), StringDictionary)
                If dictObject IsNot Nothing Then
                    Me.BKRid = dictObject("bkrid")
                    Me.BooID = dictObject("booid")
                    ''Me.BPDid = dictObject("bpdid")
                    Me.ButtonSelection = dictObject("butSel")
                    Me.VHRid = dictObject("vhrid")
                    Me.hidContactID.Value = dictObject("Contact")

                End If
            End If
        End If
    End Sub

    Sub getDisplayAvailableVehiclesWithCost()
        
        Try

        
            Dim rntid As String = ""
            Dim butSel As String = "Display Availability With Cost"

            ''uncomment this part
            Me.GetSessionForMBR()

           
            xmlstring = Aurora.Common.Data.ExecuteScalarSP("RES_getDisplayAvailableVehiclesWithCostOSLO", Me.VHRid, Me.BooID, rntid, MyBase.UserCode, Me.ButtonSelection)

            Dim xmlTemp As New XmlDocument
            xmlTemp.LoadXml(xmlstring)
            If Not xmlTemp.SelectSingleNode("VehicleRequest/bookingnum") Is Nothing Then
                If xmlTemp.SelectSingleNode("VehicleRequest/bookingnum").InnerText <> "" Then
                    Me.SetValueLink(Trim(" :Booking " & xmlTemp.SelectSingleNode("VehicleRequest/bookingnum").InnerText), "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & xmlTemp.SelectSingleNode("VehicleRequest/bookingnum").InnerText)
                Else
                    Me.SetValueLink("", "")
                End If

                xmlTemp = Nothing
            End If
            

            reader = New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(reader)


            If ds.Tables("VehicleRequest").Rows.Count = -1 Then
                Exit Sub
            End If

            Me.GetTable(ds)

            ds = Nothing
            Me.BindGrid()

        Catch ex As Exception

            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub BindGrid(ByVal mBKRid As String, ByVal mBranchCKO As String, ByVal mBranchCKOwhen As String, ByVal mCOampm As String, ByVal mBranchCKI As String, ByVal mBranchCKIwhen As String, ByVal mCIampm As String, ByVal mHirePeriod As String, ByVal mProductID As String, ByVal mBooID As String, ByVal mPrgmName As String, ByVal mCityCode As String)
        Dim xmldoc As New XmlDocument
        Try
            xmlstring = ManageAvailability.GetAvailabilityWithCost _
                                                    (mBKRid, _
                                                     mBranchCKO, _
                                                     mBranchCKOwhen, _
                                                     mCOampm, _
                                                     mBranchCKI, _
                                                     mBranchCKIwhen, _
                                                     mCIampm, _
                                                     mHirePeriod, _
                                                     mProductID, _
                                                     "0", _
                                                     mBooID, _
                                                     "", _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     String.Empty, _
                                                     UserId, _
                                                     mPrgmName, _
                                                     IIf(String.IsNullOrEmpty(mCityCode), MyBase.CountryCode, mCityCode))


            xmlstring = xmlstring.Replace("<Data><data>", "<Data>")
            xmlstring = xmlstring.Replace("</data></Data>", "</Data>")



            xmldoc.LoadXml(xmlstring)

            ''select first error
            If Not (xmldoc.DocumentElement.SelectSingleNode("Error/ErrDescription/Aurora/Error/Description") Is Nothing) Then
                Dim errorMsg As String = xmldoc.DocumentElement.SelectSingleNode("Error/ErrDescription/Aurora/Error/Description").InnerText
                If Not String.IsNullOrEmpty(errorMsg) Then
                    Throw New Exception(errorMsg)
                End If
            Else
                If xmlstring.Equals("<data></data>") Or xmlstring.Equals("<Data></Data>") Then
                    Throw New Exception("Invalid XML: &lt;Data&gt;&lt;/Data&gt;")
                End If
            End If


            Me.ByPass = xmldoc.SelectSingleNode("//Data/Forward[@BP]").Attributes(0).Value
            Me.ProductName = xmldoc.SelectSingleNode("//Data/Forward/Vehicle/veh").InnerText
            Me.Availabilitynumber = xmldoc.SelectSingleNode("//Data/Forward/Avail/av").InnerText
            Me.CriteriaMatch = xmldoc.SelectSingleNode("//Data/Forward/Crit/cr").InnerText.Trim
            Me.Package = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pkg").InnerText
            Me.PP = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pp").InnerText
            Me.Brand = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/br").InnerText

            Me.UOM = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/uom").InnerText
            If mCostIndex = Nothing Then mCostIndex = "@tc"

            Me.Val = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[" & mCostIndex & "]").Attributes(0).Value
            Me.TCvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@tc]").Attributes(0).Value
            Me.PPvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@pp]").Attributes(1).Value
            Me.PDvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@pd]").Attributes(2).Value
            Me.VCvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vc]").Attributes(3).Value
            Me.VDNvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vdn]").Attributes(4).Value
            Me.VDGvalue = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/Valqty/Value/val[@vdg]").Attributes(5).Value

            Me.CUR = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/cur").InnerText

            ''use in getxmlfor save
            Me.PKGid = xmldoc.SelectSingleNode("//Data/Forward/Package/Detail/pkg[@pkgid]").Attributes(1).Value
            Me.veh_ID = xmldoc.SelectSingleNode("//Data/Forward/Vehicle/veh[@ID]").Attributes(0).Value

            Dim ds As New DataSet
            xmlstring = ExtractGetAvailXML(xmlstring)
            reader = New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            reader.ReadOuterXml()
            ds.ReadXml(reader)


            Dim view As New DataView(ds.Tables(0))
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC "
            End If
            Session("dssorted") = ds
            view.Sort = ViewState("sort")
            GridView1.DataSource = view

            ''GridView1.DataSource = ds
            GridView1.DataBind()
            trdataTableDefault.Visible = False


        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
            Me.trdataTableDefault.Visible = True
            ToggleButton(False)
            xmldoc = Nothing
        End Try
    End Sub

    Sub BindGridSorted()
        If CType(Session("dssorted"), DataSet) IsNot Nothing Then
            Dim view As New DataView(CType(Session("dssorted"), DataSet).Tables(0))
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC "
            End If
            Session("dssorted") = CType(Session("dssorted"), DataSet)
            view.Sort = ViewState("sort")
            GridView1.DataSource = view
            GridView1.DataBind()
        End If
    End Sub

    Sub BindGrid()
        Try
            Dim prodID As String = ""
            If blnresubmit = True Then
                prodID = Me.GetProductID(Me.PickerControlForProduct.DataId)
            Else
                prodID = Me.ProductID
            End If
            Dim ampmCKO As String = IIf(Me.ddlamppmCKO.Text.Contains("AM"), "AM", "PM")
            Dim ampmCKI As String = IIf(Me.ddlAMPMcki.Text.Contains("AM"), "AM", "PM")
            ''mia:rev may 15
            Me.BindGrid(Me.BKRid, Me.BranchCKO, Me.DateControlForCKO.Text, ampmCKO, Me.BranchCKI, Me.DateControlForCKI.Text, ampmCKI, Me.txthireperiod.Text, _
                                           prodID, Me.BooID, Me.PrgmName, Me.CityCode)

            '' Me.BindGrid(Me.BKRid, Me.BranchCKO, Me.DateControlForCKO.Text, Me.ddlamppmCKO.Text, Me.BranchCKI, Me.DateControlForCKI.Text, Me.ddlAMPMcki.Text, Me.txthireperiod.Text, _
            ''                    prodID, Me.BooID, Me.PrgmName, Me.CityCode)

            blnresubmit = False

            ''orig
            ''Me.BindGrid(Me.BKRid, Me.BranchCKO, Me.DateControlForCKO.Text, Me.ddlamppmCKO.Text, Me.BranchCKI, Me.DateControlForCKI.Text, Me.ddlAMPMcki.Text, Me.txthireperiod.Text, _
            ''                    Me.ProductID, Me.BooID, Me.PrgmName, Me.CityCode)
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
        
    End Sub


    Sub Resubmit(Optional ByVal withMsg As Boolean = False)
        Dim sb As New StringBuilder
        Try

            blnresubmit = True
            With sb
                .Append("<Root>")
                .AppendFormat("<BkrId>{0}</BkrId>", Me.BKRid)
                .AppendFormat("<vhrId>{0}</vhrId>", Me.VHRid)
                .AppendFormat("<choBranch>{0}</choBranch>", Me.BranchCKO)
                .AppendFormat("<choDate>{0}</choDate>", (Me.DateControlForCKO.Text & " " & ParsetimeToString(Me.ddlamppmCKO.Text.ToString)))
                .AppendFormat("<choAmPm>{0}</choAmPm>", IIf(Me.ddlamppmCKO.Text.Contains("AM"), "AM", "PM"))
                .AppendFormat("<chiBranch>{0}</chiBranch>", Me.BranchCKI)
                .AppendFormat("<chiDate>{0}</chiDate>", (Me.DateControlForCKI.Text & " " & ParsetimeToString(Me.ddlAMPMcki.Text.ToString)))
                .AppendFormat("<chiAmPm>{0}</chiAmPm>", IIf(Me.ddlAMPMcki.Text.Contains("AM"), "AM", "PM"))
                .AppendFormat("<prdId>{0}</prdId>", GetProductID(Me.ProductID))
                .AppendFormat("<hirPrd>{0}</hirPrd>", Me.txthireperiod.Text)
                .Append("</Root>")
            End With

            Dim strValue As String = SelectedVehicle.manageDuplicateVehicleRequest(sb.ToString)
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(strValue)
            If xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "False" Then
                HashProperties("VHRid") = xmldoc.SelectSingleNode("//Root/Data").InnerText
                Me.VHRid = xmldoc.SelectSingleNode("//Root/Data").InnerText
                Me.BindGrid()
                If withMsg Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Your revised request has been resubmited")
                End If
            Else
                MyBase.SetShortMessage(AuroraHeaderMessageType.Error, xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText)
            End If
            xmldoc = Nothing

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub OneDayBackAndForward(ByVal isOneDayBack As Boolean)

        Dim inOnedayback As Integer
        Try

        
            If isOneDayBack = False Then
                inOnedayback = -1
            Else
                inOnedayback = 1
            End If
            Dim xmlstring As String = CalculateRequestPeriod(Me.DateControlForCKO.Text, _
                        IIf(Me.ddlamppmCKO.Text.Contains("AM"), "AM", "PM"), _
                        Me.DateControlForCKI.Text, _
                        IIf(Me.ddlAMPMcki.Text.Contains("AM"), "AM", "PM"), _
                        Me.txthireperiod.Text, _
                        Me.ddldays.SelectedValue, inOnedayback)

            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xmlstring)

            Me.DateControlForCKO.Text = xmldoc.SelectSingleNode("//Root/D1").InnerText
            Me.DateControlForCKI.Text = xmldoc.SelectSingleNode("//Root/D2").InnerText
            Me.txthireperiod.Text = xmldoc.SelectSingleNode("//Root/Dif").InnerText
            xmldoc = Nothing
            Resubmit(True)
            ''CalculateRequestPeriod()
            Me.lblckiDay.Text = ParseDateAsDayWord(Me.DateControlForCKI.Text)
            Me.lblckoDay.Text = ParseDateAsDayWord(Me.DateControlForCKO.Text)
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub GetXMLforSave(ByVal navigate As Boolean)

        Dim sb As New StringBuilder
        Try

        

            sb.Append("<Root>")
            sb.Append(RetrieveValueFromGrid)
            sb.AppendFormat("<UserCode>{0}</UserCode>", UserCode)
            sb.AppendFormat("<PrgName>{0}</PrgName>", Me.ProgramID)
            sb.Append("</Root>")

            Dim objXML As New XmlDocument
            objXML.LoadXml(ManageAvailability.manageAvailableVehicle(Me.VHRid, sb.ToString))
            Dim strMessage As String = objXML.SelectSingleNode("//Root/Error/ErrDescription").InnerText
            Dim strSplitMsg As String() = strMessage.Split("^")
            If strMessage <> "" Then
                If objXML.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "false" Then
                    MyBase.SetInformationShortMessage(strSplitMsg(1))
                Else
                    MyBase.SetInformationShortMessage(strMessage)
                End If
            End If


            ''BindGrid()

            If (navigate AndAlso Me.TotalQTY <> 0 AndAlso Me.TotalQTY <> "") Then
                Me.AddSessionContent()
                Response.Redirect(QS_RS_SELVEHLST)
            Else
                If navigate = True Then
                    MyBase.SetInformationShortMessage("Please select the vehicle")
                End If
                Me.AddSessionContent()
            End If
            objXML = Nothing

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    ''transfer to utility
    Function ParsetimeToString(ByVal starttime As DateTime) As String
        Dim strTime As String = (Trim(starttime.ToString.Remove(0, starttime.ToString.IndexOf(" ") + 1)))
        Dim index As Integer = strTime.LastIndexOf(":")
        strTime = strTime.Remove(index, 3)
        If strTime.Contains("a.m") Then
            strTime = strTime.Replace("a.m.", "AM")
        Else
            strTime = strTime.Replace("p.m.", "PM")
        End If
        Return strTime
    End Function

    ''transfer to utility
    Function ParsetimeToString(ByVal starttime As String) As String
        Dim strtime As String
        strtime = starttime
        If strtime.Trim = "AM" Then Return ""
        If strtime.Trim = "PM" Then Return ""

        If strtime.Contains("AM") Then
            strtime = strtime.Replace("AM", "").Trim
        Else
            strtime = strtime.Replace("PM", "").Trim
        End If
        Return strtime
    End Function

    ''transfer to Bookingprocess
    Private Sub SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, Optional ByVal ddlControl As DropDownList = Nothing)
        Try

            Dim index As Integer
           

            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If



            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, MyBase.UserCode)
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                MyBase.SetInformationShortMessage("No available time for location code '" & locationCode & "' and userCode '" & MyBase.UserCode & "'")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(0))
            Dim endtime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(1))

            Dim strtime As String = ParsetimeToString(startTime)

            ddlControl.Items.Clear()
            ddlControl.Items.Add(strtime)

            Do

                startTime = startTime.AddMinutes(15)
                strtime = ParsetimeToString(startTime)
                ddlControl.Items.Add(strtime)

            Loop While startTime <> endtime
            ddlControl.SelectedIndex = index
        Catch ex As Exception
            Me.SetErrorMessage("SelectLocationBasedOnCodeAndUser -> " & ex.Message)
        End Try
    End Sub

    Sub GetTable(ByVal ds As DataSet)
        Try

        
            Me.PickerControlForProduct.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productname") ''ds.Tables(0).Rows(0).Item("productname")
            Me.PickerControlForProduct.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid") 'ds.Tables(0).Rows(0).Item("productid")
            Me.txthireperiod.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod") ''ds.Tables(0).Rows(0).Item("hireperiod")
            Me.HirePeriod = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod") ''ds.Tables(0).Rows(0).Item("hireperiod")
            Me.ddldays.SelectedValue = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hiretype") ''ds.Tables(0).Rows(0).Item("hiretype")
            Me.DaysMode = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hiretype") ''ds.Tables(0).Rows(0).Item("hiretype")

            SelectLocationBasedOnCodeAndUser(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch"), Me.ddlamppmCKO)
            Me.PickerControlForBranchCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch") ''ds.Tables(0).Rows(0).Item("checkoutbranch")


            Me.lblckoDay.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).coDw") ''ds.Tables(0).Rows(0).Item("coDw")
            Me.DateControlForCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate") ''ds.Tables(0).Rows(0).Item("checkoutdate")
            Me.BranchCKOwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate") ''ds.Tables(0).Rows(0).Item("checkoutdate")

            Dim ampmCKO As String
            If DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString.Contains("00:00") Then
                ampmCKO = Me.ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")))
                ampmCKO = ampmCKO.Replace("00:00 ", "") ''Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString.Replace("00:00 ", ""))
            Else
                ampmCKO = Me.ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")))
                ''ampmCKO = Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString)
            End If
            Me.ddlamppmCKO.Text = IIf(ampmCKO.StartsWith("0"), ampmCKO.Remove(0, 1), ampmCKO) ''DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm")
            If ampmCKO = "12:00 AM" Then
                ddlamppmCKO.SelectedIndex = 0
            ElseIf ampmCKO = "12:00 PM" Then
                ddlamppmCKO.SelectedIndex = 1
            End If

            SelectLocationBasedOnCodeAndUser(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch"), Me.ddlAMPMcki)
            Me.PickerControlForBranchCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch") ''ds.Tables(0).Rows(0).Item("checkinbranch")

            Me.lblckiDay.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).ciDw") ''ds.Tables(0).Rows(0).Item("ciDw")
            Me.DateControlForCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate") ''ds.Tables(0).Rows(0).Item("checkindate")
            Me.BranchCKIwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate") ''ds.Tables(0).Rows(0).Item("checkindate")

            Dim ampmCKI As String
            If DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString.Contains("00:00") Then
                ampmCKI = Me.ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm")))
                ampmCKI = ampmCKI.Replace("00:00 ", "") ''DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString.Replace("00:00 ", "")
            Else
                ''ampmCKI = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString
                ampmCKI = Me.ParsetimeToString(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm")))
            End If
            Me.ddlAMPMcki.Text = IIf(ampmCKI.StartsWith("0"), ampmCKI.Remove(0, 1), ampmCKI)
            If ampmCKI = "12:00 AM" Then
                ddlAMPMcki.SelectedIndex = 0
            ElseIf ampmCKI = "12:00 PM" Then
                ddlAMPMcki.SelectedIndex = 1
            End If


            Me.BKRid = DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid") ''ds.Tables(0).Rows(0).Item("bkrid")
            Me.CostDef = DataBinder.Eval(ds, "Tables(0).DefaultView(0).costdef") ''ds.Tables(0).Rows(0).Item("costdef")
            Me.BranchCKO = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch") ''ds.Tables(0).Rows(0).Item("checkoutbranch")
            Me.BranchCKOwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate") ''ds.Tables(0).Rows(0).Item("checkoutdate")
            Me.COampm = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm") ''ds.Tables(0).Rows(0).Item("checkoutampm")
            Me.VHRid = DataBinder.Eval(ds, "Tables(0).DefaultView(0).vhrId") ''ds.Tables(0).Rows(0).Item("vhrId")
            Me.BranchCKI = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch") ''ds.Tables(0).Rows(0).Item("checkinbranch")
            Me.BranchCKIwhen = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate") ''ds.Tables(0).Rows(0).Item("checkindate")
            Me.CIampm = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm") ''ds.Tables(0).Rows(0).Item("checkinampm")
            Me.HirePeriod = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod") ''ds.Tables(0).Rows(0).Item("hireperiod")
            Me.ProductID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid") ''ds.Tables(0).Rows(0).Item("productid")
            Me.CityCode = DataBinder.Eval(ds, "Tables(0).DefaultView(0).rntCty") ''ds.Tables(0).Rows(0).Item("rntCty")
            Dim userid As String = MyBase.UserId
            Me.ProgramID = "vehiclerequestmgt.aspx"
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

#End Region

#Region " Private Properties"
    Private ReadOnly Property HirePeriodDiff() As String
        Get
            Try
                Return DateDiff(DateInterval.Day, CDate(Me.DateControlForCKO.Text), CDate(Me.DateControlForCKI.Text)).ToString
            Catch ex As Exception
                MyBase.AddErrorMessage(ex.Message)
            End Try
        End Get
    End Property
 
    Private Property ButtonSelection() As String
        Get

            ''Display Availability With Cost
            If CType(HashProperties("ButtonSelection"), String) Is Nothing Then
                Return "Display Availability With Cost"
            Else
                Return CType(HashProperties("ButtonSelection"), String)
            End If

        End Get
        Set(ByVal value As String)
            HashProperties("ButtonSelection") = value
        End Set
    End Property

    Private Property TotalQTY() As String
        Get
            Return CType(HashProperties("TotalQTY"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("TotalQTY") = value
        End Set
    End Property

    Private Property veh_ID() As String
        Get
            Return CType(HashProperties("veh_ID"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("veh_ID") = value
        End Set
    End Property

    Private Property DaysMode() As String
        Get
            Return CType(HashProperties("DaysMode"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("DaysMode") = value
        End Set
    End Property

    Private mqty As String
    Private Property Qty() As String
        Get
            Return CType(HashProperties("Qty"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Qty") = value
        End Set
    End Property

    Private mpkgID As String
    Private Property PKGid() As String
        Get
            'Return mpkgID
            Return CType(HashProperties("PKGid"), String)
        End Get
        Set(ByVal value As String)
            'mpkgID = value
            HashProperties("PKGid") = value
        End Set
    End Property

    Private mvc As String
    Private Property VCvalue() As String
        Get
            Return CType(HashProperties("VCvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("VCvalue") = value
        End Set
    End Property

    Private mtc As String
    Private Property TCvalue() As String
        Get
            Return CType(HashProperties("TCvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("TCvalue") = value
        End Set
    End Property

    Private mppvalue As String
    Private Property PPvalue() As String
        Get
            Return CType(HashProperties("PPvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("PPvalue") = value
        End Set
    End Property

    Private mpd As String
    Private Property PDvalue() As String
        Get
            Return CType(HashProperties("PDvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("PDvalue") = value
        End Set
    End Property

    Private mvdn As String
    Private Property VDNvalue() As String
        Get
            Return CType(HashProperties("VDNvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("VDNvalue") = value
        End Set
    End Property

    Private mvdg As String
    Private Property VDGvalue() As String
        Get
            Return CType(HashProperties("VDGvalue"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("VDGvalue") = value
        End Set
    End Property

    Private mBooid As String
    Private Property BooID() As String
        Get
            Return CType(HashProperties("BooID"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("BooID") = value
        End Set
    End Property

    Private mProgramID As String
    Private Property ProgramID() As String
        Get
            Return CType(HashProperties("ProgramID"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("ProgramID") = value
        End Set
    End Property

    Private mcoampm As String
    Private Property COampm() As String
        Get
            Return CType(HashProperties("COampm"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("COampm") = value
        End Set
    End Property

    Private mciampm As String
    Private Property CIampm() As String
        Get
            Return CType(HashProperties("CIampm"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("CIampm") = value
        End Set
    End Property

    Private mProductID As String
    Private Property ProductID() As String
        Get
            If String.IsNullOrEmpty(mProductID) Then
                Return Me.PickerControlForProduct.DataId
            Else
                If String.IsNullOrEmpty(CType(HashProperties("ProductID"), String)) Then
                    Return mProductID
                Else
                    Return CType(HashProperties("ProductID"), String)
                End If
            End If
        End Get
        Set(ByVal value As String)
            mProductID = value
            HashProperties("ProductID") = value
        End Set
    End Property

    Private mCityCode As String
    Private Property CityCode() As String
        Get
            Return CType(HashProperties("CityCode"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("CityCode") = value
        End Set
    End Property

    Private mBranchCKO As String
    Protected Property BranchCKO() As String
        Get
            If String.IsNullOrEmpty(mBranchCKO) Then
                If Me.PickerControlForBranchCKO.Text <> "" Then
                    If Me.PickerControlForBranchCKO.Text.IndexOf("-") <> -1 Then
                        Return Me.PickerControlForBranchCKO.Text.Substring(0, Me.PickerControlForBranchCKO.Text.IndexOf("-")).TrimEnd
                    Else
                        Return Me.PickerControlForBranchCKO.Text
                    End If

                Else
                    Return ""
                End If

            Else
                Return mBranchCKO
            End If

        End Get
        Set(ByVal value As String)
            If value.Contains("-") Then
                mBranchCKO = value.Substring(0, value.IndexOf("-"))
            Else
                mBranchCKO = value
            End If
        End Set
    End Property

    Private mBranchCKI As String
    Private Property BranchCKI() As String
        Get

            If String.IsNullOrEmpty(mBranchCKI) Then
                If Me.PickerControlForBranchCKI.Text <> "" Then
                    If Me.PickerControlForBranchCKI.Text.IndexOf("-") <> -1 Then
                        Return Me.PickerControlForBranchCKI.Text.Substring(0, Me.PickerControlForBranchCKI.Text.IndexOf("-")).TrimEnd
                    Else
                        Return Me.PickerControlForBranchCKI.Text
                    End If

                Else
                    Return ""
                End If

            Else
                Return mBranchCKI
            End If
        End Get
        Set(ByVal value As String)
            If value.Contains("-") Then
                mBranchCKI = value.Substring(0, value.IndexOf("-"))
            Else
                mBranchCKI = value
            End If
        End Set
    End Property

    Protected Property BKRid() As String
        Get
            Return Me.HidBKRid.Value
        End Get
        Set(ByVal value As String)
            Me.HidBKRid.Value = value
        End Set
    End Property

    Private Property VHRid() As String
        Get
            Return Me.HidVHRid.Value
        End Get
        Set(ByVal value As String)
            Me.HidVHRid.Value = value
        End Set
    End Property

    Private mCostDef As String
    Private Property CostDef() As String
        Get
            Return CType(HashProperties("CostDef"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("CostDef") = value
        End Set
    End Property

    Private mBranchCKOwhen As String
    Private Property BranchCKOwhen() As String
        Get
            Return CType(HashProperties("BranchCKOwhen"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("BranchCKOwhen") = value
        End Set
    End Property

    Private mBranchCKIwhen As String
    Private Property BranchCKIwhen() As String
        Get
            Return CType(HashProperties("BranchCKIwhen"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("BranchCKIwhen") = value
        End Set
    End Property

    Private mHirePeriod As String
    Private Property HirePeriod() As String
        Get
            Return CType(HashProperties("HirePeriod"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("HirePeriod") = value
        End Set
    End Property

    Private Property RentalID() As String
        Get
            Return HidRentalID.Value
        End Get
        Set(ByVal value As String)
            HidRentalID.Value = value
        End Set
    End Property
#End Region

#Region " Protected Properties "

    Private mBrand As String
    Protected Property Brand() As String
        Get
            Return CType(HashProperties("Brand"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Brand") = value
        End Set
    End Property

    Private mPackage As String
    Protected Property Package() As String
        Get
            Return CType(HashProperties("Package"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Package") = value
        End Set
    End Property

    Private mPP As String
    Protected Property PP() As String
        Get
            Return CType(HashProperties("PP"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("PP") = value
        End Set
    End Property

    Private mUOM As String
    Protected Property UOM() As String
        Get
            Return CType(HashProperties("UOM"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("UOM") = value
        End Set
    End Property

    Private mCUR As String
    Protected Property CUR() As String
        Get
            Return CType(HashProperties("CUR"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("CUR") = value
        End Set
    End Property

    Private mVal As String
    Protected Property Val() As String
        Get
            Return CType(HashProperties("Val"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Val") = value
        End Set
    End Property

    Private mBypass As String
    Protected Property ByPass() As String
        Get
            Return CType(HashProperties("ByPass"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("ByPass") = value
        End Set
    End Property

    Protected ReadOnly Property CKOandCKI() As String
        Get
            Return Me.BranchCKO & " - " & Me.BranchCKI
        End Get

    End Property

    Private mvehicles As String
    Protected Property Vehicles() As String
        Get
            Return CType(HashProperties("Vehicles"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Vehicles") = value
        End Set
    End Property

    Private mAvailabilitynumber As String
    Protected Property Availabilitynumber() As String
        Get
            Return CType(HashProperties("Availabilitynumber"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("Availabilitynumber") = value
        End Set
    End Property

    Private mcriteriamatch As String
    Protected Property CriteriaMatch() As String
        Get
            Return CType(HashProperties("CriteriaMatch"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("CriteriaMatch") = value
        End Set
    End Property

    Private mProductName As String
    Protected Property ProductName() As String
        Get
            Return CType(HashProperties("ProductName"), String)
        End Get
        Set(ByVal value As String)
            HashProperties("ProductName") = value
        End Set
    End Property

#End Region

#Region " Load"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.txthireperiod.ReadOnly = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SESSION_VehicleRequestMgt = "RS-VEHREQMGT" & Me.UserCode

        Try


            If Not Page.IsPostBack Then

                blnresubmit = False
                CType(Me.PickerControlForBranchCKO.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKO('" & Me.UserCode & "');")
                CType(Me.PickerControlForBranchCKI.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetAvailableRequestTimeCKI('" & Me.UserCode & "');")

                Me.NavigationTab()
                Me.getDisplayAvailableVehiclesWithCost()

                If Not Session("HashProperties") Is Nothing Then
                    Session.Remove("HashProperties")
                End If
                Session("HashProperties") = Me.HashProperties

            Else

                ' BindGrid()

                If Session("HashProperties") IsNot Nothing Then
                    Me.HashProperties = CType(Session("HashProperties"), Hashtable)
                End If

            End If
            ''ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AttachedEvent", Me.AttachedEventHandler(Me.PickerControlForProduct), True)

        Catch ex As Exception
            Me.trdataTableDefault.Visible = False

            MyBase.AddErrorMessage(ex.Message)
            ToggleButton(False)
        End Try
    End Sub
#End Region

#Region " Events"

    Protected Sub btnModifyBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModifyBookingRequest.Click
        Me.AddSessionContent()
        Response.Redirect(QS_RS_VEHREQMGT)
    End Sub

    Protected Sub rdList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdList.SelectedIndexChanged
        Select Case rdList.SelectedIndex
            Case 0 'perperson 2369
                mCostIndex = "@pp"
            Case 1 ' perday  189
                mCostIndex = "@pd"
            Case 2 ' total   4738
                mCostIndex = "@tc"
            Case 3  'vehtotal 4738
                mCostIndex = "@vc"
            Case 4  'vehdaynet 189
                mCostIndex = "@vdn"
            Case 5  'vehdaygross 252
                mCostIndex = "@vdg"
        End Select

        '' BindGrid()
        BindGridSorted()


    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        Dim col As DataControlField = Nothing
        Dim img As HtmlImage = Nothing
        If e.Row.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To GridView1.Columns.Count - 1
                col = GridView1.Columns(i)
                img = New HtmlImage
                img.Border = 0

                If col.HeaderText = "Package" Or col.HeaderText = "CUR" Then
                    If CType(ViewState("sort"), String).Contains("ASC") Then
                        img.Src = "~/images/sort_ascending.gif"
                        e.Row.Cells(i).Controls.Add(img)
                    Else
                        img.Src = "~/images/sort_descending.gif"
                        e.Row.Cells(i).Controls.Add(img)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim lblvalue As Label = Nothing
        If e.Row.RowType = DataControlRowType.DataRow Then
            Select Case mCostIndex
                Case "@pp"
                    lblvalue = CType(e.Row.FindControl("lblvaluePP"), Label)
                Case "@pd"
                    lblvalue = CType(e.Row.FindControl("lblvaluePD"), Label)
                Case "@tc"
                    lblvalue = CType(e.Row.FindControl("lblvalueTC"), Label)
                Case "@vc"
                    lblvalue = CType(e.Row.FindControl("lblvalueVC"), Label)
                Case "@vdn"
                    lblvalue = CType(e.Row.FindControl("lblvalueVDN"), Label)
                Case "@vdg"
                    lblvalue = CType(e.Row.FindControl("lblvalueVDG"), Label)
            End Select

            If lblvalue IsNot Nothing Then
                lblvalue.Visible = True
            End If


            If sbglobal IsNot Nothing Then
                sbglobal = Nothing
            End If
            sbglobal = New StringBuilder

            Dim txtValue As TextBox = CType(e.Row.FindControl("txtvalue"), TextBox)
            txtValue.Text = 0

            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            If String.IsNullOrEmpty(rowview("pkg_text").ToString) Then
                txtValue.Enabled = False
                Me.btnSaveNext.Enabled = False
                Me.btnSubmit.Enabled = False
            Else
                Me.btnSaveNext.Enabled = True
                Me.btnSubmit.Enabled = True
                txtValue.Enabled = True

            End If
        End If
    End Sub

    Protected Sub GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridView1.Sorting
        If e.SortExpression = "pkg_text" Then
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "pkg_text ASC"
            Else
                If ViewState("sort") = "pkg_text ASC" Then
                    ViewState("sort") = "pkg_text DESC"
                Else
                    ViewState("sort") = "pkg_text ASC"
                End If
            End If

        Else
            If ViewState("sort") Is Nothing Then
                ViewState("sort") = "cur ASC"
            Else
                If ViewState("sort") = "cur ASC" Then
                    ViewState("sort") = "cur DESC"
                Else
                    ViewState("sort") = "cur ASC"
                End If
            End If

        End If
        BindGridSorted()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        GetXMLforSave(False)
    End Sub

    Protected Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Resubmit(True)
    End Sub

    Protected Sub btnOneDayBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOneDayBack.Click
        OneDayBackAndForward(False)
    End Sub

    Protected Sub btnOneDayForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOneDayForward.Click
        OneDayBackAndForward(True)
    End Sub

    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        GetXMLforSave(True)
    End Sub

    ''rev:mia Feb.16    
    Protected Sub btnNewBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewBookingRequest.Click
        Me.RemoveSessionForNBR()
        Response.Redirect("VehicleRequestMgt.aspx")
    End Sub

    Protected Sub btnResubmit_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResubmit.Click
        Resubmit(True)
    End Sub

#End Region

   
    Protected Sub DateControlForCKI_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCKI.DateChanged

        CalculateRequestPeriod()
        Me.lblckiDay.Text = ParseDateAsDayWord(Me.DateControlForCKI.Text)
    End Sub

    Protected Sub DateControlForCKO_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCKO.DateChanged
        CalculateRequestPeriod()
        Me.lblckoDay.Text = ParseDateAsDayWord(Me.DateControlForCKO.Text)
    End Sub

    

    'Protected Sub PickerControlForProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForProduct.TextChanged
    '    Me.SetInformationMessage("Please 'Resubmit' to update table")
    '    blnresubmit = True
    'End Sub

    'Protected Sub PickerControlForBranchCKI_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForBranchCKI.TextChanged
    '    ''Me.SetInformationMessage("Please 'Resubmit' to update table")
    '    blnresubmit = True
    'End Sub

    'Protected Sub PickerControlForBranchCKO_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PickerControlForBranchCKO.TextChanged
    '    '' Me.SetInformationMessage("Please 'Resubmit' to update table")
    '    blnresubmit = True
    'End Sub

    'Protected Sub ddlAMPMcki_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAMPMcki.SelectedIndexChanged
    '    Me.SetInformationMessage("Please 'Resubmit' to update table")
    '    blnresubmit = True
    'End Sub

    'Protected Sub ddlamppmCKO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlamppmCKO.SelectedIndexChanged
    '    Me.SetInformationMessage("Please 'Resubmit' to update table")
    '    blnresubmit = True
    'End Sub
End Class
