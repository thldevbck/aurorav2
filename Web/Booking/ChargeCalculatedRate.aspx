<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChargeCalculatedRate.aspx.vb" Inherits="Booking_ChargeCalculatedRate" MasterPageFile="~/Include/PopupHeader.master" validateRequest="false" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Xml ID="Xml1" runat="server" />

    <table width="100%">
        <tr>
            <td align="right">
                    <asp:Button 
                        runat="server" 
                        ID="btnClose" 
                        OnClientClick="window.close();" 
                        Text="Close" 
                        CssClass="Button_Standard Button_Close" />
            </td>
        </tr>
    </table>

</asp:Content>