Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports Aurora.Booking.Services

<AuroraPageTitleAttribute("Booking Request")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_VehicleRequestMgt
    Inherits AuroraPage


#Region " Constant"

    Private Const QS_RS_RNTDUPLST As String = "RentalDuplicateList.aspx?funCode=RS-RNTDUPLST"
    Private Const QS_RS_VEHREQMGTNBR As String = "VehicleRequestMgt.aspx?funcode=RS-VEHREQMGT"
    Private Const SESSION_XMLVehicleRequest As String = "XMLVehicleRequest"
    Private Const SESSION_hashproperties As String = "hashproperties"
    Private Const btnValue As String = "Display Availability With Cost"
    Private Const SESSION_GetPackageXMLinHiddenValues As String = "GetPackageXMLinHiddenValues"
    Private Const SCRIPT_DOFOCUS As String = "window.setTimeout('DoFocus()', 1); function DoFocus(){  try { document.getElementById('REQUEST_LASTFOCUS').focus(); } catch (ex) {}    };"


#End Region

#Region " Variables"

    Private SESSION_VehicleRequestMgt As String
    Private dictObject As New StringDictionary
    Private sbFeatures As New StringBuilder
    Private strButtonListValue As String
    Private strGridItemValue As String
    Protected productid As String
    Protected XMLVehicleRequest As New XmlDocument
    Protected gProductID As String
    Protected gAgentID As String
    Protected gAgentCode As String
    Protected gAgentIsMisc As String
    Protected gMiscAgnContact As String
    Protected gMiscAgnName As String
    Protected gPackageId As String
    Protected gDdToday As String
    Protected ginitialValue As String
    Protected gNewValue As String
    Protected hashProperties As New Hashtable

#End Region

#Region " Private Properties"

    Private mContact As String = ""
    Private Property Contact() As String
        Get
            Return mContact
        End Get
        Set(ByVal value As String)
            mContact = value
        End Set
    End Property
    Private Property VhrID() As String
        Get
            If String.IsNullOrEmpty(hid_html_vhrId.Value) Then
                Try
                    If Not String.IsNullOrEmpty(CType(hashProperties("VhrID"), String)) Then
                        Return CType(hashProperties("VhrID"), String)
                    Else
                        Return lblVhrID.Text
                    End If

                Catch ex As Exception
                    Return lblVhrID.Text
                End Try

            End If

            Return hid_html_vhrId.Value

        End Get
        Set(ByVal value As String)
            hid_html_vhrId.Value = value

            If Not hashProperties Is Nothing Then
                hashProperties("VhrID") = value
            End If
            lblVhrID.Text = value
        End Set
    End Property

    Private Property BkrID() As String
        Get
            Return hid_html_bkrId.Value
        End Get
        Set(ByVal value As String)
            hid_html_bkrId.Value = value
        End Set
    End Property

    Private Property BooID() As String
        Get
            Return hid_html_booId.Value
        End Get
        Set(ByVal value As String)
            hid_html_booId.Value = value
        End Set
    End Property

    Private mreference As String = ""
    Private Property Reference() As String
        Get
            Return CType(hashProperties("Reference"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("Reference") = value
        End Set
    End Property


    Private mbrand As String = ""
    Private Property Brand() As String
        Get
            Return CType(hashProperties("Brand"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("Brand") = value
        End Set
    End Property

    Private mclass As String = ""
    Private Property [Class]() As String
        Get
            Return CType(hashProperties("Class"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("Class") = value
        End Set
    End Property

    Private mTypeID As String = ""
    Private Property TypeID() As String
        Get
            Return CType(hashProperties("TypeID"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("TypeID") = value
        End Set
    End Property

    Private mbkrIntegrityNo As String = ""
    Private Property BKRIntegrityNo() As String
        Get
            Return CType(hashProperties("BKRIntegrityNo"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("BKRIntegrityNo") = value
        End Set
    End Property

    Private mMiscAgnName As String = ""
    Private Property MiscAgnName() As String
        Get
            Return CType(hashProperties("MiscAgnName"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("MiscAgnName") = value
        End Set
    End Property

    Private mMiscAgentContact As String = ""
    Private Property MiscAgentContact() As String
        Get
            Return CType(hashProperties("MiscAgentContact"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("MiscAgentContact") = value
        End Set
    End Property


    Private ReadOnly Property NumVehicle() As String
        Get
            '' Return Me.txtNoOfVehicles.Text
            Return Me.ddlNoOfVehicles.Text
        End Get

    End Property

    Private ReadOnly Property NumberOfAdults() As String
        Get
            ''Return Me.txtAdult.Text
            Return Me.ddlAdultNO.Text
        End Get
    End Property

    Private ReadOnly Property NumberOfChildren() As String
        Get
            '' Return Me.txtchildren.Text
            Return Me.ddlChildNO.Text
        End Get
    End Property

    Private ReadOnly Property NumberOfInfants() As String
        Get
            ''Return Me.txtinfants.Text
            Return Me.ddlInfantsNO.Text
        End Get
    End Property

    Private ReadOnly Property CKOLocation() As String
        Get
            Return Splitter(Me.PickerControlForCheckOut.Text)
        End Get
    End Property

    Private ReadOnly Property CKODate() As String
        Get
            Return Me.DateControlForCheckOut.Text & " " & IIf(Me.ddlAMPMcheckout.Text.Equals("AM") = False AndAlso Me.ddlAMPMcheckout.Text.Equals("PM") = False, Me.ddlAMPMcheckout.Text, "")
        End Get
    End Property

    Private ReadOnly Property CKODayPart() As String
        Get
            Return GetAMPM(Me.ddlAMPMcheckout.Text)
        End Get
    End Property

    Private ReadOnly Property CKILocation() As String
        Get
            Return Splitter(Me.PickerControlForCheckIn.Text)
        End Get
    End Property

    Private ReadOnly Property CKIDate() As String
        Get
            ''Return Me.DateControlForCheckIn.Text
            Return Me.DateControlForCheckIn.Text & " " & IIf(Me.ddlAMPMcheckin.Text.Equals("AM") = False AndAlso Me.ddlAMPMcheckin.Text.Equals("PM") = False, Me.ddlAMPMcheckin.Text, "")
        End Get
    End Property

    Private ReadOnly Property CKIDayPart() As String
        Get
            Return GetAMPM(Me.ddlAMPMcheckin.Text)
        End Get
    End Property

    Private ReadOnly Property LastName() As String
        Get
            Return Me.txtSurname.Text
        End Get
    End Property

    Private ReadOnly Property FirstName() As String
        Get
            Return Me.txtfirstName.Text
        End Get
    End Property

    Private ReadOnly Property Titles() As String
        Get
            Return Me.ddltitle.Text
        End Get
    End Property

    Private ReadOnly Property RequestSource() As String
        Get
            If Me.ddlRequestSource.SelectedValue = "0" AndAlso Me.ddlRequestSource.SelectedItem.Text.Trim = "" Then
                Return ""
            Else
                Return Me.ddlRequestSource.SelectedValue
            End If
        End Get
    End Property



    Private ReadOnly Property HirePeriod() As String
        Get
            Try
                Return DateDiff(DateInterval.Day, CDate(Me.DateControlForCheckOut.Text), CDate(Me.DateControlForCheckIn.Text)).ToString
            Catch ex As Exception
                MyBase.AddErrorMessage(ex.Message)
            End Try
        End Get
    End Property

    Private mhireoum As String = "1"
    Private Property HireOUM() As String
        Get
            Return IIf(mhireoum = "", "1", mhireoum)
        End Get
        Set(ByVal value As String)
            mhireoum = value
        End Set
    End Property

    Private ReadOnly Property GetGriditemKey() As String
        Get
            Return HidGridViewItem.Value
        End Get
    End Property

#End Region

#Region " Public Properties"

    Private Property CacheContactID() As String
        Get
            If IsNothing(Cache("CacheContactID")) = False Then
                Return CType(Cache("CacheContactID"), String)
            Else
                Return hidContactID.Value
            End If
        End Get
        Set(ByVal value As String)
            If IsNothing(Cache("CacheContactID")) Then
                Cache.Insert("CacheContactID", value)
            Else
                Cache.Remove("CacheContactID")
                Cache.Insert("CacheContactID", value)
            End If
        End Set
    End Property


    Protected ReadOnly Property GetPackageXMLinHiddenValues() As String
        Get
            Dim strpackage As String = Me.hidCKOandCKIinfo.Value
            Dim intlastindex As Integer = strpackage.LastIndexOf(",")
            Dim ckiDate As String = ""

            If Me.DateControlForCheckIn.Text <> "" Then
                ''REV:MIA May 6

                If Me.ddlAMPMcheckin.Text.Equals("AM") Then
                    ckiDate = Me.DateControlForCheckIn.Text & " " & "07:00:00"
                Else
                    '', "13:00:00")
                    If Me.ddlAMPMcheckin.Text.Equals("PM") Then
                        ckiDate = Me.DateControlForCheckIn.Text & " " & "13:00:00"
                    Else
                        ckiDate = Me.DateControlForCheckIn.Text & " " & Me.ddlAMPMcheckin.Text
                    End If
                End If


            End If

            If ckiDate <> "" AndAlso strpackage <> "" AndAlso intlastindex <> -1 Then

                strpackage = strpackage.Substring(0, intlastindex)
                strpackage = strpackage & "," & ckiDate
            End If
            '' lblTest.Text = strpackage
            '' ViewState("Param3") = strpackage
            Return strpackage
        End Get
    End Property

    Public ReadOnly Property CurrentUserCode() As String
        Get
            Return MyBase.UserCode
        End Get
    End Property


    ''' <summary>
    ''' this is the first parameter for getting the 
    ''' package
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property AgentParameter() As String
        Get
            Return Me.PickerControlForAgent.Text
        End Get
    End Property



    ''' <summary>
    ''' this is the third parameter for getting the 
    ''' package
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property ParameterThree() As String
        Get
            Return GetPackageXML()
        End Get
    End Property



#End Region

#Region " Private Sub"

    Private Sub IasyncFocus(ByVal controlid As String, ByVal sc As ScriptManager)
        Select Case controlid

            Case "ctl00$ContentPlaceHolder$PickerControlForCheckOut$pickerTextBox"
                sc.SetFocus(Me.ddlAMPMcheckout)

            Case "ctl00$ContentPlaceHolder$ddlAMPMcheckout"
                sc.SetFocus(Me.DateControlForCheckOut)

            Case "ctl00$ContentPlaceHolder$DateControlForCheckOut$dateTextBox"
                sc.SetFocus(Me.PickerControlForCheckIn)


            Case "ctl00$ContentPlaceHolder$PickerControlForCheckIn$pickerTextBox"
                sc.SetFocus(Me.ddlAMPMcheckin)

            Case "ctl00$ContentPlaceHolder$ddlAMPMcheckIn"
                sc.SetFocus(Me.DateControlForCheckIn)

            Case "ctl00$ContentPlaceHolder$DateControlForCheckIn$dateTextBox"
                sc.SetFocus(Me.txtHirePeriod)


            Case "ctl00$ContentPlaceHolder$DateControlForCheckIn"
                Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)

            Case "ctl00$ContentPlaceHolder$DateControlForCheckOut"
                Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)

            Case "ctl00$ContentPlaceHolder$txtSurname"
                sc.SetFocus(Me.txtfirstName)

            Case "ctl00$ContentPlaceHolder$btnCalculate"
                sc.SetFocus(Me.txtSurname)

            Case "ctl00$ContentPlaceHolder$ddlAdultNo"
                sc.SetFocus(Me.ddlChildNO)
            Case "ctl00$ContentPlaceHolder$ddlChildNO"
                sc.SetFocus(Me.ddlInfantsNO)
            Case "ctl00$ContentPlaceHolder$ddlInfantsNO"
                sc.SetFocus(Me.PickerControlForPackage)

            Case "ctl00$ContentPlaceHolder$btnNewBooking"
                Me.RemoveSessionForNBR()
                Response.Redirect(QS_RS_VEHREQMGTNBR)
            Case "ctl00$ContentPlaceHolder$ddlAdultNO"
                sc.SetFocus(Me.ddlChildNO)
            Case "ctl00$ContentPlaceHolder$ddlChildNO"
                sc.SetFocus(Me.ddlInfantsNO)
            Case "ctl00$ContentPlaceHolder$ddlInfantsNO"
                sc.SetFocus(Me.PickerControlForPackage)

        End Select
    End Sub

    Private Sub SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, Optional ByVal ddlControl As DropDownList = Nothing)
        Try

            Dim index As Integer
            If Page.IsPostBack Then
                Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
                Select Case sc.AsyncPostBackSourceElementID
                    Case "ctl00$ContentPlaceHolder$DateControlForCheckOut$dateTextBox"
                        If HidCKO.Value = Me.PickerControlForCheckOut.Text Then
                            index = Me.ddlAMPMcheckout.SelectedIndex
                        End If
                    Case "ctl00$ContentPlaceHolder$DateControlForCheckIn$dateTextBox"
                        If HidCKI.Value = Me.PickerControlForCheckIn.Text Then
                            index = Me.ddlAMPMcheckin.SelectedIndex
                        End If
                    Case "ctl00$ContentPlaceHolder$PickerControlForCheckOut$PickerTextbox"
                        HidCKO.Value = Me.PickerControlForCheckOut.Text
                    Case "ctl00$ContentPlaceHolder$PickerControlForCheckIn$PickerTextbox"
                        HidCKI.Value = Me.PickerControlForCheckIn.Text
                    Case Else
                        Exit Sub
                End Select
            End If

            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If



            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, MyBase.UserCode)
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                MyBase.SetInformationShortMessage("No available time for location code '" & locationCode & "' and userCode '" & MyBase.UserCode & "'")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(0))
            Dim endtime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(1))

            Dim strtime As String = ParsetimeToString(startTime)

            ddlControl.Items.Clear()
            ddlControl.Items.Add(strtime)

            Do

                startTime = startTime.AddMinutes(15)
                strtime = ParsetimeToString(startTime)
                ddlControl.Items.Add(strtime)

            Loop While startTime <> endtime
            ddlControl.SelectedIndex = index
        Catch ex As Exception
            Me.SetErrorMessage("SelectLocationBasedOnCodeAndUser -> " & ex.Message)
        End Try
    End Sub

    Function ParsetimeToString(ByVal starttime As DateTime) As String
        Dim strTime As String = (Trim(starttime.ToString.Remove(0, starttime.ToString.IndexOf(" ") + 1)))
        Dim index As Integer = strTime.LastIndexOf(":")
        strTime = strTime.Remove(index, 3)
        If strTime.Contains("a.m") Then
            strTime = strTime.Replace("a.m.", "AM")
        Else
            strTime = strTime.Replace("p.m.", "PM")
        End If
        Return strTime
    End Function

    Private Sub ChildHookedOnFocus(ByVal currentcontrol As Control)

        If TypeOf currentcontrol Is TextBox OrElse _
           TypeOf currentcontrol Is DropDownList OrElse _
           TypeOf currentcontrol Is UserControls_PickerControl OrElse _
           TypeOf currentcontrol Is UserControls_DateControl OrElse _
           TypeOf currentcontrol Is Button Then

            If Not TypeOf currentcontrol Is UserControls_PickerControl AndAlso Not TypeOf currentcontrol Is UserControls_DateControl Then
                CType(currentcontrol, WebControl).Attributes.Add("onfocus", "document.getElementById('__LASTFOCUS').value=this.id")

            Else
                CType(currentcontrol, UserControl).Attributes.Add("onfocus", "document.getElementById('__LASTFOCUS').value=this.id")
            End If

            If currentcontrol.HasControls Then
                For Each mycontrol As Control In currentcontrol.Controls
                    ChildHookedOnFocus(mycontrol)
                Next
            End If
        Else
            If currentcontrol.HasControls Then
                For Each mycontrol As Control In currentcontrol.Controls
                    ChildHookedOnFocus(mycontrol)
                Next
            End If
        End If

    End Sub

    Private Sub PageHookedOnFocus()


        Dim currentcontrol As Control
        For Each currentcontrol In Me.panelbranchCheckout.Controls
            ChildHookedOnFocus(currentcontrol)
        Next
        For Each currentcontrol In Me.panelbranchCheckin.Controls
            ChildHookedOnFocus(currentcontrol)
        Next

        ''panelVehicleInformationHire
        For Each currentcontrol In panelVehicleInformationHire.Controls
            ChildHookedOnFocus(currentcontrol)
        Next

        'For Each currentcontrol In panelVehicleInformationPassengers.Controls
        '    ChildHookedOnFocus(currentcontrol)
        'Next


    End Sub

    Private Sub SetFocusToControl(ByVal controlToFocus As Control)
        Dim scriptBlock As String = "document.getElementById('" & controlToFocus.ClientID & "').focus()"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetFocusScript", scriptBlock, True)
    End Sub

    Private Sub RemoveSessionForNBR()
        If Session(SESSION_VehicleRequestMgt) IsNot Nothing Then
            Session.Remove(SESSION_VehicleRequestMgt)
        End If
    End Sub

    Dim postBackOut As String
    Dim postBackIn As String
    Private Sub AddJSAttributes()



        postBackOut = Me.Page.ClientScript.GetPostBackEventReference(Me.DateControlForCheckOut, "OUT")
        postBackIn = Me.Page.ClientScript.GetPostBackEventReference(Me.DateControlForCheckIn, "IN")

        CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).Attributes.Add("onchange", "getDayofWeek(2,1);GetPackageXML(); ")
        CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).Attributes.Add("onchange", "getDayofWeek(2,2); GetPackageXML(); ")

        CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); ") '' & postBackOut & " ;")
        CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();  ") '' & postBackIn & ";")

        CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); GetAvailableTimeCKO('" & Me.CurrentUserCode & "');")
        CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); GetAvailableTimeCKI('" & Me.CurrentUserCode & "');")
        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();")

        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();")
        CType(Me.PickerControlForPackage.FindControl("pickerImage"), Image).Attributes.Add("onfocus", "GetPackageXML();")

        CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "getContacts('" + Me.PickerControlForAgent.DataId + "');")
        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetProductID('" & Me.CurrentUserCode & "');")
        Me.ddlContact.Attributes.Add("onchange", _
            "document.getElementById('ctl00_ContentPlaceHolder_hidContactID').value= this.value + '|' + document.getElementById('ctl00_ContentPlaceHolder_ddlContact').options[document.getElementById('ctl00_ContentPlaceHolder_ddlContact').options.selectedIndex].text;")

        Me.txtHirePeriod.Attributes.Add("onblur", "CalculateDays();GetPackageXML()")

        Me.ddlAdultNO.Attributes.Add("onchange", "GetPackageXML();")
        Me.ddlChildNO.Attributes.Add("onchange", "GetPackageXML();")
        Me.ddlInfantsNO.Attributes.Add("onchange", "GetPackageXML();")

        Me.ddlAdultNO.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlChildNO.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlInfantsNO.Attributes.Add("onblur", "GetPackageXML();")

        ''Me.txtNoOfVehicles.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlNoOfVehicles.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlNoOfVehicles.Attributes.Add("onchange", "GetPackageXML();")

        Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")
        Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")

        Me.ddlAMPMcheckout.Attributes.Add("onblur", "GetPackageXML(); CalculateDays();")
        Me.ddlAMPMcheckin.Attributes.Add("onblur", "GetPackageXML();  CalculateDays();")


        Me.ddlAMPMcheckout.Attributes.Add("onchange", "GetPackageXML(); ") ' GetAvailableTimeCKO('" & Me.CurrentUserCode & "');")
        Me.ddlAMPMcheckin.Attributes.Add("onchange", "GetPackageXML(); ") ' GetAvailableTimeCKI('" & Me.CurrentUserCode & "');")

        Me.ddlDays.Attributes.Add("onblur", "GetPackageXML();")
        Me.txtSurname.Attributes.Add("onblur", "GetPackageXML();")
        ''Me.txtchildren.Attributes.Add("onblur", "GetPackageXML();")
        ''Me.txtinfants.Attributes.Add("onblur", "GetPackageXML();")


        Me.btnRequestBrochure.Attributes.Add("onblur", "document.getElementById('" & Me.PickerControlForAgent.ClientID & "').focus();")
        Me.btnCalculate.Attributes.Add("onblur", "document.getElementById('" & Me.txtSurname.ClientID & "').focus();")


    End Sub

    Private Sub NavigationTab()
        Dim index As Integer = 1

        CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).TabIndex = index
        'CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).Focus()

        Me.ddlContact.TabIndex = index + 1
        Me.txtRef.TabIndex = index + 1
        ''Me.btnNewAgent.TabIndex = index + 1

        CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        ''Me.txtNoOfVehicles.TabIndex = index + 1
        Me.ddlNoOfVehicles.TabIndex = index + 1

        Me.ddlRequestSource.TabIndex = index + 1

        CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        Me.ddlAMPMcheckout.TabIndex = index + 1

        CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1
        CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).TabIndex = index + 1
        Me.ddlAMPMcheckin.TabIndex = index + 1

        Me.txtHirePeriod.TabIndex = index + 1
        Me.ddlDays.TabIndex = index + 1
        Me.btnCalculate.TabIndex = index + 1
        Me.txtSurname.TabIndex = index + 1
        Me.txtfirstName.TabIndex = index + 1
        Me.ddltitle.TabIndex = index + 1

        ''rev: may 14
        ''Me.txtAdult.TabIndex = index + 1
        ''Me.txtchildren.TabIndex = index + 1
        ''Me.txtinfants.TabIndex = index + 1

        Me.ddlAdultNO.TabIndex = index + 1
        Me.ddlChildNO.TabIndex = index + 1
        Me.ddlInfantsNO.TabIndex = index + 1

        CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).TabIndex = index + 1

        Me.btnDisplayAvailabilityWithcost.TabIndex = index + 1
        Me.btnReloc.TabIndex = index + 1
        Me.btnNewBooking.TabIndex = index + 1
        Me.btnRequestBrochure.TabIndex = index + 1

        ''Me.openPanel.TabIndex = index + 1
        Me.ddlBrand.TabIndex = index + 1
        Me.rdlACorAV.TabIndex = index + 1
        Me.ddlUncommonFeature1.TabIndex = index + 1
        Me.ddlUncommonFeature2.TabIndex = index + 1

    End Sub

    Private Sub SetXmlString(ByVal xmlstring As String)
        Try

            If Me.IsValidXMLstring(xmlstring) Then
                Dim xmldoc As New XmlDocument
                xmldoc.LoadXml(xmlstring)
                Me.VhrID = xmldoc.SelectSingleNode("//Root/NewBookingRequest/VhrId").InnerText
                Me.BkrID = xmldoc.SelectSingleNode("//Root/NewBookingRequest/BkrId").InnerText
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText = Me.VhrID
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText = Me.BkrID
                xmldoc = Nothing
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try

    End Sub

    Private Sub CalculateDays(ByVal ddate As String, ByVal strCKO As String)
        If ddate <> "" Or Char.IsDigit(ddate) Then
            Dim aryDay As String() = strCKO.Split("/")
            Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            Me.DateControlForCheckIn.Text = dt.AddDays(CDbl(ddate)).ToShortDateString
        End If
    End Sub

    Private Sub FillControls()

        Dim xmlString As String = "<CommonFeatures>" & Aurora.Common.Data.ExecuteScalarSP("RES_getFeatures", "", 1) & "</CommonFeatures>"
        Dim xmlReader As New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        Dim ds As New DataSet
        ds.ReadXml(xmlReader)

        Me.chkFeatures.DataSource = ds
        Me.chkFeatures.DataTextField = "FtrDesc"
        Me.chkFeatures.DataValueField = "FtrId"
        Me.chkFeatures.DataBind()


        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        xmlString = "<UnCommonFeatures>" & Aurora.Common.Data.ExecuteScalarSP("RES_getFeatures", "", 0) & "</UnCommonFeatures>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)
        Me.ddlUncommonFeature1.DataSource = ds
        Me.ddlUncommonFeature1.DataTextField = "FtrDesc"
        Me.ddlUncommonFeature1.DataValueField = "FtrId"
        Me.ddlUncommonFeature1.DataBind()
        Me.ddlUncommonFeature1.SelectedIndex = 0

        Me.ddlUncommonFeature2.DataSource = ds
        Me.ddlUncommonFeature2.DataTextField = "FtrDesc"
        Me.ddlUncommonFeature2.DataValueField = "FtrId"
        Me.ddlUncommonFeature2.DataBind()
        Me.ddlUncommonFeature2.SelectedIndex = 0

        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        xmlString = "<Brand>" & Aurora.Common.Data.ExecuteScalarSP("GEN_getComboData", "BRAND", "", "", CurrentUserCode) & "</Brand>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)
        Me.ddlBrand.DataSource = ds
        Me.ddlBrand.DataTextField = "Description"
        Me.ddlBrand.DataValueField = "ID"
        Me.ddlBrand.DataBind()
        Me.ddlBrand.SelectedIndex = 0

        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet

        Dim xmldoc As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "11", "", "")
        ds.ReadXml(New XmlNodeReader(xmldoc))
        Me.ddlRequestSource.DataSource = ds
        Me.ddlRequestSource.DataTextField = "Code"
        Me.ddlRequestSource.DataValueField = "ID"
        Me.ddlRequestSource.DataBind()
        Me.ddlRequestSource.SelectedIndex = 0

        Me.ddlContact.Items.Add(" ")
        Me.ddlContact.Items.Add(" ")


        xmlReader = Nothing
        ds = Nothing
        ds = New DataSet
        xmlString = "<Type>" & Aurora.Common.Data.ExecuteScalarSP("RES_getClass", "", 1) & "</Type>"
        xmlReader = New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing)
        xmlReader.ReadOuterXml()
        ds.ReadXml(xmlReader)


        Me.rdlACorAV.DataSource = ds
        Me.rdlACorAV.DataTextField = "ClaCode"
        Me.rdlACorAV.DataValueField = "ClaID"
        Me.rdlACorAV.DataBind()
        xmlReader = Nothing

    End Sub



    Private Sub LoadScreen(ByVal strXML As String)

        Try

            XMLVehicleRequest.LoadXml(Me.ChangeRootElement(strXML))

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/today").InnerText) Then
                gDdToday = XMLVehicleRequest.SelectSingleNode("VehicleRequest/today").InnerText
            End If

            If Not String.IsNullOrEmpty(Me.HidAgentCode.Value) Then
                XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText = Me.HidAgentCode.Value
            End If

            Call Me.SplitData()

            Me.ginitialValue = String.Concat(gProductID, _
                                             Me.PickerControlForCheckOut.Text, _
                                             Me.DateControlForCheckOut.Text, _
                                             Me.ddlAMPMcheckout.Text, _
                                             Me.PickerControlForCheckIn.Text, _
                                             Me.DateControlForCheckIn.Text, _
                                             Me.ddlAMPMcheckin.Text)

            Me.HidGinitialValue.Value = Me.ginitialValue

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub SplitData()

        Try

            'JL 2006/06/17
            'Disable the add and update button

            ''Me.btnNewAgent.Visible = String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText)
            'Me.tdbuttonNewAgent.Visible = String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText)
            If String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText) Then
                updateAgentButton.Enabled = True
                addAgentButton.Enabled = True
            Else
                updateAgentButton.Enabled = False
                addAgentButton.Enabled = False
            End If



            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/productid").InnerText) Then
                gProductID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/productid").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agnId").InnerText) Then
                gAgentID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agnId").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText) Then
                gAgentCode = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agent").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/agentIsMisc").InnerText) Then
                gAgentIsMisc = XMLVehicleRequest.SelectSingleNode("VehicleRequest/agentIsMisc").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/contact").InnerText) Then
                gMiscAgnContact = XMLVehicleRequest.SelectSingleNode("VehicleRequest/contact").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText) Then
                gMiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/pkgid").InnerText) Then
                gPackageId = XMLVehicleRequest.SelectSingleNode("VehicleRequest/pkgid").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText) Then
                Me.VhrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText) Then
                Me.BkrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText) Then
                Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
            End If

            If Not String.IsNullOrEmpty(XMLVehicleRequest.SelectSingleNode("VehicleRequest/bookingnum").InnerText) Then
                'JL 2008/06/18
                'MyBase.SetValueLink((" :Booking :" & XMLVehicleRequest.DocumentElement.SelectSingleNode("bookingnum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & XMLVehicleRequest.DocumentElement.SelectSingleNode("bookingnum").InnerText)
                MyBase.SetValueLink((XMLVehicleRequest.DocumentElement.SelectSingleNode("bookingnum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&sTxtSearch=" & XMLVehicleRequest.DocumentElement.SelectSingleNode("bookingnum").InnerText)
                _title = "Booking Request : Booking : "

            Else
                'JL 2008/06/18
                'MyBase.SetValueLink("", "")
                MyBase.SetValueLink("", "")
                _title = "Booking Request"
            End If


        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub LoadScreen()
        Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequest", Me.VhrID, Me.BooID, Me.CurrentUserCode)
        Me.LoadScreen(xmlstring)
    End Sub

    Private Sub BindData(ByVal blnBind As Boolean)
        Try


            If blnBind = True Then
                Dim strcontact() As String = Me.hidContactID.Value.ToString.Split("|")
                ''Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequest", Me.VhrID, Me.BooID, Me.CurrentUserCode)
                Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequestForOslo", Me.VhrID, Me.BooID, Me.CurrentUserCode)
                Dim tempContactxml As String

                If strcontact.Length > 1 Then
                    tempContactxml = "<contactname>" & strcontact(1) & "</contactname></VehicleRequest>"
                Else
                    If String.IsNullOrEmpty(Me.CacheContactID) = False Then
                        strcontact = Me.CacheContactID.Split("|")
                        tempContactxml = "<contactname>" & strcontact(1) & "</contactname></VehicleRequest>"
                    Else
                        tempContactxml = "<contactname></contactname></VehicleRequest>"
                    End If

                End If

                xmlstring = xmlstring.Replace("</VehicleRequest>", tempContactxml)
                Me.LoadScreen(xmlstring)

                Dim ds As New DataSet
                Dim reader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
                reader.ReadOuterXml()
                ds.ReadXml(reader)


                If ds.Tables("VehicleRequest").Rows.Count = -1 Then Exit Sub

                Me.PickerControlForAgent.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agent")
                Me.PickerControlForAgent.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).agnId")

                ''rev:MIA May 4
                ''Me.ddlContact.SelectedItem.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).contactname")

                Me.btnDisplayAvailabilityWithcost.Focus()
                Me.PickerControlForProduct.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productname")
                Me.PickerControlForProduct.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).productid")
                ''Me.txtNoOfVehicles.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).numvehicle")
                ddlNoOfVehicles.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).numvehicle")


                'JL 2008/06/17
                'Handle empty date/time
                Me.PickerControlForCheckOut.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutbranch")
                Dim ckoTime As String = ""
                If Not String.IsNullOrEmpty(ds.Tables(0).Rows(0)("checkoutdate")) Then
                    Dim ckoDate As String = DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")), ckoTime)
                    Me.DateControlForCheckOut.Text = ckoDate ''DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutdate")
                    'Me.lbldayCKO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).coDw")
                    Me.lbldayCKO.Text = Me.ParseDateAsDayWord(ckoDate)
                End If
                Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
                'Me.ddlAMPMcheckout.Text = ckoTime.Remove(ckoTime.Length - 2, 2) & DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString
                Me.ddlAMPMcheckout.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkoutampm").ToString


                Me.PickerControlForCheckIn.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinbranch")
                Dim ckiTime As String = ""
                If Not String.IsNullOrEmpty(ds.Tables(0).Rows(0)("checkindate")) Then
                    Dim ckiDate As String = DateTimeSplitter(Convert.ToDateTime(DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")), ckiTime)
                    Me.DateControlForCheckIn.Text = ckiDate ''DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkindate")
                    'Me.lblDayCKI.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).ciDw")
                    Me.lblDayCKI.Text = Me.ParseDateAsDayWord(ckiDate)
                End If
                Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
                'Me.ddlAMPMcheckin.Text = ckiTime.Remove(ckiTime.Length - 2, 2) & DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString
                Me.ddlAMPMcheckin.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).checkinampm").ToString
                'JL 2008/06/17


                Me.txtHirePeriod.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hireperiod")

                Me.VhrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).VhrId")
                Me.BooID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).booid")
                Me.BkrID = DataBinder.Eval(ds, "Tables(0).DefaultView(0).bkrid")


                Me.txtSurname.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirersurname")
                Me.txtfirstName.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirerfirstname")
                Me.ddltitle.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).hirertitle")

                ''rev:mia may 14
                'Me.txtAdult.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).adult")
                'Me.txtchildren.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).childern")
                'Me.txtinfants.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).infants")
                Me.ddlAdultNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).adult")
                Me.ddlChildNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).childern")
                Me.ddlInfantsNO.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).infants")


                Me.PickerControlForPackage.DataId = DataBinder.Eval(ds, "Tables(0).DefaultView(0).pkgid")
                Me.PickerControlForPackage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).package")

                Me.ddlRequestSource.SelectedValue = DataBinder.Eval(ds, "Tables(0).DefaultView(0).requestsource")
                Me.lblDaysMessage.Text = DataBinder.Eval(ds, "Tables(0).DefaultView(0).tillDays")


                reader = Nothing


            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub GetCommonFeatures()

        With sbFeatures
            For Each chkitems As ListItem In chkFeatures.Items
                If chkitems.Selected Then
                    sbFeatures.Append("," & chkitems.Value)
                End If
            Next
        End With


    End Sub

    Private Sub GetSelectedFeatures()
        If Me.ddlUncommonFeature1.SelectedItem IsNot "" Then
            If Me.ddlUncommonFeature1.SelectedValue IsNot "" Then
                sbFeatures.Append("," & Me.ddlUncommonFeature1.SelectedValue)
            End If
        End If

        If Me.ddlUncommonFeature2.SelectedItem IsNot "" Then
            If Me.ddlUncommonFeature2.SelectedValue IsNot "" Then
                sbFeatures.Append("," & Me.ddlUncommonFeature2.SelectedValue)
            End If
        End If
        If sbFeatures.ToString.StartsWith(",") Then
            sbFeatures.Remove(0, 1)
        End If
    End Sub

    Private Sub GetVehicleTypes(ByVal VehicleTypeID As String)
        If VehicleTypeID IsNot "NA" Then
            Dim xmlstring As String = "<VehicleTypes>" & Aurora.Common.Data.ExecuteScalarSP("RES_getType", VehicleTypeID, "") & "</VehicleTypes>"
            Dim xmlreader As New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing)
            xmlreader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(xmlreader)

            Me.GridView1.DataSource = ds
            Me.GridView1.DataBind()
            Me.GridView1.SelectedIndex = 0
            xmlreader = Nothing
        End If
    End Sub

    Private Sub SplitDataOnPostBack()

        'Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingRequest", Me.VhrID, Me.BooID, CurrentUserCode)

        hashProperties = CType(Session(SESSION_hashproperties), Hashtable)
        XMLVehicleRequest = CType(hashProperties(SESSION_XMLVehicleRequest), XmlDocument)

        Dim xmlstring As String = XMLVehicleRequest.OuterXml
        If IsValidXMLstring(xmlstring) Then
            Dim xmlelem As XmlElement = XMLVehicleRequest.DocumentElement
            Me.VhrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText
            Me.BkrID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrid").InnerText
            Me.BooID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/booid").InnerText
            Me.Reference = XMLVehicleRequest.SelectSingleNode("VehicleRequest/referance").InnerText
            Me.Brand = XMLVehicleRequest.SelectSingleNode("VehicleRequest/brand").InnerText
            Me.Class = XMLVehicleRequest.SelectSingleNode("VehicleRequest/class").InnerText
            Me.TypeID = XMLVehicleRequest.SelectSingleNode("VehicleRequest/type").InnerText
            Me.BKRIntegrityNo = XMLVehicleRequest.SelectSingleNode("VehicleRequest/bkrIntegrityNo").InnerText
            Me.MiscAgnName = XMLVehicleRequest.SelectSingleNode("VehicleRequest/miscAgnName").InnerText
            Me.HireOUM = XMLVehicleRequest.SelectSingleNode("VehicleRequest/hiretype").InnerText
            '' Me.productid = XMLVehicleRequest.SelectSingleNode("VehicleRequest/PrdId").InnerText
        End If
    End Sub

    Private Sub BlankValueInDropdown(ByVal ddlControl As DropDownList)
        Dim item As ListItem = Nothing
        If ddlControl.ID = "ddlContact" Then
            '    item = New ListItem(" ", 0)
        Else
            item = New ListItem(" ", 0)
        End If

        ddlControl.Items.Insert(0, item)
        ddlControl.SelectedIndex = 0
    End Sub

    Private Sub GetSessionForMBR()

        If Not String.IsNullOrEmpty(Request.QueryString("funcode")) Then
            If Request.QueryString("funcode").Equals("RS-VEHREQMGT") Then

                dictObject = CType(Session(SESSION_VehicleRequestMgt), StringDictionary)
                If dictObject IsNot Nothing Then
                    Me.BkrID = dictObject("bkrid")
                    Me.BooID = dictObject("booid")
                    Me.hid_html_butSel.Value = dictObject("butSel")
                    Me.VhrID = dictObject("vhrid")
                    Me.hidContactID.Value = dictObject("Contact")
                    Me.hidCKOandCKIinfo.Value = dictObject("GetPackageXMLinHiddenValues")

                End If
            End If
        End If
    End Sub

    Private Sub AddSessionForMBR()

        If dictObject IsNot Nothing Then
            dictObject.Clear()
        End If

        dictObject.Add("vhrId", Me.VhrID)
        dictObject.Add("BkrID", Me.BkrID)
        dictObject.Add("booId", Me.BooID)
        dictObject.Add("bpdId", "")
        dictObject.Add("butsel", Me.btnDisplayAvailabilityWithcost.Text)
        dictObject.Add("rntid", "")
        dictObject.Add("Contact", Me.hidContactID.Value)
        dictObject.Add("GetPackageXMLinHiddenValues", Me.hidCKOandCKIinfo.Value)
        Me.CacheContactID = Me.hidContactID.Value

        Session.Add(SESSION_VehicleRequestMgt, dictObject)

    End Sub

    Private Sub CompareValues()
        Me.gNewValue = String.Concat(gProductID, _
                                         Me.PickerControlForCheckOut.Text, _
                                         Me.DateControlForCheckOut.Text, _
                                         Me.ddlAMPMcheckout.Text, _
                                         Me.PickerControlForCheckIn.Text, _
                                         Me.DateControlForCheckIn.Text, _
                                         Me.ddlAMPMcheckin.Text)


        If Not Me.HidGinitialValue.Value.ToString.Equals(Me.gNewValue) Then
            Me.VhrID = ""
        End If
    End Sub


    Private Sub CalculateRequestPeriod()
        Try
            If Me.DateControlForCheckOut.Text = "" Then
                Me.SetInformationShortMessage("Check-Out date is empty")
                Exit Sub
            End If

            If DateControlForCheckIn.Text = "" Then
                Me.SetInformationShortMessage("Check-In date is empty")
                Exit Sub
            End If

            ''rev:mia May 6
            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                       Me.DateControlForCheckOut.Text, _
                                                       GetAMPM(Me.ddlAMPMcheckout.Text), _
                                                       Me.DateControlForCheckIn.Text, _
                                                       GetAMPM(Me.ddlAMPMcheckin.Text), _
                                                       HirePeriod, _
                                                       Me.ddlDays.SelectedValue, _
                                                       0)

            If Me.IsValidXMLstring(xmlstring) = False Then Exit Sub
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xmlstring)
            Dim xmlelem As XmlElement = xmldoc.DocumentElement
            Me.DateControlForCheckOut.Text = xmlelem.SelectSingleNode("//Root/D1").InnerText
            Me.DateControlForCheckIn.Text = xmlelem.SelectSingleNode("//Root/D2").InnerText
            Me.txtHirePeriod.Text = xmlelem.SelectSingleNode("//Root/Dif").InnerText
            xmldoc = Nothing

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub DefaultContent()
        ''Me.txtNoOfVehicles.Text = 1
        'Me.txtAdult.Text = 0
        'Me.txtinfants.Text = 0
        'Me.txtchildren.Text = 0

        ''rev: may 14
        Me.ddlNoOfVehicles.Text = 1
        Me.ddlChildNO.SelectedIndex = 0
        Me.ddlInfantsNO.SelectedIndex = 0
        Me.ddlAdultNO.SelectedIndex = 1
    End Sub

    Sub ToggleButton(ByVal bln As Boolean)
        If bln = True Then
            Me.btnDisplayAvailabilityWithcost.Enabled = True
            Me.btnReloc.Enabled = True

            Me.btnNewBooking.Enabled = True
            Me.btnRequestBrochure.Enabled = True
            Me.btnCalculate.Enabled = True
            '' Me.btnNewAgent.Enabled = True

        Else
            Me.btnDisplayAvailabilityWithcost.Enabled = False
            Me.btnReloc.Enabled = False

            Me.btnNewBooking.Enabled = True
            Me.btnRequestBrochure.Enabled = True
            Me.btnCalculate.Enabled = True
            '' Me.btnNewAgent.Enabled = True

        End If
    End Sub
#End Region

#Region " Private Function"

    Private Function DateTimeSplitter(ByVal CKOCKIdate As DateTime, ByRef CKOCKItime As String) As String
        Dim CKOCKIdatestring As String = CKOCKIdate.ToShortDateString
        CKOCKItime = CKOCKIdate.ToShortTimeString
        CKOCKItime = CKOCKItime.Replace("a.m.", "AM")
        CKOCKItime = CKOCKItime.Replace("p.m.", "PM")
        Return CKOCKIdatestring
    End Function
    ''rev:mia May 6
    Private Function GetAMPM(ByVal controldate As String) As String
        If controldate.Contains("AM") Then
            Return "AM"
        Else
            Return "PM"
        End If
    End Function

    Private Function ChangeRootElement(ByVal strxml As String) As String
        strxml = strxml.Replace("<data>", "<Data>")
        strxml = strxml.Replace("</data>", "</Data>")
        Return strxml
    End Function


    Private Function IsValidIntegerValue(ByVal value As String) As Boolean
        Try
            If (CInt(value) = 0) Then Return True
            Dim temp As Integer = Integer.Parse(value)
            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function IsValidDropdownValue(ByVal ddlControl As DropDownList) As Boolean
        If String.IsNullOrEmpty(ddlControl.SelectedItem.Text) Then
            Return False
        End If
        If String.IsNullOrEmpty(ddlControl.SelectedValue.ToString) Then
            Return False
        End If
        Return True
    End Function

    Private Function ParseDateAsDayWord(ByVal strDate As String) As String
        Try

            Dim aryDay As String() = strDate.Split("/")
            Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            Return dt.ToString("dddd").Substring(0, 3)
        Catch
        End Try
    End Function

    Private Function ParseRemainingDay(ByVal strdate As String) As String
        Try
            Dim aryDay As String() = strdate.Split("/")
            Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            Return DateDiff(DateInterval.Day, DateTime.Now, dt)
        Catch
        End Try
    End Function


    Private Function GetProductID() As String
        Try


            Call GetCommonFeatures()
            Call GetSelectedFeatures()

            Dim paramOne_ProductCode As String = Splitter(Me.PickerControlForProduct.Text)
            Dim paramTwo_Brand As String = Me.ddlBrand.SelectedValue

            Dim paramThree_Class As String = ""
            If strButtonListValue <> "" Then
                paramThree_Class = strButtonListValue
            Else
                If Session("listvalue") IsNot Nothing Then
                    paramThree_Class = CType(Session("listvalue"), String)
                End If
            End If

            Dim paramFour_Type As String = ""

            strGridItemValue = strGridItemValue

            If strGridItemValue <> "" Then
                paramFour_Type = strGridItemValue
            Else
                If Session("GridItemValue") IsNot Nothing Then
                    paramFour_Type = CType(Session("GridItemValue"), String)
                End If
            End If

            Dim paramFive As String = ""
            Dim paramSix_Features As String = sbFeatures.ToString
            Dim paramSeven As String = ""


            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", _
                                                       paramOne_ProductCode, _
                                                       paramTwo_Brand, _
                                                       paramThree_Class, _
                                                       paramFour_Type, _
                                                       paramFive, _
                                                       paramSix_Features, _
                                                       paramSeven, _
                                                       CurrentUserCode)


            If xmlstring <> "" Then

                If IsValidXMLstring(xmlstring) Then
                    Dim xmldoc As New XmlDocument
                    xmldoc.LoadXml(xmlstring)
                    Dim xmlelem As XmlElement = xmldoc.DocumentElement
                    Return xmlelem.SelectSingleNode("Id").InnerText
                End If
            Else
                Return ""
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Function

    Private Function GetPackageXML() As String

        Dim agentid As String = IIf(Me.PickerControlForAgent.DataId <> "", Me.PickerControlForAgent.DataId, Me.hidAgentID.Value) & ","
        If agentid = "," Then
            agentid = Me.hidAgentID.Value & ","
        End If

        productid = GetProductID() & ","
        If productid = "," Then Return ""

        ''rev:mia May 6
        ''Dim ckoDate As String = Me.DateControlForCheckOut.Text & " " & IIf(Me.ddlAMPMcheckout.Text.Equals("AM"), "07:00:00", "13:00:00") & ","
        Dim ckoDate As String
        If Me.ddlAMPMcheckout.Text.Equals("AM") Then
            ckoDate = Me.ddlAMPMcheckout.Text & " " & "07:00:00" & ","
        Else
            '', "13:00:00")
            If Me.ddlAMPMcheckout.Text.Equals("PM") Then
                ckoDate = Me.DateControlForCheckOut.Text & " " & "13:00:00" & ","
            Else
                ckoDate = Me.DateControlForCheckOut.Text & " " & Me.ddlAMPMcheckout.Text & ","
            End If
        End If


        Dim ckoBranch As String = Splitter(Me.PickerControlForCheckOut.Text) & ","
        Dim ckiBranch As String = Splitter(Me.PickerControlForCheckIn.Text) & ","

        'Dim adultNo As String = Me.txtAdult.Text & ","
        'Dim ChildNo As String = Me.txtchildren.Text & ","
        'Dim InfantNo As String = Me.txtinfants.Text & ","

        ''rev:mia May 14
        Dim adultNo As String = Me.NumberOfAdults & ","
        Dim ChildNo As String = Me.NumberOfChildren & ","
        Dim InfantNo As String = Me.NumberOfInfants & ","

        ''rev:mia May 6
        ''Dim ckiDate As String = Me.DateControlForCheckIn.Text & " " & IIf(Me.ddlAMPMcheckin.Text.Equals("AM"), "07:00:00", "13:00:00")
        Dim ckiDate As String
        If Me.ddlAMPMcheckin.Text.Equals("AM") Then
            ckiDate = Me.DateControlForCheckIn.Text & " " & "07:00:00"
        Else
            '', "13:00:00")
            If Me.ddlAMPMcheckin.Text.Equals("PM") Then
                ckiDate = Me.DateControlForCheckIn.Text & " " & "13:00:00"
            Else
                ckiDate = Me.DateControlForCheckIn.Text & " " & Me.ddlAMPMcheckin.Text
            End If
        End If


        Return String.Concat(agentid, productid, ckoDate, ckoBranch, ckiBranch, adultNo, ChildNo, InfantNo, ckiDate)
    End Function

    Private Function IsRequiredFieldHaveValues() As Boolean

        If Me.PickerControlForAgent.Text = "" Then
            Me.SetInformationShortMessage("Agent ID is mandatory")
            Me.PickerControlForAgent.Focus()
            Return False
        End If

        If Me.PickerControlForProduct.Text = "" Then
            Me.SetInformationShortMessage("Product is mandatory")
            Me.PickerControlForProduct.Focus()
            Return False
        End If

        If PickerControlForCheckOut.Text = "" Then
            Me.SetInformationShortMessage("Check-out Location is mandatory")
            PickerControlForCheckOut.Focus()
            Return False
        End If

        If DateControlForCheckOut.Text = "" Then
            Me.SetInformationShortMessage("Check-out date is mandatory")
            DateControlForCheckOut.Focus()
            Return False
        End If

        If PickerControlForCheckIn.Text = "" Then
            Me.SetInformationShortMessage("Check-In Location is mandatory")
            PickerControlForCheckIn.Focus()
            Return False
        End If

        If DateControlForCheckIn.Text = "" Then
            Me.SetInformationShortMessage("Check-in date is mandatory")
            DateControlForCheckIn.Focus()
            Return False
        End If

        If txtSurname.Text = "" Then
            Me.SetInformationShortMessage("Surname is mandatory")
            txtSurname.Focus()
            Return False
        End If

        'JL 2008/06/18
        'Validations
        If String.IsNullOrEmpty(ddlAdultNO.Text) Then
            Me.SetInformationShortMessage("Please enter the number of adults (Zero is a valid value).")
            ddlAdultNO.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(ddlChildNO.Text) Then
            Me.SetInformationShortMessage("Please enter the number of children (Zero is a valid value).")
            ddlChildNO.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(ddlInfantsNO.Text) Then
            Me.SetInformationShortMessage("Please enter the number of infants (Zero is a valid value).")
            ddlInfantsNO.Focus()
            Return False
        End If
        'JL 2008/06/18

        'If txtAdult.Text = "" Then
        '    Me.SetInformationShortMessage("Please enter the number of adults (Zero is a valid value).")
        '    txtAdult.Focus()
        '    Return False
        'End If

        'If txtchildren.Text = "" Then
        '    Me.SetInformationShortMessage("Please enter the number of children (Zero is a valid value).")
        '    txtchildren.Focus()
        '    Return False
        'End If

        'If txtinfants.Text = "" Then
        '    Me.SetInformationShortMessage("Please enter the number of infants (Zero is a valid value).")
        '    txtinfants.Focus()
        '    Return False
        'End If

        Return True

    End Function

    Private Function Splitter(ByVal strToSplit As String) As String
        If strToSplit.Contains("-") Then
            Dim strArray As String() = strToSplit.Split("-")
            Return strArray(0)
        End If
    End Function

    Private Function IsValidXMLstring(ByVal strXml As String) As Boolean
        Try
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(strXml)
            xmldoc = Nothing
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Function GetXMLString(Optional ByVal getnewcontact As Boolean = False) As String
        Try


            Dim strcontact() As String = Me.hidContactID.Value.ToString.Split("|")

            If sbFeatures IsNot Nothing Then sbFeatures = Nothing : sbFeatures = New StringBuilder

            With sbFeatures
                .Append("<Root>")
                .Append("<NewBookingRequest>")

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/BooId") Is Nothing Then
                    .AppendFormat("<BooId>{0}</BooId>", Me.BooID)
                Else
                    .AppendFormat("<BooId>{0}</BooId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BooId").InnerText)
                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/BkrId") Is Nothing Then
                    .AppendFormat("<BkrId>{0}</BkrId>", Me.BkrID)
                Else
                    .AppendFormat("<BkrId>{0}</BkrId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BkrId").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId") Is Nothing Then
                    .AppendFormat("<VhrId>{0}</VhrId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrId").InnerText)
                Else
                    .Append("<VhrId/>")
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/AgnCode") Is Nothing Then
                    .AppendFormat("<AgnCode>{0}</AgnCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/AgnCode").InnerText)
                Else
                    .AppendFormat("<AgnCode>{0}</AgnCode>", Splitter(Me.PickerControlForAgent.Text))
                End If

                If Me.ddlContact.SelectedIndex > 0 Then
                    If XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode") Is Nothing Then
                        .AppendFormat("<ConCode>{0}</ConCode>", strcontact(0))
                    Else
                        .AppendFormat("<ConCode>{0}</ConCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode").InnerText)
                    End If
                Else
                    If XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode") Is Nothing Then
                        .AppendFormat("<ConCode>{0}</ConCode>", String.Empty)
                    Else
                        .AppendFormat("<ConCode>{0}</ConCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ConCode").InnerText)
                    End If

                End If

                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrAgnRef") Is Nothing Then
                    .AppendFormat("<VhrAgnRef>{0}</VhrAgnRef>", Me.Reference)
                Else
                    .AppendFormat("<VhrAgnRef>{0}</VhrAgnRef>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/VhrAgnRef").InnerText)
                End If

                .Append("<SapId/>")


                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/PrdId") Is Nothing Then
                    If Me.productid = "," Or Me.productid = "" Then
                        Me.productid = Me.hidPackageID.Value
                    End If
                    Me.productid = Me.productid.Replace(",", "")
                    .AppendFormat("<PrdId>{0}</PrdId>", Me.productid)
                Else
                    .AppendFormat("<PrdId>{0}</PrdId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PrdId").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/BrdCode") Is Nothing Then
                    .AppendFormat("<BrdCode>{0}</BrdCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/BrdCode").InnerText)
                Else
                    .AppendFormat("<BrdCode>{0}</BrdCode>", Me.Brand)
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ClaId") Is Nothing Then
                    .AppendFormat("<ClaId>{0}</ClaId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ClaId").InnerText)
                Else
                    .AppendFormat("<ClaId>{0}</ClaId>", Me.Class)
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgCode") Is Nothing Then
                    .AppendFormat("<PkgCode>{0}</PkgCode>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgCode").InnerText)
                Else
                    .AppendFormat("<PkgCode>{0}</PkgCode>", Splitter(Me.PickerControlForPackage.Text))
                End If



                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgId") Is Nothing Then
                    .AppendFormat("<PkgId>{0}</PkgId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/PkgId").InnerText)
                Else
                    .AppendFormat("<PkgId>{0}</PkgId>", Me.PickerControlForPackage.DataId)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/TypeId") Is Nothing Then
                    .AppendFormat("<TypeId>{0}</TypeId>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/TypeId").InnerText)
                Else
                    .AppendFormat("<TypeId>{0}</TypeId>", Me.TypeID)
                End If


                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/IntegrityNo") Is Nothing Then
                    .AppendFormat("<IntegrityNo>{0}</IntegrityNo>", Me.BKRIntegrityNo)
                Else
                    .AppendFormat("<IntegrityNo>{0}</IntegrityNo>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/IntegrityNo").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentName") Is Nothing Then
                    .AppendFormat("<MiscAgentName>{0}</MiscAgentName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentName").InnerText)
                Else
                    .AppendFormat("<MiscAgentName>{0}</MiscAgentName>", Me.MiscAgnName)
                End If



                If XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentContact") Is Nothing Then
                    .AppendFormat("<MiscAgentContact>{0}</MiscAgentContact>", Me.MiscAgentContact)
                Else
                    .AppendFormat("<MiscAgentContact>{0}</MiscAgentContact>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/MiscAgentContact").InnerText)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfVehicles") Is Nothing Then
                    .AppendFormat("<NumberOfVehicles>{0}</NumberOfVehicles>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfVehicles").InnerText)
                Else
                    .AppendFormat("<NumberOfVehicles>{0}</NumberOfVehicles>", Me.NumVehicle)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfAdults") Is Nothing Then
                    .AppendFormat("<NumberOfAdults>{0}</NumberOfAdults", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfAdults").InnerText)
                Else
                    .AppendFormat("<NumberOfAdults>{0}</NumberOfAdults>", Me.NumberOfAdults)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfChildren") Is Nothing Then
                    .AppendFormat("<NumberOfChildren>{0}</NumberOfChildren>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfChildren").InnerText)
                Else
                    .AppendFormat("<NumberOfChildren>{0}</NumberOfChildren>", Me.NumberOfChildren)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfInfants") Is Nothing Then
                    .AppendFormat("<NumberOfInfants>{0}</NumberOfInfants>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/NumberOfInfants").InnerText)
                Else
                    .AppendFormat("<NumberOfInfants>{0}</NumberOfInfants>", Me.NumberOfInfants)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoLocation") Is Nothing Then
                    .AppendFormat("<ckoLocation>{0}</ckoLocation>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoLocation").InnerText)
                Else
                    .AppendFormat("<ckoLocation>{0}</ckoLocation>", Me.CKOLocation)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDate") Is Nothing Then
                    .AppendFormat("<ckoDate>{0}</ckoDate>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDate").InnerText)
                Else
                    .AppendFormat("<ckoDate>{0}</ckoDate>", Me.CKODate)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDayPart") Is Nothing Then
                    .AppendFormat("<ckoDayPart>{0}</ckoDayPart>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckoDayPart").InnerText)
                Else
                    .AppendFormat("<ckoDayPart>{0}</ckoDayPart>", Me.CKODayPart)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiLocation") Is Nothing Then
                    .AppendFormat("<ckiLocation>{0}</ckiLocation>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiLocation").InnerText)
                Else
                    .AppendFormat("<ckiLocation>{0}</ckiLocation>", Me.CKILocation)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDate") Is Nothing Then
                    .AppendFormat("<ckiDate>{0}</ckiDate>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDate").InnerText)
                Else
                    .AppendFormat("<ckiDate>{0}</ckiDate>", Me.CKIDate)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDayPart") Is Nothing Then
                    .AppendFormat("<ckiDayPart>{0}</ckiDayPart>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/ckiDayPart").InnerText)
                Else
                    .AppendFormat("<ckiDayPart>{0}</ckiDayPart>", Me.CKIDayPart)
                End If

                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/HirePeriod") Is Nothing Then
                    .AppendFormat("<HirePeriod>{0}</HirePeriod>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/HirePeriod").InnerText)
                Else
                    .AppendFormat("<HirePeriod>{0}</HirePeriod>", Me.HirePeriod)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/HireUOM") Is Nothing Then
                    .AppendFormat("<HireUOM>{0}</HireUOM>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/HireUOM").InnerText)
                Else
                    .AppendFormat("<HireUOM>{0}</HireUOM>", Me.HireOUM)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName") Is Nothing Then
                    .AppendFormat("<LastName>{0}</LastName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/LastName").InnerText)
                Else
                    .AppendFormat("<LastName>{0}</LastName>", Me.LastName)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/FirstName") Is Nothing Then
                    .AppendFormat("<FirstName>{0}</FirstName>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/FirstName").InnerText)
                Else
                    .AppendFormat("<FirstName>{0}</FirstName>", Me.LastName)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/Title") Is Nothing Then
                    .AppendFormat("<Title>{0}</Title>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/Title").InnerText)
                Else
                    .AppendFormat("<Title>{0}</Title>", Me.Titles)
                End If


                If Not XMLVehicleRequest.SelectSingleNode("VehicleRequest/Title") Is Nothing Then
                    .AppendFormat("<RequestSource>{0}</RequestSource>", XMLVehicleRequest.SelectSingleNode("VehicleRequest/RequestSource").InnerText)
                Else
                    .AppendFormat("<RequestSource>{0}</RequestSource>", Me.RequestSource)
                End If

                .AppendFormat("<ButClick>{0}</ButClick>", btnValue)

                For Each item As ListItem In Me.chkFeatures.Items
                    If item.Selected Then
                        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", item.Value)
                    End If

                Next

                If Me.ddlUncommonFeature1.SelectedItem.Text <> "" Then
                    If Me.ddlUncommonFeature1.SelectedValue <> 0 Then
                        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", Me.ddlUncommonFeature1.SelectedValue)
                    End If
                End If

                If Me.ddlUncommonFeature2.SelectedItem.Text <> "" Then
                    If Me.ddlUncommonFeature2.SelectedValue <> 0 Then
                        .AppendFormat("<Feature><FtrId>{0}</FtrId></Feature>", Me.ddlUncommonFeature2.SelectedValue)
                    End If
                End If
                .Append("</NewBookingRequest>")
                .AppendFormat("<Aurora><AddModUserId>{0}</AddModUserId></Aurora>", Me.CurrentUserCode)
                .AppendFormat("<Aurora><AddProgName>{0}</AddProgName></Aurora>", "VehicleRequestMgt.aspx")
                .Append("</Root>")

            End With
            Return sbFeatures.ToString

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Function


#End Region

#Region " Page Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        DefaultContent()
        Page.MaintainScrollPositionOnPostBack = True
    End Sub

    Private _title As String
    Public Overrides ReadOnly Property PageTitle() As String
        Get
            Return _title
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SESSION_VehicleRequestMgt = "RS-VEHREQMGT" & MyBase.UserCode

        Try


            Call Me.GetSessionForMBR()
            Call Me.ToggleButton(True)
            If Not Page.IsPostBack Then

                Me.PageHookedOnFocus()

                ''Dim postbackNBR As String = Me.Page.ClientScript.GetPostBackEventReference(Me.btnNewBooking, "NBR")
                ''btnNewBooking.Attributes.Add("onclick", postbackNBR)

                'JL 2008/06/17
                'get bookingId for queryString
                If Not String.IsNullOrEmpty(Request.QueryString("booId")) Then
                    BooID = Request.QueryString("booId")
                End If
                'JL 2008/06/17

                SetFocusToControl(Me.PickerControlForAgent)
                Call Me.NavigationTab()
                Call Me.AddJSAttributes()
                Call Me.FillControls()

                If Not String.IsNullOrEmpty(Request.QueryString("funcode")) Then
                    If Request.QueryString("funcode").Equals("RS-VEHREQMGT") Then
                        Call Me.BindData(True)
                        Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
                        Me.PickerControlForPackage.Param1 = Splitter(AgentParameter)
                    End If
                Else
                    Me.VhrID = ""
                    Me.BooID = ""
                    Me.BkrID = ""
                    Me.LoadScreen()
                    Me.CacheContactID = ""
                End If

                If hashProperties Is Nothing Then
                    hashProperties = New Hashtable
                End If
                hashProperties(SESSION_XMLVehicleRequest) = XMLVehicleRequest
                If Session(SESSION_hashproperties) IsNot Nothing Then
                    Session.Remove(SESSION_hashproperties)
                End If
                Session(SESSION_hashproperties) = hashProperties


            Else

                'Dim strEvent As String = Request("__EVENTARGUMENT")
                'If strEvent <> "" Then
                '    If strEvent = "IN" Then
                '        SetFocusToControl(Me.txtHirePeriod)

                '    ElseIf strEvent = "OUT" Then
                '        SetFocusToControl(Me.PickerControlForCheckIn)

                '    End If
                'End If

                Call SplitDataOnPostBack()
                Me.PickerControlForPackage.Param3 = ParameterThree
                Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
                Me.PickerControlForPackage.Param1 = Splitter(AgentParameter)

                Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
                IasyncFocus(sc.AsyncPostBackSourceElementID, sc)

            End If

            '' ScriptManager.RegisterStartupScript(Me, Me.GetType, "ScriptDofocus", SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request("__LASTFOCUS")), True)




        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
            ToggleButton(False)
        End Try
    End Sub

#End Region

#Region " Control Events"

    Protected Sub rdlACorAV_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlACorAV.DataBound
        Dim NAitemOntop As ListItem = New ListItem("NA")
        NAitemOntop.Selected = True
        rdlACorAV.Items.Insert(0, NAitemOntop)
    End Sub

    Protected Sub rdlACorAV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlACorAV.SelectedIndexChanged
        If rdlACorAV.SelectedItem.Text <> "NA" Then
            strButtonListValue = rdlACorAV.SelectedValue
            Session("listvalue") = strButtonListValue
            Call GetVehicleTypes(strButtonListValue)
            tableview.Visible = True
            tableviewDefault.Visible = False
        Else
            Me.GridView1.DataSource = Nothing
            Me.GridView1.DataBind()
            tableview.Visible = False
            tableviewDefault.Visible = True
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.style.background='#BEBEBE';this.style.cursor='hand';")
            e.Row.Attributes.Add("onmouseout", "this.style.background='#ffffff';this.style.cursor='hand';")
            e.Row.Attributes.Add("onclick", "document.getElementById('ctl00_ContentPlaceHolder_HidGridViewItem').value =" & e.Row.DataItemIndex.ToString & ";")
        End If
    End Sub

    Protected Sub ddlRequestSource_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRequestSource.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlUncommonFeature1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature1.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlUncommonFeature2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUncommonFeature2.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlBrand_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub DateControlForCheckIn_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCheckIn.DateChanged
        Me.lblDayCKI.Text = Me.ParseDateAsDayWord(DateControlForCheckIn.Text)
        Me.lblDaysMessage.Text = Me.ParseRemainingDay(DateControlForCheckOut.Text)

        Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
        If sc.AsyncPostBackSourceElementID = "ctl00$ContentPlaceHolder$DateControlForCheckIn$dateTextBox" Then
            If Me.PickerControlForCheckIn.Text <> "" Then
                If Me.HidCKI.Value = "" Then
                    Me.HidCKI.Value = Me.PickerControlForCheckIn.Text
                End If

            End If
            Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckIn.Text, Me.ddlAMPMcheckin)
        End If
        Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
        RefreshContactID()
    End Sub

    Protected Sub DateControlForCheckOut_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateControlForCheckOut.DateChanged
        Me.lbldayCKO.Text = Me.ParseDateAsDayWord(DateControlForCheckOut.Text)
        Me.lblDaysMessage.Text = Me.ParseRemainingDay(DateControlForCheckOut.Text)

        Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
        If sc.AsyncPostBackSourceElementID = "ctl00$ContentPlaceHolder$DateControlForCheckOut$dateTextBox" Then
            If Me.PickerControlForCheckOut.Text <> "" Then
                If Me.HidCKO.Value = "" Then
                    Me.HidCKO.Value = Me.PickerControlForCheckOut.Text
                End If

            End If
            Me.SelectLocationBasedOnCodeAndUser(Me.PickerControlForCheckOut.Text, Me.ddlAMPMcheckout)
        End If
        Me.PickerControlForPackage.Param3 = GetPackageXMLinHiddenValues
        RefreshContactID()
    End Sub

    Sub RefreshContactID()
        If Me.hidContactID.Value <> "" AndAlso Me.hidContactID.Value.Contains("|") Then
            Dim ary() As String = Me.hidContactID.Value.Split("|")
            If ary(1) IsNot Nothing AndAlso ary(1).Length > 0 Then
                Me.ddlContact.SelectedItem.Text = ary(1)
            End If

        End If
    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        CalculateDays(Me.txtHirePeriod.Text, Me.DateControlForCheckOut.Text)
        Me.lblDayCKI.Text = Me.ParseDateAsDayWord(DateControlForCheckIn.Text)
        CalculateRequestPeriod()
    End Sub

    Protected Sub CV_txtAdult_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(txtAdult.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Protected Sub CV_txtchildren_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(txtchildren.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Protected Sub CV_txtinfants_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(txtinfants.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Protected Sub CV_txthireperiod_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(txtHirePeriod.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Protected Sub CV_txtvehicles_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CBool(IsValidIntegerValue(Me.txtNoOfVehicles.Text)) = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If

    End Sub

    Protected Sub btnDisplayAvailabilityWithcost_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplayAvailabilityWithcost.Click
        Try
            If Page.IsValid Then
                Me.CompareValues()

                If IsRequiredFieldHaveValues() Then
                    Dim strResult As String = Aurora.Booking.Services.BookingRequest.ManageBookingRequest(GetXMLString)
                    If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnError) Then
                        MyBase.SetInformationShortMessage(Aurora.Booking.Services.BookingRequest.ReturnError)
                        Exit Sub
                    End If

                    If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnWarning) Then
                        MyBase.SetInformationShortMessage(Aurora.Booking.Services.BookingRequest.ReturnWarning)
                        Exit Sub
                    End If

                    Call Me.SetXmlString(strResult)
                    Me.hid_html_butSel.Value = btnValue


                    Me.AddSessionForMBR()
                    Response.Redirect(QS_RS_RNTDUPLST)
                End If

            Else

                Dim validator As BaseValidator
                For Each validator In Page.GetValidators("Book")
                    If Not validator.IsValid Then
                        validator.FindControl(validator.ControlToValidate).Focus()
                        MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                        Exit For
                    End If
                Next

            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try

    End Sub

    Protected Sub btnNewBooking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewBooking.Click
        Me.RemoveSessionForNBR()
        Response.Redirect(QS_RS_VEHREQMGTNBR)
    End Sub

    'JL 2008/06/17
    Protected Sub updateAgentButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateAgentButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then
            Me.SetErrorShortMessage("No Valid agent has been specified in the Agent field")
        Else

            Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
            Dim dt As New DataTable
            dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
            Dim agnIsMisc As Boolean = False
            If dt.Rows.Count > 0 And dt.Columns.Contains("ISMISC") Then
                agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
            End If

            If agnIsMisc Then
                bookingRequestAgentMgtPopupUserControl1.BookingId = BkrID
                bookingRequestAgentMgtPopupUserControl1.loadPopup()
            Else
                Response.Redirect("~/Agent/Maintenance.aspx?agnId=" & PickerControlForAgent.DataId)
            End If
        End If

    End Sub

    'JL 2008/06/17
    Protected Sub addAgentLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addAgentButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.Text) Then
            Response.Redirect("~/Agent/Maintenance.aspx?agnId=")
        Else
            If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then

                Dim agentName As String = ""

                agentName = PickerControlForAgent.Text
                Dim ds As DataSet
                ds = Booking.ShowBookingRequestAgent(agentName)

                If ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 OrElse ds.Tables(0).Rows(0)(0) = "Error" Then
                    Response.Redirect("~/Agent/Maintenance.aspx?agnId=")
                Else
                    showBookingRequestAgentPopupUserControl1.loadPopup(PickerControlForAgent.Text)
                End If

            Else
                Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
                Dim dt As New DataTable
                dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
                Dim agnIsMisc As Boolean = False
                If dt.Rows.Count > 0 Then
                    agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
                End If

                If agnIsMisc Then
                    bookingRequestAgentMgtPopupUserControl1.BookingId = BkrID
                    bookingRequestAgentMgtPopupUserControl1.loadPopup()
                    Return
                Else
                    showBookingRequestAgentPopupUserControl1.loadPopup(PickerControlForAgent.Text)
                    Return
                End If
            End If

        End If

    End Sub

    'JL 2008/06/17
    Protected Sub agentContactImageButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles agentContactImageButton.Click

        If String.IsNullOrEmpty(PickerControlForAgent.DataId) Then

            'JL 2008/06/17
            'empty the ddlcontact items
            ddlContact.DataSource = Nothing
            ddlContact.DataBind()

            SetShortMessage(AuroraHeaderMessageType.Information, "No Valid agent has been specified in the Agent field")

        Else
            Dim agentCode As String = PickerControlForAgent.Text.Split("-")(0).Trim
            Dim dt As New DataTable
            dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
            Dim agnIsMisc As Boolean = False
            If dt.Rows.Count > 0 Then
                agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
            End If

            If agnIsMisc Then
                'JL 2008/06/17
                'empty the ddlcontact items
                ddlContact.DataSource = Nothing
                ddlContact.DataBind()

                SetShortMessage(AuroraHeaderMessageType.Information, "No list contact is available  for a miscellaneous agent.\nEnter or update the contact name via the Agent details(folder icon)")
            Else
                ContactAgentMgtPopupUserControl1.AgentId = PickerControlForAgent.DataId
                ContactAgentMgtPopupUserControl1.LoadPopup()
            End If
        End If

    End Sub

    'Protected Sub txtAdult_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAdult.TextChanged
    '    Me.SetFocusToControl(Me.txtchildren)
    'End Sub

    'Protected Sub txtchildren_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtchildren.TextChanged
    '    Me.SetFocusToControl(Me.txtinfants)
    'End Sub

    'Protected Sub txtinfants_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtinfants.TextChanged
    '    Me.SetFocusToControl(Me.PickerControlForPackage)
    'End Sub

#End Region

#Region "popup postback"
    'JL 2008/06/17
    Protected Sub showBookingRequestAgentPopupUserControl1_PostBack(ByVal sender As Object, ByVal addButton As Boolean, ByVal selectedAgent As Boolean, ByVal param As Object) Handles showBookingRequestAgentPopupUserControl1.PostBack

        If addButton Then
            Response.Redirect("~/Agent/Maintenance.aspx?agnId=")

        ElseIf selectedAgent Then

            Dim list As ArrayList
            list = CType(param, ArrayList)

            Dim agentId As String = Utility.ParseString(list.Item(0), "")
            Dim agentCode As String = Utility.ParseString(list.Item(1), "")
            Dim agentName As String = Utility.ParseString(list.Item(2), "")

            PickerControlForAgent.DataId = agentId
            PickerControlForAgent.Text = agentCode & " - " & agentName

        End If

    End Sub

    'JL 2008/06/17
    Protected Sub bookingRequestAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal saveButton As Boolean, ByVal backButton As Boolean, ByVal param As Object) Handles bookingRequestAgentMgtPopupUserControl1.PostBack
        If saveButton Then
        End If
    End Sub

    Protected Sub ContactAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles ContactAgentMgtPopupUserControl1.PostBack
        Dim requireRefresh As Boolean
        requireRefresh = Utility.ParseBoolean(param, True)

        If requireRefresh Then
            ddlContact_DataBinding(PickerControlForAgent.DataId)
        End If
    End Sub


    Private Sub ddlContact_DataBinding(ByVal agentid As String)

        If Not String.IsNullOrEmpty(agentid) Then
            Dim xmlDoc As XmlDocument
            xmlDoc = Data.ExecuteSqlXmlSPDoc("RES_getContact", "AGENT", agentid, "Reservation", "")
            Dim ds As DataSet = New DataSet()
            ds.ReadXml(New XmlNodeReader(xmlDoc))

            If ds.Tables.Count > 0 Then
                ddlContact.DataSource = ds.Tables(0)
                ddlContact.DataTextField = "ConName"
                ddlContact.DataValueField = "ConId"
                ddlContact.DataBind()

                ddlContact.Items.Add(New ListItem("", ""))
            End If
        Else
            ddlContact.DataSource = Nothing
            ddlContact.DataBind()
        End If

    End Sub




#End Region

End Class

#Region "Unused"
'postBackOut = Me.Page.ClientScript.GetPostBackEventReference(Me.DateControlForCheckOut, "OUT")
'postBackIn = Me.Page.ClientScript.GetPostBackEventReference(Me.DateControlForCheckIn, "IN")


'CType(Me.PickerControlForAgent.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "getContacts('" + Me.PickerControlForAgent.DataId + "');")
'Me.ddlContact.Attributes.Add("onblur", "document.getElementById('" & Me.txtRef.ClientID & "').focus();")
'Me.ddlContact.Attributes.Add("onchange", "document.getElementById('ctl00_ContentPlaceHolder_hidContactID').value= this.value + '|' + document.getElementById('ctl00_ContentPlaceHolder_ddlContact').options[document.getElementById('ctl00_ContentPlaceHolder_ddlContact').options.selectedIndex].text;")

'Me.txtRef.Attributes.Add("onblur", "document.getElementById('" & Me.btnNewAgent.ClientID & "').focus();")
'Me.btnNewAgent.Attributes.Add("onblur", "document.getElementById('" & Me.PickerControlForProduct.ClientID & "').focus();")
'CType(Me.PickerControlForProduct.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetProductID('" & Me.CurrentUserCode & "'); document.getElementById('" & Me.txtNoOfVehicles.ClientID & "').focus();")


'Me.txtNoOfVehicles.Attributes.Add("onblur", "CalculateDays();GetPackageXML();document.getElementById('" & Me.ddlRequestSource.ClientID & "').focus();")
'Me.ddlRequestSource.Attributes.Add("onblur", "document.getElementById('" & Me.PickerControlForCheckOut.ClientID & "').focus();")

'CType(Me.PickerControlForCheckOut.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.DateControlForCheckOut.ClientID & "').focus();")
'CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).Attributes.Add("onchange", "getDayofWeek(2,1);GetPackageXML(); ")
'CType(Me.DateControlForCheckOut.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); " & postBackOut & "; document.getElementById('" & Me.ddlAMPMcheckout.ClientID & "').focus();")
'Me.ddlAMPMcheckout.Attributes.Add("onblur", "document.getElementById('" & Me.PickerControlForCheckIn.ClientID & "').focus();")


'CType(Me.PickerControlForCheckIn.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.DateControlForCheckIn.ClientID & "').focus();")
'CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).Attributes.Add("onchange", "getDayofWeek(2,2); GetPackageXML(); ")
'CType(Me.DateControlForCheckIn.FindControl("dateTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();  " & postBackIn & "; document.getElementById('" & Me.ddlAMPMcheckin.ClientID & "').focus();")
'Me.ddlAMPMcheckin.Attributes.Add("onblur", "document.getElementById('" & Me.txtHirePeriod.ClientID & "').focus();")

'Me.txtHirePeriod.Attributes.Add("onblur", "CalculateDays();GetPackageXML(); document.getElementById('" & Me.ddlDays.ClientID & "').focus();")
'Me.ddlDays.Attributes.Add("onblur", "document.getElementById('" & Me.btnCalculate.ClientID & "').focus();")
'Me.btnCalculate.Attributes.Add("onblur", "document.getElementById('" & Me.txtSurname.ClientID & "').focus();")

'txtSurname.Attributes.Add("onblur", "document.getElementById('" & Me.txtfirstName.ClientID & "').focus();")
'txtfirstName.Attributes.Add("onblur", "document.getElementById('" & Me.ddltitle.ClientID & "').focus();")
'Me.ddltitle.Attributes.Add("onblur", "document.getElementById('" & txtAdult.ClientID & "').focus();")


'Me.txtAdult.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.txtchildren.ClientID & "').focus();")
'Me.txtchildren.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.txtinfants.ClientID & "').focus();")
'Me.txtinfants.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.PickerControlForPackage.ClientID & "').focus();")


'CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();")
'CType(Me.PickerControlForPackage.FindControl("pickerTextBox"), TextBox).Attributes.Add("onblur", "GetPackageXML();")
'CType(Me.PickerControlForPackage.FindControl("pickerImage"), Image).Attributes.Add("onfocus", "GetPackageXML();")


''Me.txtNoOfVehicles.Attributes.Add("onblur", "GetPackageXML();")
''Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")
''Me.ddlRequestSource.Attributes.Add("onblur", "GetPackageXML();")

''Me.ddlAMPMcheckout.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.PickerControlForCheckIn.ClientID & "').focus();")
''Me.ddlAMPMcheckin.Attributes.Add("onblur", "GetPackageXML();  document.getElementById('" & Me.txtHirePeriod.ClientID & "').focus();")


''Me.ddlDays.Attributes.Add("onblur", "GetPackageXML();")
''Me.txtSurname.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.txtchildren.ClientID & "').focus();")
''Me.txtchildren.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.txtinfants.ClientID & "').focus();")
''Me.txtinfants.Attributes.Add("onblur", "GetPackageXML(); document.getElementById('" & Me.PickerControlForPackage.ClientID & "').focus();")

'Me.btnRequestBrochure.Attributes.Add("onblur", "document.getElementById('" & Me.PickerControlForAgent.ClientID & "').focus();")
'Me.btnCalculate.Attributes.Add("onblur", "document.getElementById('" & Me.txtSurname.ClientID & "').focus();")


#End Region

