''rev:mia Oct 16 2014 - addition of PromoCode
''                    - RES_manageRental                                            - addition of PromoCode
''                    - RES_getForModifyRental                                      - addition of PromoCode
''                    - Aurora.Booking.Data\DataRepository.vb                           
''                    - Aurora.Booking.Services\Rental.vb                           
''---------------------------------------------------------
''rev:mia 11nov2014 - slot management
''                  - RES_manageRental
''                  - getAvailableSlotForRntIdOrAvp
''                  - Aurora.Booking.Data\DataRepository
''                  - Aurora.Booking.Services\Rental.vb                           

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Drawing

<AuroraPageTitleAttribute("Modify Rental :  Booking:")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN046")> _
Partial Class Booking_RentalEdit
    Inherits AuroraPage

    Dim bookingId As String = ""
    Dim rentalId As String = ""
    Dim bookingNumber As String = ""
    Dim isFromSearch As String = "0"
    'Delegate Sub DelPopupPostBackObject(ByVal requireRefresh As Boolean)

    Private Const RentalEdit_AgentId_ViewState As String = "RentalEdit_AgentId"
    Private Const RentalEdit_KnockBack_ViewState As String = "RentalEdit_KnockBack"
    Private Const RentalEdit_RntIntNo_ViewState As String = "RentalEdit_RntIntNo"
    Private Const RentalEdit_BooIntNo_ViewState As String = "RentalEdit_BooIntNo"
    Private Const RentalEdit_AddWhen_ViewState As String = "RentalEdit_AddWhen"
    Private Const RentalEdit_RntCkoLocCode_ViewState As String = "RentalEdit_RntCkoLocCode"
    Private Const RentalEdit_TodaysDate_ViewState As String = "RentalEdit_TodaysDate"

    Private Const RentalEdit_CkiWhen_ViewState As String = "RentalEdit_CkiWhen"
    Private Const RentalEdit_CkoWhen_ViewState As String = "RentalEdit_CkoWhen"

    Private Const RentalEdit_ArrivalDetailsId_ViewState As String = "RentalEdit_ArrivalDetailsId"
    Private Const RentalEdit_DepartureDetailsId_ViewState As String = "RentalEdit_DepartureDetailsId"

    Private Const RentalEdit_ArrivalDetailsIntegrity_ViewState As String = "RentalEdit_ArrivalDetailsIntegrity"
    Private Const RentalEdit_DepartureDetailsIntegrity_ViewState As String = "RentalEdit_DepartureDetailsIntegrity"

    Private Const RentalEdit_ArrivalDetailsNoteIntegrity_ViewState As String = "RentalEdit_ArrivalDetailsNoteIntegrity"
    Private Const RentalEdit_DepartureDetailsNoteIntegrity_ViewState As String = "RentalEdit_DepartureDetailsNoteIntegrity"

    Private Const RentalEdit_ArrivalDetailsAddressIntegrity_ViewState As String = "RentalEdit_ArrivalDetailsAddressIntegrity"
    Private Const RentalEdit_DepartureDetailsAddressIntegrity_ViewState As String = "RentalEdit_DepartureDetailsAddressIntegrity"

    ''rev:mia 10nov2014 - slot management
    Private _hasSlot As Boolean
    Property HasSlot As Boolean
        Get
            Return ViewState("_hasSlot")
        End Get
        Set(value As Boolean)
            ViewState("_hasSlot") = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        PopulateDayTimeList(ddlArrivalTime)
        PopulateDayTimeList(ddlDepartureTime)

        Dim venueTypes As DataTable = Rental.GetVenueTypes()

        BindVenueList(lstArrivalVenue, venueTypes)
        BindVenueList(lstDepartureVenue, venueTypes)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3287253"
            'rentalId = "3287253-1"
            'bookingId = "3285499"
            'rentalId = "3285499-1"


            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If
            If Not Request.QueryString("hdBookingNum") Is Nothing Then
                bookingNumber = Request.QueryString("hdBookingNum").ToString()
            End If
            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearch = Request.QueryString("isFromSearch").ToString()
            End If

            Me.SetValueLink(bookingNumber, "~\Booking\BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch)

            If Not Page.IsPostBack Then

                ''rev:mia 10nov2014 - slot management
                PopulateAvailableSlot(rentalId)

                rentalSourceDropDownList_DataBinding()
                agentContactDropDownList_DataBinding()
                dataBindRentalDetail()


            End If


        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub dataBindRentalDetail()

        ' 1 rentalGridView
        Dim dtRental As DataTable
        dtRental = Rental.GetForModifyRental(bookingId, rentalId, UserCode)

        'If dsRental.Tables.Count = 1 Then
        If dtRental.Rows.Count > 0 Then

            'rentalGridView
            Dim dv As DataView
            dv = New DataView(dtRental)
            dv.RowFilter = "RntId = '" & rentalId & "'"
            rentalGridView.DataSource = dv
            rentalGridView.DataBind()

            Dim dr As DataRow
            dr = dv.Item(0).Row
            ''System.Diagnostics.Debug.WriteLine(dr("RntArrivalAtBranchDateTime"))
            'agentLabel.Text = dr.Item("AgnName")
            'hirerLabel.Text = dr.Item("Hirer")
            'statusLabel.Text = dr.Item("BookingSt")
            'initialStatusLabel.Text = dr.Item("Status")

            ''rev:mia july 4 2012 - start-- addition of 3rd party field
            Me.txtthirdParty.Text = dr.Item("thirdparty")
            ''rev:mia Oct 16 2014 - addition of PromoCode
            Me.promocodeTextbox.Text = HttpUtility.HtmlDecode(dr.Item("RntPromoCode"))

            agentTextBox.Text = dr.Item("AgnName")
            hirerTextBox.Text = dr.Item("Hirer")
            statusTextBox.Text = dr.Item("BookingSt")
            initialStatusTextBox.Text = dr.Item("IniStatus")

            'packagePickerControl
            packagePickerControl.DataId = dr.Item("PkgId")
            packagePickerControl.Text = dr.Item("Package")
            ''rev:mia 13jan2014-addition of supervisor override password
            OriginalPackage = String.Concat(packagePickerControl.DataId, "|", packagePickerControl.Text)

            packagePickerControl.Param3 = dr.Item("AgnId") & "," & dr.Item("PrdId") & "," & dr.Item("RntCkoWhen") & "," & _
                dr.Item("RntCkoLocCode") & "," & dr.Item("RntCkiLocCode") & "," & dr.Item("NoOfAd") & "," & _
                dr.Item("NoOfCh") & "," & dr.Item("NoOfIn") & "," & dr.Item("RntCkiWhen") & "," & dr.Item("RntId")

            'Connections
            arrRefTextBox.Text = dr.Item("ArrRef")
            'arrDateTextBox.Text = dr.Item("ArrDate")
            If dr.Item("ArrDate") <> "" Then
                arrDateControl.Date = Utility.ParseDateTime(dr.Item("ArrDate"), Utility.SystemCulture)
            End If

            arrTimeControl.Text = dr.Item("ArrTm")
            depRefTextBox.Text = dr.Item("DeptRef")
            'depDateTextBox.Text = dr.Item("DeptDate")
            If dr.Item("DeptDate") <> "" Then
                depDateControl.Date = Utility.ParseDateTime(dr.Item("DeptDate"), Utility.SystemCulture)
            End If
            depTimeControl.Text = dr.Item("DeptTm")

            'Customers
            adultTextBox.Text = dr.Item("NoOfAd")
            childrenTextBox.Text = dr.Item("NoOfCh")
            infantsTextBox.Text = dr.Item("NoOfIn")
            vipRentalCheckBox.Checked = dr.Item("IsVIP")
            refCustomerCheckBox.Checked = dr.Item("RepeatCus")
            partOfConvoyCheckBox.Checked = dr.Item("IsCon")

            'Agent Details
            agentContactDropDownList.SelectedValue = dr.Item("Contact")
            'agentIdLabel.Text = dr.Item("AgnId")
            ViewState(RentalEdit_AgentId_ViewState) = dr.Item("AgnId")
            agentRefTextBox.Text = dr.Item("AgnRef")
            agentVoucherNoTextBox.Text = dr.Item("VoucNum")

            'Dates
            If dr.Item("BookedWhen") <> "" Then
                'bookedDateControl.Date = Utility.ParseDateTime(dr.Item("BookedWhen"), Utility.SystemCulture)
                bookedDateControl.Date = Utility.DateDBParse(dr.Item("BookedWhen"))
            End If

            rentalSourceDropDownList.SelectedValue = dr.Item("RntSource")
            'followUpDateTextBox.Text = dr.Item("FollowUpWhen")
            followUpDateTextBox.Text = Utility.DateDBUIConvert(dr.Item("FollowUpWhen"))

            'triggerTextBox.Text = dr.Item("TriggerDate")
            triggerTextBox.Text = Utility.DateDBUIConvert(dr.Item("TriggerDate"))
            oldRentalNumTextBox.Text = dr.Item("OldRntNum")

            'transferredTextBox.Text = dr.Item("Transferred")
            transferredTextBox.Text = Utility.DateDBUIConvert(dr.Item("Transferred"))

            ' knockBackLabel.Text = dr.Item("KnockBack")
            ViewState(RentalEdit_KnockBack_ViewState) = dr.Item("KnockBack")
            ViewState(RentalEdit_RntIntNo_ViewState) = dr.Item("RntIntNo")
            ViewState(RentalEdit_BooIntNo_ViewState) = dr.Item("BooIntNo")
            ViewState(RentalEdit_AddWhen_ViewState) = dr.Item("AddWhen")
            ViewState(RentalEdit_RntCkoLocCode_ViewState) = dr.Item("RntCkoLocCode")
            ViewState(RentalEdit_TodaysDate_ViewState) = dr.Item("TodaysDate")

            ' 2 otherRentalGridView
            Dim dsBooking As DataSet
            dsBooking = Booking.GetBookingDetail(bookingId, "RentalRecord", "", "", UserCode)

            Dim dvBooking As DataView
            dvBooking = New DataView(dsBooking.Tables(0))
            dvBooking.RowFilter = "RntId <> '' and RntId <> '" & rentalId & "'"

            otherRentalGridView.DataSource = dvBooking
            otherRentalGridView.DataBind()

            If dvBooking.Count > 0 Then
                detailsCollapsiblePanel.Visible = True
                detailsContentPanel.Visible = True
            Else
                detailsCollapsiblePanel.Visible = False
                detailsContentPanel.Visible = False
            End If

            SetGridViewStatusColor()

            ''rev:mia 10nov2014 - slot management
            If (Not hasSlot) Then
                Dim arrivalValue As String = DirectCast(dr("RntArrivalAtBranchDateTime"), String)
                If arrivalValue <> "" Then
                    Dim itemArrival As ListItem = ddlArrivalTime.Items.FindByValue(CDate(arrivalValue).TimeOfDay.TotalMinutes)
                    If (itemArrival Is Nothing) Then
                        ddlArrivalTime.SelectedValue = GetNearestTime(CDate(arrivalValue))
                    Else
                        ddlArrivalTime.SelectedValue = CDate(arrivalValue).TimeOfDay.TotalMinutes
                    End If
                End If
            End If
            

            Dim dropOffValue As String = DirectCast(dr("RntDropOffAtBranchDateTime"), String)
            If dropOffValue <> "" Then

                Dim itemDrop As ListItem = ddlDepartureTime.Items.FindByValue(CDate(dropOffValue).TimeOfDay.TotalMinutes)
                If (itemDrop Is Nothing) Then
                    ddlDepartureTime.SelectedValue = GetNearestTime(CDate(dropOffValue))
                Else
                    ddlDepartureTime.SelectedValue = CDate(dropOffValue).TimeOfDay.TotalMinutes
                End If
            End If

            ViewState(RentalEdit_CkiWhen_ViewState) = dr("RntCkiWhen")
            ViewState(RentalEdit_CkoWhen_ViewState) = dr("RntCkoWhen")

            DataBindArrivalDepratureDetails(rentalId, dr("RntCkoWhen"), dr("CtyCode"), dr("CheckoutTown"), _
                                            dr("RntCkiWhen"), dr("CheckinCountryCode"), dr("CheckinTown"))
        End If

    End Sub

    Protected Sub DataBindArrivalDepratureDetails(ByVal rentalId As String, _
                                                  ByVal defaultArrivalDate As DateTime, ByVal defaultArrivalCountry As String, ByVal defaultArrivalTown As String, _
                                                  ByVal defaultDepartureDate As DateTime, ByVal defaultDepartureCountry As String, ByVal defaultDepartureTown As String)

        Dim arrivalDepartureDetails As DataTable = Rental.GetArrivalDepartureDetails(rentalId)

        Dim arrivalData As DataRow = Nothing
        Dim departureData As DataRow = Nothing
        Dim dtPickupDate As Nullable(Of DateTime)

        If arrivalDepartureDetails.Rows.Count <> 0 Then
            If arrivalDepartureDetails.Rows(0)("RntArrDepType") = "arrival" Then
                arrivalData = arrivalDepartureDetails.Rows(0)
                If arrivalDepartureDetails.Rows.Count > 1 Then
                    departureData = arrivalDepartureDetails.Rows(1)
                End If
            Else
                departureData = arrivalDepartureDetails.Rows(0)
            End If
        End If

        'ARRIVAL DATA

        If arrivalData IsNot Nothing Then
            ViewState(RentalEdit_ArrivalDetailsId_ViewState) = GetString(arrivalData("RntArrDepId"))
            ViewState(RentalEdit_ArrivalDetailsIntegrity_ViewState) = GetString(arrivalData("ArrivalDepartureIntegrityNo"))
            ViewState(RentalEdit_ArrivalDetailsNoteIntegrity_ViewState) = GetString(arrivalData("NoteIntegrityNo"))
            ViewState(RentalEdit_ArrivalDetailsAddressIntegrity_ViewState) = GetString(arrivalData("AddressIntegrityNo"))
        End If


        If arrivalData IsNot Nothing _
            AndAlso Not AreAllEmpty(arrivalData, "RntArrDepPickupDateTime", "RntArrDepVenueCodTypeId", _
                                    "RntArrDepVenueName", "AddAddress1", "AddAddress2", "AddAddress3", _
                                    "AddState", "AddPostcode", "CountryName", "NteDesc") Then

            dtPickupDate = GetDate(arrivalData("RntArrDepPickupDateTime"))


            If dtPickupDate.HasValue Then
                dateArrivalPickup.Date = dtPickupDate
                timeArrivalPickup.Time = dtPickupDate.Value.TimeOfDay
            End If

            lstArrivalVenue.SelectedValue = GetString(arrivalData("RntArrDepVenueCodTypeId"))
            txtArrivalVenueName.Text = GetString(arrivalData("RntArrDepVenueName"))
            txtArrivalStreet.Text = GetString(arrivalData("AddAddress1"))
            txtArrivalSuburb.Text = GetString(arrivalData("AddAddress2"))
            txtArrivalCity.Text = GetString(arrivalData("AddAddress3"))
            txtArrivalState.Text = GetString(arrivalData("AddState"))
            txtArrivalPostCode.Text = GetString(arrivalData("AddPostcode"))
            pkrArrivalCountry.Text = GetString(arrivalData("CountryName"))
            txtArrivalSpecialInstructions.Text = GetString(arrivalData("NteDesc"))
        Else
            dateArrivalPickup.Date = defaultArrivalDate
            txtArrivalCity.Text = defaultArrivalTown
            pkrArrivalCountry.Text = defaultArrivalCountry
        End If


        'DEPARTURE DATA
        If departureData IsNot Nothing Then
            ViewState(RentalEdit_DepartureDetailsId_ViewState) = GetString(departureData("RntArrDepId"))

            ViewState(RentalEdit_DepartureDetailsIntegrity_ViewState) = GetString(departureData("ArrivalDepartureIntegrityNo"))
            ViewState(RentalEdit_DepartureDetailsNoteIntegrity_ViewState) = GetString(departureData("NoteIntegrityNo"))
            ViewState(RentalEdit_DepartureDetailsAddressIntegrity_ViewState) = GetString(departureData("AddressIntegrityNo"))
        End If



        If departureData IsNot Nothing _
            AndAlso Not AreAllEmpty(departureData, "RntArrDepPickupDateTime", "RntArrDepVenueCodTypeId", _
                                    "RntArrDepVenueName", "AddAddress1", "AddAddress2", "AddAddress3", _
                                    "AddState", "AddPostcode", "CountryName", "NteDesc") Then

            dtPickupDate = GetDate(departureData("RntArrDepPickupDateTime"))

            If dtPickupDate.HasValue Then
                dateDeparturePickup.Date = dtPickupDate
                timeDeparturePickup.Time = dtPickupDate.Value.TimeOfDay
            End If


            lstDepartureVenue.SelectedValue = GetString(departureData("RntArrDepVenueCodTypeId"))
            txtDepartureVenueName.Text = GetString(departureData("RntArrDepVenueName"))
            txtDepartureStreet.Text = GetString(departureData("AddAddress1"))
            txtDepartureSuburb.Text = GetString(departureData("AddAddress2"))
            txtDepartureCity.Text = GetString(departureData("AddAddress3"))
            txtDepartureState.Text = GetString(departureData("AddState"))
            txtDeparturePostCode.Text = GetString(departureData("AddPostcode"))
            pkrDepartureCountry.Text = GetString(departureData("CountryName"))
            txtDepartureSpecialInstructions.Text = GetString(departureData("NteDesc"))
        Else
            dateDeparturePickup.Date = defaultDepartureDate
            txtDepartureCity.Text = defaultDepartureTown
            pkrDepartureCountry.Text = defaultDepartureCountry
        End If

    End Sub

    Protected Function AreAllEmpty(ByVal row As DataRow, ByVal ParamArray columnNames() As String) As Boolean
        For i As Integer = 0 To columnNames.Length - 1 Step 1
            Dim value As Object = row(columnNames(i))
            If value IsNot Nothing AndAlso Not Object.ReferenceEquals(value, DBNull.Value) _
                AndAlso Not (value.GetType() Is GetType(String) AndAlso DirectCast(value, String) = "") Then
                Return False
            End If
        Next

        Return True
    End Function

    Protected Function GetDate(ByVal value As Object) As Nullable(Of DateTime)
        If value Is Nothing OrElse Object.ReferenceEquals(value, DBNull.Value) Then
            Return Nothing
        End If

        Return Convert.ToDateTime(value)
    End Function

    Protected Function GetString(ByVal value As Object) As String
        If value Is Nothing OrElse Object.ReferenceEquals(value, DBNull.Value) Then
            Return Nothing
        End If

        Return Convert.ToString(value)
    End Function

    Protected Sub BindVenueList(ByVal list As DropDownList, ByVal venueTypes As DataTable)
        list.DataSource = venueTypes
        list.DataTextField = "CodCode"
        list.DataValueField = "CodId"
        list.DataBind()
        list.Items.Insert(0, "")
    End Sub

    Protected Sub PopulateDayTimeList(ByVal dropDownList As DropDownList)
        Dim dtTimeOfDay As DateTime
        Dim strTime As String
        dropDownList.Items.Clear()
        dropDownList.Items.Add("")
        For intMinutes As Integer = 0 To 1425 Step 15
            strTime = dtTimeOfDay.ToString("HH:mm")
            dropDownList.Items.Add(New ListItem(strTime, intMinutes))

            dtTimeOfDay = dtTimeOfDay.AddMinutes(15)
        Next
    End Sub

    Protected Function GetCombinedDateParts(ByVal dateValue As String, ByVal timeValue As String) As Nullable(Of DateTime)
        Dim dtDate As Nullable(Of DateTime) = GetDateTimeFromString(dateValue)
        Dim intMinutes As Nullable(Of Integer) = GetMinutesFromString(timeValue)

        If Not intMinutes.HasValue OrElse Not dtDate.HasValue Then
            Return Nothing
        End If

        Return dtDate.Value.Date.AddMinutes(intMinutes)
    End Function

    Protected Function GetDateTimeFromString(ByVal value As String) As Nullable(Of DateTime)

        'Doing .NET 4's equivalent of string.IsNullOrWhiteSpace (missing in .NET 3.5 and earlier)
        If value Is Nothing Then
            Return Nothing 'Return a value equivalent to null
        Else
            value = value.Trim()
            If value.Length = 0 Then
                Return Nothing
            End If
        End If

        Dim dtDate As DateTime

        If Not DateTime.TryParse(value, dtDate) Then
            Return Nothing
        End If

        Return dtDate
    End Function


    Protected Function GetMinutesFromString(ByVal value As String) As Nullable(Of Integer)

        'Doing .NET 4's equivalent of string.IsNullOrWhiteSpace (missing in .NET 3.5 and earlier)
        If value Is Nothing Then
            Return Nothing 'Return a value equivalent to null
        Else
            value = value.Trim()
            If value.Length = 0 Then
                Return Nothing
            End If
        End If

        Dim intMinutes As Integer

        If Not Integer.TryParse(value, intMinutes) OrElse intMinutes < 0 Then
            Return Nothing
        End If

        Return intMinutes
    End Function

    Public Shared Function GetNotification() As DataTable
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetCodecodetype("11", "")
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function


    Protected Function GetRentalUrl(ByVal rentalId As Object) As String
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(rentalId)

        Return "./RentalEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalIdString & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch
    End Function

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        ''rev:mia issue # 554 Modify Booking: page - two missing buttons and <Back> button not functioning as expected
        Dim tempActiveTab As String = Request.QueryString("activeTab")
        tempActiveTab = IIf(String.IsNullOrEmpty(tempActiveTab), "", "&activeTab=" & tempActiveTab)
        Response.Redirect("./Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&isFromSearch=" & isFromSearch & tempActiveTab)
    End Sub


    Protected Sub rentalSourceDropDownList_DataBinding()
        rentalSourceDropDownList.DataSource = GetNotification()
        rentalSourceDropDownList.DataTextField = "CODE"
        rentalSourceDropDownList.DataValueField = "ID"
        rentalSourceDropDownList.DataBind()

        rentalSourceDropDownList.Items.Add(New ListItem("", ""))

    End Sub

    Protected Sub agentContactDropDownList_DataBinding()
        agentContactDropDownList.DataSource = Aurora.Common.Service.GetPopUpData("CONTACTPOPUP", bookingId, "", "", "", "", UserCode)
        agentContactDropDownList.DataTextField = "DESCRIPTION"
        agentContactDropDownList.DataValueField = "CODE"
        agentContactDropDownList.DataBind()

        agentContactDropDownList.Items.Add(New ListItem("", ""))
    End Sub

    Protected Sub bookedDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bookedDateControl.DateChanged

        If Not (bookedDateControl.IsValid And arrDateControl.IsValid And depDateControl.IsValid) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            Return
        End If

        Dim addWhen As DateTime

        If ViewState(RentalEdit_AddWhen_ViewState) <> "" Then
            addWhen = Utility.ParseDateTime(ViewState(RentalEdit_AddWhen_ViewState), Utility.SystemCulture)
            If bookedDateControl.Date > addWhen Then
                SetWarningMessage("The Booked Date is later than the date the rental was added")
            End If
        End If

        Dim followupDateString As String = Rental.RntGetFollowUpDate(bookedDateControl.Date, ViewState(RentalEdit_RntCkoLocCode_ViewState), initialStatusTextBox.Text)
        'followUpDateTextBox.Text = followupDateString
        followUpDateTextBox.Text = Utility.DateDBUIConvert(followupDateString)

        Dim followupDate As Date
        followupDate = Utility.ParseDateTime(followupDateString)

        Dim todaysDate As Date = Utility.ParseDateTime(ViewState(RentalEdit_TodaysDate_ViewState))

        If followupDate < todaysDate Then
            SetShortMessage(AuroraHeaderMessageType.Information, "Please note: Follow Up date has already passed.")
        End If

    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        PreSave()

    End Sub

    Protected Function updateArrivalDepartureDetails() As Boolean

        Dim arrivalDetailsId As Nullable(Of Long) = GetNullableInt64(DirectCast(ViewState(RentalEdit_ArrivalDetailsId_ViewState), String))

        Dim arrivalIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_ArrivalDetailsIntegrity_ViewState), String), arrivalIntegrityNo)
        Dim arrivalNoteIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_ArrivalDetailsNoteIntegrity_ViewState), String), arrivalNoteIntegrityNo)
        Dim arrivalAddressIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_ArrivalDetailsAddressIntegrity_ViewState), String), arrivalAddressIntegrityNo)

        Dim departureDetailsId As Nullable(Of Long) = GetNullableInt64(DirectCast(ViewState(RentalEdit_DepartureDetailsId_ViewState), String))

        Dim departureIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_DepartureDetailsIntegrity_ViewState), String), departureIntegrityNo)
        Dim departureNoteIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_DepartureDetailsNoteIntegrity_ViewState), String), departureNoteIntegrityNo)
        Dim departureAddressIntegrityNo As Integer
        Integer.TryParse(DirectCast(ViewState(RentalEdit_DepartureDetailsAddressIntegrity_ViewState), String), departureAddressIntegrityNo)

        If Not updateArrivalDepartureDetails( _
                Rental.ArrivalDepartureType.Arrival, _
                arrivalDetailsId, _
                dateArrivalPickup, _
                timeArrivalPickup, _
                lstArrivalVenue.SelectedValue, _
                txtArrivalVenueName.Text, _
                txtArrivalStreet.Text, _
                txtArrivalSuburb.Text, _
                txtArrivalCity.Text, _
                txtArrivalState.Text, _
                txtArrivalPostCode.Text, _
                pkrArrivalCountry.DataId, _
                txtArrivalSpecialInstructions.Text, _
                arrivalIntegrityNo, _
                arrivalNoteIntegrityNo, _
                arrivalAddressIntegrityNo _
            ) _
            OrElse Not updateArrivalDepartureDetails( _
                Rental.ArrivalDepartureType.Departure, _
                departureDetailsId, _
                dateDeparturePickup, _
                timeDeparturePickup, _
                lstDepartureVenue.SelectedValue, _
                txtDepartureVenueName.Text, _
                txtDepartureStreet.Text, _
                txtDepartureSuburb.Text, _
                txtDepartureCity.Text, _
                txtDepartureState.Text, _
                txtDeparturePostCode.Text, _
                pkrDepartureCountry.DataId, _
                txtDepartureSpecialInstructions.Text, _
                departureIntegrityNo, _
                departureNoteIntegrityNo, _
                departureAddressIntegrityNo _
                ) Then
            Return False
        End If

        Return True
    End Function

    Protected Function updateArrivalDepartureDetails(ByVal type As Rental.ArrivalDepartureType, ByVal id As Nullable(Of Long), _
        ByVal pickupDate As ASP.usercontrols_datecontrol_datecontrol_ascx, ByVal pickupTime As ASP.usercontrols_timecontrol_timecontrol_ascx, _
        ByVal venueTypeId As String, ByVal venueName As String, ByVal street As String, ByVal suburb As String, _
        ByVal city As String, ByVal state As String, ByVal postCode As String, ByVal country As String, ByVal instructions As String, _
        ByVal integrityNo As Integer, ByVal noteIntegrityNo As Integer, ByVal addressIntegrityNo As Integer _
        ) As Boolean

        venueTypeId = EnsureNotNull(venueTypeId).Trim()

        venueName = EnsureNotNull(venueName).Trim()
        street = EnsureNotNull(street).Trim()
        suburb = EnsureNotNull(suburb).Trim()
        city = EnsureNotNull(city).Trim()
        state = EnsureNotNull(state).Trim()
        postCode = EnsureNotNull(postCode).Trim()
        country = EnsureNotNull(country).Trim()
        instructions = EnsureNotNull(instructions).Trim()

        Dim addressEnabled As Boolean = street <> "" OrElse suburb <> "" OrElse _
                                       city <> "" OrElse state <> "" OrElse postCode <> "" OrElse _
                                       country <> "" OrElse instructions <> ""

        'If either of the fields on the arrival-departure details is specified,
        'it means that the rest of the form should be filled out properly, or it cannot be accepted.
        '
        '   LOGIC:
        '   - Date and time and venue type are required AND
        '   - Either venue name OR street + suburb + country are required.



        'Verifying whether arrival details need to be processed at all
        'If Not id.HasValue _
        '    AndAlso venueTypeId = "" AndAlso venueName = "" _
        '    AndAlso Not pickupDate.IsValid AndAlso Not pickupTime.IsValid _
        '    AndAlso Not addressEnabled Then
        '    'The arrival details form is empty, no need to create

        '    Return True
        'End If

        If venueTypeId = "" AndAlso venueName = "" _
            AndAlso street = "" AndAlso Not pickupTime.IsValid Then
            'The arrival details form is empty, no need to create

            'Check if the rest of the fields are blank and there was a record previously created that needs to be blanked out now
            If Not id.HasValue Then
                Return True
            Else
                'Blank out all the fields
                Return SaveBlankArrivalDepartureDetailsData(type, id, integrityNo, noteIntegrityNo, addressIntegrityNo)
            End If


        End If

        Dim typeText As String
        If type = Rental.ArrivalDepartureType.Arrival Then
            typeText = "arrival"
        Else
            typeText = "departure"
        End If

        If instructions IsNot Nothing AndAlso instructions.Length > 2048 Then
            SetWarningMessage("The text entered into the " + typeText + " special request field is too long.")
            Return False
        End If

        'If any of the fields are specified, then:
        '   1. Make sure that, the date and time fields are set, as well as:
        '   2. At least one of the following combinations are complete (or both):
        '       - Venue type and name
        '       - OR
        '       - Address fields that are non-optional
        If (addressEnabled OrElse venueTypeId <> "" OrElse venueName <> "" OrElse pickupDate.IsValid OrElse pickupTime.IsValid) _
            AndAlso (Not pickupDate.IsValid OrElse Not pickupTime.IsValid OrElse venueTypeId = "" _
                     OrElse (venueName = "" AndAlso (street = "" OrElse country = "")) _
            ) Then

            SetWarningMessage("Information entered into the " + typeText + " details form is incomplete. At least venue type, pickup date, time and venue name or valid address must be specified or all should be left with default values.")
            Return False
        End If

        Dim pickupDateValue As Nullable(Of DateTime)

        If pickupDate.IsValid Then
            pickupDateValue = pickupDate.Date + pickupTime.Time
        Else
            pickupDateValue = Nothing
        End If

        'All seems valid - updating
        Dim errorMessage As String = Rental.ManageArrivalDepartureDetails(id, rentalId, _
            type, pickupDateValue, venueTypeId, venueName, _
            instructions, integrityNo, noteIntegrityNo, _
            UserCode, Me.Page.ToString(), _
            street, suburb, city, _
            postCode, state, country, _
            addressIntegrityNo)

        If errorMessage <> "" Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If

        Return True
    End Function

    Protected Function SaveBlankArrivalDepartureDetailsData(ByVal type As Rental.ArrivalDepartureType, ByVal id As Nullable(Of Long), _
        ByVal integrityNo As Integer, ByVal noteIntegrityNo As Integer, ByVal addressIntegrityNo As Integer _
        ) As Boolean
        'All seems valid - updating
        Dim errorMessage As String = Rental.ManageArrivalDepartureDetails(id, rentalId, _
            type, Nothing, Nothing, "", _
            "", integrityNo, noteIntegrityNo, _
            UserCode, Me.Page.ToString(), _
            "", "", "", _
            "", "", "", _
            addressIntegrityNo)

        If errorMessage <> "" Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If

        If errorMessage <> "" Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If

        Return True
    End Function

    Protected Function EnsureNotNull(ByVal value As String) As String
        If value Is Nothing Then
            Return ""
        End If
        Return value
    End Function

    Protected Function GetNullableInt64(ByVal value As String) As Nullable(Of Long)
        Dim parsed As Long

        If Long.TryParse(value, parsed) Then
            Return New Nullable(Of Long)(parsed)
        End If

        Return Nothing
    End Function

    Protected Sub agentContactImageButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles agentContactImageButton.Click
        ContactAgentMgtPopupUserControl1.AgentId = ViewState(RentalEdit_AgentId_ViewState)
        ContactAgentMgtPopupUserControl1.LoadPopup()
    End Sub

    Protected Sub ContactAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles ContactAgentMgtPopupUserControl1.PostBack
        PopupPostBack(param)
    End Sub

    Public Sub PopupPostBack(ByVal requireRefresh As Boolean)
        If requireRefresh Then
            agentContactDropDownList_DataBinding()
        End If
    End Sub

    Private Sub SetGridViewStatusColor()
        'Add color for status
        For Each r As GridViewRow In rentalGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(7).Text)))
        Next
        For Each rr As GridViewRow In otherRentalGridView.Rows
            rr.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(rr.Cells(7).Text)))
        Next

    End Sub

    Protected Function GetArrivalDepartureTime(ByVal value As Object) As String
        If value Is Nothing _
            OrElse (TypeOf value Is String _
                    AndAlso DirectCast(value, String).Trim().Length = 0 _
                    ) Then

            Return ""
        End If

        Dim dtValue As DateTime = CDate(value)

        Return " [" + dtValue.ToString("HH:mm") + "]"
    End Function

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function

    ''REV:MIA Sept.10 2012 - addition of roundoff coming from booking modify product
    ''                      - tomorrow is KZ bday
    Function GetNearestTime(ByVal timeOfDate As DateTime) As Double

        ' Sample time: 01:07:00
        Dim t As New TimeSpan(timeOfDate.Hour, timeOfDate.Minute, timeOfDate.Second)

        ' The round number, here is a quarter...
        Dim Round As Integer = 15

        ' Count of round number in this total minutes...
        Dim CountRound As Double = (t.TotalMinutes / Round)

        ' The main formula to calculate round time...
        Dim MinA As Integer = CInt(Math.Abs(CountRound + 0.5)) * Round


        ' Now show the result...
        Dim tRes As New TimeSpan(0, MinA, 0)

        Return tRes.TotalMinutes

    End Function


#Region "REV:10NOV2014 - SLOT MANAGEMENT"

    
    Private Property SlotDataSet As DataSet
        Get
            Return ViewState("SlotDataSet")
        End Get
        Set(value As DataSet)
            ViewState("SlotDataSet") = value
        End Set
    End Property

    Sub PopulateAvailableSlot(sRntId As String, Optional packageId As String = "")

        Try


            Dim result As DataSet
            If (String.IsNullOrEmpty(packageId)) Then
                result = Rental.GetAvailableSlot(sRntId, "", "", "0")
            Else
                result = Rental.GetAndValidateAvailableSlot(sRntId, String.Empty, String.Empty, 0, Date.MinValue, String.Empty, String.Empty, String.Empty, packageId, String.Empty)
            End If

            If (result.Tables.Count = -1) Then
                overrideSlotTR.Visible = False
                PopulateDayTimeList(ddlArrivalTime)
                HasSlot = False
                Exit Sub
            End If
            If (result.Tables(0).Rows.Count = 0) Then
                overrideSlotTR.Visible = False
                PopulateDayTimeList(ddlArrivalTime)
                HasSlot = False
                Exit Sub
            End If

            HasSlot = True
            arrivalslot.InnerText = "Arrival Slot:"
            arrivaltimeestimate.InnerText = ""
            overrideSlotTR.Visible = True


            ddlArrivalTime.Items.Clear()
            ddlArrivalTime.AppendDataBoundItems = True
            ddlArrivalTime.Items.Insert(0, New ListItem("--Please select--", -1))
            ddlArrivalTime.DataSource = result
            ddlArrivalTime.DataTextField = "SlotDesc"
            ddlArrivalTime.DataValueField = "SlotId"
            ddlArrivalTime.DataBind()

            Dim selectedindex As Integer = -1


            Dim NoOfBulkBookings As Integer = 1
            For Each row As DataRow In result.Tables(0).Rows
                Dim item As New ListItem
                Dim slotid As Integer = Convert.ToInt16(row("SlotId"))
                Dim allocated As Integer = 0
                Dim booked As Integer = 0
                Dim available As Integer = 0
                Dim isSlotAvailable As Integer = -1
                Dim isDefault As Integer = -1
                Dim isAlldaySlot As Integer = 0

                If (Not IsDBNull(row("IsAllDaySlot"))) Then
                    isAlldaySlot = Convert.ToInt16(row("IsAllDaySlot"))
                End If

                If (Not IsDBNull(row("AllocatedNumbers"))) Then
                    allocated = Convert.ToInt16(row("AllocatedNumbers"))
                End If

                If (Not IsDBNull(row("BookedNumbers"))) Then
                    booked = Convert.ToInt16(row("BookedNumbers"))
                End If

                If (Not IsDBNull(row("AvailableNumbers"))) Then
                    available = Convert.ToInt16(row("AvailableNumbers"))
                End If

                If (Not IsDBNull(row("IsSelected"))) Then
                    isDefault = Convert.ToInt16(row("IsSelected"))
                End If

                If (Not IsDBNull(row("IsSlotAvailable"))) Then
                    isSlotAvailable = Convert.ToInt16(row("isSlotAvailable"))
                End If


                If (CInt(row("IsSelected") = 1)) Then
                    item = ddlArrivalTime.Items.FindByValue(slotid)
                    item.Selected = True
                    item.Attributes.Add("default", "Y")
                End If


                If (CInt(row("IsSlotAvailable") = 0)) Then
                    item = ddlArrivalTime.Items.FindByValue(slotid)
                    If (OverrideIsOK = False) Then
                        item.Attributes.Add("disabled", "disabled")
                    End If
                End If

                item = ddlArrivalTime.Items.FindByValue(slotid)
                Dim tooltip As String = String.Format("Allocated: {0}, Booked: {1}, Available: {2}, BookingSummary: {3}, IsSlotAvailable: {4}, IsSelected: {5}", allocated, booked, available, NoOfBulkBookings, isSlotAvailable, isDefault)
                item.Attributes.Add("title", tooltip)

                ddlArrivalTime.Attributes.Add("onchange", "MarkOptionAsDisabledPageLoad('" & selectedindex & "')")


            Next
            Dim NoOfSlotsAvailable() As DataRow = result.Tables(0).Select("IsSlotAvailable = 0")
            If (NoOfSlotsAvailable.Length - 1 = -1) Then
                ''if items are available. there is no need to activate the override password
                overrideCheckbox.Enabled = False
            End If
        Catch ex As Exception
            HasSlot = False
        End Try


    End Sub

#End Region

#Region "rev:mia 13jan2015-addition of supervisor override password"

    Private Property OverrideIsOK As Boolean
        Get
            Return CBool(ViewState("OverrideIsOK"))
        End Get
        Set(value As Boolean)
            ViewState("OverrideIsOK") = value
        End Set
    End Property

    ''rev:mia 15Jan2015 - Today in PH history, Pope Francis is being expected in  Manila and Tacloban for
    ''                    a 3 days visit.
    Protected Sub overrideCheckbox_CheckedChanged(sender As Object, e As System.EventArgs) Handles overrideCheckbox.CheckedChanged
        OverrideIsOK = False
        If (overrideCheckbox.Checked) Then
            SlotSupervisorOverrideControl.Show()
        Else
            SlotSupervisorOverrideControl.Hide()
        End If
        PopulateAvailableSlot(rentalId)
    End Sub

    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

    Private Property OriginalPackage As String
        Get
            Return CStr(ViewState("OriginalPackage"))
        End Get
        Set(value As String)
            ViewState("OriginalPackage") = value
        End Set
    End Property

    Sub PreSave()

        Dim newPackageID As String = packagePickerControl.DataId
        Dim newPackageName As String = packagePickerControl.Text

        If (Not String.IsNullOrEmpty(OriginalPackage)) Then
            If (Not OriginalPackage.Split("|")(0).Equals(newPackageID)) And (Not OriginalPackage.Split("|")(1).Equals(newPackageName)) Then
                If (SlotAvailableControl.IsSlotRequired(Nothing, rentalId, PkgId:=newPackageID) = True) Then
                    SlotAvailableControl.NoOfBulkBookings = 1 ''set to one
                    SlotAvailableControl.SupervisorOverride = False
                    HasSlot = True
                    SlotAvailableControl.Show(Nothing, rentalId, PkgId:=newPackageID)
                Else
                    ''rev:mia 10nov2014 - slot management
                    SlotId = Nothing
                    SlotDescription = Nothing
                    HasSlot = False
                    Save()
                End If
            Else
                Save()
            End If
        End If

    End Sub

    Sub Save()
        If Not String.IsNullOrEmpty(packagePickerControl.DataId) Then


            If Not (bookedDateControl.IsValid And arrDateControl.IsValid And depDateControl.IsValid) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
                Return
            End If

            If Not (arrTimeControl.IsValid And depTimeControl.IsValid) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, "Enter time in hh:mm format")
                Return
            End If


            Dim arrDate As DateTime = Nothing
            If Not arrDateControl.Date = Date.MinValue Then
                If arrTimeControl.Time > TimeSpan.Zero Then
                    arrDate = arrDateControl.Date.Add(arrTimeControl.Time)
                End If
            End If

            Dim deptDate As DateTime = Nothing
            If Not depDateControl.Date = Date.MinValue Then
                If depTimeControl.Time > TimeSpan.Zero Then
                    deptDate = depDateControl.Date.Add(depTimeControl.Time)
                End If
            End If

            Dim bookedDate As DateTime = Nothing
            bookedDate = bookedDateControl.Date

            Dim followUpDate As DateTime = Nothing
            followUpDate = Utility.DateUIParse(followUpDateTextBox.Text)

            Dim transaction As New Data.DatabaseTransaction()

            Dim returnError As String

            ''rev:mia 10nov2014   - slot management, addition of RntSelectedSlotId ,RntSelectedSlot   
            Dim slotid As String = String.Empty
            Dim slotdesc As String = String.Empty

            Try

                If (HasSlot) Then
                    ''IF arrival does not contain slot like 08:00 am - 10:00 am then call the populateAvailableSlot
                    If (ddlArrivalTime.Items.Count - 1 > 8) Then
                        PopulateAvailableSlot(rentalId, packagePickerControl.DataId)
                    End If

                    If (Me.SlotId <> "") Then
                        Dim newPackageID As String = packagePickerControl.DataId
                        Dim newPackageName As String = packagePickerControl.Text
                        Dim haschange As Boolean = (Not OriginalPackage.Split("|")(0).Equals(newPackageID)) And (Not OriginalPackage.Split("|")(1).Equals(newPackageName))

                        If (Me.SlotId <> ddlArrivalTime.SelectedValue And ddlArrivalTime.SelectedValue <> -1 And Not haschange) Then
                            Me.SlotId = ddlArrivalTime.SelectedValue
                            Me.SlotDescription = ddlArrivalTime.SelectedItem.Text
                        End If

                        slotid = Me.SlotId
                        slotdesc = Me.SlotDescription

                    Else
                        If (ddlArrivalTime.SelectedValue <> -1) Then
                            slotid = ddlArrivalTime.SelectedValue
                            slotdesc = ddlArrivalTime.SelectedItem.Text
                            PopulateAvailableSlot(rentalId)
                        End If
                    End If

                Else
                    ''IF arrival does  contain slot like 08:00 am - 10:00 am then call the PopulateDayTimeList
                    If (ddlArrivalTime.Items.Count - 1 < 8) Then
                        PopulateDayTimeList(ddlArrivalTime)
                        slotid = String.Empty
                        slotdesc = String.Empty
                    End If

                    Me.SlotId = String.Empty
                    Me.SlotDescription = String.Empty
                    overrideSlotTR.Visible = False
                        End If

                        ''use est.arrival and make slotid empty
                        If (slotdesc.Contains("am") = False And slotdesc.Contains("pm") = False And arrivalslot.InnerText.Contains("Arrival Slot") = False) Then
                            slotid = String.Empty
                            slotdesc = String.Empty
                        End If

            Catch ex As Exception
                ''use arrival slot
                slotid = String.Empty
                slotdesc = String.Empty
            End Try

            Try
                If Not updateArrivalDepartureDetails() Then
                    transaction.RollbackTransaction()
                    Return
                End If

                returnError = Rental.ManageRental(rentalId, arrRefTextBox.Text, arrDate, depRefTextBox.Text, deptDate, _
                      adultTextBox.Text, childrenTextBox.Text, infantsTextBox.Text, vipRentalCheckBox.Checked, refCustomerCheckBox.Checked, partOfConvoyCheckBox.Checked, _
                      agentRefTextBox.Text, agentVoucherNoTextBox.Text, _
                      bookedDate, followUpDate, rentalSourceDropDownList.SelectedValue, ViewState(RentalEdit_KnockBack_ViewState), _
                      packagePickerControl.DataId, ViewState(RentalEdit_RntIntNo_ViewState), _
                      ViewState(RentalEdit_BooIntNo_ViewState), UserCode, Me.Page.ToString(), Me.txtthirdParty.Text, _
                      GetCombinedDateParts(ViewState(RentalEdit_CkoWhen_ViewState), ddlArrivalTime.SelectedValue), _
                      GetCombinedDateParts(ViewState(RentalEdit_CkiWhen_ViewState), _
                      ddlDepartureTime.SelectedValue), _
                      HttpUtility.HtmlEncode(promocodeTextbox.Text), _
                      slotid, _
                      slotdesc) ''rev:mia 10nov2014   - slot management, addition of RntSelectedSlotId ,RntSelectedSlot   
                transaction.CommitTransaction()
                ''transaction.RollbackTransaction()

            Catch ex As Exception
                transaction.RollbackTransaction()
                returnError = "Error while updating."
            End Try

            'rev:mia 10nov2014   - slot management
            If (Not String.IsNullOrEmpty(slotid)) Then
                ddlArrivalTime.SelectedValue = slotid
            End If


        If Not String.IsNullOrEmpty(returnError) Then
            SetShortMessage(AuroraHeaderMessageType.Error, returnError)
        Else
            dataBindRentalDetail()
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        End If

        Else
        SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
        End If

    End Sub

    Protected Sub SlotAvailableControl_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles SlotAvailableControl.PostBack
        If (isSaveButton) Then
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)
            HasSlot = True
            Save()
            Logging.LogDebug("RentalEdit SlotAvailableControl_PostBack", "SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
    End Sub

    Protected Sub SlotSupervisorOverrideControl_PostBack(sender As Object, isOkButton As Boolean, param As String) Handles SlotSupervisorOverrideControl.PostBack
        If isOkButton Then
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(SlotSupervisorOverrideControl.Login, SlotSupervisorOverrideControl.Password, "OLMAN")

            If (sReturnMsg = "True") Then
                OverrideIsOK = True
                SlotSupervisorOverrideControl.Hide()
                overrideCheckbox.Enabled = False
            Else
                SlotSupervisorOverrideControl.Show()
                SetErrorShortMessage(sReturnMsg)
            End If
        End If

        OverrideIsOK = False
    End Sub

    

#End Region

   
   
End Class


