<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookingNoteSearch.aspx.vb" Inherits="Booking_BookingNoteSearch" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Booking/Controls/BookingNotesPopupUserControl.ascx" TagName="BookingNotesPopupUserControl"
    TagPrefix="uc2" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc3" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <table width="100%">
            <tr width="100%">
                <td width="150">
                    Booking No:</td>
                <td width="250">
                    <asp:TextBox ID="bookingNoTextBox" runat="server" ReadOnly="true">
                    </asp:TextBox></td>
                <td width="150">
                    Rental No: 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <contenttemplate>
                     From&nbsp;   
                    <asp:DropDownList ID="rentalFromDropDownList" runat="server" width="50">
                    </asp:DropDownList>
                    &nbsp;To&nbsp;
                    <asp:DropDownList ID="rentalToDropDownList" runat="server" width="50">
                </asp:DropDownList>
                </contenttemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    Type:</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <contenttemplate>
                <asp:DropDownList ID="typeDropDownList" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="All" Value="All" Selected="true"></asp:ListItem>
                </asp:DropDownList></contenttemplate>
                    </asp:UpdatePanel>
                </td>
            
                <td>
                    Added By:</td>
                <td>
                    <uc3:PickerControl ID="userPickerControl" runat="server" PopupType="GETDATAFORUSER" />
                </td>
            </tr>
            <tr>
                <td>
                    Added Date: 
                    <asp:CustomValidator ID="dateCustomValidator" runat="server" OnServerValidate="dateCustomValidator_OnServerValidate"
                        ErrorMessage="">*</asp:CustomValidator>
                </td>
                <td colspan=3>
                    <table>
                        <tr>
                            <td>
                                From&nbsp;<uc4:DateControl ID="fromDateControl" runat="server" Nullable="true" />
                            </td>
                            <td>
                                &nbsp;To&nbsp;
                                <uc4:DateControl ID="toDateControl" runat="server" Nullable="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Audience:</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <contenttemplate>
                <asp:DropDownList ID='audienceDropDownList' runat="server" >
                    
                </asp:DropDownList></contenttemplate>
                    </asp:UpdatePanel>
                </td>
            
                <td>
                    Active:</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <contenttemplate>
                <asp:DropDownList ID="activeDropDownList" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="All" Value="All" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                    <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
                </asp:DropDownList></contenttemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr >
                <td>
                    Containing Text:</td>
                <td  colspan=3>
                    <asp:TextBox ID="textTextBox" runat="server" Width="60%" MaxLength="50">
                    </asp:TextBox></td>
            </tr>
            <tr>
 
                <td colspan="4" align="right">
                <br />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                    <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" /></td>
            </tr>
        </table>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <contenttemplate>
        
    <TABLE id="sortTable"  runat=server width="100%" visible=false >
        <tbody>
            <tr>
                <td width="400" style="border-width: 1px 0px">
              
                    <i><asp:Label ID="resultLabel" runat="server"></asp:Label></i>
                </td>
                <td  width="150" style="border-width: 1px 0px">
                    Sort By:
                </td>
                <td align="right" style="border-width: 1px 0px">
                    <asp:RadioButtonList ID="headerRadioButtonList" runat="server" AutoPostBack="True"
                        RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Text="Priority" Value="Pr" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Date Added" Value="ADTDate"></asp:ListItem>
                        <asp:ListItem Text="Audience" Value="Audience"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <asp:Panel ID="resultPanel" runat="server" Width="100%" Height="70%"
         Visible="false">
        <asp:Repeater ID="bookingNotesRepeater" runat="server">
             <HeaderTemplate>
                <table width="100%" class="dataTable">
             </HeaderTemplate>
            <ItemTemplate>
                    <tr class="evenRow">
                        <td width="10%">
                            <asp:Label runat="server" ID="Label1" Text='<%# Bind("RNo") %>' />
                        </td>
                        <td width="17%">
                            <i>Audience&nbsp;</i>
                            <asp:Label runat="server" ID="Label2"  Text=' <%# Bind("Audience") %>' />
                        </td>
                        <td width="18%">
                            <i>Type&nbsp;</i>
                            <asp:Label runat="server" ID="Label3" Text=' <%# Bind("BookingType") %>' />
                        </td>
                        <td width="10%">
                            <i>Priority&nbsp;</i>
                            <asp:Label runat="server" ID="Label4" Text=' <%# Bind("Pr") %>' />
                        </td>
                        <td width="40%">
                            <i>Added&nbsp;</i>
                            <asp:Label runat="server" ID="Label5" Text=' <%# GetDateFormat(eval("ADT")) %> ' />
                            <i>&nbsp;by&nbsp;</i>
                            <asp:Label runat="server" ID="Label6" Text=' <%# Bind("AUS") %>' />
                        </td>
                        <td width="5%">
                            <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="update" 
                            Enabled='<%# AllowUpdate(Eval("BookingType")) %>' CommandArgument='<%# Eval("NId") %>'>
                            Edit</asp:LinkButton>
                        </td>
                    </tr>
                    <tr class="evenRow">
                        <td colspan="6">
                            <asp:Label ID="lblNotes" runat="server" Width="95%" Text='<%# Eval("Dsc").ToString.Replace(chr(10), "<br />") %>' /></td>
                    </tr>
            
            </ItemTemplate>
             <AlternatingItemTemplate>
                
                    <tr class="oddRow">
                        <td width="10%">
                            <asp:Label runat="server" ID="Label1" Text='<%# Bind("RNo") %>' />
                        </td>
                        <td width="17%">
                            <i>Audience&nbsp;</i>
                            <asp:Label runat="server" ID="Label2" Text=' <%# Bind("Audience") %>' />
                        </td>
                        <td width="18%">
                            <i>Type&nbsp;</i>
                            <asp:Label runat="server" ID="Label3" Text=' <%# Bind("BookingType") %>' />
                        </td>
                        <td width="10%">
                            <i>Priority&nbsp;</i>
                            <asp:Label runat="server" ID="Label4" Text=' <%# Bind("Pr") %>' />
                        </td>
                        <td width="40%">
                            <i>Added&nbsp;</i>
                            <asp:Label runat="server" ID="Label5" Text=' <%# GetDateFormat(eval("ADT")) %> ' />
                            <i>&nbsp;by&nbsp;</i>
                            <asp:Label runat="server" ID="Label6" Text=' <%# Bind("AUS") %>' />
                        </td>
                        <td width="5%">
                            <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="update" 
                                Enabled='<%# AllowUpdate(Eval("BookingType")) %>' CommandArgument='<%# Eval("NId") %>'>
                                Edit</asp:LinkButton>
                        </td>
                    </tr>
                    <tr class="oddRow">
                        <td colspan="6">
                            <asp:Label ID="lblNotes" runat="server" Width="95%" Text='<%# Eval("Dsc").ToString.Replace(chr(10), "<br />") %>' /></td>
                    </tr>
        
            </AlternatingItemTemplate>
            
            <FooterTemplate>
            </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <uc2:BookingNotesPopupUserControl ID="BookingNotesPopupUserControl1" runat="server" />
    
    </contenttemplate>
            <triggers>
        <asp:AsyncPostBackTrigger ControlID="headerRadioButtonList" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel>
        <br />
    </asp:Panel>
</asp:Content>
