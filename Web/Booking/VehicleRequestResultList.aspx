<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="VehicleRequestResultList.aspx.vb" Inherits="Booking_VehicleRequestResultList"
    Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript" src="VehicleRequestMgt.js"> 
      
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>
<asp:Panel ID="formPanel" runat=server Width=780>
    <table width="100%" class="dataTable">
       <thead>
            <tr width="100%">
                <th colspan="5" style="text-align: left;">
                    Product Information
                </th>
            </tr>
        </thead>
        <tr class="oddRow">
            <td width="100%">
                <asp:Panel ID="pnlProduct" runat="server" Width="100%">
                    <table Width="100%">
                        <tbody>
                            <tr>
                                <td width="15%">
                                    Product:</td>
                                <td width="35%">
                                    <uc1:PickerControl ID="PickerControlForProduct" runat="server" AutoPostBack="false"
                                        AppendDescription="true" Width="200" PopupType="Product4BookingSearch"></uc1:PickerControl>
                                    &nbsp;*
                                </td>
                                <td width="15%">
                                    Hire Period:</td>
                                <td width="25%">
                                    <asp:TextBox ID="txthireperiod" runat="server" Width="90"></asp:TextBox>
                                </td>
                                <td width="10%">
                                    <asp:DropDownList ID="ddldays" runat="server" Width="80">
                                        <asp:ListItem Selected="True" Text="Days" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Weeks" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Months" Value="30"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" border="0" class="dataTable">
        <thead align="center">
            <tr width="100%">
                <th colspan="5" style="text-align: left;">
                    Check-Out
                </th>
            </tr>
        </thead>
        <tr width="100%" class="oddRow">
            <td>
                <asp:Panel ID="pnlCKO" runat="server" Width="100%">
                    <table width="100%">
                        <tbody>
                            <tr >
                                <td width="15%">
                                    Branch:
                                </td>
                                <td width="35%">
                                    <uc1:PickerControl ID="PickerControlForBranchCKO" runat="server" AutoPostBack="false"
                                        AppendDescription="true" Width="200" PopupType="lOCATION"></uc1:PickerControl>&nbsp;*
                                </td>
                                <td width="15%">
                                    Date:
                                </td>
                                <td width="25%">
                                    <uc2:DateControl ID="DateControlForCKO" runat="server" width="60" AutoPostBack="false">
                                    </uc2:DateControl>&nbsp;*
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblckoDay" runat="server" Width="40"></asp:Label>
                                   </td>
                                <td width="10%"> 
                                    <asp:DropDownList ID="ddlamppmCKO" runat="server" Width="80" AutoPostBack="false">
                                        <asp:ListItem>AM</asp:ListItem>
                                        <asp:ListItem>PM</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" border="0" class="dataTable">
        <thead >
            <tr width="100%">
                <th colspan="5" style="text-align: left;">
                    Check-In
                </th>
            </tr>
        </thead>
        <tr width="100%" class="oddRow">
            <td>
                <asp:Panel ID="pnlCKI" runat="server" Width="100%">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td width="15%">
                                    Branch:
                                </td>
                                <td width="35%">
                                    <uc1:PickerControl ID="PickerControlForBranchCKI" runat="server" AutoPostBack="false"
                                        AppendDescription="true" Width="200" PopupType="lOCATION"></uc1:PickerControl>&nbsp;* 
                                </td>
                                <td width="15%">
                                    Date:
                                </td>
                                <td width="25%">
                                    <uc2:DateControl ID="DateControlForCKI" runat="server" AutoPostBack="false">
                                    </uc2:DateControl>&nbsp;* 
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblckiDay" runat="server" Width="40"></asp:Label>
                                    
                                     </td>
                                <td width="10%">
                                <asp:DropDownList ID="ddlAMPMcki" runat="server" Width="80" AutoPostBack="false">
                                        <asp:ListItem>AM</asp:ListItem>
                                        <asp:ListItem>PM</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tbody>
            <tr>
                <td align="right">
                    <asp:Button ID="btnResubmit" runat="server" Width="150" CssClass="Button_Standard"
                        Text="Resubmit"></asp:Button>
                    <asp:Button ID="btnOneDayBack" runat="server" Width="150" CssClass="Button_Standard"
                        Text="1 Day Back"></asp:Button>
                    <asp:Button ID="btnOneDayForward" runat="server" Width="150" CssClass="Button_Standard"
                        Text="1 Day Forward"></asp:Button>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <table width="100%">
        <tbody>
            <tr width="100%">
                <td width="15%">
                    Cost
                </td>
                <td width="85%">
                    <asp:RadioButtonList ID="rdList" runat="server" AutoPostBack="True" Width="100%"
                        Height="18px" RepeatLayout="Table" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">PerPerson</asp:ListItem>
                        <asp:ListItem Value="2">PerDay</asp:ListItem>
                        <asp:ListItem Selected="True" Value="3">Total</asp:ListItem>
                        <asp:ListItem Value="4">VehDayNet</asp:ListItem>
                        <asp:ListItem Value="5">VehDayGross</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    
    <asp:Panel ID="pnlInfo" runat="server" Width="100%">
        <table width="100%">
            <tbody>
                <tr width="100%">
                    <td valign="bottom" width="85%">
                        <table class="dataTable" width="100%">
                            <thead>
                                <tr align="center">
                                    <th style="width: 103px">
                                        Location
                                    </th>
                                    <th style="width: 272px">
                                        Vehicles
                                    </th>
                                    <th style="width: 201px">
                                        Availability
                                    </th>
                                    <th>
                                        Criteria Match
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="evenRow">
                                    <td style="width: 103px">
                                        <%= CKOandCKI %>
                                    </td>
                                    <td style="width: 272px">
                                        <%=ProductName%>
                                    </td>
                                    <td style="width: 201px">
                                        <b>
                                            <%= Availabilitynumber%>
                                        </b>
                                    </td>
                                    <td>
                                        <%=CriteriaMatch%>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <br />
    <table width="100%">
        <tbody>
            <tr id="trdataTableDefault" runat="server" width="100%">
                <td style="height: 42px" width="100%">
                    <table class="dataTable" cellspacing="0" cellpadding="0" width="100%" border="1"
                        visible="false">
                        <thead>
                            <tr width="100%">
                                <th style="width: 104px">
                                    Package
                                </th>
                                <th style="width: 23px">
                                    PP
                                </th>
                                <th style="width: 253px">
                                    Brand
                                </th>
                                <th style="width: 104px">
                                    OUM
                                </th>
                                <th style="width: 101px">
                                    CUR
                                </th>
                                <th>
                                    Value
                                </th>
                                <th>
                                    Qty
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="evenRow" align="left" width="100%">
                                <td style="width: 104px">
                                    <asp:TextBox ID="txtDummy" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 23px">
                                    <asp:TextBox ID="TextBox1" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 253px">
                                    <asp:TextBox ID="TextBox2" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                    <asp:TextBox ID="TextBox3" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 101px">
                                    <asp:TextBox ID="TextBox4" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox5" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox6" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr width="100%">
                <td width="100%">
                    <asp:GridView ID="GridView1" runat="server" CssClass="dataTableGrid" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False">
                        <RowStyle CssClass="evenRow"></RowStyle>
                        <Columns>
                            <asp:HyperLinkField DataTextField="pkg_text" NavigateUrl="~/Booking/Booking.aspx"
                                HeaderText="Package" SortExpression="pkg_text">
                                <HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px">
                                </ItemStyle>
                            </asp:HyperLinkField>
                            <asp:TemplateField HeaderText="PP">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="pp" Checked='<%# eval("pp")%>' Enabled="false" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px">
                                </ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="br" HeaderText="Brand">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="uom" HeaderText="OUM">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" Width="100px">
                                </ItemStyle>
                            </asp:BoundField>
                            <asp:HyperLinkField DataTextField="cur" NavigateUrl="~/Booking/Booking.aspx" HeaderText="CUR"
                                SortExpression="cur">
                                <HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
                            </asp:HyperLinkField>
                            <asp:TemplateField HeaderText="Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblvalueTC" runat="server" Width="50" Visible="false"><%# eval("val_tc") %></asp:Label>
                                    <asp:Label ID="lblvaluePP" runat="server" Width="50" Visible="false"><%# eval("val_pp") %></asp:Label>
                                    <asp:Label ID="lblvaluePD" runat="server" Width="50" Visible="false"><%# eval("val_pd") %></asp:Label>
                                    <asp:Label ID="lblvalueVC" runat="server" Width="50" Visible="false"><%# eval("val_vc") %></asp:Label>
                                    <asp:Label ID="lblvalueVDN" runat="server" Width="50" Visible="false"><%# eval("val_vdn") %></asp:Label>
                                    <asp:Label ID="lblvalueVDG" runat="server" Width="50" Visible="false"><%# eval("val_vdg") %></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" Width="50px">
                                </ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty">
                                <ItemTemplate>
                                    <asp:TextBox Text="" runat="server" ID="txtvalue" Width="30" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtvalue" runat="server" FilterType="Numbers"
                                        TargetControlID="txtvalue" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" BorderWidth="1px" BorderStyle="Solid" Width="30px">
                                </ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="oddRow"></AlternatingRowStyle>
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <table width="100%">
        <tbody>
            <tr>
                <td align="right">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" Width="150" CssClass="Button_Standard">
                    </asp:Button>
                    <asp:Button ID="btnSaveNext" runat="server" Text="Save/Next" Width="150" CssClass="Button_Standard">
                    </asp:Button>
                    <asp:Button ID="btnModifyBookingRequest" runat="server" Text="Modify Booking Request"
                        Width="150" CssClass="Button_Standard"></asp:Button>
                    <asp:Button ID="btnNewBookingRequest" runat="server" Text="New Booking Request" Width="150"
                        CssClass="Button_Standard"></asp:Button>
                </td>
            </tr>
        </tbody>
    </table>
    
    <ajaxToolkit:ConfirmButtonExtender ID="Confirm_NBR" runat="server" TargetControlID="btnNewBookingRequest"
        ConfirmText="Are you sure you want to start a new Booking Request?">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:HiddenField ID="HidVHRid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidBKRid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidContactID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidRentalID" runat="server" EnableViewState="true" />

    </asp:Panel>
        </contenttemplate>
    </asp:UpdatePanel>
</asp:Content>
