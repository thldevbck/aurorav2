<%@ Import Namespace="Aurora.Common" %>
<%@ MasterType VirtualPath="~/Include/AuroraHeader.master" %>
<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookingProcess.aspx.vb" Inherits="Booking_BookingProcess" Title="Booking Process"
    EnableEventValidation="false" EnableViewState="true" Trace="false" TraceMode="SortByCategory"   %>

<%@ Register Src="../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/WizardControl/WizardControl.ascx" TagName="WizardControl" TagPrefix="uc1" %>

<%----%>   
<%@ Register Src="~/Booking/Controls/ShowBookingRequestAgentPopupUserControl.ascx" TagName="ShowBookingRequestAgentPopupUserControl" TagPrefix="uc4" %>
<%@ Register Src="~/Booking/Controls/BookingRequestAgentMgtPopupUserControl.ascx" TagName="BookingRequestAgentMgtPopupUserControl" TagPrefix="uc5" %>
<%@ Register Src="~\Booking\Controls\ContactAgentMgtPopupUserControl.ascx" TagName="ContactAgentMgtPopupUserControl" TagPrefix="uc6" %>
 
 <%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="cbQuickAvail"   TagPrefix="uc10" %>


<%@ Register Src="../UserControls/SlotAvailableControl/SlotAvailableControl.ascx" TagName="TimeSlotAvailability"   TagPrefix="Slot" %>


 

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script type="text/javascript" src="JS/VehicleRequestMgt.js"></script>
    <script type="text/javascript" src="JS/VehicleAvailExtension.js"></script>
    <script src="../Booking/Jquery/jquery-ui-1.7.3.custom.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        function MarkOptionAsDisabledPageLoad() {

            clearMessages()
            var myselect = document.getElementById("ctl00_ContentPlaceHolder_ddlArrivalTime");
            var isDisabled = false
            var isSelected = false
            var lastindex = myselect.options[myselect.selectedIndex].value;
            var opts = myselect.options;
            var opt = 0;
            var selectedindex = -1
            for (opt = 0; opt < opts.length; opt++) {

                if (opts[opt].getAttribute("default") != null) {
                    if (opts[opt].getAttribute("default") == 'Y') {
                        selectedindex = opt;
                        break
                    }
                }
            }

            for (opt = 0; opt < opts.length; opt++) {

                if (opts[opt].getAttribute("disabled") != null) {
                    isDisabled = opts[opt].getAttribute("disabled");

                    if (isDisabled == 'True' || isDisabled == true) {
                        if (lastindex == opts[opt].value) {
                            myselect.selectedIndex = selectedindex;

                            setWarningMessage("The slot " + opts[opt].text + " is not available.");
                            break;
                        }
                    }
                }
            }
        }
    

    function onPageSearch(searchParam)
    {
        if (searchParam != '')
        {
            GoToBooking()
        }
        else
        {		
            if(confirm("Are you sure you want to start a new Booking Request?"))
            { 
		        document.forms[0].action="./BookingProcess.aspx"
		        document.forms[0].method="POST"
                document.forms[0].submit(); 
            }
        }
    }

  

    
    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);
    
    function BeginRequestHandler(sender, args) {
       pbControl = args.get_postBackElement();  //the control causing the postback
       //debugger;
      

        if(pbControl.id.indexOf('Step3_PickerControlForProduct') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKO') > -1 || pbControl.id.indexOf('Step3_DateControlForCKO') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKI') > -1 || pbControl.id.indexOf('Step3_DateControlForCKI') > -1 || pbControl.id.indexOf('Step3_btnOneDayForward') > -1 || pbControl.id.indexOf('Step3_btnOneDayBack') > -1)
        {   
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = false;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayBack').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayForward').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = false;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = false;
           
        }
        if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_Step4_GridSummary_') > -1) 
        {
           // var usercode = '<%= UserCode %>'
           // if (usercode == 'ma2') alert(pbControl.id)
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnInquiry').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnQuote').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnKnockBack').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnWaitlist').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnProvisional').disabled = true;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnConfirm').disabled = true;

            //alert(pbControl.id)
        }
        
    }

    function EndRequestHandler(sender, args) {
        
        if(pbControl.id.indexOf('Step3_PickerControlForProduct') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKO') > -1 || pbControl.id.indexOf('Step3_DateControlForCKO') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKI') > -1 || pbControl.id.indexOf('Step3_DateControlForCKI') > -1 || pbControl.id.indexOf('Step3_btnOneDayForward') > -1 || pbControl.id.indexOf('Step3_btnOneDayBack') > -1)
        { 
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = false;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayBack').disabled = false;
            $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayForward').disabled = false;
            if(pbControl.id.indexOf('Step3_DateControlForCKO') > -1 || pbControl.id.indexOf('Step3_btnOneDayBack') > -1 || pbControl.id.indexOf('Step3_btnOneDayForward') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKO') > -1)
                {
                   
                    var ampmcko = $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO')
                    
                        if(ampmcko.options.length-1 <= -1)
                            {
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = true;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = true;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = true;
                            }
                            else
                            {
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = false;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = false;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = false;
                            }
   
                          
                }
            if(pbControl.id.indexOf('Step3_DateControlForCKI') > -1 || pbControl.id.indexOf('Step3_btnOneDayBack') > -1 || pbControl.id.indexOf('Step3_btnOneDayForward') > -1 || pbControl.id.indexOf('Step3_PickerControlForBranchCKI') > -1)
                {
                    var ampmcki = $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlAMPMcki')
                        if(ampmcki.options.length-1 <= -1)
                        {
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = true;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = true;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = true;                        }
                        else
                        {   
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = false;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = false;
                                $get('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = false;
                        }
                }
                
        }

        if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_Step4_GridSummary_') > -1) 
        {
//            var usercode = '<%= UserCode %>'
//            if (usercode == 'ma2') {
//                alert(pbControl.id)
//            }
            if ($get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnConfirm').disabled == true) {
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnInquiry').disabled = true;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnQuote').disabled = true;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnKnockBack').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnWaitlist').disabled = true;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnProvisional').disabled = true;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnConfirm').disabled = true;
            }
            else {
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnInquiry').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnQuote').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnKnockBack').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnWaitlist').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnProvisional').disabled = false;
                $get('ctl00_ContentPlaceHolder_wizBookingProcess_BtnConfirm').disabled = false;
            }
        }
        
         if(pbControl.id.indexOf('lnkToggleCompany') > -1)
        {
            var text = $get('ctl00_lnkToggleCompany').innerText
            if(text.indexOf('Explore') >-1 || text.indexOf('THL') >-1)
              {
                $get('ctl00_titleLink').innerText='';
                $get('ctl00_titleLink').href=''
              }
        }
        
        pbControl = null;
    }
    
    </script>

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {

            //-----------------------------------------------------------------------------------
            //rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
            //-----------------------------------------------------------------------------------
            var baseUrl = document.location.toString();
            var time_in;
            var time_out;
            if (baseUrl != '') {
                var indexes = baseUrl.indexOf('&outD');
                if (indexes > 0) {
                    $("#ctl00_titlelabel").text("Booking Process : ")
                    if ('<%= Aurora.Booking.Services.BookingRequest.ReturnError%>' == "") {
                        $("#ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox").trigger("change")
                        $("#ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox").trigger("change")
                        $("#ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox").trigger("change")
                    }

                    var location = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                    var mydate = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                    if (mydate == null) return
                    if (location == null) return
                    if (mydate.value == '' || location.value == '') return

                    var requestURL = "GetVehicleRequestAjax.aspx?mode="
                    var url = requestURL + 'GetAvailableTimeCKIv2&paramcki=' + location.value + '&controltype=2' + '&traveldate=' + mydate.value
                    $.post(url, { GetAvailableTimeCKIv2: "" }, function (result) {
                        if (result != null) {
                            ClearLocationTimeAndSet(result.documentElement);

                            time_in = unescape(getUrlVars()["inT"])
                            if (time_in.length == 4) {
                                time_in = "0" + time_in
                            }


                            $("#ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin").val(time_in)

                            location = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                            mydate = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')

                            if (mydate.value == '' || location.value == '') return
                            url = requestURL + 'GetAvailableTimeCKOv2&paramcko=' + location.value + '&controltype=1' + '&traveldate=' + mydate.value
                            $.post(url, { GetAvailableTimeCKIv2: "" }, function (result) {
                                if (result != null) {
                                    ClearLocationTimeAndSet(result.documentElement);
                                    time_out = unescape(getUrlVars()["outT"])
                                    if (time_out.length == 4) {
                                        time_out = "0" + time_out
                                    }

                                    $("#ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout").val(time_out)
                                }
                            });
                        }
                    });
                }
                if (document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox') != null) {
                    setTillDays()
                 }
                
            }

        });
//rev:mia July 2 2013 - added for fixing the time location
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }    
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
   <uc1:WizardControl Id="wizardControl" runat="server" SelectedIndex="0">
        <Items>
            <asp:ListItem>Booking Request</asp:ListItem>
            <asp:ListItem>Duplicate Rentals</asp:ListItem>
            <asp:ListItem>Display Vehicles</asp:ListItem>
            <asp:ListItem>Summary</asp:ListItem>
        </Items>
    </uc1:WizardControl>
    
    <hr />
  
  
    <asp:Wizard
        ID="wizBookingProcess" 
        runat="server" 
        ActiveStepIndex="0" 
        Width="100%" 
        DisplaySideBar="false">

<%----------------------------------------------------------------------------------------------------------------------%>
        <StartNavigationTemplate>
            <asp:Button 
                ID="btnNewBooking" 
                runat="server" 
                Text="New Booking" 
                CssClass="Button_Standard Button_New"
                OnClick="btnNewBooking_Click" />
             
                
            <ajaxToolkit:ConfirmButtonExtender 
                ID="Confirm_NBR" 
                runat="server" 
                TargetControlID="btnNewBooking"
                ConfirmText="Are you sure you want to start a new Booking Request?" />
            
          
            <asp:Button 
                ID="btnRequestBrochure" 
                runat="server" 
                Text="Brochure"
                CssClass="Button_Standard" 
                OnClick="Step1_RedirectCustomer_Click" Visible="False" />
               

            <asp:Button 
                ID="btnReloc" 
                runat="server" 
                Text="Relocation" 
                CssClass="Button_Standard" 
                OnClick="Relocation_Click"
                CommandName="MoveNext" 
                ValidationGroup="Book" />
                
            <asp:Button 
                ID="StartNextButton" 
                runat="server" 
                CommandName="MoveNext" 
                Text="Next"
                CssClass="Button_Standard Button_Next" 
                OnClick="StartNextButton_Click" 
                ValidationGroup="Book" />



        </StartNavigationTemplate>

<%----------------------------------------------------------------------------------------------------------------------%>
        <StepNavigationTemplate>
            <asp:Button 
                ID="btnNewBookingRequest" 
                TabIndex="3" 
                runat="server" 
                Text="New Booking"
                CssClass="Button_Standard Button_New" 
                OnClick="btnNewBooking_Click" />
            
            <ajaxToolkit:ConfirmButtonExtender 
                ID="ConfirmButtonExtender1" 
                runat="server" 
                ConfirmText="Are you sure you want to start a new Booking Request?"
                TargetControlID="btnNewBookingRequest" />

            <asp:Button 
                ID="btnAddToBooking" 
                TabIndex="4" 
                runat="server" 
                Text="Add to Booking" 
                CssClass="Button_Standard Button_Add" 
                OnClick="Step2_Set_Click" 
                Width="125" />
                
            <asp:Button 
                ID="btnUnset" 
                TabIndex="5" 
                runat="server" 
                Text="Unset Add to Booking" 
                CssClass="Button_Standard Button_Remove"
                OnClick="Step2_Unset_Click" 
                Width="160" />

            <asp:Button 
                ID="btnSubmit" 
                runat="server" 
                Text="Save" 
                CssClass="Button_Standard"
                OnClick="Step3_Submit_Click" />
                
            &nbsp;&nbsp;&nbsp;

            <asp:Button 
                ID="StepPreviousButton" 
                runat="server" 
                CausesValidation="False" 
                CommandName="MovePrevious"
                Text="Previous" 
                CssClass="Button_Standard Button_Prev" />
                
            <asp:Button 
                ID="StepNextButton" 
                TabIndex="1" 
                runat="server" 
                Text="Next" 
                CssClass="Button_Standard Button_Next"
                CommandName="MoveNext" />
                
            <asp:Button 
                ID="btnSaveNext" 
                runat="server" 
                Text="Save/Next" 
                CssClass="Button_Standard Button_Next"
                CommandName="MoveNext" 
                OnClick="Step3_SaveNext_Click" />
                
        </StepNavigationTemplate>

<%----------------------------------------------------------------------------------------------------------------------%>
        <FinishNavigationTemplate>
        
         <ajaxToolkit:ConfirmButtonExtender 
                ID="Confirm_NBR_Summary" 
                runat="server" 
                TargetControlID="btnNewBookingRequestSummary"
                ConfirmText="Are you sure you want to start a new Booking Request?" />
        
            <asp:Button 
                ID="btnNewBookingRequestSummary" 
                TabIndex="3" 
                runat="server" 
                Text="New Booking"
                CssClass="Button_Standard Button_New" 
                OnClick="btnNewBooking_Click" />
                
            <asp:Button 
                ID="FinishPreviousButton" 
                runat="server" 
                CausesValidation="False" 
                CommandName="MovePrevious"
                Text="Previous" 
                CssClass="Button_Standard Button_Prev" />
                
            <asp:Button 
                ID="BtnSaveSummary" 
                runat="server" 
                CssClass="Button_Standard Button_Save" 
                Text="Save"
                OnClick="Step4_BtnSave_Click" />
                
            <asp:Button 
                ID="BtnSaveNextSummary" 
                runat="server" 
                CssClass="Button_Standard Button_Next" 
                Text="Save/Next"
                OnClick="Step4_BtnSaveNextSummary_Click" 
                CommandName="MoveComplete" />
        </FinishNavigationTemplate>
        
<%----------------------------------------------------------------------------------------------------------------------%>
        <WizardSteps>

<%--..................................................................................................................--%>
            <asp:WizardStep ID="bookingRequestStep1" runat="server" Title="Booking Request">
             
                <table width="100%" border="0">
                    <tr>
                        <td colspan="7"><b>Agent Contact and Reference</b></td>
                    </tr>
                    <tr>
                        <td width="150">Agent:</td>
                        <td width="340">
                        
                                    <uc1:PickerControl 
                                        ID="PickerControlForAgent" 
                                        runat="server" 
                                        EnableViewState="false"
                                        Width="300" 
                                        PopupType="AGENT" 
                                        AppendDescription="true" 
                                        AutoPostBack="false"  />&nbsp;*
                        
                        </td>
                        <td>
                            <asp:Button ID="updateAgentButton" runat="server" Text="Edit Agent" CssClass="Button_Standard Button_Edit" />
                            <asp:Button ID="addAgentButton" runat="server" Text="New Agent" CssClass="Button_Standard Button_New" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>Contact:</td>
                        <td><asp:DropDownList ID="ddlContact" TabIndex="1" runat="server" Width="322" /></td>
                        <td>
                            <asp:Button ID="agentContactImageButton" runat="server" Text="Edit Contact" CssClass="Button_Standard Button_Edit" />
                        </td>
                    </tr>
                    <tr>
                        <td>Reference:</td>
                        <td>
                            <asp:TextBox ID="txtRef" TabIndex="2" runat="server" Width="315" MaxLength="30" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="Filter_TxtRef" runat="server"  FilterMode="InvalidChars" FilterType="Custom"  TargetControlID="txtRef"/> 
                        </td>
                        <td>&nbsp;</td>
                    </tr>

                    <%--
                        rev:mia July 2 2012 - addition of 3rd party reference
                                            - first day of work after two weeks holiday
                    --%>
                    <tr>
                        <td>3rd Party Reference:</td>
                        <td>
                            <asp:TextBox ID="txtthirdParty" TabIndex="3" runat="server" Width="315" MaxLength="30" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="Filter_txtthirdParty" runat="server"  FilterMode="InvalidChars" FilterType="Custom"  TargetControlID="txtthirdParty"/> 
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                       
             

                <br />
                
                <asp:UpdatePanel ID="StepOne_UpdatePanelInformationHidden" runat="server">
                    <contenttemplate>
                        <uc1:CollapsiblePanel 
                            ID="detailsCollapsiblePanel" 
                            Title="Additional Vehicle Information"
                            runat="server" 
                            Width="100%" 
                            TargetControlID="panelVehicleInformationHidden" visible="false" />
                        
                        <asp:Panel Style="overflow: hidden" ID="panelVehicleInformationHidden" runat="server" Width="100%" visible="false" >
                            <table width="100%">
                                <tr>
                                    <td width="150">Brand:</td>
                                    <td colspan="3"><asp:DropDownList ID="ddlBrand" TabIndex="14" runat="server" Width="322" AutoPostBack="false"/></td>
                                <tr>
                                    <td>Type:</td>
                                    <td colspan="3">
                                        <asp:RadioButtonList 
                                            ID="rdlACorAV" 
                                            TabIndex="15" 
                                            runat="server" 
                                            Width="104px" 
                                            AutoPostBack="true" 
                                            RepeatDirection="Horizontal" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td id="tableviewDefault" colspan="3" runat="server" visible="true">
                                        <table class="dataTable" width="322">
                                            <thead>
                                                <tr >
                                                    <th>Code</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input disabled="disabled" type="text" /></td>
                                                    <td><input disabled="disabled" type="text" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td id="tableview" runat="server" visible="false">
                                        <asp:GridView ID="GridView1" runat="server" Width="322" CssClass="dataTableGrid"
                                            DataKeyNames="TypId" AutoGenerateColumns="False" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AutoGenerateSelectButton="false">
                                            <RowStyle CssClass="evenRow"></RowStyle>
                                            <AlternatingRowStyle CssClass="oddRow"></AlternatingRowStyle>
                                            <Columns>
                                                 <asp:ButtonField CommandName="Select" Visible="False"></asp:ButtonField>
                                                <asp:BoundField DataField="TypCode" HeaderText="Code"></asp:BoundField>
                                                <asp:BoundField DataField="TypDesc" HeaderText="Description"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">Features:</td>
                                    <td valign="top" colspan="3"><asp:CheckBoxList ID="chkFeatures" TabIndex="16" runat="server" Width="240px" AutoPostBack="false" /></td>
                                </tr>
                                <tr>
                                    <td valign="top">Others:</td>
                                    <td valign="top">
                                        <asp:DropDownList ID="ddlUncommonFeature1" TabIndex="17" runat="server" Width="200" AutoPostBack="false" />
                                        &nbsp;
                                        <asp:DropDownList ID="ddlUncommonFeature2" TabIndex="18" runat="server" Width="200" AutoPostBack="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                  </contenttemplate>
                </asp:UpdatePanel>
                
                <br />
                
                <asp:Panel ID="StepOne_panelVehicleInformation" runat="server" Width="100%">
                    <asp:UpdatePanel ID="StepOne_UpdatepanelVehicleInformation" runat="server">
                        <contenttemplate>
                            <table width="100%" border="0" style="border-top-style: dotted; border-top-width: 1px;">
                                <tr>
                                    <td colspan="4"><b>Products</b></td>
                                </tr>
                                <tr>
                                    <td width="150">Product:</td>
                                    <td width="400">
                                        <uc1:PickerControl 
                                            ID="PickerControlForProduct" 
                                            runat="server" 
                                            EnableViewState="true"
                                            PopupType="getProduct"
                                            AppendDescription="true"
                                            Width="300" AutoPostBack="false" 
                                            />&nbsp;*
                                    </td>
                                    <td>No. Of Vehicles:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlNoOfVehicles" runat="server" Width="50px">
                                            <asp:ListItem Selected="True">1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <%--rev:mia june 2 2011 this is not required--%>
                                <tr style="visibility:hidden;">
                                    <td>Request Source:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlRequestSource" 
                                                          TabIndex="5" 
                                                          runat="server" 
                                                          Width="322px" 
                                                          visible = "false"/>&nbsp;*
                                        <asp:RequiredFieldValidator 
                                            ID="RFV_ddlrequestor" 
                                            runat="server" 
                                            EnableClientScript="False"
                                            ValidationGroup="Book" 
                                            Display="Dynamic" 
                                            ControlToValidate="ddlRequestSource"
                                            SetFocusOnError="True" 
                                            ErrorMessage=""
                                            text=""
                                            InitialValue="0"
                                            Enabled="false"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                            </table>
                        </contenttemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                
                <br />
                
                <asp:Panel ID="StepOne_panelbranchCheckout" runat="server" Width="100%">
                    <%--<asp:UpdatePanel ID="UpdateForCKOBranch" runat="server">
                        <contenttemplate>--%>
                            <table width="100%" border="0" style="border-top-style: dotted; border-top-width: 1px;" >
                                <tr>
                                    <td colspan="7"><b>Check-Out / Check-In</b></td>
                                </tr>
                                <tr>
                                    <td width="150">Check-Out Branch:</td>
                                    <td runat="server" id="tdCheckOutLocation">
                                        <%--PopupType="LOCATIONFORVEHICLEREQ"--%>
                                        <uc1:PickerControl 
                                            ID="PickerControlForCheckOut" 
                                            runat="server" 
                                            PopupType="GETVALIDLOCATION"
                                            EnableViewState="true"
                                            AppendDescription="true" 
                                            Width="182" AutoPostBack="false" />&nbsp;*
                                    </td>
                                    <td>Date:</td>
                                    <td align="right" runat="server" id="tdCheckOutDate">
                                                <uc1:DateControl ID="DateControlForCheckOut" runat="server" AutoPostBack="false" />
                                    </td>
                                    <td align="left">*</td>
                                    <td width="50" align="center">
                                        <asp:Label ID="lbldayCKO" runat="server" Width="40px" />
                                    </td>
                                    <td>
                                                   <asp:DropDownList ID="ddlAMPMcheckout" TabIndex="6" runat="server" Width="80px" autopostback="false" enableviewstate="true">
                                                            <asp:ListItem Selected="True">AM</asp:ListItem>
                                                            <asp:ListItem>PM</asp:ListItem>
                                                    </asp:DropDownList>
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td>Check-In Branch:</td>
                                    <td runat="server" id="tdCheckinLocation">
                                        <%--PopupType="LOCATIONFORVEHICLEREQ"--%>
                                        <uc1:PickerControl 
                                            ID="PickerControlForCheckIn" 
                                            runat="server" 
                                            PopupType="GETVALIDLOCATION"
                                            EnableViewState="true"
                                            AppendDescription="true" 
                                            Width="182" AutoPostBack="false"/>&nbsp;*
                                    </td>
                                    <td>Date:</td>
                                    <td align="right" runat="server" id="tdCheckInDate">
                                                <uc1:DateControl ID="DateControlForCheckIn" runat="server" AutoPostBack="false" />
                                    </td>
                                    <td align="left">*</td>
                                    <td align="center">
                                        <asp:Label ID="lblDayCKI" runat="server" Width="40px" />
                                    </td>
                                        <td>
                                                    <asp:DropDownList ID="ddlAMPMcheckin" TabIndex="7" runat="server" Width="80px" autopostback="false" enableviewstate="true"> 
                                                            <asp:ListItem Selected="True">AM</asp:ListItem>
                                                            <asp:ListItem>PM</asp:ListItem>
                                                    </asp:DropDownList>
                                    
                                       </td>
                                </tr>
                                <tr>
                                    <td>Hire Period:</td>
                                    <td>
                                        <asp:TextBox 
                                            ID="txtHirePeriod" 
                                            TabIndex="8" 
                                            runat="server" 
                                            Width="40px" 
                                            MaxLength="3"
                                            AutoPostBack="true" />
                                        <ajaxToolkit:FilteredTextBoxExtender 
                                            ID="FLT_txtHirePeriod" 
                                            runat="server" 
                                            TargetControlID="txtHirePeriod"
                                            FilterType="Numbers" />
                                        <asp:CustomValidator 
                                            ID="CV_txthireperiod" 
                                            runat="server" 
                                            ValidationGroup="Book"
                                            Display="Dynamic" 
                                            ErrorMessage="Invalid value.Only numbers are allowed" 
                                            ControlToValidate="txtHirePeriod"
                                            OnServerValidate="CV_txthireperiod_ServerValidate" 
                                            ClientValidationFunction="IsNumberValid">*</asp:CustomValidator>
                                                
                                        <asp:DropDownList ID="ddlDays" TabIndex="9" runat="server" Width="60px">
                                            <asp:ListItem Selected="True" Text="Days" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Weeks" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="Months" Value="30"></asp:ListItem>
                                        </asp:DropDownList>    
                                        <asp:Button 
                                                            ID="btnCalculate" 
                                                            runat="server"
                                                            TabIndex="10" 
                                                            width ="100px"
                                                            OnClick="btnCalculate_Click" 
                                                            Text="Calculate" 
                                                            CssClass="Button_Standard Button_Calculate" visible="true" />
                                    </td>
                                    <td colspan="2">
                                                   <%--rev:mia june 1 2011 addition of quick avail--%>
                                                            <asp:Button 
                                                            ID="btnQuickAvail" 
                                                            runat="server" 
                                                            Text="Quick Avail"
                                                            CssClass="Button_Standard" 
                                                            OnClick="btnQuickAvail_Click"
                                                            Visible="true" 
                                                            ValidationGroup="Book"/> 
                                                   
                                                
                                         
                                    </td>
                                    <td colspan="2">Begins:</td>
                                    <td>
                                        <asp:Label ID="lblDaysMessage" runat="server" Width="50px"></asp:Label> day(s)
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7">&nbsp;</td>
                                </tr>
                            </table>
                        <%--</contenttemplate>
                    </asp:UpdatePanel>--%>
                </asp:Panel>
                
                <br />
                
                <asp:Panel ID="StepOne_panelVehicleInformationHire" runat="server" Width="100%">
                    <asp:UpdatePanel ID="StepOne_UpdatePanelInformation" runat="server">
                        <contenttemplate>
                            <table width="100%" border="0" style="border-top-style: dotted; border-top-width: 1px;">
                                <tr>
                                    <td colspan="7"><b>Hirer Details</b></td>
                                </tr>
                                <tr>
                                    <td width="150">Last Name:</td>
                                    <td>
                                        <asp:TextBox 
                                            ID="txtSurname" 
                                            runat="server" 
                                            Width="165px" 
                                            MaxLength="25" 
                                            EnableViewState="true" />&nbsp;*
                                            <ajaxToolkit:FilteredTextBoxExtender ID="Filter_txtSurname" runat="server" FilterType="Custom"  FilterMode="InvalidChars" TargetControlID="txtSurname"/> 
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator 
                                            ID="rfv_Surname" 
                                            runat="server" 
                                            ValidationGroup="Book"
                                            SetFocusOnError="True" 
                                            Display="Dynamic" 
                                            EnableClientScript="False" 
                                            ErrorMessage=""
                                            text=""
                                            ControlToValidate="txtSurname"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>First Name:</td>
                                    <td>
                                        <asp:TextBox 
                                            ID="txtfirstName" 
                                            runat="server" 
                                            Width="165px" 
                                            MaxLength="25" 
                                            EnableViewState="true" />
                                            <ajaxToolkit:FilteredTextBoxExtender ID="Filter_txtfirstName" runat="server" FilterType="Custom"  FilterMode="InvalidChars" TargetControlID="txtfirstName"/> 
                                    </td>
                                    <td>Title:</td>
                                    <td>
                                        <%--rev:mia Sept.2 2013 - Customer Title from query
                                        <asp:DropDownList ID="ddltitle" runat="server" Width="50">
                                            <asp:ListItem Selected="True"></asp:ListItem>
                                            <asp:ListItem>Mr.</asp:ListItem>
                                            <asp:ListItem>Mrs.</asp:ListItem>
                                            <asp:ListItem>Ms.</asp:ListItem>
                                            <asp:ListItem>Miss</asp:ListItem>
                                            <asp:ListItem>Master</asp:ListItem>
                                        </asp:DropDownList>
                                        --%>
                                        <asp:DropDownList ID="customertitleDropdown" runat="server" Width="50px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Adult:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlAdultNO" runat="server" Width="50px" AutoPostBack="false">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem Selected="True">1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>Children:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlChildNO" runat="server" Width="50px" AutoPostBack="false">
                                            <asp:ListItem Selected="true">0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                    <td>Infants:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlInfantsNO" runat="server" Width="50px" AutoPostBack="false">
                                            <asp:ListItem Selected="true">0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>Email Address:</td>
                                    <td colspan="5">
                                        
                                            <asp:TextBox 
                                            ID="txtEmailAddress" 
                                            runat="server" 
                                            Width="165px" 
                                            MaxLength="100" 
                                            EnableViewState="true" />
                                       
                                    </td>
                                </tr>
                                
                            </table>
                        </contenttemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
               <asp:Panel ID="StepOne_panelPackage" runat="server" Width="100%" >
               <table width="100%" border="0" style="border-top-style: dotted; border-top-width: 1px;"  id="tblPackage">
               <tr>
                                    <td width="150" >Package:</td>
                                    <td>
                                        <asp:updatePanel id="updatepanelPickerControlForPackage" runat="server" >
                                            <ContentTemplate>
                                                    <uc1:PickerControl 
                                                    ID="PickerControlForPackage" 
                                                    runat="server" 
                                                    Width="280" 
                                                    PopupType="AGENTPACKAGE4BOOKINGREQ"
                                                    AppendDescription="true" AutoPostBack="false" />
                                                    <%--<asp:TextBox id="testboxOrig" runat="server" width="100%"  autopostback="true"/>
                                                    <asp:TextBox id="testboxNew" runat="server" width="100%" />
                                                    <uc7:BookingPackageControl 
                                                    ID="BookingPackageControl1" 
                                                    runat="server" 
                                                    Width="280" 
                                                    AppendDescription="true" AutoPostBack="false"/>--%>
                                            </ContentTemplate>
                                        </asp:updatePanel>
                                        
                                          
                                    </td>
                                    <td>
                                        Promo Code:
                                    </td>
                                    <td>
                                        <asp:Textbox runat="server" ID="promocodeTextbox" Width="170"  MaxLength="30"></asp:Textbox>
                                    </td>
                                </tr>
               </table>
               </asp:Panel>
                <br />
            </asp:WizardStep>
            
<%--..................................................................................................................--%>
            <asp:WizardStep ID="DuplicateRentalsStep2" runat="server" Title="Duplicate Rentals">
                <asp:Panel ID="Step2_panelinfo" runat="server" Width="100%">
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="5"><b>Agent and Hirers</b></td>
                        </tr>
                        <tr>
                            <td width="150">Agent:</td>
                            <td width="250">
                                <input id="txtagent" readonly="readonly" type="text" runat="server"  style="width:350" />
                            </td>
                            <td width="150">Hirer:</td>
                            <td width="250">
                                <input id="txtHirer" readonly="readonly" type="text" runat="server" style="width:350" />
                            </td>
                            <td>
                                <asp:Button ID="btnBrochure" runat="server" Text="Brochure" CssClass="Button_Standard" tabindex = "1" Visible= "False" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                
                <br />
                <!-- rev:mia 19nov2014 start - part of  slot dropdown addition-->
                <asp:Panel ID="panelForDuplicate" runat="server" Width="100%" ScrollBars="Auto" Height="70%">
                    
                            <asp:GridView 
                                ID="Step2_GridView1" 
                                runat="server" 
                                Width="100%" 
                                AutoGenerateColumns="False"
                                DataKeyNames="BooId" 
                                CssClass="dataTableGrid">
                                
                                <RowStyle CssClass="evenRow"></RowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="Booking Rental">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hpLink" runat="server" NavigateUrl="" Text='<%# eval("BookingRental") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width ="80" />
                                    </asp:TemplateField>
                                    <%--
                                    <asp:BoundField DataField="AgentHirer" HeaderText="Agent Hirer">
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:BoundField>
                                    --%>
                                    <asp:TemplateField HeaderText="Agent Hirer">
                                        <ItemTemplate>
                                            <asp:Literal ID="litAgentHirer" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="AgentRefPackage" HeaderText="Agent Ref Package">
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:BoundField>
                                        <asp:TemplateField HeaderText="Check-Out">
                                        <ItemTemplate>
                                            <%# FormattedDate(eval("CheckOut")) %>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check-In">
                                        <ItemTemplate>
                                            <%# FormattedDate(eval("CheckIn")) %>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="HirePdStatus" HeaderText="Hire Pd Status">
                                        <ItemStyle VerticalAlign="Top"/>
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Vehicle" HeaderText="Vehicle">
                                        <ItemStyle VerticalAlign="Top" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RentalValue" HeaderText="Rental Value" ItemStyle-Width="55"></asp:BoundField>
                                    <asp:TemplateField  HeaderText="Check">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkError" Checked="false" runat="server" Visible='<%# ParseEvalItemToBoolean(Eval("HasError"),eval("BookingRental")) %>' autopostback="false" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="evenRow"></AlternatingRowStyle>
                            </asp:GridView>
                </asp:Panel>
                <br />
            </asp:WizardStep>

<%--..................................................................................................................--%>
            <asp:WizardStep ID="DisplayAvailableStep3" runat="server" Title="Display Vehicles">
                <asp:UpdatePanel ID="Step3_UpdatePanelDisplayAvailable" runat="server">
                    <contenttemplate>
                        <asp:Panel ID="Step3_PanelDisplayAvailable" runat="server" Width="100%">
                            <table width="100%">
                                <tr>
                                    <td><b>Product Information</b></td>
                                </tr>
                            </table>
                                
                            <asp:Panel ID="pnlProduct" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td width="150">Product:</td>
                                        <td width="250">
                                            <uc1:PickerControl 
                                                ID="Step3_PickerControlForProduct" 
                                                runat="server" 
                                                AppendDescription="true" 
                                                Width="150" 
                                                PopupType="getProduct" AutoPostBack="true" />&nbsp;*
                                        </td>
                                        <td width="150" align="center">Hire Period:</td>
                                        <td>
                                            <asp:TextBox ID="Step3_txthireperiod" runat="server" Width="90" />
                                        </td>
                                        <td width="40">&nbsp;</td>
                                        <td>
                                            <asp:DropDownList ID="Step3_ddldays" runat="server" Width="80">
                                                <asp:ListItem Selected="True" Text="Days" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Weeks" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="Months" Value="30"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            
                            <br />
                            
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="5"><b>Check-Out / Check-In</b</td>
                                </tr>
                            </table>                                
                            <asp:Panel ID="pnlCKO" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td width="150">Check-Out Branch:</td>
                                        <td runat="server" id="td_step3_CKO">
                                            <%--PopupType="LOCATION"--%>
                                            <uc1:PickerControl 
                                                ID="Step3_PickerControlForBranchCKO" 
                                                runat="server" 
                                                AppendDescription="true" 
                                                Width="150px" 
                                                PopupType="GETVALIDLOCATION" AutoPostBack="true" />&nbsp;*
                                                
                                        </td>
                                        <td>Date:</td>
                                        <td align="right">
                                            <uc1:DateControl 
                                                ID="Step3_DateControlForCKO" 
                                                runat="server" AutoPostBack="true"/>
                                        </td>
                                        <td>*</td>
                                        <td align="center">
                                            <asp:Label ID="Step3_lblckoDay" runat="server" Width="40" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Step3_ddlamppmCKO" runat="server" Width="80">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                            <asp:Panel ID="pnlCKI" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td width="150">Check-In Branch:</td>
                                        <td runat="server" id="td_step3_CKI">
                                            <%--PopupType="LOCATION"--%>
                                            <uc1:PickerControl 
                                                ID="Step3_PickerControlForBranchCKI" 
                                                runat="server" 
                                                AppendDescription="true" 
                                                Width="150" 
                                                PopupType="GETVALIDLOCATION" AutoPostBack="true" />&nbsp;*
                                        </td>
                                        <td>Date:</td>
                                        <td align="right">
                                            <uc1:DateControl 
                                                ID="Step3_DateControlForCKI" 
                                                runat="server" AutoPostBack="true" />
                                        </td>
                                        <td>*</td>
                                        <td align="center">
                                            <asp:Label ID="Step3_lblckiDay" runat="server" Width="40" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Step3_ddlAMPMcki" runat="server" Width="80">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            
                            <br />
                          
                            <table width="100%">
                                <tr>
                                    <td align="right">
                                        <asp:Button 
                                            ID="Step3_btnResubmit" 
                                            runat="server" 
                                            CssClass="Button_Standard" 
                                            Text="Resubmit" />
                                            
                                        <asp:Button 
                                            ID="Step3_btnOneDayBack" 
                                            runat="server" 
                                            CssClass="Button_Standard" 
                                            Text="1 Day Back" />
                                            
                                        <asp:Button 
                                            ID="Step3_btnOneDayForward" 
                                            runat="server" 
                                            CssClass="Button_Standard" 
                                            Text="1 Day Forward" />
                                    </td>
                                </tr>
                            </table>
                            
                            <br />
                            
                            <table width="100%">
                                <tr>
                                    <td width="15%"></td>
                                    <td width="85%">
                                        <asp:RadioButtonList 
                                            ID="Step3_rdList" 
                                            runat="server" 
                                            AutoPostBack="True" 
                                            Width="100%"
                                            Height="18px" 
                                            RepeatLayout="Table" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">CostPerPerson</asp:ListItem>
                                            <asp:ListItem Value="2">PerDay</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="3">Total</asp:ListItem>
                                            <asp:ListItem Value="4">VehTot</asp:ListItem>
                                            <asp:ListItem Value="5">VehDayNet</asp:ListItem>
                                            <asp:ListItem Value="6">VehDayGross</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                            
                            <br />
                            
                            <asp:Panel ID="Step3_pnlInfo" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td valign="bottom" width="85%">
                                            <table class="dataTable" width="100%">
                                                <thead>
                                                    <tr align="center">
                                                        <th style="width: 103px">Location</th>
                                                        <th style="width: 272px">Vehicles</th>
                                                        <th style="width: 201px">Availability</th>
                                                        <th>Criteria Match</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="evenRow">
                                                        <td><%= CKOandCKI %></td>
                                                        <td><%=ProductName%></td>
                                                        <td><b><%= Availabilitynumber%></b></td>
                                                        <td><%=CriteriaMatch%></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            
                            <br />
                            
                            <table width="100%">
                                <tr id="trdataTableDefault" runat="server" width="100%">
                                    <td style="height: 42px" width="100%">
                                        <table class="dataTable" cellspacing="0" cellpadding="0" width="100%" border="1" visible="false">
                                            <thead>
                                                <tr>
                                                    <th style="width: 104px">Package</th>
                                                    <th style="width: 23px">PP</th>
                                                    <th style="width: 253px">Brand</th>
                                                    <th style="width: 104px">OUM</th>
                                                    <th style="width: 101px">CUR</th>
                                                    <th>Value</th>
                                                    <th>Qty</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="evenRow" align="left">
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                    <td><input type="text" readonly="readonly" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <asp:GridView 
                                            ID="Step3_GridView1" 
                                            runat="server" 
                                            CssClass="dataTableGrid" 
                                            Width="100%"
                                            AllowSorting="True" 
                                            AutoGenerateColumns="False">
                                            <RowStyle CssClass="evenRow"></RowStyle>
                                            <Columns>
                                            
                                                <%--<asp:HyperLinkField 
                                                    DataTextField="pkg_text" 
                                                    NavigateUrl="~/Package/package.aspx?pkgId=" 
                                                    HeaderText="Package" 
                                                    SortExpression="pkg_text">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px" />
                                                </asp:HyperLinkField>
                                                --%>
                                                <asp:TemplateField HeaderText="Package" SortExpression="pkg_text">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hpLinkPackage" runat="server" NavigateUrl='<%# GetPackageUrl(eval("pkg_pkgid"))%>' Text='<%# eval("pkg_text") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px" />
                                                 </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PP">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="pp" Checked='<%# eval("pp")%>' Enabled="false" />
                                                        <asp:label id="lblpackageID" runat="server" visible="false" text='<%# eval("pkg_pkgid") %>' />
                                                        <%--<asp:label id="lblvehID" runat="server" visible="false" text='<%# eval("veh_ID") %>' />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="br" HeaderText="Brand">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="uom" HeaderText="UOM">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" Width="100px" />
                                                </asp:BoundField>
                                                
                                           <%--    <asp:HyperLinkField
                                                    DataTextField="cur" 
                                                    NavigateUrl="~/Booking/Booking.aspx" 
                                                    HeaderText="CUR" 
                                                    SortExpression="cur">
                                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                    <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" />
                                                </asp:HyperLinkField>--%>
                                                
                                                <asp:TemplateField HeaderText="CUR" SortExpression="cur">
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton id="curLink" style="cursor: pointer" onClientclick="openCurrencyExchangeWindow();return false;"  runat="server">
                                                        <%# eval("cur") %></asp:LinkButton>--%>
                                                        <asp:LinkButton id="curLink" style="cursor: pointer"   runat="server">
                                                        <%# eval("cur") %></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" BorderWidth="1px" BorderStyle="Solid" Width="10px" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Value">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblvalueTC" runat="server" Width="50" Visible="false"><%# eval("val_tc") %></asp:Label>
                                                        <asp:Label ID="lblvaluePP" runat="server" Width="50" Visible="false"><%# eval("val_pp") %></asp:Label>
                                                        <asp:Label ID="lblvaluePD" runat="server" Width="50" Visible="false"><%# eval("val_pd") %></asp:Label>
                                                        <asp:Label ID="lblvalueVC" runat="server" Width="50" Visible="false"><%# eval("val_vc") %></asp:Label>
                                                        <asp:Label ID="lblvalueVDN" runat="server" Width="50" Visible="false"><%# eval("val_vdn") %></asp:Label>
                                                        <asp:Label ID="lblvalueVDG" runat="server" Width="50" Visible="false"><%# eval("val_vdg") %></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" BorderWidth="1px" BorderStyle="Solid" Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty">
                                                    <ItemTemplate>
                                                        <asp:TextBox  runat="server" ID="txtvalue" Width="30" MaxLength="2" text='<%# eval("qty") %>' />
                                                        <ajaxToolkit:FilteredTextBoxExtender 
                                                            ID="FLT_txtvalue" 
                                                            runat="server" 
                                                            FilterType="Numbers"  
                                                            TargetControlID="txtvalue" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" BorderWidth="1px" BorderStyle="Solid" Width="30px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="oddRow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </contenttemplate>
                </asp:UpdatePanel>
                
                <br />
            </asp:WizardStep>

<%--..................................................................................................................--%>
            <asp:WizardStep ID="SummaryStep4" runat="server" Title="Summary">
                <asp:UpdatePanel ID="Step4_UpdatePanelSummary" runat="server">
                    <contenttemplate>
                        <asp:Panel ID="Step4_pnlTitle" runat="server" Width="100%">
                            <table width="100%">
                                <tr>
                                    <td width="70%">
                                        <b><asp:Label ID="lblselectedmsg" runat="server" /></b>
                                    </td>
                                    <td width="30%" align="right">
                                        <b>
                                            <asp:CheckBox 
                                                ID="chkconvoy" 
                                                runat="server" 
                                                AutoPostBack="True" 
                                                TextAlign="Left"
                                                Text="Is Convoy" />
                                        </b>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        
                        <br />
                        
                        <asp:GridView 
                            ID="Step4_GridSummary" 
                            runat="server" 
                            CssClass="dataTableGrid" 
                            Width="100%"
                            AutoGenerateColumns="False" 
                            ShowFooter="True" 
                            DataKeyNames="SlvId">
                            <RowStyle CssClass="evenRow"></RowStyle>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="Check-Out" HeaderStyle-Width="120">
                                    <FooterTemplate>
                                        <i>Checked Total:</i>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <%# eval("CoDw") %>
                                        <br />
                                        <b>
                                            <%# FormattedDateAndTime(Eval("CoDate"))%>
                                        </b>
                                        <br />
                                        <%# eval("CoLoc") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Check-In" HeaderStyle-Width="120">
                                    <ItemTemplate>
                                        <%# eval("CiDw") %>
                                        <br />
                                        <%# FormattedDateAndTime(Eval("CiDate"))%>
                                        <br />
                                        <%# eval("CiLoc") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hire Pd&lt;br/&gt;Package" HeaderStyle-Width="85">
                                    <ItemTemplate>
                                        <%# eval("Hper") %><b>/ </b><%#Eval("Hpkg")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vehicle" HeaderStyle-Width="70">
                                    <ItemTemplate>
                                        <%#Eval("Veh")%><b>/ </b><%#Eval("VehSel")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bypass" FooterText="Check Total: ">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtByPassUnitNo" runat="server" Text='<%# eval("VehicleRanges") %>' Width="50" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Avail" HeaderStyle-Width="30">
                                    <ItemTemplate>
                                        <%#Eval("Avail")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Ref &lt;br/&gt; 3rd Party &lt;br/&gt;Request Source" HeaderStyle-Width="150">
                                    <ItemTemplate>
                                        <%#Eval("AgnRef")%> 
                                        <br />
                                        <b><%# Eval("ThirdPartyRef")%></b>
                                        <br />
                                        <asp:DropDownList ID="ddlRS" runat="server" AutoPostBack="true" Width="160" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost">
                                    <FooterTemplate>
                                        <b><asp:Label ID="lblfootercost" runat="server" Text='<%# eval("TotalCheckItems") %>' Width="30" /></b>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <%#Eval("Cost")%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <FooterStyle HorizontalAlign="Left" Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Curr" HeaderStyle-Width="20">
                                    <ItemTemplate>
                                        <%#Eval("Curr")%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <%#Eval("Status")%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField  HeaderText="Check" HeaderStyle-Width="40">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkCheck" AutoPostBack="true" OnCheckedChanged="CheckBox1_CheckedChanged"  />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="30px" />
                                    <HeaderStyle HorizontalAlign="center" Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Part of &lt;br/&gt;Convoy" Visible="False">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkPOC" Checked='<%# Eval("Poc") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="oddRow"></AlternatingRowStyle>
                        </asp:GridView>
                        
                        <br />
                        
                        <asp:Panel ID="Step4_pnlButtonsconfirmation" runat="server" Width="100%">
                            <table width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td align="right">
                                            <asp:Button 
                                                ID="BtnInquiry" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Inquiry(IN)"
                                                Width="80px" />
                                            <asp:Button 
                                                ID="BtnQuote" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Quote(QN)"
                                                Width="80px" />
                                            <asp:Button 
                                                ID="BtnKnockBack" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Knock-Back(KB)"
                                                Width="105px" />
                                            <asp:Button 
                                                ID="BtnWaitlist" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Waitlist(WL)"
                                                Width="80px" />
                                            <asp:Button 
                                                ID="BtnProvisional" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Provisional(NN)"
                                                Width="100px" />
                                            <asp:Button 
                                                ID="BtnConfirm" 
                                                runat="server" 
                                                CssClass="Button_Standard" 
                                                Text="Confirm(KK)"
                                                Width="80px" />
                                            <ajaxToolkit:ConfirmButtonExtender 
                                                ID="confirm_Inquiry" 
                                                runat="server" 
                                                TargetControlID="BtnInquiry"
                                                ConfirmText="" 
                                                Enabled="True" />
                                            <ajaxToolkit:ConfirmButtonExtender 
                                                ID="Confirm_Qoute" 
                                                runat="server" 
                                                TargetControlID="BtnQuote"
                                                ConfirmText="" 
                                                Enabled="True" />
                                            <ajaxToolkit:ConfirmButtonExtender 
                                                ID="Confirm_Knockback" 
                                                runat="server" 
                                                TargetControlID="BtnKnockBack"
                                                ConfirmText="" 
                                                Enabled="True" />
                                            <ajaxToolkit:ConfirmButtonExtender 
                                                ID="Confirm_Waitlisted" 
                                                runat="server" 
                                                TargetControlID="BtnWaitlist"
                                                ConfirmText="" 
                                                Enabled="True" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        
                        <br />
                        <!-- rev:mia 19nov2014 start - part of  slot dropdown addition-->
                        <asp:Panel ID="Step4_pnlNewBookinginformation" runat="server" Width="100%">
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="2"><b>Booking / Knock-Back </b></td>
                                </tr>
                                <tr id="tr" runat="server" width="100%">
                                    <td width="150" runat="server">New Booking Number:</td>
                                    <td runat="server">
                                        <asp:TextBox ID="txtNewBookingNumber" runat="server" Width="165px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Knock-Back:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlKnockBack" runat="server" Width="170px" AutoPostBack="True" />
                                    </td>
                                </tr>
                                
                            </table>
                        </asp:Panel>
                        <!-- rev:mia 19nov2014 end - part of  slot dropdown addition  -->
                         <Slot:TimeSlotAvailability Title="Vehicle Slot Availability" 
                                                    runat="server" 
                                                    SaveButtonText ="Save this Slot"
                                                    SkipButtonText = "Cancel"
                                                    ID="TimeSlotAvailability"  />
                    </contenttemplate>
                </asp:UpdatePanel>

            </asp:WizardStep>
        </WizardSteps>

<%----------------------------------------------------------------------------------------------------------------------%>
    </asp:Wizard>
 
    <asp:Panel ID="pnlSearchLink" runat="server">
        <a href="" id="searchAvailableVehiclesLink" target="_blank">Search available vehicles</a>

        <script type="text/javascript">
            (function () {
                var oLink = document.getElementById('searchAvailableVehiclesLink'),
                sQuery = '',
                oCodeRegexp = /^\s*[a-zA-Z0-9]+\s+/,
                trimStr = function (s) {
                    return s.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                },
                getCode = function (value) {
                    return trimStr((value.match(oCodeRegexp) || [''])[0]);
                },
                appendParam = function (value) {
                    if (sQuery)
                        sQuery += '&';
                    sQuery += value;
                };

                oLink.onclick = function () {
                    var sDateFrom = document.getElementById('<%=DateControlForCheckOut.ClientID %>').value,
                    sDateTo = document.getElementById('<%=DateControlForCheckIn.ClientID %>').value,
                    sBranchOut = getCode(document.getElementById('<%=PickerControlForCheckOut.ClientID %>').value),
                    sBranchIn = getCode(document.getElementById('<%=PickerControlForCheckIn.ClientID %>').value),
                    sTimeFrom = document.getElementById('<%=ddlAMPMcheckout.ClientID %>').value,
                    sTimeTo = document.getElementById('<%=ddlAMPMcheckin.ClientID %>').value,
                    //sProduct = getCode(document.getElementById('<%=PickerControlForProduct.ClientID %>').value),
                    sAgent = getCode(document.getElementById('<%=PickerControlForAgent.ClientID %>').value),
                    sChildren = document.getElementById('<%=ddlChildNO.ClientID %>').value,
                    sAdults = document.getElementById('<%=ddlAdultNO.ClientID %>').value,
                    aDateFromParts = sDateFrom.split('/'),
                    aDateToParts = sDateTo.split('/'),
                    sUrl;

                    sQuery = '';

                    //if (sProduct)
                    //    appendParam('vh=' + sProduct);

                    if (!sBranchOut)
                        return false;

                    if (sTimeFrom.toLowerCase() == 'am')
                        sTimeFrom = '0:00';
                    else if (sTimeFrom.toLowerCase() == 'pm')
                        sTimeFrom = '12:00';

                    if (sTimeTo.toLowerCase() == 'am')
                        sTimeTo = '11:59';
                    else if (sTimeFrom.toLowerCase() == 'pm')
                        sTimeTo = '23:59';

                    if (sBranchOut)
                        appendParam('pb=' + sBranchOut);

                    if (sBranchIn)
                        appendParam('db=' + sBranchIn);

                    if (sAgent)
                        appendParam('ac=' + sAgent);

                    if (sAdults)
                        appendParam('na=' + sAdults);

                    if (sChildren)
                        appendParam('nc=' + sChildren);

                    if (aDateFromParts.length == 3) {
                        appendParam('pd=' + aDateFromParts[0]);
                        appendParam('pm=' + aDateFromParts[1]);
                        appendParam('py=' + aDateFromParts[2]);
                        appendParam('pt=' + sTimeFrom);
                    }

                    if (aDateToParts.length == 3) {
                        appendParam('dd=' + aDateToParts[0]);
                        appendParam('dm=' + aDateToParts[1]);
                        appendParam('dy=' + aDateToParts[2]);
                        appendParam('dt=' + sTimeTo);
                    }

                    jQuery.ajax({
                        url: 'AuroraWebService.asmx/GetCountryByLocation?locationCode=' + sBranchOut,
                        dataType: "xml",
                        success: function (result) {
                            var sCountry = $(result.documentElement).text().toLowerCase();

                            if (!sCountry)
                                return;

                            sUrl = 'https://secure.motorhomesandcars.com/' + sCountry + '/select' + (sQuery ? '?' + sQuery + '&cr=' + '&rnd=' + (new Date()).valueOf() : '');
                        },
                        async: false
                    });

                    if (!sUrl)
                        return false;

                    oLink.href = sUrl;
                };
            })();
        </script>
    </asp:Panel>

                 <asp:UpdatePanel ID="updatePanelShowBookingRequestAgentPopup" runat="server">
                        <contenttemplate>
                            <uc4:ShowBookingRequestAgentPopupUserControl id="showBookingRequestAgentPopupUserControl1" runat="server"></uc4:ShowBookingRequestAgentPopupUserControl>
                    </contenttemplate>
                 </asp:UpdatePanel>

                 <asp:UpdatePanel ID="updatePanelBookingRequestAgentMgtPopup" runat="server">
                        <contenttemplate>
                            <uc5:BookingRequestAgentMgtPopupUserControl id="bookingRequestAgentMgtPopupUserControl1" runat="server"></uc5:BookingRequestAgentMgtPopupUserControl>
                        </contenttemplate>
                 </asp:UpdatePanel>

                 <uc6:ContactAgentMgtPopupUserControl ID="ContactAgentMgtPopupUserControl1" runat="server" />

                
                  <uc10:cbQuickAvail ID="cbQuickAvail" runat="server" 
                                          Title="Quick Availability" 
                                          Text="" 
                                          MessageType ="Information"
                                          LeftButton_AutoPostBack ="true"
                                          RightButton_AutoPostBack ="false"
                                          RightButton_Visible = "false"  />     

                 

                 <asp:Panel id="panelPopupExtension" runat="server" cssclass="modalPopup" style="display: none; width:400px;"  >
                        <div id="divAvailMessage" style="position: relative; border: 0px solid #ccc" />
                 </asp:Panel>

                  <!-- rev:mia 19nov2014 start - part of  slot dropdown addition-->
                  
                  <asp:UpdatePanel runat="server" ID="updatepanelSlot">
                      <ContentTemplate>
                         
                      </ContentTemplate>
                  </asp:UpdatePanel>
                  <!-- rev:mia 19nov2014 end - part of  slot dropdown addition  -->

    <%--FlagForSideBar template--%>
    <asp:HiddenField ID="FlagForSideBar" runat="server" EnableViewState="true" />
    <input id="hid_html_butSel" type="hidden" runat="server" />
    <asp:HiddenField ID="hidContactID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidCKOandCKIinfo" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidcompanion" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidAgentID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidPackageID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidGridViewItem" runat="server" EnableViewState="true"/>
    <asp:HiddenField ID="HidAgentCode" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidGinitialValue" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidCKO" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidCKI" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidCKOampm" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidCKIampm" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidbooid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidTemp" runat="server" EnableViewState="true" />
    <asp:Label ID="lblbooid" runat="server" EnableViewState="true" Visible="false" />
    <asp:HiddenField ID="Step3_HidRentalID" runat="server" EnableViewState="true" />
    <asp:Literal id="displayvalue" runat="server" EnableViewState="true" Visible="false" />
    <asp:label id="displayvaluetest" runat="server" EnableViewState="true" Visible="false" />
    <asp:HiddenField ID="HidDMW" runat="server" EnableViewState="true" />
    <asp:Literal ID="Hidchosenvehicle" runat="server" EnableViewState="true" Visible="false" />
    <asp:HiddenField ID="HidWarningMessage" runat="server" EnableViewState="true" />
  
    
    <%--
    
    <asp:HiddenField ID="HidOrig" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidTempPackageData" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidOrigPackageData" runat="server" EnableViewState="true" />--%>
   <asp:HiddenField ID="HidhasValueCKO" runat="server" EnableViewState="true" />
   <asp:HiddenField ID="HidhasValueCKI" runat="server" EnableViewState="true" />
  <%-- <asp:HiddenField ID="HiddenCompleteData" runat="server" EnableViewState="true" />--%>
  <asp:HiddenField ID="HidDS" runat="server" EnableViewState="true" />
  <asp:HiddenField ID="HiddenFieldVehicleExtension" runat="server" />
  <asp:HiddenField ID="HiddenFieldConfirmationBehaviorID" runat="server" EnableViewState="true" />
  <asp:Button ID="btnQuickAvailButton" runat="server"  style="visibility:hidden"/>

 



</asp:Content>
