﻿Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports System.Data
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Xml
Imports System.IO
Imports System.Math

Partial Class Booking_PriceMatchingListener_PriceMatchingListener
    Inherits System.Web.UI.Page


    Const DETAILS As String = "getChargeDetailsPriceMatching"
    Const SUMMARIES As String = "getChargeSummaryPriceMatching"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim qs As String = Request.QueryString("Mode")
        Dim returnedvalue As String = ""
        Dim encoded As String = ""

        Try


            If Not String.IsNullOrEmpty(qs) Then
                If qs.Equals("PRICEMATCHING") Then
                    encoded = Request("PRICEMATCHING")
                    Dim encodedProduct As String = Request("PRICEMATCHINGPRODUCTS")
                    Dim extraProduct As String = Request("EXTRAPARAMETER")

                    Logging.LogInformation("PRICEMATCHING QUERY", "PRICEMATCHINGPRODUCTS param: " & encodedProduct & vbCrLf & "EXTRAPARAMETER param: " & extraProduct)

                    Dim PriceMRntId As String = EncodeText(encoded.Split("|")(0))
                    Dim PriceMRequestCodTypeId As String = encoded.Split("|")(1)
                    Dim PriceMRequestor As String = EncodeText(encoded.Split("|")(2))
                    Dim PriceMCompetitorCodId As String = encoded.Split("|")(3)
                    Dim PriceMStatusCodId As String = encoded.Split("|")(4)

                    Dim PriceMCompetitorVehicle As String = EncodeText(encoded.Split("|")(5))
                    Dim PriceMCompetitorPrice As Decimal = EncodeText(encoded.Split("|")(6))

                    Dim PriceMCompetitorQuoteDate As String
                    If (String.IsNullOrEmpty(EncodeText(encoded.Split("|")(7)))) Then
                        PriceMCompetitorQuoteDate = String.Empty
                    Else
                        PriceMCompetitorQuoteDate = EncodeText(encoded.Split("|")(7))
                    End If


                    Dim PriceMCompetitorQuoteLink As String
                    If (String.IsNullOrEmpty(EncodeText(encoded.Split("|")(8)))) Then
                        PriceMCompetitorQuoteLink = String.Empty
                    Else
                        PriceMCompetitorQuoteLink = EncodeText(encoded.Split("|")(8))
                    End If

                    Dim PriceMNote As String
                    If (String.IsNullOrEmpty(EncodeText(encoded.Split("|")(9)))) Then
                        PriceMNote = String.Empty
                    Else
                        PriceMNote = EncodeText(encoded.Split("|")(9))
                    End If

                    Dim PriceMCompetitorOthers As String = EncodeText(encoded.Split("|")(10))
                    Dim PriceMDecisionBy As String = EncodeText(encoded.Split("|")(11))
                    Dim AddUsrId As String = EncodeText(encoded.Split("|")(12))
                    Dim PriceMSeqId As Int32 = 0
                    If (String.IsNullOrEmpty(encoded.Split("|")(13))) Then
                        PriceMSeqId = -1
                    Else
                        PriceMSeqId = Convert.ToInt32(encoded.Split("|")(13))
                    End If
                    If (PriceMSeqId <= 0) Then PriceMSeqId = -1

                    Dim PriceMCompetitorName As String = EncodeText(encoded.Split("|")(14))
                    If (PriceMCompetitorName <> "Others") Then
                        PriceMCompetitorOthers = String.Empty
                    End If

                    Dim PriceMAlternateVehicle As String = EncodeText(encoded.Split("|")(15))

                    Dim PriceMAlternatePrice As String = EncodeText(encoded.Split("|")(16))
                    If (String.IsNullOrEmpty(PriceMAlternatePrice)) Then
                        PriceMAlternatePrice = 0
                    End If

                    Dim agent As String = EncodeText(encoded.Split("|")(17))

                    Dim rowcount As Integer = BookingPriceMatching.BookingPriceMatchingInsertUpdate(IIf(String.IsNullOrEmpty(PriceMSeqId), -1, Convert.ToInt32(PriceMSeqId)), _
                                           PriceMRntId, _
                                           PriceMRequestCodTypeId, _
                                           PriceMRequestor, _
                                           PriceMCompetitorCodId, _
                                            PriceMCompetitorOthers, _
                                            PriceMCompetitorVehicle, _
                                            Convert.ToDecimal(PriceMCompetitorPrice), _
                                            PriceMCompetitorQuoteDate, _
                                            PriceMCompetitorQuoteLink, _
                                            PriceMStatusCodId, _
                                            PriceMDecisionBy, _
                                            PriceMNote, _
                                            AddUsrId, _
                                            PriceMAlternateVehicle, _
                                            PriceMAlternatePrice, _
                                            encodedProduct)
                    If (rowcount <> -1) Then
                        returnedvalue = "OK"
                        SendEmail(AddUsrId, EmailTemplateV2(PriceMRntId, extraProduct), agent)
                    Else
                        returnedvalue = "ERROR:Errors encountered processing the Price Matching"
                    End If
                ElseIf qs.Equals("LOADPRICEMATCH") Then
                    encoded = Request("LOADPRICEMATCH")
                    PMHireperiod = EncodeText(encoded.Split("|")(3))
                    Logging.LogInformation("LOADPRICEMATCH QUERY", "LOADPRICEMATCH param: " & encoded & "START TIME " & DateTime.Now.ToLongTimeString)
                    returnedvalue = ProductsMatrix(EncodeText(encoded.Split("|")(0)), EncodeText(encoded.Split("|")(1)), EncodeText(encoded.Split("|")(2)))
                    Logging.LogInformation("LOADPRICEMATCH QUERY", "LOADPRICEMATCH param: " & encoded & "END  TIME " & DateTime.Now.ToLongTimeString)
                ElseIf qs.Equals("LOADNOTEPRICEMATCH") Then
                    encoded = Request("LOADNOTEPRICEMATCH")
                    PMHireperiod = EncodeText(encoded.Split("|")(3))
                    Logging.LogInformation("LOADNOTEPRICEMATCH QUERY", "LOADNOTEPRICEMATCH param: " & encoded)
                    returnedvalue = PriceMatchNotes(EncodeText(encoded.Split("|")(0)), EncodeText(encoded.Split("|")(1)), EncodeText(encoded.Split("|")(2)))
                Else
                    returnedvalue = "ERROR:Errors encountered processing the Price Matching"
                    Logging.LogInformation("PRICEMATCH", returnedvalue)
                End If

            End If

        Catch ex As Exception
            Logging.LogError("PriceMatchingListener", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
            returnedvalue = "ERROR: Errors encountered processing your request"
        End Try
        Response.Write(returnedvalue)
        Response.End()
    End Sub

    Function EncodedString(ByVal xml As String) As String
        Return HttpUtility.UrlDecode(xml)
    End Function

#Region "Sending of Email"

    Private _emailsubject As String
    Private ReadOnly Property Subject As String
        Get
            Return _emailsubject
        End Get
    End Property

    Private Function EmailTemplateV2(PriceMRntId As String, htmlText As String) As String
        PriceMRntId = PriceMRntId.Replace("/", "-")
        Dim sb As New StringBuilder
        Try


            Dim pm As BookingPriceMatching = New BookingPriceMatching(PriceMRntId)

            Dim PMRentalId As String = pm.BookingInformationDataTable.Rows(0).Item("BookingRef").ToString
            Dim PMBaseRate As String = pm.BookingInformationDataTable.Rows(0).Item("UnitPrice").ToString
            Dim PMCheckout As String = pm.BookingInformationDataTable.Rows(0).Item("Check-Out").ToString
            Dim PMCheckin As String = pm.BookingInformationDataTable.Rows(0).Item("Check-in").ToString
            Dim PMHirePeriod As String = pm.BookingInformationDataTable.Rows(0).Item("HirePd").ToString
            Dim PMPackage As String = pm.BookingInformationDataTable.Rows(0).Item("Package").ToString
            Dim PMBrand As String = pm.BookingInformationDataTable.Rows(0).Item("Brand").ToString
            Dim PMVehicleName As String = pm.BookingInformationDataTable.Rows(0).Item("VehicleName").ToString
            Dim PMVehicle As String = pm.BookingInformationDataTable.Rows(0).Item("Vehicle").ToString.ToUpper
            Dim PMDiscountPrice As String = pm.BookingInformationDataTable.Rows(0).Item("DiscountedPrice").ToString
            Dim PMStatus As String = pm.BookingInformationDataTable.Rows(0).Item("BookingStatus").ToString


            Dim CompetitorVehicle As String = String.Empty
            Dim CompetitorCurrentPrice As String = String.Empty
            Dim NoteText As String = String.Empty
            Dim CompetitorDateQuote As String = String.Empty
            Dim CompetitorOther As String = String.Empty
            Dim CompetitorPrice As Decimal = 0
            Dim PriceMRequestCodTypeId_Value As String = String.Empty
            Dim PriceMCompetitorCodId_Value As String = String.Empty
            Dim PriceMStatusCodId_Value As String = String.Empty

            Dim PriceMRequestCodTypeId_text As String = String.Empty
            Dim PriceMCompetitorCodId_text As String = String.Empty
            Dim PriceMStatusCodId_text As String = String.Empty

            If (pm.BookingPriceMatchingDataTable.Rows.Count - 1 > -1) Then
                NoteText = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMNote").ToString)
                PriceMRequestCodTypeId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMRequestCodTypeId")
                PriceMCompetitorCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorCodId")
                PriceMStatusCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMStatusCodId")
            End If


            For Each item As DataRow In pm.PriceMatchingCodeDataTable.Rows
                If (PriceMRequestCodTypeId_Value = item.Item("CodId").ToString) Then
                    PriceMRequestCodTypeId_text = item.Item("CodDesc").ToString
                    Exit For
                End If
            Next

            For Each item As DataRow In pm.CompetitorsDataTable.Rows
                If (PriceMCompetitorCodId_Value = item.Item("CodId").ToString) Then
                    PriceMCompetitorCodId_text = item.Item("CodDesc").ToString
                    Exit For
                End If
            Next

            If (Not String.IsNullOrEmpty(CompetitorOther)) Then
                PriceMCompetitorCodId_text = CompetitorOther
            End If

            For Each item As DataRow In pm.PriceMatchingStatusDataTable.Rows
                If (PriceMStatusCodId_Value = item.Item("CodId").ToString) Then
                    PriceMStatusCodId_text = item.Item("CodDesc").ToString
                    Exit For
                End If
            Next

            Logging.LogInformation("Price Matching-Emailtemplate", PriceMStatusCodId_text)
            If (PriceMStatusCodId_text = "Initiated" And PriceMRequestCodTypeId_text <> "Book Now Campaign") Then
                _emailsubject = String.Concat(PriceMRequestCodTypeId_text, ": Request for ", PMBrand, "-", PMVehicle, " with ", PriceMCompetitorCodId_text, "-", CompetitorVehicle)
                _emailsubject = _emailsubject & String.Concat(" for Travel ", PMCheckout, " - ", PMCheckin)
                _emailsubject = _emailsubject & String.Concat(" THL Reference: ", PMRentalId)
            Else
                _emailsubject = ""
            End If

            Const TR_ROW As String = "<tr><td>{0}</td><td>{1}</td></tr>"
            Const TR_ROW_BOLD As String = "<tr><td style='align:right;'>{0}</td><td><b>{1}</b></td></tr>"
            sb.Append("<table>")
            sb.Append("<tr><td><b>INFORMATION</b></td></tr>")
            sb.AppendFormat(TR_ROW_BOLD, "REFERENCE:", PMRentalId)
            sb.AppendFormat(TR_ROW_BOLD, "REQUEST TYPE:", PriceMRequestCodTypeId_text.ToUpper)
            sb.AppendFormat(TR_ROW, "Pick-Up Details:".ToLower, PMCheckout)
            sb.AppendFormat(TR_ROW, "Drop-Off Details:".ToLower, PMCheckin)
            sb.AppendFormat(TR_ROW, "Our Brand:".ToLower, PMBrand.ToUpper)
            sb.AppendFormat(TR_ROW, "Package:".ToLower, PMPackage.ToUpper)
            sb.AppendFormat(TR_ROW, "Competitor Brand:".ToLower, PriceMCompetitorCodId_text.ToUpper)
            sb.Append("<tr><td></td></tr>")
            sb.Append("</table>")
            If (Not PriceMRequestCodTypeId_text.ToUpper.Equals("Price Match".ToUpper)) Then
                htmlText = "<hr/><b>Notes:</b>" & htmlText & "<hr/>"
            End If
            sb.Append(htmlText)


        Catch ex As Exception
            Logging.LogError("PriceMatchingListener-EMAIL TEMPLATE", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
            _emailsubject = ""
        End Try
        Return sb.ToString
    End Function

    Private Sub SendEmail(UserCode As String, sText As String, Optional sAgent As String = "")

        If (String.IsNullOrEmpty(Subject)) Then
            Logging.LogInformation("PriceMatching email", "Sending of Email for Price Matching is for INITIATED STATUS ONLY.. If the subject is empty then dont send email")
            Return
        End If

        Dim fromAddress As String = String.Empty
        Dim toAddress As String = String.Empty
        Dim bccAddress As String = String.Empty
        Dim emailSubject As String = Subject

        Try
            fromAddress = Aurora.Common.Data.ExecuteScalarSP("Booking_PriceMatchingGetRequestorEmail", UserCode)
            toAddress = Aurora.Common.Data.ExecuteScalarSP("PriceMatch_IsDirectAgent", sAgent)
            bccAddress = Aurora.Common.Data.ExecuteScalarSQL("select dbo.getUviValue('PRICEMATCH_BCC')")

        Catch ex As Exception
            If (String.IsNullOrEmpty(toAddress)) Then
                toAddress = fromAddress
            End If
        End Try



        Dim oMailMsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
        Try
            Dim splitAddresses As String()
            Dim itemaddress As String

            oMailMsg.From = fromAddress

            If (toAddress.Contains(",") = True) Then
                splitAddresses = toAddress.Split(",")
                toAddress = ""
                For Each itemaddress In splitAddresses
                    If (itemaddress.Contains("@") = True) Then
                        toAddress = toAddress & itemaddress & ";"
                    End If
                Next
                If (toAddress.EndsWith(";") = True) Then
                    toAddress = toAddress.Remove(toAddress.Length - 1, 1)
                End If

            End If
            oMailMsg.To = toAddress

            If (Not String.IsNullOrEmpty(bccAddress)) Then

                If (bccAddress.Contains(",") = True) Then
                    splitAddresses = bccAddress.Split(",")
                    bccAddress = ""
                    For Each itemaddress In splitAddresses
                        If (itemaddress.Contains("@") = True) Then
                            bccAddress = bccAddress & itemaddress & ";"
                        End If
                    Next
                    If (bccAddress.EndsWith(";") = True) Then
                        bccAddress = bccAddress.Remove(bccAddress.Length - 1, 1)
                    End If
                End If
                oMailMsg.Bcc = bccAddress
            End If

            ' fix for weird mapping problem
            oMailMsg.Headers.Add("Reply-To", fromAddress)
            ' fix for weird mapping problem
            oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
            oMailMsg.Subject = emailSubject
            oMailMsg.Body = sText
            oMailMsg.BodyFormat = Web.Mail.MailFormat.Html

            Logging.LogInformation("PriceMatching email".ToUpper, "Sending of Email start TO: " & toAddress & " FROM: " & fromAddress & " MSG: " & sText & " SUBJECT: " & emailSubject)

            Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
            Web.Mail.SmtpMail.Send(oMailMsg)
            Logging.LogInformation("PriceMatching email".ToUpper, "Sending of Email end")
        Catch ex As Exception
            Logging.LogError("PriceMatchingListener- SendEmail ", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
        End Try
    End Sub

#End Region

#Region "rev:mia July 19 2013 - added for pricematching..refactoring the codes"
    Dim _PMHireperiod As String
    Private Property PMHireperiod As String
        Get
            Return _PMHireperiod
        End Get
        Set(value As String)
            _PMHireperiod = value
        End Set
    End Property

    Private ReadOnly Property GetProducts(PMBookingId As String, PMRentalId As String, usercode As String) As String
        Get
            Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getBookedProductListForPriceMatch", PMBookingId, PMRentalId.Replace("/", "-"), "", 0, usercode).OuterXml
            Return xmlstring
        End Get
    End Property

    Private ReadOnly Property ProductsMatrix(PMBookingId As String, PMRentalId As String, usercode As String) As String
        Get

            Dim xmlproducts As String = ReformatXML(GetProducts(PMBookingId, PMRentalId, usercode), usercode)
            Return TransformedDocuments(xmlproducts)

        End Get
    End Property

    Private Function TransformedDocuments(productlist As String) As String

        Dim returnedxml As String = ""
        Dim priceMatchXml As New System.Xml.XmlDocument
        Try

            priceMatchXml.LoadXml(productlist)
            Dim transform As New System.Xml.Xsl.XslTransform()
            transform.Load(Server.MapPath(Me.ResolveUrl("~/Booking/xsl/PriceMatching.xslt")))

            Using writer As New StringWriter
                transform.Transform(priceMatchXml, Nothing, writer, Nothing)
                returnedxml = Server.HtmlDecode(writer.ToString())
            End Using

        Catch ex As Exception
            Logging.LogError("TransformedDocuments ERROR", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
            returnedxml = ""
        End Try

        Return returnedxml
    End Function

    Private Function AddNewAttribute(priceMatchXml As XmlDocument, _
                           ByRef elem As XmlElement, _
                           identity As String, _
                           value As String) As Boolean

        Dim addons_attr As XmlAttribute = priceMatchXml.CreateAttribute(identity)
        addons_attr.Value = value
        elem.Attributes.Append(addons_attr)
        Return True
    End Function

    Private Function ModifiedPriceNode(ByRef elem As XmlElement, pricematchtype As String, originalprice As Double, discountedprice As Double) As Boolean
        elem.SelectSingleNode("PriceMatchType").InnerText = pricematchtype
        elem.SelectSingleNode("OriginalPrice").InnerText = String.Format("{0:F2}", originalprice)
        elem.SelectSingleNode("DiscountedPrice").InnerText = String.Format("{0:F2}", discountedprice)
        Return True
    End Function

    Private Function ModifiedPriceNode(ByRef elem As XmlElement, originalprice As Double, discountedprice As Double) As Boolean
        elem.SelectSingleNode("OriginalPrice").InnerText = String.Format("{0:F2}", originalprice)
        elem.SelectSingleNode("DiscountedPrice").InnerText = String.Format("{0:F2}", discountedprice)
        Return True
    End Function


    Private Function GetElementValue(ByRef itemnode As XmlElement, identifier As String) As String
        Dim retvalue As String = ""
        Try
            retvalue = itemnode.SelectSingleNode(identifier).InnerText
        Catch ex As Exception
            Logging.LogError("PriceMatchingListener.GetElementValue", ex.Message & " element being looked is " & identifier)
        End Try
        Return retvalue
    End Function

    Private Function GetAttributeValue(ByRef itemnode As XmlElement, identifier As String) As String
        Dim retvalue As String = ""
        Try
            retvalue = itemnode.Attributes(identifier).Value
        Catch ex As Exception
            Logging.LogError("PriceMatchingListener.GetAttributeValue", ex.Message & " attribute being looked is " & identifier)
        End Try
        Return retvalue

    End Function

    Private Function IsELementAlreadyExist(ByRef priceMatchXml As XmlDocument, identity As String) As Boolean
        Dim tempXpath As String = "/data/BookedProduct[PriceMatchType='" & identity & "']"
        Dim itemnode As XmlElement = priceMatchXml.DocumentElement.SelectSingleNode(tempXpath)
        Dim result As Boolean = True
        Try
            result = IIf(itemnode Is Nothing, False, True)
        Catch ex As Exception
            Return False
        End Try
        Return result
    End Function

    Private Function ReformatXML(productlist As String, Optional UserCode As String = "") As String

        Dim priceMatchXml As New System.Xml.XmlDocument
        priceMatchXml.LoadXml(productlist)
        Dim priceMatch As String = ""
        Dim hasAdded As Boolean = False

        Dim root As XmlElement = priceMatchXml.DocumentElement
        Dim addNode As Boolean = False

        Dim addNodeForSubTotal As Boolean = False
        

        Dim bpdid As String = String.Empty
        Dim GetOriginalPrice As Decimal = 0
        Dim GetDiscountedPrice As Decimal = 0

        Dim GrossOfferedAmount_ADDSON As Decimal = 0
        Dim GrossOriginalAmount_ADDSON As Decimal = 0


        Dim GrossOfferedAmount_SUBTOTAL As Decimal = 0
        Dim GrossOriginalAmount_SUBTOTAL As Decimal = 0

        Dim GrossOfferedAmount_TOTAL As Decimal = 0
        Dim GrossOriginalAmount_TOTAL As Decimal = 0

        
        Dim counter As Integer = -1
        Dim QtUom As String = String.Empty
        Dim ChgDetUom As String = String.Empty

        Dim totalPercentage As Double = 0
        Dim ProdVehType As String = ""

        Dim isInsurance As Boolean = False
        Dim insuranceCount As Integer = priceMatchXml.SelectNodes("data/BookedProduct[ProdVehType='VPRO']").Count
        Dim xpath As String = ""
        Dim addoncounter As Integer = 1

        Dim PrdShortName As String = ""
        Dim ChgDetQty As String = ""

        Dim duplicateaddons As String = ""

        Dim duplicateaddons_GetDiscountedPrice As Double = 0
        Dim duplicateaddons_GetOriginalPrice As Double = 0
        Dim duplicateaddons_TotalQtUom As Integer = 1
        

        Dim insurancePrice_Orig As Double = 0
        Dim insurancePrice_Disc As Double = 0
        Dim insurancePrice_Orig_SingleItem As Double = 0
        Dim insurancePrice_Disc_SingleItem As Double = 0



        Dim vehiclePrice_Orig As Double = 0
        Dim vehiclePrice_Disc As Double = 0


        Dim agentcharge As Double = 0
        Dim customercharge As Double = 0
        Dim hireperiod As String = ""
        Dim summarycounter As Integer = -1
        Dim isVehicle As String = String.Empty
        priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[ProdVehType='VPRO']")
        Dim productIsAvehicle As Boolean = False

        Dim elemDailyTotal As XmlElement
        Dim elem As XmlElement


        Dim innertext As String = String.Empty
        Dim isAddonIsSingleItemButMultipleQty As Boolean = False

        Try
            Dim nodes As XmlNodeList = priceMatchXml.SelectNodes("data/BookedProduct")
            For Each itemnode As XmlNode In nodes


                isVehicle = GetElementValue(itemnode, "IsVehicle")
                agentcharge = 0
                customercharge = 0
                summarycounter = 0

                If (Not String.IsNullOrEmpty(isVehicle)) Then

                    productIsAvehicle = IIf(isVehicle = "1", True, False)
                    bpdid = GetElementValue(itemnode, "BpdId")
                    ProdVehType = GetElementValue(itemnode, "ProdVehType")
                    PrdShortName = GetElementValue(itemnode, "PrdShortName")
                    addoncounter = priceMatchXml.SelectNodes("data/BookedProduct[PrdShortName='" & PrdShortName & "']").Count
                    hireperiod = GetElementValue(itemnode, "HrPrd")

                    If (GetElementValue(itemnode, "AgnChg") <> "") Then
                        agentcharge = GetElementValue(itemnode, "AgnChg")
                    End If
                    
                    If (GetElementValue(itemnode, "CusChg") <> "") Then
                        customercharge = GetElementValue(itemnode, "CusChg")
                    End If
                    
                    If (productIsAvehicle) Then
                        summarycounter = GetSummariesCount(bpdid)
                        isInsurance = False
                    Else
                        isInsurance = CheckIfInsurance(PrdShortName)
                        If (isInsurance) Then
                            GetSummariesCount(bpdid)
                        End If
                    End If

                    If (hireperiod.Contains("DAY") = True) Then
                        isAddonIsSingleItemButMultipleQty = (addoncounter = 1 And Convert.ToInt16(hireperiod.Replace("DAY", "").TrimEnd) > 1 And Not productIsAvehicle And Not isInsurance)
                    ElseIf (hireperiod.Contains("24HR") = True) Then
                        isAddonIsSingleItemButMultipleQty = (addoncounter = 1 And Convert.ToInt16(hireperiod.Replace("24HR", "").TrimEnd) > 1 And Not productIsAvehicle And Not isInsurance)
                    Else
                        isAddonIsSingleItemButMultipleQty = (addoncounter = 1 And Convert.ToInt16(hireperiod.TrimEnd) > 1 And Not productIsAvehicle And Not isInsurance)
                    End If

                    ''System.Diagnostics.Debug.Assert(PrdShortName <> ("ALLINCA"), "product is " & PrdShortName)
                    If (addoncounter > 1 And Not isInsurance And Not productIsAvehicle And duplicateaddons <> PrdShortName) Then
                        hireperiod = 0
                        GetOriginalPrice = 0
                        GetDiscountedPrice = 0

                        Dim temp_AgentCharge As Double = 0
                        Dim temp_CustomerCharge As Double = 0
                        Dim temp_DiscountedCharge As Double = 0
                        Dim temp_hireperiod As Integer = 0
                        Dim duplicateNodes As XmlNodeList = priceMatchXml.SelectNodes("data/BookedProduct[PrdShortName='" & PrdShortName & "']")
                        For Each dupNode As XmlNode In duplicateNodes

                            If (Not String.IsNullOrEmpty(GetElementValue(dupNode, "AgnChg"))) Then
                                temp_AgentCharge = GetElementValue(dupNode, "AgnChg")
                            End If

                            If (Not String.IsNullOrEmpty(GetElementValue(dupNode, "CusChg"))) Then
                                temp_CustomerCharge = GetElementValue(dupNode, "CusChg")
                            End If

                            
                            If (Not String.IsNullOrEmpty(GetElementValue(dupNode, "HrPrd"))) Then
                                If (GetElementValue(dupNode, "HrPrd").Contains("DAY") = True) Then
                                    temp_hireperiod = GetElementValue(dupNode, "HrPrd").Replace("DAY", "").TrimEnd
                                ElseIf (GetElementValue(dupNode, "HrPrd").Contains("24HR") = True) Then
                                    temp_hireperiod = GetElementValue(dupNode, "HrPrd").Replace("24HR", "").TrimEnd
                                Else
                                    temp_hireperiod = GetElementValue(dupNode, "HrPrd").TrimEnd
                                End If

                            End If

                            If (Not String.IsNullOrEmpty(GetElementValue(dupNode, "HrPrd"))) Then
                                If GetElementValue(dupNode, "HrPrd").Contains("DAY") = True Then
                                    hireperiod = hireperiod + 1
                                Else
                                    hireperiod = hireperiod + temp_hireperiod
                                End If
                            End If


                            GetOriginalPrice = IIf(temp_AgentCharge <> 0, temp_AgentCharge, temp_CustomerCharge) + GetOriginalPrice
                            GetDiscountedPrice = IIf(temp_AgentCharge <> 0, temp_AgentCharge, temp_CustomerCharge) + GetDiscountedPrice


                        Next

                        GetOriginalPrice = String.Format("{0:F2}", GetOriginalPrice)
                        GetDiscountedPrice = String.Format("{0:F2}", GetDiscountedPrice)

                    Else

                        If (duplicateaddons = PrdShortName) Then
                            GetOriginalPrice = 0
                            GetDiscountedPrice = 0
                        Else
                            GetOriginalPrice = String.Format("{0:F2}", IIf(agentcharge <> 0, agentcharge, customercharge))
                            GetDiscountedPrice = String.Format("{0:F2}", IIf(agentcharge <> 0, agentcharge, customercharge))
                        End If

                    End If


                    Try
                        QtUom = hireperiod.Split(" ")(0)
                        ChgDetUom = hireperiod.Split(" ")(1)
                        ChgDetQty = QtUom

                    Catch ex As Exception
                        ChgDetUom = hireperiod
                        ChgDetQty = hireperiod
                    End Try

                    If (productIsAvehicle) Then
                        GetOriginalPrice = OriginalRate
                        GetDiscountedPrice = OfferedRate
                    End If

                    Try


                        If (isInsurance) Then

                            If (ChgDetQty > 1) Then
                                GetOriginalPrice = OriginalRate
                                GetDiscountedPrice = OfferedRate

                                ''insurancePrice_Orig = String.Format("{0:F2}", (GetOriginalPrice + insurancePrice_Orig) / ChgDetQty)
                                ''insurancePrice_Disc = String.Format("{0:F2}", (GetDiscountedPrice + insurancePrice_Disc) / ChgDetQty)

                                insurancePrice_Orig = String.Format("{0:F2}", (GetOriginalPrice + insurancePrice_Orig))
                                insurancePrice_Disc = String.Format("{0:F2}", (GetDiscountedPrice + insurancePrice_Disc))
                            Else
                                insurancePrice_Orig_SingleItem = String.Format("{0:F2}", insurancePrice_Orig_SingleItem + GetOriginalPrice)
                                insurancePrice_Disc_SingleItem = String.Format("{0:F2}", insurancePrice_Disc_SingleItem + GetDiscountedPrice)
                            End If

                            ChgDetQty = 1
                        Else

                            Dim tempHireperiod As Integer = IIf(hireperiod.Contains("DAY") = True, 1, hireperiod.Replace("DAY", ""))
                            If (addoncounter > 1) Then

                                If (String.IsNullOrEmpty(duplicateaddons)) Then

                                    AddNewAttribute(priceMatchXml, itemnode, "IsBasedDuplicate", "Y")
                                    AddNewAttribute(priceMatchXml, itemnode, "numberhired", tempHireperiod)
                                    '' AddNewAttribute(priceMatchXml, itemnode, "numberhired", hireperiod.Replace("DAY", "").TrimEnd)

                                    AddNewAttribute(priceMatchXml, itemnode, "BasedDuplicatePrice", GetOriginalPrice)
                                    duplicateaddons = PrdShortName
                                Else
                                    If (duplicateaddons = PrdShortName) Then
                                        'Dim duplicateaddonNode As XmlElement = priceMatchXml.SelectSingleNode("/data/BookedProduct[PrdShortName='" & PrdShortName & "'][@IsBasedDuplicate = 'Y']")
                                        'If (Not duplicateaddonNode Is Nothing) Then
                                        '    duplicateaddons_TotalQtUom = GetAttributeValue(duplicateaddonNode, "numberhired") ''Convert.ToInt16(duplicateaddonNode.Attributes("numberhired").Value)
                                        '    duplicateaddonNode.Attributes("numberhired").Value = Convert.ToInt16(duplicateaddons_TotalQtUom) + 1 ''ChgDetQty
                                        '    AddNewAttribute(priceMatchXml, duplicateaddonNode, "numberhired", duplicateaddonNode.Attributes("numberhired").Value)
                                        'End If
                                        AddNewAttribute(priceMatchXml, itemnode, "IsBasedDuplicate", "N")
                                    Else
                                        AddNewAttribute(priceMatchXml, itemnode, "IsBasedDuplicate", "Y")
                                        AddNewAttribute(priceMatchXml, itemnode, "numberhired", tempHireperiod)

                                        AddNewAttribute(priceMatchXml, itemnode, "BasedDuplicatePrice", GetOriginalPrice)
                                        duplicateaddons = PrdShortName
                                    End If
                                End If
                            Else
                                If (isAddonIsSingleItemButMultipleQty And addoncounter = 1) Then
                                    AddNewAttribute(priceMatchXml, itemnode, "IsBasedDuplicate", "Y")
                                    AddNewAttribute(priceMatchXml, itemnode, "numberhired", tempHireperiod)
                                    AddNewAttribute(priceMatchXml, itemnode, "BasedDuplicatePrice", GetOriginalPrice)
                                End If
                            End If

                            QtUom = ChgDetQty
                        End If

                        itemnode.SelectSingleNode("UOM").InnerText = QtUom
                    Catch ex As Exception
                    End Try

                    ''calculate percentage
                    If (GetOriginalPrice > 0 And GetDiscountedPrice > 0) Then
                        totalPercentage = totalPercentage + (((GetOriginalPrice - GetDiscountedPrice) / GetOriginalPrice) * 100)
                        AddNewAttribute(priceMatchXml, itemnode, "Percentage", String.Format("{0:F2}", (((GetOriginalPrice - GetDiscountedPrice) / GetOriginalPrice) * 100)))
                    End If


                    ''for vehicle
                    If (productIsAvehicle) Then
                        vehiclePrice_Orig = GetOriginalPrice
                        vehiclePrice_Disc = GetDiscountedPrice

                        ModifiedPriceNode(itemnode, "Vehicle", vehiclePrice_Orig, vehiclePrice_Disc)
                        GrossOfferedAmount_SUBTOTAL = String.Format("{0:F2}", GrossOfferedAmount_SUBTOTAL + vehiclePrice_Disc)
                        GrossOriginalAmount_SUBTOTAL = String.Format("{0:F2}", GrossOriginalAmount_SUBTOTAL + vehiclePrice_Orig)

                        counter = counter + 1
                    Else

                        If (isInsurance) Then
                            AddNewAttribute(priceMatchXml, itemnode, "insOum", PMHireperiod.TrimEnd)
                            If (insurancePrice_Orig = 0 And insurancePrice_Disc = 0) Then
                                ModifiedPriceNode(itemnode, "Insurance", insurancePrice_Orig_SingleItem, insurancePrice_Disc_SingleItem)
                            Else
                                ModifiedPriceNode(itemnode, "Insurance", insurancePrice_Orig, insurancePrice_Disc)
                            End If

                            GrossOfferedAmount_SUBTOTAL = String.Format("{0:F2}", GrossOfferedAmount_SUBTOTAL + insurancePrice_Orig)
                            GrossOriginalAmount_SUBTOTAL = String.Format("{0:F2}", GrossOriginalAmount_SUBTOTAL + insurancePrice_Orig)

                            If counter = 0 Then
                                addNode = True
                                addNodeForSubTotal = True
                            End If

                            counter = counter + 1

                        Else

                            If (GetOriginalPrice >= 0) Then
                                counter = counter + 1
                                AddNewAttribute(priceMatchXml, itemnode, "addons", "Total")
                                ModifiedPriceNode(itemnode, IIf(Not hasAdded, "", GetElementValue(itemnode, "PriceMatchType")), GetOriginalPrice, GetDiscountedPrice)
                                GrossOfferedAmount_ADDSON = GrossOfferedAmount_ADDSON + GetDiscountedPrice
                                GrossOriginalAmount_ADDSON = GrossOriginalAmount_ADDSON + GetOriginalPrice
                            End If

                        End If
                    End If


                    If (nodes.Count = 1 Or (addNode And isInsurance = True)) Then
                        elem = DailySubTotalNewNode(priceMatchXml, IIf(addNodeForSubTotal Or nodes.Count = 1, "DailySubTotal", ""), GrossOriginalAmount_SUBTOTAL, GrossOfferedAmount_SUBTOTAL)
                        If (addNodeForSubTotal) Then
                            If (insuranceCount > 1) Then
                                xpath = "/data/BookedProduct[ProdVehType='VPRO'][position() = '1']"
                            Else
                                xpath = "/data/BookedProduct[ProdVehType='VPRO'][position() = '" & insuranceCount & "']"
                            End If
                            priceMatchXml.DocumentElement.InsertAfter(elem, priceMatchXml.DocumentElement.SelectSingleNode(xpath))
                        Else
                            If (nodes.Count = 1) Then
                                priceMatchXml.DocumentElement.InsertAfter(elem, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='Vehicle']"))
                            End If
                        End If

                        GrossOriginalAmount_SUBTOTAL = (vehiclePrice_Orig + insurancePrice_Orig) * PMHireperiod
                        GrossOfferedAmount_SUBTOTAL = (vehiclePrice_Disc + insurancePrice_Disc) * PMHireperiod
                        elemDailyTotal = DailyTotalAddNode(priceMatchXml, "DailyTotal", GrossOriginalAmount_SUBTOTAL, GrossOfferedAmount_SUBTOTAL, PMHireperiod)
                        priceMatchXml.DocumentElement.InsertAfter(elemDailyTotal, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailySubTotal']"))

                        addNode = False
                        addNodeForSubTotal = False
                        hasAdded = True

                    ElseIf isInsurance = False And Not productIsAvehicle And hasAdded = False Then

                        ''no insurance and only vehicle and add ons
                        If (Not IsELementAlreadyExist(priceMatchXml, "DailySubTotal")) Then
                            elem = DailySubTotalNewNode(priceMatchXml, "DailySubTotal", GrossOriginalAmount_SUBTOTAL, GrossOfferedAmount_SUBTOTAL)
                            priceMatchXml.DocumentElement.InsertAfter(elem, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='Vehicle']"))
                        End If

                        GrossOriginalAmount_SUBTOTAL = (vehiclePrice_Orig + insurancePrice_Orig) * PMHireperiod
                        GrossOfferedAmount_SUBTOTAL = (vehiclePrice_Disc + insurancePrice_Disc) * PMHireperiod

                        If (Not IsELementAlreadyExist(priceMatchXml, "DailyTotal")) Then
                            elemDailyTotal = DailyTotalAddNode(priceMatchXml, "DailyTotal", GrossOriginalAmount_SUBTOTAL, GrossOfferedAmount_SUBTOTAL, PMHireperiod)
                            priceMatchXml.DocumentElement.InsertAfter(elemDailyTotal, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailySubTotal']"))
                        End If


                    Else
                        If (isInsurance And counter > 0) Then
                            GrossOriginalAmount_SUBTOTAL = ((vehiclePrice_Orig + insurancePrice_Orig) * PMHireperiod) + insurancePrice_Orig_SingleItem
                            GrossOfferedAmount_SUBTOTAL = ((vehiclePrice_Disc + insurancePrice_Disc) * PMHireperiod) + insurancePrice_Orig_SingleItem
                            AddNewAttribute(priceMatchXml, itemnode, "insOum", hireperiod.TrimEnd)
                            If (hireperiod.Contains("DAY") = True) Then
                                ModifiedPriceNode(itemnode, "Insurance", insurancePrice_Orig, insurancePrice_Disc)
                            Else
                                ModifiedPriceNode(itemnode, "Insurance", GetOriginalPrice, GetDiscountedPrice)
                            End If


                            priceMatchXml.DocumentElement.InsertBefore(itemnode, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailySubTotal']"))

                            Dim xmlchangeInsurance As XmlElement = priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailyTotal']")
                            ModifiedPriceNode(xmlchangeInsurance, GrossOriginalAmount_SUBTOTAL, GrossOfferedAmount_SUBTOTAL)

                            xmlchangeInsurance = priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailySubTotal']")
                            ModifiedPriceNode(xmlchangeInsurance, (vehiclePrice_Orig + insurancePrice_Orig + insurancePrice_Orig_SingleItem), (vehiclePrice_Disc + insurancePrice_Disc + insurancePrice_Orig_SingleItem))

                        ElseIf isInsurance = False And Not productIsAvehicle And hasAdded = True And counter > 0 Then
                            priceMatchXml.DocumentElement.InsertAfter(itemnode, priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailyTotal']"))
                        End If
                    End If


                End If

            Next

            Dim addnodes As XmlNodeList = priceMatchXml.SelectNodes("data/BookedProduct[@IsBasedDuplicate='Y']")
            For Each myitem As XmlNode In addnodes

                PrdShortName = GetElementValue(myitem, "PrdShortName")
                duplicateaddons_GetDiscountedPrice = Convert.ToDouble(GetAttributeValue(myitem, "BasedDuplicatePrice"))
                duplicateaddons_GetOriginalPrice = Convert.ToDouble(GetAttributeValue(myitem, "BasedDuplicatePrice"))
                duplicateaddons_TotalQtUom = Convert.ToDouble(GetAttributeValue(myitem, "numberhired"))

                If (duplicateaddons_TotalQtUom > 1) Then
                    myitem.SelectSingleNode("PrdShortName").InnerText = String.Concat(PrdShortName, "x", (duplicateaddons_TotalQtUom).ToString)
                End If


                ModifiedPriceNode(myitem, duplicateaddons_GetOriginalPrice, duplicateaddons_GetDiscountedPrice)
            Next

            addnodes = priceMatchXml.SelectNodes("data/BookedProduct[@IsBasedDuplicate='N']")
            For Each myitem As XmlNode In addnodes
                myitem.ParentNode.RemoveChild(myitem)
            Next

            priceMatchXml.DocumentElement.InsertBefore(AddOnsNewNode(priceMatchXml, "Daily Rates", "PriceMatchType"), priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='Vehicle']"))
            priceMatchXml.DocumentElement.InsertAfter(AddOnsNewNode(priceMatchXml, "AddOns", "PriceMatchType"), priceMatchXml.DocumentElement.SelectSingleNode("/data/BookedProduct[PriceMatchType='DailyTotal']"))

            Dim elemHirePeriodTotal As XmlElement = priceMatchXml.CreateElement("BookedProduct")
            AddNewAttribute(priceMatchXml, elemHirePeriodTotal, "addons", "BookingTotal")
            AddNewAttribute(priceMatchXml, elemHirePeriodTotal, "totalPercentage", totalPercentage)

            GrossOfferedAmount_TOTAL = String.Format("{0:F2}", GrossOfferedAmount_TOTAL + GrossOfferedAmount_SUBTOTAL + GrossOfferedAmount_ADDSON)
            GrossOriginalAmount_TOTAL = String.Format("{0:F2}", GrossOriginalAmount_TOTAL + GrossOriginalAmount_SUBTOTAL + GrossOriginalAmount_ADDSON)


            DailyTotalNode(priceMatchXml, _
                           elemHirePeriodTotal, _
                           "BookingTotal", _
                           GrossOriginalAmount_TOTAL, _
                           GrossOfferedAmount_TOTAL, _
                           PMHireperiod)
            priceMatchXml.DocumentElement.InsertAfter(elemHirePeriodTotal, priceMatchXml.DocumentElement.LastChild)

        Catch ex As Exception
            Logging.LogError("ReformatXML ERROR", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
        End Try
        Return priceMatchXml.OuterXml
    End Function

  
    Function DailyTotalAddNode(priceMatchXml As XmlDocument, _
                          innertext As String, _
                          Optional GrossOriginalAmount As Decimal = 0, _
                          Optional GrossOfferedAmount As Double = 0, _
                          Optional hireperiod As String = "") As XmlElement

        Dim elem As XmlElement = priceMatchXml.CreateElement("BookedProduct")
        Dim node As XmlNode

        node = priceMatchXml.CreateElement("PrdShortName")
        node.InnerText = hireperiod

        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("OriginalPrice")
        node.InnerText = String.Format("{0:F2}", GrossOriginalAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("DiscountedPrice")
        node.InnerText = String.Format("{0:F2}", GrossOfferedAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("PriceMatchType")
        node.InnerText = innertext
        elem.AppendChild(node)

        Return elem
    End Function

    Function AddOnsNewNode(priceMatchXml As XmlDocument, _
                           innertext As String, identity As String) As XmlElement

        Dim elem As XmlElement = priceMatchXml.CreateElement("BookedProduct")
        Dim node As XmlNode
        node = priceMatchXml.CreateElement(identity)
        node.InnerText = innertext
        elem.AppendChild(node)

        Return elem
    End Function

    Function DailySubTotalNewNode(priceMatchXml As XmlDocument, _
                          innertext As String, _
                          Optional GrossOriginalAmount As Decimal = 0, _
                          Optional GrossOfferedAmount As Double = 0) As XmlElement

        Dim elem As XmlElement = priceMatchXml.CreateElement("BookedProduct")
        Dim node As XmlNode

        node = priceMatchXml.CreateElement("OriginalPrice")
        node.InnerText = String.Format("{0:F2}", GrossOriginalAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("DiscountedPrice")
        node.InnerText = String.Format("{0:F2}", GrossOfferedAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("PriceMatchType")
        node.InnerText = innertext
        elem.AppendChild(node)

        Return elem
    End Function

    Sub DailyTotalNode(priceMatchXml As XmlDocument, _
                         ByRef elem As XmlElement, _
                         innertext As String, _
                         Optional GrossOriginalAmount As Decimal = 0, _
                         Optional GrossOfferedAmount As Double = 0, _
                         Optional hireperiod As String = "")

        Dim node As XmlNode

        node = priceMatchXml.CreateElement("PrdShortName")
        node.InnerText = hireperiod

        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("OriginalPrice")
        node.InnerText = String.Format("{0:F2}", GrossOriginalAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("DiscountedPrice")
        node.InnerText = String.Format("{0:F2}", GrossOfferedAmount)
        elem.AppendChild(node)

        node = priceMatchXml.CreateElement("PriceMatchType")
        node.InnerText = innertext
        elem.AppendChild(node)


    End Sub


    Private _OriginalRate As String
    Private ReadOnly Property OriginalRate As String
        Get
            Return _OriginalRate
        End Get
    End Property

    Private _OfferedRate As String
    Private ReadOnly Property OfferedRate As String
        Get
            Return _OfferedRate
        End Get
    End Property


    Private Function CheckIfInsurance(product As String) As Boolean
        Dim result As String
        result = Aurora.Common.Data.ExecuteScalarSP("PriceMatching_CheckIfInsurance", product)
        Return IIf(result.ToUpper = "YES", True, False)
    End Function

    Private Function GetSummariesCount(bpdid As String) As Integer
        

        Dim _baseOrig As Double = 0
        Dim _offeredOrig As Double = 0
        Dim _QtUOM As Integer = 0



        Dim _DisTemp As Double = 0
        Dim _RateTemp As Double = 0
        Dim _baseTemp As Double = 0
        Dim _offeredOrigTemp As Double = 0
        Dim _QtUOMtemp As String = String.Empty

        Dim summary As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc(SUMMARIES, bpdid, "", "").OuterXml
        Dim xmltemp As New XmlDocument
        summary = summary.Replace("<Details>", "")
        summary = summary.Replace("</Details>", "")
        xmltemp.LoadXml(summary)

        Dim nodes As XmlNodeList = xmltemp.SelectNodes("data/Detail")

        For Each mynode As XmlNode In nodes

            If (String.IsNullOrEmpty(mynode.SelectSingleNode("QtUOM").InnerText)) Then
                _QtUOMtemp = "1"
            Else
                If (mynode.SelectSingleNode("QtUOM").InnerText.Contains("DAY") = True) Then
                    _QtUOMtemp = mynode.SelectSingleNode("QtUOM").InnerText.Replace("DAY", "").TrimEnd
                ElseIf (mynode.SelectSingleNode("QtUOM").InnerText.Contains("24HR") = True) Then
                    _QtUOMtemp = mynode.SelectSingleNode("QtUOM").InnerText.Replace("24HR", "").TrimEnd
                End If
                If (String.IsNullOrEmpty(_QtUOMtemp)) Then _QtUOMtemp = "0"
            End If

            'If (_QtUOMtemp <> "0") Then
            _QtUOM = Convert.ToInt16(_QtUOMtemp) + _QtUOM
            'End If


            If (String.IsNullOrEmpty(mynode.SelectSingleNode("Rate").InnerText)) Then
                _RateTemp = 0
            Else
                _RateTemp = Convert.ToDouble(mynode.SelectSingleNode("Rate").InnerText)
            End If

            If (String.IsNullOrEmpty(mynode.SelectSingleNode("Dis").InnerText)) Then
                _DisTemp = 0
            Else
                _DisTemp = Convert.ToDouble(mynode.SelectSingleNode("Dis").InnerText)
                _DisTemp = Math.Abs(_DisTemp)
            End If

            ''get the base Rate formula: Rate + Dis
            _baseTemp = (_RateTemp + _DisTemp)
            _baseTemp = _baseTemp * Convert.ToInt16(_QtUOMtemp)
            _baseOrig = _baseTemp + _baseOrig

            ''get the base Rate formula: Rate 
            _offeredOrigTemp = _RateTemp * Convert.ToInt16(_QtUOMtemp)
            _offeredOrig = _offeredOrig + _offeredOrigTemp
        Next

        _baseOrig = String.Format("{0:F2}", _baseOrig / _QtUOM)
        _offeredOrig = String.Format("{0:F2}", _offeredOrig / _QtUOM)

        _OriginalRate = _baseOrig.ToString
        _OfferedRate = _offeredOrig.ToString



        Return xmltemp.SelectNodes("data/Detail").Count - 1
    End Function

    

#End Region

#Region "Notes Loading"
    Private _PriceMatchNotes As String = "<table id='TablePriceMatchNotes'><tr valign='top'><td><textarea name='priceMatchNote' rows='10' cols='120' class='priceMatchNote'>{0}</textarea></td></tr></table><br/>"
    Private ReadOnly Property PriceMatchNotes(PMBookingId As String, PMRentalId As String, usercode As String) As String
        Get
            Dim xmlproducts As String = GetProducts(PMBookingId, PMRentalId, usercode)
            Dim dsNotes As New DataSet
            dsNotes.ReadXml(New XmlTextReader(xmlproducts, System.Xml.XmlNodeType.Document, Nothing))
            Dim note As String = dsNotes.Tables(0).Rows(0)("Notes").ToString
            _PriceMatchNotes = _PriceMatchNotes.Replace("{0}", note)
            Return _PriceMatchNotes
        End Get

    End Property

#End Region

#Region "not used"

    'Sub DailySubTotalNode(priceMatchXml As XmlDocument, _
    '                      ByRef elem As XmlElement, _
    '                      innertext As String, _
    '                      Optional GrossOriginalAmount As Decimal = 0, _
    '                      Optional GrossOfferedAmount As Double = 0)

    '    Dim node As XmlNode

    '    node = priceMatchXml.CreateElement("OriginalPrice")
    '    node.InnerText = String.Format("{0:F2}", GrossOriginalAmount)
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("DiscountedPrice")
    '    node.InnerText = String.Format("{0:F2}", GrossOfferedAmount)
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("PriceMatchType")
    '    node.InnerText = innertext
    '    elem.AppendChild(node)


    'End Sub

    'Sub DailyRatesAndAddOnsNode(priceMatchXml As XmlDocument, _
    '                       ByRef elem As XmlElement, _
    '                       innertext As String)


    '    Dim node As XmlNode
    '    node = priceMatchXml.CreateElement("PriceMatchType")
    '    node.InnerText = innertext
    '    elem.AppendChild(node)
    'End Sub
    'Sub NewPriceMatchNode(priceMatchXml As XmlDocument, _
    '                          ByRef elem As XmlElement, _
    '                          innertext As String, _
    '                          Optional GrossOriginalAmount As Decimal = 0, _
    '                          Optional GrossOfferedAmount As Double = 0, _
    '                          Optional hireperiod As String = "")

    '    Dim node As XmlNode


    '    node = priceMatchXml.CreateElement("BpdId")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("PrdId")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("PrdShortName")
    '    If (Not String.IsNullOrEmpty(hireperiod)) Then
    '        node.InnerText = hireperiod
    '    End If
    '    elem.AppendChild(node)


    '    node = priceMatchXml.CreateElement("FrDate")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("ToDt")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("LocCode")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("CKLocCode")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("OriginalPrice")
    '    node.InnerText = String.Format("{0:F2}", GrossOriginalAmount)
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("DiscountedPrice")
    '    node.InnerText = String.Format("{0:F2}", GrossOfferedAmount)
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("IsVehicle")
    '    elem.AppendChild(node)

    '    node = priceMatchXml.CreateElement("PriceMatchType")
    '    node.InnerText = innertext
    '    elem.AppendChild(node)


    'End Sub
    'Private Function EmailTemplate(PriceMRntId As String) As String
    '    PriceMRntId = PriceMRntId.Replace("/", "-")
    '    Dim sb As New StringBuilder
    '    Try


    '        Dim pm As BookingPriceMatching = New BookingPriceMatching(PriceMRntId)

    '        Dim PMRentalId As String = pm.BookingInformationDataTable.Rows(0).Item("BookingRef").ToString
    '        Dim PMBaseRate As String = pm.BookingInformationDataTable.Rows(0).Item("UnitPrice").ToString
    '        Dim PMCheckout As String = pm.BookingInformationDataTable.Rows(0).Item("Check-Out").ToString
    '        Dim PMCheckin As String = pm.BookingInformationDataTable.Rows(0).Item("Check-in").ToString
    '        Dim PMHirePeriod As String = pm.BookingInformationDataTable.Rows(0).Item("HirePd").ToString
    '        Dim PMPackage As String = pm.BookingInformationDataTable.Rows(0).Item("Package").ToString
    '        Dim PMBrand As String = pm.BookingInformationDataTable.Rows(0).Item("Brand").ToString
    '        Dim PMVehicleName As String = pm.BookingInformationDataTable.Rows(0).Item("VehicleName").ToString
    '        Dim PMVehicle As String = pm.BookingInformationDataTable.Rows(0).Item("Vehicle").ToString.ToUpper
    '        Dim PMDiscountPrice As String = pm.BookingInformationDataTable.Rows(0).Item("DiscountedPrice").ToString
    '        Dim PMStatus As String = pm.BookingInformationDataTable.Rows(0).Item("BookingStatus").ToString


    '        Dim CompetitorVehicle As String = String.Empty
    '        Dim CompetitorCurrentPrice As String = String.Empty
    '        Dim NoteText As String = String.Empty
    '        Dim CompetitorDateQuote As String = String.Empty
    '        Dim CompetitorOther As String = String.Empty
    '        Dim CompetitorPrice As Decimal = 0
    '        Dim PriceMRequestCodTypeId_Value As String = String.Empty
    '        Dim PriceMCompetitorCodId_Value As String = String.Empty
    '        Dim PriceMStatusCodId_Value As String = String.Empty

    '        Dim PriceMRequestCodTypeId_text As String = String.Empty
    '        Dim PriceMCompetitorCodId_text As String = String.Empty
    '        Dim PriceMStatusCodId_text As String = String.Empty
    '        If (pm.BookingPriceMatchingDataTable.Rows.Count - 1 > -1) Then

    '            CompetitorVehicle = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorVehicle").ToString)
    '            CompetitorCurrentPrice = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorPrice").ToString)
    '            NoteText = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMNote").ToString)

    '            If (Not String.IsNullOrEmpty(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorQuoteDate").ToString)) Then
    '                CompetitorDateQuote = Convert.ToDateTime(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorQuoteDate").ToString)
    '            End If


    '            CompetitorOther = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorOthers").ToString)
    '            CompetitorPrice = Convert.ToDecimal(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorPrice").ToString)

    '            PriceMRequestCodTypeId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMRequestCodTypeId")
    '            PriceMCompetitorCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorCodId")
    '            PriceMStatusCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMStatusCodId")
    '        End If


    '        For Each item As DataRow In pm.PriceMatchingCodeDataTable.Rows
    '            If (PriceMRequestCodTypeId_Value = item.Item("CodId").ToString) Then
    '                PriceMRequestCodTypeId_text = item.Item("CodDesc").ToString
    '                Exit For
    '            End If
    '        Next

    '        For Each item As DataRow In pm.CompetitorsDataTable.Rows
    '            If (PriceMCompetitorCodId_Value = item.Item("CodId").ToString) Then
    '                PriceMCompetitorCodId_text = item.Item("CodDesc").ToString
    '                Exit For
    '            End If
    '        Next

    '        If (Not String.IsNullOrEmpty(CompetitorOther)) Then
    '            PriceMCompetitorCodId_text = CompetitorOther
    '        End If

    '        For Each item As DataRow In pm.PriceMatchingStatusDataTable.Rows
    '            If (PriceMStatusCodId_Value = item.Item("CodId").ToString) Then
    '                PriceMStatusCodId_text = item.Item("CodDesc").ToString
    '                Exit For
    '            End If
    '        Next

    '        Logging.LogInformation("Price Matching-Emailtemplate", PriceMStatusCodId_text)
    '        If (PriceMStatusCodId_text = "Initiated") Then
    '            _emailsubject = String.Concat(PriceMRequestCodTypeId_text, ": Request for ", PMBrand, "-", PMVehicle, " with ", PriceMCompetitorCodId_text, "-", CompetitorVehicle)
    '            _emailsubject = _emailsubject & String.Concat(" for Travel ", PMCheckout, " - ", PMCheckin)

    '            ''rev:mia July 11, 2013 - remove HirePeriod
    '            ''_emailsubject = _emailsubject & String.Concat(" Hire Period: ", PMHirePeriod, " THL Reference: ", PMRentalId)
    '            _emailsubject = _emailsubject & String.Concat(" THL Reference: ", PMRentalId)

    '        Else
    '            _emailsubject = ""
    '        End If


    '        sb.Append("<table>")
    '        sb.Append("<tr><td><b>THL INFORMATION</b></td></tr>")
    '        sb.AppendFormat("<tr><td>{0}</td><td><b>{1}</b></td></tr>", "REFERENCE:", PMRentalId)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Pick-Up Details:", PMCheckout)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Drop-Off Details:", PMCheckin)

    '        ''rev:mia July 11, 2013 - added PMHirePeriod
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Hire Period:", PMHirePeriod)

    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Brand:", PMBrand)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Vehicle:", PMVehicle & " - " & PMVehicleName)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Package:", PMPackage)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Original Rate:", FormatCurrency(PMBaseRate, 2))
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Discounted Rate:", FormatCurrency(PMDiscountPrice, 2))
    '        sb.Append("<tr><td></td></tr>")
    '        sb.Append("<tr><td></td></tr>")
    '        sb.Append("<tr><td><b>COMPETITOR INFORMATION</b></td></tr>")
    '        sb.Append("<tr><td></td></tr>")
    '        sb.Append("<tr><td></td></tr>")
    '        sb.AppendFormat("<tr><td>{0}</td><td><b>{1}</b></td></tr>", "REQUEST TYPE:", PriceMRequestCodTypeId_text.ToUpper)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Brand:", PriceMCompetitorCodId_text)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Vehicle:", CompetitorVehicle)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Quote Date:", CompetitorDateQuote)
    '        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", "Rate:", FormatCurrency(CompetitorPrice, 2))

    '        sb.Append("<tr><td></td></tr>")
    '        sb.Append("<tr><td></td></tr>")
    '        sb.AppendFormat("<tr><td>{0}</td><td><b>{1}</b></td></tr>", "NOTE:", NoteText)

    '        sb.Append("</table>")

    '    Catch ex As Exception
    '        Logging.LogError("PriceMatchingListener-EMAIL TEMPLATE", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
    '        _emailsubject = ""
    '    End Try
    '    Return sb.ToString
    'End Function

    Private Function GetDetailPrice(bpdid As String, _
                                ChgFromDate As String, _
                                ChgToDate As String, _
                                ChgFrom As String, _
                                ChgTo As String, _
                                Usercode As String, _
                                Optional BaseId As String = "") As DataSet


        Dim xmlstring As String = String.Empty
        Dim dsShowChargesDetails As New DataSet
        ChgFromDate = ChgFromDate.Replace("AM", "").Replace("PM", "")
        ChgToDate = ChgToDate.Replace("AM", "").Replace("PM", "")
        Try
            ''EXEC RES_GetChargeDetails 'FFFFFFFF-FFFF-0000-0000-000150837299', 'Jun 25 2013  9:00AM', 'Jun 30 2013  9:00AM', '', '', 'Booking.aspx', 'ma2', ''


            Dim summary As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc(SUMMARIES, bpdid, "", "").OuterXml
            Dim xmltemp As New XmlDocument
            summary = summary.Replace("<Details>", "")
            summary = summary.Replace("</Details>", "")
            xmltemp.LoadXml(summary)

            ''rates overlapping
            Dim GrossAmount As Decimal = 0
            Dim GetOriginalPrice As Decimal = 0
            Dim GetDiscountedPrice As Decimal = 0

            Dim nodes As XmlNodeList = xmltemp.SelectNodes("data/Detail")
            If (nodes.Count - 1 >= 1) Then
                For Each mynode As XmlNode In nodes
                    If (Convert.ToDouble(mynode("Rate").InnerText) <> 0 And Convert.ToDouble(mynode("TotOw").InnerText) <> 0) Then
                        ChgFromDate = String.Concat(Convert.ToDateTime(mynode("ChgFrDt").InnerText.ToString).ToString("MMM dd yyyy"), " ", mynode("FrTm").InnerText.ToString, "")
                        ChgToDate = String.Concat(Convert.ToDateTime(mynode("ChgToDt").InnerText.ToString).ToString("MMM dd yyyy"), " ", mynode("ToTm").InnerText.ToString, "")
                        xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc(DETAILS, bpdid, ChgFromDate, ChgToDate, ChgFrom, ChgTo, "Booking.aspx", Usercode, BaseId).OuterXml

                        dsShowChargesDetails.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

                        If (Not String.IsNullOrEmpty(mynode("AgnDis").InnerText) And Not String.IsNullOrEmpty(mynode("AgnDis").InnerText)) Then
                            GrossAmount = GrossAmount + Convert.ToDouble(mynode("AgnDis").InnerText) + Convert.ToDouble(mynode("TotOw").InnerText)
                        End If
                        dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetGrossAmt") = GrossAmount

                        If (Convert.ToDouble(mynode("Dis").InnerText) > 0) Then
                            If (Not String.IsNullOrEmpty(mynode("Dis").InnerText) And Not String.IsNullOrEmpty(mynode("Rate").InnerText)) Then
                                GetOriginalPrice = GetOriginalPrice + Math.Abs(Convert.ToDouble(mynode("Dis").InnerText)) + Convert.ToDouble(mynode("Rate").InnerText)
                            End If
                            dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetBaseRate") = GetOriginalPrice

                            If (Not String.IsNullOrEmpty(mynode("Rate").InnerText)) Then
                                GetDiscountedPrice = GetDiscountedPrice + Convert.ToDouble(mynode("Rate").InnerText)
                            End If
                            dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetRate") = GetDiscountedPrice
                        Else
                            GetOriginalPrice = GetOriginalPrice + Math.Abs(Convert.ToDouble(mynode("Dis").InnerText)) + Convert.ToDouble(mynode("Rate").InnerText)
                            dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetBaseRate") = GetOriginalPrice ''/ nodes.Count

                            GetDiscountedPrice = GetDiscountedPrice + Convert.ToDouble(mynode("Rate").InnerText)
                            dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetRate") = GetDiscountedPrice ''/ nodes.Count
                        End If

                    End If


                Next
            Else
                xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc(DETAILS, bpdid, ChgFromDate, ChgToDate, ChgFrom, ChgTo, "Booking.aspx", Usercode, BaseId).OuterXml
                dsShowChargesDetails.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
            End If


        Catch ex As Exception
            Logging.LogError("GetPriceDetails ERROR", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
        End Try

        Return dsShowChargesDetails
    End Function

#End Region

End Class
