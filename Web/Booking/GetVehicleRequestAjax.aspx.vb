Imports System.xml
Imports System.Data

Partial Class Booking_GetVehicleRequestAjax
    Inherits System.Web.UI.Page

    Private Const RES As String = "Reservation"
    Private agentid As String = ""

    Private Function GetAgentID(ByVal contactName As String) As String
        Dim id As String = CType(Session("BookingRequestUserID"), String)
        Dim contact As String
        If (contactName.Contains("-") = True AndAlso contactName <> "") Then
            contact = contactName.Substring(0, contactName.IndexOf("-"))
        Else
            If contactName <> "" Then
                contact = contactName
            Else
                Return String.Empty
            End If

        End If

        'JL 2008/06/17
        'The part of the agentid value got trim

        'agentid = Aurora.Common.Data.ExecuteScalarSP("GEN_GetPopUpData", "AGENT", contact, "", "", "", "", id)
        'Dim xmlstring As String = "<AgentID>" & agentid & "</AgentID>"
        'Dim xmldoc As New XmlDocument
        'xmldoc.LoadXml(xmlstring)
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "AGENT", contact, "", "", "", "", id)

        Try
            agentid = xmldoc.GetElementsByTagName("ID").Item(0).InnerText
        Catch ex As Exception
            Return ""
        End Try

        ''Return xmldoc.GetElementsByTagName("ID").Item(0).InnerText
        Return agentid

    End Function

    Private Function GetContactInformation(ByVal agentid As String) As String
        Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("RES_getContact", "AGENT", agentid, RES, "")
        If xmlstring = "" Then Return ""

        'JL 2008/06/17
        'Dont need to convert to xmlDoc
        Return xmlstring & "<AgentID>" & agentid & "</AgentID>"
        'Dim xmldoc As New XmlDocument
        'xmldoc.LoadXml(xmlstring)
        'Return xmldoc.OuterXml
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mode As String = Request.QueryString("mode")
        Dim returnvalue As String = Nothing
        Dim param As String

        If mode.Equals("getcontact") Then
            ''get what type of id is passed
            param = Request.QueryString("paramContactID")
            If param <> "" Then
                returnvalue = GetContactInformation(param)
            Else
                param = Request.QueryString("paramContactName")
                If param <> "" Then
                    returnvalue = GetContactInformation(GetAgentID(param))
                End If
            End If

            '' returnvalue = "<Contacts>" & returnvalue & "</Contacts>"
            returnvalue = String.Concat("<Contacts>", returnvalue, "</Contacts>", "<AddAgentID>", agentid, "</AddAgentID>")

        ElseIf mode.Equals("getdatehired") Then

            param = Request.QueryString("paramdatehired")
            If param <> "" Then
                Dim ary() As String = param.Split("|")
                Dim cko As String = ary(0)
                Dim ckoampm As String = ary(1)
                Dim cki As String = ary(2)
                Dim ckiampm As String = ary(3)
                Dim hireddays As String = ary(4)
                Dim daysmode As String = ary(5)
                returnvalue = CalculateRequestPeriod(cko, ckoampm, cki, ckiampm, hireddays, daysmode)
            End If

        ElseIf mode.Equals("getproductid") Then
            param = Request.QueryString("paramProductid")
            Dim paramuserid As String = Request.QueryString("uid")
            returnvalue = GetProductID(param, paramuserid)

        ElseIf mode.Equals("GetAvailableTimeCKO") Then

            param = Request.QueryString("paramcko")
            Dim paramuserid As String = Request.QueryString("uid")
            Dim paramcontroltype As String = Request.QueryString("controltype")

            returnvalue = Me.SelectLocationBasedOnCodeAndUser(param, paramuserid, "1")

            ''Dim paramDate As String = Request.QueryString("traveldate")
            ''returnvalue = Me.SelectLocationBasedOnCodeAndUserV2(param, paramDate, "1", True) ''rev:mia nov18

        ElseIf mode.Equals("GetAvailableTimeCKI") Then

            param = Request.QueryString("paramcki")
            Dim paramuserid As String = Request.QueryString("uid")
            Dim paramcontroltype As String = Request.QueryString("controltype")

            returnvalue = Me.SelectLocationBasedOnCodeAndUser(param, paramuserid, "2")

            ''Dim paramDate As String = Request.QueryString("traveldate")
            ''returnvalue = Me.SelectLocationBasedOnCodeAndUserV2(param, paramDate, "2", False) ''rev:mia nov18

        ElseIf mode.Equals("getAgentID") Then

            param = Request.QueryString("paramagentid")
            Dim temp As String = GetContactInformation(param)
            returnvalue = "<AgentID>" & temp & "</AgentID>"


        ElseIf mode.Equals("GetAvailableTimeCKOv2") Then ''reV:mia nov18
            param = Request.QueryString("paramcko")
            Dim paramcontroltype As String = Request.QueryString("controltype")
            Dim paramDate As String = Request.QueryString("traveldate")
            returnvalue = Me.SelectLocationBasedOnCodeAndUserV2(param, paramDate, "1", True) ''rev:mia nov18
        ElseIf mode.Equals("GetAvailableTimeCKIv2") Then ''reV:mia nov18

            param = Request.QueryString("paramcki")
            Dim paramcontroltype As String = Request.QueryString("controltype")
            Dim paramDate As String = Request.QueryString("traveldate")
            returnvalue = Me.SelectLocationBasedOnCodeAndUserV2(param, paramDate, "2", False) ''rev:mia nov18

        End If



        Response.Clear()
        Response.ContentType = "text/xml"
        Response.Write(returnvalue)
        Response.End()

    End Sub

    Function GetProductID(ByVal productcode As String, ByVal paramuserid As String) As String

        Dim splitProductCode As String()
        If productcode.Contains("-") Then
            splitProductCode = productcode.Split("-")
            productcode = splitProductCode(0)
        End If
        Return Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", _
                                                  productcode, _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  paramuserid)
    End Function

    Function CalculateRequestPeriod(ByVal cko As String, ByVal ckoampm As String, ByVal cki As String, ByVal ckiampm As String, ByVal hireperiod As String, ByVal daysmode As String) As String

        Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                   cko, _
                                                   ckoampm, _
                                                   cki, _
                                                   ckiampm, _
                                                   hireperiod, _
                                                   daysmode, _
                                                   0)
        xmlstring = xmlstring.Replace("<Root>", "<XMLDate><Root>")
        xmlstring = xmlstring.Replace("</Root>", "</Root></XMLDate>")
        Return xmlstring

    End Function

    Private Function SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, ByVal usercode As String, ByVal controltype As String) As String
        Try
            Dim sb As New StringBuilder
            Dim controlxml As String = "<mode>" & controltype & "</mode>"
            
            If String.IsNullOrEmpty(locationCode) Then
                Return "<root>" & controlxml & "</root>"
            End If

            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If

            sb.Append("<root>")
            sb.Append(controlxml)

            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, usercode)
            If ds.Tables(0).Rows.Count = 0 Then
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", "AM")
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", "PM")
                sb.Append("</root>")
                Return sb.ToString
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)(0).ToString
            Dim endtime As String = ds.Tables(0).Rows(0)(1).ToString

            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If

            ' fix by shoel to break out of blackhole loops :)
            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":"c)(0)), CInt(endtime.Split(":"c)(1)), CInt(endtime.Split(":"c)(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":"c)(0)), CInt(startTime.Split(":"c)(1)), CInt(startTime.Split(":"c)(2)))

            While tmTempTimeValue < tmEndTime
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", tmTempTimeValue.Hours.ToString().PadLeft(2, "0"c) & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"c))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While

            ' when tmTempTimeValue = tmEndTime
            sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", tmEndTime.Hours.ToString().PadLeft(2, "0"c) & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"c))

            ' fix by shoel to break out of blackhole loops :)
            sb.Append("</root>")
            Return sb.ToString


        Catch ex As Exception
            ''Me.SetErrorMessage("SelectLocationBasedOnCodeAndUser -> " & ex.Message)
        End Try
    End Function

    Function ParsetimeToString(ByVal starttime As DateTime) As String
        Dim strTime As String = (Trim(starttime.ToString.Remove(0, starttime.ToString.IndexOf(" ") + 1)))
        Dim index As Integer = strTime.LastIndexOf(":")
        strTime = strTime.Remove(index, 3)
        If strTime.Contains("a.m") Then
            strTime = strTime.Replace("a.m.", "AM")
        Else
            strTime = strTime.Replace("p.m.", "PM")
        End If
        Return strTime
    End Function


#Region "Code changes after release!!!"

    Private Function SelectLocationBasedOnCodeAndUserV2(ByVal locationCode As String, _
                                                        ByVal traveldate As String, _
                                                        ByVal controltype As String, _
                                                        ByVal isCKO As Boolean) As String
        Try
            Dim sb As New StringBuilder
            Dim controlxml As String = "<mode>" & controltype & "</mode>"

            If String.IsNullOrEmpty(locationCode) Then
                Return "<root>" & controlxml & "</root>"
            End If

            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If

            sb.Append("<root>")
            sb.Append(controlxml)


            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationStartFinishTime", dsTemp, locationCode, traveldate, IIf(isCKO = True, 0, 1))
            If ds.Tables(0).Rows.Count = 0 Then
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", "AM")
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", "PM")
                sb.Append("</root>")
                Return sb.ToString
            End If

            Dim startTime As String = ds.Tables(0).Rows(0)("StartTime").ToString
            Dim endtime As String = ds.Tables(0).Rows(0)("FinishTime").ToString

            Dim locationinterval As Integer = CInt(ConfigurationManager.AppSettings("TIMEINTERVAL"))

            If (locationinterval <> 15 AndAlso locationinterval <> 30) Then
                locationinterval = 15
            End If


            Dim tmEndTime, tmTempTimeValue As TimeSpan
            tmEndTime = New TimeSpan(CInt(endtime.Split(":"c)(0)), CInt(endtime.Split(":"c)(1)), CInt(endtime.Split(":"c)(2)))
            tmTempTimeValue = New TimeSpan(CInt(startTime.Split(":"c)(0)), CInt(startTime.Split(":"c)(1)), CInt(startTime.Split(":"c)(2)))

            While tmTempTimeValue < tmEndTime
                sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", tmTempTimeValue.Hours.ToString().PadLeft(2, "0"c) & ":" & tmTempTimeValue.Minutes.ToString().PadLeft(2, "0"c))
                tmTempTimeValue = tmTempTimeValue.Add(New TimeSpan(0, locationinterval, 0))
            End While


            sb.AppendFormat("<ckocki><data>{0}</data></ckocki>", tmEndTime.Hours.ToString().PadLeft(2, "0"c) & ":" & tmEndTime.Minutes.ToString().PadLeft(2, "0"c))


            sb.Append("</root>")
            Return sb.ToString


        Catch ex As Exception

        End Try
    End Function
#End Region

End Class
