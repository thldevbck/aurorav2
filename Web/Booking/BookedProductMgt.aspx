<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookedProductMgt.aspx.vb" Inherits="Booking_BookedProductMgt" Title="Untitled Page" %>

<%@ Register Src="../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc5" %>

<%@ Register Src="../UserControls/ConfirmationBoxExtended/ConfirmationBoxControlExtended.ascx"
    TagName="ConfirmationBoxControlExtended" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc4" %>
    
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc2" %>
   
<%--rev:mia 19jan2015-addition of supervisor override password--%>
<%@ Register Src="~/UserControls/SlotAvailableControl/SlotAvailableControl.ascx" TagName="SlotAvailableControl"   TagPrefix="uc6" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
 <script type="text/javascript">
 
     // Mod: Raj - 18 May, 2011

    function clientValidate() 
    {
   
         var boolValidate = true;

         var productPickerControl;
         productPickerControl = $get('<%= productPickerControl.ClientID %>');

         if (!validateAControl(productPickerControl)) {
             productPickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             productPickerControl.style.background = "white";
         }


         var locationFromPickerControl;
         locationFromPickerControl = $get('<%= locationFromPickerControl.ClientID %>');

         if (!validateAControl(locationFromPickerControl)) {
             locationFromPickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             locationFromPickerControl.style.background = "white";
         }

         var travelFromDateControl;
         travelFromDateControl = $get('<%= travelFromDateControl.ClientID %>');

         if (!validateAControl(travelFromDateControl)) {
             travelFromDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             travelFromDateControl.style.background = "white";
         }

         var travelFromTimeControl;
         travelFromTimeControl = $get('<%= travelFromTimeControl.ClientID %>');

         if (!validateAControl(travelFromTimeControl)) {
             travelFromTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             travelFromTimeControl.style.background = "white";
         }

         var chargedFromDateControl;
         chargedFromDateControl = $get('<%= chargedFromDateControl.ClientID %>');

         if (!validateAControl(chargedFromDateControl)) {
             chargedFromDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             chargedFromDateControl.style.background = "white";
         }

         var chargedFromTimeControl;
         chargedFromTimeControl = $get('<%= chargedFromTimeControl.ClientID %>');

         if (!validateAControl(chargedFromTimeControl)) {
             chargedFromTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             chargedFromTimeControl.style.background = "white";
         }

        //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

         var locationToPickerControl;
         locationToPickerControl = $get('<%= locationToPickerControl.ClientID %>');

         if (!validateAControl(locationToPickerControl)) {
             locationToPickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             locationToPickerControl.style.background = "white";
         }

         var travelToDateControl;
         travelToDateControl = $get('<%= travelToDateControl.ClientID %>');

         if (!validateAControl(travelToDateControl)) {
             travelToDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             travelToDateControl.style.background = "white";
         }

         var travelToTimeControl;
         travelToTimeControl = $get('<%= travelToTimeControl.ClientID %>');

         if (!validateAControl(travelToTimeControl)) {
             travelToTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             travelToTimeControl.style.background = "white";
         }

         var chargedToDateControl;
         chargedToDateControl = $get('<%= chargedToDateControl.ClientID %>');

         if (!validateAControl(chargedToDateControl)) {
             chargedToDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             chargedToDateControl.style.background = "white";
         }

         var chargedToTimeControl;
         chargedToTimeControl = $get('<%= chargedToTimeControl.ClientID %>');

         if (!validateAControl(chargedToTimeControl)) {
             chargedToTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;
         }
         else {
             chargedToTimeControl.style.background = "white";
         }

         //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


         if (boolValidate == false) {
             //alert('invalid');
             return false;
         }
         return true;
     }

     function validateAControl(obj) {

         if (obj.value == "") {
             //setWarningShortMessage(getErrorMessage("GEN005"));
             return false
         }
         else {
             return true
         }
     }
</script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">

    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();  //the control causing the postback
        
        if(pbControl.id.indexOf('saveButton') > -1 || pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('centerButton') > -1 || pbControl.id.indexOf('rightButton') > -1 || pbControl.id.indexOf('ok') > -1)
        {   
            //alert(pbControl.id)
            pbControl.disabled = true;
        }

        //rev:mia: Sept 7 2012 - added enabling of save button to buy sometime when doing postback
        if (pbControl.id.indexOf('dateTextBox') > -1 || pbControl.id.indexOf('timeTextBox'))
         {
             $get('ctl00_ContentPlaceHolder_saveButton').disabled = true;
             $get('ctl00_ContentPlaceHolder_RecalculateChargesBtn').disabled = true;
             $get('ctl00_ContentPlaceHolder_backButton').disabled = true;
                 
        }
    }

    function EndRequestHandler(sender, args) {
        if(pbControl.id.indexOf('saveButton') > -1 || pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('centerButton') > -1 || pbControl.id.indexOf('rightButton') > -1 || pbControl.id.indexOf('ok') > -1)
        {
            pbControl.disabled = false;
        }

        //rev:mia: Sept 7 2012 - added enabling of save button to buy sometime when doing postback
        if (pbControl.id.indexOf('dateTextBox') > -1 || pbControl.id.indexOf('timeTextBox')) {
            $get('ctl00_ContentPlaceHolder_saveButton').disabled = false;
            $get('ctl00_ContentPlaceHolder_RecalculateChargesBtn').disabled = false;
            $get('ctl00_ContentPlaceHolder_backButton').disabled = false;
        }

        pbControl = null;
    }
    
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="tabContentUpdatePanel" runat="server">
        <contenttemplate>

<asp:Panel id="panel1" runat="server">

    <table width="100%">
        <tr>
            <td width="6%">
                Agent:</td>
            <td width="45%">    
                <asp:TextBox ID="agentTextBox" runat="server" ReadOnly="true" Width="330">
                </asp:TextBox>
            </td>
            <td width="6%">
                Hirer:</td>
            <td width="30%">
                <asp:TextBox ID="hirerTextBox" runat="server" ReadOnly="true" Width="200">
                </asp:TextBox>
            </td>
            <td width="6%">
                Status:</td>
            <td>
                <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="50">
                </asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="rentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
        CssClass="dataTable">
        <alternatingrowstyle cssclass="oddRow" />
        <rowstyle cssclass="evenRow" />
        <columns>
                <asp:BoundField DataField="RentalNum" HeaderText="Rental" ReadOnly="True" />
               <%-- <asp:BoundField DataField="Check-Out" HeaderText="Check-out" ReadOnly="True" />
                <asp:BoundField DataField="Check-In" HeaderText="Check-in" ReadOnly="True" />--%>
                <asp:TemplateField HeaderText="Check-Out" >
                        <ItemTemplate>
                            <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) + GetArrivalDepartureTime(eval("RntArrivalAtBranchDateTime")) %>'></asp:Label>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Check-In" >
                        <ItemTemplate>
                            <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) + GetArrivalDepartureTime(eval("RntDropOffAtBranchDateTime")) %>'></asp:Label>
                        </ItemTemplate>
                 </asp:TemplateField>
                 
                <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
            </columns>
    </asp:GridView>
    <br />
    <table width="100%">
        <tr>
            <td width="150">
                Product:
            </td>
            <td width="250">
                <uc1:PickerControl ID="productPickerControl" runat="server" AppendDescription="true"
                    PopupType="PRODUCT4BPD" Width="200" Nullable="false" />
                &nbsp;*
            </td>
            <td width="150">
                Unit:
            </td>
            <td>
                <asp:TextBox id="unitNumTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Rego:
            </td>
            <td width="250">
                <asp:TextBox id="regoTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
            <td width="150">
                Serial:
            </td>
            <td>
                <asp:TextBox id="serialTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="150">
                Initial Status:
            </td>
            <td width="250">
                <asp:TextBox id="initialStatusTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
            <td width="150">
                Current:
            </td>
            <td>
                <asp:DropDownList id="currentDropDownList" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td width="100%" align="right">
               <asp:Button runat="server" Text="Validation Booking" id="ValidationBookingBtn" class="Button_Standard" visible="false" />
            </td>
            <td>
                 <asp:Button runat="server" Text="Recalculate Charges" id="RecalculateChargesBtn" class="Button_Standard" />
            </td>
            <td>
                <asp:Button runat="server" Text="Recalculate GST" ID="RecalculateGSTBtn" class="Button_Standard" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel id="checkPanel" runat="server">
        <table width="100%" class="dataTable">
            <thead>
                <tr>
                    <th width="150">
                    </th>
                    <th id="locationTh" runat="server" >
                        Location*</th>
                    <th id="travelTh" runat="server" >
                        Travel*</th>
                    <th id="chargedTh" runat="server" >
                        Charged*</th>
                    <th id="odoTh" runat="server" >
                        Odo*</th>
                </tr>
            </thead>
            <tr>
                <td>
                    From
                </td>
                <td id="frLocationTd" runat="server" >
                <%--PopupType="LocForBPD"--%>
                    <uc1:PickerControl ID="locationFromPickerControl" runat="server" AppendDescription="true"
                        PopupType="GETVALIDLOCATION" Nullable="false" Width="150" />
                </td>
                <td id="frTravelTd" runat="server" >
                    <uc2:DateControl ID="travelFromDateControl" runat="server" AutoPostBack="true" ></uc2:DateControl>
                    <uc2:TimeControl ID="travelFromTimeControl" runat="server" AutoPostBack="true" ></uc2:TimeControl>
                    <asp:TextBox ID="frTextBox" runat="server">
                    </asp:TextBox>
                </td>
                <td id="frChargedTd" runat="server" >
                    <uc2:DateControl ID="chargedFromDateControl" runat="server"></uc2:DateControl>
                    <uc2:TimeControl ID="chargedFromTimeControl" runat="server"></uc2:TimeControl>
                </td>
                <td id="frOdoTd" runat="server" >
                    <asp:TextBox ID="odoFromTextBox" runat="server" Width="90" onkeypress="keyStrokeDecimal();"
                        style="text-align: right">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    To
                </td>
                <td id="toLocationTd" runat="server" >
                <%--PopupType="LocForBPD"--%>
                    <uc1:PickerControl ID="locationToPickerControl" runat="server" AppendDescription="true"
                        PopupType="GETVALIDLOCATION" Nullable="false" Width="150" />
                </td>
                <td id="toTravelTd" runat="server" >
                    <uc2:DateControl ID="travelToDateControl" runat="server" AutoPostBack="true" ></uc2:DateControl>
                    <uc2:TimeControl ID="travelToTimeControl" runat="server" AutoPostBack="true" ></uc2:TimeControl>
                    <asp:TextBox ID="toTextBox" runat="server">
                    </asp:TextBox>
                </td>
                <td id="toChargedTd" runat="server" >
                    <uc2:DateControl ID="chargedToDateControl" runat="server"></uc2:DateControl>
                    <uc2:TimeControl ID="chargedToTimeControl" runat="server"></uc2:TimeControl>
                </td>
                <td id="toOdoTd" runat="server" >
                    <asp:TextBox ID="odoToTextBox" runat="server" Width="90" onkeypress="keyStrokeDecimal();"
                        style="text-align: right">
                    </asp:TextBox>
                </td>
            </tr>
        </table>
    
    <table width="100%">
        <tr>
            <td width="150">
                Period:
            </td>
            <td width="250">
                <asp:TextBox id="periodTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
            <td width="150">
            </td>
            <td>
                <asp:TextBox id="currOrgTextBox" runat="server" readonly="true" width="217">
                </asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    </asp:Panel>
    
    <table width="100%">
        <tr>
            <td width="150">
                Charge To:
            </td>
            <td width="250">
                <asp:DropDownList id="chargeToDropDownList" runat="server">
                    <asp:ListItem Text="Agent" Value="0">
                    </asp:ListItem>
                    <asp:ListItem Text="Customer" Value="1">
                    </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td width="150">
                Transferred:
            </td>
            <td>
                <asp:TextBox id="transferredTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label id="bookingRefLabel" runat="server" text="Booking Ref:"></asp:Label></td>
            <td>
                <asp:TextBox id="bookingRefTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox id="focCheckBox" runat="server" text="FOC">
                </asp:CheckBox>
            </td>
            <td width="250">
                <asp:CheckBox id="applyOrigRatesCheckBox" runat="server" text="Apply Orig Rates"
                    enabled="false">
                </asp:CheckBox>
            </td>
            <td width="150">
                Cancelled:
            </td>
            <td>
                <asp:TextBox id="cancelledTextBox" runat="server" readonly="true">
                </asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" class="dataTable">
        <thead>
            <tr>
                <th width="15%">
                    From</th>
                <th width="15%">
                    To</th>
                <th width="9%">
                    Booked</th>
                <th width="9%">
                    Rate</th>
                <th width="8%">
                    Qty/UOM</th>
                <th width="7%">
                    Curr</th>
                <th width="9%">
                    Discounts</th>
                <th width="14%">
                    Agent Discount</th>
                <th width="10%">
                    Total Owing</th>
                <th width="4%">
                    FOC?</th>
            </tr>
        </thead>
        <tr>
            <td>
                <asp:Label id="fromLabel" runat="server" Text=""></asp:Label></td>
            <td>
                <asp:Label id="toLabel" runat="server" Text=""></asp:Label></td>
            <td>
                <asp:Label id="bookedLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="rateLabel" runat="server" Text=""></asp:Label> &nbsp;</td>
            <td>
                <asp:Label id="qtyLabel" runat="server" Text=""></asp:Label></td>
            <td>
                <asp:Label id="currLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="discountsLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="agentDiscountLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="totalOwingLabel" runat="server" Text=""></asp:Label></td>
            <td>
                <asp:CheckBox id="tFocCheckBox" runat="server" enabled="false">
                </asp:CheckBox></td>
        </tr>
        <tr class="oddRow">
            <td></td>
            <td><b>Totals</b></td>
            <td><b>GST</b></td>
            <td align=right >
                <asp:Label id="tGstLabel" runat="server" Text=""  style="text-align: right" ></asp:Label> &nbsp;</td>
            <td></td>
            <td>
                <asp:Label id="tCurrLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="tDiscountsLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="tAgentDiscountLabel" runat="server" Text=""></asp:Label></td>
            <td align=right >
                <asp:Label id="tTotalOwingLabel" runat="server" Text=""></asp:Label></td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td width="100%" align="right">
                <asp:Button id="saveButton" text="Save" runat="server" class="Button_Standard Button_Save"  OnClientClick="return clientValidate();" />
                <asp:Button id="backButton" text="Back" runat="server" class="Button_Standard Button_Back" />
                <asp:Button id="selectProductButton" text="Select Product" runat="server" class="Button_Standard"  visible="false"/>
            </td>
        </tr>
    </table>
    

</asp:Panel>
    

  </contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="pnlUserControl" runat="server">
        <ContentTemplate>
            <uc3:ConfirmationBoxControlExtended id="ConfirmationBoxControlExtended1" runat="server" />

            <uc3:ConfirmationBoxControlExtended id="ConfirmationBoxControlExtendedGST" runat="server" 
            CenterButton_AutoPostBack="true" CenterButton_Text="Charge - 12.5%" CenterButton_Visible="true" 
            LeftButton_AutoPostBack="true" LeftButton_Text="Charge - 15%" LeftButton_Visible="true" 
            RightButton_AutoPostBack="false" RightButton_Text="Cancel" RightButton_Visible="true" 
            Title="Recalculate GST" Text="Recalculate GST using either 15% or 12.5% as the rate"
            />

            <uc4:ConfirmationBoxControl id="ConfirmationBoxControl1" runat="server"/>
            <uc5:SupervisorOverrideControl ID="SupervisorOverrideControl1" runat="server" />

            <%--rev:mia 15jan2015-addition of supervisor override password--%>
            <uc6:SlotAvailableControl runat="server" ID="SlotAvailableControl" />
        </ContentTemplate>
    </asp:UpdatePanel>
   
</asp:Content>
