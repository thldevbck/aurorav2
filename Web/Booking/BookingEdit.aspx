<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookingEdit.aspx.vb" Inherits="Booking_BookingEdit" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="AuroraControls" TagPrefix="SearchAgent" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/ShowBookingRequestAgentPopupUserControl.ascx"
    TagName="ShowBookingRequestAgentPopupUserControl" TagPrefix="uc2" %>
<%@ Register Src="~/Booking/Controls/BookingRequestAgentMgtPopupUserControl.ascx"
    TagName="BookingRequestAgentMgtPopupUserControl" TagPrefix="uc3" %>

<%--rev:mia 15jan2015-addition of supervisor override password--%>
<%@ Register Src="~/UserControls/SlotAvailableControl/SlotAvailableControl.ascx" TagName="SlotAvailableControl"   TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript">

var errorMessage = <%= MessageJSON %>;
    
function ClientValidation()
{ 
    var pickerTextBox;
    pickerTextBox = document.getElementById('<%=agentPickerControl.FindControl( "pickerTextBox").ClientID%>');
    if (pickerTextBox.value == "")
    {
        setWarningShortMessage(getErrorMessage("GEN005"))
        return false
    }

    var surnameTextBox;
    surnameTextBox = $get('<%= surnameTextBox.ClientID %>');
    if (surnameTextBox.value == "")
    {
        setWarningShortMessage(getErrorMessage("GEN005"))
        return false
    }
} 

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <b>Agent Details</b>
        <table width="100%">
            <tr width="100%">
                <td width="150">
                    Agent:
                </td>
                <td width="350">
                    <asp:UpdatePanel ID="updatePanel3" runat="server">
                        <contenttemplate>
                        <uc1:PickerControl ID="agentPickerControl" runat="server" PopupType="AGENT" AppendDescription="true" Width="300"
                            Nullable=false  />&nbsp;*
                    </contenttemplate>
                    </asp:UpdatePanel>
                </td>
                <td align=right >
                   <%-- <asp:LinkButton ID="updateAgentLinkButton" runat="server">Update Agent</asp:LinkButton>--%>
                    <asp:Button ID="updateAgentLinkButton" runat="server" Text="Edit Agent" CssClass="Button_Standard Button_Edit" />               
                   <%-- <asp:LinkButton ID="addAgentLinkButton" runat="server">Add Agent</asp:LinkButton>--%>
                    <asp:Button ID="addAgentLinkButton" runat="server" Text="Add Agent" CssClass="Button_Standard Button_Add" />
                    
                </td>
            </tr>
        </table>
        <br />
        <b>Hirer Details</b>
        <table width="100%">
            <tr width="100%">
                <td width="150">
                    Surname:
                </td>
                <td width="250">
                    <asp:TextBox ID="surnameTextBox" runat="server" MaxLength="128"></asp:TextBox>&nbsp;*</td>
                <td width="150">
                    First Name:</td>
                <td>
                    <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="128"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Title:</td>
                <td>
                    <%--<asp:TextBox ID="titleTextBox" runat="server" MaxLength="10"></asp:TextBox>--%>
                    <asp:DropDownList ID="customertitleDropdown" runat="server" Width="50px">
                                        </asp:DropDownList>
                    </td>
            </tr>
        </table>
        <br />
        <b>Booking Details</b>
        <table width="100%">
            <tr width="100%">
                <td width="150">
                    Status:</td>
                <td width="250">
                    <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="200"></asp:TextBox></td>
                <td width="150">
                    Check-Out:</td>
                <td>
                    <asp:TextBox ID="checkOutTextBox" runat="server" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Check-In:</td>
                <td>
                    <asp:TextBox ID="checkInTextBox" runat="server" ReadOnly="true"></asp:TextBox></td>
                <td>
                    Transferred:</td>
                <td>
                    <asp:TextBox ID="transferredTextBox" runat="server" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Referral Type:
                </td>
                <td>
                    <asp:DropDownList ID="referralTypeDropDownList" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </table>
        <br />
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        OnClientClick="return ClientValidation();" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
        <br />
        <asp:UpdatePanel ID="updatePanel1" runat="server">
            <contenttemplate>
            <uc2:ShowBookingRequestAgentPopupUserControl id="showBookingRequestAgentPopupUserControl1" runat=server></uc2:ShowBookingRequestAgentPopupUserControl>
    </contenttemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updatePanel2" runat="server">
            <contenttemplate>
            <uc3:BookingRequestAgentMgtPopupUserControl id="bookingRequestAgentMgtPopupUserControl1" runat=server></uc3:BookingRequestAgentMgtPopupUserControl>

        <%--rev:mia 15jan2015-addition of supervisor override password--%>
        <uc6:SlotAvailableControl runat="server" ID="SlotAvailableControl" />

        </contenttemplate>
        </asp:UpdatePanel>

       
    </asp:Panel>
</asp:Content>
