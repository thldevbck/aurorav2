<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookingSearch.aspx.vb" Inherits="Booking_BookingSearch" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

<script language=javascript>

function onPageSearch(searchParam)
{
    if (searchParam != '')
    {
        GoToBooking()
    }
    else
    {
        if(confirm("Are you sure you want to start a new search?"))
        { 
            //document.forms[0].submit(); 
            document.forms[0].action="./BookingSearch.aspx?sTxtSearch=new"
			document.forms[0].method="POST"
            document.forms[0].submit(); 
            
        }
    }

}

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
<asp:Panel id="panel11" runat=server Width="100%">

    <table cellspacing="5" width="100%" border="0">
        <tr width="100%">
            <td width="150">
                Hirer Surname:</td>
            <td width="250" colspan="2">
                <asp:TextBox ID="hirerSurnameTextBox" runat="server" MaxLength="128"></asp:TextBox></td>
            <td width="150">
                Country (of Travel):</td>
            <td>
                <asp:DropDownList ID="countryDropDownList" runat="server" Width="200px">
                    <%--<asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="AU - Australia" Value="AU"></asp:ListItem>
                    <asp:ListItem Text="NZ - New Zealand" Value="NZ"></asp:ListItem>--%>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td width="150">
            </td>
            <td width="125">
                From</td>
            <td width="125">
                To</td><td></td><td></td>
        </tr>
        <tr>
            <td width="150">
                Check-Out Date:
                <asp:CustomValidator ID="customValidator" runat="server"
                    OnServerValidate="customValidator_OnServerValidate" ErrorMessage=""></asp:CustomValidator>
            </td>
            <td width="125">
                <uc2:DateControl ID="pickUpFromDateControl" runat="server" />
            </td>
            <td width="125">
                <uc2:DateControl ID="pickUpToDateControl" runat="server" />
            </td>
            <td width="150">
                Check-Out Location:</td>
            <td>
                <uc1:PickerControl ID="pickUpLocPickerControl" runat="server" PopupType="LOCATION" AppendDescription="true" width="200" />
                </td>
        </tr>
        <tr>
            <td>
                Check-In Date:
            </td>
            <td>
                <uc2:DateControl ID="dropOffFromDateControl" runat="server" />
            </td>
            <td>
                <uc2:DateControl ID="dropOffToDateControl" runat="server" />
            </td>
            <td>
                Check-In Location:</td>
            <td>
                <uc1:PickerControl ID="dropOffLocPickerControl" runat="server" PopupType="LOCATION" AppendDescription="true" width="200"  />
            </td>
        </tr>
        <tr>
            <td>
                Booked Date:
            </td>
            <td>
                    <uc2:DateControl ID="bookedFromDateControl" runat="server"  />
            </td>
            <td>
                    <uc2:DateControl ID="bookedToDateControl" runat="server" />
            </td>
            <td>
                Unit #:</td>
            <td>
                <asp:TextBox ID="unitTextBox" runat="server" Width="30%" MaxLength="64"></asp:TextBox>
                Rego #:
                <asp:TextBox ID="regoTextBox" runat="server" Width="30%" MaxLength="64"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
            <td >
                In Error?:</td>
            <td>
                <asp:DropDownList ID="errorDropDownList" runat="server">
                    <asp:ListItem Text="All" Value=""></asp:ListItem>
                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                </asp:DropDownList></td>
        </tr>
    </table>

    <br />
    <uc3:CollapsiblePanel ID="detailsCollapsiblePanel" runat="server" Title="Additional Search Criteria"
        TargetControlID="detailsContentPanel" />
  
    <asp:Panel ID="detailsContentPanel" runat="server" Width="100%">
        <table cellspacing="4" width="100%">
            <tr width="100%">
                <td colspan="2" width="400">
                    <b>Booking Details</b></td>
                <td colspan="2">
                    <b>Vehicle</b></td>
            </tr>
            <tr width="100%">
                <td width="150">
                    Booking Status:</td>
                <td width="250">
                    <asp:DropDownList ID="bookingStatusDropDownList" runat="server" AppendDataBoundItems="true" >
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList></td>
                <td width="150">
                    Brand:</td>
                <td>
                    <asp:DropDownList ID="brandDropDownList" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value="" ></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    Rental Status:</td>
                <td >
                    <asp:DropDownList ID="rentalStatusDropDownList" runat="server" AppendDataBoundItems="true"  >
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                    Type/Class:</td>
                <td>
                    <asp:DropDownList ID="typeDropDownList" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    Package:</td>
                <td >
                    <uc1:PickerControl ID="packagePickerControl" runat="server" PopupType="PACKAGE" width="150"  />
                    </td>
                <td>
                    Old Booking No.:</td>
                <td>
                    <asp:TextBox ID="oldBookingNoTextBox" runat="server" Width="80" MaxLength="64"></asp:TextBox>
                    &nbsp;/ Rental No.:&nbsp;&nbsp;<asp:TextBox ID="oldRentalNoTextBox" runat="server" Width="50" MaxLength="20"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Booked Product:</td>
                <td colspan=3>
                    <uc1:PickerControl ID="bookedProductPickerControl" runat="server" 
                        PopupType="Product4BookingSearch" AppendDescription="true" width="500"  />
                    </td>
            </tr>
            <tr>
                <td >
                    Vehicle of Travel:</td>
                <td colspan=3>
                    <uc1:PickerControl ID="vehiclePickerControl" runat="server" PopupType="FleetModel4BookingSearch" 
                        AppendDescription="true" width="500"  />
                    </td>
            </tr>
        <%--</table>--%>
        <tr><td><br /></td></tr>
        <%--<table cellspacing="4" width="100%" border="0">--%>
            <tr width="100%">
                <td colspan="2" >
                    <b>Hirer</b></td>
                <td colspan="2" >
                    <b>Agent</b></td>
            </tr>
            <tr width="100%">
                <td width="150">
                    Address:</td>
                <td width="250">
                    <asp:TextBox ID="addressTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
                <td width="150">
                    Agent:</td>
                <td >
                    <uc1:PickerControl ID="agentPickerControl" runat="server" PopupType="BOOKINGAGN" width="200"  /></td>
            </tr>
            <tr>
                <td> 
                    State:</td>
                <td>
                    <asp:TextBox ID="stateTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
                <td>
                    Agent Reference:</td>
                <td>
                    <asp:TextBox ID="agentReferenceTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
            </tr>
            
            <%--rev:mia july 4 2012 - start-- addition of 3rd party field--%>
            <tr>
                <td></td>
                <td></td>

                <td>
                    3rd Party Reference:
                </td>
                <td>
                    <asp:TextBox ID="txtthirdParty"  runat="server" Width="140" MaxLength="64" />
                </td>
            </tr>
            <%--rev:mia july 4 2012 - end-- addition of 3rd party field--%>

            <tr>
                <td>
                    Town/City:</td>
                <td>
                    <asp:TextBox ID="cityTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
                <td>
                    Voucher Number:</td>
                <td>
                    <asp:TextBox ID="voucherNumberTextBox" runat="server" MaxLength="256"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Country:</td>
                <td>
                    <uc1:PickerControl ID="countryPickerControl" runat="server" PopupType="POPUPCOUNTRY" 
                        AppendDescription="true" width="200"  /></td>
                <td>
                    Agent Type:</td>
                <td>
                    <asp:DropDownList ID="agentTypeDropDownList" runat="server">
                        <asp:ListItem Text="All" Value="All" Selected=true></asp:ListItem>
                        <asp:ListItem Text="Direct" Value="Direct"></asp:ListItem>
                        <asp:ListItem Text="Not Direct" Value="NoDirect"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    Connection Ref.:</td>
                <td>
                    <asp:TextBox ID="arrivalTextBox" runat="server" MaxLength="256"></asp:TextBox>
                    Arrival</td>
                <td>
                    Debtor Status:</td>
                <td>
                    <asp:DropDownList ID="debtorStatusDropDownList" runat="server">
                        <asp:ListItem Text="All" Value="" Selected></asp:ListItem>
                        <asp:ListItem Text="Credit" Value="Approved"></asp:ListItem>
                        <asp:ListItem Text="Prepaid" Value="Unapproved"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="departureTextBox" runat="server" MaxLength="256"></asp:TextBox>
                    Departure
                </td>
            </tr>
        <%--</table>--%>
        <tr><td><br /></td></tr>
        <%--<table cellspacing="4" width="100%" border="0">--%>
            <tr width="100%">
                <td colspan="2">
                    <b>Customer</b></td>
            </tr>
            <tr width="100%">
                <td width="150">
                    Surname:</td>
                <td width="250">
                    <asp:TextBox ID="surnameTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
                <td width="150">
                    First Name:</td>
                <td>
                    <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="64"></asp:TextBox></td>
            </tr>
        </table>
    </asp:Panel>

    <br />
    <table width="100%" border="0">
        <tr>
            <td colspan=2 align="right">
                <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" CausesValidation="true" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" /></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>
        <TABLE id="sortTable"  runat=server width="100%" visible=false ><TBODY><TR>
        <TD style="border-width: 1px 0px">
        
        <i><asp:Label id="resultLabel" runat="server" ></asp:Label></i> </TD>
         <TD style="border-width: 1px 0px" align=right width="10%">Sort By: </TD>
          <TD style="border-width: 1px 0px" align=right width="30%">
                    <asp:RadioButtonList id="headerRadioButtonList" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" Enabled="false"  CausesValidation="true">
                    <asp:ListItem Text="Hirer" Value="HLName"></asp:ListItem>
                    <asp:ListItem Text="Check-Out Date" Value="CKOWhenDate" Selected=True></asp:ListItem>
                    <asp:ListItem Text="Booking" Value="BooRnt"></asp:ListItem>
                </asp:RadioButtonList> 
                
                </TD></TR></TBODY></TABLE>
                <br />
        
        <asp:GridView id="resultGridView" runat="server" Width="100%" AutoGenerateColumns="False" class="dataTableColor">
        <AlternatingRowStyle CssClass="oddRow" />
        <RowStyle CssClass="evenRow" />
        
        <Columns>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="booRntLabel" runat="server" Text='<%# Bind("BooRnt") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Booking/Rental">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("BooRnt")%>' 
                        NavigateUrl='<%# GetBookingUrl(Eval("BooId"), Eval("RntId")) %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AgnCode" HeaderText="Agent" ReadOnly="True" SortExpression="AgnCode" />
            <asp:BoundField DataField="HLName" HeaderText="Hirer Last Name" ReadOnly="True" SortExpression="HLName" />
            <asp:BoundField DataField="RntSt" HeaderText="St" ReadOnly="True" SortExpression="RntSt" />
            <asp:BoundField DataField="UnitNo" HeaderText="Unit No" ReadOnly="True" SortExpression="UnitNo" />
            <asp:TemplateField HeaderText="Check-Out Date" >
                <ItemTemplate>
                    <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("CKOWhen")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CKOWhenDate" HeaderText="Check-Out Date-new" ReadOnly="True" SortExpression="CKOWhenDate" Visible=false  />
            <asp:BoundField DataField="CKOLoc" HeaderText="Check-Out Loc" ReadOnly="True" SortExpression="CKOLoc" />
            <asp:TemplateField HeaderText="Check-In Date" >
                <ItemTemplate>
                    <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("CKIWhen")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
          
            <asp:BoundField DataField="CKILoc" HeaderText="Check-In Loc" ReadOnly="True" SortExpression="CKILoc" />
            <asp:BoundField DataField="VehType" HeaderText="Veh Type" ReadOnly="True" SortExpression="VehType" />
        </Columns>
    </asp:GridView> 
</contenttemplate>
        <triggers>
        <asp:AsyncPostBackTrigger ControlID="headerRadioButtonList" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <br />
    </asp:Panel> 
</asp:Content>
