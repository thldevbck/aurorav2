''REV:MIA SEP.4 2013 - Customer Title from query
''                    - add DecodeText And EncodeText to trapped invalid text  
''REV:MIA SEP.4 2013 -- Addition of Country pickercontrol and changing the textbox for passport country to pickercontrol

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports Aurora.Booking.Services.BookingCustomer


<AuroraPageTitleAttribute("Customer Maintenance")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN023,GEN031,GEN045,GEN046,GEN107")> _
Partial Class Booking_CustomerEdit
    Inherits AuroraPage

    Private bookingId As String = ""
    Private rentalId As String = ""
    Private customerId As String = ""
    Private isFromSearch As String = "0"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'trFlag.Visible = False
            trFlag.Style("display") = "none"

            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If
            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearch = Request.QueryString("isFromSearch").ToString()
            End If

            If Not Page.IsPostBack Then
                ''rev:mia Sept.2 2013 - Customer Title from query
                PopulateTitle()

                personTypeDropDownList_DataBound()
                prefCommDropDownList_DataBound()

                If Not Request.QueryString("customerId") Is Nothing Then
                    customerIdLabel.Text = Request.QueryString("customerId").ToString()
                    customerId = customerIdLabel.Text
                End If
                getCustomerDetail()
                Me.PopulateDefaultTextbox()

               
            Else
                customerId = customerIdLabel.Text
            End If

            If String.IsNullOrEmpty(customerId) Then
                deleteButton.Enabled = False
                deleteButton1.Enabled = False
            Else
                deleteButton.Enabled = True
                deleteButton1.Enabled = True
            End If

            'JL 2008/07/10
            'Do the logic on the server side
            'Dim dateTextBox As TextBox
            'dateTextBox = dobDateControl.FindControl("dateTextBox")
            'dateTextBox.Attributes.Add("onBlur", "dobDateControlOnChange('" + dateTextBox.ClientID + "');")

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub personTypeDropDownList_DataBound()
        Dim ds As DataSet
        ds = Data.GetCodecodetype("41", "")
        personTypeDropDownList.DataSource = ds.Tables(0)
        personTypeDropDownList.DataValueField = "ID"
        personTypeDropDownList.DataTextField = "CODE"
        personTypeDropDownList.DataBind()
    End Sub

    Protected Sub prefCommDropDownList_DataBound()
        Dim ds As DataSet
        ds = Data.GetCodecodetype("11", "")
        prefCommDropDownList.DataSource = ds.Tables(0)
        prefCommDropDownList.DataValueField = "ID"
        prefCommDropDownList.DataTextField = "CODE"
        prefCommDropDownList.DataBind()
    End Sub

    Private Sub getCustomerDetail()

        Dim fleetDriver As Boolean
        fleetDriver = BookingCustomer.FleetDriverCheckBoxEnable(UserCode)
        fleetDriverCheckBox.Enabled = fleetDriver

        Dim ds As DataSet

        If Not String.IsNullOrEmpty(customerId) Then
            ds = BookingCustomer.GetCustomerDetails(customerId)
        Else
            ds = BookingCustomer.GetCustomerDetails(UserCode)
        End If

        
        'Customer Details

        lastNameTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("LastName"))
        firstNameTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("FirstName"))
        ''titleTextBox.Text = ds.Tables(1).Rows(0)("Title")
        'JL 2008/07/10
        'use hirerCheckBox
        'hirerLabel.Text = ds.Tables(1).Rows(0)("Hirer")

        ''rev:mia Sept.2 2013 - Customer Title from query
        Try
            customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DecodeText(ds.Tables(1).Rows(0)("Title").ToString)))
        Catch ex As Exception
            customertitleDropdown.SelectedIndex = 0
        End Try

        licenceNumTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("LicenceNum"))
        If Not String.IsNullOrEmpty(ds.Tables(1).Rows(0)("ExpiryDate")) Then
            expiryDateDateControl.Date = Utility.ParseDateTime(ds.Tables(1).Rows(0)("ExpiryDate"))
        Else
            expiryDateDateControl.Text = ""
        End If
        issuingAuthorityTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("IssingAuthority"))

        If Not String.IsNullOrEmpty(ds.Tables(1).Rows(0)("DOB")) Then
            dobDateControl.Date = Utility.ParseDateTime(ds.Tables(1).Rows(0)("DOB"))
        Else
            dobDateControl.Text = ""
        End If

        Dim dob As Date = dobDateControl.Date

        If Not dob = Nothing And dob <> "1/1/1900" Then
            Dim age As Integer = Today.Year - dob.Year

            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                age -= 1
            End If
            ageLabel.Text = Utility.ParseInt(age) & " years old"
        Else
            ageLabel.Text = ""
        End If

        vipCheckBox.Checked = ds.Tables(1).Rows(0)("Vip")
        fleetDriverCheckBox.Checked = ds.Tables(1).Rows(0)("fleetDriver")
        personTypeDropDownList.SelectedValue = ds.Tables(1).Rows(0)("PersonType")

        passportNoTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PassportNo"))
        ''rev:mia Sept.2 2013 - country Title from query -REPLACE WITH DROPDOWN
        passportCountryPickerControl.Text = DecodeText(ds.Tables(1).Rows(0)("PassportCountry"))

        integrityNoLabel.Text = ds.Tables(1).Rows(0)("IntegrityNo")

        'Physical Address
        physicalAddressIdLabel.Text = DecodeText(ds.Tables(2).Rows(0)("AddId"))
        physicalAddressCodIdLabel.Text = DecodeText(ds.Tables(2).Rows(0)("CodeId"))
        physicalAddressIntegrityNoLabel.Text = DecodeText(ds.Tables(2).Rows(0)("IntegrityNo"))
        'physicalStreetTextBox.Text = ds.Tables(2).Rows(0)("Street")

        ExtractNumberAndStreet(DecodeText(ds.Tables(2).Rows(0)("Street").ToString()))

        physicalSuburbTextBox.Text = DecodeText(ds.Tables(2).Rows(0)("Suburb"))
        physicalCityTextBox.Text = DecodeText(ds.Tables(2).Rows(0)("CityTown"))
        physicalStateTextBox.Text = DecodeText(ds.Tables(2).Rows(0)("State"))
        physicalPostCodeTextBox.Text = DecodeText(ds.Tables(2).Rows(0)("Postcode"))
        physicalCountryPickerControl.Text = DecodeText(ds.Tables(2).Rows(0)("Country"))

        'Postal Address
        postalAddressIdLabel.Text = DecodeText(ds.Tables(3).Rows(0)("AddId"))
        postalAddressCodIdLabel.Text = DecodeText(ds.Tables(3).Rows(0)("CodeId"))
        postalAddressIntegrityNoLabel.Text = DecodeText(ds.Tables(3).Rows(0)("IntegrityNo"))
        postalStreetTextBox.Text = DecodeText(ds.Tables(3).Rows(0)("Street"))
        postalSuburbTextBox.Text = DecodeText(ds.Tables(3).Rows(0)("Suburb"))
        postalCityTextBox.Text = DecodeText(ds.Tables(3).Rows(0)("CityTown"))
        postalStateTextBox.Text = DecodeText(ds.Tables(3).Rows(0)("State"))
        postalPostcodeTextBox.Text = DecodeText(ds.Tables(3).Rows(0)("Postcode"))
        postalCountryPickerControl.Text = DecodeText(ds.Tables(3).Rows(0)("Country"))

        'Contact Numbers
        dayCountryTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayCtyCode"))
        dayAreaTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayAreaCode"))
        dayNumberTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayNumber"))
        dayCodIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayCodId"))
        dayIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayId"))
        dayIntNumLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhDayIntNum"))

        nightCountryTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightCtyCode"))
        nightAreaTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightAreaCode"))
        nightNumberTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightNumber"))
        nightCodIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightCodId"))
        nightIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightId"))
        nightIntNumLabel.Text = DecodeText(ds.Tables(1).Rows(0)("PhNightIntNum"))

        mobileCountryTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("MobileCtyCode"))
        mobileAreaTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("MobileAreaCode"))
        mobileNumberTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("MobileNumber"))
        mobileCodIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("MobileCodId"))
        mobileIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("MobileId"))
        mobileIntNumLabel.Text = DecodeText(ds.Tables(1).Rows(0)("MobileIntNum"))

        faxCountryTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("FaxCtyCode"))
        faxAreaTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("FaxAreaCode"))
        faxNumberTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("FaxNumber"))
        faxCodIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("FaxCodId"))
        faxIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("FaxId"))
        faxIntNumLabel.Text = DecodeText(ds.Tables(1).Rows(0)("FaxIntNum"))

        emailTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("Email"))
        emailCodIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("EmailCodId"))
        emailIdLabel.Text = DecodeText(ds.Tables(1).Rows(0)("EmailId"))
        emailIntNumLabel.Text = DecodeText(ds.Tables(1).Rows(0)("EmailIntNum"))

        'Marketing Data
        mailingListCheckBox.Checked = ds.Tables(1).Rows(0)("MailLists")
        surveysCheckBox.Checked = ds.Tables(1).Rows(0)("SurVeys")
        prefCommDropDownList.SelectedValue = ds.Tables(1).Rows(0)("PrefComm")
        genderDropDownList.SelectedValue = ds.Tables(1).Rows(0)("Gender")
        occupationTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("Occupation"))
        loyaltyCardNumberTextBox.Text = DecodeText(ds.Tables(1).Rows(0)("LoyaltyCardNumber"))

        'Brochures
        brochuresGridView.DataSource = ds.Tables(5)
        brochuresGridView.DataBind()



        'Get checkbox values
        If Not (String.IsNullOrEmpty(bookingId) Or String.IsNullOrEmpty(rentalId) Or String.IsNullOrEmpty(customerId)) Then
            Dim dt2 As DataTable
            dt2 = BookingCustomer.GetCustomerSummary(True, bookingId, rentalId)

            For Each dr As DataRow In dt2.Rows
                If Utility.ParseString(dr.Item("CusId"), "").Trim = customerId Then
                    hirerCheckBox.Checked = Utility.ParseString(dr.Item("Hirer"), "0")
                    primaryHirerCheckBox.Checked = Utility.ParseString(dr.Item("PriHirer"), "0")
                    driverCheckBox.Checked = Utility.ParseString(dr.Item("IsDriver"), "0")
                End If
            Next
        Else
            If Not Request.QueryString("isDriver") Is Nothing Then
                If Request.QueryString("isDriver").ToString() = "1" Then
                    driverCheckBox.Checked = True
                End If
            End If
        End If

        checkBoxCheckedChanged()

    End Sub

#Region "Checkbox & Button Events"

    Protected Sub hirerCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hirerCheckBox.CheckedChanged
        If Not hirerCheckBox.Checked Then
            primaryHirerCheckBox.Checked = False
        End If
        checkBoxCheckedChanged()
        ScriptManager.GetCurrent(Page).SetFocus(primaryHirerCheckBox)
    End Sub

    Protected Sub primaryHirerCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles primaryHirerCheckBox.CheckedChanged
        If primaryHirerCheckBox.Checked Then
            hirerCheckBox.Checked = True
        End If
        checkBoxCheckedChanged()
        ScriptManager.GetCurrent(Page).SetFocus(driverCheckBox)
    End Sub

    Protected Sub driverCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles driverCheckBox.CheckedChanged
        checkBoxCheckedChanged()
        ScriptManager.GetCurrent(Page).SetFocus(licenceNumTextBox)
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If dobDateControl.IsValid And expiryDateDateControl.IsValid Then
            If updateCustomer() Then
                getCustomerDetail()
            End If
        Else
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
        End If

    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If dobDateControl.IsValid And expiryDateDateControl.IsValid Then
            If updateCustomer() Then
                customerIdLabel.Text = ""
                customerId = ""

                getCustomerDetail()
            End If
        Else
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
        End If

    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        getCustomerDetail()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        deleteCustomer()

        customerIdLabel.Text = ""
        customerId = ""
        getCustomerDetail()
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=" & BookingTabIndex.Customer & "&isFromSearch=" & isFromSearch)

    End Sub

#End Region

    Private Function updateCustomer() As Boolean

        Dim isHirer As Boolean = hirerCheckBox.Checked

        Dim action As String = ""
        Dim errorMessage As String = ""

        If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
            If String.IsNullOrEmpty(expiryDateDateControl.Text) Or String.IsNullOrEmpty(issuingAuthorityTextBox.Text) Or String.IsNullOrEmpty(dobDateControl.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, "You must enter Expiry Date, Issuing Authority and Date of Birth for licence details")
                Return False
            End If
        End If

        If String.IsNullOrEmpty(rentalId) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN031"))
            Return False
        End If

        If String.IsNullOrEmpty(dayIntNumLabel.Text) Then
            dayIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(nightIntNumLabel.Text) Then
            nightIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(mobileIntNumLabel.Text) Then
            mobileIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(faxIntNumLabel.Text) Then
            faxIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(emailIntNumLabel.Text) Then
            emailIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(integrityNoLabel.Text) Then
            integrityNoLabel.Text = "0"
        End If

        'Check for Mandatory Fields
        If String.IsNullOrEmpty(lastNameTextBox.Text) Or String.IsNullOrEmpty(firstNameTextBox.Text) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
            Return False
        End If

        If isHirer Then
            If String.IsNullOrEmpty(physicalCityTextBox.Text) Or String.IsNullOrEmpty(physicalCountryPickerControl.DataId) Or String.IsNullOrEmpty(physicalCityTextBox.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
                Return False
            End If
        End If

        'validate the driver if the driver licence field is not empty
        If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
            If String.IsNullOrEmpty(dobDateControl.Text) Or String.IsNullOrEmpty(issuingAuthorityTextBox.Text) Or String.IsNullOrEmpty(expiryDateDateControl.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN107"))
                Return False
            End If
            ''the customer is driver

            errorMessage = ""
            errorMessage = BookingCustomer.ValidateDriver(rentalId, dobDateControl.Date, expiryDateDateControl.Date)

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return False
            End If
        End If

        'Generate Customer ID
        If String.IsNullOrEmpty(customerId) Then
            customerId = Aurora.Common.Data.GetNewId()
            action = "I"
        Else
            action = "M"
        End If

        'Validating Phone Details by Executing stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(dayNumberTextBox.Text, "", dayIdLabel.Text, dayCodIdLabel.Text) Then
            Return False
        End If

        'Validating night Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(nightNumberTextBox.Text, "", nightIdLabel.Text, nightCodIdLabel.Text) Then
            Return False
        End If

        'Validating mobile Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(mobileNumberTextBox.Text, "", mobileIdLabel.Text, mobileCodIdLabel.Text) Then
            Return False
        End If

        'Validating FAX Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(faxNumberTextBox.Text, "", faxIdLabel.Text, faxCodIdLabel.Text) Then
            Return False
        End If

        'Validating Email Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber("", emailTextBox.Text, emailIdLabel.Text, emailCodIdLabel.Text) Then
            Return False
        End If

        'Validating Physical Address details by Executing stored procedure "sp_validateAddress"
        If Not validateAdderss(physicalCountryPickerControl.DataId, physicalStateTextBox.Text, physicalAddressIdLabel.Text, physicalAddressCodIdLabel.Text) Then
            Return False
        End If

        'Validating Postal Address details by Executing stored procedure "sp_validateAddress"
        If Not validateAdderss(postalCountryPickerControl.DataId, postalStateTextBox.Text, postalAddressIdLabel.Text, postalAddressCodIdLabel.Text) Then
            Return False
        End If

        'Update customer details       
        errorMessage = ""
        ''rev:mia Sept.2 2013 - Customer Title from query - CHANGE titleTextBox.Text TO customertitleDropdown.SELECTEDITEM.TEXT
        errorMessage = BookingCustomer.UpdateCustomer(customerId, _
                                                      EncodeText(firstNameTextBox.Text), _
                                                      EncodeText(lastNameTextBox.Text), _
                                                      vipCheckBox.Checked, _
                                                      fleetDriverCheckBox.Checked, _
                                                      personTypeDropDownList.SelectedValue, _
                                                      customertitleDropdown.SelectedItem.Text, _
                                                      dobDateControl.Date, _
                                                      EncodeText(licenceNumTextBox.Text), _
                                                      expiryDateDateControl.Date, _
                                                      EncodeText(issuingAuthorityTextBox.Text), _
                                                      genderDropDownList.SelectedValue, _
                                                      EncodeText(occupationTextBox.Text), _
                                                      prefCommDropDownList.SelectedValue, _
                                                      mailingListCheckBox.Checked, _
                                                      surveysCheckBox.Checked, _
                                                      EncodeText(integrityNoLabel.Text), _
                                                      EncodeText(passportNoTextBox.Text), _
                                                      EncodeText(passportCountryPickerControl.DataId), _
                                                      rentalId, _
                                                      UserCode, _
                                                      UserCode, _
                                                      "CustomerMgt.aspx", _
                                                      EncodeText(loyaltyCardNumberTextBox.Text.Trim()))


        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If

        'Updating Day Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(dayIdLabel.Text, dayCodIdLabel.Text, dayAreaTextBox.Text, dayCountryTextBox.Text, dayNumberTextBox.Text, dayIntNumLabel.Text, "") Then
            Return False
        End If

        'Updating Night Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(nightIdLabel.Text, nightCodIdLabel.Text, nightAreaTextBox.Text, nightCountryTextBox.Text, nightNumberTextBox.Text, nightIntNumLabel.Text, "") Then
            Return False
        End If

        'Updating Mobile Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(mobileIdLabel.Text, mobileCodIdLabel.Text, mobileAreaTextBox.Text, mobileCountryTextBox.Text, mobileNumberTextBox.Text, mobileIntNumLabel.Text, "") Then
            Return False
        End If

        'Updating Fax Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(faxIdLabel.Text, faxCodIdLabel.Text, faxAreaTextBox.Text, faxCountryTextBox.Text, faxNumberTextBox.Text, faxIntNumLabel.Text, "") Then
            Return False
        End If

        'Updating Email details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(emailIdLabel.Text, emailCodIdLabel.Text, "", "", "", emailIntNumLabel.Text, emailTextBox.Text) Then
            Return False
        End If

        ''Updating Physical Address details by Executing stored procedure "sp_updateAddress"
        If Not updateAddress(physicalAddressIdLabel.Text, GetCombinedNumberAndStreet(), physicalAddressCodIdLabel.Text, _
            customerId, physicalSuburbTextBox.Text, physicalPostCodeTextBox.Text, physicalCountryPickerControl.DataId, _
            physicalCityTextBox.Text, physicalStateTextBox.Text, physicalAddressIntegrityNoLabel.Text) Then
            Return False
        End If

        ''Updating Postal Address details by Executing stored procedure "sp_updateAddress"
        If Not updateAddress(postalAddressIdLabel.Text, postalStreetTextBox.Text, postalAddressCodIdLabel.Text, _
                   customerId, postalSuburbTextBox.Text, postalPostcodeTextBox.Text, postalCountryPickerControl.DataId, _
                   postalCityTextBox.Text, postalStateTextBox.Text, postalAddressIntegrityNoLabel.Text) Then
            Return False
        End If

        'Update traveller details
        'JL 2008/07/10
        'use DriverCheckBox rather than licenceNumTextBox
        Dim isDriver As Boolean = driverCheckBox.Checked
        'If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
        '    isDriver = True
        'End If
        Dim isPrimaryHirer As Boolean = primaryHirerCheckBox.Checked

        errorMessage = ""
        errorMessage = BookingCustomer.UpdateTraveller(customerId, rentalId, isDriver, isHirer, isPrimaryHirer, UserCode, UserCode, "CustomerMgt.aspx")
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If


        ' create Costomer Brochdure record
        For Each r As GridViewRow In brochuresGridView.Rows
            Dim qtyTextBox As TextBox
            qtyTextBox = r.FindControl("qtyTextBox")

            Dim qty As Integer = 0
            qty = Utility.ParseInt(qtyTextBox.Text, 0)

            If qty > 0 Then

                Dim brhIdLabel As Label
                brhIdLabel = r.FindControl("brhIdLabel")

                errorMessage = ""
                errorMessage = BookingCustomer.UpdateCustomerBrochure(brhIdLabel.Text, customerId, qty, UserCode, UserCode, "CustomerMgt.aspx")

                If Not String.IsNullOrEmpty(errorMessage) Then
                    SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                    Return False
                End If

            End If

        Next

        'Set success message
        If action = "I" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
        ElseIf action = "M" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        End If

        Return True

    End Function

    Private Function validatePhoneNumber(ByVal number As String, ByVal email As String, ByVal phoneId As String, ByVal codId As String) As Boolean

        Dim action As String
        If Not String.IsNullOrEmpty(number) Then
            If String.IsNullOrEmpty(phoneId) Then
                action = "I"
            Else
                action = "M"
            End If

            Dim errorMessage As String = ""
            errorMessage = BookingCustomer.ValidatePhoneNumber(action, "Customer", customerId, codId, number, email)

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return False
            End If
        End If
        Return True
    End Function

    Private Function validateAdderss(ByVal country As String, ByVal state As String, ByVal addressId As String, ByVal codId As String) As Boolean
        Dim action As String = ""

        If String.IsNullOrEmpty(addressId) Then
            action = "I"
        Else
            action = "M"
        End If

        Dim errorMessage As String = ""
        errorMessage = BookingCustomer.ValidateAddress(action, "Customer", customerId, codId, country, state)

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, errorMessage)
            Return False
        End If
        Return True
    End Function

    Private Function updatePhoneNumber(ByVal phoneId As String, ByVal codId As String, ByVal areaCode As String, _
        ByVal cityCode As String, ByVal number As String, ByVal integrityNo As String, ByVal email As String) As Boolean

        Dim action As String = ""
        If String.IsNullOrEmpty(phoneId) Then
            action = "I"
        Else
            action = "M"
        End If

        Dim errorMessage As String = ""
        errorMessage = BookingCustomer.UpdatePhoneNumber(action, _
                                                         EncodeText(phoneId), _
                                                         "Customer", _
                                                         EncodeText(customerId), _
                                                         codId, _
                                                         EncodeText(areaCode), _
                                                         EncodeText(cityCode), _
                                                         EncodeText(number), _
                                                         EncodeText(integrityNo), _
                                                         EncodeText(email), _
                                                         UserCode, _
                                                         "CustomerMgt.aspx")

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If
        Return True
    End Function

    Private Function updateAddress(ByVal addressId As String, ByVal street As String, _
        ByVal codeId As String, ByVal customerId As String, ByVal suburb As String, ByVal postCode As String, _
        ByVal countryCode As String, ByVal city As String, ByVal stateCode As String, ByVal integrityNo As String) As Boolean

        Dim active As String = ""
        If String.IsNullOrEmpty(addressId) Then
            active = "I"
        Else
            active = "M"
        End If

        Dim errorMessage As String = ""

        errorMessage = BookingCustomer.UpdateAddress(active, _
                                                     EncodeText(addressId), _
                                                     "Customer", _
                                                     EncodeText(street), _
                                                     EncodeText(codeId), _
                                                     EncodeText(customerId), _
                                                     EncodeText(suburb), _
                                                     EncodeText(postCode), _
                                                     EncodeText(countryCode), _
                                                     EncodeText(city), _
                                                     EncodeText(stateCode), _
                                                     EncodeText(integrityNo), _
                                                     UserCode, _
                                                     "CustomerMgt.aspx")

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If
        Return True

    End Function

    Private Sub deleteCustomer()

        ''the customer must associate with rental, if it isn't there return

        'validate the if any rental associates with the customer
        Dim errorMessage As String = ""
        errorMessage = ""
        errorMessage = BookingCustomer.ValidateDeletion(customerId, rentalId)
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

        ' delete the customer and associated tables
        ' validate the if any rental associates with the customer
        errorMessage = ""
        errorMessage = BookingCustomer.DeleteCustomer(customerId)
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

    End Sub


    Private Sub checkBoxCheckedChanged()

        hirerCollapsiblePanel.Collapsed = False
        driverCollapsiblePanel.Collapsed = True

        div1.Style.Value = "DISPLAY: inline"
        div2.Style.Value = "DISPLAY: inline"
        div3.Style.Value = "DISPLAY: inline"
        div4.Style.Value = "DISPLAY: inline"
        divIsStreetNumberRequired.Style.Value = "display: inline"

        div5.Style.Value = "DISPLAY: none"
        div6.Style.Value = "DISPLAY: none"
        div7.Style.Value = "DISPLAY: none"
        div8.Style.Value = "DISPLAY: none"

        If driverCheckBox.Checked Then
            driverCollapsiblePanel.Collapsed = False

            div5.Style.Value = "DISPLAY: inline"
            div6.Style.Value = "DISPLAY: inline"
            div7.Style.Value = "DISPLAY: inline"
            div8.Style.Value = "DISPLAY: inline"

            If Not hirerCheckBox.Checked Then
                hirerCollapsiblePanel.Collapsed = True

                div1.Style.Value = "DISPLAY: none"
                div2.Style.Value = "DISPLAY: none"
                div3.Style.Value = "DISPLAY: none"
                div4.Style.Value = "DISPLAY: none"

                divIsStreetNumberRequired.Style.Value = "display: none"
            End If
        End If

    End Sub

    Protected Sub dobDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dobDateControl.DateChanged

        'CalC age
        If dobDateControl.IsValid Then
            Dim age As Integer


            ''rev:mia Issue #567
            If Not (dobDateControl.Date = "#12:00:00 AM#") Then
                If dobDateControl.Date > Date.Today Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, "Customer's date of birth must be ealier than today!")
                    ageLabel.Text = ""
                Else
                    age = Utility.GetAge(dobDateControl.Date)
                    ageLabel.Text = Utility.GetAge(dobDateControl.Date) & " years old"

                    If String.IsNullOrEmpty(issuingAuthorityTextBox.Text) _
                        AndAlso String.IsNullOrEmpty(expiryDateDateControl.Text) Then
                        findDriver()
                    End If
                End If
            Else
                ageLabel.Text = ""
            End If
            
        End If
        If CBool(CType(hirerCollapsiblePanel.FindControl("collapsiblePanelExtender_ClientState"), System.Web.UI.WebControls.HiddenField).Value) Then
            ScriptManager.GetCurrent(Page).SetFocus(saveButton1)
            saveButton1.Style("color") = "#FFBD49"
        Else
            ScriptManager.GetCurrent(Page).SetFocus(vipCheckBox)
        End If

    End Sub

    Protected Sub licenceNumTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles licenceNumTextBox.TextChanged
        If String.IsNullOrEmpty(issuingAuthorityTextBox.Text) _
            AndAlso String.IsNullOrEmpty(expiryDateDateControl.Text) Then
            findDriver()
        End If
        ScriptManager.GetCurrent(Page).SetFocus(expiryDateDateControl.FindControl("dateTextBox"))
    End Sub

    Private Sub findDriver()
        If (Not String.IsNullOrEmpty(rentalId)) And (Not String.IsNullOrEmpty(licenceNumTextBox.Text)) _
            And dobDateControl.IsValid Then

            Dim dt As DataTable

            dt = BookingCustomer.FindDriverByLicence(rentalId, licenceNumTextBox.Text, dobDateControl.Date)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("DriverId") <> customerId Then
                    lastNameTextBox.Text = DecodeText(dt.Rows(0)("LastName"))
                    firstNameTextBox.Text = DecodeText(dt.Rows(0)("FirstName"))

                    ''titleTextBox.Text = dt.Rows(0)("Title")
                    ''rev:mia Sept.2 2013 - Customer Title from query
                    Try
                        customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DecodeText(dt.Rows(0)("Title").ToString)))
                    Catch ex As Exception
                        customertitleDropdown.SelectedIndex = 0
                    End Try


                    issuingAuthorityTextBox.Text = DecodeText(dt.Rows(0)("IssuingAuthority"))
                    expiryDateDateControl.Date = Utility.ParseDateTime(dt.Rows(0)("ExpiryDate"))
                    ageLabel.Text = dt.Rows(0)("Age")
                Else
                    SetShortMessage(AuroraHeaderMessageType.Information, dt.Rows(0)("LastName") & " " & dt.Rows(0)("FirstName") & " is already in the driver table !")
                End If
            End If

            '<RentalId>3285499-2</RentalId>
            '<DriverId>FA4A7B55-CFC5-4909-8F8C-2F1E6F09594D</DriverId>
            '<LicenceNo>1</LicenceNo>
            '<DOB>19/12/1980</DOB>
            '<LastName>Fdfd</LastName>
            '<FirstName>C</FirstName>
            '<Title></Title>
            '<IssuingAuthority>1</IssuingAuthority>
            '<ExpiryDate>16/02/2008</ExpiryDate>
            '<RemoveDriver>0</RemoveDriver>
            '<InterityNo>106</InterityNo>
            '<Age>27</Age>

        End If
    End Sub

    Sub PopulateDefaultTextbox()
        ''rev:mia 591 Customer Search page - can't save customer details
        If Request.QueryString("lnm") IsNot Nothing Then
            lastNameTextBox.Text = Request.QueryString("lnm")
        End If

        If Request.QueryString("fnm") IsNot Nothing Then
            firstNameTextBox.Text = Request.QueryString("fnm")
        End If

        If Request.QueryString("dob") IsNot Nothing Then
            dobDateControl.Text = Request.QueryString("dob")
        End If

        If Request.QueryString("vip") IsNot Nothing Then
            vipCheckBox.Checked = IIf(Request.QueryString("vip") = "True", True, False)
        End If

        If Request.QueryString("flt") IsNot Nothing Then
            fleetDriverCheckBox.Checked = IIf(Request.QueryString("flt") = "True", True, False)
        End If

        If Request.QueryString("str") IsNot Nothing Then
            physicalStreetTextBox.Text = Request.QueryString("str")
        End If

        If Request.QueryString("cty") IsNot Nothing Then
            physicalCityTextBox.Text = Request.QueryString("cty")
        End If

        If Request.QueryString("sta") IsNot Nothing Then
            physicalStateTextBox.Text = Request.QueryString("sta")
        End If

        If Request.QueryString("lic") IsNot Nothing Then
            licenceNumTextBox.Text = Request.QueryString("lic")
        End If

        If Request.QueryString("ctry") IsNot Nothing Then
            physicalCountryPickerControl.Text = Request.QueryString("ctry")
        End If


    End Sub

    Protected Function GetCombinedNumberAndStreet() As String
        Return txtNumber.Text + vbCrLf + physicalStreetTextBox.Text
    End Function

    Protected Sub ExtractNumberAndStreet(ByVal value As String)

        If (value IsNot Nothing) Then
            value = value.Trim()

            If value.Length = 0 Then
                value = Nothing
            End If
        End If

        If value IsNot Nothing Then
            Dim index As Integer
            index = value.IndexOf(vbCrLf)

            If index > -1 Then
                txtNumber.Text = value.Substring(0, index)
                index += vbCrLf.Length
                If (index < value.Length) Then
                    physicalStreetTextBox.Text = value.Substring(index)
                Else
                    physicalStreetTextBox.Text = ""
                End If
                Return
            End If

            physicalStreetTextBox.Text = value
        Else
            physicalStreetTextBox.Text = ""
        End If

        txtNumber.Text = ""
    End Sub

#Region "rev:mia Sept.2 2013 - Customer Title from query"
    Sub PopulateTitle()
        customertitleDropdown.Items.Clear()
        customertitleDropdown.AppendDataBoundItems = True
        customertitleDropdown.Items.Add(New ListItem("", 0))
        customertitleDropdown.DataSource = GetCustomerTitle()
        customertitleDropdown.DataTextField = "CodDesc"
        customertitleDropdown.DataValueField = "CodCode"
        customertitleDropdown.DataBind()
    End Sub

#End Region
    
End Class

