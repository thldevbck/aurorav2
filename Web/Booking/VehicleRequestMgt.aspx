<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="VehicleRequestMgt.aspx.vb" Inherits="Booking_VehicleRequestMgt" Title="Untitled Page"
    EnableEventValidation="false" EnableViewStateMac="false" %>

<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc3" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/ShowBookingRequestAgentPopupUserControl.ascx"
    TagName="ShowBookingRequestAgentPopupUserControl" TagPrefix="uc4" %>
<%@ Register Src="~/Booking/Controls/BookingRequestAgentMgtPopupUserControl.ascx"
    TagName="BookingRequestAgentMgtPopupUserControl" TagPrefix="uc5" %>
<%@ Register Src="~\Booking\Controls\ContactAgentMgtPopupUserControl.ascx" TagName="ContactAgentMgtPopupUserControl"
    TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript" src="VehicleRequestMgt.js"> 
    </script>

    <asp:UpdatePanel id="UpdatePanelAgent" runat="server">
        <contenttemplate>
    <table width="100%" class="dataTable">
        <thead>
            <tr width="100%">
                <th colspan="8" style="text-align: left;">
                    Agent Contact and Reference
                </th>
            </tr>
        </thead>
        <tr width="100%">
            <td width="155">
                Agent:
            </td>
            <td width="340">
                <uc1:PickerControl ID="PickerControlForAgent" runat="server" EnableViewState="true"
                    AutoPostBack="false" AppendDescription="true" PopupType="AGENT" Width="300"></uc1:PickerControl>
                *
            </td>
            <td align="right" nowrap="nowrap">
                <asp:Button ID="updateAgentButton" runat="server" Text="Update Agent" CssClass="Button_Standard" />
                <asp:Button ID="addAgentButton" runat="server" Text="Add Agent" CssClass="Button_Standard" />
            </td>
        </tr>
        <tr width="100%">
            <td width="155">
                Contact:
            </td>
            <td width="340">
                <asp:DropDownList ID="ddlContact" TabIndex="1" runat="server" Width="322" AutoPostBack="false">
                </asp:DropDownList>
            </td>
            <td align="left">
                <asp:ImageButton ID="agentContactImageButton" runat="server" 
                    ImageUrl="~/Images/ShowDetail.gif" ImageAlign="AbsMiddle" />
            </td>
        </tr>
        <tr>
            <td width="155">
                Ref:
            </td>
            <td width="340">
                <asp:TextBox ID="txtRef" TabIndex="2" runat="server" MaxLength="20" AutoPostBack="false"
                    width="315">
                </asp:TextBox>
            </td>
            <td runat="server" id="tdbuttonNewAgent">
            </td>
        </tr>
    </table>
     
        </contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="UpdatePanelInformationHidden" runat="server">
        <contenttemplate>
         <uc3:CollapsiblePanel ID="detailsCollapsiblePanel" runat="server" Title="Additional Vehicle Information"   
                                  TargetControlID="panelVehicleInformationHidden" Width="100%"/>    
        
             <asp:Panel id="panelVehicleInformationHidden" runat="server" Style="overflow: hidden;"  Width="100%">
	                <table cellspacing="4" width="730" border="0">
                        <tr width="730">
				                <td width="155">Brand: </td>
				                <td>
					                <asp:DropDownList id="ddlBrand" tabIndex="14" runat="server" Width="322"></asp:DropDownList>
				                </td>
			                </tr>
		
	                </table>
	                <table cellspacing="4" width="730" border="0">
	                    <tr width="730">
	                        <td width="155" >Type: </td>
	                        <td colspan="3">
		    			        <asp:RadioButtonList id="rdlACorAV" tabIndex="15" runat="server" Width="104px" AutoPostBack="True" RepeatDirection="Horizontal"></asp:RadioButtonList>
        				    </td>
	                    </tr>
	                    <tr width="730">
	                     <td width="115"></td>
	                       <td runat="server" id="tableviewDefault" visible="true" colspan="3">
	                             <table  width="322" class="dataTable">
	                                <thead>
	                                    <tr>
	                                        <th>
	                                            Code
	                                        </th>
	                                        <th>
	                                            Description
	                                        </th>
	                                    </tr>
	                                </thead>
	                                <tr>
	                                    <td>
	                                        <input type="text" disabled="disabled"/>
	                                    </td>
	                                    <td>
	                                        <input type="text" disabled="disabled"/>
	                                    </td>
	                                </tr>
	                                
	                             </table>   
	                             
	                       </td> 
	                      </tr>
	                      <tr>
	                       <td width="155"></td>
	                       <td id="tableview" visible="false" runat="server">
	                       <asp:GridView id="GridView1" runat="server" CssClass="dataTableGrid" Width="322" AutoGenerateColumns="False"	DataKeyNames="TypId">
						        <RowStyle CssClass="evenRow"></RowStyle>
						        <AlternatingRowStyle CssClass="oddRow"></AlternatingRowStyle>
						            <Columns>
							            <asp:ButtonField CommandName="Select" Visible="False"></asp:ButtonField>
							            <asp:BoundField DataField="TypCode" HeaderText="Code"></asp:BoundField>
							            <asp:BoundField DataField="TypDesc" HeaderText="Description"></asp:BoundField>
						            </Columns>
					           </asp:GridView>
	                       </td>
	                      </tr>
	                </table>
	                <table>
	                    <tr width="100%">
	                        <td width="155" vAlign="top">Features: </td>
	                        <td vAlign="top">
					                <asp:CheckBoxList id="chkFeatures" tabIndex="16" runat="server" Width="240px"></asp:CheckBoxList>
				             </td>
	                    </tr>
	                </table>
	                <table cellspacing="4" width="100%" border="0">
	                     <tr>
                                <td width="155" vAlign="top">Others: </td>
        				        <td vAlign="top">
		        			        <asp:DropDownList id="ddlUncommonFeature1" tabIndex="17" runat="server" Width="322" AutoPostBack="True"></asp:DropDownList>
				        	        <br /><br />
					                <asp:DropDownList id="ddlUncommonFeature2" tabIndex="18" runat="server" Width="322" AutoPostBack="True"></asp:DropDownList>
				                </td>
			        </tr>
	                </table>
             </asp:Panel>
        </contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Panel ID="panelVehicleInformation" runat="server" Width="100%">
        <asp:UpdatePanel ID="UpdatepanelVehicleInformation" runat="server">
            <contenttemplate>
                  <table width="780" class="dataTable">
                          <thead>
                              <tr width="100%">
                                 <th colspan="7" style="text-align: left;">
                                        Products
                                 </th>
                               </tr>
                          </thead>
                          <tr width="100%">
                                <td height="5">
                                </td>
                          </tr>
                          <tr width="100%">
                                <td width="155">
                                    Product:
                                </td>
                                <td width="340">
                                    <uc1:PickerControl ID="PickerControlForProduct" runat="server" EnableViewState="true"
                                        PopupType="Product4BookingSearch" AppendDescription="true" Width="300"></uc1:PickerControl>
                                    *
                                </td>
                                <td width="160">
                                    No.of Vehicles:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNoOfVehicles" TabIndex="4" runat="server" Width="73px" Visible="false"
                                        Enabled="false"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtNoOfVehicles" runat="server" FilterType="Numbers"
                                        TargetControlID="txtNoOfVehicles">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFV_txtvehicles" runat="server" EnableClientScript="False"
                                        ValidationGroup="Book" Display="Dynamic" ControlToValidate="txtNoOfVehicles"
                                        SetFocusOnError="True" ErrorMessage="Please enter the number of Vehicles" Enabled="false">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CV_txtvehicles" runat="server" EnableClientScript="False"
                                        ValidationGroup="Book" Display="Dynamic" ControlToValidate="txtNoOfVehicles"
                                        ErrorMessage="Invalid value.Only numbers are allowed" ClientValidationFunction="IsNumberValid"
                                        OnServerValidate="CV_txtvehicles_ServerValidate">*</asp:CustomValidator>
                                    <asp:DropDownList ID="ddlNoOfVehicles" runat="server" AutoPostBack="false" Width="100px">
                                        <asp:ListItem Selected="true">1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>6</asp:ListItem>
                                        <asp:ListItem>7</asp:ListItem>
                                        <asp:ListItem>8</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                          </tr>
                          <tr width="100%">
                                <td width="155">
                                    Request Source:
                                </td>
                                <td width="340">
                                    <asp:DropDownList ID="ddlRequestSource" TabIndex="5" runat="server" Width="322">
                                    </asp:DropDownList>
                                    *
                                    <asp:RequiredFieldValidator ID="RFV_ddlrequestor" runat="server" EnableClientScript="False"
                                        ValidationGroup="Book" Display="Dynamic" ControlToValidate="ddlRequestSource"
                                        SetFocusOnError="True" ErrorMessage="Please select Request Source">*</asp:RequiredFieldValidator>
                                </td>
                    </tr>
        </table>
            </contenttemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <br />
    <asp:Panel ID="panelbranchCheckout" runat="server" Width="100%">
        <asp:UpdatePanel ID="UpdateForBranches" runat="server">
            <contenttemplate>
            <table width="100%" border="0" class="dataTable">
            <thead>
                <tr width="100%">
                    <th colspan="7" style="text-align: left;">
                        Check-Out / Check-In
                    </th>
                </tr>
            </thead>
            <tr width="100%">
                <td width="155">
                    Check-Out: &nbsp;Branch:
                </td>
                <td>
                    <uc1:PickerControl ID="PickerControlForCheckOut" runat="server" PopupType="lOCATION"
                        AppendDescription="true" Width="182" AutoPostBack="false" EnableViewState="true" />
                    *
                </td>
                <td>
                    Date:
                </td>
                <td align="right">
                    <asp:UpdatePanel id="UpdateForCKOBranch" runat="server" ChildrenAsTriggers="false"
                        UpdateMode="Conditional">
                        <triggers>
                                        
                                          <asp:AsyncPostBackTrigger ControlID="txtHirePeriod" EventName="TextChanged" />
                                          <asp:AsyncPostBackTrigger ControlID="DateControlForCheckOut" EventName="DateChanged" />
                                          <asp:AsyncPostBackTrigger ControlID="ddlAMPMcheckout" EventName="SelectedIndexChanged" />
                                          <asp:AsyncPostBackTrigger ControlID="btncalculate" EventName="Click" />
                                        
                                    </triggers>
                        <contenttemplate>
                                            <uc2:DateControl ID="DateControlForCheckOut" runat="server" AutoPostBack="true"/>
                                    </contenttemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="left">
                    *
                </td>
                <td width="100" align="center">
                    <asp:Label ID="lbldayCKO" runat="server" Width="40" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlAMPMcheckout" TabIndex="6" runat="server" Width="80" AutoPostBack="false">
                        <asp:ListItem Selected="True">AM</asp:ListItem>
                        <asp:ListItem>PM</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr width="100%">
                <td width="155">
                    Check-In: &nbsp;&nbsp;&nbsp; Branch:
                </td>
                <td>
                    <uc1:PickerControl ID="PickerControlForCheckIn" runat="server" PopupType="location"
                        AppendDescription="true" Width="182" AutoPostBack="false" EnableViewState="true" />
                    *
                </td>
                <td>
                    Date:
                </td>
                <td align="right">
                    <asp:UpdatePanel id="UpdateForCKIBranch" runat="server" ChildrenAsTriggers="false"
                        UpdateMode="Conditional">
                                       <triggers>
                                               
                                                <asp:AsyncPostBackTrigger ControlID="DateControlForCheckIn" EventName="DateChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="txtHirePeriod" EventName="TextChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlAMPMcheckin" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btncalculate" EventName="Click" />
                                               
                                        </triggers>
                                        <contenttemplate>
                                                <uc2:DateControl ID="DateControlForCheckIn"  runat="server"  AutoPostBack="true" />
                                        </contenttemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="left">
                    *
                </td>
                <td width="100" align="center">
                    <asp:Label ID="lblDayCKI" runat="server" Width="40" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlAMPMcheckin" TabIndex="7" runat="server" Width="80" AutoPostBack="false">
                        <asp:ListItem Selected="True">AM</asp:ListItem>
                        <asp:ListItem>PM</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                    <td width="155">Hire Period: </td>
                    <td>
                         <asp:TextBox id="txtHirePeriod" tabIndex=8 runat="server" Width="73px" MaxLength="3" autopostback = true></asp:TextBox> 
                        <ajaxToolkit:FilteredTextBoxExtender id="FLT_txtHirePeriod" runat="server" FilterType="Numbers" TargetControlID="txtHirePeriod"></ajaxToolkit:FilteredTextBoxExtender> <asp:CustomValidator id="CV_txthireperiod" runat="server" ValidationGroup="Book" Display="Dynamic" ControlToValidate="txtHirePeriod" ErrorMessage="Invalid value.Only numbers are allowed" ClientValidationFunction="IsNumberValid" OnServerValidate="CV_txthireperiod_ServerValidate">*</asp:CustomValidator> 
                      
                       <asp:DropDownList ID="ddlDays" TabIndex="9" runat="server" Width="68px">
                                                            <asp:ListItem Selected="True" Text="Days" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Weeks" Value="7"></asp:ListItem>
                                                            <asp:ListItem Text="Months" Value="30"></asp:ListItem>
                                            </asp:DropDownList>    

                                </td>
                                <td width="105" colspan="2">
                                     <asp:Button ID="btnCalculate" TabIndex="10" runat="server"
                                                Text="Calculate" CssClass="Button_Standard"></asp:Button>       
                                </td>
                                <td colspan="2">
                                     Begins:
                                </td>
                                <td>
                                    <asp:Label ID="lblDaysMessage" runat="server" Width="50px"></asp:Label> day(s)
                                </td>
            </tr>
            
            <tr>
                <td height="5">
                </td>
            </tr>
        </table>
            </contenttemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="panelbranchCheckin" runat="server" Width="100%">
        <%-- 
                     <table width="100%" border="0" class="dataTable">
                      <thead>
                            <tr width="100%">
                                <th colspan="7" style="text-align: left;">
                                    Check-In
                                </th>
                            </tr>
                        </thead>
                       
                         <tr width="100%">
                            <td width="155">
                                Branch
                            </td>
                            <td>
                                <uc1:PickerControl ID="PickerControlForCheckIn" runat="server" PopupType="location"
                                    AppendDescription="true" Width="182" AutoPostBack="false" EnableViewState="true" />
                                *
                            </td>
                            <td>
                                Date
                            </td>
                            <td align="right">
                                <asp:UpdatePanel id="UpdateForCKIBranch" runat="server" ChildrenAsTriggers="false"
                                    UpdateMode="Conditional">
                                    <triggers>
                                               
                                                <asp:AsyncPostBackTrigger ControlID="DateControlForCheckIn" EventName="DateChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="txtHirePeriod" EventName="TextChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlAMPMcheckin" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btncalculate" EventName="Click" />
                                               
                                        </triggers>
                                    <contenttemplate>
                                                <uc2:DateControl ID="DateControlForCheckIn"  runat="server"  AutoPostBack="true" />
                                        </contenttemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td align="left">
                                *
                            </td>
                            <td width="50" align="center">
                                <asp:Label ID="lblDayCKI" runat="server" Width="40" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAMPMcheckin" TabIndex="7" runat="server" Width="80" AutoPostBack="false">
                                    <asp:ListItem Selected="True">AM</asp:ListItem>
                                    <asp:ListItem>PM</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>

                      </table>
       --%>
    </asp:Panel>
    <asp:UpdatePanel id="UpdatePanelInformation" runat="server" ChildrenAsTriggers="false"
        UpdateMode="Conditional">
        <triggers>
             
            <asp:AsyncPostBackTrigger ControlID="ddlAdultNO" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlChildNO" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlInfantsNO" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtHirePeriod" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtSurname" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="btncalculate" EventName="Click" />
           
            
        </triggers>
        <contenttemplate>
        
                  <asp:Panel id="panelVehicleInformationHirePeriod" runat="server"  Width="100%">               
                    <table width="100%">
                            <tr width="100%">
                            
                                    <td width="155"></td>
                                    <td width="133">
                                    <%--
                                        <asp:TextBox id="txtHirePeriod" tabIndex=8 runat="server" Width="73px" MaxLength="3" autopostback = true></asp:TextBox> 
                                            <ajaxToolkit:FilteredTextBoxExtender id="FLT_txtHirePeriod" runat="server" FilterType="Numbers" TargetControlID="txtHirePeriod"></ajaxToolkit:FilteredTextBoxExtender> <asp:CustomValidator id="CV_txthireperiod" runat="server" ValidationGroup="Book" Display="Dynamic" ControlToValidate="txtHirePeriod" ErrorMessage="Invalid value.Only numbers are allowed" ClientValidationFunction="IsNumberValid" OnServerValidate="CV_txthireperiod_ServerValidate">*</asp:CustomValidator> 
                                    --%>
                                    
                                    </td>
                                    <%--
                                    <td width="93">
                                        <asp:DropDownList id="ddlDays" tabIndex=9 runat="server" Width="68px">
                                                <asp:ListItem Selected="True" Text="Days" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Weeks" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="Months" Value="30"></asp:ListItem>
                                        </asp:DropDownList> 
                                    
                                    </td>
                                    <td width="105">
                                                                <asp:Button id="btnCalculate" tabIndex=10 onclick="btnCalculate_Click" runat="server" Text="Calculate" cssclass="Button_Standard"></asp:Button> 
                                                        </td>
                                                         <td>
                                                                 Travel Begins: <asp:Label id="lblDaysMessage" runat="server" Width="50px" ></asp:Label> 
                                                        </td>
                                         </tr>
                                         <tr>
                                                        <td height="5">
                                                        </td>
                                         </tr>
                                    --%>
                                    
                     </table>
                  </asp:Panel>
                     
                  <asp:Panel id="panelVehicleInformationHire" runat="server"  Width="100%">                          
                      <table width="100%" border="0" class="dataTable">
                          <thead >
                                                       <tr width="100%">
                                                             <th colspan="7" style="text-align: left;">
                                                                     Hirer Details
                                                              </th>
                                                       </tr>
                          </thead>
                                                <tr>
                                                     <td  width="155">
                                                        Surname:
                                                     </td>
                                                     <td>
                                                                               <asp:TextBox id="txtSurname" 
                                                                                runat="server" 
                                                                                Width="165px" 
                                                                                MaxLength="25" 
                                                                                autopostback="false" enableviewstate="true"></asp:TextBox> *
                                                     </td>
                                                     
                                                     <td>
                                                         <asp:RequiredFieldValidator id="rfv_Surname" runat="server" ErrorMessage="Please enter Surname" SetFocusOnError="True" ControlToValidate="txtSurname" Display="Dynamic" ValidationGroup="Book" EnableClientScript="False">*</asp:RequiredFieldValidator> 
                                                     </td>
                                                     <td>
                                                        First Name:
                                                     </td>
                                                     <td>
                                                        <asp:TextBox id="txtfirstName" runat="server" Width="165px" MaxLength="25" autopostback="false" enableviewstate="true"></asp:TextBox> 
                                                     </td>
                                                     <td>
                                                        Title:
                                                     </td>
                                                     <td>
                                                                                <asp:DropDownList id="ddltitle" runat="server" width="50">
                                                                                    <asp:ListItem Selected="True">Mr.</asp:ListItem>
                                                                                    <asp:ListItem>Mrs.</asp:ListItem>
                                                                                    <asp:ListItem>Ms.</asp:ListItem>
                                                                                </asp:DropDownList> 
                                                     </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td  width="110">
                                                        Adult:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList id="ddlAdultNO" runat="server" autopostback="false" width="50px">
                                                             <asp:ListItem></asp:ListItem>
                                                             <asp:ListItem>0</asp:ListItem>
                                                             <asp:ListItem>1</asp:ListItem>
                                                             <asp:ListItem>2</asp:ListItem>
                                                             <asp:ListItem>3</asp:ListItem>
                                                             <asp:ListItem>4</asp:ListItem>
                                                             <asp:ListItem>5</asp:ListItem>
                                                             <asp:ListItem>6</asp:ListItem>
                                                             <asp:ListItem>7</asp:ListItem>
                                                             <asp:ListItem>8</asp:ListItem>
                                                        </asp:DropDownList> *
                                                    </td>
                                                    <td>
                                                       
                                                    </td>
                                                    <td>
                                                        Children:
                                                    </td>
                                                    <td>
                                                       <asp:DropDownList id="ddlChildNO" runat="server" autopostback="false" width="50px">
                                                         <asp:ListItem></asp:ListItem>
                                                         <asp:ListItem>0</asp:ListItem>
                                                         <asp:ListItem>1</asp:ListItem>
                                                         <asp:ListItem>2</asp:ListItem>
                                                         <asp:ListItem>3</asp:ListItem>
                                                         <asp:ListItem>4</asp:ListItem>
                                                         <asp:ListItem>5</asp:ListItem>
                                                         <asp:ListItem>6</asp:ListItem>
                                                         <asp:ListItem>7</asp:ListItem>
                                                         <asp:ListItem>8</asp:ListItem>
                                                    </asp:DropDownList> *
                                                    </td>
                                                    <td>
                                                        Infants:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList id="ddlInfantsNO" runat="server" autopostback="false" width="50px">
                                                         <asp:ListItem selected ="true"></asp:ListItem>
                                                         <asp:ListItem>0</asp:ListItem>
                                                         <asp:ListItem>1</asp:ListItem>
                                                         <asp:ListItem>2</asp:ListItem>
                                                         <asp:ListItem>3</asp:ListItem>
                                                         <asp:ListItem>4</asp:ListItem>
                                                         <asp:ListItem>5</asp:ListItem>
                                                         <asp:ListItem>6</asp:ListItem>
                                                         <asp:ListItem>7</asp:ListItem>
                                                         <asp:ListItem>8</asp:ListItem>
                                                    </asp:DropDownList> *
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <td  width="155">Package: </td>
                                                   <td colspan="5" width="400">
                                                                                <uc1:PickerControl id="PickerControlForPackage" runat="server" 
                                                                                    PopupType="AGENTPACKAGE4BOOKINGREQ" 
                                                                                    AppendDescription="true" AutoPostBack="true" 
                                                                                    Width="320">
                                                                              </uc1:PickerControl> 
                                              </td>
                                           
                                                </tr>
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                     
                        </table>
                  </asp:Panel>         
                                 
                                    
                                
                  <asp:Panel id="panelVehicleInformationPassengers1" runat="server" Width="90%" visible = false>
                      <table cellspacing="5" width="100%" border="0">
                       <tr>
                           <td width="152">
                                    Adult / Children / Infants
                            </td>
                          <td  width="11%">
                             <asp:TextBox id="txtAdult" tabIndex=11 runat="server" Width="47px" MaxLength="1"  autopostback="false"></asp:TextBox> *
                              <ajaxToolkit:FilteredTextBoxExtender id="FLT_txtAdult" 
                                    runat="server" 
                                    FilterType="Numbers" 
                                    TargetControlID="txtAdult">
                             </ajaxToolkit:FilteredTextBoxExtender> 
                               <asp:RequiredFieldValidator id="RFV_txtAdult" 
                                    Enabled = "false" 
                                    runat="server" 
                                    EnableClientScript="False" 
                                    ValidationGroup="Book" 
                                    Display="Dynamic" 
                                    ControlToValidate="txtAdult" 
                                    SetFocusOnError="True" 
                                    ErrorMessage="Please enter the number of adults (Zero is a valid value).">*</asp:RequiredFieldValidator> 
                                    
                                      <asp:CustomValidator id="CV_txtAdult" 
                                                           runat="server" 
                                                           EnableClientScript="False" 
                                                           ValidationGroup="Book" 
                                                           Display="Dynamic" 
                                                           ControlToValidate="txtAdult" 
                                                           ErrorMessage="Invalid value.Only numbers are allowed" 
                                                           ClientValidationFunction="IsNumberValid" 
                                                           OnServerValidate="CV_txtAdult_ServerValidate">*</asp:CustomValidator> 

                          </td>
                          
                          <td  width="11%">
                                <asp:TextBox id="txtchildren" 
                                             
                                             tabIndex=12 
                                             runat="server" 
                                             Width="47px" 
                                             MaxLength="1" autopostback="true"></asp:TextBox> *
                                <ajaxToolkit:FilteredTextBoxExtender id="FLT_txtchildren" 
                                            runat="server" 
                                            FilterType="Numbers" 
                                            TargetControlID="txtchildren"></ajaxToolkit:FilteredTextBoxExtender> 
                           
                                       <asp:RequiredFieldValidator id="RFV_txtchildren" 
                                                                   enabled = "false"   
                                                                   runat="server" 
                                                                   EnableClientScript="False" 
                                                                   ValidationGroup="Book" 
                                                                   Display="Dynamic" 
                                                                   ControlToValidate="txtchildren" 
                                                                   SetFocusOnError="True" 
                                                                   ErrorMessage="Please enter the number of adults (Zero is a valid value).">*</asp:RequiredFieldValidator> 
                                        <asp:CustomValidator id="CV_txtchildren" 
                                                             runat="server" 
                                                             EnableClientScript="False" 
                                                             ValidationGroup="Book" 
                                                             Display="Dynamic" 
                                                             ControlToValidate="txtchildren" 
                                                             ErrorMessage="Invalid value.Only numbers are allowed" 
                                                             ClientValidationFunction="IsNumberValid" 
                                                             OnServerValidate="CV_txtchildren_ServerValidate">*</asp:CustomValidator> 
                          </td>
                          
                          <td>
                            <asp:TextBox id="txtinfants" 
                                         tabIndex=13 
                                         runat="server" 
                                         Width="47px" MaxLength="1" autopostback="true">
                            </asp:TextBox> *
                           
                            <ajaxToolkit:FilteredTextBoxExtender id="FLT_txtinfants" 
                                        runat="server" 
                                        FilterType="Numbers" 
                                        TargetControlID="txtinfants">
                            </ajaxToolkit:FilteredTextBoxExtender> 
                          
                            <asp:RequiredFieldValidator id="RFV_txtinfants" 
                                                        enabled = "false" 
                                                        runat="server" 
                                                        EnableClientScript="False" 
                                                        ValidationGroup="Book" 
                                                        Display="Dynamic" 
                                                        ControlToValidate="txtinfants" 
                                                        SetFocusOnError="True" 
                                                        ErrorMessage="Please enter the number of infants (Zero is a valid value).">*</asp:RequiredFieldValidator> 
                                                        
                                                        <asp:CustomValidator id="CV_txtinfants" 
                                                                             runat="server" 
                                                                             EnableClientScript="False" 
                                                                             ValidationGroup="Book" 
                                                                             Display="Dynamic" 
                                                                             ControlToValidate="txtinfants" 
                                                                             ErrorMessage="Invalid value.Only numbers are allowed" 
                                                                             ClientValidationFunction="IsNumberValid" 
                                                                             OnServerValidate="CV_txtinfants_ServerValidate">*</asp:CustomValidator> 
                            </td>
                          </tr>
                          
                       </Table>
                  </asp:Panel> 
                  
                  
                  <asp:Panel id="panelVehicleInformationPassengers" runat="server"   Width="90%"> 
                  <%--
                      <table cellspacing="5" width="100%" border="0">
                       <tr>
                           <td width="152">
                                    Adult / Children / Infants
                            </td>
                           <td  width="10%">
                                <asp:DropDownList id="ddlAdultNO" runat="server" autopostback="false" width="50px">
                                     <asp:ListItem>0</asp:ListItem>
                                     <asp:ListItem Selected="True">1</asp:ListItem>
                                     <asp:ListItem>2</asp:ListItem>
                                     <asp:ListItem>3</asp:ListItem>
                                     <asp:ListItem>4</asp:ListItem>
                                     <asp:ListItem>5</asp:ListItem>
                                     <asp:ListItem>6</asp:ListItem>
                                     <asp:ListItem>7</asp:ListItem>
                                     <asp:ListItem>8</asp:ListItem>
                                     
                                </asp:DropDownList> *
                          </td>
                          <td  width="11%">
                                <asp:DropDownList id="ddlChildNO" runat="server" autopostback="false" width="50px">
                                     <asp:ListItem selected ="true">0</asp:ListItem>
                                     <asp:ListItem>1</asp:ListItem>
                                     <asp:ListItem>2</asp:ListItem>
                                     <asp:ListItem>3</asp:ListItem>
                                     <asp:ListItem>4</asp:ListItem>
                                     <asp:ListItem>5</asp:ListItem>
                                     <asp:ListItem>6</asp:ListItem>
                                     <asp:ListItem>7</asp:ListItem>
                                     <asp:ListItem>8</asp:ListItem>
                                     
                                </asp:DropDownList> *
                          </td>
                          
                          <td>
                                <asp:DropDownList id="ddlInfantsNO" runat="server" autopostback="false" width="50px">
                                     <asp:ListItem selected ="true">0</asp:ListItem>
                                     <asp:ListItem>1</asp:ListItem>
                                     <asp:ListItem>2</asp:ListItem>
                                     <asp:ListItem>3</asp:ListItem>
                                     <asp:ListItem>4</asp:ListItem>
                                     <asp:ListItem>5</asp:ListItem>
                                     <asp:ListItem>6</asp:ListItem>
                                     <asp:ListItem>7</asp:ListItem>
                                     <asp:ListItem>8</asp:ListItem>
                                     
                                </asp:DropDownList> *
                          </td>

                      </tr>
                   </table>       
                   --%>
                  </asp:Panel>                      
                    
                  <asp:Panel id="panelVehicleInformationPackage" runat="server"   Width="100%">               
                        <table width="100%">
                        <%--
                        <tr width="100%">
                                              <td  width="155">Package </td>
                                              <td>
                                                                                <uc1:PickerControl id="PickerControlForPackage" runat="server" 
                                                                                    PopupType="AGENTPACKAGE4BOOKINGREQ" 
                                                                                    AppendDescription="true" AutoPostBack="true" 
                                                                                    Width="300">
                                                                              </uc1:PickerControl> 
                                              </td>
                          </tr>                    
                        --%>
                                           
                                            
                                           
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>

                                         </table>
                 </asp:Panel>                            
                       
                      
                 <asp:Panel id="panelVehicleInformationButons" runat="server" Style="overflow: hidden;"  Width="100%">               
                                                <table cellspacing="5" width="100%" border="0">   
                            <tr width="100%">
                                
                            <td vAlign=bottom width="100%" align="right">
                                <asp:Button id="btnDisplayAvailabilityWithcost" tabIndex=19 runat="server" Width="180px" Text="Display Availability With Cost" ValidationGroup="Book" UseSubmitBehavior="False" CssClass="Button_Standard"></asp:Button> 
                                <asp:Button id="btnReloc" tabIndex=20 runat="server"  Text="Reloc Only" CssClass="Button_Standard"></asp:Button> 
                                <asp:Button id="btnNewBooking" tabIndex=21 runat="server" Text="New Booking" CssClass="Button_Standard"></asp:Button> 
                                <asp:Button id="btnRequestBrochure" tabIndex=22 runat="server"  Text="Request Brochure" CssClass="Button_Standard"></asp:Button> 
                            </td>
                           </tr>
                          </Table>
                       </asp:Panel>  
                          
                               
  
                                <ajaxToolkit:ConfirmButtonExtender id="Confirm_NBR" runat="server" TargetControlID="btnNewBooking" ConfirmText="Are you sure you want to start a new Booking Request?">
            </ajaxToolkit:ConfirmButtonExtender> 
                      
            
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updatePanel1" runat="server">
        <contenttemplate>
            <uc4:ShowBookingRequestAgentPopupUserControl id="showBookingRequestAgentPopupUserControl1" runat=server></uc4:ShowBookingRequestAgentPopupUserControl>
    </contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updatePanel2" runat="server">
        <contenttemplate>
            <uc5:BookingRequestAgentMgtPopupUserControl id="bookingRequestAgentMgtPopupUserControl1" runat=server></uc5:BookingRequestAgentMgtPopupUserControl>
        </contenttemplate>
    </asp:UpdatePanel>
    <uc6:ContactAgentMgtPopupUserControl ID="ContactAgentMgtPopupUserControl1" runat="server" />
    <input id="hid_html_vhrId" type="hidden" runat="server" />
    <input id="hid_html_bkrId" type="hidden" runat="server" />
    <input id="hid_html_booId" type="hidden" runat="server" />
    <input id="hid_html_butSel" type="hidden" runat="server" />
    <asp:HiddenField ID="hidContactID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidCKOandCKIinfo" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidcompanion" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidAgentID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidPackageID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidGridViewItem" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidAgentCode" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidGinitialValue" runat="server" EnableViewState="true" />
    <asp:Label ID="lblVhrID" runat="server" Text="Label" EnableViewState="true" Visible="false" />
    <asp:Label ID="lblConcode" runat="server" Text="Label" EnableViewState="true" Visible="false" />
    &nbsp;
    <asp:Literal ID="litJS" runat="server" EnableViewState="true" Visible="false" />
    <asp:Label ID="lblTest" runat="server" Visible="true" EnableViewState="true" />
    <asp:HiddenField ID="HidCKO" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidCKI" runat="server" EnableViewState="true" />
</asp:Content>
