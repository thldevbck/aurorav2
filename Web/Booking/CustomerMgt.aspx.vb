Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

<AuroraPageTitleAttribute("Customer Maintenance")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN023,GEN031,GEN045,GEN046,GEN107")> _
Partial Class Booking_CustomerMgt
    Inherits AuroraPage

    Private bookingId As String = ""
    Private rentalId As String = ""
    Private customerId As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3285499"
            'rentalId = "3285499-2"
            'customerId = "EF887292C3A346FBBFAAAFFE13E996FA"

            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If

            If Not Page.IsPostBack Then

                personTypeDropDownList_DataBound()
                prefCommDropDownList_DataBound()

                If Not Request.QueryString("customerId") Is Nothing Then
                    customerIdLabel.Text = Request.QueryString("customerId").ToString()
                    customerId = customerIdLabel.Text
                End If
                getCustomerDetail()
            Else
                customerId = customerIdLabel.Text
            End If

            If String.IsNullOrEmpty(customerId) Then
                deleteButton.Enabled = False
                deleteButton1.Enabled = False
            Else
                deleteButton.Enabled = True
                deleteButton1.Enabled = True
            End If

            Dim dateTextBox As TextBox
            dateTextBox = dobDateControl.FindControl("dateTextBox")
            dateTextBox.Attributes.Add("onBlur", "dobDateControlOnChange('" + dateTextBox.ClientID + "');")


        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub personTypeDropDownList_DataBound()
        Dim ds As DataSet
        ds = Data.GetCodecodetype("41", "")
        personTypeDropDownList.DataSource = ds.Tables(0)
        personTypeDropDownList.DataValueField = "ID"
        personTypeDropDownList.DataTextField = "CODE"
        personTypeDropDownList.DataBind()
    End Sub

    Protected Sub prefCommDropDownList_DataBound()
        Dim ds As DataSet
        ds = Data.GetCodecodetype("11", "")
        prefCommDropDownList.DataSource = ds.Tables(0)
        prefCommDropDownList.DataValueField = "ID"
        prefCommDropDownList.DataTextField = "CODE"
        prefCommDropDownList.DataBind()
    End Sub

    Private Sub getCustomerDetail()

        Dim fleetDriver As Boolean
        fleetDriver = BookingCustomer.FleetDriverCheckBoxEnable(UserCode)
        fleetDriverCheckBox.Enabled = fleetDriver

        Dim ds As DataSet

        If Not String.IsNullOrEmpty(customerId) Then
            ds = BookingCustomer.GetCustomerDetails(customerId)
        Else
            ds = BookingCustomer.GetCustomerDetails(UserCode)
        End If

        'Customer Details

        lastNameTextBox.Text = ds.Tables(1).Rows(0)("LastName")
        firstNameTextBox.Text = ds.Tables(1).Rows(0)("FirstName")
        titleTextBox.Text = ds.Tables(1).Rows(0)("Title")
        hirerLabel.Text = ds.Tables(1).Rows(0)("Hirer")

        licenceNumTextBox.Text = ds.Tables(1).Rows(0)("LicenceNum")
        If Not String.IsNullOrEmpty(ds.Tables(1).Rows(0)("ExpiryDate")) Then
            expiryDateDateControl.Date = Utility.ParseDateTime(ds.Tables(1).Rows(0)("ExpiryDate"))
        Else
            expiryDateDateControl.Text = ""
        End If
        issuingAuthorityTextBox.Text = ds.Tables(1).Rows(0)("IssingAuthority")

        If Not String.IsNullOrEmpty(ds.Tables(1).Rows(0)("DOB")) Then
            dobDateControl.Date = Utility.ParseDateTime(ds.Tables(1).Rows(0)("DOB"))
        Else
            dobDateControl.Text = ""
        End If

        Dim dob As Date = dobDateControl.Date

        If Not dob = Nothing Then
            Dim age As Integer = Today.Year - dob.Year

            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                age -= 1
            End If
            ageLabel.Text = Utility.ParseInt(age)
        Else
            ageLabel.Text = ""
        End If

        vipCheckBox.Checked = ds.Tables(1).Rows(0)("Vip")
        fleetDriverCheckBox.Checked = ds.Tables(1).Rows(0)("fleetDriver")
        personTypeDropDownList.SelectedValue = ds.Tables(1).Rows(0)("PersonType")

        passportNoTextBox.Text = ds.Tables(1).Rows(0)("PassportNo")
        passportCountryTextBox.Text = ds.Tables(1).Rows(0)("PassportCountry")

        integrityNoLabel.Text = ds.Tables(1).Rows(0)("IntegrityNo")

        'Physical Address
        physicalAddressIdLabel.Text = ds.Tables(2).Rows(0)("AddId")
        physicalAddressCodIdLabel.Text = ds.Tables(2).Rows(0)("CodeId")
        physicalAddressIntegrityNoLabel.Text = ds.Tables(2).Rows(0)("IntegrityNo")
        physicalStreetTextBox.Text = ds.Tables(2).Rows(0)("Street")
        physicalSuburbTextBox.Text = ds.Tables(2).Rows(0)("Suburb")
        physicalCityTextBox.Text = ds.Tables(2).Rows(0)("CityTown")
        physicalStateTextBox.Text = ds.Tables(2).Rows(0)("State")
        physicalPostCodeTextBox.Text = ds.Tables(2).Rows(0)("Postcode")
        physicalCountryPickerControl.Text = ds.Tables(2).Rows(0)("Country")

        'Postal Address
        postalAddressIdLabel.Text = ds.Tables(3).Rows(0)("AddId")
        postalAddressCodIdLabel.Text = ds.Tables(3).Rows(0)("CodeId")
        postalAddressIntegrityNoLabel.Text = ds.Tables(3).Rows(0)("IntegrityNo")
        postalStreetTextBox.Text = ds.Tables(3).Rows(0)("Street")
        postalSuburbTextBox.Text = ds.Tables(3).Rows(0)("Suburb")
        postalCityTextBox.Text = ds.Tables(3).Rows(0)("CityTown")
        postalStateTextBox.Text = ds.Tables(3).Rows(0)("State")
        postalPostcodeTextBox.Text = ds.Tables(3).Rows(0)("Postcode")
        postalCountryPickerControl.Text = ds.Tables(3).Rows(0)("Country")

        'Contact Numbers
        dayCountryTextBox.Text = ds.Tables(1).Rows(0)("PhDayCtyCode")
        dayAreaTextBox.Text = ds.Tables(1).Rows(0)("PhDayAreaCode")
        dayNumberTextBox.Text = ds.Tables(1).Rows(0)("PhDayNumber")
        dayCodIdLabel.Text = ds.Tables(1).Rows(0)("PhDayCodId")
        dayIdLabel.Text = ds.Tables(1).Rows(0)("PhDayId")
        dayIntNumLabel.Text = ds.Tables(1).Rows(0)("PhDayIntNum")

        nightCountryTextBox.Text = ds.Tables(1).Rows(0)("PhNightCtyCode")
        nightAreaTextBox.Text = ds.Tables(1).Rows(0)("PhNightAreaCode")
        nightNumberTextBox.Text = ds.Tables(1).Rows(0)("PhNightNumber")
        nightCodIdLabel.Text = ds.Tables(1).Rows(0)("PhNightCodId")
        nightIdLabel.Text = ds.Tables(1).Rows(0)("PhNightId")
        nightIntNumLabel.Text = ds.Tables(1).Rows(0)("PhNightIntNum")

        mobileCountryTextBox.Text = ds.Tables(1).Rows(0)("MobileCtyCode")
        mobileAreaTextBox.Text = ds.Tables(1).Rows(0)("MobileAreaCode")
        mobileNumberTextBox.Text = ds.Tables(1).Rows(0)("MobileNumber")
        mobileCodIdLabel.Text = ds.Tables(1).Rows(0)("MobileCodId")
        mobileIdLabel.Text = ds.Tables(1).Rows(0)("MobileId")
        mobileIntNumLabel.Text = ds.Tables(1).Rows(0)("MobileIntNum")

        faxCountryTextBox.Text = ds.Tables(1).Rows(0)("FaxCtyCode")
        faxAreaTextBox.Text = ds.Tables(1).Rows(0)("FaxAreaCode")
        faxNumberTextBox.Text = ds.Tables(1).Rows(0)("FaxNumber")
        faxCodIdLabel.Text = ds.Tables(1).Rows(0)("FaxCodId")
        faxIdLabel.Text = ds.Tables(1).Rows(0)("FaxId")
        faxIntNumLabel.Text = ds.Tables(1).Rows(0)("FaxIntNum")

        emailTextBox.Text = ds.Tables(1).Rows(0)("Email")
        emailCodIdLabel.Text = ds.Tables(1).Rows(0)("EmailCodId")
        emailIdLabel.Text = ds.Tables(1).Rows(0)("EmailId")
        emailIntNumLabel.Text = ds.Tables(1).Rows(0)("EmailIntNum")

        'Marketing Data
        mailingListCheckBox.Checked = ds.Tables(1).Rows(0)("MailLists")
        surveysCheckBox.Checked = ds.Tables(1).Rows(0)("SurVeys")
        prefCommDropDownList.SelectedValue = ds.Tables(1).Rows(0)("PrefComm")
        genderDropDownList.SelectedValue = ds.Tables(1).Rows(0)("Gender")
        occupationTextBox.Text = ds.Tables(1).Rows(0)("Occupation")

        'Brochures
        brochuresGridView.DataSource = ds.Tables(5)
        brochuresGridView.DataBind()

    End Sub

#Region "Button Events"

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If dobDateControl.IsValid And expiryDateDateControl.IsValid Then
            updateCustomer()
            getCustomerDetail()
        Else
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
        End If

    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If dobDateControl.IsValid And expiryDateDateControl.IsValid Then
            updateCustomer()
            customerIdLabel.Text = ""
            customerId = ""
            getCustomerDetail()
        Else
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
        End If

    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        getCustomerDetail()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        deleteCustomer()

        customerIdLabel.Text = ""
        customerId = ""
        getCustomerDetail()
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=" & BookingTabIndex.Customer)

    End Sub

#End Region

    Private Sub updateCustomer()

        Dim isHirer As Boolean = hirerLabel.Text
        Dim action As String = ""
        Dim errorMessage As String = ""

        If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
            If String.IsNullOrEmpty(expiryDateDateControl.Text) Or String.IsNullOrEmpty(issuingAuthorityTextBox.Text) Or String.IsNullOrEmpty(dobDateControl.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, "You must enter Expiry Date, Issuing Authority and Date of Birth for licence details")
                Return
            End If
        End If

        If String.IsNullOrEmpty(rentalId) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN031"))
            Return
        End If

        If String.IsNullOrEmpty(dayIntNumLabel.Text) Then
            dayIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(nightIntNumLabel.Text) Then
            nightIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(mobileIntNumLabel.Text) Then
            mobileIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(faxIntNumLabel.Text) Then
            faxIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(emailIntNumLabel.Text) Then
            emailIntNumLabel.Text = "0"
        End If
        If String.IsNullOrEmpty(integrityNoLabel.Text) Then
            integrityNoLabel.Text = "0"
        End If

        'Check for Mandatory Fields
        If String.IsNullOrEmpty(lastNameTextBox.Text) Or String.IsNullOrEmpty(firstNameTextBox.Text) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
            Return
        End If

        If isHirer Then
            If String.IsNullOrEmpty(physicalCityTextBox.Text) Or String.IsNullOrEmpty(physicalCountryPickerControl.DataId) Or String.IsNullOrEmpty(physicalCityTextBox.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
                Return
            End If
        End If

        'validate the driver if the driver licence field is not empty
        If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
            If String.IsNullOrEmpty(dobDateControl.Text) Or String.IsNullOrEmpty(issuingAuthorityTextBox.Text) Or String.IsNullOrEmpty(expiryDateDateControl.Text) Then
                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN107"))
                Return
            End If
            ''the customer is driver

            errorMessage = ""
            errorMessage = BookingCustomer.ValidateDriver(rentalId, dobDateControl.Date, expiryDateDateControl.Date)

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return
            End If
        End If

        'Generate Customer ID
        If String.IsNullOrEmpty(customerId) Then
            customerId = Aurora.Common.Data.GetNewId()
            action = "I"
        Else
            action = "M"
        End If

        'Validating Phone Details by Executing stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(dayNumberTextBox.Text, "", dayIdLabel.Text, dayCodIdLabel.Text) Then
            Return
        End If

        'Validating night Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(nightNumberTextBox.Text, "", nightIdLabel.Text, nightCodIdLabel.Text) Then
            Return
        End If

        'Validating mobile Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(mobileNumberTextBox.Text, "", mobileIdLabel.Text, mobileCodIdLabel.Text) Then
            Return
        End If

        'Validating FAX Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber(faxNumberTextBox.Text, "", faxIdLabel.Text, faxCodIdLabel.Text) Then
            Return
        End If

        'Validating Email Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not validatePhoneNumber("", emailTextBox.Text, emailIdLabel.Text, emailCodIdLabel.Text) Then
            Return
        End If

        'Validating Physical Address details by Executing stored procedure "sp_validateAddress"
        If Not validateAdderss(physicalCountryPickerControl.DataId, physicalStateTextBox.Text, physicalAddressIdLabel.Text, physicalAddressCodIdLabel.Text) Then
            Return
        End If

        'Validating Postal Address details by Executing stored procedure "sp_validateAddress"
        If Not validateAdderss(postalCountryPickerControl.DataId, postalStateTextBox.Text, postalAddressIdLabel.Text, postalAddressCodIdLabel.Text) Then
            Return
        End If

        'Update customer details       
        errorMessage = ""
        errorMessage = BookingCustomer.UpdateCustomer(customerId, firstNameTextBox.Text, lastNameTextBox.Text, _
            vipCheckBox.Checked, fleetDriverCheckBox.Checked, personTypeDropDownList.SelectedValue, _
            titleTextBox.Text, dobDateControl.Date, licenceNumTextBox.Text, expiryDateDateControl.Date, _
            issuingAuthorityTextBox.Text, genderDropDownList.SelectedValue, occupationTextBox.Text, _
            prefCommDropDownList.SelectedValue, mailingListCheckBox.Checked, surveysCheckBox.Checked, _
            integrityNoLabel.Text, passportNoTextBox.Text, passportCountryTextBox.Text, rentalId, UserCode, UserCode, "CustomerMgt.aspx", "")
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

        'Updating Day Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(dayIdLabel.Text, dayCodIdLabel.Text, dayAreaTextBox.Text, dayCountryTextBox.Text, dayNumberTextBox.Text, dayIntNumLabel.Text, "") Then
            Return
        End If

        'Updating Night Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(nightIdLabel.Text, nightCodIdLabel.Text, nightAreaTextBox.Text, nightCountryTextBox.Text, nightNumberTextBox.Text, nightIntNumLabel.Text, "") Then
            Return
        End If

        'Updating Mobile Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(mobileIdLabel.Text, mobileCodIdLabel.Text, mobileAreaTextBox.Text, mobileCountryTextBox.Text, mobileNumberTextBox.Text, mobileIntNumLabel.Text, "") Then
            Return
        End If

        'Updating Fax Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(faxIdLabel.Text, faxCodIdLabel.Text, faxAreaTextBox.Text, faxCountryTextBox.Text, faxNumberTextBox.Text, faxIntNumLabel.Text, "") Then
            Return
        End If

        'Updating Email details by Executing stored procedure "sp_updatePhoneNumber"
        If Not updatePhoneNumber(emailIdLabel.Text, emailCodIdLabel.Text, "", "", "", emailIntNumLabel.Text, emailTextBox.Text) Then
            Return
        End If

        ''Updating Physical Address details by Executing stored procedure "sp_updateAddress"
        If Not updateAddress(physicalAddressIdLabel.Text, physicalStreetTextBox.Text, physicalAddressCodIdLabel.Text, _
            customerId, physicalSuburbTextBox.Text, physicalPostCodeTextBox.Text, physicalCountryPickerControl.DataId, _
            physicalCityTextBox.Text, physicalStateTextBox.Text, physicalAddressIntegrityNoLabel.Text) Then
            Return
        End If

        ''Updating Postal Address details by Executing stored procedure "sp_updateAddress"
        If Not updateAddress(postalAddressIdLabel.Text, postalStreetTextBox.Text, postalAddressCodIdLabel.Text, _
                   customerId, postalSuburbTextBox.Text, postalPostcodeTextBox.Text, postalCountryPickerControl.DataId, _
                   postalCityTextBox.Text, postalStateTextBox.Text, postalAddressIntegrityNoLabel.Text) Then
            Return
        End If

        'Update traveller details
        Dim isDriver As Boolean = False
        If Not String.IsNullOrEmpty(licenceNumTextBox.Text) Then
            isDriver = True
        End If

        errorMessage = ""
        errorMessage = BookingCustomer.UpdateTraveller(customerId, rentalId, isDriver, isHirer, False, UserCode, UserCode, "CustomerMgt.aspx")
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If


        ' create Costomer Brochdure record
        For Each r As GridViewRow In brochuresGridView.Rows
            Dim qtyTextBox As TextBox
            qtyTextBox = r.FindControl("qtyTextBox")

            Dim qty As Integer = 0
            qty = Utility.ParseInt(qtyTextBox.Text, 0)

            If qty > 0 Then

                Dim brhIdLabel As Label
                brhIdLabel = r.FindControl("brhIdLabel")

                errorMessage = ""
                errorMessage = BookingCustomer.UpdateCustomerBrochure(brhIdLabel.Text, customerId, qty, UserCode, UserCode, "CustomerMgt.aspx")

                If Not String.IsNullOrEmpty(errorMessage) Then
                    SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                    Return
                End If

            End If

        Next

        'Set success message
        If action = "I" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
        ElseIf action = "M" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        End If

    End Sub

    Private Function validatePhoneNumber(ByVal number As String, ByVal email As String, ByVal phoneId As String, ByVal codId As String) As Boolean

        Dim action As String
        If Not String.IsNullOrEmpty(number) Then
            If String.IsNullOrEmpty(phoneId) Then
                action = "I"
            Else
                action = "M"
            End If

            Dim errorMessage As String = ""
            errorMessage = BookingCustomer.ValidatePhoneNumber(action, "Customer", customerId, codId, number, email)

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return False
            End If
        End If
        Return True
    End Function

    Private Function validateAdderss(ByVal country As String, ByVal state As String, ByVal addressId As String, ByVal codId As String) As Boolean
        Dim action As String = ""

        If String.IsNullOrEmpty(addressId) Then
            action = "I"
        Else
            action = "M"
        End If

        Dim errorMessage As String = ""
        errorMessage = BookingCustomer.ValidateAddress(action, "Customer", customerId, codId, country, state)

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, errorMessage)
            Return False
        End If
        Return True
    End Function

    Private Function updatePhoneNumber(ByVal phoneId As String, ByVal codId As String, ByVal areaCode As String, _
        ByVal cityCode As String, ByVal number As String, ByVal integrityNo As String, ByVal email As String) As Boolean

        Dim action As String = ""
        If String.IsNullOrEmpty(phoneId) Then
            action = "I"
        Else
            action = "M"
        End If

        Dim errorMessage As String = ""
        errorMessage = BookingCustomer.UpdatePhoneNumber(action, phoneId, "Customer", customerId, codId, _
            areaCode, cityCode, number, integrityNo, email, UserCode, "CustomerMgt.aspx")

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If
        Return True
    End Function

    Private Function updateAddress(ByVal addressId As String, ByVal street As String, _
        ByVal codeId As String, ByVal customerId As String, ByVal suburb As String, ByVal postCode As String, _
        ByVal countryCode As String, ByVal city As String, ByVal stateCode As String, ByVal integrityNo As String) As Boolean

        Dim active As String = ""
        If String.IsNullOrEmpty(addressId) Then
            active = "I"
        Else
            active = "M"
        End If

        'oAuroraDAL.appendParameter("QueryAction", 200, 2, QueryAction)
        'oAuroraDAL.appendParameter("AddId", 200, 64, physAddrAddId)
        'oAuroraDAL.appendParameter("ConPrntTableName", 200, 64, physAddrTableName)
        'oAuroraDAL.appendParameter("AddAddress1", 200, 64, physAddrStreet)
        'oAuroraDAL.appendParameter("AddCodTypeId", 200, 64, physAddrCodeId)
        'oAuroraDAL.appendParameter("AddPrntID", 200, 64, CusId)
        'oAuroraDAL.appendParameter("AddAddress2", 200, 64, physAddrSuburb)
        'oAuroraDAL.appendParameter("AddPostcode", 200, 15, physAddrPostcode)
        'oAuroraDAL.appendParameter("AddCtyCode", 200, 12, physAddrCountry)
        'oAuroraDAL.appendParameter("AddAddress3", 200, 64, physAddrCityTown)
        'oAuroraDAL.appendParameter("AddStaCode", 200, 12, physAddrState)
        'oAuroraDAL.appendParameter("iIntegrityNo", 3, 4, physAddrIntegrityNo)
        'oAuroraDAL.appendParameter("AddModUsrId", 200, 64, sUsrId)
        'oAuroraDAL.appendParameter("AddPrgmName", 200, 64, sProgname)
        'oAuroraDAL.appendParameter("ReturnError", 200, 500, "")

        Dim errorMessage As String = ""

        errorMessage = BookingCustomer.UpdateAddress(active, addressId, "Customer", street, codeId, customerId, suburb, _
            postCode, countryCode, city, stateCode, integrityNo, UserCode, "CustomerMgt.aspx")

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return False
        End If
        Return True

    End Function

    Private Sub deleteCustomer()

        ''the customer must associate with rental, if it isn't there return

        'validate the if any rental associates with the customer
        Dim errorMessage As String = ""
        errorMessage = ""
        errorMessage = BookingCustomer.ValidateDeletion(customerId, rentalId)
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

        ' delete the customer and associated tables
        ' validate the if any rental associates with the customer
        errorMessage = ""
        errorMessage = BookingCustomer.DeleteCustomer(customerId)
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

    End Sub

End Class
