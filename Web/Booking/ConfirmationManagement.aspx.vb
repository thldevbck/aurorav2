'' Change Log
'' 5.6.8 / Shoel - Fixes for Issue 1 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=1)
'' 10.6.8 / Shoel - Fixes for Issue 30 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=30)  - Save Exception
'' 10.6.8 / Shoel - The page no longer uses Session variables - everything in the Viewstate 
'' 30.7.8 / Shoel - Booking Number displayed instead of Booking Id
'' 30.7.8 / Shoel - Selection of Rental combo fixed
'' 14.8.8 / Shoel - Changed the page Function code to fix the "access denied" prob on sys test
'' 17.9.8 / Shoel - Fixed issue wherein multiple rentals had a problem with ALL combo 
'' 6.11.8 / Shoel - Added HTML Encode/Decode for Header & Footer Notes
'' 13.1.9 / Shoel - Mega fix for a forgotten functionality!
'' 15.10.12/ Manny - Do massive cleanup for confirmations stuffs
'' 11.12.13/ Manny - Added fixes for Job number is 38709.
'' 06.01.14/ Manny - fixing problem on the returning value 'ALL'

Imports System.Xml
Imports System.Xml.Xsl
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

<AuroraPageTitleAttribute("Confirmation Management")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN045,GEN046")> _
Partial Class Booking_ConfirmationManagement
    Inherits AuroraPage

    Public Const VIEWSTATE_CONFIRMATIONID As String = "ViewState_ConfMgmt_ConfId"
    Public Const VIEWSTATE_BOOKINGID As String = "ViewState_ConfMgmt_BooId"
    Public Const VIEWSTATE_INTEGRITYNO As String = "ViewState_ConfMgmt_IntegrityNo"

    Public Const VIEWSTATE_RENTALID As String = "ViewState_ConfMgmt_RentalId"
    Public Const VIEWSTATE_RENTALNO As String = "ViewState_ConfMgmt_RentalNo"
    Public Const VIEWSTATE_AUDIENCETYPE As String = "ViewState_ConfMgmt_AudienceType"

    Public Const VIEWSTATE_OLD_RENTALID As String = "ViewState_ConfMgmt_OldRentalID"
    Public Const VIEWSTATE_COUNTRIESDATA As String = "ViewState_ConfMgmt_CountriesData"
    Public Const THIS_PAGENAME As String = "ConfirmationManagement.aspx"

    Public Const VIEWSTATE_BOOKINGNUM As String = "ViewState_ConfMgmt_BookingNum"
    Public Const HEADER_EXP_MESSAGE As String = "Personal Note Header cannot be more than 1000 characters in length."
    Public Const FOOTER_EXP_MESSAGE As String = "Personal Note Footer cannot be more than 1000 characters in length."

    Const SORTED_FIELDS As String = "Selec DESC" '' "NtsOrder, noteType"
    Const ALL_BOOKINGS As String = "All"
    Const DEFAULT_RENTAL As String = "Displaying default Rental # "
    Const UNCHECKALL_RENTAL As String = "Uncheck All"
    Const CHECKALL_RENTAL As String = "Check All"
    Const RENTALID_MANDATORY As String = "RentalId is mandatory."


    Public Property DualCountryCreateOrUpdateStatus() As String
        Get
            If ViewState("ViewState_ConfMgmt_DualCountryCreateOrUpdateStatus") Is Nothing Then
                Return String.Empty
            Else
                Return CStr(ViewState("ViewState_ConfMgmt_DualCountryCreateOrUpdateStatus")).ToUpper().Trim()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ViewState_ConfMgmt_DualCountryCreateOrUpdateStatus") = value.ToUpper().Trim()
        End Set
    End Property

    Public Property PassedRentalNumber() As String
        Get
            Return CStr(ViewState("CNFMGT_PassedRntNo"))
        End Get
        Set(ByVal value As String)
            ViewState("CNFMGT_PassedRntNo") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        hiddenFlagForPostBack.Value = IIf(String.IsNullOrEmpty(hiddenFlagForPostBack.Value), "True", hiddenFlagForPostBack.Value)

        If Not IsPostBack Then
            ViewState(VIEWSTATE_BOOKINGID) = CStr(Request("BookingId"))
            ViewState(VIEWSTATE_CONFIRMATIONID) = CStr(Request("ConfirmationId"))
            ViewState(VIEWSTATE_BOOKINGNUM) = CStr(Request("BookingNum"))

            PassedRentalNumber = CStr(Request("RentalNo"))
            If PassedRentalNumber = "undefined" Then
                PassedRentalNumber = ""
            End If

            btnBack.Attributes.Add("OnClick", "return GoBackToBookingsPage('" & CStr(Request("BookingId")) & "','" & CStr(Request("RentalId")) & "')")
            btnBack2.Attributes.Add("OnClick", "return GoBackToBookingsPage('" & CStr(Request("BookingId")) & "','" & CStr(Request("RentalId")) & "')")

            ViewState(VIEWSTATE_OLD_RENTALID) = CStr(Request("RentalId"))

            LoadConfirmationPage(ViewState(VIEWSTATE_CONFIRMATIONID))
            'i will need the Boo and Rnt Ids to go back!
            labelRentalCheckall.Text = CHECKALL_RENTAL ''IIf(txtrentals.Value = ALL_BOOKINGS, UNCHECKALL_RENTAL, CHECKALL_RENTAL)

        End If



    End Sub


    ''rev:mia jan 8 2009
    Sub LoadConfirmationPage(Optional ByVal ConfirmationId As String = "")
        ' If ConfirmationId <> "" Then
        'old confirmation

        Dim xmlMain As XmlDocument
        Dim dstTemp As DataSet = New DataSet
        Dim dtCountries, dtRentals, dtAudienceType As DataTable

        xmlMain = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(ViewState(VIEWSTATE_BOOKINGID), ConfirmationId)

        dstTemp.ReadXml(New XmlNodeReader(xmlMain.DocumentElement.SelectSingleNode("Countries")))
        dtCountries = dstTemp.Tables(0)

        dstTemp = New DataSet
        dstTemp.ReadXml(New XmlNodeReader(xmlMain.DocumentElement.SelectSingleNode("Rentals")))
        dtRentals = dstTemp.Tables(0)

        dstTemp = New DataSet
        dstTemp.ReadXml(New XmlNodeReader(xmlMain.DocumentElement.SelectSingleNode("audienceType")))
        dtAudienceType = dstTemp.Tables(0)


        cmbAudience.DataSource = dtAudienceType
        cmbAudience.DataValueField = "CodId"
        cmbAudience.DataTextField = "CodCode"
        cmbAudience.DataBind()

        ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
        cmbRental.DataSource = dtRentals
        cmbRental.DataValueField = "rentId"
        cmbRental.DataTextField = "rentNum"
        cmbRental.DataBind()
        Dim liNewListItem As ListItem = New ListItem(ALL_BOOKINGS, ALL_BOOKINGS)
        cmbRental.Items.Insert(0, liNewListItem)

        cblRentals.DataSource = dtRentals
        cblRentals.DataValueField = "rentId"
        cblRentals.DataTextField = "rentNum"
        cblRentals.DataBind()

        lblBooking.Text = ViewState(VIEWSTATE_BOOKINGNUM)

        chkHighlightNote.Checked = CBool(xmlMain.DocumentElement.SelectSingleNode("Confirmations/highlight").InnerText)

        txaHeader.Text = Server.HtmlDecode(xmlMain.DocumentElement.SelectSingleNode("Confirmations/headnote").InnerText)
        txaFooter.Text = Server.HtmlDecode(xmlMain.DocumentElement.SelectSingleNode("Confirmations/footnote").InnerText)

        cmbAudience.SelectedValue = xmlMain.DocumentElement.SelectSingleNode("Confirmations/audType").InnerText

        If xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentNum").InnerText.ToUpper().Trim().ToUpper = ALL_BOOKINGS.ToUpper Then
            txtrentals.Value = ALL_BOOKINGS

            For Each liitem As ListItem In cblRentals.Items
                If liitem.Selected = False Then
                    liitem.Selected = True
                End If

            Next
        ElseIf xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentNum").InnerText.ToUpper().Trim().Contains(",") = True Then
            txtrentals.Value = xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentNum").InnerText.ToUpper().Trim()

            For Each item As String In txtrentals.Value.Split(",")
                For Each liitem As ListItem In cblRentals.Items
                    If liitem.Text = item Then
                        liitem.Selected = True
                    End If
                Next
            Next


        Else
            If Not xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText.Trim().Equals(String.Empty) Then
                cblRentals.SelectedValue = xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText
                txtrentals.Value = CStr(xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText.Split("-")(1))
            Else
                Try
                    txtrentals.Value = CStr(Request("RentalId").ToString.Split("-")(1))
                    cblRentals.SelectedIndex = cblRentals.Items.IndexOf(cblRentals.Items.FindByText(txtrentals.Value))
                Catch ex As Exception
                    txtrentals.Value = ALL_BOOKINGS
                End Try

            End If
        End If

        ' fix for improper rental selection

        ViewState(VIEWSTATE_INTEGRITYNO) = xmlMain.DocumentElement.SelectSingleNode("Confirmations/integrityno").InnerText

        ''rev:mia jan6,2009 - get the ActiveRentalCountry querystring
        Dim ActiveRentalCountry As String = dtCountries.Rows(0)(0)
        If Not Page.IsPostBack Then
            If Request.QueryString("ActiveRentalCountry") IsNot Nothing Then
                ActiveRentalCountry = Request.QueryString("ActiveRentalCountry")
            End If
            ''rev:mia 09-March-2016 https://thlonline.atlassian.net/browse/AURORA-777
            If (String.IsNullOrEmpty(ActiveRentalCountry)) Then
                ActiveRentalCountry = dtCountries.Rows(0)(0)
            End If
        Else
            ActiveRentalCountry = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(cblRentals.SelectedValue)
        End If

        If Not ActiveRentalCountry.Trim.Equals(String.Empty) Then
            'Dim newtable As DataTable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec(ActiveRentalCountry, ViewState(VIEWSTATE_CONFIRMATIONID), UserCode)
            'Dim dview As DataView = newtable.DefaultView
            'dview.RowFilter = GetViewParameters()
            'If (dview.Count = 0) Then
            '    dview.RowFilter = GetViewParameters(True)
            'End If
            'dview.Sort = SORTED_FIELDS
            'gwMain.DataSource = dview
            'gwMain.DataBind()
            UpdateGrids(ActiveRentalCountry)
        End If

        'read only part
        If ConfirmationId = "" Then
            'new record
            cmbAudience.Enabled = True
        Else
            'old record
            cmbAudience.Enabled = False
        End If

        'for new record there is no country in the xmlmain

        If xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText <> "" Then
            'old record
            SelectCountryForRental(xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText, dtCountries, False)
        Else
            SelectCountryForRental(ViewState(VIEWSTATE_OLD_RENTALID), dtCountries, False)
        End If




        ViewState(VIEWSTATE_AUDIENCETYPE) = cmbAudience.SelectedValue
        If (cblRentals.SelectedValue <> "") Then

            RemoveTrailingComma()
            Dim rentalselection As RentalSelectionEnum = MapSelectionToEnum()

            If rentalselection = RentalSelectionEnum.All Then
                ViewState(VIEWSTATE_RENTALID) = ALL_BOOKINGS
                ViewState(VIEWSTATE_RENTALNO) = ALL_BOOKINGS
            ElseIf rentalselection = RentalSelectionEnum.ManySelection Then
                ''do nothing
            Else
                ViewState(VIEWSTATE_RENTALID) = cblRentals.SelectedValue
                ViewState(VIEWSTATE_RENTALNO) = cblRentals.SelectedItem.Text
            End If

        Else
            ViewState(VIEWSTATE_RENTALID) = ALL_BOOKINGS
            ViewState(VIEWSTATE_RENTALNO) = ALL_BOOKINGS
        End If
        ViewState(VIEWSTATE_COUNTRIESDATA) = dtCountries

        'hack for View button
        SetViewButtonJavaScript()

    End Sub


    Sub SelectRental(ByVal RentalNo As String)
        Try
            For i As Int16 = 0 To cmbRental.Items.Count - 1
                If cmbRental.Items(i).Text = RentalNo Then
                    cmbRental.ClearSelection()
                    cmbRental.Items(i).Selected = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            'do nothing
        End Try

    End Sub


    Protected Sub gwMain_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwMain.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(4).Text.Trim() = "1" Then
                e.Row.Cells(4).Text = "H"
            ElseIf e.Row.Cells(4).Text.Trim() = "0" Then
                e.Row.Cells(4).Text = "F"
            End If

            ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand.
            ''As part of the changes, whatever brands defined in the confirmation setup will set a default check in 
            ''the items checkobx
            Dim defaultcolor As System.Drawing.Color = e.Row.BackColor

            Dim chkChecked As CheckBox = DirectCast(e.Row.FindControl("chkChecked"), CheckBox)
            Dim labelbrand As Label = DirectCast(e.Row.FindControl("labelbrand"), Label)
            Dim brandName As String = ""
            Dim tobechecked As Boolean = False

            If (Not chkChecked Is Nothing) Then
                Try
                    If (Not DataBinder.Eval(e.Row.DataItem, "NtsBrdCode") Is DBNull.Value) Then
                        Dim NtsBrdCode As String = DataBinder.Eval(e.Row.DataItem, "NtsBrdCode")
                        NtsBrdCode = NtsBrdCode.Trim


                        ''if its first load
                        If (hiddenFlagForPostBack.Value = "True") Then

                            ''its a generic and no brand assigned
                            If (String.IsNullOrEmpty(NtsBrdCode)) Then
                                chkChecked.Checked = False
                            Else
                                ''looked for brand assigned
                                chkChecked.Checked = IIf(_brand.ToUpper.Contains(NtsBrdCode.ToUpper) = True, True, False)
                            End If

                            If (chkChecked.Checked = True) Then
                                e.Row.BackColor = Drawing.Color.AntiqueWhite
                            Else

                                ''this is a generic
                                If (String.IsNullOrEmpty(NtsBrdCode)) Then
                                    e.Row.BackColor = defaultcolor
                                Else
                                    e.Row.Visible = False
                                End If
                            End If

                        Else

                            ''this is a generic
                            Dim selec As String = ""
                            If (Not DataBinder.Eval(e.Row.DataItem, "selec") Is DBNull.Value) Then
                                selec = DataBinder.Eval(e.Row.DataItem, "selec").ToString
                            End If

                            If (String.IsNullOrEmpty(selec)) Then
                                chkChecked.Checked = False
                            Else
                                chkChecked.Checked = IIf(selec = "1", True, False)
                            End If




                            If (String.IsNullOrEmpty(NtsBrdCode)) Then
                                ''do nothing                                
                            Else
                                tobechecked = IIf(_brand.ToUpper.Contains(NtsBrdCode.ToUpper) = True, True, False)
                                ''i assume that its different brand
                                If (tobechecked = False) Then
                                    e.Row.Visible = False
                                Else
                                    e.Row.BackColor = Drawing.Color.AntiqueWhite
                                End If
                            End If

                        End If

                        brandName = getBrandName(NtsBrdCode)

                        labelbrand.Text = brandName
                    Else
                        e.Row.ToolTip = "Generic"
                        labelbrand.Text = String.Empty
                    End If
                Catch ex As Exception

                End Try

            End If

        ElseIf e.Row.RowType = DataControlRowType.Header Then
            'header row
            CType(e.Row.Cells(1).FindControl("chkCheckAll"), CheckBox).Attributes.Add("OnClick", "SelectAllCheck();")
        End If
    End Sub

    Protected Sub gwMain_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwMain.RowCreated
        e.Row.Cells(0).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave2.Click
        SaveConfirmationData()
    End Sub

    Sub SaveConfirmationData(Optional ByVal ActionCode As String = "U")

        ' look at personal notes
        If txaHeader.Text.Length > 1000 Then
            txaHeader.Text = txaHeader.Text.Substring(0, 1000)
            SetWarningShortMessage(HEADER_EXP_MESSAGE)
            'Exit Sub
        End If

        If txaFooter.Text.Length > 1000 Then
            txaFooter.Text = txaFooter.Text.Substring(0, 1000)
            SetWarningShortMessage(FOOTER_EXP_MESSAGE)
            'Exit Sub
        End If
        ' look at personal notes

        'this is the notes part
        Dim xmlNotesData As New XmlDocument
        xmlNotesData.LoadXml(BuildNotesXMLString())

        Dim strConfId As String = ""

        'the main screen part'

        '                               @sCfnId,                            @sBooId,        @sRntId,@sRntNum,@sAudType,                         @sHead, @sFoot, @sEmail,    @sFax,      @sOutputType,   @sFileName,@bHighlight,@IntegrityNo,@sAdduserId,@sAddModuserId,@sAddProgramName,        @sAction
        'exec cfn_update_confirmations 'B2C1CD60-0F88-48D9-B031-C0DE90111617', '6353412', '6353412-1', '1', '34CE97B1-5807-41C5-B043-5B10D43685C7', '',  '',        '',         '',     '',             '',         0,          3,          'sp7',      'sp7',          'confirmationlistener.asp', 'U'
        If CStr(ViewState(VIEWSTATE_INTEGRITYNO)) = String.Empty Then
            ViewState(VIEWSTATE_INTEGRITYNO) = 1
            ActionCode = "I"
            strConfId = System.Guid.NewGuid().ToString()
        Else
            strConfId = CStr(ViewState(VIEWSTATE_CONFIRMATIONID)) ' bug fixed here! was reading the integrity no for some reason!
        End If

        Dim xmlReturn As XmlDocument = New XmlDocument

        ' check for dual country rental
        If DualCountryCreateOrUpdateStatus <> "UPDATE" Then
            ''rev:mia Sept 2 2013 -    this will allow confirmation email to be  process by adding multiple checkboxes
            ''If cmbRental.SelectedValue = "All" And CType(ViewState(VIEWSTATE_COUNTRIESDATA), DataTable).Rows.Count > 1 Then
            If txtrentals.Value = ALL_BOOKINGS And CType(ViewState(VIEWSTATE_COUNTRIESDATA), DataTable).Rows.Count > 1 Then
                DualCountryCreateOrUpdateStatus = "UPDATE"
                ViewState(VIEWSTATE_INTEGRITYNO) = 0
                ActionCode = "I"
            Else
                '' ViewState(VIEWSTATE_INTEGRITYNO) = CInt(ViewState(VIEWSTATE_INTEGRITYNO)) + 1
            End If
        ElseIf DualCountryCreateOrUpdateStatus = "UPDATE" Then

            If ViewState(VIEWSTATE_INTEGRITYNO) = 0 Then
                ViewState(VIEWSTATE_INTEGRITYNO) = 1
            Else
                ViewState(VIEWSTATE_INTEGRITYNO) = CInt(ViewState(VIEWSTATE_INTEGRITYNO)) + 1
            End If
        End If

        Dim isMultiRentals As Boolean = False
        Dim bookingId As String = ViewState(VIEWSTATE_BOOKINGID)
        Dim rentidValue As String = String.Empty
        Dim rentidText As String = String.Empty

        Dim isMultiple As Boolean = IIf(Request("bookingnum").Contains(","), True, False)

        Dim rentalselection As RentalSelectionEnum = RentalSelectionCount()
        If rentalselection = RentalSelectionEnum.ManySelection Then
            isMultiRentals = True
            ViewState(VIEWSTATE_RENTALID) = ""
            ViewState(VIEWSTATE_RENTALNO) = ""
            ViewState(VIEWSTATE_CONFIRMATIONID) = ""
            For Each de As DictionaryEntry In RentalSelectionContent()

                rentidValue = de.Value.ToString
                rentidText = de.Key.ToString
                ''rev:mia Nov.14, 2013 - Record Deleted by User fixing
                ''rev:mia dec.24,2013 - fixing..modified sp: cfn_update_confirmations
                If (ViewState(VIEWSTATE_INTEGRITYNO) = 1 And ActionCode = "I") Then
                    strConfId = System.Guid.NewGuid().ToString()
                Else

                    Dim confirmId As String = ""
                    confirmId = Aurora.Booking.Services.BookingConfirmation.GetConfirmationId(Request("bookingnum"), Request("bookingid"), rentidText)
                    If (confirmId <> "ERROR" And Not String.IsNullOrEmpty(confirmId)) Then
                        strConfId = confirmId
                    Else
                        If (Not isMultiple) Then
                            strConfId = System.Guid.NewGuid().ToString()
                            ActionCode = "I"
                        Else
                            SetErrorShortMessage("Cannot update the this Confirmation")
                            Exit Sub
                        End If

                    End If
                End If
                System.Diagnostics.Debug.WriteLine("ConfirmationID: " & strConfId & vbCrLf & "RentalID : " & rentidText & vbCrLf & "Rental Value: " & rentidValue & vbCrLf & "Integrity: " & ViewState(VIEWSTATE_INTEGRITYNO) & vbCrLf & "ActionCode:" & ActionCode)

                ViewState(VIEWSTATE_RENTALID) = ViewState(VIEWSTATE_RENTALID) & rentidValue & ","
                ViewState(VIEWSTATE_RENTALNO) = ViewState(VIEWSTATE_RENTALNO) & rentidText & ","
                ViewState(VIEWSTATE_CONFIRMATIONID) = ViewState(VIEWSTATE_CONFIRMATIONID) & strConfId & ","
                xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation(strConfId, _
                                                                                   bookingId, _
                                                                                   rentidValue, _
                                                                                   txtrentals.Value, _
                                                                                   cmbAudience.SelectedValue, _
                                                                                   Server.HtmlEncode(txaHeader.Text), _
                                                                                   Server.HtmlEncode(txaFooter.Text), _
                                                                                   "", _
                                                                                   "", _
                                                                                   "", _
                                                                                   "", _
                                                                                   True, _
                                                                                   CInt(ViewState(VIEWSTATE_INTEGRITYNO)), _
                                                                                   UserCode, _
                                                                                   UserCode, _
                                                                                   THIS_PAGENAME, _
                                                                                   ActionCode, _
                                                                                   xmlNotesData, _
                                                                                   "", _
                                                                                   "")

                If xmlReturn.DocumentElement.SelectSingleNode("Error") Is Nothing Then
                Else
                    Exit For
                End If


            Next
        Else
            If rentalselection = RentalSelectionEnum.All Or cblRentals.SelectedValue = "" Then
                rentidValue = ALL_BOOKINGS
                rentidText = ALL_BOOKINGS
            Else
                rentidValue = cblRentals.SelectedValue
                rentidText = cblRentals.SelectedItem.Text
            End If


            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation(strConfId, _
                                                                           ViewState(VIEWSTATE_BOOKINGID), _
                                                                           rentidValue, _
                                                                           rentidText, _
                                                                           cmbAudience.SelectedValue, _
                                                                           Server.HtmlEncode(txaHeader.Text), _
                                                                           Server.HtmlEncode(txaFooter.Text), _
                                                                           "", _
                                                                           "", _
                                                                           "", _
                                                                           "", _
                                                                           True, _
                                                                           CInt(ViewState(VIEWSTATE_INTEGRITYNO)), _
                                                                           UserCode, _
                                                                           UserCode, _
                                                                           THIS_PAGENAME, _
                                                                           ActionCode, _
                                                                           xmlNotesData, _
                                                                           "", _
                                                                           "")

            ViewState(VIEWSTATE_RENTALID) = rentidValue
            ViewState(VIEWSTATE_RENTALNO) = rentidText
            ViewState(VIEWSTATE_CONFIRMATIONID) = strConfId
        End If


        If xmlReturn.DocumentElement.SelectSingleNode("Error") Is Nothing Then
            ' no error
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ''ViewState(VIEWSTATE_CONFIRMATIONID) = strConfId

            If DualCountryCreateOrUpdateStatus = "UPDATE" Then
                cmbAudience.Enabled = False
            Else
                hiddenFlagForPostBack.Value = "False"
                ''  LoadConfirmationPage(strConfId)
                ''todo: 
            End If
            cblRentals.Enabled = False
            pnlRentals.Enabled = False
            chkRentalCheckall.Enabled = False
            Try
                If rentalselection = RentalSelectionEnum.ManySelection Then
                    For Each item As ListItem In cblRentals.Items
                        item.Selected = True
                    Next
                    For Each de As DictionaryEntry In FinalUnselectedRental()
                        cblRentals.Items(cblRentals.Items.IndexOf(cblRentals.Items.FindByText(de.Key.ToString))).Selected = False
                    Next
                End If
            Catch ex As Exception
            End Try



        Else
            ' error
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
            Exit Sub
        End If

        ViewState(VIEWSTATE_AUDIENCETYPE) = cmbAudience.SelectedValue
        SetViewButtonJavaScript()

    End Sub

    Function BuildNotesXMLString() As String
        'builds the monstrosity to save the xml
        Dim sbXMLToSave As New StringBuilder

        ''If (cmbRental.SelectedItem.Text = "All") Then
        If (txtrentals.Value = ALL_BOOKINGS) Then
            sbXMLToSave.Append("<Data check='ALL'>")
        Else
            sbXMLToSave.Append("<Data check='SINGLE'>")
        End If

        Dim ctrItem As Integer = cblRentals.Items.Count - 1 ''cmbRental.Items.Count - 1
        Dim ctr As Integer = 0
        For i As Integer = 0 To gwMain.Rows.Count - 1

            Dim chkBox As CheckBox = CType(gwMain.Rows(i).Cells(1).FindControl("chkChecked"), CheckBox)

            sbXMLToSave.Append("<NoteSpec>")
            sbXMLToSave.Append("<Action>")
            sbXMLToSave.Append(IIf(chkBox.Checked, "I", "D"))
            sbXMLToSave.Append("</Action>")


            sbXMLToSave.Append("<NoteId>")
            sbXMLToSave.Append(gwMain.Rows(i).Cells(0).Text)
            sbXMLToSave.Append("</NoteId>")



            sbXMLToSave.Append("<NoteCfnBrand>")
            If chkBox.Checked Then
                Dim tooltip As String = gwMain.Rows(i).ToolTip
                If (tooltip.Contains("Generic") = True) Then
                    ''do nothing
                Else
                    If (tooltip.Contains("/") = True) Then
                        tooltip = tooltip.Split("/")(1)
                        sbXMLToSave.Append(tooltip)
                    End If
                End If
            Else
                ''do nothing
            End If

            sbXMLToSave.Append("</NoteCfnBrand>")
            sbXMLToSave.Append("</NoteSpec>")
        Next

        sbXMLToSave.Append("</Data>")
        Return sbXMLToSave.ToString()
    End Function


    Sub ShowConfirmation()
        'call db with the same
        Dim xmlReturnDoc As XmlDocument

        '' If cmbRental.SelectedValue = "All" Then
        If txtrentals.Value = ALL_BOOKINGS Then
            xmlReturnDoc = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(ViewState(VIEWSTATE_CONFIRMATIONID), True)
        Else
            xmlReturnDoc = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(ViewState(VIEWSTATE_CONFIRMATIONID), False)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel2.Click
        hiddenFlagForPostBack.Value = ""
        LoadConfirmationPage(ViewState(VIEWSTATE_CONFIRMATIONID))
    End Sub

    Sub PopulateCountryOfTravelCombo(ByVal CountriesTable As DataTable, ByVal CountryCode As String, ByVal CountryCanBeChanged As Boolean)
        cmbCountryOfTravel.Items.Clear()
        For i As Integer = 0 To CountriesTable.Rows.Count - 1
            Dim sCountryCode, sCountryName As String

            sCountryCode = CountriesTable.Rows(i)("CtyCode")
            sCountryName = CountriesTable.Rows(i)("CtyName")

            Dim oNewItem As ListItem = New ListItem(sCountryName, sCountryCode)

            cmbCountryOfTravel.Items.Add(oNewItem)
        Next

        ''rev:mia jan 7 2009
        If cmbCountryOfTravel.Items.Count = 1 Then
            cmbCountryOfTravel.Items(0).Selected = True
        Else
            If CountryCode = "NZ" Then
                cmbCountryOfTravel.Items(1).Selected = True
            Else
                cmbCountryOfTravel.Items(0).Selected = True
            End If

        End If

        ''If cmbRental.SelectedValue = "All" Then
        If txtrentals.Value = ALL_BOOKINGS Then
            cmbCountryOfTravel.Enabled = True
        Else
            If Not CountryCanBeChanged Then
                cmbCountryOfTravel.Enabled = False
            Else
                cmbCountryOfTravel.Enabled = True
            End If
        End If

    End Sub

    Sub SelectCountryForRental(ByVal RentalId As String, ByVal CountriesTable As DataTable, ByVal CountryCanBeChanged As Boolean)
        Dim sCountryCode As String = ""
        ''If cmbRental.SelectedItem.Text <> "All" Then
        If txtrentals.Value = ALL_BOOKINGS Then
            sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(RentalId)
        Else
            If cmbCountryOfTravel.Items.Count = 1 Then
                If cmbCountryOfTravel.SelectedItem.Text = "Australia" Then
                    sCountryCode = "AU"
                End If
                If cmbCountryOfTravel.SelectedItem.Text = "New Zealand" Then
                    sCountryCode = "NZ"
                End If
            Else
                ''sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(RentalId)
                sCountryCode = "AU"
            End If
        End If

        If sCountryCode <> "" Then
            PopulateCountryOfTravelCombo(CountriesTable, sCountryCode, CountryCanBeChanged)
        End If
    End Sub

    Protected Sub cmbRental_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbRental.SelectedIndexChanged
        SelectCountryForRental(cmbRental.SelectedValue, CType(ViewState(VIEWSTATE_COUNTRIESDATA), DataTable), False)
        SetViewButtonJavaScript()

        ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
        _brand = getFilteredBrands()

        Dim newtable As DataTable = Nothing
        Dim dview As DataView = Nothing
        Dim sCountryCode As String
        If cmbRental.SelectedItem.Text <> "All" Then
            sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(cmbRental.SelectedValue)
            newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec(sCountryCode, "", UserCode)
            dview = newtable.DefaultView
            dview.RowFilter = GetViewParameters() ''"NtsBrdCode   = '" & _brand & "' or NtsBrdCode = '' or NtsBrdCode  is null"
            dview.Sort = SORTED_FIELDS
            gwMain.DataSource = dview
            cmbCountryOfTravel.Enabled = False
        Else
            If cmbCountryOfTravel.Items.Count = 1 Then
                If cmbCountryOfTravel.SelectedItem.Text = "Australia" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("AU", "", UserCode)
                End If
                If cmbCountryOfTravel.SelectedItem.Text = "New Zealand" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("NZ", "", UserCode)
                End If
            Else
                If cmbCountryOfTravel.SelectedItem.Text = "Australia" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("AU", "", UserCode)
                End If
                If cmbCountryOfTravel.SelectedItem.Text = "New Zealand" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("NZ", "", UserCode)
                End If
            End If

            dview = newtable.DefaultView
            dview.RowFilter = GetViewParameters()
            dview.Sort = SORTED_FIELDS
            gwMain.DataSource = dview

            cmbCountryOfTravel.Enabled = True

        End If
        gwMain.DataBind()


    End Sub

    Protected Sub cmbCountryOfTravel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCountryOfTravel.SelectedIndexChanged
        gwMain.DataSource = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec(cmbCountryOfTravel.SelectedValue, ViewState(VIEWSTATE_CONFIRMATIONID), UserCode)
        gwMain.DataBind()
    End Sub

    Sub SetViewButtonJavaScript()
        ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
        If (txtrentals.Value = ALL_BOOKINGS Or lblBooking.Text = ALL_BOOKINGS) Then
            btnView.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ALL"))
            btnView2.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ALL"))
        ElseIf lblBooking.Text.ToUpper = ALL_BOOKINGS.ToUpper Then
            btnView.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ALL"))
            btnView2.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ALL"))
        Else
            If (txtrentals.Value.Contains(",") = True) Then
                btnView.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("MANY"))
                btnView2.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("MANY"))
            Else
                btnView.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ONE"))
                btnView2.Attributes.Add("OnClick", BuildStringForShowconfirmationRedirect("ONE"))
            End If
        End If
    End Sub

    Function BuildStringForShowconfirmationRedirect(ByVal AllOrOne As String) As String
        Dim result As String = ""
        Dim ItemsExcluded As String = ""

        Select Case AllOrOne
            Case "ALL"
                result = "return ShowConfirmationScreen('1$" & ViewState(VIEWSTATE_CONFIRMATIONID) & "$" & UserCode & "$ConfirmationManagement.aspx$" & ViewState(VIEWSTATE_BOOKINGID) & "$" & ViewState(VIEWSTATE_OLD_RENTALID) & "$" & ViewState(VIEWSTATE_RENTALNO) & "$" & ViewState(VIEWSTATE_AUDIENCETYPE) & "$none" & "','" & ViewState(VIEWSTATE_CONFIRMATIONID) & "');"
            Case "MANY"
                If (FinalUnselectedRental Is Nothing) Then
                    FinalUnselectedRental = RentalSelectionContentUnselected()
                End If
                If (Not FinalUnselectedRental Is Nothing) Then
                    If FinalUnselectedRental.Count - 1 <> -1 Then
                        For Each de As DictionaryEntry In FinalUnselectedRental
                            ItemsExcluded = ItemsExcluded & de.Value & ","
                        Next
                        ItemsExcluded = ItemsExcluded.Substring(0, ItemsExcluded.Length - 1)
                    End If
                End If
                result = "return ShowConfirmationScreen('1$" & ViewState(VIEWSTATE_CONFIRMATIONID) & "$" & UserCode & "$ConfirmationManagement.aspx$" & ViewState(VIEWSTATE_BOOKINGID) & "$" & ViewState(VIEWSTATE_OLD_RENTALID) & "$" & ViewState(VIEWSTATE_RENTALNO) & "$" & ViewState(VIEWSTATE_AUDIENCETYPE) & "$" & ItemsExcluded & "','" & ViewState(VIEWSTATE_CONFIRMATIONID) & "');"
            Case "ONE"
                result = "return ShowConfirmationScreen('0$" & ViewState(VIEWSTATE_CONFIRMATIONID) & "$" & UserCode & "$ConfirmationManagement.aspx$" & ViewState(VIEWSTATE_BOOKINGID) & "$" & ViewState(VIEWSTATE_RENTALID) & "$" & ViewState(VIEWSTATE_RENTALNO) & "$" & ViewState(VIEWSTATE_AUDIENCETYPE) & "','" & ViewState(VIEWSTATE_CONFIRMATIONID) & "');"
        End Select
        LogDebug("BuildStringForShowconfirmationRedirect : " & AllOrOne & " " & result)
        Return result
        ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
        'If AllOrOne = 1 Then
        '    Return "return ShowConfirmationScreen('1$" & ViewState(VIEWSTATE_CONFIRMATIONID) & "$" & UserCode & "$ConfirmationManagement.aspx$" & ViewState(VIEWSTATE_BOOKINGID) & "$" & ViewState(VIEWSTATE_OLD_RENTALID) & "$" & ViewState(VIEWSTATE_RENTALNO) & "$" & ViewState(VIEWSTATE_AUDIENCETYPE) & "','" & ViewState(VIEWSTATE_CONFIRMATIONID) & "');"
        'Else
        '    Return "return ShowConfirmationScreen('0$" & ViewState(VIEWSTATE_CONFIRMATIONID) & "$" & UserCode & "$ConfirmationManagement.aspx$" & ViewState(VIEWSTATE_BOOKINGID) & "$" & ViewState(VIEWSTATE_RENTALID) & "$" & ViewState(VIEWSTATE_RENTALNO) & "$" & ViewState(VIEWSTATE_AUDIENCETYPE) & "','" & ViewState(VIEWSTATE_CONFIRMATIONID) & "');"
        'End If
    End Function

#Region "rev:mia Oct.4 2012 - Confirmation Setup addition of brand"
    Private _brand As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        _brand = getAllBrand(CStr(Request("RentalId")))
    End Sub

    Private Function getAllBrand(rentalId As String) As String
        Return Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrand(rentalId)
    End Function

    Private Function getBrandName(brandCode As String) As String
        Return Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrandName(brandCode)
    End Function

    Private Function getFilteredBrands() As String
        Dim rentalIds As String = ""
        Dim temp As String = _brand
        _brand = ""

        For Each item As ListItem In cblRentals.Items ''cmbRental.Items
            If item.Selected = True Then
                temp = getAllBrand(item.Value)
                If (_brand.Contains(temp) = False) Then
                    If (String.IsNullOrEmpty(_brand)) Then
                        _brand = String.Concat(_brand, temp, ",")
                    Else
                        If (_brand.IndexOf(temp) = -1) Then
                            _brand = String.Concat(_brand, temp, ",")
                        End If
                    End If
                End If
                rentalIds = rentalIds & item.Value.Split("-")(1) & ","
            End If
        Next

        If (_brand.LastIndexOf(",") <> -1) Then
            _brand = _brand.Remove(_brand.LastIndexOf(","))
        End If
        If (rentalIds.LastIndexOf(",") <> -1) Then
            rentalIds = rentalIds.Remove(rentalIds.LastIndexOf(","))
        End If

        If (rentalIds.Split(",").Length = cblRentals.Items.Count) Then
            txtrentals.Value = ALL_BOOKINGS
            '' labelRentalCheckall.Text = UNCHECKALL_RENTAL
        Else
            txtrentals.Value = rentalIds
        End If


        Return _brand
    End Function

    Function GetViewParameters(Optional firstResultEmpty As Boolean = False) As String
        Dim parameter As New StringBuilder

        If String.IsNullOrEmpty(_brand) Then
            _brand = getAllBrand(CStr(Request("RentalId")))
        End If

        ''rev:mia jan 11 2013 - fixes for Job number is 38709. added to the  NtsBrdCode  = '' to the first condition
        If (Not String.IsNullOrEmpty(_brand) And firstResultEmpty = False) Then
            If (_brand.Contains(",") = False) Then
                Return "NtsBrdCode   = '" & _brand & "' or NtsBrdCode  = ''"
            End If
        Else
            If (Not String.IsNullOrEmpty(_brand) And firstResultEmpty = True) Then
                Return "NtsBrdCode =  '' or NtsBrdCode  is null"
            End If

        End If

        parameter.Append("NtsBrdCode in (")
        For Each item As String In _brand
            If (item <> ",") Then
                parameter.AppendFormat("'{0}',", item)
            End If
        Next
        If (parameter.ToString.LastIndexOf(",") = parameter.ToString.Length - 1) Then
            parameter.Remove(parameter.Length - 1, 1)
        End If
        parameter.Append(") or NtsBrdCode = '' or NtsBrdCode  is null")

        Return parameter.ToString
    End Function
#End Region

#Region "rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes."

    Protected Sub cblRentals_SelectedIndexChanged(ByVal s As Object, ByVal obj As EventArgs)

        Dim rentalIdDefault As String = cblRentals.SelectedValue
        If (String.IsNullOrEmpty(rentalIdDefault)) Then
            txtrentals.Value = CStr(Request("RentalId").ToString.Split("-")(1))
            cblRentals.SelectedIndex = cblRentals.Items.IndexOf(cblRentals.Items.FindByText(txtrentals.Value))
            SetInformationShortMessage(RENTALID_MANDATORY & DEFAULT_RENTAL & txtrentals.Value)
            Return
        End If

        SelectCountryForRental(rentalIdDefault, CType(ViewState(VIEWSTATE_COUNTRIESDATA), DataTable), False)
        FinalUnselectedRental = RentalSelectionContentUnselected()
        SetViewButtonJavaScript()

        ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
        _brand = getFilteredBrands()

        Dim newtable As DataTable = Nothing
        Dim dview As DataView = Nothing
        Dim sCountryCode As String
        If txtrentals.Value <> ALL_BOOKINGS Then
            sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(IIf(String.IsNullOrEmpty(cblRentals.SelectedValue), _
                                                                                            Request.QueryString("ActiveRentalCountry"), _
                                                                                            cblRentals.SelectedValue))

            'If (String.IsNullOrEmpty(sCountryCode)) Then sCountryCode = Request.QueryString("ActiveRentalCountry")
            'If (String.IsNullOrEmpty(sCountryCode)) Then sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(CStr(Request("RentalId")))
            'newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec(sCountryCode, "", UserCode)
            'dview = newtable.DefaultView
            'dview.RowFilter = GetViewParameters() ''"NtsBrdCode   = '" & _brand & "' or NtsBrdCode = '' or NtsBrdCode  is null"
            'dview.Sort = SORTED_FIELDS
            'gwMain.DataSource = dview

            UpdateGrids(sCountryCode)
            cmbCountryOfTravel.Enabled = False
        Else
            If cmbCountryOfTravel.Items.Count = 1 Then
                If cmbCountryOfTravel.SelectedItem.Text = "Australia" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("AU", "", UserCode)
                End If
                If cmbCountryOfTravel.SelectedItem.Text = "New Zealand" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("NZ", "", UserCode)
                End If
            Else
                If cmbCountryOfTravel.SelectedItem.Text = "Australia" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("AU", "", UserCode)
                End If
                If cmbCountryOfTravel.SelectedItem.Text = "New Zealand" Then
                    newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec("NZ", "", UserCode)
                End If
            End If

            dview = newtable.DefaultView
            dview.RowFilter = GetViewParameters()
            dview.Sort = SORTED_FIELDS
            gwMain.DataSource = dview
            gwMain.DataBind()
            cmbCountryOfTravel.Enabled = True


        End If

    End Sub

    Private Property FinalUnselectedRental As Hashtable
        Get
            Return ViewState("FinalUnselectedRental")
        End Get
        Set(value As Hashtable)
            ViewState("FinalUnselectedRental") = value
        End Set
    End Property

    Private Function RentalSelectionCount() As RentalSelectionEnum
        Dim ctr As Integer = RentalSelectionEnum.Noselection
        For Each li As ListItem In cblRentals.Items
            If li.Selected = True Then
                ctr = ctr + 1
            End If
        Next

        If (ctr = 0) Then
            Return RentalSelectionEnum.Noselection
        ElseIf ctr = 1 Then
            Return RentalSelectionEnum.Oneselection
        Else
            If (ctr = cblRentals.Items.Count) Then
                Return RentalSelectionEnum.All
            Else
                Return RentalSelectionEnum.ManySelection
            End If
        End If
    End Function

    Private Function RentalSelectionContent() As Hashtable
        Dim myhash As New Hashtable
        For Each li As ListItem In cblRentals.Items
            If li.Selected = True Then
                myhash.Add(li.Text, li.Value)
            End If
        Next

        Return myhash
    End Function

    Private Function RentalSelectionContentUnselected() As Hashtable
        Dim myhash As New Hashtable
        For Each li As ListItem In cblRentals.Items
            If li.Selected = False Then
                myhash.Add(li.Text, li.Value)
            End If
        Next

        Return myhash
    End Function

    Public Enum RentalSelectionEnum
        Noselection = 0
        Oneselection = 1
        ManySelection = 2
        All = 3
    End Enum

    Sub RemoveTrailingComma()
        Try
            If ViewState(VIEWSTATE_RENTALID) Is Nothing Then
                Exit Sub
            End If

            If (ViewState(VIEWSTATE_RENTALID).ToString.EndsWith(",") = True) Then
                ViewState(VIEWSTATE_RENTALID) = ViewState(VIEWSTATE_RENTALID).ToString.Substring(0, ViewState(VIEWSTATE_RENTALID).ToString.LastIndexOf(","))
                ViewState(VIEWSTATE_RENTALNO) = ViewState(VIEWSTATE_RENTALNO).ToString.Substring(0, ViewState(VIEWSTATE_RENTALNO).ToString.LastIndexOf(","))
                ViewState(VIEWSTATE_CONFIRMATIONID) = ViewState(VIEWSTATE_CONFIRMATIONID).ToString.Substring(0, ViewState(VIEWSTATE_CONFIRMATIONID).ToString.LastIndexOf(","))
            End If
        Catch ex As Exception

        End Try

    End Sub

    Function MapSelectionToEnum() As RentalSelectionEnum
        Dim rentalselection As RentalSelectionEnum
        If ViewState(VIEWSTATE_RENTALID) Is Nothing Then
            '' Return RentalSelectionEnum.All
            ''rev:mia jan 6 2014 - added code below to trap values giving "AlL"
            Dim confirmId As String = ""
            Dim strConfId As String = ""

            If (txtrentals.Value.Contains(",")) Then
                Dim strvalue() As String = txtrentals.Value.Split(",")
                Dim item As String
                Dim _RENTALID As String = ""
                Dim _RENTALNO As String = ""

                For Each item In strvalue
                    _RENTALID = String.Concat(_RENTALID, cblRentals.SelectedValue.Split("-")(0), "-", item, ",")
                    _RENTALNO = String.Concat(_RENTALNO, item, ",")
                    confirmId = Aurora.Booking.Services.BookingConfirmation.GetConfirmationId(txtrentals.Value, cblRentals.SelectedValue.Split("-")(0), item)
                    If (confirmId <> "ERROR" And Not String.IsNullOrEmpty(confirmId)) Then
                        strConfId = String.Concat(strConfId, confirmId, ",")
                    End If
                Next
                ViewState(VIEWSTATE_CONFIRMATIONID) = strConfId.Remove(strConfId.Length - 1)
                ViewState(VIEWSTATE_RENTALID) = _RENTALID.Remove(_RENTALID.Length - 1)
                ViewState(VIEWSTATE_RENTALNO) = _RENTALNO
                Return RentalSelectionEnum.ManySelection
            ElseIf txtrentals.Value = "All" Then
                ViewState(VIEWSTATE_RENTALID) = "All"
                ViewState(VIEWSTATE_RENTALNO) = "All"
                Return RentalSelectionEnum.All
            Else
                ViewState(VIEWSTATE_RENTALID) = cblRentals.SelectedValue
                ViewState(VIEWSTATE_RENTALNO) = cblRentals.SelectedValue.Split("-")(1)
                Return RentalSelectionEnum.Oneselection
            End If

        End If


        If (ViewState(VIEWSTATE_RENTALID).ToString.Contains(",") = True) Then
            rentalselection = RentalSelectionEnum.ManySelection
        ElseIf ViewState(VIEWSTATE_RENTALID).ToString = ALL_BOOKINGS Then
            rentalselection = RentalSelectionEnum.All
        ElseIf (ViewState(VIEWSTATE_RENTALID).ToString.Contains(",") = False) Then
            rentalselection = RentalSelectionEnum.Oneselection
        Else
            rentalselection = RentalSelectionEnum.Noselection
        End If

        Return rentalselection
    End Function
#End Region


    Protected Sub chkRentalCheckall_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkRentalCheckall.CheckedChanged
        Dim rentalIds As String = ""
        Try
            For Each item As ListItem In cblRentals.Items ''cmbRental.Items
                item.Selected = chkRentalCheckall.Checked
                If (item.Selected = True) Then
                    rentalIds = rentalIds & item.Value.Split("-")(1) & ","
                End If
            Next
            If (rentalIds.LastIndexOf(",") <> -1) Then
                rentalIds = rentalIds.Remove(rentalIds.LastIndexOf(","))
            End If
            If (rentalIds = "") Then
                txtrentals.Value = CStr(Request("RentalId").ToString.Split("-")(1))
                cblRentals.SelectedIndex = cblRentals.Items.IndexOf(cblRentals.Items.FindByText(txtrentals.Value))
                SetInformationShortMessage(DEFAULT_RENTAL & txtrentals.Value)
            Else
                If (rentalIds.Split(",").Length = cblRentals.Items.Count) Then
                    txtrentals.Value = ALL_BOOKINGS
                Else
                    txtrentals.Value = rentalIds
                End If
            End If

            _brand = getFilteredBrands()

            Dim sCountryCode As String = Request.QueryString("ActiveRentalCountry")
            If (String.IsNullOrEmpty(cblRentals.SelectedValue)) Then
                sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(Request.QueryString("ActiveRentalCountry"))
            Else
                sCountryCode = Aurora.Booking.Services.BookingConfirmation.GetRentalCountry(cblRentals.SelectedValue)
            End If

            UpdateGrids(sCountryCode)
        Catch ex As Exception
            txtrentals.Value = CStr(Request("RentalId").ToString.Split("-")(1))
            cblRentals.SelectedIndex = cblRentals.Items.IndexOf(cblRentals.Items.FindByText(txtrentals.Value))
        End Try
    End Sub

    Private Property ConfirmationTable As DataTable
        Get
            Return CType(ViewState("ConfirmationTable"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("ConfirmationTable") = value
        End Set
    End Property


    Sub UpdateGrids(country As String)
        Dim dview As DataView = Nothing
        Dim newtable As DataTable
        If ConfirmationTable Is Nothing Then
            newtable = Aurora.Booking.Services.BookingConfirmation.GetConfirmationNoteSpec(country, "", UserCode)
            ConfirmationTable = newtable
        Else
            newtable = ConfirmationTable
        End If

        dview = newtable.DefaultView
        dview.RowFilter = GetViewParameters()
        If (dview.Count = 0) Then
            dview.RowFilter = GetViewParameters(True)
        End If
        dview.Sort = SORTED_FIELDS
        gwMain.DataSource = dview
        gwMain.DataBind()
    End Sub

End Class
