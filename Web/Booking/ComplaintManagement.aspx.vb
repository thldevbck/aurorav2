'-- RKS : 18-Nov-2010
'-- Call 31092: Complaint tab enhancement

Imports System.Xml
Imports System.Data
Imports Aurora.Common.Utility
Imports Aurora.Common
Imports Aurora.Booking.Services

<AuroraPageTitleAttribute("Complaint Maintenance")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Public Class Booking_ComplaintManagement
    Inherits AuroraPage

    Public Property ComplaintId() As Long
        Get
            If (ViewState("CMPMGT_ComplaintId") IsNot Nothing) Then
                Return CLng(ViewState("CMPMGT_ComplaintId"))
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Long)
            ViewState("CMPMGT_ComplaintId") = value
        End Set
    End Property

    Public Property RentalId() As String
        Get
            If (ViewState("CMPMGT_RentalId") IsNot Nothing) Then
                Return CStr(ViewState("CMPMGT_RentalId"))
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("CMPMGT_RentalId") = value
        End Set
    End Property

    Public Property ComplaintDataTable() As DataTable
        Get
            If (Session("CMP_ComplaintDataTable") IsNot Nothing) Then
                Return CType(Session("CMP_ComplaintDataTable"), DataTable)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataTable)
            Session("CMP_ComplaintDataTable") = value
        End Set
    End Property

    Public Property IntegrityNo() As Int16
        Get
            If (ViewState("CMPMGT_IntegrityNo") IsNot Nothing) Then
                Return CInt(ViewState("CMPMGT_IntegrityNo"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Int16)
            ViewState("CMPMGT_IntegrityNo") = value
        End Set
    End Property

    Public Property CauseSubCauseMasterDataTable() As DataTable
        Get
            If (Session("CMP_CauseSubCauseMasterDataTable") IsNot Nothing) Then
                Return CType(Session("CMP_CauseSubCauseMasterDataTable"), DataTable)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataTable)
            Session("CMP_CauseSubCauseMasterDataTable") = value
        End Set
    End Property

    Public Property OwnerGroupUsersMasterDataTable() As DataTable
        Get
            If (Session("CMP_OwnerGroupUsersMasterDataTable") IsNot Nothing) Then
                Return CType(Session("CMP_OwnerGroupUsersMasterDataTable"), DataTable)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataTable)
            Session("CMP_OwnerGroupUsersMasterDataTable") = value
        End Set
    End Property

#Region "Complaint Data Property Region"

    Property CalledFromSaveNew() As Boolean
        Get
            Return CBool(ViewState("CMP_DATA_CalledFromSaveNew"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("CMP_DATA_CalledFromSaveNew") = value
        End Set
    End Property

    Property sCmpLocCode() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpLocCode"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpLocCode") = value
        End Set
    End Property
    Property sCmpOwnerGroupRolId() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpOwnerGroupRolId"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpOwnerGroupRolId") = value
        End Set
    End Property
    Property sCmpOwnerUserUsrCode() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpOwnerUserUsrCode"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpOwnerUserUsrCode") = value
        End Set
    End Property
    Property sCmpComplaintSummary() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpComplaintSummary"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpComplaintSummary") = value
        End Set
    End Property
    Property sCmpResolutionSummary() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpResolutionSummary"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpResolutionSummary") = value
        End Set
    End Property
    Property sCmpOriginalsHeldAt() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpOriginalsHeldAt"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpOriginalsHeldAt") = value
        End Set
    End Property
    Property sCmpPrimaryRootCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpPrimaryRootCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpPrimaryRootCause") = value
        End Set
    End Property
    Property sCmpPrimarySubCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpPrimarySubCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpPrimarySubCause") = value
        End Set
    End Property
    Property sCmpPrimaryDetail() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpPrimaryDetail"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpPrimaryDetail") = value
        End Set
    End Property
    Property sCmpSecondaryRootCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpSecondaryRootCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpSecondaryRootCause") = value
        End Set
    End Property
    Property sCmpSecondarySubCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpSecondarySubCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpSecondarySubCause") = value
        End Set
    End Property
    Property sCmpSecondaryDetail() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpSecondaryDetail"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpSecondaryDetail") = value
        End Set
    End Property
    Property sCmpTertiaryRootCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpTertiaryRootCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpTertiaryRootCause") = value
        End Set
    End Property
    Property sCmpTertiarySubCause() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpTertiarySubCause"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpTertiarySubCause") = value
        End Set
    End Property
    Property sCmpTertiaryDetail() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpTertiaryDetail"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpTertiaryDetail") = value
        End Set
    End Property
    Property sCmpCompensationOther() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpCompensationOther"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpCompensationOther") = value
        End Set
    End Property
    Property sAddUsrId() As String
        Get
            Return CStr(ViewState("CMP_DATA_sAddUsrId"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sAddUsrId") = value
        End Set
    End Property
    Property sModUsrId() As String
        Get
            Return CStr(ViewState("CMP_DATA_sModUsrId"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sModUsrId") = value
        End Set
    End Property
    Property sAddPrgmName() As String
        Get
            Return CStr(ViewState("CMP_DATA_sAddPrgmName"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sAddPrgmName") = value
        End Set
    End Property
    Property sCmpNotified() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpNotified"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpNotified") = value
        End Set
    End Property
    Property sCmpHandlerUsrCode() As String
        Get
            Return CStr(ViewState("CMP_DATA_sCmpHandlerUsrCode"))
        End Get
        Set(ByVal value As String)
            ViewState("CMP_DATA_sCmpHandlerUsrCode") = value
        End Set
    End Property

    Property nCmpCompensationFOCDays() As Int16
        Get
            Return Int16.Parse(ViewState("CMP_DATA_nCmpCompensationFOCDays"))
        End Get
        Set(ByVal value As Int16)
            ViewState("CMP_DATA_nCmpCompensationFOCDays") = value
        End Set
    End Property
    Property nCmpActivityId() As Integer
        Get
            Return Integer.Parse(ViewState("CMP_DATA_nCmpActivityId"))
        End Get
        Set(ByVal value As Integer)
            ViewState("CMP_DATA_nCmpActivityId") = value
        End Set
    End Property
    Property nCmpStatus() As Int16
        Get
            Return Int16.Parse(ViewState("CMP_DATA_nCmpStatus"))
        End Get
        Set(ByVal value As Int16)
            ViewState("CMP_DATA_nCmpStatus") = value
        End Set
    End Property
    Property nCmpStatHols() As Int16
        Get
            Return Int16.Parse(ViewState("CMP_DATA_nCmpStatHols"))
        End Get
        Set(ByVal value As Int16)
            ViewState("CMP_DATA_nCmpStatHols") = value
        End Set
    End Property

    Property dCmpCompensationPaidtoCustomer() As Double
        Get
            Return CDbl(ViewState("CMP_DATA_dCmpCompensationPaidtoCustomer"))
        End Get
        Set(ByVal value As Double)
            ViewState("CMP_DATA_dCmpCompensationPaidtoCustomer") = value
        End Set
    End Property
    Property dCmpCompensationCreditNote() As Double
        Get
            Return CDbl(ViewState("CMP_DATA_dCmpCompensationCreditNote"))
        End Get
        Set(ByVal value As Double)
            ViewState("CMP_DATA_dCmpCompensationCreditNote") = value
        End Set
    End Property


    Property dtCmpDateReceived() As DateTime
        Get
            Return CDate(ViewState("CMP_DATA_dtCmpDateReceived"))
        End Get
        Set(ByVal value As DateTime)
            ViewState("CMP_DATA_dtCmpDateReceived") = value
        End Set
    End Property
    Property dtCmpDateResolved() As DateTime
        Get
            Return CDate(ViewState("CMP_DATA_dtCmpDateResolved"))
        End Get
        Set(ByVal value As DateTime)
            ViewState("CMP_DATA_dtCmpDateResolved") = value
        End Set
    End Property
    Property dtAddDateTime() As DateTime
        Get
            Return CDate(ViewState("CMP_DATA_dtAddDateTime"))
        End Get
        Set(ByVal value As DateTime)
            ViewState("CMP_DATA_dtAddDateTime") = value
        End Set
    End Property
    Property dtModDateTime() As DateTime
        Get
            Return CDate(ViewState("CMP_DATA_dtModDateTime"))
        End Get
        Set(ByVal value As DateTime)
            ViewState("CMP_DATA_dtModDateTime") = value
        End Set
    End Property


#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ' Initial Load

            If Request.QueryString("CmpId") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString("CmpId")) Then
                ComplaintId = CLng(Request.QueryString("CmpId"))
            End If

            If Request.QueryString("RntId") IsNot Nothing Then
                RentalId = CStr(Request.QueryString("RntId"))
            End If

            LoadPage()

        End If
    End Sub

    Sub LoadPage()
        btnReset.Attributes.Add("onclick", "window.location.href='ComplaintManagement.aspx?CmpId=" & ComplaintId.ToString() & "&RntId=" & RentalId & "';return false;")
        btnBack.Attributes.Add("onclick", "window.opener.location=GetBackURL() ;window.close();return false;")

        Dim PrimaryCauseDataTable, _
            PrimarySubCauseDataTable, _
            SecondaryCauseDataTable, _
            SecondarySubCauseDataTable, _
            TertiaryCauseDataTable, _
            TertiarySubCauseDataTable, _
            OwnerGroupDataTable, _
            OwnerUsersDataTable, _
            NotifiedOptionsDataTable, _
            VehiclesDataTable, _
            HeaderDataTable As DataTable

        '-- Complain supervisor to modify certain field.
        Dim bIsSupervisor As Boolean

        BookingComplaints.GetPageData(ComplaintId, RentalId, ComplaintDataTable, _
                                      CauseSubCauseMasterDataTable, _
                                      OwnerGroupUsersMasterDataTable, _
                                      PrimaryCauseDataTable, PrimarySubCauseDataTable, _
                                      SecondaryCauseDataTable, SecondarySubCauseDataTable, _
                                      TertiaryCauseDataTable, TertiarySubCauseDataTable, _
                                      OwnerGroupDataTable, OwnerUsersDataTable, _
                                      NotifiedOptionsDataTable, VehiclesDataTable, _
                                      HeaderDataTable)
        If ComplaintDataTable.Rows.Count > 0 Then
            With ComplaintDataTable
                SetPrimaryCauseCombo(PrimaryCauseDataTable, .Rows(0)("CmpPrimaryRootCause").ToString())
                SetSecondaryCauseCombo(SecondaryCauseDataTable, .Rows(0)("CmpSecondaryRootCause").ToString())
                SetTertiaryCauseCombo(TertiaryCauseDataTable, .Rows(0)("CmpTertiaryRootCause").ToString())

                SetPrimarySubCauseCombo(PrimarySubCauseDataTable, .Rows(0)("CmpPrimarySubCause").ToString())
                SetSecondarySubCauseCombo(SecondarySubCauseDataTable, .Rows(0)("CmpSecondarySubCause").ToString())
                SetTertiarySubCauseCombo(TertiarySubCauseDataTable, .Rows(0)("CmpTertiarySubCause").ToString())

                SetOwnerGroupCombo(OwnerGroupDataTable, .Rows(0)("CmpOwnerGroupRolId").ToString())
                SetOwnerUserCombo(OwnerUsersDataTable, .Rows(0)("CmpOwnerUserUsrCode").ToString())

                SetNotifiedOptionsCombo(NotifiedOptionsDataTable, .Rows(0)("CmpNotified").ToString())

                '-- RKS : 18-Nov-2010
                '-- Call 31092: Complaint tab enhancement
                '-- New field to validate editing
                '-- this variable is passed in <<<EnableDisablePageControls>> sub

                bIsSupervisor = .Rows(0)("IsSuperVisor")

            End With
        Else
            SetPrimaryCauseCombo(PrimaryCauseDataTable, "")
            SetSecondaryCauseCombo(SecondaryCauseDataTable, "")
            SetTertiaryCauseCombo(TertiaryCauseDataTable, "")

            SetPrimarySubCauseCombo(PrimarySubCauseDataTable, "")
            SetSecondarySubCauseCombo(SecondarySubCauseDataTable, "")
            SetTertiarySubCauseCombo(TertiarySubCauseDataTable, "")

            SetOwnerGroupCombo(OwnerGroupDataTable, "")
            SetOwnerUserCombo(OwnerUsersDataTable, "")

            SetNotifiedOptionsCombo(NotifiedOptionsDataTable, "")
        End If


        grdVehicles.DataSource = VehiclesDataTable
        grdVehicles.DataBind()

        SetFields(ComplaintDataTable, HeaderDataTable)

        'If cmbStatus.SelectedValue = 2 Then
        If cmbStatus.SelectedValue = 2 Or cmbStatus.SelectedValue = 3 Then
            ' disable everything
            '-- RKS : 18-Nov-2010
            '-- Call 31092: Complaint tab enhancement
            '-- New parameter <<bIsSupervisor>> passed to enable editing for supervisor after complete

            EnableDisablePageControls(False, bIsSupervisor)
        End If

        pkrHandler.Param2 = UserSettings.Current.CtyCode

        Dim bAllowedToEdit As Boolean = False
        'For Each drUser As DataRow In OwnerUsersDataTable.Rows
        '    If drUser("UsrCode").ToString() = UserCode Then
        '        bAllowedToEdit = True
        '        Exit For
        '    End If
        'Next
        Dim nResult As Int16 = 0
        nResult = BookingComplaints.CheckVehicleDataExists(RentalId, UserCode)
        If nResult = 2 Then
            bAllowedToEdit = True
        End If

        ' Check Role here !
        If Not bAllowedToEdit Then

            '-- RKS : 18-Nov-2010
            '-- Call 31092: Complaint tab enhancement
            '-- New parameter <<bIsSupervisor>> passed to enable editing for supervisor after complete

            EnableDisablePageControls(False, bIsSupervisor)
        End If ' Check Role here ! - END
    End Sub

    ''' <summary>
    ''' This binds a combo box to a datatable
    ''' </summary>
    ''' <param name="DataTableToBindTo">Data Table To Bind To</param>
    ''' <param name="DataTextField">What you want to see in the drop down Text</param>
    ''' <param name="DataValueField">what gets saved in the database</param>
    ''' <param name="SelectedValue">Selected Value, if any</param>
    ''' <param name="TargetElement">Combobox id</param>
    ''' <remarks></remarks>
    Sub SetCombo(ByVal DataTableToBindTo As DataTable, _
                 ByVal DataTextField As String, _
                 ByVal DataValueField As String, _
                 ByVal SelectedValue As String, _
                 ByRef TargetElement As DropDownList, _
                 Optional ByVal AddBlankOption As Boolean = False)
        With TargetElement
            .Items.Clear()
            .DataSource = DataTableToBindTo
            .DataTextField = DataTextField
            .DataValueField = DataValueField
            .DataBind()
            Try
                If Not String.IsNullOrEmpty(SelectedValue) Then
                    .SelectedValue = SelectedValue
                End If
            Catch Ex As Exception
                ' do nothing
                ''System.Diagnostics.Debug.WriteLine(Ex.Message)
            End Try
            If AddBlankOption Then
                .Items.Insert(0, " ")
            End If
        End With
    End Sub


    Sub SetPrimaryCauseCombo(ByVal PrimaryCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(PrimaryCauseDataTable, "CauDesc", "CauId", SelectedValue, cmbPrimaryRootCause, True)
    End Sub

    Sub SetSecondaryCauseCombo(ByVal SecondaryCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(SecondaryCauseDataTable, "CauDesc", "CauId", SelectedValue, cmbSecondaryRootCause, True)
    End Sub

    Sub SetTertiaryCauseCombo(ByVal TertiaryCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(TertiaryCauseDataTable, "CauDesc", "CauId", SelectedValue, cmbTertiaryRootCause, True)
    End Sub

    Sub SetPrimarySubCauseCombo(ByVal PrimarySubCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(PrimarySubCauseDataTable, "SbcDesc", "SbcId", SelectedValue, cmbPrimarySubCause)
    End Sub

    Sub SetSecondarySubCauseCombo(ByVal SecondarySubCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(SecondarySubCauseDataTable, "SbcDesc", "SbcId", SelectedValue, cmbSecondarySubCause)
    End Sub

    Sub SetTertiarySubCauseCombo(ByVal TertiarySubCauseDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(TertiarySubCauseDataTable, "SbcDesc", "SbcId", SelectedValue, cmbTertiarySubCause)
    End Sub

    Sub SetOwnerGroupCombo(ByVal OwnerGroupDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(OwnerGroupDataTable, "RolDesc", "RolId", SelectedValue, cmbComplaintOwnerGroup)
    End Sub

    Sub SetOwnerUserCombo(ByVal OwnerUserDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(OwnerUserDataTable, "UsrName", "UsrCode", SelectedValue, cmbComplaintOwnerIndividual)
    End Sub

    Sub SetNotifiedOptionsCombo(ByVal NotifiedOptionsDataTable As DataTable, ByVal SelectedValue As String)
        SetCombo(NotifiedOptionsDataTable, "CodCode", "CodId", SelectedValue, cmbNotified)
    End Sub

    Sub SetFields(ByVal ComplaintDataTable As DataTable, ByVal HeaderDataTable As DataTable)
        If ComplaintDataTable.Rows.Count > 0 Then
            With ComplaintDataTable
                If Not ReferenceEquals(.Rows(0)("CmpCompensationCreditNote"), System.DBNull.Value) Then
                    txtCompensationCreditNote.Text = CDec(.Rows(0)("CmpCompensationCreditNote")).ToString("f2")
                Else
                    txtCompensationCreditNote.Text = .Rows(0)("CmpCompensationCreditNote").ToString()
                End If

                txtCompensationFOCDays.Text = .Rows(0)("CmpCompensationFOCDays").ToString()
                txtCompensationOther.Text = .Rows(0)("CmpCompensationOther").ToString()

                If Not ReferenceEquals(.Rows(0)("CmpCompensationPaidtoCustomer"), System.DBNull.Value) Then
                    txtCompensationPaidToCustomer.Text = CDec(.Rows(0)("CmpCompensationPaidtoCustomer")).ToString("f2")
                Else
                    txtCompensationPaidToCustomer.Text = .Rows(0)("CmpCompensationPaidtoCustomer").ToString()
                End If

                txtOriginalsHeldAt.Text = .Rows(0)("CmpOriginalsHeldAt").ToString()
                txtPrimaryDetail.Text = .Rows(0)("CmpPrimaryDetail").ToString()
                txtSecondaryDetail.Text = .Rows(0)("CmpSecondaryDetail").ToString()
                txtSummaryOfComplaint.Text = .Rows(0)("CmpComplaintSummary").ToString()
                txtSummaryOfResolution.Text = .Rows(0)("CmpResolutionSummary").ToString()
                txtTertiaryDetail.Text = .Rows(0)("CmpTertiaryDetail").ToString()

                'txtWorkingDaysToResolve.Text = 
                If Not ReferenceEquals(.Rows(0)("CmpDateReceived"), System.DBNull.Value) Then
                    dtReceived.Date = CDate(.Rows(0)("CmpDateReceived"))
                End If

                If Not ReferenceEquals(.Rows(0)("CmpDateResolved"), System.DBNull.Value) Then
                    dtResolved.Date = CDate(.Rows(0)("CmpDateResolved"))
                End If

                If Not ReferenceEquals(.Rows(0)("CmpStatHols"), System.DBNull.Value) Then
                    cmbStatHols.SelectedValue = .Rows(0)("CmpStatHols").ToString()
                End If

                If Not ReferenceEquals(.Rows(0)("CmpDateReceived"), System.DBNull.Value) AndAlso _
                  Not ReferenceEquals(.Rows(0)("CmpDateResolved"), System.DBNull.Value) AndAlso _
                  Not ReferenceEquals(.Rows(0)("CmpStatHols"), System.DBNull.Value) _
                Then
                    txtWorkingDaysToResolve.Text = CStr(dtResolved.Date.Subtract(dtReceived.Date).Days - CInt(cmbStatHols.SelectedValue))
                End If

                pkrHandler.Text = .Rows(0)("CmpHandlerUsrCode").ToString()
                pkrHandler.DataId = .Rows(0)("CmpHandlerUsrCode").ToString()

                pkrBranch.Text = .Rows(0)("CmpLocCode").ToString()
                pkrBranch.DataId = .Rows(0)("CmpLocCode").ToString()

                If Not ReferenceEquals(.Rows(0)("CmpNotified"), System.DBNull.Value) Then
                    cmbNotified.SelectedValue = .Rows(0)("CmpNotified").ToString()
                End If

                If Not ReferenceEquals(.Rows(0)("CmpStatus"), System.DBNull.Value) Then
                    cmbStatus.SelectedValue = .Rows(0)("CmpStatus").ToString()
                End If

                IntegrityNo = CInt(.Rows(0)("IntegrityNo"))

            End With
            MakeFieldsCompulsory(ToShowOrHideCompulsoryFields())
        Else
            ' no data - new record
            txtCompensationCreditNote.Text = ""
            txtCompensationFOCDays.Text = ""
            txtCompensationOther.Text = ""
            txtCompensationPaidToCustomer.Text = ""
            txtOriginalsHeldAt.Text = ""
            txtPrimaryDetail.Text = ""
            txtSecondaryDetail.Text = ""
            txtSummaryOfComplaint.Text = ""
            txtSummaryOfResolution.Text = ""
            txtTertiaryDetail.Text = ""

            dtReceived.Text = ""

            dtResolved.Text = ""

            cmbStatHols.SelectedValue = ""

            txtWorkingDaysToResolve.Text = ""

            pkrHandler.Text = ""
            pkrHandler.DataId = ""

            pkrBranch.Text = ""
            pkrBranch.DataId = ""

            cmbNotified.SelectedIndex = 0

            cmbStatus.SelectedValue = 0

            IntegrityNo = 0
            MakeFieldsCompulsory(False)
        End If

        With HeaderDataTable
            lblAgent.Text = .Rows(0)("AgnName").ToString()
            lblAIMSRefNo.Text = .Rows(0)("AimsRef").ToString()
            lblBookingRentalNumber.Text = .Rows(0)("BookingNo").ToString()
            lblHirer.Text = .Rows(0)("Hirer").ToString()
            lblPackage.Text = .Rows(0)("PkgCode").ToString()
        End With
    End Sub


    Protected Sub cmbPrimaryRootCause_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtPrimarySubCauseDataTable As New DataTable
        BookingComplaints.SetCurrentCauseSubCauseTables(Nothing, CType(sender, DropDownList).SelectedValue, "", "", CauseSubCauseMasterDataTable, Nothing, dtPrimarySubCauseDataTable, Nothing, Nothing, Nothing, Nothing)
        SetPrimarySubCauseCombo(dtPrimarySubCauseDataTable, "")
    End Sub

    Protected Sub cmbSecondaryRootCause_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtSecondarySubCauseDataTable As New DataTable
        BookingComplaints.SetCurrentCauseSubCauseTables(Nothing, "", CType(sender, DropDownList).SelectedValue, "", CauseSubCauseMasterDataTable, Nothing, Nothing, Nothing, dtSecondarySubCauseDataTable, Nothing, Nothing)
        SetSecondarySubCauseCombo(dtSecondarySubCauseDataTable, "")
    End Sub

    Protected Sub cmbTertiaryRootCause_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtTertiarySubCauseDataTable As New DataTable
        BookingComplaints.SetCurrentCauseSubCauseTables(Nothing, "", "", CType(sender, DropDownList).SelectedValue, CauseSubCauseMasterDataTable, Nothing, Nothing, Nothing, Nothing, Nothing, dtTertiarySubCauseDataTable)
        SetTertiarySubCauseCombo(dtTertiarySubCauseDataTable, "")
    End Sub

    Protected Sub cmbComplaintOwnerGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOwnerUserTable As New DataTable
        BookingComplaints.SetCurrentOwnerGroupOwnerUserTables(Nothing, OwnerGroupUsersMasterDataTable, CType(sender, DropDownList).SelectedValue, Nothing, dtOwnerUserTable)
        SetOwnerUserCombo(dtOwnerUserTable, "")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ValidateData() Then
            If cmbStatus.SelectedValue = 2 Or cmbStatus.SelectedValue = 3 Then
                cbcConfirmation.Show()
                Exit Sub
            End If
            CalledFromSaveNew = False
            DoSaveAction()
        End If
    End Sub

    Protected Sub btnSaveAndNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ValidateData() Then
            If cmbStatus.SelectedValue = 2 Or cmbStatus.SelectedValue = 3 Then
                cbcConfirmation.Show()
                Exit Sub
            End If
            CalledFromSaveNew = True
            DoSaveAction()
        End If
    End Sub

    Function ValidateData() As Boolean

        ValidateData = False

        '	CmpId                             bigint        
        '	CmpRntId                          varchar       
        '	CmpLocCode                        varchar       
        '	CmpOwnerGroupRolId                varchar       
        '	CmpOwnerUserUsrCode               varchar       
        '	CmpDateReceived                   datetime      
        '	CmpDateResolved                   datetime      
        '	CmpStatus                         tinyint       
        '	CmpStatHols                       tinyint       
        '	CmpComplaintSummary               varchar       
        '	CmpResolutionSummary              varchar       
        '	CmpOriginalsHeldAt                varchar       
        '	CmpPrimaryRootCause               varchar       
        '	CmpPrimarySubCause                varchar       
        '	CmpPrimaryDetail                  varchar       
        '	CmpSecondaryRootCause             varchar       
        '	CmpSecondarySubCause              varchar       
        '	CmpSecondaryDetail                varchar       
        '	CmpTertiaryRootCause              varchar       
        '	CmpTertiarySubCause               varchar       
        '	CmpTertiaryDetail                 varchar       
        '	CmpCompensationPaidtoCustomer     money         
        '	CmpCompensationCreditNote         money         
        '	CmpCompensationFOCDays            tinyint       
        '	CmpCompensationOther              varchar       
        '	CmpActivityId                     int           
        '	AddDateTime                       smalldatetime 
        '	ModDateTime                       smalldatetime 
        '	AddUsrId                          varchar       
        '	ModUsrId                          varchar       
        '	AddPrgmName                       varchar       


        nCmpActivityId = -1
        'For Each oRow As GridViewRow In grdVehicles.Rows
        '    If CType(oRow.FindControl("radVehicle"), RadioButton).Checked Then
        '        nCmpActivityId = CInt(grdVehicles.DataKeys(oRow.DataItemIndex).Value)
        '        Exit For
        '    End If
        'Next
        If Request.Form("rdgVehicle") IsNot Nothing Then
            nCmpActivityId = CInt(Request.Form("rdgVehicle").ToString())
        End If

        If nCmpActivityId = -1 Then
            SetErrorShortMessage("No Vehicle Selected")
            Exit Function
        End If

        If Not pkrBranch.IsValid OrElse pkrBranch.Text.Trim().Equals("") Then
            SetErrorShortMessage("Please select a valid location")
            Exit Function
        End If
        sCmpLocCode = pkrBranch.DataId

        sCmpOwnerGroupRolId = cmbComplaintOwnerGroup.SelectedValue
        sCmpOwnerUserUsrCode = cmbComplaintOwnerIndividual.SelectedValue

        If String.IsNullOrEmpty(txtSummaryOfComplaint.Text.Trim()) Then
            SetErrorShortMessage("Please enter a Summary of Complaint")
            Exit Function
        Else
            sCmpComplaintSummary = txtSummaryOfComplaint.Text.Trim()
        End If

        If Not dtResolved.IsValid Then
            SetErrorShortMessage("Please enter a valid Resolved Date")
            Exit Function
        End If
        dtCmpDateResolved = dtResolved.Date

        sCmpResolutionSummary = txtSummaryOfResolution.Text.Trim()

        If String.IsNullOrEmpty(txtOriginalsHeldAt.Text.Trim()) Then
            SetErrorShortMessage("Please enter where the originals are held")
            Exit Function
        Else
            sCmpOriginalsHeldAt = txtOriginalsHeldAt.Text.Trim()
        End If

        sCmpPrimaryRootCause = cmbPrimaryRootCause.SelectedValue.Trim()
        sCmpPrimarySubCause = cmbPrimarySubCause.SelectedValue.Trim()
        sCmpPrimaryDetail = txtPrimaryDetail.Text.Trim()
        sCmpSecondaryRootCause = cmbSecondaryRootCause.SelectedValue.Trim()
        sCmpSecondarySubCause = cmbSecondarySubCause.SelectedValue.Trim()
        sCmpSecondaryDetail = txtSecondaryDetail.Text.Trim()
        sCmpTertiaryRootCause = cmbTertiaryRootCause.SelectedValue.Trim()
        sCmpTertiarySubCause = cmbTertiarySubCause.SelectedValue.Trim()
        sCmpTertiaryDetail = txtTertiaryDetail.Text.Trim()
        sCmpCompensationOther = txtCompensationOther.Text.Trim()
        sCmpNotified = cmbNotified.SelectedValue

        If pkrHandler.IsValid Then
            sCmpHandlerUsrCode = pkrHandler.DataId
        Else
            SetErrorShortMessage("Please select a valid Complaint Handler")
            Exit Function
        End If


        If ComplaintId > -1 Then
            sModUsrId = UserCode
            dtModDateTime = Now
        Else
            sAddUsrId = UserCode
            sAddPrgmName = Request.Url.AbsoluteUri
            dtAddDateTime = Now
        End If

        If IsNumeric(txtCompensationFOCDays.Text) Then
            nCmpCompensationFOCDays = CInt(txtCompensationFOCDays.Text)
        Else
            nCmpCompensationFOCDays = -1
        End If

        nCmpStatus = CInt(cmbStatus.SelectedValue)

        If IsNumeric(cmbStatHols.SelectedValue) Then
            nCmpStatHols = CInt(cmbStatHols.SelectedValue)
        Else
            nCmpStatHols = -1
        End If

        If dtCmpDateResolved > DateTime.MinValue Then
            ' something entered here!
            nCmpStatus = 2
            If nCmpStatHols = -1 Then
                SetErrorShortMessage("Please select the number of Stat Hols between the Date Received and Date Resolved")
                Exit Function
            End If
            If String.IsNullOrEmpty(sCmpResolutionSummary) Then
                SetErrorShortMessage("Please enter a Summary of Resolution")
                Exit Function
            End If
            If cmbPrimaryRootCause.SelectedValue = "" OrElse cmbPrimarySubCause.SelectedValue = "" Then
                SetErrorShortMessage("Please enter a Primary Cause and Sub Cause")
                Exit Function
            End If
        End If

        If nCmpStatus = 2 Then
            ' something entered here!
            If nCmpStatHols = -1 Then
                SetErrorShortMessage("Please select the number of Stat Hols between the Date Received and Date Resolved")
                Exit Function
            End If
            If dtCmpDateResolved = DateTime.MinValue Then
                SetErrorShortMessage("Please enter Date Resolved")
                Exit Function
            End If
            If String.IsNullOrEmpty(sCmpResolutionSummary) Then
                SetErrorShortMessage("Please enter a Summary of Resolution")
                Exit Function
            End If
            If cmbPrimaryRootCause.SelectedValue = "" OrElse cmbPrimarySubCause.SelectedValue = "" Then
                SetErrorShortMessage("Please enter a Primary Cause and Sub Cause")
                Exit Function
            End If
        End If

        If IsNumeric(txtCompensationPaidToCustomer.Text) Then
            dCmpCompensationPaidtoCustomer = CDbl(txtCompensationPaidToCustomer.Text)
        Else
            If String.IsNullOrEmpty(txtCompensationPaidToCustomer.Text.Trim()) Then
                dCmpCompensationPaidtoCustomer = -1
            Else
                SetErrorShortMessage("Please enter a valid amount for Compensation Paid to Customer")
                Exit Function
            End If

        End If

        If IsNumeric(txtCompensationCreditNote.Text) Then
            dCmpCompensationCreditNote = CDbl(txtCompensationCreditNote.Text)
        Else
            If String.IsNullOrEmpty(txtCompensationCreditNote.Text) Then
                dCmpCompensationCreditNote = -1
            Else
                SetErrorShortMessage("Please enter a valid amount for Compensation Credit Note")
                Exit Function
            End If
        End If

        If Not dtReceived.IsValid OrElse dtReceived.Text.Trim().Equals(String.Empty) Then
            SetErrorShortMessage("Please enter a valid Received Date")
            Exit Function
        End If

        dtCmpDateReceived = dtReceived.Date

        ValidateData = True

        ' Save variables for later use


    End Function

    Function SaveData() As Boolean

        SaveData = False

        Dim nNewComplaintId As Int64
        Dim sErrorMessage As String = ""
        nNewComplaintId = Aurora.Booking.Data.DataRepository.CreateUpdateComplaintData(ComplaintId, _
                                                                                       RentalId, _
                                                                                       sCmpLocCode, _
                                                                                       sCmpOwnerGroupRolId, _
                                                                                       sCmpOwnerUserUsrCode, _
                                                                                       dtCmpDateReceived, _
                                                                                       dtCmpDateResolved, _
                                                                                       nCmpStatus, _
                                                                                       nCmpStatHols, _
                                                                                       sCmpComplaintSummary, _
                                                                                       sCmpResolutionSummary, _
                                                                                       sCmpOriginalsHeldAt, _
                                                                                       sCmpPrimaryRootCause, _
                                                                                       sCmpPrimarySubCause, _
                                                                                       sCmpPrimaryDetail, _
                                                                                       sCmpSecondaryRootCause, _
                                                                                       sCmpSecondarySubCause, _
                                                                                       sCmpSecondaryDetail, _
                                                                                       sCmpTertiaryRootCause, _
                                                                                       sCmpTertiarySubCause, _
                                                                                       sCmpTertiaryDetail, _
                                                                                       dCmpCompensationPaidtoCustomer, _
                                                                                       dCmpCompensationCreditNote, _
                                                                                       nCmpCompensationFOCDays, _
                                                                                       sCmpCompensationOther, _
                                                                                       nCmpActivityId, _
                                                                                       sCmpNotified, _
                                                                                       sCmpHandlerUsrCode, _
                                                                                       sAddUsrId, _
                                                                                       sModUsrId, _
                                                                                       sAddPrgmName, _
                                                                                       IntegrityNo, _
                                                                                       sErrorMessage)
        If ComplaintId = -1 Then
            ComplaintId = nNewComplaintId
        End If

        If Not String.IsNullOrEmpty(sErrorMessage) Then
            SetErrorMessage(sErrorMessage)
            Exit Function
        End If

        SetInformationMessage("Updated Successfully")

        SaveData = True
    End Function

    Sub ConfirmationPostbackHandler(ByVal sender As Object, ByVal isLeftButton As Boolean, ByVal isRightButton As Boolean, ByVal param As String) Handles cbcConfirmation.PostBack
        If isLeftButton Then
            DoSaveAction()
        End If
    End Sub

    Sub DateReceivedPostbackHandler(ByVal sender As Object, ByVal e As EventArgs) Handles dtReceived.DateChanged
        CalculateWorkingDaysToResolve()
        ScriptManager.GetCurrent(Page).SetFocus(dtResolved.FindControl("dateTextBox"))
    End Sub

    Sub DateResolvedPostbackHandler(ByVal sender As Object, ByVal e As EventArgs) Handles dtResolved.DateChanged
        CalculateWorkingDaysToResolve()
        MakeFieldsCompulsory(ToShowOrHideCompulsoryFields())
        ScriptManager.GetCurrent(Page).SetFocus(cmbStatHols)
    End Sub

    Sub cmbStatHols_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbStatHols.SelectedIndexChanged
        CalculateWorkingDaysToResolve()
        ScriptManager.GetCurrent(Page).SetFocus(txtSummaryOfComplaint)
    End Sub

    Sub cmbStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbStatus.SelectedIndexChanged
        '-- RKS : 18-Nov-2010
        '-- Call 31092: Complaint tab enhancement
        '-- Blank ResolvedDate if status is not resolved

        If cmbStatus.SelectedValue <> 2 AndAlso dtResolved.Text.Length > 0 Then dtResolved.Text = ""
        MakeFieldsCompulsory(ToShowOrHideCompulsoryFields())
        ScriptManager.GetCurrent(Page).SetFocus(pkrBranch.FindControl("pickerTextBox"))
    End Sub


    Sub CalculateWorkingDaysToResolve()
        If dtReceived.IsValid AndAlso _
                   dtReceived.Date > DateTime.MinValue AndAlso _
                   dtResolved.IsValid AndAlso _
                   dtResolved.Date > DateTime.MinValue _
                Then
            Dim nStatHols As Int16 = 0
            If IsNumeric(cmbStatHols.SelectedValue) AndAlso cmbStatHols.SelectedValue > 0 Then
                nStatHols = CInt(cmbStatHols.SelectedValue)
            End If
            Dim nWeekEndDays As Int16 = 0
            For i As Int16 = 0 To dtResolved.Date.Subtract(dtReceived.Date).Days - 1
                If dtReceived.Date.AddDays(i).DayOfWeek = DayOfWeek.Sunday OrElse dtReceived.Date.AddDays(i).DayOfWeek = DayOfWeek.Saturday Then
                    nWeekEndDays += 1
                End If
            Next
            txtWorkingDaysToResolve.Text = (dtResolved.Date.Subtract(dtReceived.Date).Days - nStatHols - nWeekEndDays).ToString()
        Else
            txtWorkingDaysToResolve.Text = ""
        End If
    End Sub


    Function ToShowOrHideCompulsoryFields() As Boolean
        Dim bAllVisible As Boolean = False

        If dtResolved.IsValid AndAlso _
           dtResolved.Date > DateTime.MinValue _
        Then
            If ComplaintDataTable.Rows(0)("IssuperVisor") = False Then cmbStatus.SelectedValue = 2
            bAllVisible = True
        End If

        If cmbStatus.SelectedValue = 2 AndAlso Not bAllVisible Then
            bAllVisible = True
        End If
        Return bAllVisible
    End Function

    Sub MakeFieldsCompulsory(ByVal ShouldShow As Boolean)
        If ShouldShow Then
            lblCompulsory1.Text = "&nbsp;*"
            lblCompulsory2.Text = "&nbsp;*"
            lblCompulsory3.Text = "&nbsp;*"
            lblCompulsory4.Text = "&nbsp;*"
            lblCompulsory5.Text = "&nbsp;*"
        Else
            lblCompulsory1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;"
            lblCompulsory2.Text = "&nbsp;&nbsp;&nbsp;&nbsp;"
            lblCompulsory3.Text = "&nbsp;&nbsp;&nbsp;&nbsp;"
            lblCompulsory4.Text = "&nbsp;&nbsp;&nbsp;&nbsp;"
            lblCompulsory5.Text = "&nbsp;&nbsp;&nbsp;&nbsp;"
        End If

    End Sub

    Sub DoSaveAction()
        If SaveData() Then
            ClearSavedData()
            If CalledFromSaveNew Then
                ComplaintId = -1
            End If
            LoadPage()
        End If
    End Sub

    Sub ClearSavedData()
        sCmpLocCode = Nothing
        sCmpOwnerGroupRolId = Nothing
        sCmpOwnerUserUsrCode = Nothing
        sCmpComplaintSummary = Nothing
        sCmpResolutionSummary = Nothing
        sCmpOriginalsHeldAt = Nothing
        sCmpPrimaryRootCause = Nothing
        sCmpPrimarySubCause = Nothing
        sCmpPrimaryDetail = Nothing
        sCmpSecondaryRootCause = Nothing
        sCmpSecondarySubCause = Nothing
        sCmpSecondaryDetail = Nothing
        sCmpTertiaryRootCause = Nothing
        sCmpTertiarySubCause = Nothing
        sCmpTertiaryDetail = Nothing
        sCmpCompensationOther = Nothing
        sAddUsrId = Nothing
        sModUsrId = Nothing
        sAddPrgmName = Nothing
        sCmpNotified = Nothing
        sCmpHandlerUsrCode = Nothing

        nCmpCompensationFOCDays = Nothing
        nCmpActivityId = Nothing
        nCmpStatus = Nothing
        nCmpStatHols = Nothing

        dCmpCompensationPaidtoCustomer = Nothing
        dCmpCompensationCreditNote = Nothing

        dtCmpDateReceived = Nothing
        dtCmpDateResolved = Nothing
        dtAddDateTime = Nothing
        dtModDateTime = Nothing
    End Sub

    Sub EnableDisablePageControls(ByVal SwitchBit As Boolean, Optional ByVal superVisorSwitch As Boolean = False)

        '-- RKS : 18-Nov-2010
        '-- Call 31092: Complaint tab enhancement
        '-- new condition to set new variable <<bSuperOrNormal>>
        '-- replaced <<SwitchBit>> with <<bSuperOrNormal>> in certain condition see below

        Dim bSuperOrNormal As Boolean
        If superVisorSwitch = True And SwitchBit = False Then
            bSuperOrNormal = True
        Else
            bSuperOrNormal = SwitchBit
        End If

        btnSave.Enabled = bSuperOrNormal    '-- RKS : 18-Nov-2010
        btnSaveAndNew.Enabled = SwitchBit
        btnReset.Enabled = SwitchBit

        txtCompensationCreditNote.ReadOnly = Not bSuperOrNormal     '-- RKS : 18-Nov-2010
        txtCompensationFOCDays.ReadOnly = Not bSuperOrNormal        '-- RKS : 18-Nov-2010
        txtCompensationOther.ReadOnly = Not bSuperOrNormal          '-- RKS : 18-Nov-2010
        txtCompensationPaidToCustomer.ReadOnly = Not bSuperOrNormal '-- RKS : 18-Nov-2010
        txtOriginalsHeldAt.ReadOnly = Not SwitchBit
        txtPrimaryDetail.ReadOnly = Not bSuperOrNormal              '-- RKS : 18-Nov-2010
        txtSecondaryDetail.ReadOnly = Not bSuperOrNormal            '-- RKS : 18-Nov-2010
        txtSummaryOfComplaint.ReadOnly = Not SwitchBit
        txtSummaryOfResolution.ReadOnly = Not SwitchBit
        txtTertiaryDetail.ReadOnly = Not bSuperOrNormal             '-- RKS : 18-Nov-2010
        txtWorkingDaysToResolve.ReadOnly = Not SwitchBit

        'cmbComplaintOwnerGroup.Enabled = SwitchBit
        'cmbComplaintOwnerIndividual.Enabled = SwitchBit
        'cmbNotified.Enabled = SwitchBit
        'cmbPrimaryRootCause.Enabled = SwitchBit
        'cmbPrimarySubCause.Enabled = SwitchBit
        'cmbSecondaryRootCause.Enabled = SwitchBit
        'cmbSecondarySubCause.Enabled = SwitchBit
        'cmbTertiaryRootCause.Enabled = SwitchBit
        'cmbTertiarySubCause.Enabled = SwitchBit
        'cmbStatHols.Enabled = SwitchBit
        'cmbStatus.Enabled = SwitchBit
        EnableDisableDropDown(cmbComplaintOwnerGroup, SwitchBit)
        EnableDisableDropDown(cmbComplaintOwnerIndividual, SwitchBit)
        EnableDisableDropDown(cmbNotified, SwitchBit)
        EnableDisableDropDown(cmbPrimaryRootCause, bSuperOrNormal)      '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbPrimarySubCause, bSuperOrNormal)       '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbSecondaryRootCause, bSuperOrNormal)    '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbSecondarySubCause, bSuperOrNormal)     '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbTertiaryRootCause, bSuperOrNormal)     '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbTertiarySubCause, bSuperOrNormal)      '-- RKS : 18-Nov-2010
        EnableDisableDropDown(cmbStatHols, SwitchBit)
        EnableDisableDropDown(cmbStatus, bSuperOrNormal)                '-- RKS : 18-Nov-2010


        pkrBranch.ReadOnly = Not bSuperOrNormal                         '-- RKS : 18-Nov-2010
        pkrHandler.ReadOnly = Not SwitchBit

        dtReceived.ReadOnly = Not SwitchBit
        dtResolved.ReadOnly = Not bSuperOrNormal                        '-- RKS : 18-Nov-2010

    End Sub

    Sub EnableDisableDropDown(ByRef TargetControl As DropDownList, ByVal SwitchBit As Boolean)
        With TargetControl
            If Not SwitchBit Then
                .Attributes.Add("onclick", "this.blur();return false;")
                .Attributes.Add("onfocus", "this.blur();return false;")
                .Attributes.Add("onchange", "this.selectedIndex = " & .SelectedIndex & ";")
                For i As Int16 = 0 To .Items.Count - 1
                    If i <> .SelectedIndex Then
                        .Items(i).Enabled = False
                    End If
                Next
                .CssClass += " selectdisabled"
            Else
                If (.Attributes("onclick") IsNot Nothing) Then
                    .Attributes.Remove("onclick")
                End If
                If (.Attributes("onfocus") IsNot Nothing) Then
                    .Attributes.Remove("onfocus")
                End If
                If (.Attributes("onchange") IsNot Nothing) Then
                    .Attributes.Remove("onchange")
                End If

                For i As Int16 = 0 To .Items.Count - 1
                    .Items(i).Enabled = True
                Next
                If (.CssClass.Contains("selectdisabled")) Then
                    .CssClass = .CssClass.Replace("selectdisabled", "").Trim()
                End If
            End If
        End With
    End Sub


    
End Class
