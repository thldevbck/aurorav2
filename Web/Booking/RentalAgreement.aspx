<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RentalAgreement.aspx.vb"
    Inherits="Booking_RentalAgreement" MasterPageFile="~/Include/PopupHeader.master"
    ValidateRequest="false" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Xml ID="Xml1" runat="server" />
    <br />
    <center>
        <div id="pdf" runat="server">
            <iframe src="#" width="1000px" height="800px" id="iframePdf" runat="server" />
        </div>
    </center>
    <asp:Literal ID="lit" runat="server" />
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button runat="server" ID="btnClosePage" 
                    Text="Close" CssClass="Button_Standard Button_Close" />
            </td>
        </tr>
    </table>
</asp:Content>
