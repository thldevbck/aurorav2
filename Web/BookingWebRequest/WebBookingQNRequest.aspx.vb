﻿''new stored procedures
''WEB_GetQNBookingListByDate
''WEB_GetQNBookingStatusListFilteredV1
''Products_WEbGetLocationsAndBrands   - UPDATED
''WEB_B2CQNBookingActivityActions
''B2CBookingActivity                  - UPDATED  
''WEB_GetBookingQNStatusTotalOutstanding
''WEB_GetBookingQNStatusTotalInProgress
''WEB_GetBookingQNStatusTotalPending
''QN_fnGetQNbookingData

Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.WebbookingRequestQNDisplay)> _
Partial Class BookingWebRequest_WebBookingQNRequest
    Inherits AuroraPage


#Region "Constants"
    Private Const BRAND_MSG As String = "The search criteria has been changed. Please select  'Vehicle' and 'Status' then click 'Show Details' button to refresh the content."
    Private Const VEHICLE_MSG As String = "The search criteria has been changed. Please select 'Status' then click 'Show Details' button to refresh the content."
    Private Const STATUS_MSGS As String = ""
    Private Const CTY_MSGS As String = "The search criteria has been changed. Please select the ‘Show Details’ button to refresh content."
    Private Const COOKIENAME As String = "B2CQNcookies"
#End Region

#Region "Private Properties"
    Private _ByPassCaching As Boolean
    Property ByPassCaching As Boolean
        Get
            Return _ByPassCaching
        End Get
        Set(ByVal value As Boolean)
            _ByPassCaching = value
        End Set
    End Property

    Private _brand As String
    Private Property Brand() As String
        Get
            Return _brand
        End Get
        Set(ByVal value As String)
            _brand = value
        End Set
    End Property

    Private _vehicle As String
    Private Property Vehicle() As String
        Get
            Return _vehicle
        End Get
        Set(ByVal value As String)
            _vehicle = value
        End Set
    End Property

    Private Property FirstLoad() As Boolean
        Get
            Return ViewState("FirstLoad")
        End Get
        Set(ByVal value As Boolean)
            ViewState("FirstLoad") = value
        End Set
    End Property

    Private Property DefaultTimeMsg() As String
        Get
            Return ViewState("defaulttimeMsg")
        End Get
        Set(ByVal value As String)
            ViewState("defaulttimeMsg") = value
        End Set
    End Property

    Private Property CurrentTimeMsg() As Integer
        Get
            Return ViewState("currentTimeMsg")
        End Get
        Set(ByVal value As Integer)
            ViewState("currentTimeMsg") = value
        End Set
    End Property

    Private Property BrandSelected() As String
        Get
            Return ViewState("BrandSelected")
        End Get
        Set(ByVal value As String)
            ViewState("BrandSelected") = value
        End Set
    End Property

    Private Property VehicleNameSelected() As String
        Get
            Return ViewState("VehicleNameSelected")
        End Get
        Set(ByVal value As String)
            ViewState("VehicleNameSelected") = value
        End Set
    End Property


    Private ReadOnly Property AgentIDPopup As String
        Get
            If (String.IsNullOrEmpty(Me.PickerControlForAgent.Text) And String.IsNullOrEmpty(Me.PickerControlForAgent.DataId)) Then
                Return ""
            End If
            If Me.PickerControlForAgent.Text = "(All)" Then Return ""
            Return Me.PickerControlForAgent.DataId

        End Get
    End Property

    Private ReadOnly Property AgentIDPopupCookie As String
        Get
            If (String.IsNullOrEmpty(Me.PickerControlForAgent.Text) And String.IsNullOrEmpty(Me.PickerControlForAgent.DataId)) Then
                Return ""
            End If
            If Me.PickerControlForAgent.Text = "(All)" Then Return ""
            Return Me.PickerControlForAgent.DataId & "|" & Me.PickerControlForAgent.Text

        End Get
    End Property
#End Region

#Region "Procedures"
    Function GetQNCacheInfoList(ByVal bypasscache As Boolean, ByVal key As String) As Object

        Dim cacheitem As Object = CType(Cache(key), Object)

        If bypasscache Or cacheitem Is Nothing Then
            Select Case key
                Case "InitBrand"
                    cacheitem = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebLocationsAndBrands(Me.UserCode)
                Case "flowApprover"
                    cacheitem = Aurora.Common.Data.ExecuteScalarSP("sp_B2CBookFlowApprover", Me.UserCode)
                Case Else

            End Select

            Cache.Insert(key, cacheitem, Nothing, DateTime.Now.AddSeconds(120), TimeSpan.Zero)
        End If
        Return CType(cacheitem, Object)
    End Function

    Private Function isB2CBookflowApprover() As Boolean
        Dim xmldoc As New System.Xml.XmlDocument
        ''xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_B2CBookFlowApprover", Me.UserCode))
        xmldoc.LoadXml(CType(GetQNCacheInfoList(False, "flowApprover"), String))
        If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
            If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
        End If
        Return False
    End Function

    Sub InitBrand()
        Dim ds As DataSet = CType(GetQNCacheInfoList(False, "InitBrand"), DataSet) ''Aurora.SalesAndMarketing.Data.DataRepository.Get_WebLocationsAndBrands(Me.UserCode)

        Dim dscount As Integer = ds.Tables.Count

        Me.CountryDropDown.Items.Clear()
        Me.CountryDropDown.Items.Add("(All)")
        Me.CountryDropDown.AppendDataBoundItems = True
        Me.CountryDropDown.DataSource = ds.Tables(0).DefaultView
        Me.CountryDropDown.DataTextField = "Countries"
        Me.CountryDropDown.DataValueField = "CtyCode"
        Me.CountryDropDown.AutoPostBack = False
        Me.CountryDropDown.DataBind()


        Me.brandDropDown.AutoPostBack = False
        Me.brandDropDown.Items.Clear()
        Me.brandDropDown.Items.Add("(All)")
        Me.brandDropDown.AppendDataBoundItems = True
        Me.brandDropDown.DataSource = ds.Tables(1)
        Me.brandDropDown.DataTextField = "BrdName"
        Me.brandDropDown.DataValueField = "BrdCode"
        Me.brandDropDown.DataBind()

        ' ''some users has no default useragent id
        'If (ds.Tables(dscount - 1).Rows.Count > 0) Then
        '    Me.PickerControlForAgent.DataId = ds.Tables(dscount - 1).Rows(0).Item(0).ToString
        '    Me.PickerControlForAgent.Text = ds.Tables(dscount - 1).Rows(0).Item(1).ToString
        'End If


        Me.VehicleDropDown.AutoPostBack = False
        Me.VehicleDropDown.Items.Clear()
        Me.VehicleDropDown.AppendDataBoundItems = True
        Me.VehicleDropDown.Items.Add("(All)")
        Dim dsVehicle As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetActiveVehicles("", "")
        Me.VehicleDropDown.DataSource = dsVehicle.Tables(0)
        Me.VehicleDropDown.DataTextField = "ProductName"
        Me.VehicleDropDown.DataValueField = "PrdShortName"
        Me.VehicleDropDown.DataBind()
    End Sub

    Sub Filtered()
        Dim sBrand As String = IIf(Me.brandDropDown.SelectedItem.Text = "(All)", "", Me.brandDropDown.SelectedItem.Text)
        Dim sAgentCode As String = AgentIDPopup
        Dim sVehicle As String = IIf(Me.VehicleDropDown.SelectedItem.Text = "(All)", "", Me.VehicleDropDown.SelectedValue)
        Dim sActionFilter As String = IIf(Me.actionFilterDropdown.SelectedItem.Text = "(All)", "", Me.actionFilterDropdown.SelectedValue)
        Dim sCitycode As String = IIf(Me.CountryDropDown.SelectedItem.Text = "(All)", "", Me.CountryDropDown.SelectedValue)

        ''get parameter for the controls
        InitializedQNcontrols(sBrand, sAgentCode, sVehicle, sActionFilter, sCitycode)

        ''load data to the controls
        LoadControlData()
        FirstLoad = False
        sAgentCode = AgentIDPopupCookie
        CreateCookie(sCitycode, sBrand, sVehicle, sAgentCode, sActionFilter)
    End Sub

    Sub InitializedQNcontrols(ByVal sBrand As String, ByVal sAgentcode As String, ByVal sVehicle As String, ByVal sActionFilter As String, ByVal sCitycode As String, Optional ByVal visible As Boolean = False)
        Me.BookingListUserControl1.Brand = sBrand
        Me.BookingListUserControl1.AgentId = sAgentcode
        Me.BookingListUserControl1.Vehicle = sVehicle
        Me.BookingListUserControl1.ActionFilter = sActionFilter
        Me.BookingListUserControl1.Citycode = sCitycode
        '' Me.BookingListUserControl1.ByPassCaching = ByPassCaching

        Me.BookingListUserControl1.HiddenCollapsedOld = Me.HiddenCollapsedOld.Value
        Me.BookingListUserControl1.HiddenCollapsedToday = Me.HiddenCollapsedToday.Value
        Me.BookingListUserControl1.HiddenCollapsedYesterday = Me.HiddenCollapsedYesterday.Value
    End Sub

    Sub LoadControlData(Optional ByVal visible As Boolean = False)
        Me.BookingListUserControl1.LoadData(visible)
    End Sub

    Sub FilteredByCookie()
        Dim sBrand As String = IIf(Me.brandDropDown.SelectedItem.Text = "(All)", "", Me.brandDropDown.SelectedItem.Text)
        Dim sAgentCode As String = AgentIDPopup
        Dim sVehicle As String = IIf(Me.VehicleDropDown.SelectedItem.Text = "(All)", "", Me.VehicleDropDown.SelectedValue)
        Dim sActionFilter As String = IIf(Me.actionFilterDropdown.SelectedItem.Text = "(All)", "", Me.actionFilterDropdown.SelectedValue)
        Dim sCitycode As String = IIf(Me.CountryDropDown.SelectedItem.Text = "(All)", "", Me.CountryDropDown.SelectedValue)

        ''get parameter for the controls
        InitializedQNcontrols(sBrand, sAgentCode, sVehicle, sActionFilter, sCitycode)

        ''load data to the controls
        LoadControlData(True)
    End Sub

    Private Sub CreateCookie(ByVal CountryValue As String, _
                                   ByVal BrandValue As String, _
                                   ByVal VehicleValue As String, _
                                   ByVal AgentCode As String, _
                                   ByVal ActionValue As String)


        Dim B2Ccookies As New HttpCookie(COOKIENAME)
        B2Ccookies("CountryValue") = CountryValue
        B2Ccookies("BrandValue") = BrandValue
        B2Ccookies("VehicleValue") = VehicleValue
        B2Ccookies("ActionValue") = ActionValue
        B2Ccookies("AgentCode") = AgentCode.Split("|")(0)
        B2Ccookies("AgentText") = AgentCode.Split("|")(1)
        Response.Cookies.Add(B2Ccookies)
    End Sub

    Private Sub ReadCookie()

        If Request.Cookies(COOKIENAME) IsNot Nothing Then

            If Not Request.Cookies(COOKIENAME)("CountryValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(COOKIENAME)("CountryValue")) Then
                    Me.CountryDropDown.SelectedIndex = -1
                Else
                    Dim item As ListItem = Me.CountryDropDown.Items.FindByValue(Request.Cookies(COOKIENAME)("CountryValue"))
                    If item IsNot Nothing Then
                        Me.CountryDropDown.SelectedValue = item.Value
                    End If
                End If

            End If

            If Not Request.Cookies(COOKIENAME)("BrandValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(COOKIENAME)("BrandValue")) Then
                    Me.brandDropDown.SelectedIndex = -1
                Else
                    Dim item As ListItem = Me.brandDropDown.Items.FindByText(Request.Cookies(COOKIENAME)("BrandValue"))
                    If item IsNot Nothing Then
                        Me.brandDropDown.SelectedValue = item.Value
                    End If

                End If

            End If

            If Not Request.Cookies(COOKIENAME)("VehicleValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(COOKIENAME)("VehicleValue")) Then
                    Me.VehicleDropDown.SelectedIndex = -1
                Else
                    Me.VehicleDropDown.SelectedValue = Request.Cookies(COOKIENAME)("VehicleValue")
                End If

            End If

            If Not Request.Cookies(COOKIENAME)("ActionValue") Is Nothing Then
                Dim item As ListItem = Me.actionFilterDropdown.Items.FindByText(Request.Cookies(COOKIENAME)("ActionValue"))
                If item IsNot Nothing Then
                    Me.actionFilterDropdown.SelectedValue = item.Value
                End If

            End If

            If Not Request.Cookies(COOKIENAME)("AgentCode") Is Nothing Then
                PickerControlForAgent.DataId = Request.Cookies(COOKIENAME)("AgentCode")
                PickerControlForAgent.Text = Request.Cookies(COOKIENAME)("AgentText")
            End If

            FilteredByCookie()
        End If
    End Sub


#End Region

#Region "Page Load"
    Protected Sub BookingWebRequest_WebBookingQNRequest_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            InitBrand()
            FirstLoad = True
            Dim intMilliseconds As Integer = 60000

            Me.DefaultTimeMsg = Aurora.Reservations.Data.WebBookingItemProvider.B2CpageRefreshInterval
            ajaxtimer.Interval = CInt(Me.DefaultTimeMsg) * intMilliseconds
            Me.IntervalLiteral.Text = "Page refresh every  <b>" & CInt(Me.DefaultTimeMsg) & "</b> minute(s)"
            Me.BookingListUserControl1.B2CBookflowApprover = Me.isB2CBookflowApprover

            ''ignore cookie reading
            ReadCookie()
        Else
            Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
            If sc.AsyncPostBackSourceElementID.IndexOf("ChildGridView") <> -1 And sc.IsInAsyncPostBack Then
                Filtered()
            End If
        End If

    End Sub
#End Region

#Region "control events"
    Protected Sub filterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterButton.Click
        If Not String.IsNullOrEmpty(Me.PickerControlForAgent.Text) Then
            Filtered()
        Else
            Me.SetWarningMessage("Agent is required")
        End If
    End Sub


    Protected Sub ajaxtimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        Filtered()
    End Sub

    Protected Sub filterActionButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterActionButton.Click
        Filtered()
    End Sub
#End Region

End Class
