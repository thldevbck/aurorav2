Imports System
Imports System.data
Imports System.data.SqlClient
Imports System.Threading


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.WebbookingRequestDisplay)> _
Partial Class BookingWebRequest_WebBoookingRequest
    Inherits AuroraPage

#Region "Constants"
    Private Const BRAND_MSG As String = "The search criteria has been changed. Please select  'Vehicle' and 'Status' then click 'Show Details' button to refresh the content."
    Private Const VEHICLE_MSG As String = "The search criteria has been changed. Please select 'Status' then click 'Show Details' button to refresh the content."
    Private Const STATUS_MSGS As String = ""
    Private Const CTY_MSGS As String = "The search criteria has been changed. Please select the �Show Details� button to refresh content."
#End Region

#Region "Private Properties"
    Private _brand As String
    Private Property Brand() As String
        Get
            Return _brand
        End Get
        Set(ByVal value As String)
            _brand = value
        End Set
    End Property

    Private _vehicle As String
    Private Property Vehicle() As String
        Get
            Return _vehicle
        End Get
        Set(ByVal value As String)
            _vehicle = value
        End Set
    End Property

    Private Property FirstLoad() As Boolean
        Get
            Return ViewState("FirstLoad")
        End Get
        Set(ByVal value As Boolean)
            ViewState("FirstLoad") = value
        End Set
    End Property

    Private Property DefaultTimeMsg() As String
        Get
            Return ViewState("defaulttimeMsg")
        End Get
        Set(ByVal value As String)
            ViewState("defaulttimeMsg") = value
        End Set
    End Property

    Private Property CurrentTimeMsg() As Integer
        Get
            Return ViewState("currentTimeMsg")
        End Get
        Set(ByVal value As Integer)
            ViewState("currentTimeMsg") = value
        End Set
    End Property

    Private Property BrandSelected() As String
        Get
            Return ViewState("BrandSelected")
        End Get
        Set(ByVal value As String)
            ViewState("BrandSelected") = value
        End Set
    End Property

    Private Property VehicleNameSelected() As String
        Get
            Return ViewState("VehicleNameSelected")
        End Get
        Set(ByVal value As String)
            ViewState("VehicleNameSelected") = value
        End Set
    End Property
#End Region

#Region "Procedures"

    Private Function isB2CBookflowApprover() As Boolean
        Dim xmldoc As New System.Xml.XmlDocument
        xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_B2CBookFlowApprover", Me.UserCode))
        If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
            If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
        End If
        Return False
    End Function

    Sub InitBrand()
        Dim ds As DataSet = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebLocationsAndBrands(Me.UserCode)

        Me.CountryDropDown.Items.Add("(All)")
        Me.CountryDropDown.AppendDataBoundItems = True
        Me.CountryDropDown.DataSource = ds.Tables(0).DefaultView
        Me.CountryDropDown.DataTextField = "Countries"
        Me.CountryDropDown.DataValueField = "CtyCode"
        Me.CountryDropDown.AutoPostBack = False
        Me.CountryDropDown.DataBind()


        Me.brandDropDown.AutoPostBack = False
        Me.brandDropDown.Items.Clear()
        Me.brandDropDown.Items.Add("(All)")
        Me.brandDropDown.AppendDataBoundItems = True
        Me.brandDropDown.DataSource = ds.Tables(1)
        Me.brandDropDown.DataTextField = "BrdName"
        Me.brandDropDown.DataValueField = "BrdCode"
        Me.brandDropDown.DataBind()

        Me.StatusDropDownList.Items.Add("(All)")
        Me.StatusDropDownList.Items.Add("KB")
        Me.StatusDropDownList.Items.Add("WL")

        Me.VehicleDropDown.AutoPostBack = False
        Me.VehicleDropDown.Items.Clear()
        Me.VehicleDropDown.AppendDataBoundItems = True
        Me.VehicleDropDown.Items.Add("(All)")
        Dim dsVehicle As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetActiveVehicles("", "")
        Me.VehicleDropDown.DataSource = dsVehicle.Tables(0)
        Me.VehicleDropDown.DataTextField = "ProductName"
        Me.VehicleDropDown.DataValueField = "PrdShortName"
        Me.VehicleDropDown.DataBind()
    End Sub

   

    Sub Filtered()

        Dim sBrand As String = IIf(Me.brandDropDown.SelectedItem.Text = "(All)", "", Me.brandDropDown.SelectedItem.Text)
        Dim sStatus As String = IIf(Me.StatusDropDownList.SelectedItem.Text = "(All)", "", Me.StatusDropDownList.SelectedValue)
        Dim sVehicle As String = IIf(Me.VehicleDropDown.SelectedItem.Text = "(All)", "", Me.VehicleDropDown.SelectedValue)
        Dim sActionFilter As String = IIf(Me.actionFilterDropdown.SelectedItem.Text = "(All)", "", Me.actionFilterDropdown.SelectedValue)
        Dim sCitycode As String = IIf(Me.CountryDropDown.SelectedItem.Text = "(All)", "", Me.CountryDropDown.SelectedValue)

        Me.BookingListUserControl1.Brand = sBrand
        Me.BookingListUserControl1.Status = sStatus
        Me.BookingListUserControl1.Vehicle = sVehicle
        Me.BookingListUserControl1.ActionFilter = sActionFilter
        Me.BookingListUserControl1.Citycode = sCitycode

        Me.BookingListUserControl1.HiddenCollapsedOld = Me.HiddenCollapsedOld.Value
        Me.BookingListUserControl1.HiddenCollapsedToday = Me.HiddenCollapsedToday.Value
        Me.BookingListUserControl1.HiddenCollapsedYesterday = Me.HiddenCollapsedYesterday.Value

        Me.BookingListUserControl1.LoadData()
        FirstLoad = False
        CreateCookie(sCitycode, sBrand, sVehicle, sStatus, sActionFilter)
    End Sub

    Sub FilteredByCookie()

        Dim sBrand As String = IIf(Me.brandDropDown.SelectedItem.Text = "(All)", "", Me.brandDropDown.SelectedItem.Text)
        Dim sStatus As String = IIf(Me.StatusDropDownList.SelectedItem.Text = "(All)", "", Me.StatusDropDownList.SelectedValue)
        Dim sVehicle As String = IIf(Me.VehicleDropDown.SelectedItem.Text = "(All)", "", Me.VehicleDropDown.SelectedValue)
        Dim sActionFilter As String = IIf(Me.actionFilterDropdown.SelectedItem.Text = "(All)", "", Me.actionFilterDropdown.SelectedValue)
        Dim sCitycode As String = IIf(Me.CountryDropDown.SelectedItem.Text = "(All)", "", Me.CountryDropDown.SelectedValue)

        Me.BookingListUserControl1.Brand = sBrand
        Me.BookingListUserControl1.Status = sStatus
        Me.BookingListUserControl1.Vehicle = sVehicle
        Me.BookingListUserControl1.ActionFilter = sActionFilter
        Me.BookingListUserControl1.Citycode = sCitycode

        Me.BookingListUserControl1.HiddenCollapsedOld = Me.HiddenCollapsedOld.Value
        Me.BookingListUserControl1.HiddenCollapsedToday = Me.HiddenCollapsedToday.Value
        Me.BookingListUserControl1.HiddenCollapsedYesterday = Me.HiddenCollapsedYesterday.Value

        Me.BookingListUserControl1.LoadData(True)
    End Sub
    
    Sub WebBrandsFiltered(ByVal cty As String)
        Dim ds As DataSet = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebBrandsFiltered(cty, Me.UserCode)
        Me.brandDropDown.Items.Clear()
        Me.brandDropDown.Items.Add("(All)")
        Me.brandDropDown.AppendDataBoundItems = True
        Me.brandDropDown.DataSource = ds.Tables(0).DefaultView
        Me.brandDropDown.DataTextField = "BrdName"
        Me.brandDropDown.DataValueField = "BrdCode"
        Me.brandDropDown.DataBind()
    End Sub

    Private Sub CreateCookie(ByVal CountryValue As String, _
                                    ByVal BrandValue As String, _
                                    ByVal VehicleValue As String, _
                                    ByVal StatusValue As String, _
                                    ByVal ActionValue As String)

        Dim cookieName As String = "B2Ccookies"
        Dim B2Ccookies As New HttpCookie(cookieName)
        B2Ccookies("CountryValue") = CountryValue
        B2Ccookies("BrandValue") = BrandValue
        B2Ccookies("VehicleValue") = VehicleValue
        B2Ccookies("ActionValue") = ActionValue
        B2Ccookies("StatusValue") = StatusValue
        Response.Cookies.Add(B2Ccookies)
    End Sub

    Private Sub ReadCookie()
        Dim cookieName As String = "B2Ccookies"
        If Request.Cookies(cookieName) IsNot Nothing Then

            If Not Request.Cookies(cookieName)("CountryValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(cookieName)("CountryValue")) Then
                    Me.CountryDropDown.SelectedIndex = -1
                Else
                    Dim item As ListItem = Me.CountryDropDown.Items.FindByValue(Request.Cookies(cookieName)("CountryValue"))
                    If item IsNot Nothing Then
                        Me.CountryDropDown.SelectedValue = item.Value
                        ''WebBrandsFiltered(CountryDropDown.SelectedValue)
                    End If
                    
                End If

            End If

            If Not Request.Cookies(cookieName)("BrandValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(cookieName)("BrandValue")) Then
                    Me.brandDropDown.SelectedIndex = -1
                Else
                    Dim item As ListItem = Me.brandDropDown.Items.FindByText(Request.Cookies(cookieName)("BrandValue"))
                    If item IsNot Nothing Then
                        ''InitVehicles(item.Value, CountryDropDown.SelectedValue)
                        Me.brandDropDown.SelectedValue = item.Value
                    End If

                End If
                
            End If

            If Not Request.Cookies(cookieName)("VehicleValue") Is Nothing Then
                If String.IsNullOrEmpty(Request.Cookies(cookieName)("VehicleValue")) Then
                    Me.VehicleDropDown.SelectedIndex = -1
                Else
                    Me.VehicleDropDown.SelectedValue = Request.Cookies(cookieName)("VehicleValue")
                End If

            End If

            If Not Request.Cookies(cookieName)("ActionValue") Is Nothing Then
                Dim item As ListItem = Me.actionFilterDropdown.Items.FindByText(Request.Cookies(cookieName)("ActionValue"))
                If item IsNot Nothing Then
                    Me.actionFilterDropdown.SelectedValue = item.Value
                End If

            End If

            If Not Request.Cookies(cookieName)("StatusValue") Is Nothing Then
                Dim item As ListItem = Me.StatusDropDownList.Items.FindByText(Request.Cookies(cookieName)("StatusValue"))
                If item IsNot Nothing Then
                    Me.StatusDropDownList.SelectedValue = item.Value
                End If
            End If

            FilteredByCookie()
        End If
    End Sub

#End Region

#Region "Page Loads"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            InitBrand()
            FirstLoad = True
            Dim intMilliseconds As Integer = 60000

            Me.DefaultTimeMsg = Aurora.Reservations.Data.WebBookingItemProvider.B2CpageRefreshInterval
            ajaxtimer.Interval = CInt(Me.DefaultTimeMsg) * intMilliseconds
            Me.IntervalLiteral.Text = "Page refresh every  <b>" & CInt(Me.DefaultTimeMsg) & "</b> minute(s)"
            Me.BookingListUserControl1.B2CBookflowApprover = Me.isB2CBookflowApprover
            ReadCookie()
        End If
    End Sub

#End Region

#Region "Control events"
   

    Protected Sub filterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterButton.Click
        Filtered()
    End Sub

   
    Protected Sub ajaxtimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        Filtered()
    End Sub

    Protected Sub filterActionButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterActionButton.Click
        Filtered()
    End Sub


#End Region


#Region "REV: MIA DEC 3 2009 ...UNUSED EVENTS AND FUNCTIONS.. THIS WAS THE RESULT OF PERFORMANCE ISSUES"
    Protected Sub CountryDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CountryDropDown.SelectedIndexChanged
        'If String.IsNullOrEmpty(CountryDropDown.SelectedValue) Then Return
        'If String.IsNullOrEmpty(CountryDropDown.SelectedItem.Text) Then Return

        ' ''incase country selection change, we need to refreh the value of the brands
        'WebBrandsFiltered(CountryDropDown.SelectedValue)

        ' ''incase country selection change, we need to refreh the value of the vehicle
        'If CountryDropDown.SelectedValue = "" Or String.IsNullOrEmpty(CountryDropDown.SelectedValue) Then Return
        'If brandDropDown.SelectedValue = "" Or String.IsNullOrEmpty(brandDropDown.SelectedValue) Then Return
        'InitVehicles(brandDropDown.SelectedValue, CountryDropDown.SelectedValue)

        'If FirstLoad Then Return
        'Me.SetInformationMessage(CTY_MSGS)

    End Sub

    Protected Sub VehicleDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VehicleDropDown.SelectedIndexChanged
        'If VehicleDropDown.SelectedValue = "" Or String.IsNullOrEmpty(VehicleDropDown.SelectedValue) Then Return
        'Me.Vehicle = IIf(VehicleDropDown.SelectedItem.Text = "(All)", "(All)", VehicleDropDown.SelectedValue)
        'Me.VehicleNameSelected = VehicleDropDown.SelectedItem.Text
        'If FirstLoad Then Return
        'Dim list As ListItem = VehicleDropDown.Items.FindByText(Me.VehicleNameSelected)
        'If list IsNot Nothing Then
        '    VehicleDropDown.SelectedItem.Text = Me.VehicleNameSelected
        'End If
        'Me.SetInformationMessage(VEHICLE_MSG)
    End Sub

    Protected Sub brandDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles brandDropDown.SelectedIndexChanged
        'If CountryDropDown.SelectedValue = "" Or String.IsNullOrEmpty(CountryDropDown.SelectedValue) Then Return
        'If brandDropDown.SelectedValue = "" Or String.IsNullOrEmpty(brandDropDown.SelectedValue) Then Return
        'InitVehicles(brandDropDown.SelectedValue, CountryDropDown.SelectedValue)
        'Me.Brand = IIf(brandDropDown.SelectedItem.Text = "(All)", "(All)", brandDropDown.SelectedValue)
        'Me.BrandSelected = brandDropDown.SelectedItem.Text
        'If FirstLoad Then Return
        'Dim item As ListItem = Me.VehicleDropDown.Items.FindByValue(Me.Vehicle)
        'If item Is Nothing Then
        '    item = Me.VehicleDropDown.Items.FindByText(Me.Vehicle)
        'End If
        'If item Is Nothing Then
        '    Me.SetInformationMessage(BRAND_MSG)
        'Else
        '    Me.VehicleDropDown.SelectedValue = Me.Vehicle
        'End If
    End Sub

    Sub InitVehicles(ByVal Brand As String, ByVal ctycode As String)
        ''do nothing now..no more postback retrieval of data

        'Me.VehicleDropDown.Items.Clear()
        'Me.VehicleDropDown.Items.Add("(All)")
        'Dim ds As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetActiveVehicles(Brand, ctycode)
        'Me.VehicleDropDown.DataSource = ds.Tables(0)
        'Me.VehicleDropDown.DataTextField = "ProductName"
        'Me.VehicleDropDown.DataValueField = "PrdShortName"
        'Me.VehicleDropDown.DataBind()
    End Sub
#End Region

End Class
