﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="WebBookingQNRequest.aspx.vb" Inherits="BookingWebRequest_WebBookingQNRequest" EnableEventValidation="false" %>

<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<%@ Register Src="~/BookingWebRequest/Control/QNBookiListUserControl.ascx" TagName="BookingListUserControl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <link type="text/css" rel="Stylesheet" href="Control/CSS/grid.css" />
    <link type="text/css" rel="Stylesheet" href="Control/CSS/dialog.css" />
    <link type="text/css" rel="Stylesheet" href="Control/CSS/pager.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
<script language="javascript" type="text/javascript">

    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();
        if (pbControl.id.indexOf('Button') > -1 || pbControl.id.indexOf('MainGridView') > -1) {
            pbControl.disabled = true;
            Disabled(true)
        }

        

    }

    function EndRequestHandler(sender, args) {
        if (pbControl.id.indexOf('Button') > -1 || pbControl.id.indexOf('MainGridView') > -1) {
            Disabled(false) 
            pbControl.disabled = false;
        }
    }


    function Disabled(status) {
        try {
            document.getElementById('ctl00_ContentPlaceHolder_filterButton').disabled = status
            document.getElementById('ctl00_ContentPlaceHolder_filterActionButton').disabled = status
            document.getElementById('ctl00_ContentPlaceHolder_BookingListUserControl1_MainGridView').disabled = status
        }
        catch (err) {
        }
        
    }
</script>

    <div class="outer">
        <div class="inner">
            <div class="content">
                <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <br />
                        <br />
                        <asp:Panel ID="PanelHeader" runat="server" Width="800px">
                            <table style="width: 100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td style="width: 100px">
                                        Country:
                                    </td>
                                    <td style="width: 100px">
                                        <asp:DropDownList ID="CountryDropDown" runat="server" AutoPostBack="true" Width="130px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 150px">
                                        Brand:
                                    </td>
                                    <td style="width: 100px">
                                        <asp:DropDownList ID="brandDropDown" runat="server" AutoPostBack="true" Width="90px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 100px">
                                        Vehicle:
                                    </td>
                                    <td style="width: 150px">
                                        <asp:DropDownList ID="VehicleDropDown" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 150px">
                                        Agent:
                                    </td>
                                    <td style="width: 100px">
                                        <uc1:PickerControl 
                                            ID="PickerControlForAgent" 
                                            runat="server" 
                                            EnableViewState="false"
                                            Width="110" 
                                            PopupType="AGENT" 
                                            AppendDescription="true" AutoPostBack="false" />
                                     </td>
                                    <td style="width: 100px">
                                        <asp:Button ID="filterButton" runat="server" Text="Show Details" CssClass="Button_Standard"
                                            Width="100" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            
                            <table style="width: 100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td rowspan="7" style="width: 600px">
                                     <asp:HyperLink ID="b2cBookingHyperlink" runat="server" NavigateUrl="~/BookingWebRequest/WebBoookingRequest.aspx" >Link back to B2C Booking Activity </asp:HyperLink>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="actionFilterDropdown" runat="server" Width="130px" Visible="false" Enabled="false">
                                            <asp:ListItem Value="All">(All)</asp:ListItem>
                                            <asp:ListItem Value="FirstQuote">First Quote</asp:ListItem>
                                            <asp:ListItem Value="LatestQuote">Latest Quote</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="filterActionButton" runat="server" Text="Filter Quote" CssClass="Button_Standard"
                                            Width="100" Visible="false" Enabled="false"/>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            
                            <table style="width: 100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td style="width: 100%;" align="right">
                                        <asp:Literal ID="IntervalLiteral" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:BookingListUserControl id="BookingListUserControl1" runat="server">
                                                </uc1:BookingListUserControl>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <input type="hidden" runat="server" id="HiddenCollapsedOld" />
    <input type="hidden" runat="server" id="HiddenCollapsedToday" />
    <input type="hidden" runat="server" id="HiddenCollapsedYesterday" />
    <asp:Timer ID="ajaxtimer" runat="server" OnTick="ajaxtimer_Tick">
    </asp:Timer>
</asp:Content>
