<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WebBoookingRequest.aspx.vb"
    Inherits="BookingWebRequest_WebBoookingRequest" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="Control/BookingListUserControl.ascx" TagName="BookingListUserControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <link type="text/css" rel="Stylesheet" href="Control/CSS/grid.css" />
    <link type="text/css" rel="Stylesheet" href="Control/CSS/dialog.css" />
    <link type="text/css" rel="Stylesheet" href="Control/CSS/pager.css" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    
    <div class="outer">
        <div class="inner">
            <div class="content">
                <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <br />
                        <br />
                                <asp:Panel id="PanelHeader" runat="server" Width="800px">
                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td style="width: 100px">
                                                    Country:
                                                </td>
                                                <td style="width: 100px">
                                                    <asp:DropDownList ID="CountryDropDown" runat="server" autopostback="true" width="150px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 150px">
                                                    Brand:
                                                </td>
                                                <td style="width: 100px">
                                                    <asp:DropDownList ID="brandDropDown" runat="server" autopostback="true" width="100px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 100px">
                                                    Vehicle:
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:DropDownList ID="VehicleDropDown" runat="server" width="150px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 150px">
                                                    Status:
                                                </td>
                                                <td style="width: 100px">
                                                    <asp:DropDownList ID="StatusDropDownList" runat="server" width="100px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 100px">
                                                    <asp:Button id="filterButton" runat="server" text="Show Details" cssclass="Button_Standard" width="100" />
                                                </td>
                                               
                                            </tr>
                                        </table>
                                        <br />
                                        <%--<hr />--%>
                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td  rowspan="7" style="width: 600px">
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="actionFilterDropdown" runat="server" width="100px">
                                                                <asp:ListItem Value="All" >(All)</asp:ListItem>
                                                               <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                                                <asp:ListItem Value="InProgress">InProgress</asp:ListItem>
                                                            </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button id="filterActionButton" runat="server" text="Filter Action" cssclass="Button_Standard" width="100"  />
                                                </td>
                                            </tr>
                                        </table>
                                          <br />
                                       <%-- <hr />--%>
                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td style="width:100%;" align="right">
                                                <asp:Literal ID="IntervalLiteral" runat="server"></asp:Literal>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <uc1:BookingListUserControl id="BookingListUserControl1" runat="server">
                                                </uc1:BookingListUserControl>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel> 
          
                       
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>
 </div>
   
 <%--<div class="footer">
                <div class="outer"> 
                    <div class="inner">
                        <div class="content">
                        </div>
                    </div>
                </div>
            </div>--%>
    <input type="hidden" runat="server" id="HiddenCollapsedOld" />
    <input type="hidden" runat="server" id="HiddenCollapsedToday" />
    <input type="hidden" runat="server" id="HiddenCollapsedYesterday" />
    <asp:Timer id="ajaxtimer" runat="server" OnTick="ajaxtimer_Tick"></asp:Timer>
        
</asp:Content>
