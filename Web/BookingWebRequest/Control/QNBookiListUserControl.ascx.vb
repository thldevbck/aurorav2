﻿Imports System.Collections.Generic
Imports Aurora.Reservations.Data
Imports System.Data
Imports Aurora.SalesAndMarketing

Partial Class BookingWebRequest_Control_QNBookiListUserControl
    Inherits AuroraUserControl

#Region "Constant"
    Private Const SUCCESFULL As String = "Booking Actions updated successfully"
    Private Const REFRESH As String = " .Please refresh the page."
    Private Const PENDING_MESSAGE_ERROR As String = "Please select 'InProgress' or 'Complete' action when clicking Update Button."
#End Region

#Region "Property"


    Public Property counterTwodays As Integer
        Get
            Return ViewState("counterTwodays")
        End Get
        Set(ByVal value As Integer)
            ViewState("counterTwodays") = value
        End Set
    End Property


    Public Property counterFivedays As Integer
        Get
            Return ViewState("counterfivedays")
        End Get
        Set(ByVal value As Integer)
            ViewState("counterfivedays") = value
        End Set
    End Property


    Public Property counterSevendays As Integer
        Get
            Return ViewState("countersevendays")
        End Get
        Set(ByVal value As Integer)
            ViewState("countersevendays") = value
        End Set
    End Property

    Private _ByPassCaching As Boolean
    Private Property ByPassCaching As Boolean
        Get
            Return _ByPassCaching
        End Get
        Set(ByVal value As Boolean)
            _ByPassCaching = value
        End Set
    End Property

    Private _agentId As String
    Public Property AgentId As String
        Get
            Return _agentId
        End Get
        Set(ByVal value As String)
            _agentId = value
        End Set
    End Property

    Public ReadOnly Property BookingName() As String
        Get
            Return Server.HtmlEncode("<ul>Booking</ul>")
        End Get

    End Property

    Private Property UnsavedPending() As Integer
        Get
            Return ViewState("UnsavedPending")
        End Get
        Set(ByVal value As Integer)
            ViewState("UnsavedPending") = value
        End Set
    End Property

    Private ReadOnly Property TotalCount() As String
        Get
            Return GetQNCacheInfoList(ByPassCaching, "Total") ''Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalOutstanding(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private ReadOnly Property TotalInprogress() As String
        Get
            Return GetQNCacheInfoList(ByPassCaching, "Inprogress") ''Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalInProgress(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private ReadOnly Property TotalPending() As String
        Get
            Return GetQNCacheInfoList(ByPassCaching, "Pending") '' Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalPending(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private _brand As String
    Public Property Brand() As String
        Get
            Return _brand
        End Get
        Set(ByVal value As String)
            _brand = value
        End Set
    End Property

    Private _vehicle As String
    Public Property Vehicle() As String
        Get
            Return _vehicle
        End Get
        Set(ByVal value As String)
            _vehicle = value
        End Set
    End Property

    Private _status As String
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    Private _ActionFilter As String
    Public Property ActionFilter() As String
        Get
            Return _ActionFilter
        End Get
        Set(ByVal value As String)
            _ActionFilter = value
        End Set
    End Property

    Private _citycode As String
    Public Property Citycode() As String
        Get
            Return _citycode
        End Get
        Set(ByVal value As String)
            _citycode = value
        End Set
    End Property

    Private _old As String
    Public Property HiddenCollapsedOld() As String
        Get
            Return _old
        End Get
        Set(ByVal value As String)
            _old = value
        End Set
    End Property

    Private _today As String
    Public Property HiddenCollapsedToday() As String
        Get
            Return _today
        End Get
        Set(ByVal value As String)
            _today = value
        End Set
    End Property

    Private _yesterday As String
    Public Property HiddenCollapsedYesterday() As String
        Get
            Return _yesterday
        End Get
        Set(ByVal value As String)
            _yesterday = value
        End Set
    End Property
#End Region

#Region "Private Property"
    Public Property B2CBookflowApprover() As Boolean
        Get
            Try
                Return Boolean.Parse(ViewState("B2CBookflowApprover"))
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal value As Boolean)
            ViewState("B2CBookflowApprover") = value
        End Set
    End Property



    
#End Region
#Region "Procedures"

    
    Function GetQNCacheInfoList(ByVal bypasscache As Boolean, ByVal key As String) As Object

        Dim cacheitem As Object = CType(Cache(key), Object)

        If Not bypasscache Or cacheitem Is Nothing Then
            Select Case key
                Case "ListByDate"
                    cacheitem = Aurora.Reservations.Data.WebBookingItemProvider.GetQNBookingListByDate(Me.AgentId)
                Case "Total"
                    cacheitem = Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalOutstanding(MyBase.CurrentPage.UserCode, AgentId)
                Case "Inprogress"
                    cacheitem = Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalInProgress(MyBase.CurrentPage.UserCode, AgentId)
                Case "Pending"
                    cacheitem = Aurora.Reservations.Data.WebBookingItemProvider.GetBookingQNStatusTotalPending(MyBase.CurrentPage.UserCode, AgentId)
                Case Else

            End Select

            Cache.Insert(key, cacheitem, Nothing, DateTime.Now.AddSeconds(120), TimeSpan.Zero)
        End If
        Return CType(cacheitem, Object)
    End Function
    Public Sub LoadData(Optional ByVal visible As Boolean = False)
        ''Dim ds As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetQNBookingListByDate(Me.AgentId)
        Me.MainGridView.DataSource = DateDataView(CType(GetQNCacheInfoList(True, "ListByDate"), DataSet))
        Me.MainGridView.DataBind()
        Recount()

        hiddenRentalId.Value = ""
        HiddenCustomer.Value = ""

        ''MainGridView.DataBind()
        Me.counterFivedays = 0
        Me.counterTwodays = 0
        Me.counterSevendays = 0
    End Sub

    

    Private Function DateDataView(ByVal ds As DataSet) As DataView
        Dim tempTable As DataTable = New DataTable
        tempTable.Columns.Add(New DataColumn("Group", Type.GetType("System.String")))
        tempTable.Columns.Add(New DataColumn("id", Type.GetType("System.String")))
        Dim newRow As DataRow = Nothing
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            newRow = tempTable.NewRow
            newRow("Group") = row("Group").ToString
            If row("Group").ToString.Equals("Quotes 2-5 days old") Then
                newRow("id") = 1
            ElseIf row("Group").ToString.Equals("Quotes 5-6 days old") Then
                newRow("id") = 2
            Else
                newRow("id") = 3
            End If
            tempTable.Rows.Add(newRow)
        Next
        Dim dview As DataView = tempTable.DefaultView
        dview.Sort = "id asc"
        Return dview
    End Function

#End Region


#Region "Controls Events"

    Protected Sub GridView_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        If DirectCast(sender, GridView).ID.Equals("ChildGridView") = False Then Return
        If (String.IsNullOrEmpty(SortDirection)) Then
            e.SortExpression = e.SortExpression & " ASC"
            SortDirection = e.SortExpression
        Else
            If e.SortExpression = SortDirection.Split(" ")(0) Then
                If SortDirection.Split(" ")(1) = "ASC" Then
                    e.SortExpression = e.SortExpression & " DESC"
                    SortDirection = e.SortExpression
                Else
                    e.SortExpression = e.SortExpression & " ASC"
                    SortDirection = e.SortExpression
                End If
            Else
                e.SortExpression = e.SortExpression & " ASC"
                SortDirection = e.SortExpression
            End If
        End If

    End Sub

    Protected Sub GridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If DirectCast(sender, GridView).ID.Equals("ChildGridView") = False Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim objTemplate As GridViewRow = DirectCast(sender, GridView).NamingContainer
            If objTemplate IsNot Nothing Then
                Dim ObjectDataSource3 As ObjectDataSource = DirectCast(objTemplate.FindControl("ObjectDataSource3"), ObjectDataSource)
                If Not ObjectDataSource3 Is Nothing Then
                    Dim pnlDate As Panel = objTemplate.FindControl("pnlDate")
                    If pnlDate IsNot Nothing Then
                        Dim GroupHiddenfield As Label = pnlDate.FindControl("GroupHiddenfield")

                        Dim litGroup2DaysOld As HtmlGenericControl = CType(objTemplate.FindControl("litGroup2DaysOld"), HtmlGenericControl)
                        Dim litGroup5DaysOld As HtmlGenericControl = CType(objTemplate.FindControl("litGroup5DaysOld"), HtmlGenericControl)
                        Dim litGroup7DaysOld As HtmlGenericControl = CType(objTemplate.FindControl("litGroup7DaysOld"), HtmlGenericControl)

                        If Not String.IsNullOrEmpty(GroupHiddenfield.Text) Then
                            If GroupHiddenfield.Text.ToUpper.Equals("Quotes 2-5 days old".ToUpper) = True Then
                                Me.counterTwodays = CType(sender, GridView).Rows.Count + 1
                                If Not litGroup2DaysOld Is Nothing Then litGroup2DaysOld.InnerText = Me.counterTwodays

                            Else
                                If GroupHiddenfield.Text.ToUpper.Equals("Quotes 5-6 days old".ToUpper) = True Then
                                    Me.counterFivedays = CType(sender, GridView).Rows.Count + 1
                                    If Not litGroup5DaysOld Is Nothing Then litGroup5DaysOld.InnerText = Me.counterFivedays

                                Else
                                    If GroupHiddenfield.Text.ToUpper.Equals("Quotes 7 days plus".ToUpper) = True Then
                                        Me.counterSevendays = CType(sender, GridView).Rows.Count + 1
                                        If Not litGroup7DaysOld Is Nothing Then litGroup7DaysOld.InnerText = Me.counterSevendays
                                    End If
                                End If
                            End If
                        End If

                        If GroupHiddenfield IsNot Nothing Then
                            ObjectDataSource3.SelectParameters("parameterDate").DefaultValue = GroupHiddenfield.Text
                            ObjectDataSource3.SelectParameters("Brand").DefaultValue = Me.Brand
                            ObjectDataSource3.SelectParameters("Vehicle").DefaultValue = Me.Vehicle
                            ObjectDataSource3.SelectParameters("agentId").DefaultValue = Me.AgentId
                            '' ObjectDataSource3.SelectParameters("ActionFilter").DefaultValue = IIf(ActionFilter = "All", "", ActionFilter)
                            ObjectDataSource3.SelectParameters("citycode").DefaultValue = Me.Citycode
                            ObjectDataSource3.SelectParameters("usercode").DefaultValue = MyBase.CurrentPage.UserCode
                        End If

                    End If
                End If
            End If

           
            
        End If

    End Sub

  


    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row Is Nothing Then Return
        Dim btnindex As Integer = 9
        If e.Row.RowType = DataControlRowType.DataRow Then
            ''Dim updatebutton As Button = CType(e.Row.Cells(btnindex).FindControl("UpdateButton"), Button)
            ''Dim updatebutton As HtmlInputButton = CType(e.Row.Cells(btnindex).FindControl("UpdateButton"), HtmlInputButton)

            Dim bookingnumber As String = CType(e.Row.Cells(0).FindControl("hpBookingNumber"), HtmlAnchor).InnerText
            Dim action As String = ""
            Dim actionDropdown As HtmlSelect = CType(e.Row.FindControl("actionDropdown"), HtmlSelect)

            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)

            
            action = rowview("Action").ToString
            If action = "" Then
                UnsavedPending = UnsavedPending + 1
                Return
            End If

            If Not actionDropdown Is Nothing Then

                Dim hidStatusForConcurrency As HtmlInputHidden = CType(e.Row.FindControl("hidStatusForConcurrency"), HtmlInputHidden)
                If Not hidStatusForConcurrency Is Nothing Then
                    hidStatusForConcurrency.Value = rowview("TScurrency").ToString
                End If

                If action = "Completed" Then
                    e.Row.Visible = False
                    Return
                End If

                If action = "InProgress" Then
                    Dim item As ListItem = actionDropdown.Items.FindByText("Pending")
                    If item IsNot Nothing Then
                        actionDropdown.Items.Remove(item)
                    End If
                End If

                e.Row.BackColor = Aurora.Common.DataConstants.GetBookingStatusColor(e.Row.Cells(3).Text.Trim)
                actionDropdown.Value = action


            End If

            Dim litUserID As HtmlGenericControl = CType(e.Row.FindControl("litUserID"), HtmlGenericControl)
            If Not litUserID Is Nothing Then
                litUserID.InnerHtml = Server.HtmlEncode(rowview("User").ToString.TrimEnd)
            End If
        End If

    End Sub

    Protected Sub MainGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MainGridView.RowCreated
        If e.Row Is Nothing Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim group As String = DataBinder.Eval(e.Row.DataItem, "Group")
            Dim ObjectDataSource3 As ObjectDataSource = DirectCast(e.Row.FindControl("ObjectDataSource3"), ObjectDataSource)
            If Not ObjectDataSource3 Is Nothing Then
                ObjectDataSource3.SelectParameters("parameterDate").DefaultValue = group '' DataBinder.Eval(e.Row.DataItem, "Group")
                ObjectDataSource3.SelectParameters("Brand").DefaultValue = Me.Brand
                ObjectDataSource3.SelectParameters("Vehicle").DefaultValue = Me.Vehicle
                ObjectDataSource3.SelectParameters("agentId").DefaultValue = Me.AgentId
                ''ObjectDataSource3.SelectParameters("ActionFilter").DefaultValue = IIf(Me.ActionFilter = "All", "", Me.ActionFilter)
                ObjectDataSource3.SelectParameters("citycode").DefaultValue = Me.Citycode
                ObjectDataSource3.SelectParameters("usercode").DefaultValue = MyBase.CurrentPage.UserCode
            End If


            Dim litGroup2DaysOld As HtmlGenericControl = CType(e.Row.FindControl("litGroup2DaysOld"), HtmlGenericControl)
            Dim litGroup5DaysOld As HtmlGenericControl = CType(e.Row.FindControl("litGroup5DaysOld"), HtmlGenericControl)
            Dim litGroup7DaysOld As HtmlGenericControl = CType(e.Row.FindControl("litGroup7DaysOld"), HtmlGenericControl)


            If (Not String.IsNullOrEmpty(group)) AndAlso (Not litGroup2DaysOld Is Nothing) AndAlso (Not litGroup5DaysOld Is Nothing) AndAlso (Not litGroup7DaysOld Is Nothing) Then

                If group.ToUpper.Equals("Quotes 2-5 days old".ToUpper) = True Then
                    litGroup2DaysOld.Visible = True
                Else
                    If group.ToUpper.Equals("Quotes 5-6 days old".ToUpper) = True Then
                        litGroup5DaysOld.Visible = True
                    Else
                        If group.ToUpper.Equals("Quotes 7 days plus".ToUpper) = True Then
                            litGroup7DaysOld.Visible = True
                        End If
                    End If
                End If
            End If

            Dim pnlListOfBookings As Panel = CType(e.Row.FindControl("pnlListOfBookings"), Panel)
            If pnlListOfBookings IsNot Nothing Then
                Dim ChildGridView As GridView = CType(pnlListOfBookings.FindControl("ChildGridView"), GridView)
                If ChildGridView IsNot Nothing Then
                    AddHandler ChildGridView.Sorting, AddressOf GridView_Sorting
                    AddHandler ChildGridView.RowCreated, AddressOf GridView_RowCreated
                    AddHandler ChildGridView.RowDataBound, AddressOf GridView_RowDataBound
                End If
            End If


        End If

    End Sub

    Sub ObjectDataSource3_selected(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs)
        If Not e.Exception Is Nothing Then Return
    End Sub

    Function BehaviorName(ByVal behaviorgrouping As String) As String
        Return "cpeBehavior_" + behaviorgrouping.Replace(" ", "_")
    End Function

    Protected Sub MainGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MainGridView.RowDataBound
        If e.Row Is Nothing Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then

            If Not String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "Group").ToString) Then
                Dim cpe As AjaxControlToolkit.CollapsiblePanelExtender = CType(e.Row.FindControl("cpe"), AjaxControlToolkit.CollapsiblePanelExtender)
                If Not cpe Is Nothing Then
                    cpe.BehaviorID = BehaviorName(DataBinder.Eval(e.Row.DataItem, "Group"))
                End If

                Dim imgPlus As Image = CType(e.Row.FindControl("imgPlus"), Image)
                If Not imgPlus Is Nothing Then
                    imgPlus.Attributes.Add("onclick", "collapsed('" & cpe.BehaviorID & "');")
                End If

                Dim pnlDate As Panel = e.Row.FindControl("pnlDate")
                If pnlDate IsNot Nothing Then
                    pnlDate.Attributes.Add("onclick", "collapsed('" & cpe.BehaviorID & "');")
                End If

                Dim ChildGridView As GridView = CType(e.Row.FindControl("ChildGridView"), GridView)
                If ChildGridView IsNot Nothing Then
                    ChildGridView.Enabled = B2CBookflowApprover
                End If
            End If
        End If
    End Sub



#End Region


#Region "Control Load"
    Protected Sub cpe_PreRender(ByVal sender As Object, ByVal e As EventArgs)

        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Quotes_5-6_days_old" Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedYesterday) And Page.IsPostBack Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                Return
            End If
            If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Not Page.IsPostBack Then
                If Request.RawUrl.IndexOf("WebBoookingRequest") <> -1 Then
                    CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                    CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                    Return
                End If
            End If

            If String.IsNullOrEmpty(Me.HiddenCollapsedYesterday) Then Me.HiddenCollapsedYesterday = True
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedYesterday)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedYesterday)
        End If


        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Quotes_7_days_plus" Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedOld) Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False

                Return
            End If

            If String.IsNullOrEmpty(Me.HiddenCollapsedOld) Then Me.HiddenCollapsedOld = True
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedOld)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedOld)
        End If

        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Quotes_2-5_days_old" Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Page.IsPostBack Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                Return
            Else
                If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Not Page.IsPostBack Then
                    If Request.RawUrl.IndexOf("WebBoookingRequest") <> -1 Then
                        CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                        CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                        Return
                    End If
                End If

            End If
            If String.IsNullOrEmpty(Me.HiddenCollapsedToday) Then Me.HiddenCollapsedToday = True
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedToday)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedToday)
        End If

    End Sub
#End Region

#Region "events"
    Protected Sub Update(ByVal sender As Object, ByVal e As EventArgs)

        ''Dim btn As Button = CType(sender, Button)
        Dim btn As HtmlInputButton = CType(sender, HtmlInputButton)
        Dim objtemplate As GridViewRow = btn.NamingContainer
        Dim gridactive As GridView = objtemplate.NamingContainer
        Dim bookingnumber As String = CType(objtemplate.Cells(0).FindControl("hpBookingNumber"), HtmlAnchor).InnerText

        Dim btnactionindex As Integer = 7
        Dim action As String = CType(objtemplate.Cells(8).FindControl("actionDropdown"), HtmlSelect).Value

        If action.Equals("Pending") Then
            MyBase.CurrentPage.SetWarningMessage(PENDING_MESSAGE_ERROR)
            Exit Sub
        End If
        Dim userid As String = MyBase.CurrentPage.UserCode
        Dim hidStatusForConcurrency As HtmlInputHidden = CType(objtemplate.FindControl("hidStatusForConcurrency"), HtmlInputHidden)
        
        Dim output As String = Aurora.Reservations.Data.WebBookingItemProvider.B2CQNBookingActivityActions(bookingnumber, action, userid, hidStatusForConcurrency.Value)

        Dim nameindex As Integer = 8
        If String.IsNullOrEmpty(output) Or output.Equals("Error") Then
            MyBase.CurrentPage.SetErrorMessage("Error updating Booking Actions")
        ElseIf output.Contains("ErrorConcurrency") Then
            MyBase.CurrentPage.SetWarningMessage(output.Replace("ErrorConcurrency:", "") & REFRESH)
        Else
            objtemplate.Cells(nameindex).Text = userid
            ByPassCaching = False

            MyBase.CurrentPage.SetInformationMessage(SUCCESFULL)
            Recount()
            ''MainGridView.DataBind()
            gridactive.DataBind()

        End If
    End Sub

    Private Sub Recount()
        UnsavedPending = 0
        totalTextbox.Text = TotalCount
        InProgressTextBox.Text = TotalInprogress
        PendingTextBox.Text = TotalPending

        Dim UnSaveTotal As String() = TotalPending.Split("/")
        If UnSaveTotal.Length = 2 Then
            UnsavedPending = CInt(UnSaveTotal(0)) + CInt(UnSaveTotal(1))
        Else
            UnsavedPending = TotalPending
        End If
        UnsavePendingTextBox.Text = UnsavedPending

        If Visible Then
            OutstandingTable.Visible = Visible
        Else
            OutstandingTable.Visible = Page.IsPostBack
        End If

    End Sub

    Property SortDirection As String
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
        Get
            Return ViewState("SortDirection")
        End Get
    End Property


    

#End Region


#Region "Unsused"

    Public Function FullNameVariation(ByVal objName As String) As String
        Dim tempName As String = ""
        Try
            If objName Is Nothing Then
                Return ""
            End If

            Dim txtName As String = CType(objName, String)
            Dim indexOfComma As Integer = txtName.IndexOf(",")

            If indexOfComma > 0 Then
                If txtName.Substring(indexOfComma + 1, 1) = " " Then
                    indexOfComma = indexOfComma + 1
                    txtName = txtName.Remove(indexOfComma, 1)
                End If
                indexOfComma = txtName.IndexOf(",")
                If txtName.Substring(indexOfComma - 1, 1) = " " Then
                    indexOfComma = indexOfComma - 1
                    txtName = txtName.Remove(indexOfComma, 1)
                End If
            End If

            Dim flagForNewVariation As Boolean = False
            For i As Integer = 0 To txtName.TrimEnd.Length - 1
                If i = 0 Then
                    tempName = tempName & txtName.Substring(i, 1).ToUpper
                Else
                    If Char.IsLetter(txtName.Substring(i, 1)) Then
                        If flagForNewVariation = True Then
                            tempName = tempName & txtName.Substring(i, 1).ToUpper
                        Else
                            tempName = tempName & txtName.Substring(i, 1).ToLower
                        End If
                        flagForNewVariation = False
                    ElseIf txtName.Substring(i, 1) = "'" Or txtName.Substring(i, 1) = " " Then
                        tempName = tempName & txtName.Substring(i, 1)
                        flagForNewVariation = True
                    Else

                        If (txtName.Substring(i, 1) = ",") Then
                            tempName = tempName & ","
                            flagForNewVariation = True
                            Continue For
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
        End Try
        Return tempName
    End Function

    Function FilterBasedOnAction(ByVal mode As String, _
                               ByVal value As String, _
                               ByVal previousvalue As String, _
                               ByVal e As GridViewRowEventArgs, _
                               ByVal sender As GridView) As Boolean
        If mode = "(All)" Then Return False
        Dim hide As Boolean = False
        Dim rentalArrayCurrent() As String = value.Split("-")
        Dim rentalArrayPrevious() As String = Nothing
        If Not String.IsNullOrEmpty(previousvalue) Then
            rentalArrayPrevious = previousvalue.Split("-")
        End If

        ''System.Diagnostics.Debug.Assert(value <> "B1213062-10")
        ''System.Diagnostics.Debug.Assert(previousvalue <> "B1213062-2")
        If rentalArrayCurrent.Length - 1 <> -1 AndAlso rentalArrayPrevious.Length - 1 <> -1 Then
            Dim prev As String = rentalArrayPrevious(0).TrimEnd
            Dim cur As String = rentalArrayCurrent(0).TrimEnd
            If prev.Equals(cur) Then
                prev = rentalArrayPrevious(1).TrimEnd
                cur = rentalArrayCurrent(1).TrimEnd
                If mode.ToUpper.IndexOf("FIRST") <> -1 Then
                    If CInt(cur) < CInt(prev) Then
                        DirectCast(sender, GridView).Rows(e.Row.RowIndex - 1).Visible = False
                    Else
                        hide = True
                    End If
                ElseIf mode.ToUpper.IndexOf("LATEST") <> -1 Then
                    If CInt(cur) > CInt(prev) Then
                        DirectCast(sender, GridView).Rows(e.Row.RowIndex - 1).Visible = False
                    Else
                        hide = True
                    End If
                End If

            End If
        End If

        Return hide
    End Function

    Public Sub Highlightrows(ByVal rows As ArrayList)
        For Each erow As GridViewRow In MainGridView.Rows
            If erow.RowType = DataControlRowType.DataRow Then
                Dim pnlListOfBookings As Panel = CType(erow.FindControl("pnlListOfBookings"), Panel)
                If Not pnlListOfBookings Is Nothing Then
                    Dim ChildGridView As GridView = CType(pnlListOfBookings.FindControl("ChildGridView"), GridView)
                    For Each erowchild As GridViewRow In ChildGridView.Rows
                        If erowchild.RowType = DataControlRowType.DataRow Then
                            If erowchild.RowIndex >= 0 Then
                                Dim index As Integer = rows.IndexOf(erowchild.RowIndex)
                                If index >= 0 Then
                                    erowchild.BackColor = Drawing.Color.Aqua
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Next
    End Sub
#End Region
   
End Class
