Imports System.Collections.Generic
Imports Aurora.Reservations.Data
Imports System.data
Imports Aurora.SalesAndMarketing

Partial Class BookingWebRequest_Control_BookingListUserControl
    Inherits AuroraUserControl

#Region "Constant"
    Private Const SUCCESFULL As String = "Booking Actions updated successfully"
    Private Const REFRESH As String = " .Please refresh the page."
    Private Const PENDING_MESSAGE_ERROR As String = "Please select 'InProgress' or 'Complete' action when clicking Update Button."
#End Region

#Region "Protected property"

    Public ReadOnly Property BookingName() As String
        Get
            Return Server.HtmlEncode("<ul>Booking</ul>")
        End Get
        
    End Property

    Private Property UnsavedPending() As Integer
        Get
            Return ViewState("UnsavedPending")
        End Get
        Set(ByVal value As Integer)
            ViewState("UnsavedPending") = value
        End Set
    End Property

   


    Private ReadOnly Property TotalCount() As String
        Get
            Return Aurora.Reservations.Data.WebBookingItemProvider.GetBookingStatusTotalOutstanding(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private ReadOnly Property TotalInprogress()
        Get
            Return Aurora.Reservations.Data.WebBookingItemProvider.GetBookingStatusTotalInProgress(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private ReadOnly Property TotalPending() As String
        Get
            Return Aurora.Reservations.Data.WebBookingItemProvider.GetBookingStatusTotalPending(MyBase.CurrentPage.UserCode)
        End Get
    End Property

    Private _brand As String
    Public Property Brand() As String
        Get
            Return _brand
        End Get
        Set(ByVal value As String)
            _brand = value
        End Set
    End Property

    Private _vehicle As String
    Public Property Vehicle() As String
        Get
            Return _vehicle
        End Get
        Set(ByVal value As String)
            _vehicle = value
        End Set
    End Property

    Private _status As String
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    Private _ActionFilter As String
    Public Property ActionFilter() As String
        Get
            Return _ActionFilter
        End Get
        Set(ByVal value As String)
            _ActionFilter = value
        End Set
    End Property

    Private _citycode As String
    Public Property Citycode() As String
        Get
            Return _citycode
        End Get
        Set(ByVal value As String)
            _citycode = value
        End Set
    End Property

    Private _old As String
    Public Property HiddenCollapsedOld() As String
        Get
            Return _old
        End Get
        Set(ByVal value As String)
            _old = value
        End Set
    End Property

    Private _today As String
    Public Property HiddenCollapsedToday() As String
        Get
            Return _today
        End Get
        Set(ByVal value As String)
            _today = value
        End Set
    End Property

    Private _yesterday As String
    Public Property HiddenCollapsedYesterday() As String
        Get
            Return _yesterday
        End Get
        Set(ByVal value As String)
            _yesterday = value
        End Set
    End Property
#End Region

#Region "Private Property"
    Public Property B2CBookflowApprover() As Boolean
        Get
            Try
                Return Boolean.Parse(ViewState("B2CBookflowApprover"))
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal value As Boolean)
            ViewState("B2CBookflowApprover") = value
        End Set
    End Property

#End Region

#Region "private Procedure"

    Private Function DateDataView(ByVal ds As DataSet) As DataView
        Dim tempTable As DataTable = New DataTable
        tempTable.Columns.Add(New DataColumn("Group", Type.GetType("System.String")))
        tempTable.Columns.Add(New DataColumn("id", Type.GetType("System.String")))
        Dim newRow As DataRow = Nothing
        Dim row As DataRow
        For Each row In ds.Tables(0).Rows
            newRow = tempTable.NewRow
            newRow("Group") = row("Group").ToString
            If row("Group").ToString.Equals("Today") Then
                newRow("id") = 1
            ElseIf row("Group").ToString.Equals("Yesterday") Then
                newRow("id") = 2
            Else
                newRow("id") = 3
            End If
            tempTable.Rows.Add(newRow)
        Next
        Dim dview As DataView = tempTable.DefaultView
        dview.Sort = "id asc"
        Return dview
    End Function

    Protected Sub Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim btn As Button = CType(sender, Button)
        Dim objtemplate As GridViewRow = btn.NamingContainer
        Dim bookingnumber As String = CType(objtemplate.Cells(0).FindControl("hpBookingNumber"), HyperLink).Text
        Dim action As String = CType(objtemplate.Cells(8).FindControl("actionDropdown"), DropDownList).SelectedItem.Text
        If action.Equals("Pending") Then
            MyBase.CurrentPage.SetWarningMessage(PENDING_MESSAGE_ERROR)
            Exit Sub
        End If
        Dim userid As String = MyBase.CurrentPage.UserCode
        Dim hiddenStatusForConcurrency As HiddenField = CType(objtemplate.FindControl("hiddenStatusForConcurrency"), HiddenField)
        Dim output As String = Aurora.Reservations.Data.WebBookingItemProvider.B2CBookingActivityActions(bookingnumber, action, userid, hiddenStatusForConcurrency.Value)
        If String.IsNullOrEmpty(output) Or output.Equals("Error") Then
            MyBase.CurrentPage.SetErrorMessage("Error updating Booking Actions")
        ElseIf output.Contains("ErrorConcurrency") Then
            MyBase.CurrentPage.SetWarningMessage(output.Replace("ErrorConcurrency:", "") & REFRESH)
        Else
            objtemplate.Cells(9).Text = userid
            Recount()
            MyBase.CurrentPage.SetInformationMessage(SUCCESFULL)
        End If

    End Sub

    Private Sub Recount()
        UnsavedPending = 0
        totalTextbox.Text = TotalCount
        InProgressTextBox.Text = TotalInprogress
        PendingTextBox.Text = TotalPending

        Dim UnSaveTotal As String() = TotalPending.Split("/")
        If UnSaveTotal.Length = 2 Then
            UnsavedPending = CInt(UnSaveTotal(0)) + CInt(UnSaveTotal(1))
        Else

            UnsavedPending = TotalPending
        End If
        UnsavePendingTextBox.Text = UnsavedPending

        If Visible Then
            OutstandingTable.Visible = Visible
        Else
            OutstandingTable.Visible = Page.IsPostBack
        End If

    End Sub
#End Region

#Region "public Procedures"


    Public Sub LoadData(Optional ByVal visible As Boolean = False)
        UnsavedPending = 0

        Dim ds As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetBookingListByDate()
        Me.MainGridView.DataSource = DateDataView(ds)
        Me.MainGridView.DataBind()

        totalTextbox.Text = TotalCount
        InProgressTextBox.Text = TotalInprogress
        PendingTextBox.Text = TotalPending

        Dim UnSaveTotal As String() = TotalPending.Split("/")
        If UnSaveTotal.Length = 2 Then
            UnsavedPending = CInt(UnSaveTotal(0)) + CInt(UnSaveTotal(1))
        Else

            UnsavedPending = TotalPending
        End If
        UnsavePendingTextBox.Text = UnsavedPending

        If visible Then
            OutstandingTable.Visible = visible
        Else
            OutstandingTable.Visible = Page.IsPostBack
        End If

    End Sub

    Public Function FullNameVariation(ByVal objName As String) As String
        Dim tempName As String = ""
        ''System.Diagnostics.Debug.Assert(objName <> "Diez d'Aux, Michael")
        System.Diagnostics.Debug.WriteLine(objName)
        Try
            If objName Is Nothing Then
                Return ""
            End If

            Dim txtName As String = CType(objName, String)
            Dim indexOfComma As Integer = txtName.IndexOf(",")

            If indexOfComma > 0 Then
                If txtName.Substring(indexOfComma + 1, 1) = " " Then
                    indexOfComma = indexOfComma + 1
                    txtName = txtName.Remove(indexOfComma, 1)
                End If
                indexOfComma = txtName.IndexOf(",")
                If txtName.Substring(indexOfComma - 1, 1) = " " Then
                    indexOfComma = indexOfComma - 1
                    txtName = txtName.Remove(indexOfComma, 1)
                End If
            End If

            Dim flagForNewVariation As Boolean = False
            For i As Integer = 0 To txtName.TrimEnd.Length - 1
                If i = 0 Then
                    tempName = tempName & txtName.Substring(i, 1).ToUpper
                Else
                    If Char.IsLetter(txtName.Substring(i, 1)) Then
                        If flagForNewVariation = True Then
                            tempName = tempName & txtName.Substring(i, 1).ToUpper
                        Else
                            tempName = tempName & txtName.Substring(i, 1).ToLower
                        End If
                        flagForNewVariation = False
                    ElseIf txtName.Substring(i, 1) = "'" Or txtName.Substring(i, 1) = " " Then
                        tempName = tempName & txtName.Substring(i, 1)
                        flagForNewVariation = True
                    Else

                        If (txtName.Substring(i, 1) = ",") Then
                            tempName = tempName & ","
                            flagForNewVariation = True
                            Continue For
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
        End Try
        System.Diagnostics.Debug.WriteLine(tempName)
        System.Diagnostics.Debug.WriteLine("")
        Return tempname
    End Function
#End Region

#Region "Controls Events"

    Protected Sub GridView_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        If DirectCast(sender, GridView).ID.Equals("ChildGridView") = False Then Return
    End Sub

    Protected Sub GridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If DirectCast(sender, GridView).ID.Equals("ChildGridView") = False Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim objTemplate As GridViewRow = DirectCast(sender, GridView).NamingContainer
            If objTemplate IsNot Nothing Then
                Dim ObjectDataSource2 As ObjectDataSource = DirectCast(objTemplate.FindControl("ObjectDataSource2"), ObjectDataSource)
                If Not ObjectDataSource2 Is Nothing Then
                    Dim pnlDate As Panel = objTemplate.FindControl("pnlDate")
                    If pnlDate IsNot Nothing Then
                        Dim GroupHiddenfield As Label = pnlDate.FindControl("GroupHiddenfield")
                        If GroupHiddenfield IsNot Nothing Then
                            ObjectDataSource2.SelectParameters("parameterDate").DefaultValue = GroupHiddenfield.Text
                            ObjectDataSource2.SelectParameters("Brand").DefaultValue = Me.Brand
                            ObjectDataSource2.SelectParameters("Vehicle").DefaultValue = Me.Vehicle
                            ObjectDataSource2.SelectParameters("Status").DefaultValue = Me.Status
                            ObjectDataSource2.SelectParameters("ActionFilter").DefaultValue = IIf(ActionFilter = "All", "", ActionFilter)
                            ObjectDataSource2.SelectParameters("citycode").DefaultValue = Me.Citycode
                            ObjectDataSource2.SelectParameters("usercode").DefaultValue = MyBase.CurrentPage.UserCode
                        End If
                    End If
                End If
            End If
        Else
            'If e.Row.RowType = DataControlRowType.Header Then
            '    Dim col As DataControlField = Nothing
            '    Dim gv As GridView = DirectCast(sender, GridView)

            '    If e.Row.RowType = DataControlRowType.Header Then
            '        For i As Integer = 0 To gv.Columns.Count - 1
            '            col = gv.Columns(i)

            '            If Not String.IsNullOrEmpty(col.HeaderText) Then

            '                If col.HeaderText <> "Details" And _
            '                   col.HeaderText <> "Action" And _
            '                   col.HeaderText <> "User" And _
            '                   col.HeaderText.Length <> 0 Then
            '                    col.HeaderText = Server.HtmlEncode("<ul>" & col.HeaderText & "</ul>")
            '                End If
            '            End If
            '        Next
            '    End If
            'End If
        End If
        
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row Is Nothing Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim updatebutton As Button = CType(e.Row.Cells(10).FindControl("UpdateButton"), Button)
            Dim bookingnumber As String = CType(e.Row.Cells(0).FindControl("hpBookingNumber"), HyperLink).Text.TrimEnd
            Dim action As String = ""
            Dim actionDropdown As DropDownList = CType(e.Row.FindControl("actionDropdown"), DropDownList)


            '' Dim ds As DataSet = Aurora.Reservations.Data.WebBookingItemProvider.GetB2CBookingActivityActions(bookingnumber)
            'If ds Is Nothing Then Return

            'If ds.Tables(0).Rows.Count = 0 Then
            '    UnsavedPending = UnsavedPending + 1
            '    Return
            'End If

            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            action = rowview("Action").ToString
            If action = "" Then
                UnsavedPending = UnsavedPending + 1
                Return
            End If

            If Not actionDropdown Is Nothing Then
                ''action = ds.Tables(0).Rows(0)(0).ToString.TrimEnd

                Dim hiddenStatusForConcurrency As HiddenField = CType(e.Row.FindControl("hiddenStatusForConcurrency"), HiddenField)
                If hiddenStatusForConcurrency IsNot Nothing Then
                    ''hiddenStatusForConcurrency.Value = ds.Tables(0).Rows(0)(2).ToString.TrimEnd
                    hiddenStatusForConcurrency.Value = rowview("TScurrency").ToString ''DataBinder.Eval(e.Row.DataItem, "TScurrency")
                End If

                If action = "Completed" Then
                    e.Row.Visible = False
                    Return
                End If

                If action = "InProgress" Then
                    Dim item As ListItem = actionDropdown.Items.FindByText("Pending")
                    If item IsNot Nothing Then
                        actionDropdown.Items.Remove(item)
                    End If
                End If

                e.Row.BackColor = Aurora.Common.DataConstants.GetBookingStatusColor(e.Row.Cells(3).Text.Trim)
                actionDropdown.SelectedValue = action

                
            End If


            Dim labelUserID As Label = CType(e.Row.FindControl("labelUserID"), Label)
            If Not labelUserID Is Nothing Then
                Dim user As String = rowview("User").ToString ''DataBinder.Eval(e.Row.DataItem, "User") ''ds.Tables(0).Rows(0)(1).ToString
                labelUserID.Text = user
            End If
       

        End If

    End Sub

    Protected Sub MainGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MainGridView.RowCreated
        If e.Row Is Nothing Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ObjectDataSource2 As ObjectDataSource = DirectCast(e.Row.FindControl("ObjectDataSource2"), ObjectDataSource)
            If Not ObjectDataSource2 Is Nothing Then
                ObjectDataSource2.SelectParameters("parameterDate").DefaultValue = DataBinder.Eval(e.Row.DataItem, "Group")
                ObjectDataSource2.SelectParameters("Brand").DefaultValue = Me.Brand
                ObjectDataSource2.SelectParameters("Vehicle").DefaultValue = Me.Vehicle
                ObjectDataSource2.SelectParameters("Status").DefaultValue = Me.Status
                ObjectDataSource2.SelectParameters("ActionFilter").DefaultValue = IIf(Me.ActionFilter = "All", "", Me.ActionFilter)
                ObjectDataSource2.SelectParameters("citycode").DefaultValue = Me.Citycode
                ObjectDataSource2.SelectParameters("usercode").DefaultValue = MyBase.CurrentPage.UserCode
            End If

           
            Dim pnlListOfBookings As Panel = CType(e.Row.FindControl("pnlListOfBookings"), Panel)
            If pnlListOfBookings IsNot Nothing Then
                Dim ChildGridView As GridView = CType(pnlListOfBookings.FindControl("ChildGridView"), GridView)
                If ChildGridView IsNot Nothing Then
                    AddHandler ChildGridView.Sorting, AddressOf GridView_Sorting
                    AddHandler ChildGridView.RowCreated, AddressOf GridView_RowCreated
                    AddHandler ChildGridView.RowDataBound, AddressOf GridView_RowDataBound
                End If
            End If
        End If

    End Sub

    Protected Sub MainGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MainGridView.RowDataBound
        If e.Row Is Nothing Then Return
        If e.Row.RowType = DataControlRowType.DataRow Then
            

            Dim KBlabel As TextBox = CType(e.Row.FindControl("KBTextbox"), TextBox)
            If KBlabel IsNot Nothing Then
                KBlabel.Text = Aurora.Reservations.Data.WebBookingItemProvider.KBcount
            End If

            Dim WLLabel As TextBox = CType(e.Row.FindControl("WLTextbox"), TextBox)
            If WLLabel IsNot Nothing Then
                WLLabel.Text = Aurora.Reservations.Data.WebBookingItemProvider.WLcount
            End If


            Dim cpe As AjaxControlToolkit.CollapsiblePanelExtender = CType(e.Row.FindControl("cpe"), AjaxControlToolkit.CollapsiblePanelExtender)
            If Not cpe Is Nothing Then
                Dim behaviorName As String = "cpeBehavior_" & DataBinder.Eval(e.Row.DataItem, "Group")
                cpe.BehaviorID = "cpeBehavior_" & DataBinder.Eval(e.Row.DataItem, "Group")
            End If

            Dim imgPlus As Image = CType(e.Row.FindControl("imgPlus"), Image)
            If Not imgPlus Is Nothing Then
                imgPlus.Attributes.Add("onclick", "collapsed('" & cpe.BehaviorID & "');")
            End If

            Dim pnlDate As Panel = e.Row.FindControl("pnlDate")
            If pnlDate IsNot Nothing Then
                pnlDate.Attributes.Add("onclick", "collapsed('" & cpe.BehaviorID & "');")
            End If

            Dim ChildGridView As GridView = CType(e.Row.FindControl("ChildGridView"), GridView)
            If ChildGridView IsNot Nothing Then
                ChildGridView.Enabled = B2CBookflowApprover
            End If
        End If

    End Sub

    Protected Sub DropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'ActionFilter = CType(sender, DropDownList).SelectedValue
        'Me.LoadData()
        'CType(sender, DropDownList).SelectedValue = ActionFilter
    End Sub

#End Region

#Region "Control Load"
    Protected Sub cpe_PreRender(ByVal sender As Object, ByVal e As EventArgs)

        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Yesterday" Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedYesterday) And Page.IsPostBack Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                Return
            End If
            If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Not Page.IsPostBack Then
                If Request.RawUrl.IndexOf("WebBoookingRequest") <> -1 Then
                    CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                    CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                    Return
                End If
            End If

            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedYesterday)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedYesterday)
        End If

        ''If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Old" Then
        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID).ToString.IndexOf("cpeBehavior_Older than Yesterday") <> -1 Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedOld) Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False

                Return
            End If

            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedOld)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedOld)
        End If

        If (CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).BehaviorID) = "cpeBehavior_Today" Then
            If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Page.IsPostBack Then
                ''INITIAL VALUE IS true
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                Return
            Else
                If String.IsNullOrEmpty(Me.HiddenCollapsedToday) And Not Page.IsPostBack Then
                    If Request.RawUrl.IndexOf("WebBoookingRequest") <> -1 Then
                        CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                        CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = False
                        Return
                    End If
                End If

            End If

            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = Not Boolean.Parse(Me.HiddenCollapsedToday)
            CType(sender, AjaxControlToolkit.CollapsiblePanelExtender).ClientState = Not Boolean.Parse(Me.HiddenCollapsedToday)
        End If

    End Sub
#End Region

End Class
