<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingListUserControl.ascx.vb"
    Inherits="BookingWebRequest_Control_BookingListUserControl" %>
<%@ Register Src="../../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>

<script language="javascript" type="text/javascript">
           
           var objPanelExtender;

            function controlLoad(sender) {
                objPanelExtender = $find(sender)
                objPanelExtender.add_expandComplete(getCollapsibleState);
                objPanelExtender.add_collapseComplete(getCollapsibleState); 
            }
            function collapsed(sender)
                {
                   // alert(sender);
                     objPanelExtender = $find(sender)
                     if(objPanelExtender.get_Collapsed())
                                {
                                    
                                    if(sender.indexOf('Old')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedOld').value ="true";
                                            return;
                                        }  
                                    
                                    if(sender.indexOf('Today')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedToday').value ="true";
                                            return;
                                        }            
                                    
                                    if(sender.indexOf('Yesterday')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedYesterday').value ="true";
                                            return;
                                        }                
                                }
                            else
                                {
                                    if(sender.indexOf('Old')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedOld').value ="false";
                                            return;
                                        }  
                                    
                                    if(sender.indexOf('Today')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedToday').value ="false";
                                            return;
                                        }            
                                    
                                    if(sender.indexOf('Yesterday')>-1)
                                        {
                                            $get('ctl00_ContentPlaceHolder_HiddenCollapsedYesterday').value ="false";
                                            return;
                                        }                
                                }
                                
                                
                }
//                function HookedDropDownList(dropdown,updatebutton)
//                    {
//                            var ddl = document.getElementById(dropdown)
//                            var index = ddl.selectedIndex;
//                            var value  = ddl.options[index].value;
//                            var btn = document.getElementById(updatebutton)
//                            if(value != 'Pending')
//                                {
//                                            
//                                            btn.disabled = false;
//                                            btn.style.backgroundImage ='url(~/Images/button_bg.png)';
//                                            btn.style.backgroundRepeat='repeat-x';
//                                            btn.style.backgroundColor = 'Transparent';
//                                }
//                            else
//                                {
//                                            btn.disabled = true;
//                                }
//                            return true;
//                    }
</script>
<div id="dlg" class="dialog" style="width: 100%">
           <div class="header" style="cursor: default; height:30px; width:100%;" >
                 <div class="outer">
                    <div class="inner">
                        <div class="content">
                             <table id="OutstandingTable" runat="server" visible="false" width="100%" border="1">
                             <thead>
                                    <tr>
                                        <td>
                                            <b>Total Outstanding [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="totalTextbox" runat="server" ReadOnly="true" Width="50"  ToolTip ="Total number of Waitlisted(WL) and KnockBack(KB) status" Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <b>Total In-Progress [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="InProgressTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of In-Progress status that were updated." Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pending [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="PendingTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of Pending status that were updated." Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <%--<b>Current Pending:</b>--%>
                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Pending:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="UnsavePendingTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of Pending status that are waiting to be updated." Font-Size="X-Small"/>
                                        </td>
                                    </tr>
                                </thead>
                        </table>
                        </div>
                    </div>
                </div>
           </div>
    </div>
     
<asp:GridView ID="MainGridView" runat="server" AutoGenerateColumns="False" Width="100%"
    ShowHeader="false" AllowPaging="false" EnableViewState="true" CssClass="grid">
     <RowStyle CssClass="row" />
     <AlternatingRowStyle CssClass="altrow" />
        <Columns>
    
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="pnlDate" runat="server" CssClass="group">
                   <%-- <span class="header">--%>
                   <span>
                        <table width="100%" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td>
                                    <asp:Image CssClass="first" runat="server" ID="imgPlus" ImageUrl="~/Images/plus.png"
                                        Style="margin-right: 5px"/>
                                </td>
                                <td style="width:285px;">
                                    <%#Eval("Group").ToString.ToUpper%>
                                </td>
                                <td >
                                    Waitlisted:&nbsp;&nbsp; 
                                    <asp:TextBox ID="WLTextbox" runat="server" ReadOnly="true" Width="50" ToolTip ="Number of Waitlisted(WL) record." Font-Size="X-Small"/>
                                </td>
                                
                                <td style="width:285px;">
                                    Knockback: 
                                    <asp:TextBox ID="KBTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Number of KnockBack(KB) record." Font-Size="X-Small"/>
                                </td>
                                
                            </tr>
                        </table>
                    </span>
                    <asp:Label ID="GroupHiddenfield" Text='<%#Eval("Group")%>' runat="server" Visible="false" />
                </asp:Panel>
                <%--<hr />--%>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetBookingItemsDSFiltered"
                    TypeName="Aurora.Reservations.Data.WebBookingItemProvider">
                    <SelectParameters>
                        <asp:Parameter Name="parameterDate" Type="String" DefaultValue="" />
                        <asp:Parameter Name="Brand" Type="String" DefaultValue="" />
                        <asp:Parameter Name="Vehicle" Type="String" DefaultValue="" />
                        <asp:Parameter Name="Status" Type="String" DefaultValue="" />
                        <asp:Parameter Name="ActionFilter" Type="String" DefaultValue="" />
                        <asp:Parameter Name="citycode" Type="String" DefaultValue="" />
                        <asp:Parameter Name="usercode" Type="String" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Panel ID="pnlListOfBookings" runat="server" Style="margin-left: 20px; margin-right: 20px">
                    <asp:GridView ID="ChildGridView" runat="server" AutoGenerateColumns="false" Width="100%"
                        DataSourceID="ObjectDataSource2" CssClass="dataTableGrid" AllowSorting="true"
                        EnableViewState="true">
                        <%--<RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />--%>
                       
                        <Columns>
                            <asp:TemplateField HeaderText="Booking" ItemStyle-Width="100px" SortExpression="bookingnumber">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpBookingNumber" runat="server" Text='<%# eval("RntId") %>' NavigateUrl='<%# "~/Booking/Booking.aspx?activeTab=7&hdRentalId=" + eval("RntId")+ "&hdBookingId=" + eval("BookingNumber") %>'   />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="brand" HeaderText="Brand" SortExpression="Brand" ItemStyle-Width="100" />
                            
                             <asp:TemplateField HeaderText="Name" ItemStyle-Width="80px" SortExpression="customername" >
                                <ItemTemplate>
                                    <asp:Label ID="labelCustomerName" Text = '<%# FullNameVariation(eval("customername")) %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                               
                            <asp:BoundField DataField="status" HeaderText="St" SortExpression="status" />
                            <%--<asp:BoundField DataField="dateRequest" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"
                                SortExpression="dateRequest" />--%>
                            <asp:TemplateField HeaderText="Date" ItemStyle-Width="80px" SortExpression="dateRequest" >
                                <ItemTemplate>
                                    <asp:Label ID="labeldateRequest" Text = '<%# Eval("dateRequest","{0:dd/MM/yyyy}").ToString.TrimEnd %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>    
                            <asp:TemplateField HeaderText="Details" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <span>
                                        <table width="100%" cellspacing="0" cellpadding="0"  class="dataTableGrid">
                                            <tr class="evenRow">
                                                <td style="width:75px;">
                                                      <%#Eval("checkoutdate", "{0:dd/MM/yyyy}").ToString.TrimEnd%>  
                                                </td>
                                                 <td align="right">
                                                     <b> <%#Eval("checkoutbranch").ToString.TrimEnd %>   </b>
                                                </td>
                                            </tr>
                                            <tr class="oddRow">
                                                <td style="width:75px;">
                                                        <%#Eval("checkindate", "{0:dd/MM/yyyy}").ToString.TrimEnd%>
                                                </td>
                                                 <td align="right">
                                                        <b><%#Eval("checkinbranch").ToString.TrimEnd %></b>
                                                </td>
                                            </tr>
                                        </table>
                                      <%--  <%#Eval("checkoutdate","{0:MM/dd/yyyy}")%>
                                        &nbsp;&nbsp;&nbsp;<b><%#Eval("checkoutbranch")%></b>
                                        <%#Eval("checkindate", "{0:MM/dd/yyyy}")%>
                                        &nbsp;&nbsp;&nbsp;<b><%#Eval("checkinbranch")%></b> --%>
                                   </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="lengthofhire" HeaderText="Days" SortExpression="lengthofhire"
                                ItemStyle-Width="15" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="vehicle" HeaderText="Vehicle" SortExpression="vehicle" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:DropDownList ID="actionDropdown" runat="server" Width="100px">
                                        <asp:ListItem Value="Pending" Selected="True">Pending</asp:ListItem>
                                        <asp:ListItem Value="InProgress">InProgress</asp:ListItem>
                                        <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hiddenStatusForConcurrency" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" ItemStyle-Width="50">
                                <ItemTemplate>
                                    <asp:Label ID="labelUserID" runat="Server" Text='<%= MyBase.CurrentPage.UserCode %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="UpdateButton" Text="Update" runat="server" CssClass="Button_Standard"
                                        Width="50px" OnClick="Update"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width: 100%" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <thead>
                                    <tr class="oddRow">
                                        <td>
                                            Booking
                                        </td>
                                        <td>
                                            Brand
                                        </td>
                                        <td>
                                            Name
                                        </td>
                                        <td>
                                            St
                                        </td>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            Details
                                        </td>
                                        <td>
                                            Days
                                        </td>
                                        <td>
                                            Vehicle
                                        </td>
                                        <td>
                                            Action
                                        </td>
                                        <td>
                                            User
                                        </td>
                                    </tr>
                                </thead>
                                <tr class="evenRow">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
                <br />
                <ajaxToolkit:CollapsiblePanelExtender ID="cpe" runat="Server" TargetControlID="pnlListOfBookings"
                    CollapsedSize="0" Collapsed="True" ExpandControlID="pnlDate" CollapseControlID="pnlDate"
                    AutoCollapse="False" AutoExpand="False" ScrollContents="false" ImageControlID="imgPlus"
                    ExpandedImage="../../Images/minus.png" CollapsedImage="../../Images/plus.png"
                    ExpandDirection="Vertical" OnPreRender="cpe_PreRender" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

