﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="QNBookiListUserControl.ascx.vb" Inherits="BookingWebRequest_Control_QNBookiListUserControl" %>
<%@ Register Src="../../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>

<script language="javascript" type="text/javascript">

    var objPanelExtender;

    function controlLoad(sender) {
        objPanelExtender = $find(sender)
        objPanelExtender.add_expandComplete(getCollapsibleState);
        objPanelExtender.add_collapseComplete(getCollapsibleState);
    }
    function collapsed(sender) {
        
        objPanelExtender = $find(sender)
        if (objPanelExtender.get_Collapsed()) {
            
            if (sender.indexOf('cpeBehavior_Quotes_7_days_plus') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedOld').value = "true";
                return;
            }

            if (sender.indexOf('cpeBehavior_Quotes_2-5_days_old') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedToday').value = "true";
                return;
            }

            if (sender.indexOf('cpeBehavior_Quotes_5-6_days_old') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedYesterday').value = "true";
                return;
            }
        }
        else {
            if (sender.indexOf('cpeBehavior_Quotes_7_days_plus') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedOld').value = "false";
                return;
            }

            if (sender.indexOf('cpeBehavior_Quotes_2-5_days_old') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedToday').value = "false";
                return;
            }

            if (sender.indexOf('cpeBehavior_Quotes_5-6_days_old') > -1) {
                $get('ctl00_ContentPlaceHolder_HiddenCollapsedYesterday').value = "false";
                return;
            }
        }


    }
    
</script>
<div id="dlg" class="dialog" style="width: 100%">
           <div class="header" style="cursor: default; height:30px; width:100%;" >
                 <div class="outer">
                    <div class="inner">
                        <div class="content">
                             <table id="OutstandingTable" runat="server" visible="false" width="100%" border="1">
                             <thead>
                                    <tr>
                                        <td>
                                            <b>Total Outstanding [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="totalTextbox" runat="server" ReadOnly="true" Width="50"  ToolTip ="Total number of Booking Number(QN) status" Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <b>Total In-Progress [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="InProgressTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of Booking Number(QN) In-Progress  status that were updated." Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pending [AU/NZ]:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="PendingTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of Booking Number(QN) Pending status that were updated." Font-Size="X-Small"/>
                                        </td>
                                        <td>
                                            <%--<b>Current Pending:</b>--%>
                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Pending:</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="UnsavePendingTextBox" runat="server" ReadOnly="true" Width="50" ToolTip ="Total number of Pending status that are waiting to be updated." Font-Size="X-Small"/>
                                        </td>
                                    </tr>
                                </thead>
                        </table>
                        </div>
                    </div>
                </div>
           </div>
    </div>
     
<asp:GridView ID="MainGridView" runat="server" AutoGenerateColumns="False" Width="100%"
    ShowHeader="false" AllowPaging="false" EnableViewState="true" CssClass="grid">
     <RowStyle CssClass="row" />
     <AlternatingRowStyle CssClass="altrow" />
        <Columns>
    
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="pnlDate" runat="server" CssClass="group">
                   <%-- <span class="header">--%>
                   <span>
                        <table width="100%" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td>
                                    <asp:Image CssClass="first" runat="server" ID="imgPlus" ImageUrl="~/Images/plus.png"
                                        Style="margin-right: 5px" ENABLEVIEWSTATE = "false"/>
                                </td>
                                <td style="width:90%;" align="left">
                                    <%#Eval("Group").ToString.ToUpper%>
                                </td>
                                <td style="width:10%;" align="left">
                                  Count: &nbsp;
                                  <literal id = "litGroup2DaysOld" runat="server"  />
                                  <literal id = "litGroup5DaysOld" runat="server"  />
                                  <literal id = "litGroup7DaysOld" runat="server"  />
                                </td>
                            </tr>
                        </table>
                    </span>
                    <asp:Label ID="GroupHiddenfield" Text='<%#Eval("Group")%>' runat="server" Visible="false" ENABLEVIEWSTATE = "false"/>
                </asp:Panel>
                
              <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
                     SelectMethod="GetQNBookingItemsDSFiltered" 
                     TypeName="Aurora.Reservations.Data.WebBookingItemProvider" SortParameterName="sorting">
                        <SelectParameters>
                            <asp:Parameter Name="parameterDate" Type="String" />
                            <asp:Parameter Name="Brand" Type="String" />
                            <asp:Parameter Name="Vehicle" Type="String" />
                            <asp:Parameter Name="agentId" Type="String" />
                            <asp:Parameter Name="ActionFilter" Type="String" />
                            <asp:Parameter Name="citycode" Type="String" />
                            <asp:Parameter Name="usercode" Type="String" />
                            <asp:Parameter Name="sorting" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                <asp:Panel ID="pnlListOfBookings" runat="server" Style="margin-left: 20px; margin-right: 20px">
                    <asp:GridView ID="ChildGridView" runat="server" AutoGenerateColumns="false" Width="100%"
                        DataSourceID="ObjectDataSource3" CssClass="dataTableGrid" AllowSorting="true"
                        EnableViewState="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Booking" ItemStyle-Width="150px" SortExpression="bookingnumber">
                                <ItemTemplate>
                                    <a id="hpBookingNumber" runat="server" href='<%# "~/Booking/Booking.aspx?activeTab=7&hdRentalId=" + eval("RntId")+ "&hdBookingId=" + eval("BookingNumber") %>'><%# eval("RntId") %></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="brand" HeaderText="Brand" SortExpression="Brand" ItemStyle-Width="100" />
                            <asp:BoundField DataField="customername" HeaderText="Name" SortExpression="customername" ItemStyle-Width="80"  />
                            <asp:BoundField DataField="dateExpiry" HeaderText="Exp.Date" SortExpression="dateExpiry" ItemStyle-Width="80"  DataFormatString="{0:dd/MM/yyyy}" />

                            <asp:TemplateField HeaderText="Details" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <span>
                                        <table width="100%" cellspacing="0" cellpadding="0"  class="dataTableGrid">
                                            <tr class="evenRow">
                                                <td style="width:75px;">
                                                      <%#Eval("checkoutdate", "{0:dd/MM/yyyy}").ToString.TrimEnd%>  
                                                </td>
                                                 <td align="right">
                                                     <b> <%#Eval("checkoutbranch").ToString.TrimEnd %>   </b>
                                                </td>
                                            </tr>
                                            <tr class="oddRow">
                                                <td style="width:75px;">
                                                        <%#Eval("checkindate", "{0:dd/MM/yyyy}").ToString.TrimEnd%>
                                                </td>
                                                 <td align="right">
                                                        <b><%#Eval("checkinbranch").ToString.TrimEnd %></b>
                                                </td>
                                            </tr>
                                        </table>
                                   </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="lengthofhire" HeaderText="Days" SortExpression="lengthofhire"
                                ItemStyle-Width="10" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="vehicle" HeaderText="Vehicle" SortExpression="vehicle" />
                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <select id="actionDropdown" runat="server" Width="80px">
                                        <option value="Pending" selected="selected">Pending</option>
                                        <option value="InProgress">InProgress</option>
                                        <option value="Completed">Completed</option>
                                    </select>
                                    <input id="hidStatusForConcurrency" type="hidden" runat="server" ENABLEVIEWSTATE = "false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                             
                            <asp:TemplateField HeaderText="User" ItemStyle-Width="50">
                                <ItemTemplate>
                                     <literal id = "litUserID" runat="server"></literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                        <input type="button" ID="UpdateButton" runat="server" 
                                        class="Button_Standard" style="width: 50px;height: 20px;"   onserverclick="Update" value="Update">

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width: 100%" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <thead>
                                    <tr class="oddRow">
                                        <td>
                                            Booking
                                        </td>
                                        <td>
                                            Brand
                                        </td>
                                        <td>
                                            Name
                                        </td>
                                        <td>
                                            QN #
                                        </td>
                                        <td>
                                            Exp.Date
                                        </td>
                                        <td>
                                            Details
                                        </td>
                                        <td>
                                            Days
                                        </td>
                                        <td>
                                            Vehicle
                                        </td>
                                        <td>
                                            Action
                                        </td>
                                        <td>
                                            User
                                        </td>
                                    </tr>
                                </thead>
                                <tr class="evenRow">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
                <br />
                <ajaxToolkit:CollapsiblePanelExtender ID="cpe" runat="Server" TargetControlID="pnlListOfBookings"
                    CollapsedSize="0" Collapsed="True" ExpandControlID="pnlDate" CollapseControlID="pnlDate"
                    AutoCollapse="False" AutoExpand="False" ScrollContents="false" ImageControlID="imgPlus"
                    ExpandedImage="../../Images/minus.png" CollapsedImage="../../Images/plus.png"
                    ExpandDirection="Vertical" OnPreRender="cpe_PreRender" ENABLEVIEWSTATE = "false" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:HiddenField ID="hiddenRentalId" runat="server" Value="" />
<asp:HiddenField ID="HiddenCustomer" runat="server" Value="" />




 

