
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class LogOff
    Inherits AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Abandon()
        System.Web.Security.FormsAuthentication.SignOut()
    End Sub
End Class
