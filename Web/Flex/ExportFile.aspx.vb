Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services

<AuroraPageTitleAttribute("View File")> _
<AuroraFunctionCodeAttribute("FLEXEXPORT")> _
Partial Class Flex_ExportFile
    Inherits AuroraPage

    Private selectedFwxId As Integer
    Private export As Export = Nothing
    Private bookingWeek As BookingWeek = Nothing
    Private flexBookingWeekBrandDataRow As FlexBookingWeekBrandRow = Nothing

    Private Sub BuildExport(ByVal useViewState As Boolean)
        export = New Export(Me.CompanyCode, selectedFwxId)
        bookingWeek = New BookingWeek(Me.CompanyCode, export.WeekExportRow.FbwId)
        If Not export.WeekExportRow.IsFlxBrdCodeNull Then
            flexBookingWeekBrandDataRow = bookingWeek.FlexBookingWeekBrandRow(export.WeekExportRow.FlxBrdCode)
        End If

        bookingWeekTextBox.Text = Utility.DateUIToString(bookingWeek.BookingWeekRow.Description)
        bookingWeekTextBox.Style.Add("background-color", ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor))

        bookingWeekStatusTextBox.Text = bookingWeek.BookingWeekRow.StatusText
        bookingWeekStatusTextBox.Style.Add("background-color", ColorTranslator.ToHtml(export.WeekExportRow.BookingWeekStatusColor))

        If flexBookingWeekBrandDataRow Is Nothing Then
            brandTextBox.Text = Me.CompanyCode & " - " & Me.CompanyName
            brandTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor) + ";")
            brandStatusTextBox.Text = bookingWeek.BookingWeekRow.StatusText
            brandStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor) + ";")
            brandImage.ImageUrl = "~/Images/Company/" & Me.CompanyCode & ".png"
        Else
            brandTextBox.Text = flexBookingWeekBrandDataRow.Description
            brandTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekBrandDataRow.StatusColor) + ";")
            brandStatusTextBox.Text = flexBookingWeekBrandDataRow.StatusText
            brandStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekBrandDataRow.StatusColor) + ";")
            brandImage.ImageUrl = "~/Images/Brand/" & flexBookingWeekBrandDataRow.FbbBrdCode & ".png"
        End If

        nameTextBox.Text = Server.HtmlEncode(export.WeekExportRow.Description)

        statusTextBox.Text = export.WeekExportRow.StatusText
        statusTextBox.Style.Add("background-color", ColorTranslator.ToHtml(export.WeekExportRow.StatusColor))

        locationLabel.Text = Server.HtmlEncode("(" + UserSettings.Current.LocCode + ")")

        emailCheckBox.Checked = export.ContactsDescription.Trim().Length > 0
        ftpCheckBox.Checked = export.WeekExportRow.FlxIsFtp
        auroraCheckBox.Checked = export.WeekExportRow.FlxIsAurora

        emailContactsTextBox.Text = export.ContactsDescription

        filenameTextBox.Text = export.WeekExportRow.FileName
        textTextBox.Text = export.Text

        If export.WeekExportRow.IsReadonly OrElse Not useViewState Then
            integrityHiddenField.Value = CStr(export.WeekExportRow.IntegrityNo)
            scheduledDateTextBox.Date = export.WeekExportRow.FwxScheduledTime.AddHours(-UserSettings.Current.LocHoursBehindServerLocation)
            scheduledTimeTextBox.Time = export.WeekExportRow.FwxScheduledTime.AddHours(-UserSettings.Current.LocHoursBehindServerLocation).TimeOfDay
        End If

        Dim isReadonly As Boolean = export.WeekExportRow.IsReadonly
        scheduledDateTextBox.ReadOnly = isReadonly
        scheduledTimeTextBox.ReadOnly = isReadonly
        saveButton.Visible = Not isReadonly
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildExport(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        selectedFwxId = Integer.Parse(Request.QueryString("FwxId"))
        If Me.IsPostBack Then
            Return
        End If

        BuildForm(False)
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        BuildForm(True)

        If Not scheduledDateTextBox.IsValid OrElse Not scheduledTimeTextBox.IsValid Then
            SetWarningMessage("Specify a valid scheduled to send date and time")
            Return
        End If

        Dim scheduledTime As Date = (scheduledDateTextBox.Date + scheduledTimeTextBox.Time).AddHours(UserSettings.Current.LocHoursBehindServerLocation)

        ' save (does an overall validate too)
        Try
            export.Save(scheduledTime, export.WeekExportRow.IntegrityNo, Me.UserCode)
        Catch ex As ValidationException
            SetWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        Catch ex As SaveException
            SetWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        ' rebuild the form with the newly saved data....
        BuildForm(False)

        SetInformationMessage("Saved")
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("ExportRates.aspx")
    End Sub

    Protected Sub textDownloadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles textDownloadButton.Click
        BuildForm(False)

        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(export.WeekExportRow.FileName))
        Response.ContentType = "text/plain"
        Response.Write(export.Text)
        Response.End()
    End Sub
End Class
