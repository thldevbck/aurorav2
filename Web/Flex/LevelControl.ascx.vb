Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

Partial Class Flex_LevelControl
    Inherits System.Web.UI.UserControl

    Public Class LevelCell
        Private parent As Flex_LevelControl
        Private flexInt As Integer
        Private col As Integer
        Private row As Integer

        Private levelTableCell As WebControls.TableCell

        Private created As Boolean = False

        Public Sub New(ByVal parent As Flex_LevelControl, ByVal flexInt As Integer)
            Me.parent = parent
            Me.flexInt = flexInt
            Me.col = flexInt Mod 8 + 1
            Me.row = Int(flexInt / 8) + 1
        End Sub

        Public Sub CreateControls()
            levelTableCell = parent.levelTable.Rows(row + 1).Cells(col)
            levelTableCell.ID = "level" + CStr(flexInt) + "Cell"
            levelTableCell.BackColor = FlexConstants.FlexIntToColor(flexInt)

            created = True
            Refresh()
        End Sub

        Private Sub Refresh()
            If created Then
                If ("" + Amount).Trim().Length > 0 Then
                    levelTableCell.Text = HttpUtility.HtmlEncode(Amount)
                Else
                    levelTableCell.Text = "&nbsp;"
                End If
            End If
        End Sub

        Public Property Amount() As String
            Get
                Return CStr(parent.ViewState("Amount" + CStr(flexInt)))
            End Get
            Set(ByVal value As String)
                parent.ViewState("Amount" + CStr(flexInt)) = value
                Refresh()
            End Set
        End Property

    End Class

    Private levelCellsByFlextInt As New Dictionary(Of Integer, LevelCell)
    Public ReadOnly Property Cells(ByVal flexInt As Integer) As LevelCell
        Get
            Return levelCellsByFlextInt(flexInt)
        End Get
    End Property


    Public Sub New()
        For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
            levelCellsByFlextInt.Add(flexInt, New LevelCell(Me, flexInt))
        Next
    End Sub

    Protected Overrides Sub CreateChildControls()
        MyBase.CreateChildControls()

        For Each levelCell As LevelCell In levelCellsByFlextInt.Values
            levelCell.CreateControls()
        Next
    End Sub

End Class
