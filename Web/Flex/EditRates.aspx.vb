' 12.8.10   Shoel   Fix for call 30405

Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services

<AuroraFunctionCodeAttribute("FLEXEDIT")> _
Partial Class Flex_EditRates
    Inherits AuroraPage

    Public Const Flex_FbwId_CookieName As String = "Flex_FbwId"
    Public Const Flex_CtyCode_CookieName As String = "Flex_CtyCode"
    Public Const Flex_BrdCode_CookieName As String = "Flex_BrdCode"
    Public Const Flex_TravelYear_CookieName As String = "Flex_TravelYear"
    Public Const Flex_PrdId_CookieName As String = "Flex_PrdId"
    Public Const Flex_PkgId_CookieName As String = "Flex_PkgId"
    Public Const Flex_SapId_CookieName As String = "Flex_SapId"
    ''REV:MIA ADDED 
    Public Const Flex_Include_Domestic_CookieName As String = "Flex_Include_Domestic"

    Private selectedFbwId As Integer = Integer.MinValue
    Private selectedCtyCode As String = ""
    Private selectedBrdCode As String = ""
    Private selectedTravelYear As Integer = Integer.MinValue
    Private selectedPrdId As String = ""
    Private selectedPkgId As String = ""
    Private selectedSapId As String = ""

    Private bookingWeeks As BookingWeeks = Nothing
    Private bookingWeek As BookingWeek = Nothing
    Private flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Nothing
    Private product As Product = Nothing
    Private package As Package = Nothing

    Private Sub GetSelectedFromUI()
        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_FbwId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_FbwId_CookieName).Value) Then
            selectedFbwId = Aurora.Common.Utility.ParseInt(Request.Cookies(Flex_FbwId_CookieName).Value, Integer.MinValue)
        ElseIf Not String.IsNullOrEmpty(bookingWeekDropDown.SelectedValue) Then
            selectedFbwId = bookingWeekDropDown.SelectedValue
        Else
            selectedFbwId = Integer.MinValue
        End If

        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_BrdCode_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_BrdCode_CookieName).Value) Then
            selectedBrdCode = Request.Cookies(Flex_BrdCode_CookieName).Value
        ElseIf Not String.IsNullOrEmpty(brandDropDown.SelectedValue) Then
            selectedBrdCode = brandDropDown.SelectedValue
            brandImage.ImageUrl = "~/Images/Brand/" & brandDropDown.SelectedValue & ".png"
        Else
            selectedBrdCode = ""
        End If

        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_CtyCode_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_CtyCode_CookieName).Value) _
         AndAlso Not Request.Cookies(Flex_BrdCode_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_BrdCode_CookieName).Value) _
         AndAlso Not Request.Cookies(Flex_TravelYear_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_TravelYear_CookieName).Value) _
         AndAlso Utility.ParseInt(Request.Cookies(Flex_TravelYear_CookieName).Value, Integer.MinValue) <> Integer.MinValue Then
            selectedCtyCode = Request.Cookies(Flex_CtyCode_CookieName).Value
            selectedTravelYear = Utility.ParseInt(Request.Cookies(Flex_TravelYear_CookieName).Value)
        ElseIf Not String.IsNullOrEmpty(countryBrandYearDropDown.SelectedValue) Then
            selectedCtyCode = countryBrandYearDropDown.SelectedValue.Split(",")(0)
            selectedTravelYear = Utility.ParseInt(countryBrandYearDropDown.SelectedValue.Split(",")(2))
        Else
            selectedCtyCode = ""
            selectedTravelYear = Integer.MinValue
        End If

        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_PrdId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_PrdId_CookieName).Value) Then
            selectedPrdId = Request.Cookies(Flex_PrdId_CookieName).Value
        Else
            selectedPrdId = productDropDown.SelectedValue
        End If

        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_PkgId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_PkgId_CookieName).Value) _
         AndAlso Not Request.Cookies(Flex_SapId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_SapId_CookieName).Value) Then
            selectedPkgId = Request.Cookies(Flex_PkgId_CookieName).Value
            selectedSapId = Request.Cookies(Flex_SapId_CookieName).Value
        ElseIf Not String.IsNullOrEmpty(packageDropDown.SelectedValue) Then
            selectedPkgId = packageDropDown.SelectedValue.Split(",")(0)
            selectedSapId = packageDropDown.SelectedValue.Split(",")(1)
        Else
            selectedPkgId = ""
            selectedSapId = ""
        End If

        
        ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_CopiedPrdId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_CopiedPrdId_CookieName).Value) Then
            selectedCopiedFromPrdId = Request.Cookies(Flex_CopiedPrdId_CookieName).Value
        Else
            selectedCopiedFromPrdId = copyFromProduct.SelectedValue
        End If
    End Sub

    Private Sub BuildBookingWeeks()
        bookingWeeks = New BookingWeeks(Me.CompanyCode)

        bookingWeekDropDown.Items.Clear()
        For Each bookingWeeksRow As FlexBookingWeekRow In bookingWeeks.FlexBookingWeekDataTable
            Dim listItem As New ListItem(bookingWeeksRow.Description, bookingWeeksRow.FbwId)
            listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeeksRow.StatusColor) + ";")
            bookingWeekDropDown.Items.Add(listItem)
        Next

        ' There should always be booking weeks
        ' Even if none in the database, current and next booking weeks are mocked for the create button
        If bookingWeekDropDown.Items.FindByValue(selectedFbwId) Is Nothing Then
            selectedFbwId = bookingWeeks.CurrentFbwId
        End If

        bookingWeekDropDown.SelectedValue = selectedFbwId
    End Sub

    Private Sub BuildBookingWeek(ByVal useViewState As Boolean)
        Dim flexBookingWeekRow As FlexBookingWeekRow = bookingWeeks.FlexBookingWeekDataTable.FindByFbwId(selectedFbwId)
        bookingWeekStatusTextBox.Text = flexBookingWeekRow.StatusText
        bookingWeekStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekRow.StatusColor) + ";")
        bookingWeek = New BookingWeek(flexBookingWeekRow)

        If flexBookingWeekRow.FbwId < 0 Then
            selectedCtyCode = ""
            selectedBrdCode = ""
            selectedTravelYear = Integer.MinValue
            selectedPrdId = ""
            selectedPkgId = ""
            ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
            selectedCopiedFromPrdId = ""
            bookingWeekCreateButton.Visible = True
            productTable.Style.Add("display", "none")
            Return
        End If

        bookingWeekCreateButton.Visible = False

        brandDropDown.Items.Clear()
        For Each flexBookingWeekBrandDataRow0 As FlexBookingWeekBrandRow In bookingWeek.FlexBookingWeekBrandDataTable
            Dim listItem As New ListItem(flexBookingWeekBrandDataRow0.Description, flexBookingWeekBrandDataRow0.FbbBrdCode)
            ListItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekBrandDataRow0.StatusColor) + ";")
            brandDropDown.Items.Add(ListItem)
        Next
        If Not brandDropDown.Items.FindByValue(selectedBrdCode) Is Nothing Then
            brandDropDown.SelectedValue = selectedBrdCode
        Else
            brandDropDown.SelectedIndex = 0
            selectedBrdCode = brandDropDown.SelectedValue
        End If
        brandImage.ImageUrl = "~/Images/Brand/" & brandDropDown.SelectedValue & ".png"

        flexBookingWeekBrandRow = bookingWeek.FlexBookingWeekBrandRow(selectedBrdCode)

        brandStatusTextBox.BackColor = flexBookingWeekBrandRow.StatusColor
        brandStatusTextBox.Text = flexBookingWeekBrandRow.StatusText

        countryBrandYearDropDown.Items.Clear()
        For Each countryBrandYearsRow As CountryBrandYearsRow In bookingWeek.CountryBrandYearsDataTable
            If countryBrandYearsRow.BrdCode = selectedBrdCode Then
                countryBrandYearDropDown.Items.Add(New ListItem(countryBrandYearsRow.CtyBrdYrName, countryBrandYearsRow.CtyBrdYrCode))
            End If
        Next
        If Not countryBrandYearDropDown.Items.FindByValue(selectedCtyCode + "," + selectedBrdCode + "," + selectedTravelYear.ToString()) Is Nothing Then
            countryBrandYearDropDown.SelectedValue = selectedCtyCode + "," + selectedBrdCode + "," + selectedTravelYear.ToString()
        ElseIf countryBrandYearDropDown.Items.Count > 0 Then
            countryBrandYearDropDown.SelectedIndex = 0
            selectedCtyCode = countryBrandYearDropDown.SelectedValue.Split(",")(0)
            selectedTravelYear = Integer.Parse(countryBrandYearDropDown.SelectedValue.Split(",")(2))
        Else
            countryBrandYearDropDown.Items.Insert(0, New ListItem("(none)", ""))
            countryBrandYearDropDown.SelectedValue = ""
            selectedCtyCode = ""
            selectedTravelYear = Integer.MinValue
        End If

        ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        productDropDown.Items.Clear()
        If Not String.IsNullOrEmpty(selectedCtyCode) _
         AndAlso Not String.IsNullOrEmpty(selectedBrdCode) _
         AndAlso selectedTravelYear <> Integer.MinValue Then
            Dim weekProducts As List(Of WeekProductsRow) = bookingWeek.WeekProductsByCountryBrandCode(selectedCtyCode + "," + selectedBrdCode + "," + selectedTravelYear.ToString())
            For Each weekProductsRow As WeekProductsRow In weekProducts
                Dim listItem As New ListItem(weekProductsRow.PrdDesc, weekProductsRow.PrdId)
                listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(weekProductsRow.StatusColor) + ";")
                productDropDown.Items.Add(listItem)
            Next
        End If
        If Not productDropDown.Items.FindByValue(selectedPrdId) Is Nothing Then
            productDropDown.SelectedValue = selectedPrdId
        ElseIf productDropDown.Items.Count > 0 Then
            productDropDown.SelectedIndex = 0
            selectedPrdId = productDropDown.SelectedValue
        Else
            productDropDown.Items.Insert(0, New ListItem("(none)", ""))
            productDropDown.SelectedValue = ""
            selectedPrdId = ""
        End If

        ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        If Not copyFromProduct.Items.FindByValue(selectedCopiedFromPrdId) Is Nothing Then
            copyFromProduct.SelectedValue = selectedCopiedFromPrdId
        ElseIf copyFromProduct.Items.Count > 0 Then
            copyFromProduct.SelectedIndex = 0
            selectedCopiedFromPrdId = copyFromProduct.SelectedValue
        Else
            copyFromProduct.Items.Insert(0, New ListItem("(none)", ""))
            copyFromProduct.SelectedValue = ""
            selectedCopiedFromPrdId = ""
        End If

        productTable.Style.Add("display", "")
    End Sub

    Private Sub BuildProduct(ByVal useViewState As Boolean)
        If Not bookingWeek Is Nothing _
         AndAlso Not String.IsNullOrEmpty(selectedCtyCode) _
         AndAlso Not String.IsNullOrEmpty(selectedBrdCode) _
         AndAlso selectedTravelYear <> Integer.MinValue _
         AndAlso Not String.IsNullOrEmpty(selectedPrdId) Then
            product = New Product(bookingWeek.WeekProductsDataTable.FindByCtyCodeBrdCodeTravelYearFbwIdPrdId(selectedCtyCode, selectedBrdCode, New Date(selectedTravelYear, 4, 1), selectedFbwId, selectedPrdId))

            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 commented this thing
            ''------------------------------------------------------------------------------
            'productStatusTextBox.Text = product.WeekProductRow.StatusText
            'productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(product.WeekProductRow.StatusColor) + ";")

            ''rev:mia 1-Dec-2016
            DisabledCopyFrom(IIf(product.WeekProductRow.StatusText = "approved", False, True))
            
            productIncludedLocTextBox.Text = product.LocationsIncludedDescription
            productExcludedLocTextBox.Text = product.LocationsExcludedDescription

            If bookingWeek.BookingWeekRow.IsReadonly OrElse Not useViewState Then
                integrityHiddenField.Value = CStr(product.WeekProductRow.IntegrityNo)
            End If

            packageDropDown.Items.Clear()
            For Each weekProductPackagesRow As WeekProductPackagesRow In product.WeekProductPackagesDataTable
                packageDropDown.Items.Add(New ListItem( _
                    weekProductPackagesRow.PkgCode & " , " & weekProductPackagesRow.PrdShortName & "-" + weekProductPackagesRow.SapSuffix.ToString(), _
                    weekProductPackagesRow.PkgId & "," & weekProductPackagesRow.SapId))
            Next

            If Not packageDropDown.Items.FindByValue(selectedPkgId & "," & selectedSapId) Is Nothing Then
                packageDropDown.SelectedValue = selectedPkgId & "," & selectedSapId
            Else
                If packageDropDown.Items.Count > 0 Then
                    packageDropDown.SelectedIndex = 0
                    selectedPkgId = packageDropDown.Items(0).Value.Split(",")(0)
                    selectedSapId = packageDropDown.Items(0).Value.Split(",")(1)
                Else
                    packageDropDown.Items.Insert(0, New ListItem("(none)", ""))
                    packageDropDown.SelectedValue = ""
                    selectedPkgId = ""
                    selectedSapId = ""
                End If
            End If
            If Not String.IsNullOrEmpty(selectedPkgId) AndAlso Not String.IsNullOrEmpty(selectedSapId) Then
                Dim weekProductPackagesRow As WeekProductPackagesRow = product.WeekProductPackagesDataTable.FindByPkgIdSapId(selectedPkgId, selectedSapId)
                packageNameTextBox.Text = Server.HtmlEncode(weekProductPackagesRow.PkgName)
                packageBookingTextBox.Text = Server.HtmlEncode(weekProductPackagesRow.BookedDateDesc)
                packageTravelTextBox.Text = Server.HtmlEncode(weekProductPackagesRow.TravelDateDesc)
            Else
                packageNameTextBox.Text = ""
                packageBookingTextBox.Text = ""
                packageTravelTextBox.Text = ""
            End If

            Dim isDomestic As Boolean = IIf(FlexGridradio.SelectedValue.Equals("D"), True, False)

            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 commented this thing
            ''------------------------------------------------------------------------------

            Dim hasRecord As Boolean = False
            ''For Each productRatesRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable
            For Each productRatesRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateRowsForINTL_DOM(isDomestic)

                If (isDomestic = True) Then
                    If product.HasDomesticRecord Then
                        productStatusTextBox.Text = IIf(String.IsNullOrEmpty(productRatesRow.FlrFlpStatus), "unapproved", productRatesRow.FlrFlpStatus)
                        hasRecord = True
                        If productRatesRow.FlrFlpStatus = "approved" Then
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_ApprovedColor) + ";")
                        Else
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_NotApprovedColor) + ";")
                        End If
                    Else
                        ''this is a  history
                        productStatusTextBox.Text = product.WeekProductRow.StatusText
                        If productStatusTextBox.Text = "approved" Then
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_ApprovedColor) + ";")
                        Else
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_NotApprovedColor) + ";")
                        End If
                    End If

                Else
                    ''this is a  history
                    If product.WeekProductRow.StatusText = "approved" Then
                        productStatusTextBox.Text = product.WeekProductRow.StatusText
                        productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_ApprovedColor) + ";")
                    Else
                        productStatusTextBox.Text = IIf(String.IsNullOrEmpty(productRatesRow.FlrFlpStatus), "unapproved", productRatesRow.FlrFlpStatus)
                        hasRecord = True
                        If productRatesRow.FlrFlpStatus = "approved" Then
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_ApprovedColor) + ";")
                        Else
                            productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_NotApprovedColor) + ";")
                        End If
                    End If
                    
                End If

                


                ''System.Diagnostics.Debug.Assert(productRatesRow.FlrWkNo < 51)
        Dim rateCell As Flex_RatesControl.RateCell = ratesControl.Cells(productRatesRow.FlrWkNo)
        rateCell.TravelStart = productRatesRow.FlrTravelFrom.Day.ToString() + "/" + productRatesRow.FlrTravelFrom.Month.ToString()
        If bookingWeek.BookingWeekRow.IsReadOnly OrElse product.IsRatePast(productRatesRow) OrElse Not useViewState Then
            If Not productRatesRow.IsFlrFlexNumNull Then
                rateCell.FlexNum = productRatesRow.FlrFlexNum
                rateCell.IsError = False
            Else
                rateCell.FlexNum = FlexConstants.FlexNumDefault
                rateCell.IsError = False
            End If
        End If
        If Not productRatesRow.IsFlrFlexNumNull Then
            rateCell.PrevFlexNum = IIf(productRatesRow.FlrChanged, FlexConstants.FlexNumDefault, productRatesRow.FlrFlexNum)
        Else
            rateCell.PrevFlexNum = IIf(Not product.IsRatePast(productRatesRow), -1, FlexConstants.FlexNumDefault)
                End If


        rateCell.FleetStatus = product.FleetStatusByWkNo(productRatesRow.FlrWkNo)
        rateCell.IsPast = product.IsRatePast(productRatesRow)
        rateCell.IsCurrent = product.IsRateCurrent(productRatesRow)
        rateCell.IsReadOnly = bookingWeek.BookingWeekRow.IsReadOnly

        ' Phoenix change
        Dim saExceptions, saException As String()
        saExceptions = product.FleetStatusRow.ExceptionList.Split(","c)

        Dim sbExceptionList As StringBuilder = New StringBuilder
        Dim bHasAtleast1Exception As Boolean = False


        For i As Integer = 0 To saExceptions.Length - 1
            saException = saExceptions(i).Split("-"c)

            If productRatesRow.FlrWkNo.ToString() = saException(0) Then
                bHasAtleast1Exception = True
                sbExceptionList.Append(saException(1) & "-" & saException(2) & ",")
            End If
        Next

        If bHasAtleast1Exception Then
            rateCell.fleetStatusTextBox.Style("background-image") = "url( ../images/framelarge.png )"
            rateCell.fleetStatusTextBox.Attributes.Add("onmouseover", "showFloatingDiv(event,true,'" & sbExceptionList.ToString() & "');")
            rateCell.fleetStatusTextBox.Attributes.Add("onmouseout", "showFloatingDiv(event,false);")
        End If
        ' Phoenix change

            Next

            If hasRecord = False AndAlso product.HasDomesticRecord = True Then
                productStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.ProductStatus_NotApprovedColor) + ";")
            End If


            exceptionControl.LocationListItems.Clear()
            For Each locationRow As LocationsRow In product.LocationsDataTable
                exceptionControl.LocationListItems.Add(New ListItem(locationRow.Description, locationRow.LocCode))
            Next

            If bookingWeek.BookingWeekRow.IsReadOnly OrElse Not useViewState Then
                exceptionControl.Clear()
                Dim row As Integer = 0
                While row < FlexConstants.ExceptionMax
                    Dim exceptionRow As Flex_ExceptionControl.ExceptionRow = exceptionControl.Rows(row)
                    Dim productException As FlexBookingWeekExceptionRow = product.FlexBookingWeekExceptionDataTable(row)
                    If Not productException.IsFleIdNull() Then
                        exceptionRow.Id = productException.FleId
                    Else
                        exceptionRow.Id = 0
                    End If
                    exceptionRow.LocationCode = productException.LocationCode
                    exceptionRow.IsFrom = productException.IsFrom
                    exceptionRow.IsTo = productException.IsTo
                    exceptionRow.BookFrom = productException.FleBookStart
                    exceptionRow.BookTo = productException.FleBookEnd
                    exceptionRow.TravelFrom = productException.FleTravelStart
                    exceptionRow.TravelTo = productException.FleTravelEnd
                    exceptionRow.Change = productException.FleDelta
                    exceptionRow.IsReadOnly = bookingWeek.BookingWeekRow.IsReadOnly
                    exceptionRow.IsError = False
                    row += 1
                End While
            End If

            'ratesControl.Visible = True
            'exceptionControl.Visible = True

            approveButton.Visible = bookingWeek.CanApproveBookingWeekBrand(selectedBrdCode) AndAlso product.WeekProductRow.CanApprove()
            ''rejectButton.Visible = bookingWeek.CanApproveBookingWeekBrand(selectedBrdCode) AndAlso product.WeekProductRow.CanReject()
            rejectButton.Visible = bookingWeek.CanApproveBookingWeekBrand(selectedBrdCode) AndAlso (productStatusTextBox.Text = "approved")
        Else
            selectedPkgId = ""

            integrityHiddenField.Value = "0"
            packageDropDown.Items.Clear()

            ratesControl.Clear()
            exceptionControl.Clear()

            'ratesControl.Visible = False
            'exceptionControl.Visible = False

            approveButton.Visible = False
            rejectButton.Visible = False
        End If

        approveButton1.Visible = approveButton.Visible
        rejectButton1.Visible = rejectButton.Visible
    End Sub


    Private Sub BuildPackage()
        For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
            levelControl.Cells(flexInt).Amount = ""
        Next

        If Not product Is Nothing _
         AndAlso Not String.IsNullOrEmpty(selectedPkgId) _
         AndAlso Not String.IsNullOrEmpty(selectedSapId) Then
            package = New Package(product.WeekProductPackagesDataTable.FindByPkgIdSapId(selectedPkgId, selectedSapId))
            If Not package.FlexLevelDataTable Is Nothing Then
                For Each flexLevelRow As FlexLevelRow In package.FlexLevelDataTable
                    levelControl.Cells(FlexConstants.FlexNumToInt(CInt(flexLevelRow.FlxNum))).Amount = CInt(flexLevelRow.FlxRate).ToString()
                Next
            End If
        End If
    End Sub


    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildBookingWeeks()
        BuildBookingWeek(useViewState)
        BuildProduct(useViewState)
        BuildPackage()

        Me.Response.SetCookie(New HttpCookie(Flex_FbwId_CookieName, selectedFbwId.ToString()))
        Me.Response.SetCookie(New HttpCookie(Flex_CtyCode_CookieName, selectedCtyCode))
        Me.Response.SetCookie(New HttpCookie(Flex_BrdCode_CookieName, selectedBrdCode))
        Me.Response.SetCookie(New HttpCookie(Flex_TravelYear_CookieName, selectedTravelYear.ToString()))
        Me.Response.SetCookie(New HttpCookie(Flex_PrdId_CookieName, selectedPrdId))
        Me.Response.SetCookie(New HttpCookie(Flex_PkgId_CookieName, selectedPkgId))
        Me.Response.SetCookie(New HttpCookie(Flex_SapId_CookieName, selectedSapId))
        ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        Me.Response.SetCookie(New HttpCookie(Flex_CopiedPrdId_CookieName, selectedCopiedFromPrdId))
        ''Me.Response.SetCookie(New HttpCookie(Flex_Include_Domestic_CookieName, FlexGridradio.SelectedValue))

        ReloadCopyProductFromDropDown()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Return
        End If

        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 
        '' added 
        ''------------------------------------------------------------------------------
        Try
            '' Me.Response.SetCookie(New HttpCookie(Flex_Include_Domestic_CookieName, FlexGridradio.SelectedValue))
            ''FlexGridradio.SelectedValue = Request.Cookies(Flex_Include_Domestic_CookieName).Value.ToString
        
        Catch ex As Exception
        End Try


        GetSelectedFromUI()
        BuildForm(False)
        ''rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        ReloadCopyProductFromDropDown()
    End Sub

    Protected Sub bookingWeekDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bookingWeekDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub brandDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles brandDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub countryBrandYearDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryBrandYearDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub productDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles productDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub packageDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles packageDropDown.SelectedIndexChanged
        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 MOVE BELOW
        '' avoid postback and reload
        ''------------------------------------------------------------------------------
        GetSelectedFromUI()
        BuildForm(True)
    End Sub

    Protected Sub approveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles approveButton.Click, approveButton1.Click
        GetSelectedFromUI()
        BuildForm(True)

        ' integrity check
        If product Is Nothing _
         OrElse Not bookingWeek.CanApproveBookingWeekBrand(selectedBrdCode) _
         OrElse Not product.WeekProductRow.CanApprove() Then
            SetWarningMessage("Invalid Product: Has already been updated by another user")
        End If

        Dim valididationMessage As String = ""
        Dim validationError As Boolean = False

        ' set and validate the rates.  set everything, even the past, since it might be a new product
        For Each productRatesRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable
            Dim rateCell As Flex_RatesControl.RateCell = ratesControl.Cells(productRatesRow.FlrWkNo)
            productRatesRow.FlrFlexNum = rateCell.FlexNum
            productRatesRow.FlrChanged = Not rateCell.IsPast And rateCell.PrevFlexNum <> productRatesRow.FlrFlexNum
            Try
                product.ValidateRate(productRatesRow)
            Catch ex As ValidationException
                If String.IsNullOrEmpty(valididationMessage) Then
                    valididationMessage = ex.Message
                End If
                rateCell.IsError = True
                validationError = True
            End Try
        Next

        ' set and validate the exceptions
        Dim row As Integer = 0
        While row < FlexConstants.ExceptionMax
            Dim exceptionRow As Flex_ExceptionControl.ExceptionRow = exceptionControl.Rows(row)
            Dim productException As FlexBookingWeekExceptionRow = product.FlexBookingWeekExceptionDataTable(row)

            If exceptionRow.Id > 0 Then
                productException.FleId = exceptionRow.Id
            Else
                productException.SetFleIdNull()
            End If
            If exceptionRow.IsFrom Then
                productException.FleLocCodeFrom = exceptionRow.LocationCode
                productException.SetFleLocCodeToNull()
            Else
                productException.SetFleLocCodeFromNull()
                productException.FleLocCodeTo = exceptionRow.LocationCode
            End If
            productException.FleBookStart = exceptionRow.BookFrom
            productException.FleBookEnd = exceptionRow.BookTo
            productException.FleTravelStart = exceptionRow.TravelFrom
            productException.FleTravelEnd = exceptionRow.TravelTo
            productException.FleDelta = exceptionRow.Change
            exceptionRow.IsError = False
            Try
                product.ValidateException(productException)
            Catch ex As ValidationException
                If String.IsNullOrEmpty(valididationMessage) Then
                    valididationMessage = ex.Message
                End If
                exceptionRow.IsError = True
                validationError = True
            End Try

            row += 1
        End While

        If validationError Then
            SetWarningMessage(Server.HtmlEncode(valididationMessage))
            Return
        End If

        ' save (does an overall product validate too)
        Try
            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 MOVE BELOW
            '' added IIf(FlexGridradio.SelectedValue.Equals("D"), True, False)
            ''------------------------------------------------------------------------------
            ''Dim isDomestic As Boolean = IIf(Request.Cookies(Flex_Include_Domestic_CookieName).Value.ToString.Equals("D") = True, True, False)
            Dim isDomestic As Boolean = IIf(FlexGridradio.SelectedValue.ToString.Equals("D") = True, True, False)
            product.ApproveProduct(product.WeekProductRow.IntegrityNo, Me.UserCode, isDomestic)
        Catch ex As ValidationException
            ' change for highlighting the colliding row

            Try
                If ex.Tag IsNot Nothing Then
                    Dim oReturnedErroneousExceptionRow As Aurora.Flex.Data.FlexDataset.FlexBookingWeekExceptionRow = CType(ex.Tag, Aurora.Flex.Data.FlexDataset.FlexBookingWeekExceptionRow)
                    Dim iExceptionRowCount As Integer = 0
                    While iExceptionRowCount < FlexConstants.ExceptionMax
                        Dim exceptionRow As Flex_ExceptionControl.ExceptionRow = exceptionControl.Rows(iExceptionRowCount)

                        If exceptionRow.LocationCode = oReturnedErroneousExceptionRow.LocationCode _
                           AndAlso _
                           exceptionRow.BookFrom = oReturnedErroneousExceptionRow.FleBookStart _
                           AndAlso _
                           exceptionRow.BookTo = oReturnedErroneousExceptionRow.FleBookEnd _
                           AndAlso _
                           exceptionRow.TravelFrom = oReturnedErroneousExceptionRow.FleTravelStart _
                           AndAlso _
                           exceptionRow.TravelTo = oReturnedErroneousExceptionRow.FleTravelEnd _
                        Then
                            exceptionRow.IsError = True
                            Exit While
                        End If
                        iExceptionRowCount = iExceptionRowCount + 1
                    End While
                End If
            Catch
            End Try

            ' change for highlighting the colliding row
            SetWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        Catch ex As SaveException
            SetErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        ' rebuild the form with the newly saved data....
        BuildForm(False)

        SetInformationMessage(product.WeekProductRow.Description + " approved")
    End Sub

    Protected Sub rejectButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rejectButton.Click, rejectButton1.Click
        GetSelectedFromUI()
        BuildForm(True)

        ' integrity check
        If product Is Nothing _
         OrElse Not bookingWeek.CanApproveBookingWeekBrand(selectedBrdCode) _
         OrElse Not product.WeekProductRow.CanReject() Then
            SetWarningMessage("Invalid Product: Has already been updated by another user")
        End If

        ' reject
        Try
            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 
            '' added IIf(FlexGridradio.SelectedValue.Equals("D"), True, False)
            ''------------------------------------------------------------------------------
            ''Dim isDomestic As Boolean = IIf(Request.Cookies(Flex_Include_Domestic_CookieName).Value.ToString.Equals("D") = True, True, False)
            product.RejectProduct(product.WeekProductRow.IntegrityNo, Me.UserCode, True)
        Catch ex As ValidationException
            SetWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        Catch ex As SaveException
            SetErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        ' rebuild the form with the newly saved data....
        BuildForm(False)

        SetInformationMessage(product.WeekProductRow.Description + " rejected")
    End Sub

    Protected Sub bookingWeekCreateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bookingWeekCreateButton.Click
        Dim newFbwId As Integer = CInt(bookingWeekDropDown.SelectedValue)

        GetSelectedFromUI()
        BuildForm(True)

        If newFbwId <> selectedFbwId Then
            SetWarningMessage("Unexpected error creating booking week.  Already made?")
            Return
        End If

        Dim result As FlexBookingWeekRow
        Try
            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 MOVE BELOW
            '' added checkDomestic.Checked
            ''------------------------------------------------------------------------------
            result = bookingWeek.CreateBookingWeek(Me.UserCode, Me.PrgmName, True)
        Catch ex As SaveException
            SetErrorMessage(ex.Message)
            Return
        Catch ex As Exception
            SetErrorMessage("Error creating booking week")
            Return
        End Try

        selectedFbwId = result.FbwId
        BuildForm(False)

        SetInformationMessage(bookingWeek.BookingWeekRow.Description + " created")
    End Sub

    Protected Sub FlexGridradio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FlexGridradio.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

#Region "rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107"

    Private selectedCopiedFromPrdId As String = ""
    Public Const Flex_CopiedPrdId_CookieName As String = "Flex_CopiedPrdId"
    Public Sub CopyFlexProduct()
        Dim result As String = "OK"
        Try
            Dim copyFromThisproduct As Product = New Product(bookingWeek.WeekProductsDataTable.FindByCtyCodeBrdCodeTravelYearFbwIdPrdId(selectedCtyCode, _
                                                                                                                                        selectedBrdCode, _
                                                                                                                                        New Date(selectedTravelYear, 4, 1), _
                                                                                                                                        selectedFbwId, _
                                                                                                                                        selectedCopiedFromPrdId))
            Dim flpIdCopyToThisProduct As String = product.WeekProductRow.FlpId
            Dim flpIdCopyFromThisProduct As String = copyFromThisproduct.WeekProductRow.FlpId

            result = Aurora.Flex.Data.CopiedFlextProductTo(copyFromThisproduct.WeekProductRow.ComCode, CInt(flpIdCopyFromThisProduct), CInt(flpIdCopyToThisProduct), selectedCopiedFromPrdId, selectedPrdId)
            If (result.IndexOf("ERROR") <> -1) Then
                SetErrorMessage(result.Replace("ERROR:", ""))
            End If
        Catch ex As Exception
            result = ex.Message
            SetErrorMessage(result)
        End Try
        GetSelectedFromUI()
        BuildForm(False)
        copyFromProduct.SelectedIndex = 0
        SetInformationMessage(result.Replace("OK:", ""))
    End Sub

    Protected Sub copyfromButton_Click(sender As Object, e As System.EventArgs) Handles copyfromButton.Click
        If (copyFromProduct.SelectedItem.Text.Equals(productDropDown.SelectedItem.Text) = True) Then
            SetWarningMessage("The source and destination vehicles are the same. Please change one of them.")
            GetSelectedFromUI()
            BuildForm(False)
            Exit Sub
        End If

        If (copyFromProduct.SelectedItem.Text.IndexOf("please select") <> -1) Then
            SetWarningMessage("Please select source vehicle from the list.")
            GetSelectedFromUI()
            BuildForm(False)
            Exit Sub
        End If

        GetSelectedFromUI()
        BuildForm(False)
        CopyFlexProduct()
    End Sub

    Sub ReloadCopyProductFromDropDown()

        Dim _item As New ListItem("--please select--", "-1")
        copyFromProduct.Items.Clear()
        copyFromProduct.Items.Insert(0, _item)
        For Each item As ListItem In productDropDown.Items
            Dim proditem As New ListItem(item.Text, item.Value)
            copyFromProduct.Items.Add(proditem)
        Next

        Try
            If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_CopiedPrdId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_CopiedPrdId_CookieName).Value) Then
                selectedCopiedFromPrdId = Request.Cookies(Flex_CopiedPrdId_CookieName).Value
            Else
                copyFromProduct.SelectedValue = selectedCopiedFromPrdId
            End If
        Catch ex As Exception
            copyFromProduct.SelectedIndex = 0
        End Try
        
    End Sub

    Protected Sub confirmFlexCopy_PostBack(sender As Object, leftButton As Boolean, rightButton As Boolean, param As String) Handles confirmFlexCopy.PostBack
        If (leftButton) Then
            GetSelectedFromUI()
            BuildForm(False)
            CopyFlexProduct()
        End If
    End Sub

    Sub DisabledCopyFrom(disabled As Boolean)
        copyFromProduct.Enabled = disabled
        copyfromButton.Enabled = disabled
    End Sub
#End Region

    
   
    
End Class
