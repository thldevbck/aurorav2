<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportRates.aspx.vb" Inherits="Flex_ExportRates" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:HiddenField ID="integrityHiddenField" runat="server" />

    <table cellspacing="0" cellpadding="2" border="0" width="100%">
        <tr>
            <td width="150">Booking Week:</td>
            <td>
                <asp:DropDownList ID="bookingWeekDropDown" runat="server" AutoPostBack="True" Width="245">
                    <asp:ListItem Value="">(None)</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="bookingWeekStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150" style="text-align: center" TabIndex="-1" />
            </td>
        </tr>
        <tr>
            <td>Brand:</td>
            <td>
                <asp:DropDownList ID="brandDropDown" runat="server" AutoPostBack="True" Width="245">
                    <asp:ListItem Value="">(None)</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="brandStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150" style="text-align: center" TabIndex="-1" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #FFF; border-width: 1px 0;" align="center">
                <asp:Image ID="brandImage" runat="server" ImageUrl="~/Images/Brand/B.png" />
            </td>
        </tr>
        <tr>
            <td>Email Subject:</td>
            <td><asp:TextBox ID="emailSubjectTextBox" runat="server" Width="620px" /></td>
        </tr>
        <tr>
            <td style="vertical-align: top; margin-top: 6px;">Email Text:</td>
            <td><asp:TextBox ID="emailTextTextBox" runat="server" Wrap="true" Width="620px" TextMode="MultiLine" Rows="10" /></td>
        </tr>
    </table>
   
    <br />

    <asp:Table CssClass="dataTableColor" ID="weekExportTable" runat="server" CellPadding="2" CellSpacing="0" EnableViewState="false" Width="100%">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>Exports</asp:TableHeaderCell>
            <asp:TableHeaderCell Style="width: 150px;" align="right">File Name</asp:TableHeaderCell>
            <asp:TableHeaderCell Style="width: 100px;" align="right">Scheduled To Send (<%=Aurora.Common.UserSettings.Current.LocCode%>)</asp:TableHeaderCell>
            <asp:TableHeaderCell Style="width: 130px;" align="right">Status</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>
    
    <br />
    
    <uc1:MessagePanelControl ID="warningMessagePanelControl" runat="server" CssClass="messagePanel" />
    
    <div style="float:right">
        <asp:Button ID="approveButton" runat="server" Text="Approve" CssClass="Button_Standard Button_OK" Visible="false" />
        <asp:Button ID="rejectButton" runat="server" Text="Reject" CssClass="Button_Standard Button_Cancel" Visible="false" />
    </div>
    
    <br />

</asp:Content>

