<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditRates.aspx.vb" Inherits="Flex_EditRates"
    MasterPageFile="~/Include/AuroraHeader.master" Trace="false" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="RatesControl.ascx" TagName="RatesControl" TagPrefix="uc1" %>
<%@ Register Src="LevelControl.ascx" TagName="LevelControl" TagPrefix="uc2" %>
<%@ Register Src="ExceptionControl.ascx" TagName="ExceptionControl" TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="confirmCopy"  TagPrefix="uc4" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .flexLabel
        {
            background-color: #FFF;
            border: solid #7F9DB9 1px;
            width: 100px;
        }
        
        .ratesTable { width: 575px; }

        .ratesTable td { text-align: center; padding: 0px; }

        .ratesTable tr.trTravelStart { height: 16px; backround-color: #EEE; }
        .ratesTable tr.trRate { height: 20px; }
        .ratesTable tr.trFleetStatus { height: 20px; }

        .ratesTable th.thTravelStart { width: 80px; font-weight: normal; }
        .ratesTable th.thRate { font-weight: normal; }
        .ratesTable th.thFleetStatus { font-weight: normal; }

        .ratesTable td.tdCurrent { background-color: #FF8; }
        .ratesTable td.tdPast { background-color: #CCC; }

        .ratesTable tr.trTravelStart td { vertical-align:middle; }
        .ratesTable tr.trRate td input { width: 22px; background-color: #FFF; border: solid #7F9DB9 1px; text-align: center; margin: 1px; }
        .ratesTable tr.trFleetStatus td input { width: 14px; background-color: #EEE; border: solid #7F9DB9 1px; text-align: center; margin: 1px; }
       
        .levelTable { width: 270px; }
        .levelTable th,
        .levelTable td { width: 28px; border-width: 1px; text-align: center; font-weight: normal; }
       
        .exceptionTable { width: 725px; }
        
        .exceptionTable tr.error { background-color: #FCC; }

        .bookingWeekTable { width: 870px; }

        .productTable td { /* vertical-align:top; */ }
       
    </style>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript">

/*==================================================================================================*/
/* DropDown & IsDirty Checking                                                                      */
/*==================================================================================================*/
var dirty = false;
var bookingWeekDropDownId = '<%= bookingWeekDropDown.ClientID %>';
var countryBrandYearDropDownId = '<%= countryBrandYearDropDown.ClientID %>';
var productDropDownId = '<%= productDropDown.ClientID %>';

var bookingWeekDropDownIndex = 0;
var countryBrandYearDropDownIndex = 0;
var productDropDownIndex = 0;

function dropDown_saveSelection()
{
    bookingWeekDropDownIndex = document.getElementById (bookingWeekDropDownId).selectedIndex;
    countryBrandYearDropDownIndex = document.getElementById (countryBrandYearDropDownId).selectedIndex;
    productDropDownIndex = document.getElementById (productDropDownId).selectedIndex;
}

function dropDown_restoreSelection()
{
    document.getElementById (bookingWeekDropDownId).selectedIndex = bookingWeekDropDownIndex;
    document.getElementById (countryBrandYearDropDownId).selectedIndex = countryBrandYearDropDownIndex;
    document.getElementById (productDropDownId).selectedIndex = productDropDownIndex;
}

function dropDown_change (sender)
{
    if (dirty)
    {
    	if (confirm("You have made changes.  Do you want to lose these?"))
    	{
    	    dirty = false;    	
            dropDown_saveSelection();
            setTimeout("__doPostBack('" + this.name + "', '')", 0);
    	}
    	else
    	    dropDown_restoreSelection();
    }
	else
	{
        dropDown_saveSelection();
        setTimeout("__doPostBack('" + this.name + "', '')", 0);
    }
}

addEvent (window, "load", dropDown_saveSelection);

/*==================================================================================================*/
/* Rates Control                                                                                    */
/*==================================================================================================*/
var ratesControlId = '<%= ratesControl.ClientId %>';
var ratesColors = <%= ratesControl.RatesColorsJSON %>;

function rateTextBox_keypress (e, weekNo)
{
    var keynum = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(keynum);
    
    charcheck = /[a-hA-H1-8]/
    return charcheck.test(keychar)
}

function rateTextBox_change (e, weekNo)
{
    var rateTableCell = document.getElementById (ratesControlId + "_rate" + weekNo + "TableCell");
    var rateTextBox = document.getElementById (ratesControlId + "_rate" + weekNo + "TextBox");
    var prevHiddenField = document.getElementById (ratesControlId + "_prev" + weekNo + "HiddenField");
    var fleetStatusTableCell = document.getElementById (ratesControlId + "_fleetStatus" + weekNo + "TableCell");
    
    var rate = rateTextBox.value.toUpperCase().trim();
    rateTextBox.value = rate;
    var prev = prevHiddenField.value.toUpperCase().trim();
    
    var color = ratesColors[rate];
    if (!color)
    {
        color = ratesColors['error'];
        rateTextBox.style.backgroundColor = '';
    }
    else if (rate != prev)
        rateTextBox.style.backgroundColor = ratesColors['changed'];
    else
        rateTextBox.style.backgroundColor = '';
    
    rateTableCell.style.backgroundColor = color;
    fleetStatusTableCell.style.backgroundColor = color;
    
    dirty = true;
}

/*==================================================================================================*/
/* Exception Control                                                                                */
/*==================================================================================================*/
var exceptionControlId = '<%= exceptionControl.ClientId %>';

function exception_change (sender, row)
{
    var exceptionTableRow = document.getElementById (exceptionControlId + "_exception" + row + "TableRow");
    var locationDropDown = document.getElementById (exceptionControlId + "_location" + row + "DropDown");
    var fromRadio = document.getElementById (exceptionControlId + "_from" + row + "Radio");
    var toRadio = document.getElementById (exceptionControlId + "_to" + row + "Radio");
    var bookFromTextBox = document.getElementById (exceptionControlId + "_bookFrom" + row + "TextBox_dateTextBox");
    var bookToTextBox = document.getElementById (exceptionControlId + "_bookTo" + row + "TextBox_dateTextBox");
    var travelFromTextBox = document.getElementById (exceptionControlId + "_travelFrom" + row + "TextBox_dateTextBox");
    var travelToTextBox = document.getElementById (exceptionControlId + "_travelTo" + row + "TextBox_dateTextBox");
    var changeDropDown = document.getElementById (exceptionControlId + "_change" + row + "DropDown");
    
    if ((sender == locationDropDown) && (locationDropDown.value == ''))
    {
        fromRadio.checked = true;
        toRadio.checked = false;
        bookFromTextBox.value = '';
        bookToTextBox.value = '';
        travelFromTextBox.value = '';
        travelToTextBox.value = '';
        changeDropDown.value = '0';

        exceptionTableRow.className = ((row % 2) == 0) ? 'row' : 'rowalt';
    }
    else
    {
        var error = false;
        
        var bookFromDateStr = bookFromTextBox.value.trim();
        if (locationDropDown.value == '')
            error = error || (bookFromDateStr != '');
        else
        {
            var bookFromDate = Date.parseDate (bookFromDateStr, 'd/m/Y');
            if ((bookFromDate != null) && (bookFromDateStr != ''))
                bookFromTextBox.value = bookFromDate.print ('d/m/Y');
            else
                error = true;
        }

        var bookToDateStr = bookToTextBox.value.trim();
        if (locationDropDown.value == '')
            error = error || (bookToDateStr != '');
        else
        {
            var bookToDate = Date.parseDate (bookToDateStr, 'd/m/Y');
            if ((bookToDate != null) && (bookToDateStr != ''))
                bookToTextBox.value = bookToDate.print ('d/m/Y');
            else
                error = true;
        }

        if (bookFromDate && bookToDate && (bookToDate <= bookFromDate))
            error = true;

        var travelFromDateStr = travelFromTextBox.value.trim();
        if (locationDropDown.value == '')
            error = error || (travelFromDateStr != '');
        else
        {
            var travelFromDate = Date.parseDate (travelFromDateStr, 'd/m/Y');
            if ((travelFromDate != null) && (travelFromDateStr != ''))
                travelFromTextBox.value = travelFromDate.print ('d/m/Y');
            else
                error = true;
        }

        var travelToDateStr = travelToTextBox.value.trim();
        if (locationDropDown.value == '')
            error = error || (travelToDateStr != '');
        else
        {
            var travelToDate = Date.parseDate (travelToDateStr, 'd/m/Y');
            if ((travelToDate != null) && (travelToDateStr != ''))
                travelToTextBox.value = travelToDate.print ('d/m/Y');
            else
                error = true;
        }

        if (travelFromDate && travelToDate && (travelToDate <= travelFromDate))
            error = true;

        if ((locationDropDown.value == '') != (changeDropDown.value == '0'))
            error = true;
        
        if (error)
            exceptionTableRow.className = 'error';
        else
            exceptionTableRow.className = ((row % 2) == 0) ? 'row' : 'rowalt';
    }

    dirty = true;
}


    function showFloatingDiv(Event, Show, ExceptionList)
    {
        var target;
        var x = 0;
        var y = 0;
        if (window.event) 
        {
            Event = window.event;
            target = Event.srcElement;
            x = Event.x;
            y = Event.y;
            if (document.documentElement && document.documentElement.scrollTop)	 // Explorer 6 Strict
		        y += document.documentElement.scrollTop;
	        else if (document.body) // all other Explorers
		        y += document.body.scrollTop;
        }
        else
        {
            target = Event.target;
            x = Event.pageX;
            y = Event.pageY;
        }
        
        var popupExceptions = document.getElementById('popupExceptions');
    
        if (!Show)
        { 
            popupExceptions.style.visibility='hidden';
        }
        else 
        { 
            var arrExceptions = ExceptionList.split(',');
            var sHTMLString = '<TABLE>';
            for(var i = 0;i<arrExceptions.length-1;i++)
            {
                var arrException = arrExceptions[i].split('-');
                sHTMLString += '<TR><TD>' + arrException[0] + '</TD><TD>-</TD><TD>' + arrException[1] + '</TD>' + '</TR>'
            }
            sHTMLString += '</TABLE>';
        
            popupExceptions.innerHTML = sHTMLString;

            target.style.cursor='pointer';
            popupExceptions.style.visibility = 'visible';
            popupExceptions.style.left = (x + 5) + 'px';
            popupExceptions.style.top = (y - 5) + 'px';
        }
    }

    </script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
<div style="visibility: hidden; position: absolute; background-color: #dcdcdc; border: solid 1px black; padding: 2px; color:red; font-size:8pt;" id="popupExceptions"></div>
    <table cellspacing="0" cellpadding="2" border="0" class="bookingWeekTable">
        <tr>
            <td width="150">Booking Week:</td>
            <td>
                <asp:DropDownList ID="bookingWeekDropDown" runat="server" Width="245" onchange="return dropDown_change(this);">
                    <asp:ListItem Value="">(None)</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="bookingWeekStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150px" style="text-align: center" TabIndex="-1"></asp:TextBox>
                  <asp:Button ID="bookingWeekCreateButton" runat="server" CssClass="Button_Standard Button_Create" Text="Create" />
            </td>
          <%-- 
           <td style="visibility:hidden;">
                <asp:CheckBox runat="server" ID="checkDomestic" Text="Include Domestic Flex" AutoPostBack="True" />
            </td>
            <td align="left">
              
            </td>
            --%>
         </tr>
    </table>


    <table cellspacing="0" cellpadding="0" border="0" class="productTable" id="productTable" runat="server">
        <tr>
            <td colspan="3">
            
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td width="150">Brand:</td>
                        <td>
                            <asp:DropDownList ID="brandDropDown" runat="server" AutoPostBack="True" Width="245">
                                <asp:ListItem Value="">(None)</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:TextBox ID="brandStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150px" TabIndex="-1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="background-color: #FFF; border-width: 1px 0;" align="center">
                <asp:Image ID="brandImage" runat="server" ImageUrl="~/Images/Brand/B.png" />
            </td>
        </tr>
        <tr>
            <td style="width: 600px;">
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td width="150">Country / Year:</td>
                        <td>
                            <asp:DropDownList ID="countryBrandYearDropDown" runat="server" Width="245" onchange="return dropDown_change(this);">
                                <asp:ListItem Value="">(None)</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>Flex Type:</td>
                        <td>
                                <asp:RadioButtonList ID="FlexGridradio" runat ="server" RepeatDirection="Horizontal" RepeatColumns="2" AutoPostBack="true">
                                    <asp:ListItem Value="I" Selected="true" >International</asp:ListItem>
                                    <asp:ListItem Value="D">Domestic</asp:ListItem>
                                </asp:RadioButtonList>
                       </td>
                    </tr>

                    <tr>
                        <td>Product:</td>
                        <td>
                            <asp:DropDownList ID="productDropDown" runat="server" Width="245" onchange="return dropDown_change(this);">
                                <asp:ListItem Value="">(None)</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:TextBox ID="productStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150px" style="text-align: center" TabIndex="-1"></asp:TextBox>
                            <asp:HiddenField ID="integrityHiddenField" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
            <td style="width: 270px; border-left-width: 1px">
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td colspan="2">
                            Package / Saleable Product:<br />
                            <asp:DropDownList ID="packageDropDown" runat="server" AutoPostBack="True" Width="260px" /><br />
                            <asp:TextBox ID="packageNameTextBox" runat="server" Width="260px" ReadOnly="true" CssClass="inputreadonly" TabIndex="-1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="border-top-width: 1px">
                <table cellspacing="0" cellpadding="2" border="0" width="100%">
                    <tr>
                        <td width="150">Included Pickup Locations:</td>
                        <td><asp:TextBox ID="productIncludedLocTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="405px" TabIndex="-1"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Excluded Pickup Locations:</td>
                        <td><asp:TextBox ID="productExcludedLocTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="405px" TabIndex="-1"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td style="border-top-width: 1px">&nbsp;&nbsp;&nbsp;</td>
            <td style="border-top-width: 1px; border-left-width: 1px">
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td>Booking From/To:</td>
                        <td><asp:TextBox ID="packageBookingTextBox" runat="server" Width="150" ReadOnly="true" CssClass="inputreadonly" TabIndex="-1"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Travel From/To:</td>
                        <td><asp:TextBox ID="packageTravelTextBox" runat="server" Width="150" ReadOnly="true" CssClass="inputreadonly" TabIndex="-1"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="3" style="border-top-width: 1px;">&nbsp;</td>
        </tr>

        <tr>
            <td>
                <uc1:RatesControl ID="ratesControl" runat="server" />
            </td>
            <td>&nbsp;</td>
            <td style="vertical-align: top">
                <uc2:LevelControl ID="levelControl" runat="server" /><br />
            </td>
        </tr>
         <%--rev:mia 14-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107--%>
        <tr>
             <td style="border-top-width: 1px">
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td width="150">Copy from:</td>
                        <td>
                            <asp:DropDownList ID="copyFromProduct" runat="server" Width="245" AutoPostBack="false">
                                <asp:ListItem Value="">(None)</asp:ListItem>
                            </asp:DropDownList>
                         </td>
                         <td>
                            <asp:Button ID="copyfromButton" runat="server" CssClass="Button_Standard Button_OK" Text="Apply" />
                         </td>
                    </tr>
                </table>
             </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style="height: 34px; padding: 2px">
                <asp:Button ID="rejectButton" runat="server" CssClass="Button_Standard Button_Cancel" Text="Reject" />
                <asp:Button ID="approveButton" runat="server" CssClass="Button_Standard Button_OK" Text="Approve" />
            </td>
        </tr>

        <tr>
            <td class="col1" colspan="3">                
                <uc3:ExceptionControl ID="exceptionControl" runat="server" />
            </td>
        </tr>
       
        <tr>
            <td colspan="3" align="right" style="height: 34px; padding: 2px">
                <asp:Button ID="rejectButton1" runat="server" CssClass="Button_Standard Button_Cancel" Text="Reject" />
                <asp:Button ID="approveButton1" runat="server" CssClass="Button_Standard Button_OK" Text="Approve" />
            </td>
        </tr>
    </table>
    
    <br />

    <uc4:confirmCopy ID="confirmFlexCopy" runat="server" 
                                          Title="Copy Flex and Exception"      
                                          LeftButton_Visible="true"                                      
                                          RightButton_Visible="true"
                                          Text="Do you want to copy Flex and Exception?" />     
</asp:Content>
