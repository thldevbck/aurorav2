Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services


<AuroraFunctionCodeAttribute("FLEXEXPORT")> _
Partial Class Flex_ExportRates
    Inherits AuroraPage

    Public Const Flex_FbwId_CookieName As String = "Flex_FbwId"
    Public Const Flex_BrdCode_CookieName As String = "Flex_BrdCode"

    Private selectedFbwId As String = ""
    Private selectedBrdCode As String = ""

    Private bookingWeeks As BookingWeeks = Nothing
    Private bookingWeek As BookingWeek = Nothing
    Private flexBookingWeekBrandDataRow As FlexBookingWeekBrandRow = Nothing
    Private weekExportsForBrand As WeekExportsRow()
    Private weekProductsForBrand As WeekProductsRow()

    Private Sub GetSelectedFromUI()
        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_FbwId_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_FbwId_CookieName).Value) Then
            selectedFbwId = Aurora.Common.Utility.ParseInt(Request.Cookies(Flex_FbwId_CookieName).Value, Integer.MinValue)
        ElseIf Not String.IsNullOrEmpty(bookingWeekDropDown.SelectedValue) Then
            selectedFbwId = bookingWeekDropDown.SelectedValue
        End If

        Me.Response.SetCookie(New HttpCookie(Flex_FbwId_CookieName, selectedFbwId.ToString()))

        If Not Me.IsPostBack _
         AndAlso Not Request.Cookies(Flex_BrdCode_CookieName) Is Nothing _
         AndAlso Not String.IsNullOrEmpty(Request.Cookies(Flex_BrdCode_CookieName).Value) Then
            selectedBrdCode = Request.Cookies(Flex_BrdCode_CookieName).Value
        ElseIf Not String.IsNullOrEmpty(brandDropDown.SelectedValue) Then
            selectedBrdCode = brandDropDown.SelectedValue
        End If

        Me.Response.SetCookie(New HttpCookie(Flex_BrdCode_CookieName, selectedBrdCode))
    End Sub

    Private Sub BuildBookingWeeks()
        bookingWeekDropDown.Items.Clear()
        For Each bookingWeeksRow As FlexBookingWeekRow In bookingWeeks.FlexBookingWeekDataTable
            If bookingWeeksRow.FbwId <= 0 Then Continue For

            Dim listItem As New ListItem(bookingWeeksRow.Description, bookingWeeksRow.FbwId)
            listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeeksRow.StatusColor) + ";")
            bookingWeekDropDown.Items.Add(listItem)
        Next

        ' There should always be booking weeks
        ' Even if none in the database, current and next booking weeks are mocked for the create button
        If bookingWeekDropDown.Items.FindByValue(selectedFbwId) Is Nothing Then
            selectedFbwId = bookingWeeks.CurrentFbwId
        End If

        bookingWeekDropDown.SelectedValue = selectedFbwId
    End Sub

    Private Sub BuildBrands()
        Dim listItem As ListItem

        brandDropDown.Items.Clear()
        listItem = New ListItem(Me.CompanyCode & " - " & Me.CompanyName, "")
        listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor) + ";")

        brandDropDown.Items.Add(listItem)
        For Each flexBookingWeekBrandDataRow0 As FlexBookingWeekBrandRow In bookingWeek.FlexBookingWeekBrandDataTable
            listItem = New ListItem(flexBookingWeekBrandDataRow0.Description, flexBookingWeekBrandDataRow0.FbbBrdCode)
            listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekBrandDataRow0.StatusColor) + ";")
            brandDropDown.Items.Add(listItem)
        Next

        If brandDropDown.Items.FindByValue(selectedBrdCode) Is Nothing Then
            selectedBrdCode = ""
        End If

        brandDropDown.SelectedValue = selectedBrdCode
    End Sub

    Private Sub BuildBookingWeek(ByVal useViewState As Boolean)
        bookingWeekStatusTextBox.Text = bookingWeek.BookingWeekRow.StatusText
        bookingWeekStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor) + ";")

        If flexBookingWeekBrandDataRow Is Nothing Then
            brandStatusTextBox.Text = bookingWeek.BookingWeekRow.StatusText
            brandStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(bookingWeek.BookingWeekRow.StatusColor) + ";")
            brandImage.ImageUrl = "~/Images/Company/" & Me.CompanyCode & ".png"
        Else
            brandStatusTextBox.Text = flexBookingWeekBrandDataRow.StatusText
            brandStatusTextBox.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(flexBookingWeekBrandDataRow.StatusColor) + ";")
            brandImage.ImageUrl = "~/Images/Brand/" & flexBookingWeekBrandDataRow.FbbBrdCode & ".png"
        End If

        If Not useViewState Then
            emailSubjectTextBox.Text = ""
            emailTextTextBox.Text = ""
            If flexBookingWeekBrandDataRow Is Nothing Then
                integrityHiddenField.Value = bookingWeek.BookingWeekRow.IntegrityNo.ToString()
                If Not bookingWeek.BookingWeekRow.IsFbwEmailSubjectNull Then emailSubjectTextBox.Text = bookingWeek.BookingWeekRow.FbwEmailSubject
                If Not bookingWeek.BookingWeekRow.IsFbwEmailTextNull Then emailTextTextBox.Text = bookingWeek.BookingWeekRow.FbwEmailText
            Else
                integrityHiddenField.Value = flexBookingWeekBrandDataRow.IntegrityNo.ToString()
                If Not flexBookingWeekBrandDataRow.IsFbbEmailSubjectNull Then emailSubjectTextBox.Text = flexBookingWeekBrandDataRow.FbbEmailSubject
                If Not flexBookingWeekBrandDataRow.IsFbbEmailTextNull Then emailTextTextBox.Text = flexBookingWeekBrandDataRow.FbbEmailText
            End If
        End If

        While weekExportTable.Rows.Count > 1
            weekExportTable.Rows.RemoveAt(1)
        End While

        Dim count As Integer = 0
        For Each weekExportsRow As WeekExportsRow In weekExportsForBrand

            Dim tableRow As New WebControls.TableRow()
            tableRow.CssClass = IIf((count Mod 2) = 0, "evenRow", "oddRow")
            weekExportTable.Rows.Add(tableRow)

            Dim fileCell As New WebControls.TableCell()
            fileCell.Text = "<a href=""ExportFile.aspx?FwxId=" & CStr(weekExportsRow.FwxId) & """>" & Server.HtmlEncode(weekExportsRow.Description) & "</a>"
            fileCell.CssClass = "tdfile"
            tableRow.Cells.Add(fileCell)

            Dim filenameCell As New WebControls.TableCell()
            filenameCell.Text = Server.HtmlEncode(weekExportsRow.FileName)
            filenameCell.CssClass = "tdfilename"
            'filenameCell.Style.Add("background-color", ColorTranslator.ToHtml(weekExportsRow.StatusColor))
            tableRow.Cells.Add(filenameCell)

            Dim scheduledCell As New WebControls.TableCell()
            scheduledCell.Text = weekExportsRow.ScheduleDescription(UserSettings.Current.LocHoursBehindServerLocation)
            'scheduledCell.Style.Add("background-color", ColorTranslator.ToHtml(weekExportsRow.StatusColor))
            scheduledCell.CssClass = "tdscheduled"
            tableRow.Cells.Add(scheduledCell)

            Dim statusCell As New WebControls.TableCell()
            statusCell.Text = Server.HtmlEncode(weekExportsRow.StatusText)
            statusCell.CssClass = "tdstatus"
            statusCell.Style.Add("background-color", ColorTranslator.ToHtml(weekExportsRow.StatusColor))
            tableRow.Cells.Add(statusCell)

            count += 1
        Next

        If flexBookingWeekBrandDataRow Is Nothing AndAlso Not bookingWeek.IsReadOnlyBookingWeek Then
            Dim found As Boolean = False

            Dim text As String = "<p>There are unapproved brands:<ul>"
            For Each flexBookingWeekBrandRow0 As FlexBookingWeekBrandRow In bookingWeek.FlexBookingWeekBrandDataTable
                If Not flexBookingWeekBrandRow0.IsApproved Then
                    text += "<li>" + Server.HtmlEncode(flexBookingWeekBrandRow0.Description) + "</li>"
                    found = True
                End If
            Next
            text += "</ul></p><p>These must be approved before approving " & Server.HtmlEncode(Me.CompanyCode & " - " & Me.CompanyName) & ".</p>"

            If found Then
                warningMessagePanelControl.SetMessagePanelWarning(text)
            End If

        ElseIf flexBookingWeekBrandDataRow IsNot Nothing AndAlso bookingWeek.CanApproveBookingWeekBrand(flexBookingWeekBrandDataRow.FbbBrdCode) Then
            Dim found As Boolean = False

            Dim text As String = "<p>There are unapproved products:<ul>"
            For Each weekProductRow As WeekProductsRow In weekProductsForBrand
                If weekProductRow.IsUnApproved Then
                    text += "<li>" + Server.HtmlEncode(weekProductRow.LongDescription) + "</li>"
                    found = True
                End If
            Next
            text += "</ul></p><p>Please carefully review it before approving.</p>"

            If found Then
                warningMessagePanelControl.SetMessagePanelWarning(text)
            End If
        End If

        If (flexBookingWeekBrandDataRow Is Nothing AndAlso bookingWeek.CanApproveBookingWeek) _
         OrElse (flexBookingWeekBrandDataRow IsNot Nothing AndAlso bookingWeek.CanApproveBookingWeekBrand(flexBookingWeekBrandDataRow.FbbBrdCode)) Then
            emailSubjectTextBox.ReadOnly = False
            emailSubjectTextBox.CssClass = ""
            emailSubjectTextBox.TabIndex = 0
            emailTextTextBox.ReadOnly = False
            emailTextTextBox.CssClass = ""
            emailTextTextBox.TabIndex = -1
            approveButton.Visible = True
        Else
            emailSubjectTextBox.ReadOnly = True
            emailSubjectTextBox.CssClass = "inputreadonly"
            emailSubjectTextBox.TabIndex = -1
            emailTextTextBox.ReadOnly = True
            emailTextTextBox.CssClass = "inputreadonly"
            emailTextTextBox.TabIndex = -1
            approveButton.Visible = False
        End If

        If (flexBookingWeekBrandDataRow Is Nothing AndAlso bookingWeek.CanRejectBookingWeek) _
         OrElse (flexBookingWeekBrandDataRow IsNot Nothing AndAlso bookingWeek.CanRejectBookingWeekBrand(flexBookingWeekBrandDataRow.FbbBrdCode)) Then
            rejectButton.Visible = True
        Else
            rejectButton.Visible = False
        End If
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        bookingWeeks = New BookingWeeks(Me.CompanyCode)

        BuildBookingWeeks()

        Dim flexBookingWeekRow As FlexBookingWeekRow = bookingWeeks.FlexBookingWeekDataTable.FindByFbwId(selectedFbwId)
        bookingWeek = New BookingWeek(flexBookingWeekRow)

        BuildBrands()

        For Each flexBookingWeekBrandDataRow0 As FlexBookingWeekBrandRow In bookingWeek.FlexBookingWeekBrandDataTable
            If flexBookingWeekBrandDataRow0.FbbBrdCode = selectedBrdCode Then
                flexBookingWeekBrandDataRow = flexBookingWeekBrandDataRow0
            End If
        Next
        If flexBookingWeekBrandDataRow Is Nothing Then
            weekExportsForBrand = bookingWeek.GetWeekExportsForBrandCode(Nothing)
            weekProductsForBrand = bookingWeek.GetWeekProductsForBrandCode(Nothing)
        Else
            weekExportsForBrand = bookingWeek.GetWeekExportsForBrandCode(flexBookingWeekBrandDataRow.FbbBrdCode)
            weekProductsForBrand = bookingWeek.GetWeekProductsForBrandCode(flexBookingWeekBrandDataRow.FbbBrdCode)
        End If

        BuildBookingWeek(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then Return

        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub bookingWeekDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bookingWeekDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub brandDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles brandDropDown.SelectedIndexChanged
        GetSelectedFromUI()
        BuildForm(False)
    End Sub

    Protected Sub approveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles approveButton.Click
        GetSelectedFromUI()
        BuildForm(True)

        If flexBookingWeekBrandDataRow Is Nothing Then
            ' save (does an overall validate too)
            Try
                bookingWeek.ApproveBookingWeek(emailSubjectTextBox.Text, emailTextTextBox.Text, Integer.Parse(integrityHiddenField.Value), Me.UserCode)
            Catch ex As ValidationException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            Catch ex As SaveException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            End Try

            ' rebuild the form with the newly saved data....
            BuildForm(False)

            SetInformationMessage(bookingWeek.BookingWeekRow.Description + " approved")
        Else
            ' save (does an overall validate too)
            Try
                bookingWeek.ApproveBookingWeekBrand(flexBookingWeekBrandDataRow.FbbBrdCode, emailSubjectTextBox.Text, emailTextTextBox.Text, Integer.Parse(integrityHiddenField.Value), Me.UserCode)
            Catch ex As ValidationException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            Catch ex As SaveException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            End Try

            ' rebuild the form with the newly saved data....
            BuildForm(False)

            SetInformationMessage(flexBookingWeekBrandDataRow.Description + " approved")
        End If
    End Sub

    Protected Sub rejectButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rejectButton.Click
        GetSelectedFromUI()
        BuildForm(True)

        If flexBookingWeekBrandDataRow Is Nothing Then
            ' save (does an overall validate too)
            Try
                bookingWeek.RejectBookingWeek(Integer.Parse(integrityHiddenField.Value), Me.UserCode)
            Catch ex As ValidationException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            Catch ex As SaveException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            End Try

            ' rebuild the form with the newly saved data....
            BuildForm(False)

            SetInformationMessage(bookingWeek.BookingWeekRow.Description + " rejected")
        Else
            ' save (does an overall validate too)
            Try
                bookingWeek.RejectBookingWeekBrand(flexBookingWeekBrandDataRow.FbbBrdCode, Integer.Parse(integrityHiddenField.Value), Me.UserCode)
            Catch ex As ValidationException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            Catch ex As SaveException
                SetErrorMessage(Server.HtmlEncode(ex.Message))
                Return
            End Try

            ' rebuild the form with the newly saved data....
            BuildForm(False)

            SetInformationMessage(flexBookingWeekBrandDataRow.Description + " rejected")
        End If
    End Sub

End Class