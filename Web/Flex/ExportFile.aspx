<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportFile.aspx.vb" Inherits="Flex_ExportFile" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>


<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .exportFileTable td
        {
            vertical-align: top;
        }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript">
function initExportFile()
{
	addEvent (document.getElementById ("aspnetForm"), "submit", function() { hideLoadingIndicator(); return true; });
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(hideLoadingIndicator);
}

addEvent (window, "load", initExportFile);
  
    </script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <table cellspacing="0" cellpadding="2" border="0" class="exportFileTable">
        <tr>
            <td width="150">Booking Week:</td>
            <td>
                <asp:TextBox ID="bookingWeekTextBox" runat="server" ReadOnly="true" Width="245" CssClass="inputreadonly" TabIndex="-1" />
                &nbsp;
                <asp:TextBox ID="bookingWeekStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150" TabIndex="-1" />
                <asp:HiddenField ID="integrityHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Brand:</td>
            <td>
                <asp:TextBox ID="brandTextBox" runat="server" ReadOnly="true" Width="245" CssClass="inputreadonly" TabIndex="-1" />
                &nbsp;
                <asp:TextBox ID="brandStatusTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="150" TabIndex="-1" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #FFF; border-width: 1px 0;" align="center">
                <asp:Image ID="brandImage" runat="server" ImageUrl="~/Images/Brand/B.png" />
            </td>
        </tr>
        <tr>
            <td>Export Name:</td>
            <td><asp:TextBox ID="nameTextBox" runat="server" ReadOnly="true" Width="400px" CssClass="inputreadonly" TabIndex="-1" /></td>
        </tr>
        <tr>
            <td>Status:</td>
            <td><asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="400px" CssClass="inputreadonly" TabIndex="-1" /></td>
        </tr>
        <tr>
            <td>Scheduled to Send:</td>
            <td>
                <uc1:DateControl ID="scheduledDateTextBox" runat="server" Nullable="false" />
                &nbsp;
                <uc1:TimeControl ID="scheduledTimeTextBox" runat="server" Nullable="false" />
                &nbsp;
                <asp:Label ID="locationLabel" runat="server">(AKL)</asp:Label>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="emailCheckBox" runat="server" Enabled="False" Text="" />Email
                &nbsp;&nbsp;
                <asp:CheckBox ID="ftpCheckBox" runat="server" Enabled="False" Text="" />FTP
                &nbsp; &nbsp;
                <asp:CheckBox ID="auroraCheckBox" runat="server" Enabled="False" Text="" />Aurora</td>
        </tr>
        <tr>
            <td style="border-top-width: 1px">Email Contacts:</td>
            <td style="border-top-width: 1px"><asp:TextBox ID="emailContactsTextBox" runat="server" ReadOnly="true" Width="620px" TextMode="MultiLine" Rows="4" CssClass="inputreadonly" TabIndex="-1" /></td>
        </tr>
        <tr>
            <td style="border-top-width: 1px">File Name:</td>
            <td style="border-top-width: 1px"><asp:TextBox ID="filenameTextBox" runat="server" ReadOnly="true" Width="400px" CssClass="inputreadonly" TabIndex="-1" /></td>
        </tr>
        <tr>
            <td>File Text:</td>
            <td><asp:TextBox ID="textTextBox" runat="server" ReadOnly="true" Width="620px" TextMode="MultiLine" Rows="20" CssClass="inputreadonly" Wrap="false" Font-Names="Courier New, Arial" EnableViewState="false" TabIndex="-1" /></td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button ID="textDownloadButton" runat="server" Text="Download" CssClass="Button_Standard Button_Save" />
            </td>
        </tr>
    </table>
    
    <br />

    <div style="float:right">
        <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="true" />
        <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" Visible="true" />
    </div>

    <br />

</asp:Content>

