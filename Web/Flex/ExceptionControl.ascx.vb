Imports System
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports System.Web.Script.Serialization

<ToolboxData("<{0}:ExceptionControl runat=server></{0}:ExceptionControl>")> _
Partial Class Flex_ExceptionControl
    Inherits System.Web.UI.UserControl

    Public Class ExceptionRow
        Private parent As Flex_ExceptionControl
        Private row As Integer

        Private exceptionTableRow As WebControls.TableRow
        Private bookFromTableCell As WebControls.TableCell
        Private bookToTableCell As WebControls.TableCell
        Private locationTableCell As WebControls.TableCell
        Private fromToTableCell As WebControls.TableCell
        Private travelFromTableCell As WebControls.TableCell
        Private travelToTableCell As WebControls.TableCell
        Private changeTableCell As WebControls.TableCell

        Private bookFromTextBox As UserControls_DateControl
        Private bookToTextBox As UserControls_DateControl
        Private locationDropdown As New WebControls.DropDownList
        Private fromRadio As New WebControls.RadioButton
        Private toRadio As New WebControls.RadioButton
        Private travelFromTextBox As UserControls_DateControl
        Private travelToTextBox As UserControls_DateControl
        Private changeDropdown As New WebControls.DropDownList
        Private idHidden As New WebControls.HiddenField

        Private created As Boolean = False

        Public Sub New(ByVal parent As Flex_ExceptionControl, ByVal row As Integer)
            Me.parent = parent
            Me.row = row

            bookFromTextBox = Me.parent.LoadControl("~\UserControls\DateControl\DateControl.ascx")
            bookToTextBox = Me.parent.LoadControl("~\UserControls\DateControl\DateControl.ascx")
            travelFromTextBox = Me.parent.LoadControl("~\UserControls\DateControl\DateControl.ascx")
            travelToTextBox = Me.parent.LoadControl("~\UserControls\DateControl\DateControl.ascx")

            exceptionTableRow = New TableRow()
            exceptionTableRow.ID = "exception" + CStr(row) + "TableRow"
            parent.exceptionTable.Rows.Add(exceptionTableRow)

            locationTableCell = New TableCell()
            exceptionTableRow.Cells.Add(locationTableCell)

            locationDropdown.ID = "location" + CStr(row) + "DropDown"
            locationDropdown.Attributes.Add("onchange", "exception_change(this, " + CStr(row) + ")")
            locationTableCell.Controls.Add(locationDropdown)

            fromToTableCell = New TableCell()
            exceptionTableRow.Cells.Add(fromToTableCell)

            fromRadio.ID = "from" + CStr(row) + "Radio"
            fromRadio.GroupName = "exceptionFromTo" + CStr(row)
            fromRadio.Attributes.Add("onchange", "exception_change(this, " + CStr(row) + ")")
            fromToTableCell.Controls.Add(fromRadio)

            fromToTableCell.Controls.Add(New LiteralControl("&nbsp;/&nbsp;"))

            toRadio.ID = "to" + CStr(row) + "Radio"
            toRadio.GroupName = "exceptionFromTo" + CStr(row)
            toRadio.Attributes.Add("onchange", "exception_change(this, " + CStr(row) + ")")
            fromToTableCell.Controls.Add(toRadio)

            bookFromTableCell = New TableCell()
            exceptionTableRow.Cells.Add(bookFromTableCell)

            CreateCalendarControl("bookFrom", bookFromTableCell, bookFromTextBox)

            bookToTableCell = New TableCell()
            exceptionTableRow.Cells.Add(bookToTableCell)

            CreateCalendarControl("bookTo", bookToTableCell, bookToTextBox)

            travelFromTableCell = New TableCell()
            exceptionTableRow.Cells.Add(travelFromTableCell)

            CreateCalendarControl("travelFrom", travelFromTableCell, travelFromTextBox)

            travelToTableCell = New TableCell()
            exceptionTableRow.Cells.Add(travelToTableCell)

            CreateCalendarControl("travelTo", travelToTableCell, travelToTextBox)

            changeTableCell = New TableCell()
            exceptionTableRow.Cells.Add(changeTableCell)

            changeDropdown.ID = "change" + CStr(row) + "DropDown"
            changeDropdown.Width = Unit.Parse("50px")

            Dim sv As String = changeDropdown.SelectedValue
            changeDropdown.Items.Clear()
            For c As Integer = FlexConstants.FlexChangeMin To FlexConstants.FlexChangeMax
                Dim listItem As New ListItem(IIf(c > 0, "+", "") + CStr(c), CStr(c))
                listItem.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(FlexConstants.FlexChangeToColor(c)))
                changeDropdown.Items.Add(listItem)
            Next
            If Not changeDropdown.Items.FindByValue(sv) Is Nothing Then
                changeDropdown.SelectedValue = sv
            End If
            changeDropdown.Attributes.Add("onchange", "exception_change(this, " + CStr(row) + ")")
            changeTableCell.Controls.Add(changeDropdown)

            idHidden.ID = "id" + CStr(row) + "Hidden"
            changeTableCell.Controls.Add(idHidden)

            created = True
            Refresh()
        End Sub

        Public Sub CreateCalendarControl(ByVal name As String, ByVal cell As TableCell, ByRef dateControl As UserControls_DateControl)
            dateControl.ID = name + CStr(row) + "TextBox"
            dateControl.ReadOnly = False
            dateControl.Attributes.Add("onchange", "exception_change(this, " + CStr(row) + ")")
            cell.Controls.Add(dateControl)
        End Sub

        Private Sub Refresh()
            If created Then
                ' hack hack hack hack
                Dim sv As String = locationDropdown.SelectedValue
                If parent.LocationListItems.Count > 0 Then
                    locationDropdown.Items.Clear()
                    locationDropdown.Items.Add(New ListItem("(none)", ""))
                    For Each listItem As ListItem In parent.LocationListItems
                        locationDropdown.Items.Add(New ListItem(listItem.Text, listItem.Value))
                    Next
                End If
                If Not locationDropdown.Items.FindByValue(sv) Is Nothing Then
                    locationDropdown.SelectedValue = sv
                End If

                If IsError Then
                    exceptionTableRow.CssClass = "error"
                Else
                    exceptionTableRow.CssClass = IIf(row Mod 2 = 0, "evenRow", "oddRow")
                End If

                locationDropdown.Enabled = Not IsReadOnly
                fromRadio.Enabled = Not IsReadOnly
                toRadio.Enabled = Not IsReadOnly
                If IsReadOnly Then
                    bookFromTextBox.ReadOnly = True
                    bookFromTextBox.TabIndex = -1
                    bookToTextBox.ReadOnly = True
                    bookToTextBox.TabIndex = -1
                    travelFromTextBox.ReadOnly = True
                    travelFromTextBox.TabIndex = -1
                    travelToTextBox.ReadOnly = True
                    travelToTextBox.TabIndex = -1
                Else
                    bookFromTextBox.ReadOnly = False
                    bookFromTextBox.TabIndex = 0
                    bookToTextBox.ReadOnly = False
                    bookToTextBox.TabIndex = 0
                    travelFromTextBox.ReadOnly = False
                    travelFromTextBox.TabIndex = 0
                    travelToTextBox.ReadOnly = False
                    travelToTextBox.TabIndex = 0
                End If
                changeDropdown.Enabled = Not IsReadOnly
            End If

        End Sub

        Public Property BookFrom() As Date
            Get
                Return bookFromTextBox.Date
            End Get
            Set(ByVal value As Date)
                bookFromTextBox.Date = value
                Refresh()
            End Set
        End Property

        Public Property BookTo() As Date
            Get
                Return bookToTextBox.Date
            End Get
            Set(ByVal value As Date)
                bookToTextBox.Date = value
                Refresh()
            End Set
        End Property

        Public Property LocationCode() As String
            Get
                Return locationDropdown.SelectedValue
            End Get
            Set(ByVal value As String)
                If locationDropdown.Items.FindByValue(value) Is Nothing Then
                    locationDropdown.Items.Add(New ListItem(value, value))
                End If
                locationDropdown.SelectedValue = value
                Refresh()
            End Set
        End Property

        Public Property IsFrom() As Boolean
            Get
                Return fromRadio.Checked
            End Get
            Set(ByVal value As Boolean)
                fromRadio.Checked = value
                toRadio.Checked = Not value
                Refresh()
            End Set
        End Property

        Public Property IsTo() As Boolean
            Get
                Return Not IsFrom
            End Get
            Set(ByVal value As Boolean)
                IsFrom = Not value
            End Set
        End Property

        Public Property TravelFrom() As Date
            Get
                Return travelFromTextBox.Date
            End Get
            Set(ByVal value As Date)
                travelFromTextBox.Date = value
                Refresh()
            End Set
        End Property

        Public Property TravelTo() As Date
            Get
                Return travelToTextBox.Date
            End Get
            Set(ByVal value As Date)
                travelToTextBox.Date = value
                Refresh()
            End Set
        End Property


        Public Property Change() As Integer
            Get
                Return CInt(changeDropdown.SelectedValue)
            End Get
            Set(ByVal value As Integer)
                If changeDropdown.Items.FindByValue(CStr(value)) Is Nothing Then
                    changeDropdown.Items.Add(New ListItem(CStr(value), CStr(value)))
                End If
                changeDropdown.SelectedValue = CStr(value)
                Refresh()
            End Set
        End Property

        Public Property Id() As Integer
            Get
                Return CInt(idHidden.Value)
            End Get
            Set(ByVal value As Integer)
                idHidden.Value = value
                Refresh()
            End Set
        End Property

        Public Property IsError() As Boolean
            Get
                Return CBool(parent.ViewState("Error" + CStr(row)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("Error" + CStr(row)) = CStr(value)
                Refresh()
            End Set
        End Property

        Public Property IsReadOnly() As Boolean
            Get
                Return CBool(parent.ViewState("ReadOnly" + CStr(row)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("ReadOnly" + CStr(row)) = value
                Refresh()
            End Set
        End Property

    End Class

    Private _locationLocationListItems As New List(Of ListItem)
    Public ReadOnly Property LocationListItems() As List(Of ListItem)
        Get
            Return _locationLocationListItems
        End Get
    End Property


    Private exceptionRows As New List(Of ExceptionRow)
    Public ReadOnly Property Rows(ByVal row As Integer) As ExceptionRow
        Get
            Return exceptionRows(row)
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        For row As Integer = 0 To FlexConstants.ExceptionMax - 1
            exceptionRows.Add(New ExceptionRow(Me, row))
        Next
    End Sub

    Public Sub Clear()
        For row As Integer = 0 To FlexConstants.ExceptionMax - 1
            Dim exceptionRow As Flex_ExceptionControl.ExceptionRow = Rows(row)
            exceptionRow.Id = -1
            exceptionRow.LocationCode = ""
            exceptionRow.IsFrom = False
            exceptionRow.IsTo = False
            exceptionRow.BookFrom = Date.MinValue
            exceptionRow.BookTo = Date.MinValue
            exceptionRow.TravelFrom = Date.MinValue
            exceptionRow.TravelTo = Date.MinValue
            exceptionRow.Change = 0
            exceptionRow.IsReadOnly = False
        Next
    End Sub

End Class
