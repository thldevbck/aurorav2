Imports System
Imports System.Drawing
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


<ToolboxData("<{0}:RatesControl runat=server></{0}:RatesControl>")> _
Partial Class Flex_RatesControl
    Inherits System.Web.UI.UserControl

  

    Public Class RateCell
        Private parent As Flex_RatesControl
        Private weekNo As Integer
        Private col As Integer
        Private row As Integer

        Private travelStartTableCell As WebControls.TableCell
        Private rateTableCell As WebControls.TableCell
        Private fleetStatusTableCell As WebControls.TableCell

        Private rateTextBox As New WebControls.TextBox()
        Private prevHiddenField As New WebControls.HiddenField()
        Public fleetStatusTextBox As New WebControls.TextBox()

        Private created As Boolean = False

        Public Sub New(ByVal parent As Flex_RatesControl, ByVal weekNo As Integer)
            Me.parent = parent
            Me.weekNo = weekNo
            Me.col = (weekNo - 1) Mod 13 + 1
            Me.row = Int((weekNo - 1) / 13) * 3 + 1
        End Sub

        Public Sub CreateControls()
            travelStartTableCell = parent.ratesTable.Rows(row).Cells(col)

            rateTableCell = parent.ratesTable.Rows(row + 1).Cells(col)
            rateTableCell.ID = "rate" + CStr(weekNo) + "TableCell"

            rateTextBox.ID = "rate" + CStr(weekNo) + "TextBox"
            rateTextBox.MaxLength = 2
            rateTextBox.Attributes.Add("onchange", "rateTextBox_change(event, " + CStr(weekNo) + ")")
            'rateTextBox.Attributes.Add("onkeypress", "return rateTextBox_keypress(event, " + CStr(weekNo) + ")")
            rateTableCell.Controls.Add(rateTextBox)

            prevHiddenField.ID = "prev" + CStr(weekNo) + "HiddenField"
            rateTableCell.Controls.Add(prevHiddenField)

            fleetStatusTableCell = parent.ratesTable.Rows(row + 2).Cells(col)
            fleetStatusTableCell.ID = "fleetStatus" + CStr(weekNo) + "TableCell"

            fleetStatusTextBox.ID = "fleetStatus" + CStr(weekNo) + "Textbox"
            fleetStatusTextBox.ReadOnly = True
            fleetStatusTextBox.CssClass = "inputreadonly"
            fleetStatusTextBox.TabIndex = -1
            fleetStatusTextBox.EnableViewState = False
            fleetStatusTableCell.Controls.Add(fleetStatusTextBox)

            created = True
            Refresh()
        End Sub

        Private Sub Refresh()
            If created Then
                travelStartTableCell.Text = TravelStart

                If IsError OrElse FlexNum = Integer.MinValue Then
                    rateTextBox.BackColor = Color.White
                    rateTableCell.BackColor = FlexConstants.FlexErrorColor
                    fleetStatusTableCell.BackColor = FlexConstants.FlexErrorColor
                ElseIf IsChanged Then
                    rateTextBox.BackColor = FlexConstants.FlexRateChangedColor
                    rateTableCell.BackColor = FlexConstants.FlexNumToColor(FlexNum)
                    fleetStatusTableCell.BackColor = FlexConstants.FlexNumToColor(FlexNum)
                Else
                    rateTextBox.BackColor = Color.White
                    rateTableCell.BackColor = FlexConstants.FlexNumToColor(FlexNum)
                    fleetStatusTableCell.BackColor = FlexConstants.FlexNumToColor(FlexNum)
                End If

                If IsPast Then
                    rateTextBox.ReadOnly = True
                    rateTextBox.CssClass = "inputreadonly"
                    rateTextBox.TabIndex = -1
                    If Not IsChanged Or IsError Then
                        rateTextBox.BackColor = FlexConstants.FlexRateReadonlyColor
                    End If
                ElseIf IsReadOnly Then
                    rateTextBox.ReadOnly = True
                    rateTextBox.CssClass = "inputreadonly"
                    rateTextBox.TabIndex = -1
                Else
                    rateTextBox.ReadOnly = False
                    rateTextBox.CssClass = ""
                    rateTextBox.TabIndex = 0
                End If

                ''System.Diagnostics.Debug.Assert(weekNo < 53)
                fleetStatusTextBox.Text = FleetStatus
                fleetStatusTextBox.BackColor = FlexConstants.FleetStatusToColor(FleetStatus)

                If IsCurrent Then
                    travelStartTableCell.CssClass = "tdCurrent"
                ElseIf IsPast Then
                    travelStartTableCell.CssClass = "tdPast"
                Else
                    travelStartTableCell.CssClass = ""
                End If
            End If
        End Sub

        Public Property TravelStart() As String
            Get
                Return CStr(parent.ViewState("TravelStart" + CStr(weekNo)))
            End Get
            Set(ByVal value As String)
                ''  System.Diagnostics.Debug.Assert(weekNo < 51)
                parent.ViewState("TravelStart" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property FlexNum() As Integer
            Get
                Return FlexConstants.FlexRateToNum(rateTextBox.Text)
            End Get
            Set(ByVal value As Integer)
                rateTextBox.Text = FlexConstants.FlexNumToRate(value)
                IsError = rateTextBox.Text = ""
                IsChanged = FlexNum <> PrevFlexNum
            End Set
        End Property

        Public Property PrevFlexNum() As Integer
            Get
                Return FlexConstants.FlexRateToNum(prevHiddenField.Value)
            End Get
            Set(ByVal value As Integer)
                prevHiddenField.Value = FlexConstants.FlexNumToRate(value)
                IsChanged = FlexNum <> PrevFlexNum
            End Set
        End Property

        Public Property FleetStatus() As String
            Get
                Return CStr(parent.ViewState("FleetStatus" + CStr(weekNo)))
            End Get
            Set(ByVal value As String)
                parent.ViewState("FleetStatus" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property IsCurrent() As Boolean
            Get
                Return CBool(parent.ViewState("Current" + CStr(weekNo)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("Current" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property IsPast() As Boolean
            Get
                Return CBool(parent.ViewState("Past" + CStr(weekNo)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("Past" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property IsError() As Boolean
            Get
                Return CBool(parent.ViewState("Error" + CStr(weekNo)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("Error" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property IsChanged() As Boolean
            Get
                Return CBool(parent.ViewState("Changed" + CStr(weekNo)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("Changed" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property

        Public Property IsReadOnly() As Boolean
            Get
                Return CBool(parent.ViewState("ReadOnly" + CStr(weekNo)))
            End Get
            Set(ByVal value As Boolean)
                parent.ViewState("ReadOnly" + CStr(weekNo)) = value
                Refresh()
            End Set
        End Property
    End Class

    Private rateCellsByWeekNo As New Dictionary(Of Integer, RateCell)
    Public ReadOnly Property Cells(ByVal weekNo As Integer) As RateCell
        Get
            Return rateCellsByWeekNo(weekNo)
        End Get
    End Property

    Public ReadOnly Property RatesColorsJSON() As String
        Get
            Dim result As New StringBuilder()
            result.Append("{")
            result.Append( _
                "'error' : " + _
                "'" + ColorTranslator.ToHtml(FlexConstants.FlexErrorColor) + "',")
            result.Append( _
                "'changed' : " + _
                "'" + ColorTranslator.ToHtml(FlexConstants.FlexRateChangedColor) + "',")
            For i As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
                result.Append( _
                    "'" + FlexConstants.FlexIntToRate(i) + "' : " + _
                    "'" + ColorTranslator.ToHtml(FlexConstants.FlexIntToColor(i)) + "'" + _
                    IIf(i <> FlexConstants.FlexIntMax, ",", ""))
            Next
            result.Append("}")
            Return result.ToString()
        End Get
    End Property

    Public Sub New()
        ''rev:mia 10sept2015-I have created new travel date as 28-Mar-2015 week number 53 and it's not working on staging
        For weekNo As Integer = FlexConstants.WeekNoMin To FlexConstants.GlobalWeekNoMax
            rateCellsByWeekNo.Add(weekNo, New RateCell(Me, weekNo))
        Next
    End Sub

    Protected Overrides Sub CreateChildControls()
        MyBase.CreateChildControls()

        For Each rateCell As RateCell In rateCellsByWeekNo.Values
            rateCell.CreateControls()
        Next
    End Sub

    Public Sub Clear()
        ''rev:mia 10sept2015-I have created new travel date as 28-Mar-2015 week number 53 and it's not working on staging
        For weekNo As Integer = FlexConstants.WeekNoMin To FlexConstants.GlobalWeekNoMax
            Dim rateCell As RateCell = Cells(weekNo)
            rateCell.FlexNum = FlexConstants.FlexNumDefault
            rateCell.PrevFlexNum = FlexConstants.FlexNumDefault
            rateCell.IsError = False
            rateCell.FleetStatus = ""
            rateCell.IsPast = False
            rateCell.IsCurrent = False
            rateCell.IsReadOnly = False
        Next
    End Sub
End Class
