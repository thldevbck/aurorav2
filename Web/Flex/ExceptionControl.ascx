<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ExceptionControl.ascx.vb" Inherits="Flex_ExceptionControl" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>

<asp:Table ID="exceptionTable" runat="server" CssClass="dataTable exceptionTable" CellPadding="2" CellSpacing="0">
    <asp:TableHeaderRow>
        <asp:TableHeaderCell ColumnSpan="7">Exceptions</asp:TableHeaderCell>
    </asp:TableHeaderRow>
    <asp:TableHeaderRow>
        <asp:TableHeaderCell Style="width: 160px;">Location</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">From / To</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">Book From</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">Book To</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">Travel From</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">Travel To</asp:TableHeaderCell>
        <asp:TableHeaderCell Style="width: 95px;">Change</asp:TableHeaderCell>
    </asp:TableHeaderRow>
</asp:Table>
