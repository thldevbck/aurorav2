<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RatesControl.ascx.vb" Inherits="Flex_RatesControl" %>

<asp:Table CssClass="dataTableGrid ratesTable" ID="ratesTable" runat="server" CellPadding="2" CellSpacing="0" Style="border-width: 1px">

    <asp:TableHeaderRow>
        <asp:TableHeaderCell ColumnSpan="14">Rates</asp:TableHeaderCell>
    </asp:TableHeaderRow>

    <asp:TableHeaderRow CssClass="trTravelStart">
        <asp:TableHeaderCell Text="Travel Start:" CssClass="thTravelStart" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableHeaderRow>
    <asp:TableRow CssClass="trRate">
        <asp:TableHeaderCell Text="Rate:" CssClass="thRate" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="trFleetStatus" >
        <asp:TableHeaderCell Text="Fleet Status:" CssClass="thFleetStatus" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>

    <asp:TableHeaderRow CssClass="trTravelStart">
        <asp:TableHeaderCell Text="Travel Start:" CssClass="thTravelStart" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableHeaderRow>
    <asp:TableRow CssClass="trRate">
        <asp:TableHeaderCell Text="Rate:" CssClass="thRate" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="trFleetStatus" >
        <asp:TableHeaderCell Text="Fleet Status:" CssClass="thFleetStatus" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>

    <asp:TableHeaderRow CssClass="trTravelStart">
        <asp:TableHeaderCell Text="Travel Start:" CssClass="thTravelStart" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableHeaderRow>
    <asp:TableRow CssClass="trRate">
        <asp:TableHeaderCell Text="Rate:" CssClass="thRate" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="trFleetStatus" >
        <asp:TableHeaderCell Text="Fleet Status:" CssClass="thFleetStatus" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>

    <asp:TableHeaderRow CssClass="trTravelStart">
        <asp:TableHeaderCell Text="Travel Start:" CssClass="thTravelStart" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableHeaderRow>
    <asp:TableRow CssClass="trRate">
        <asp:TableHeaderCell Text="Rate:" CssClass="thRate" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="trFleetStatus" >
        <asp:TableHeaderCell Text="Fleet Status:" CssClass="thFleetStatus" />
        <asp:TableCell>1</asp:TableCell><asp:TableCell>2</asp:TableCell><asp:TableCell>3</asp:TableCell><asp:TableCell>4</asp:TableCell><asp:TableCell>5</asp:TableCell><asp:TableCell>6</asp:TableCell><asp:TableCell>7</asp:TableCell><asp:TableCell>8</asp:TableCell><asp:TableCell>9</asp:TableCell><asp:TableCell>10</asp:TableCell><asp:TableCell>11</asp:TableCell><asp:TableCell>12</asp:TableCell><asp:TableCell>13</asp:TableCell>
    </asp:TableRow>

    <%--rev:mia 10sept2015-I have created new travel date as 28-Mar-2015 week number 53 and it's not working on staging--%>
    <asp:TableHeaderRow CssClass="trTravelStart">
        <asp:TableHeaderCell Text="Travel Start:" CssClass="thTravelStart" />
        <asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell>
    </asp:TableHeaderRow>
    <asp:TableRow CssClass="trRate">
        <asp:TableHeaderCell Text="Rate:" CssClass="thRate" />
        <asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="trFleetStatus" >
        <asp:TableHeaderCell Text="Fleet Status:" CssClass="thFleetStatus" />
        <asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell></asp:TableCell>
    </asp:TableRow>
</asp:Table>

