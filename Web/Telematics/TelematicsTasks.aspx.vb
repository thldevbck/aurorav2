﻿
Imports System.Collections.Generic
Imports Aurora.Telematics.Data
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports Aurora.Telematics.Service.Service.Services
Imports Aurora.Telematics.Service.Services


Partial Class TelematicsTasks
    Inherits System.Web.UI.Page

    Private vehSer As VehicleService
    Private conSer As ConnectionService

    Private m_connectedSessionId As Guid

    Public Property ConnectedSessionID() As Guid
        Get
            Return m_connectedSessionId
        End Get
        Set(value As Guid)
            m_connectedSessionId = value
        End Set
    End Property

    Protected Sub btnUpd_Click(sender As Object, e As EventArgs) Handles btnUpd.Click
        'LoggingService.LogInformation("UpdateVehicleBookingId", "Received request to change bookingID to [" + newBookingId + "] for vehicle with GUID [" + vehicleGUID + "]")
        Dim message As String = String.Empty
        lblStatus.Text = ""
        If ConnectedSessionID.Equals(Guid.Empty) Then
            'LoggingService.LogError("UpdateVehicleBookingId", "Unsuccessful Login");
            message = "Your Login Was Unsuccessful"
        Else
            message = "Your Login was successful"
            'var vehList = vehSer.GetVehicleList(ConnectedSessionID);
            'foreach (var veh in vehList)
            '    Console.WriteLine(veh.Name);

            'string deviceToSearch = "THL_TEST116";
            'Dim vehByGUID As i360Vehicle = vehSer.GetVehicleByGUID(txtGUID.Text, ConnectedSessionID)

            'If vehByGUID Is Nothing Then
            '    message = "No vehicle Found with GUID:" + txtGUID.Text
            'Else
            '    Dim newBookingId As String = txtBookingId.Text
            '    vehSer.SetVehicleAttribute("BookingID", newBookingId, vehByGUID, ConnectedSessionID)
            '    message = "Booking Number for unit number " + vehByGUID.Name + " changed to " + newBookingId



            'End If
            vehSer.UpdateVehicleByGUID(txtGUID.Text, txtBookingId.Text, ConnectedSessionID)
        End If
        lblStatus.Text = message
        'Return retMessage

    End Sub

    Public Sub New()

        vehSer = New VehicleService()

        conSer = New ConnectionService()
        Dim userName As String = ConfigurationManager.AppSettings("UserName").ToString()
        Dim userPassword As String = ConfigurationManager.AppSettings("Password").ToString()
        Try
            ConnectedSessionID = conSer.Login(userName, userPassword)
        Catch ex As Exception

            Dim exMessage As String = ex.Message
        End Try



    End Sub

    Protected Sub getVehicleList_Click(sender As Object, e As EventArgs) Handles getVehicleList.Click
        Dim retMessage As String
        Dim message As String = String.Empty


        If ConnectedSessionID.Equals(Guid.Empty) Then
            'LoggingService.LogError("UpdateVehicleBookingId", "Unsuccessful Login");
            retMessage = "Your Login Was Unsuccessful"
        Else
            message = "Your Login was successful"
            Dim I360VehList As List(Of i360Vehicle) = vehSer.GetVehicleList(ConnectedSessionID)
            For Each curVeh As i360Vehicle In I360VehList
                Dim curVehId As Guid = curVeh.ID
                Dim curVehName As String = curVeh.Name
            Next
        End If

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Dim fleetSer As FleetService = New FleetService

        lblStatus.Text = ""

        If fleetSer.UpdateFleetGUIDs(ConnectedSessionID) Then
            lblStatus.Text = "updated table"
        End If

        'Aurora.Common.Data.ExecuteNonQuery(

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim fleetSer As FleetService = New FleetService

        lblStatus.Text = ""

        'If fleetSer.UpdateFleetGUID(ConnectedSessionID, txtUnitNo.Text) Then
        '    lblStatus.Text = "updated table"
        'End If
        lblStatus.Text = fleetSer.UpdateFleetGUID(ConnectedSessionID, txtUnitNo.Text)
    End Sub
End Class
