Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Public Class ReportDataSet

    Partial Public Class ReportParamRow

        Public ReadOnly Property ReportDataSet() As ReportDataSet
            Get
                Return CType(Me.Table.DataSet, ReportDataSet)
            End Get
        End Property


        Private _tag As Object
        Public Property Tag() As Object
            Get
                Return _tag
            End Get
            Set(ByVal value As Object)
                _tag = value
            End Set
        End Property


        Private _lookupDataTable As LookupDataTable
        Public ReadOnly Property LookupDataTable()
            Get
                If _lookupDataTable IsNot Nothing Then
                    Return _lookupDataTable
                End If

                If Me.IsrppLookupSqlNull OrElse String.IsNullOrEmpty(Me.rppLookupSql.Trim()) Then
                    Return Nothing
                End If

                _lookupDataTable = New LookupDataTable()

                Dim database As Database = Aurora.Common.Data.GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(Me.rppLookupSql)
                    command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                    ReportDataSet.AddEnvironmentParamsToCommand(command)

                    Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                        da.Fill(_lookupDataTable)
                    End Using

                    Return _lookupDataTable
                End Using
            End Get
        End Property


        Public ReadOnly Property DefaultValue() As Object
            Get
                If Not Me.IsrppDefaultValueNull Then
                    Return Me.rppDefaultValue
                ElseIf Me.ReportRow.DefaultValueDataRow IsNot Nothing _
                 AndAlso Me.ReportRow.DefaultValueDataRow.Table.Columns.Contains(Me.rppName) _
                 AndAlso Not Me.ReportRow.DefaultValueDataRow.IsNull(Me.rppName) Then
                    Return Me.ReportRow.DefaultValueDataRow(Me.rppName)
                Else
                    Return Nothing
                End If
            End Get
        End Property

    End Class

End Class
