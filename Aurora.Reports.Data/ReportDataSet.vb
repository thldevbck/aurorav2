﻿Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class ReportDataSet
    Public Const CodeParam As String = "funCode"

    Public ReadOnly Property RootReport() As ReportRow
        Get
            If (Me.Report.Count > 0) Then
                Return Me.Report(0)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Private _environmentParams As New Dictionary(Of String, Object)
    Public ReadOnly Property EnvironmentParams() As Dictionary(Of String, Object)
        Get
            Return _environmentParams
        End Get
    End Property


    Public Sub AddEnvironmentParamsToCommand(ByVal dbCommand As DbCommand)
        If dbCommand Is Nothing Then Throw New ArgumentNullException("dbCommand")
        If _environmentParams Is Nothing Then Return

        For Each key As String In _environmentParams.Keys
            For Each p As DbParameter In dbCommand.Parameters
                If p.ParameterName = key Then Continue For
            Next

            Dim dbParameter As DbParameter = dbCommand.CreateParameter()
            dbParameter.ParameterName = key
            If _environmentParams(key) IsNot Nothing Then
                dbParameter.Value = _environmentParams(key)
            Else
                dbParameter.Value = DBNull.Value
            End If
            dbCommand.Parameters.Add(dbParameter)
        Next
    End Sub


    Public Sub LoadFromParams(ByVal params As NameValueCollection)

        If Me.Report.Count > 0 Then Throw New ValidationException("Report already loaded")

        If params Is Nothing Then Throw New ArgumentNullException("params")
        If String.IsNullOrEmpty(params.Get(CodeParam)) Then Throw New ValidationException("Report Code not specified")

        DataRepository.GetReportByFunCode(Me, "" & params(CodeParam))
        If Me.Report.Count = 0 Then Throw New ValidationException("Report Code is invalid")
    End Sub

End Class
