Imports System.Data
Imports System.Web
Imports System.Configuration

Imports Aurora.Reports.Data.ReportDataSet

Public Module DataRepository

    Private Function GetReport( _
     ByVal reportDataSet As ReportDataSet, _
     ByVal repId As String, _
     ByVal funCode As String) As ReportDataSet

        'CREATE PROCEDURE [dbo].[Report_Get] 
        '	@repId AS varchar(64),
        '	@funCode AS varchar(12)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Report_Get", result, _
            IIf(repId IsNot Nothing, repId, DBNull.Value), _
            IIf(funCode IsNot Nothing, funCode, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), reportDataSet.Report)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), reportDataSet.ReportParam)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), reportDataSet.ReportParamListItem)

        Return reportDataSet

    End Function

    Public Function GetReportById( _
     ByVal reportDataSet As ReportDataSet, _
     ByVal repId As String) As ReportDataSet
        Return GetReport(reportDataSet, repId, Nothing)
    End Function

    Public Function GetReportByFunCode( _
     ByVal reportDataSet As ReportDataSet, _
     ByVal funCode As String) As ReportDataSet
        Return GetReport(reportDataSet, Nothing, funCode)
    End Function

    Public Function GetReports( _
     ByVal reportDataSet As ReportDataSet) As ReportDataSet
        Return GetReport(reportDataSet, Nothing, Nothing)
    End Function

    Public Sub DeleteReport( _
     ByVal repId As String)

        'CREATE PROCEDURE [dbo].[Report_Delete] 
        '	@repId AS varchar(64)

        Aurora.Common.Data.ExecuteScalarSP("Report_Delete", repId)

    End Sub


End Module
