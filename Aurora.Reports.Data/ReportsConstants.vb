Public Class ReportsConstants
    Inherits Aurora.Common.DataConstants

    Public Const ReportParamType_TEXT As String = "TEXT"
    Public Const ReportParamType_RADIO As String = "RADIO"
    Public Const ReportParamType_DROPDOWN As String = "DROPDOWN"
    Public Const ReportParamType_CHECKBOX As String = "CHECKBOX"
    Public Const ReportParamType_DATE As String = "DATE"

End Class
