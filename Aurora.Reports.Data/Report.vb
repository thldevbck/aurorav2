Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Public Class ReportDataSet

    Partial Public Class ReportRow

        Public ReadOnly Property ReportDataSet() As ReportDataSet
            Get
                Return CType(Me.Table.DataSet, ReportDataSet)
            End Get
        End Property

        Private _defaultValueDataRow As DataRow = Nothing
        Public ReadOnly Property DefaultValueDataRow() As DataRow
            Get
                If _defaultValueDataRow IsNot Nothing Then
                    Return _defaultValueDataRow
                End If

                If Me.IsrepDefaultSqlNull Then
                    Return Nothing
                End If

                Dim database As Database = Aurora.Common.Data.GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(Me.repDefaultSql)
                    command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                    ReportDataSet.AddEnvironmentParamsToCommand(command)

                    Dim result As New DataTable()
                    Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                        da.Fill(result)
                    End Using

                    If result.Rows.Count = 1 Then
                        _defaultValueDataRow = result.Rows(0)
                    End If

                    Return _defaultValueDataRow
                End Using
            End Get
        End Property

    End Class

End Class
