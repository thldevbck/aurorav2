Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration


Public Class ReportsConfiguration

    Public ReportsServiceUrl As String = "http://akl-kxdev-001/ReportServer$SQLServer2005/ReportService2005.asmx"
    Public ReportsExecutionUrl As String = "http://akl-kxdev-001/ReportServer$SQLServer2005/Execution2005.asmx"
    Public ReportsUrl As String = "http://akl-kxdev-001/reportserver$SQLServer2005"
    Public ReportsIFrameWidth As String = "780px"
    Public ReportsIFrameHeight As String = "500px"

    Private Shared _instance As ReportsConfiguration
    Public Shared ReadOnly Property Instance() As ReportsConfiguration
        Get
            If _instance Is Nothing Then _instance = CType(ConfigurationManager.GetSection("ReportsConfiguration"), ReportsConfiguration)
            If _instance Is Nothing Then _instance = New ReportsConfiguration()
            Return _instance
        End Get
    End Property

End Class


Public Class ReportsConfigurationSectionHandler
    Implements IConfigurationSectionHandler

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
        ByVal section As System.Xml.XmlNode) As Object _
        Implements System.Configuration.IConfigurationSectionHandler.Create

        Dim xs As XmlSerializer = New XmlSerializer(GetType(ReportsConfiguration))
        Return xs.Deserialize(New XmlNodeReader(section))
    End Function

End Class
