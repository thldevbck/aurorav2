
#Region "Imports"
Imports Aurora.Common
Imports System.Data.Common
Imports System.Data
Imports System.Web
Imports System.Configuration
Imports System.Xml
Imports System.Xml.XmlNode

#End Region

Public Module DataRepository

    Public Function GetMyVehicleCities(ByVal country As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyVehicleCities", New DataTable, country, user))   
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyVehicles(ByVal country As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyVehicles", New DataTable, country, user))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSubstitutions(ByVal country As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getSubstitutions", New DataTable, country, user))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyBookingAssignments(ByVal country As String, ByVal user As String, ByVal isScheduled As Boolean) As DataTable
        Try
            If isScheduled Then
                Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyBookingAssignments", New DataTable, country, user, 1))
            Else
                Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyBookingAssignments", New DataTable, country, user, 0))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyRelocations(ByVal country As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyRelocations", New DataTable, country, user))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyOffFleets(ByVal country As String, ByVal user As String, ByVal isSucceed As Boolean) As DataTable
        Try
            If isSucceed Then
                Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyOffFleets", New DataTable, country, user, 1))
            Else
                Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyOffFleets", New DataTable, country, user, 0))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyDisposals(ByVal country As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyDisposals", New DataTable, country, user))
        Catch e As Exception
            Return Nothing
        End Try
    End Function

    Public Function GetCurrentActivities(ByVal country As String, ByVal user As String) As DataTable
        Try
            If country = "NZ" Then
                country = "NZL"
            Else
                If country = "AU" Then
                    country = "AUS"
                End If
            End If

            Return (Aurora.Common.Data.ExecuteTableSP("schV_getCurrentActivities", New DataTable, country, user))

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCurrentOffFleets(ByVal country As String, ByVal user As String) As DataTable
        Try
            If country = "NZ" Then
                country = "NZL"
            Else
                If country = "AU" Then
                    country = "AUS"
                End If
            End If

            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyCurrOffFleets", New DataTable, country, user))

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMyRentalDetails(ByVal bookingNum As String, ByVal rantalNumber As String, ByVal user As String) As DataTable
        Try
            Return (Aurora.Common.Data.ExecuteTableSP("schV_getMyRentalDetails", New DataTable, bookingNum, rantalNumber, user))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Module



