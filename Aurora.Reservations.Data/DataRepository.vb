Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common

' this class holds db stuff for Reservations work
Public Class DataRepository

#Region "Booking Extension releated stuff"

    ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
    Public Shared Function ManageBookedProductExtension( _
                            ByVal BookedProductID As String, _
                            ByVal ExtCkoWhen As DateTime, _
                            ByVal ExtCkiWhen As DateTime, _
                            ByVal ApplyOriginalRates As Boolean, _
                            ByVal UserId As String, _
                            ByVal ProgramName As String, _
                            Optional ByRef Errors As String = "", _
                            Optional ByRef BPDIdsList As String = "", _
                            Optional ByRef Warning As String = "", _
                            Optional SlotId As String = "", _
                            Optional SlotDescription As String = "", _
                            Optional RentalId As String = "" _
                            ) As String 'As Aurora.Common.Data.Parameter()
        Try
            'Dim xmlDoc As XmlDocument
            ' xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getHirePeriod", BPDId, sCkoWhen, sCkiWhen, UOMId)

            Dim oParamArray(9) As Aurora.Common.Data.Parameter

            '@psBpdId    varchar(64),  
            '@pdExtCkoWhen   datetime,  
            '@pdExtCkiWhen   datetime,  
            '@psUserId    varchar(64),  
            '@psProgramName   varchar(64),  
            '@pbApplyOrigRates  bit,  
            '@psCancelScreen   varchar(1000) = NULL,  
            '@psErrors    varchar(1000)  OUTPUT,  
            '@psBpdIdsList   VARCHAR  (2000) OUTPUT,   
            '@sWarning    VARCHAR(2000)   OUTPUT  
            '' params(0) = New Aurora.Common.Data.Parameter("psBooID", DbType.String, bookingId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)


            oParamArray(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, BookedProductID, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(1) = New Aurora.Common.Data.Parameter("pdExtCkoWhen", DbType.DateTime, ExtCkoWhen, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(2) = New Aurora.Common.Data.Parameter("pdExtCkiWhen", DbType.DateTime, ExtCkiWhen, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(4) = New Aurora.Common.Data.Parameter("psUserId", DbType.String, UserId, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(5) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, ProgramName, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(3) = New Aurora.Common.Data.Parameter("pbApplyOrigRates", DbType.Boolean, ApplyOriginalRates, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(6) = New Aurora.Common.Data.Parameter("psCancelScreen", DbType.String, 1000, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(7) = New Aurora.Common.Data.Parameter("psErrors", DbType.String, 1000, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(8) = New Aurora.Common.Data.Parameter("psBpdIdsList", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(9) = New Aurora.Common.Data.Parameter("sWarning", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)

            ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
            'If (String.IsNullOrEmpty(SlotId)) Then
            '    SlotId = "-1"
            'End If
            'oParamArray(10) = New Aurora.Common.Data.Parameter("SlotId", DbType.Int16, IIf(SlotId = "-1", DBNull.Value, SlotId), Common.Data.Parameter.ParameterType.AddInParameter)
            'oParamArray(11) = New Aurora.Common.Data.Parameter("SlotDescription", DbType.String, SlotDescription, Common.Data.Parameter.ParameterType.AddInParameter)
            'oParamArray(12) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("RES_ManageBookedProductExtension", oParamArray)

            Errors = oParamArray(7).Value
            BPDIdsList = oParamArray(8).Value
            Warning = oParamArray(9).Value


            'Dim psCancelScreen, psErrors, psBpdIdsList, sWarning
            'Dim oXml
            'oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_ManageBookedProductExtension", _
            '                                                                    BookedProductID, _
            '                                                                    ExtCkoWhen, _
            '                                                                    ExtCkiWhen, _
            '                                                                    UserId, _
            '                                                                    ProgramName, _
            '                                                                    ApplyOriginalRates, _
            '                                                                    psCancelScreen, _
            '                                                                    psErrors, _
            '                                                                    psBpdIdsList, _
            '                                                                    sWarning)


            '  Return oParamArray

            ' Dim oParamArray As New Aurora.Common.Data.Parameter()


            'With oAuroraDAL
            '    ' INPUT parameters
            '    Call .appendParameter("@psBpdId", adVarChar, 64, BookedProductID)
            '    Call .appendParameter("@pdExtCkoWhen", adDate, 8, ExtCkoWhen)
            '    Call .appendParameter("@pdExtCkiWhen", adDate, 8, ExtCkiWhen)
            '    Call .appendParameter("@pbApplyOrigRates", adBoolean, 0, ApplyOriginalRates)
            '    ' Audit columns
            '    Call .appendParameter("@psUserId", adVarChar, 64, UserId)
            '    Call .appendParameter("@psProgramName", adVarChar, 64, ProgramName)
            '    Call .appendParameter("@psCancelScreen", adVarChar, 64, Nothing)
            '    ' OUTPUT parameters
            '    Call .appendParameter2("@psErrors", adVarChar, 2000, strError, True)
            '    Call .appendParameter2("@psBpdIdsList", adVarChar, 8000, strError, True)
            '    Call .appendParameter2("@sWarning", adVarChar, 2000, strReturnMessage, True)
            '    'Call .appendParameter2("psBpdIdNew", adVarChar, 64, BookedProductIDNew, True)
            '    ' QUERY is simply stored procedure name
            '    strQueryString = "RES_ManageBookedProductExtension"
            '    ' EXECUTE it
            '    strError = .updateRecords(strQueryString, "", True)
            '    If strError = "SUCCESS" Then strError = ""

            '    oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & " RETURN Value :" & strError)
            '    ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '    ' IF an error is not returned, we want
            '    ' to retrieve the pReturnError output
            '    ' parameter from the collection
            '    ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '    If Len(Trim(strError)) = 0 Then
            '        strReturnError = .RetOutPutValue("@psErrors")
            '        If Len(Trim(strReturnError)) <> 0 Then
            '            oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & " RETURN Value :" & strReturnError)
            '            GoTo GarbageCleaner
            '        End If
            '        sNewBpdId = Replace(.RetOutPutValue("@psBpdIdsList"), ",", "")
            '        strHistError = CreateHistory("", "", sNewBpdId, "", "", "BPD", "MODIFY", UserId, ProgramName & " COM, _EXT")
            '    Else
            '        strReturnError = strError
            '        GoTo GarbageCleaner
            '    End If
            '    strReturnMessage = .RetOutPutValue("@sWarning")
            'End With ' oAuroraDAL
            If Trim(Errors) = "" Then
                If Trim(Warning) = "" Then
                    Return String.Empty
                Else
                    Return Warning
                End If
            Else
                Return Errors
            End If



        Catch ex As Exception
            Return "ERROR/" & ex.Message
        End Try
    End Function



    Public Shared Function FetchDetailForExtension(ByVal RentalId As String, _
Optional ByRef Extref As String = "", _
Optional ByRef RntStatus As String = "", _
Optional ByRef CkoWhen As DateTime = Nothing, _
Optional ByRef CkiWhen As DateTime = Nothing, _
Optional ByRef CkoLocCode As String = "", _
Optional ByRef CkiLocCode As String = "", _
Optional ByRef DVASSSeqNum As String = "", _
Optional ByRef UnitNum As String = "", _
Optional ByRef RegoNum As String = "", _
Optional ByRef CkoDayPart As String = "", _
Optional ByRef CkiDayPart As String = "", _
Optional ByRef OdometerOut As Int64 = 0, _
Optional ByRef UpdateFleet As Boolean = False _
    ) As String ' As Aurora.Common.Data.Parameter()

        Try

            Dim oParamArray(13) As Aurora.Common.Data.Parameter

            oParamArray(0) = New Aurora.Common.Data.Parameter("sRntId", System.Data.DbType.String, RentalId, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(1) = New Aurora.Common.Data.Parameter("sExtRef", System.Data.DbType.String, 128, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(2) = New Aurora.Common.Data.Parameter("sRntStatus", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(3) = New Aurora.Common.Data.Parameter("sCkoWhen", System.Data.DbType.DateTime, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(4) = New Aurora.Common.Data.Parameter("sCkiWhen", System.Data.DbType.DateTime, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(5) = New Aurora.Common.Data.Parameter("sCkoLocCode", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(6) = New Aurora.Common.Data.Parameter("sCkiLocCode", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(7) = New Aurora.Common.Data.Parameter("sDVASSSeqNum", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(8) = New Aurora.Common.Data.Parameter("sUnitNum", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(9) = New Aurora.Common.Data.Parameter("sRegoNum", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(10) = New Aurora.Common.Data.Parameter("sCkoDayPart", System.Data.DbType.String, 3, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(11) = New Aurora.Common.Data.Parameter("sCkiDayPart", System.Data.DbType.String, 3, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(12) = New Aurora.Common.Data.Parameter("sOddometerOut", System.Data.DbType.Int64, 64, Common.Data.Parameter.ParameterType.AddOutParameter)
            oParamArray(13) = New Aurora.Common.Data.Parameter("bUpdateFleet", System.Data.DbType.Boolean, 1, Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("RES_FetchDetailForExtension", oParamArray)

            '  Return oParamArray

            '@sRntId			VARCHAR	 (64)	=	'',
            '@sExtRef		VARCHAR	 (128)	OUTPUT ,
            '@sRntStatus		VARCHAR	 (64)	OUTPUT ,
            '@sCkoWhen		DATETIME		OUTPUT ,
            '@sCkiWhen		DATETIME		OUTPUT ,
            '@sCkoLocCode	VARCHAR  (64)	OUTPUT ,
            '@sCkiLocCode	VARCHAR  (64)	OUTPUT ,
            '@sDVASSSeqNum	VARCHAR	 (64)	OUTPUT ,
            '@sUnitNum		VARCHAR	 (64)	OUTPUT ,
            '@sRegoNum		VARCHAR	 (64)	OUTPUT ,
            '@sCkoDayPart	VARCHAR	 (3)	OUTPUT ,
            '@sCkiDayPart	VARCHAR	 (3)	OUTPUT ,
            '@sOddometerOut	BIGINT	 		OUTPUT ,
            '@bUpdateFleet	BIT 			OUTPUT

            Extref = oParamArray(1).Value
            RntStatus = oParamArray(2).Value
            CkoWhen = oParamArray(3).Value
            CkiWhen = oParamArray(4).Value
            CkoLocCode = oParamArray(5).Value
            CkiLocCode = oParamArray(6).Value
            DVASSSeqNum = oParamArray(7).Value
            UnitNum = oParamArray(8).Value
            RegoNum = oParamArray(9).Value
            CkoDayPart = oParamArray(10).Value
            CkiDayPart = oParamArray(11).Value
            OdometerOut = oParamArray(12).Value
            UpdateFleet = oParamArray(13).Value


            '    sExtref = .RetOutPutValue("@sExtRef")
            '    sRntStatus = .RetOutPutValue("@sRntStatus")
            '    dCkoWhen = .RetOutPutValue("@sCkoWhen")
            '    dCkiWhen = .RetOutPutValue("@sCkiWhen")
            '    sCkoLocCode = .RetOutPutValue("@sCkoLocCode")
            '    sCkiLocCode = .RetOutPutValue("@sCkiLocCode")
            '    sDVASSSeqNum = .RetOutPutValue("@sDVASSSeqNum")
            '    sUnitNum = .RetOutPutValue("@sUnitNum")
            '    sRegoNum = .RetOutPutValue("@sRegoNum")
            '    sCkoDayPart = .RetOutPutValue("@sCkoDayPart")
            '    sCkiDayPart = .RetOutPutValue("@sCkiDayPart")
            '    sOddometerOut = .RetOutPutValue("@sOddometerOut")
            '    bUpdateFleet = .RetOutPutValue("@bUpdateFleet")
            'End With
            Return String.Empty
        Catch ex As Exception
            Return "ERROR/" & ex.Message
        End Try

    End Function


    Public Shared Function CreateHistory( _
    ByVal BookingId As String, ByVal RentalId As String, ByVal BPDId As String, ByVal RnhId As String, _
    ByVal ChangeEvent As String, ByVal DataType As String, ByVal BphChangedEvent As String, _
    ByVal UserCode As String, ByVal AddProgramName As String) As Boolean

        'objHistoryDAL.appendParameter("@sBooId", 200, 64, sBooId)
        'objHistoryDAL.appendParameter("@sRntId", 200, 64, sRntId)
        'objHistoryDAL.appendParameter("@sBpdId", 200, 64, sBpdId)
        'objHistoryDAL.appendParameter("@rRnhId", 200, 64, rRnhId)
        'objHistoryDAL.appendParameter("@sChangeEvent", 200, 64, sChangeEvent)
        'objHistoryDAL.appendParameter("@sDataType", 200, 64, sDataType)
        'objHistoryDAL.appendParameter("@sBphChangedEvent", 200, 64, sBphChangedEvent)
        'objHistoryDAL.appendParameter("@sUserCode", 200, 64, sUsercode)
        'objHistoryDAL.appendParameter("@sAddPrgmName", 200, 256, sAddPrgmName)
        'sXmlString = objHistoryDAL.updateRecords("RES_checkRentalHistory", "", True)


        '@sBooId				VARCHAR	 (64),
        '@sRntId				VARCHAR	 (64)	=	NULL,
        '@sBpdId				VARCHAR	 (64)	=	NULL,
        '@rRnhId				VARCHAR	 (64)	= 	NULL ,
        '@sChangeEvent		VARCHAR	 (64)	=	NULL,  -- Valid Values are ADD, MODIFY, CONTRACT, CONFIRMATION OR cANCEL
        '@sDataType			VARCHAR	 (64)	=	NULL,  -- RPT(RentalPayment), BOO(booking), RNT(Rental),BPD(Bookedproduct)
        '@sBphChangedEvent	VARCHAR	 (64)	=	NULL,  -- Valid Values are ADD OR MODIFY
        '-- Issue 879: RKS 18-MAY-2006
        '-- START
        '@sVehSapId			VARCHAR(64)	=	NULL,
        '-- END
        '@sUserCode			VARCHAR	 (64),
        '@sAddPrgmName		VARCHAR	 (256)
        'Dim strResult As String
        ' strResult = Aurora.Common.Data.ExecuteSqlXmlSP("RES_checkRentalHistory", BookingId, RentalId, BPDId, RnhId, ChangeEvent, DataType, BphChangedEvent, "", UserCode, AddProgramName)



        '   @sBooId				VARCHAR	 (64),
        '	@sRntId				VARCHAR	 (64)	=	NULL,
        '	@sBpdId				VARCHAR	 (64)	=	NULL,
        '	@rRnhId				VARCHAR	 (64)	= 	NULL ,
        '	@sChangeEvent		VARCHAR	 (64)	=	NULL,  -- Valid Values are ADD, MODIFY, CONTRACT, CONFIRMATION OR cANCEL
        '	@sDataType			VARCHAR	 (64)	=	NULL,  -- RPT(RentalPayment), BOO(booking), RNT(Rental),BPD(Bookedproduct)
        '	@sBphChangedEvent	VARCHAR	 (64)	=	NULL,  -- Valid Values are ADD OR MODIFY
        '	-- Issue 879: RKS 18-MAY-2006
        '	-- START
        '	@sVehSapId			VARCHAR(64)	=	NULL,
        '	-- END
        '	@sUserCode			VARCHAR	 (64),
        '	@sAddPrgmName		VARCHAR	 (256)


        'Dim xmlDoc As XmlDocument
        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_checkRentalHistory", BookingId, RentalId, BPDId, RnhId, ChangeEvent, DataType, BphChangedEvent, UserCode, AddProgramName)
        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_checkRentalHistory", BookingId, RentalId, BPDId, RnhId, ChangeEvent, DataType, BphChangedEvent, "", UserCode, "")
        'Dim xmlDoc As XmlDocument
        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_ssTransToFinance", BookingId, RentalId, "", "")


        Dim oParamArray(9) As Aurora.Common.Data.Parameter
        oParamArray(0) = New Aurora.Common.Data.Parameter("sBooId", System.Data.DbType.String, BookingId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(1) = New Aurora.Common.Data.Parameter("sRntId", System.Data.DbType.String, RentalId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(2) = New Aurora.Common.Data.Parameter("sBpdId", System.Data.DbType.String, BPDId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(3) = New Aurora.Common.Data.Parameter("rRnhId", System.Data.DbType.String, RnhId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(4) = New Aurora.Common.Data.Parameter("sChangeEvent", System.Data.DbType.String, ChangeEvent, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(5) = New Aurora.Common.Data.Parameter("sDataType", System.Data.DbType.String, DataType, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(6) = New Aurora.Common.Data.Parameter("sBphChangedEvent", System.Data.DbType.String, BphChangedEvent, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(7) = New Aurora.Common.Data.Parameter("sVehSapId", System.Data.DbType.String, "", Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(8) = New Aurora.Common.Data.Parameter("sUserCode", System.Data.DbType.String, UserCode, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(9) = New Aurora.Common.Data.Parameter("sAddPrgmName", System.Data.DbType.String, AddProgramName, Common.Data.Parameter.ParameterType.AddInParameter)

        Aurora.Common.Data.ExecuteOutputSP("RES_checkRentalHistory", oParamArray)


        Return True
    End Function

    ''' <summary>
    ''' This is a new version of the Create History Method that is created specifically for the Phoenix webservice item additions
    ''' It provides a means to obtain the newly created/updated rental history id for subsequent edits/additions
    ''' </summary>
    ''' <param name="BookingId">use empty string</param>
    ''' <param name="RentalId">use empty string</param>
    ''' <param name="BPDId">BPDId</param>
    ''' <param name="RnhId">pass in the RnhId returned earlier</param>
    ''' <param name="ChangeEvent">use empty string</param>
    ''' <param name="DataType">use 'BPD'</param>
    ''' <param name="BphChangedEvent">use 'ADD'</param>
    ''' <param name="UserCode">B2CNZ or B2CAU</param>
    ''' <param name="AddProgramName">use 'COM, _ADD'</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateHistoryPhoenix(ByVal BookingId As String, _
                                         ByVal RentalId As String, _
                                         ByVal BPDId As String, _
                                         ByVal RnhId As String, _
                                         ByVal ChangeEvent As String, _
                                         ByVal DataType As String, _
                                         ByVal BphChangedEvent As String, _
                                         ByVal UserCode As String, _
                                         ByVal AddProgramName As String) As String


        Dim oParamArray(10) As Aurora.Common.Data.Parameter
        oParamArray(0) = New Aurora.Common.Data.Parameter("sBooId", System.Data.DbType.String, BookingId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(1) = New Aurora.Common.Data.Parameter("sRntId", System.Data.DbType.String, RentalId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(2) = New Aurora.Common.Data.Parameter("sBpdId", System.Data.DbType.String, BPDId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(3) = New Aurora.Common.Data.Parameter("rRnhId", System.Data.DbType.String, RnhId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(4) = New Aurora.Common.Data.Parameter("sChangeEvent", System.Data.DbType.String, ChangeEvent, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(5) = New Aurora.Common.Data.Parameter("sDataType", System.Data.DbType.String, DataType, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(6) = New Aurora.Common.Data.Parameter("sBphChangedEvent", System.Data.DbType.String, BphChangedEvent, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(7) = New Aurora.Common.Data.Parameter("sVehSapId", System.Data.DbType.String, "", Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(8) = New Aurora.Common.Data.Parameter("sUserCode", System.Data.DbType.String, UserCode, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(9) = New Aurora.Common.Data.Parameter("sAddPrgmName", System.Data.DbType.String, AddProgramName, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(10) = New Aurora.Common.Data.Parameter("sOutPutRnhId", System.Data.DbType.String, 64, Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("RES_checkRentalHistory_Phoenix", oParamArray)


        Return oParamArray(10).Value

    End Function

#End Region


End Class
