Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient

Public Class WebBookingItem

    Private _bookingnumber As String
    Private _customername As String
    Private _status As String
    Private _dateRequest As DateTime
    Private _checkoutdate As DateTime
    Private _checkoutbranch As String
    Private _checkindate As DateTime
    Private _checkinbranch As String
    Private _lengthofhire As Integer
    Private _vehicle As String
    Private _brand As String


    Sub New(ByVal bookingnumber As String)
        If String.IsNullOrEmpty(bookingnumber) Then
            Throw New ArgumentNullException("Booking Number is a required parameter in WebBookingListStatus")
        End If
        _bookingnumber = bookingnumber
    End Sub

    Public ReadOnly Property BookingNumber() As String
        Get
            Return _bookingnumber
        End Get
    End Property

    Public Property CustomerName() As String
        Get
            Return _customername
        End Get
        Set(ByVal value As String)
            _customername = value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    Public Property DateRequest() As DateTime
        Get
            Return _dateRequest
        End Get
        Set(ByVal value As DateTime)
            _dateRequest = value
        End Set
    End Property

    Public Property CheckoutDate() As DateTime
        Get
            Return _checkoutdate
        End Get
        Set(ByVal value As DateTime)
            _checkoutdate = value
        End Set
    End Property

    Public Property CheckoutBranch() As String
        Get
            Return _checkoutbranch
        End Get
        Set(ByVal value As String)
            _checkoutbranch = value
        End Set
    End Property

    Public Property CheckinDate() As String
        Get
            Return _checkindate
        End Get
        Set(ByVal value As String)
            _checkindate = value
        End Set
    End Property

    Public Property CheckinBranch() As String
        Get
            Return _checkinbranch
        End Get
        Set(ByVal value As String)
            _checkinbranch = value
        End Set
    End Property

    Public Property LengthOfHire() As Integer
        Get
            Return _lengthofhire
        End Get
        Set(ByVal value As Integer)
            _lengthofhire = value
        End Set
    End Property

    Public Property Vehicle() As String
        Get
            Return _vehicle
        End Get
        Set(ByVal value As String)
            _vehicle = value
        End Set
    End Property

    Public Property Brand() As String
        Get
            Return _brand
        End Get
        Set(ByVal value As String)
            _brand = value
        End Set
    End Property


    

End Class

Public Class WebBookingItemProvider
    Private Shared _KBcount As Integer
    Public Shared Property KBcount() As String
        Get
            Return _KBcount.ToString
        End Get
        Set(ByVal value As String)
            _KBcount = CInt(value)
        End Set
    End Property

    Private Shared _WLcount As Integer
    Public Shared Property WLcount() As String
        Get
            Return _WLcount.ToString
        End Get
        Set(ByVal value As String)
            _WLcount = CInt(value)
        End Set
    End Property

    ''REV:MIA NOT USED--PREFERRED IS GetbookingitemsDSFiltered
    Public Shared Function GetBookingItems(ByVal parameterDate As String) As List(Of WebBookingItem)
        Dim list As New List(Of WebBookingItem)
        Dim oWebBookingitem As WebBookingItem
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusList", result)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider")
        End Try

        ''  _total = result.Tables(0).Rows.Count
        _KBcount = 0
        _WLcount = 0

        Dim tableRows() As DataRow = result.Tables(0).Select("GROUP = '" & parameterDate & "'")
        Dim rowItem As DataRow
        For Each rowItem In tableRows
            oWebBookingitem = New WebBookingItem(rowItem("bookingnumber"))
            With oWebBookingitem
                .CheckinBranch = rowItem("checkinbranch")
                .CheckinDate = rowItem("checkindate")
                .CheckoutBranch = rowItem("checkoutbranch")
                .CheckoutDate = rowItem("checkoutdate")
                .CustomerName = rowItem("customername")
                .DateRequest = rowItem("dateRequest")
                .Status = rowItem("status")
                .LengthOfHire = rowItem("lengthofhire")
                .Vehicle = rowItem("vehicle")
                If rowItem("status").ToString.Equals("KB") Then
                    _KBcount = _KBcount + 1
                End If
                If rowItem("status").ToString.Equals("WL") Then
                    _WLcount = _WLcount + 1
                End If
                .Brand = rowItem("Brand")
            End With
            list.Add(oWebBookingitem)
        Next
        Return list
    End Function

    Public Shared Function GetBookingListByDate() As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingListByDate", result)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider{GetBookingListByDate}")
        End Try
        Return result
    End Function

    Public Shared Function GetActiveVehicles(ByVal PrdBrdCode As String, ByVal Citycode As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetActiveVehicles", result, PrdBrdCode, Citycode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider{GetActiveVehicles}")
        End Try
        Return result
    End Function

    ''REV:MIA NOT USED--PREFERRED IS GetbookingitemsDSFiltered
    Public Shared Function GetBookingItemsDS(ByVal parameterDate As String) As DataView

        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusList", result)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider")
        End Try

        '' _total = result.Tables(0).Rows.Count
        _KBcount = 0
        _WLcount = 0

        Dim tableRows() As DataRow = result.Tables(0).Select("GROUP = '" & parameterDate & "'")
        Dim rowItem As DataRow
        For Each rowItem In tableRows
            If rowItem("status").ToString.Equals("KB") Then
                _KBcount = _KBcount + 1
            End If
            If rowItem("status").ToString.Equals("WL") Then
                _WLcount = _WLcount + 1
            End If
        Next
        Dim dview As DataView = result.Tables(0).DefaultView
        dview.Sort = "bookingnumber"
        dview.RowFilter = "GROUP = '" & parameterDate & "'"
        Return dview
    End Function

    ''rev:mia dec 3 ,2009 addition of usercode
    Public Shared GetBookingItemsDSFilteredForPending As DataSet


    Public Shared Function GetBookingItemsDSFiltered(ByVal parameterDate As String, _
                                                     ByVal Brand As String, _
                                                     ByVal Vehicle As String, _
                                                     ByVal Status As String, _
                                                     ByVal ActionFilter As String, _
                                                     ByVal citycode As String, _
                                                     Optional ByVal usercode As String = "") As DataView

        Dim result As New DataSet
        Dim qry As String = "GROUP = '" & parameterDate & "'"

        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusListFiltered", result, Brand, Vehicle, Status, ActionFilter, citycode, usercode)
            ''
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider")
        End Try


        _KBcount = 0
        _WLcount = 0



        Dim tableRows() As DataRow = result.Tables(0).Select(qry)
        Dim rowItem As DataRow
        For Each rowItem In tableRows
            Dim drowCompleted() As DataRow = result.Tables(1).Select("RentalID = '" & rowItem("RntId") & "'")
            If drowCompleted.Length = 0 Then
                If rowItem("status").ToString.Equals("KB") Then
                    _KBcount = _KBcount + 1
                End If
                If rowItem("status").ToString.Equals("WL") Then
                    _WLcount = _WLcount + 1
                End If
            End If
        Next
        Dim dview As DataView = result.Tables(0).DefaultView
        dview.Sort = "DateRequest Desc"
        dview.RowFilter = qry
        Return dview

    End Function

    Public Shared Function B2CpageRefreshInterval() As Integer
        Try
            Return Aurora.Common.Data.ExecuteScalarSP("B2CpageRefreshInterval")
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function B2CBookingActivityActions( _
                    ByVal RentalID As String, _
                    ByVal Action As String, _
                    ByVal userId As String, _
                    ByVal TSconcurrency As String, _
                    Optional ByVal NotesMessage As String = "") As String

        Dim params(4) As Aurora.Common.Data.Parameter
        Dim output As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("RentalID", DbType.String, RentalID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("Action", DbType.String, Action, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("userId", DbType.String, userId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("output", DbType.String, 100, Common.Data.Parameter.ParameterType.AddOutParameter)
            params(4) = New Aurora.Common.Data.Parameter("TSconcurrency", DbType.String, TSconcurrency, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("WEB_B2CBookingActivityActions", params)
            output = params(3).Value
            If (output.ToUpper.Contains("ERROR")) Then Throw New Exception
            If (output.ToUpper.Contains("ErrorConcurrency".ToUpper)) Then Throw New Exception
        Catch ex As Exception
        End Try
        Return output
    End Function

    Public Shared Function GetB2CBookingActivityActions( _
                        ByVal RentalID As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_getBookingActions", result, RentalID)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get Booking Actions from WebBookingItemProvider")
        End Try
        Return result
    End Function

    ''rev:mia dec 3 ,2009 addition of usercode
    Public Shared Function GetBookingStatusTotalOutstanding(Optional ByVal usercode As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusTotalOutstanding", result, usercode)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(0).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(0).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(0).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(0).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get Total Booking Outstanding for KB and WL from WebBookingItemProvider")
        End Try
        ''Return tempstr
        ''Return result.Tables(0).Rows(0)(0).ToString & " / " & result.Tables(0).Rows(1)(0).ToString
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function

    ''rev:mia dec 3 ,2009 addition of usercode
    Public Shared Function GetBookingStatusTotalPending(Optional ByVal usercode As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusTotalPending", result, usercode)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(0).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(0).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(0).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(0).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get Pending Booking for KB and WL from WebBookingItemProvider")
        End Try
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function

    ''rev:mia dec 3 ,2009 addition of usercode
    Public Shared Function GetBookingStatusTotalInProgress(Optional ByVal usercode As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingStatusTotalInProgress", result, usercode)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(0).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(0).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(0).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(0).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get InProgress Booking for KB and WL from WebBookingItemProvider")
        End Try

        ''Return tempstr
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function


#Region "ADDITION FOR QN BOOKING"
    Private _agentId As String
    Public Property AgentID As String
        Get
            Return _agentId
        End Get
        Set(ByVal value As String)
            _agentId = value
        End Set
    End Property

    Private Shared Property CachedAllData As Object
        Get
            Return System.Web.HttpContext.Current.Session("CachedAllData")
        End Get
        Set(ByVal value As Object)
            System.Web.HttpContext.Current.Session("CachedAllData") = value
        End Set
    End Property
    Private Shared Property Cacheddirection As String
        Get
            Return System.Web.HttpContext.Current.Session("Cacheddirection")
        End Get
        Set(ByVal value As String)
            System.Web.HttpContext.Current.Session("Cacheddirection") = value
        End Set
    End Property

    Public Shared Function GetQNBookingItemsDSFiltered(ByVal parameterDate As String, _
                                                    ByVal Brand As String, _
                                                    ByVal Vehicle As String, _
                                                    ByVal agentId As String, _
                                                    ByVal ActionFilter As String, _
                                                    ByVal citycode As String, _
                                                    Optional ByVal usercode As String = "", _
                                                    Optional ByVal sorting As String = ""
                                                    ) As DataView


        
        Dim result As New DataSet
        Dim qry As String = "GROUP = '" & parameterDate & "'"

        Dim spname As String = "WEB_GetQNBookingStatusListFilteredV1"
        Try
            Aurora.Common.Data.ExecuteDataSetSP(spname, result, Brand, Vehicle, agentId, ActionFilter, citycode, usercode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider")
        End Try

        Dim dview As DataView = result.Tables(0).DefaultView
        dview.RowFilter = qry


        
        Try
            
            If Not String.IsNullOrEmpty(sorting) Then
                ''System.Diagnostics.Debug.WriteLine("sorted by " & sorting)
                dview.Sort = ""
                dview.Sort = sorting
            End If

        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider{GetQNBookingItemsDSFiltered}")
        End Try

        Return dview
    End Function

    Private Shared Function GetQNBookingItemsDSFilteredV1(ByVal parameterDate As String, _
                                                   ByVal Brand As String, _
                                                   ByVal Vehicle As String, _
                                                   ByVal agentId As String, _
                                                   ByVal ActionFilter As String, _
                                                   ByVal citycode As String, _
                                                   Optional ByVal usercode As String = "", _
                                                   Optional ByVal sorting As String = ""
                                                   ) As DataView

        Dim result As New DataSet
        Dim qry As String = "GROUP = '" & parameterDate & "'"
        Dim timestart As Date = Now.ToLongTimeString

        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetQNBookingStatusListFiltered", result, Brand, Vehicle, agentId, ActionFilter, citycode, usercode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider")
        End Try

        Dim isAll As Boolean = False
        Dim tempBrandName As String = ""
        Dim tempRentId As String = ""

        Dim dview As DataView = result.Tables(0).DefaultView
        dview.RowFilter = qry

        Dim quotesorting As String = "RntId DESC, AddDateTime DESC"
        Dim qouteSortingFirstQN As String = "AddDateTime ASC, RntId ASC"
        Dim qouteSortingLastQN As String = "AddDateTime DESC, RntId DESC"

        If ActionFilter = "FirstQuote" Then
            dview.Sort = qouteSortingFirstQN
        ElseIf ActionFilter = "LatestQuote" Then
            dview.Sort = qouteSortingLastQN
        ElseIf String.IsNullOrEmpty(ActionFilter) Then
            isAll = True
            dview.Sort = qouteSortingFirstQN
        End If




        System.Diagnostics.Debug.WriteLine(dview.Count)
        'Dim dtable As DataTable = dview.ToTable(True, "BookingNumber", "RntId", "RentalNo", "Customername", "Status", "DateRequest", "DateExpiry", "Checkoutdate", "Checkindate", _
        '                                        "Checkoutbranch", "Checkinbranch", "Lengthofhire", "Brand", "GROUP", "Vehicle", "Action", "User", "TScurrency", "AddDateTime")

        'Dim dtable As DataTable = dview.ToTable(True, "BookingNumber")
        'Dim bookingnums As New Text.StringBuilder

        'For Each drow As DataRow In dtable.Rows
        '    System.Diagnostics.Debug.WriteLine(drow.Item("BookingNumber"))
        '    bookingnums.AppendFormat("'{0}',", drow.Item("BookingNumber"))
        'Next


        'If isAll Then
        'Else

        '    If ActionFilter = "FirstQuote" Or ActionFilter = "LatestQuote" Then
        '        dview.RowFilter = qry + " AND BookingNumber IN(" & bookingnums.ToString & ")"
        '    End If
        'End If


        Dim sb As New Text.StringBuilder
        Dim tempStart As Integer = 0
        Dim tempRental As String = ""
        Dim tempRentalCheck As String = ""
        Try
            If dview.Count > 0 Then
                For Each dviewrow As DataRowView In dview

                    If isAll Then

                        If Not String.IsNullOrEmpty(tempRentalCheck) Then
                            If tempRentalCheck.IndexOf(dviewrow.Row("RntId").ToString.Split("-")(0)) <> -1 Then
                                Continue For
                            End If
                        End If

                        If (tempRentId <> dviewrow.Row("RntId").ToString.Split("-")(0)) Then
                            dview.RowFilter = qry + " AND RntId LIKE '" & dviewrow.Row("RntId").ToString.Split("-")(0) & "%'"

                            If dview.Count - 1 > 0 Then
                                For Each childview As DataRowView In dview
                                    sb.Append("'" & childview.Row("RntId").ToString & "',")
                                Next
                            Else
                                sb.Append("'" & dviewrow.Row("RntId").ToString & "',")
                            End If

                            tempRentId = dviewrow.Row("RntId").ToString.Split("-")(0)
                            tempBrandName = dviewrow.Row("Brand").ToString
                        Else

                            If (tempBrandName <> dviewrow.Row("Brand").ToString) Then
                                sb.Append("'" & dviewrow.Row("RntId").ToString & "',")
                            End If

                            tempRentId = dviewrow.Row("RntId").ToString.Split("-")(0)
                            tempBrandName = dviewrow.Row("Brand").ToString

                        End If

                        tempRentalCheck = String.Concat(tempRentalCheck, ",", tempRentId)

                    Else

                        ''filtering happen here

                        If Not String.IsNullOrEmpty(tempRentalCheck) Then
                            If tempRentalCheck.IndexOf(dviewrow.Row("RntId").ToString.Split("-")(0)) <> -1 Then
                                Continue For
                            End If
                        End If

                        If tempRentId <> dviewrow.Row("RntId").ToString.Split("-")(0) Then
                            dview.RowFilter = qry + " AND RntId LIKE '" & dviewrow.Row("RntId").ToString.Split("-")(0) & "%'"

                            If dview.Count - 1 > 0 Then


                                tempStart = dviewrow.Row("RntId").ToString.Split("-")(1)
                                tempRental = dviewrow.Row("RntId").ToString
                                ''System.Diagnostics.Debug.WriteLine("------------------")
                                For Each dviewrowTest As DataRowView In dview
                                    '' System.Diagnostics.Debug.WriteLine(dviewrowTest.Row("RntId"))
                                    If ActionFilter = "FirstQuote" Then
                                        If tempStart > dviewrowTest.Row("RentalNo").ToString Then
                                            tempStart = CInt(dviewrowTest.Row("RentalNo").ToString)
                                            tempRental = dviewrowTest.Row("RntId").ToString
                                        Else
                                            If tempStart > CInt(dviewrowTest.Row("RentalNo").ToString) Then
                                                tempStart = CInt(dviewrowTest.Row("RentalNo").ToString)
                                                tempRental = dviewrowTest.Row("RntId").ToString
                                            End If
                                        End If
                                    ElseIf ActionFilter = "LatestQuote" Then
                                        If tempStart < dviewrowTest.Row("RentalNo").ToString Then
                                            tempStart = CInt(dviewrowTest.Row("RentalNo").ToString)
                                            tempRental = dviewrowTest.Row("RntId").ToString
                                        Else
                                            If tempStart < CInt(dviewrowTest.Row("RentalNo").ToString) Then
                                                tempStart = CInt(dviewrowTest.Row("RentalNo").ToString)
                                                tempRental = dviewrowTest.Row("RntId").ToString
                                            End If
                                        End If
                                    End If

                                Next


                                sb.Append("'" & tempRental & "',")
                                ''System.Diagnostics.Debug.WriteLine("GET: " & tempRental)
                                ''System.Diagnostics.Debug.WriteLine("------------------")
                            Else
                                sb.Append("'" & dviewrow.Row("RntId").ToString & "',")
                            End If

                            tempRentId = dviewrow.Row("RntId").ToString.Split("-")(0)
                        Else
                            tempRentId = dviewrow.Row("RntId").ToString.Split("-")(0)
                        End If


                        tempRentalCheck = String.Concat(tempRentalCheck, ",", tempRentId)
                    End If ''If isAll Then
                Next


                Dim indx As Integer = sb.ToString.LastIndexOf(",")
                Dim temp As String = sb.ToString
                If sb.ToString.EndsWith(",") Then
                    temp = temp.Remove(indx)
                End If

                ''ORIG
                'If ActionFilter = "FirstQuote" Then
                '    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                '    dview.Sort = "AddDateTime ASC, Brand ASC"
                'ElseIf ActionFilter = "LatestQuote" Then

                '    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                '    dview.Sort = "AddDateTime ASC, Brand ASC"
                'ElseIf String.IsNullOrEmpty(ActionFilter) Then
                '    dview.Sort = "AddDateTime ASC, Brand ASC"
                '    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                'End If

                If ActionFilter = "FirstQuote" Then
                    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                    dview.Sort = qouteSortingFirstQN
                ElseIf ActionFilter = "LatestQuote" Then
                    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                    dview.Sort = qouteSortingLastQN
                ElseIf String.IsNullOrEmpty(ActionFilter) Then
                    dview.Sort = qouteSortingFirstQN
                    dview.RowFilter = qry + " AND RntId IN(" & temp & ")"
                End If


                If Not String.IsNullOrEmpty(sorting) Then
                    System.Diagnostics.Debug.WriteLine("sorted by " & sorting)
                    dview.Sort = ""
                    dview.Sort = sorting
                End If

            End If
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message & " " & ex.StackTrace)
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider{GetQNBookingItemsDSFiltered}")
        End Try

        Dim timeEnd As Date = Now.ToLongTimeString
        Return dview
    End Function

    Public Shared Function GetQNBookingListByDate(Optional ByVal agentid As String = "") As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetQNBookingListByDate", result, agentid)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List from WebBookingItemProvider{GetQNBookingListByDate}")
        End Try
        Return result
    End Function

    Public Shared Function B2CQNBookingActivityActions( _
                    ByVal RentalID As String, _
                    ByVal Action As String, _
                    ByVal userId As String, _
                    ByVal TSconcurrency As String, _
                    Optional ByVal NotesMessage As String = "") As String

        Dim params(5) As Aurora.Common.Data.Parameter
        Dim output As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("RentalID", DbType.String, RentalID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("Action", DbType.String, Action, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("userId", DbType.String, userId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("output", DbType.String, 100, Common.Data.Parameter.ParameterType.AddOutParameter)
            params(4) = New Aurora.Common.Data.Parameter("TSconcurrency", DbType.String, TSconcurrency, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("isQN", DbType.String, 1, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("WEB_B2CQNBookingActivityActions", params)
            output = params(3).Value
            If (output.ToUpper.Contains("ERROR")) Then Throw New Exception
            If (output.ToUpper.Contains("ErrorConcurrency".ToUpper)) Then Throw New Exception
        Catch ex As Exception
        End Try
        Return output
    End Function

    Public Shared Function GetBookingQNStatusTotalOutstanding(Optional ByVal usercode As String = "", _
                                                          Optional ByVal agentId As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingQNStatusTotalOutstandingV1", result, usercode, agentId)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(1).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(1).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(1).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(1).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get Total Booking Outstanding for QN from WebBookingItemProvider")
        End Try
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function

    Public Shared Function GetBookingQNStatusTotalInProgress(Optional ByVal usercode As String = "", _
                                                          Optional ByVal agentId As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingQNStatusTotalInProgressV1", result, usercode, agentId)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(1).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(1).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(1).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(1).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get InProgress Booking for QN from WebBookingItemProvider")
        End Try

        ''Return tempstr
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function

    Public Shared Function GetBookingQNStatusTotalPending(Optional ByVal usercode As String = "", _
                                                          Optional ByVal agentId As String = "") As String
        Dim result As New DataSet
        Dim tempstr As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("WEB_GetBookingQNStatusTotalPendingV1", result, usercode, agentId)
            If result.Tables(0).Rows.Count = 2 Then
                If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                    tempstr = result.Tables(0).Rows(0)(1).ToString
                End If

                If Not TypeOf result.Tables(0).Rows(1)(0) Is DBNull Then
                    tempstr = tempstr & " / " & result.Tables(0).Rows(1)(1).ToString
                End If
            Else
                If result.Tables(0).Rows.Count = 1 Then
                    If Not TypeOf result.Tables(0).Rows(0)(0) Is DBNull Then
                        If result.Tables(0).Rows(0)(0).ToString.Equals("NZ") Then
                            tempstr = "0 / " & result.Tables(0).Rows(0)(1).ToString
                        Else
                            tempstr = result.Tables(0).Rows(0)(1).ToString & " / 0"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get Pending Booking for QN from WebBookingItemProvider")
        End Try
        Return IIf(String.IsNullOrEmpty(tempstr), "0", tempstr)
    End Function
#End Region
    
End Class