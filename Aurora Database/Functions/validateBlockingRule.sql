set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go








/*
	Copyright (C) Tourism Holdings Limited as an unpublished work. All rights reserved.

	USER FUNCTION:		validateBlockingRule
				====================

	This user function validates an individual blocking rule.

	The function is passed:
			@BlrId		AS VARCHAR(64),
			@AgnId		VARCHAR(64),
			@AgnCtyCode	VARCHAR(64),
			@CkoDate	DATETIME,
			@CkiDate	DATETIME,
			@HirePeriod	BIGINT,
			@CkoLocation	VARCHAR(64),
			@CkiLocation	VARCHAR(64),
			@BookedDate	DATETIME,
			@PrdId		VARCHAR(64),
			@PkgId		VARCHAR(64),
			@VhrId		VARCHAR(64),
			@ReturnError	VARCHAR(500),
			@ReturnStatus	VARCHAR(500)

	The function returns:
		VARCHAR(500)	Either '' (empty string) if not valid
				Or the BlrId wrapped in XML

	Notes:
*/
ALTER	FUNCTION [dbo].[validateBlockingRule]
			(
			@BlrId		VARCHAR(64),
			@AgnId		VARCHAR(64),
			@AgnCtyCode	VARCHAR(64),
			@CkoDate	DATETIME,
			@CkiDate	DATETIME,
			@lHireDuration	BIGINT,
			@CkoLocation	VARCHAR(64),
			@CkiLocation	VARCHAR(64),
			@BookedDate	DATETIME,
			@PrdId		VARCHAR(64),
			@PkgId		VARCHAR(64),
			@VhrId		VARCHAR(64)		
			)

 	RETURNS VARCHAR(4000)

AS

BEGIN 
--	LOCAL VARIABLES
DECLARE
	@lHireBookAheadPeriod	INTEGER,
	@BlrDurationOfRental		INTEGER,
	@BlrBookAheadPeriod	INTEGER,
	@BlrApplyToDept	VARCHAR(8),
	@BlrDateFrom		DATETIME,
	@BlrDateTo		DATETIME,
	@bIncludeThisRule	BIT,
	@bAllowingRule		BIT,
	@sAgentCategoryID	VARCHAR(64),
	@sBrandCode		VARCHAR(64),
	@BlrBypass		BIT,
	@BlrAllow		BIT,
	@BlrReason		varchar(500)

--	Get All Blocking Rule Values
SELECT
	@BlrDateFrom			= BlrDateFrom,
	@BlrDateTo				= BlrDateTo,
	@BlrDurationOfRental	= BlrDurationOfRental,
	@BlrBookAheadPeriod		= BlrBookAheadPeriod,
	@BlrApplyToDept			= BlrApplyToDept,
	@bAllowingRule			= BlrAllow,
	@BlrReason				= BlrReason,
	@BlrBypass				= BlrBypass
			
FROM
	BlockingRule WITH (NOLOCK)
WHERE
	BlrId = @BlrId

--	2b. The First of these test is duration of rental.
IF	ISNULL(@BlrDurationOfRental, 0) <> 0
BEGIN
	--	DURATION of hire is [hire period] at execution?
	--	Notes:	The user is able to describe the same
	--		test condition in one of two ways that
	--		are equivalent, but we need to capture
	--		either.
	--		1: BLOCK hire duration > #days
	--		2: ALLOW hire duration < #days
	--	ELIMINATE if:
	--	* [Blocking rule] and [hire duration] > [rule duration]
	IF	((@bAllowingRule = 0)
		AND	(@lHireDuration >= @BlrDurationOfRental)
		)
	--	* [Allowing rule] and [hire duration] < [rule duration]
	OR	((@bAllowingRule = 1)
		AND	(@lHireDuration < @BlrDurationOfRental)
		)
	BEGIN
		RETURN '' 
	END
END -- IF	ISNULL(@BlrDurationOfRental, 0) <> 0)

--	Test For Book-Ahead criteria
IF	ISNULL(@BlrBookAheadPeriod, 0) <> 0
BEGIN
	--	BOOK-AHEAD days is?
	SELECT	@lHireBookAheadPeriod = DATEDIFF(Day, @BookedDate, @CkoDate)
	--	Notes:	The user is able to describe the same
	--		test condition in one of two ways that
	--		are equivalent, but we need to capture
	--		either.
	--		1: BLOCK book ahead > #days
	--		2: ALLOW book ahead < #days
	--	ELIMINATE if:
	--	* [Blocking rule] and [hire book ahead] > [rule book-ahead]
	IF	(	(@bAllowingRule = 0)
		AND	(@lHireBookAheadPeriod > @BlrBookAheadPeriod)
		)
	--	* [Allowing rule] and [hire book ahead] < [rule book-ahead]
	OR	(	(@bAllowingRule = 1)
		AND	(@lHireBookAheadPeriod < @BlrBookAheadPeriod)
		)
	BEGIN
		RETURN ''
	END
END -- IF ISNULL(@BlrBookAheadPeriod, 0) <> 0

--	2.b.3	Apply to department:
IF (LTRIM(RTRIM(UPPER(@BlrApplyToDept))) = 'OUT')
BEGIN
	--	IF check-out falls outside the range
	IF	NOT (@CkoDate BETWEEN @BlrDateFrom AND @BlrDateTo)
		RETURN ''
END 
ELSE IF (LTRIM(RTRIM(UPPER(@BlrApplyToDept))) = 'IN')
BEGIN
	--	IF check-in falls outside the range
	IF	NOT (@CkiDate BETWEEN @BlrDateFrom AND @BlrDateTo)
		RETURN ''
END 
ELSE
BEGIN
	--	IF some part of the hire falls outside the range
	IF NOT((	@CkoDate BETWEEN @BlrDateFrom AND @BlrDateTo OR 
			@CkiDate BETWEEN @BlrDateFrom AND @BlrDateTo)
	   OR (	@BlrDateFrom BETWEEN @CkoDate AND @CkiDate  OR
			@BlrDateTo BETWEEN @CkoDate AND @CkiDate )) 
		RETURN ''
END

-- This value is for 2C6.

SELECT	@sAgentCategoryID = AgnCodBlkCatId  FROM	Agent WITH (NOLOCK)	WHERE	AgnId = @AgnID 

-- This value is for 2C7.
SELECT	@sBrandCode = PrdBrdCode FROM	Product WITH (NOLOCK) WHERE	PrdId = @PrdId 

IF EXISTS(SELECT BlpBlrId FROM BlockingRuleProduct WITH(NOLOCK) WHERE BlpBlrId = @BlrID  AND BlpPrdId = @PrdID) AND 
	dbo.RES_checkBlockingRuleProduct(@BlrID, @PrdID) = 0
RETURN ''

IF dbo.RES_checkBlockingRuleProduct(@BlrID, @PrdID) = 0 AND
	EXISTS(SELECT BlbBlrId FROM BlockingRuleBrand WITH (NOLOCK) 
			WHERE BlbBlrId = @BlrID  AND	BlbBrdCode = @sBrandCode ) 
	AND dbo.RES_checkBlockingRuleBrand(@BlrID, @sBrandCode) = 0
RETURN ''

IF EXISTS(SELECT BraBlrId FROM BlockingRuleAgent WITH (NOLOCK) WHERE BraBlrId = @BlrID AND BraAgnId = @AgnID ) AND
	dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 0
RETURN ''

IF dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 0 AND 
	EXISTS(SELECT BacBlrId FROM BlockingRuleAgentCategory WITH(NOLOCK) WHERE BacBlrId = @BlrID  AND BacCodAgentCtgyId = @sAgentCategoryID) 	AND
	dbo.RES_checkBlockingRuleAgentCategory(@BlrID, @sAgentCategoryID) = 0
RETURN ''

IF dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 0 AND 
	EXISTS(SELECT BrcBlrId FROM BlockingRuleAgentCountry WITH(NOLOCK) WHERE BrcBlrId = @BlrID  AND BrcCtyCode = @AgnCtyCode ) AND
	dbo.RES_checkBlockingRuleAgentCountry(@BlrID, @AgnCtyCode) = 0
RETURN ''

IF ISNULL(@Pkgid,'') <> '' AND
	EXISTS(SELECT BrpBlrId FROM BlockingRulePackage WITH(NOLOCK) WHERE BrpBlrId = @BlrID AND BrpPkgId = @PkgId) AND
	dbo.RES_checkBlockingRulePackage(@BlrID, @PkgId) = 0
RETURN ''



--	2C. Rules may also be eliminated after checkoing the passed parameters againest the Blocking Rule related Tables
	--	2C1. The First of these is to check the product Sent aginest the BlockingRuleProduct
IF 		(NOT EXISTS(SELECT BlpBlrId FROM BlockingRuleProduct WITH(NOLOCK) WHERE BlpBlrId = @BlrID /* AND BlpPrdId = @PrdID */ ) OR
		 dbo.RES_checkBlockingRuleProduct(@BlrID, @PrdID) = 1)
	--	2C2. Test the Check Out locations aginest the BlockingRuleLocation Table
	AND	(NOT EXISTS(SELECT BrlBlrId FROM BlockingRuleLocation WITH(NOLOCK) WHERE BrlBlrId = @BlrID AND BrlDirection = 'From'  ) OR
		 dbo.RES_checkBlockingRuleLocation(@BlrID, @CkoLocation, 'From') = 1)
	--	2C3. Test the Check In locations aginest the BlockingRuleLocation Table
	AND	(NOT EXISTS(SELECT BrlBlrId FROM BlockingRuleLocation WITH(NOLOCK) WHERE BrlBlrId = @BlrID AND BrlDirection = 'To'  ) OR
		 dbo.RES_checkBlockingRuleLocation(@BlrID, @CkiLocation, 'To') = 1) 
	AND /*--	2C4. Test the Agent Aginest the BlockingRuleAgent Table 
		((NOT EXISTS(SELECT BraBlrId FROM BlockingRuleAgent WITH(NOLOCK) WHERE BraBlrId = @BlrID /* AND BraAgnId = @AgnID */ ) OR 
		 dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 1) 
		--	2C6. Test the AgentCategory Aginest the BlockingRuleAgentCategory Table OR*/ 
		(NOT EXISTS(SELECT BacBlrId FROM BlockingRuleAgentCategory WITH(NOLOCK) WHERE BacBlrId = @BlrID /* AND BacCodAgentCtgyId = @sAgentCategoryID */ ) OR 
		 dbo.RES_checkBlockingRuleAgentCategory(@BlrID, @sAgentCategoryID) = 1) 

	AND
	--	2C5. Test the AgentCountry Aginest the BlockingRuleAgentCountry Table
		(NOT EXISTS(SELECT BrcBlrId FROM BlockingRuleAgentCountry WITH(NOLOCK) WHERE BrcBlrId = @BlrID /* AND BrcCtyCode = @AgnCtyCode */ ) OR 
		 dbo.RES_checkBlockingRuleAgentCountry(@BlrID, @AgnCtyCode) = 1) 
		/*OR
		(NOT EXISTS(SELECT BraBlrId FROM BlockingRuleAgent WITH(NOLOCK) WHERE BraBlrId = @BlrID /* AND BraAgnId = @AgnID */ ) OR 
		 dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 1) ) */

	--	2C7. Test the Product Brand Aginest the BlockingRuleBrand Table
	AND	(NOT EXISTS(SELECT BlbBlrId FROM BlockingRuleBrand WITH(NOLOCK) WHERE BlbBlrId = @BlrID /* AND	BlbBrdCode = @sBrandCode */ ) OR 
		 dbo.RES_checkBlockingRuleBrand(@BlrID, @sBrandCode) = 1)
	--	2C8. Test the Package Aginest the BlockingRulePackage Table
	AND	(NOT EXISTS(SELECT BrpBlrId FROM BlockingRulePackage WITH(NOLOCK) WHERE BrpBlrId = @BlrID /* AND BrpPkgId = @PkgId */ ) OR 
		 dbo.RES_checkBlockingRulePackage(@BlrID, @PkgId) = 1) 
	AND (NOT EXISTS(SELECT BraBlrId FROM BlockingRuleAgent WITH(NOLOCK) WHERE BraBlrId = @BlrID /* AND BraAgnId = @AgnID */ ) OR 
		 dbo.RES_checkBlockingRuleAgent(@BlrID, @AgnID) = 1 )
BEGIN
	RETURN	@BlrId
END
RETURN ''
END
--	**************************************** End validateBlockingAndAllowingRule() ****************************************




