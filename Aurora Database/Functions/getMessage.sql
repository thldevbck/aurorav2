
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

Alter Function [dbo].[getMessage] 
(
	@smsCode	varchar(255) = '',
	@param1	varchar(255) = '',
	@param2	varchar(255) = '',
	@param3	varchar(255) = '',
	@param4	varchar(255) = '',
	@param5	varchar(255) = '',
	@param6	varchar(255) = ''
)
Returns varchar(255)
AS
BEGIN

	Declare @errorMessage varchar(255)

	set @errorMessage = ''

	select	@errorMessage = SmsMsgText 
	from	Messages 
	where	smsCode = @smsCode

	set @errorMessage = REPLACE(@errorMessage, 'XXX1', @param1)
	set @errorMessage = REPLACE(@errorMessage, 'XXX2', @param2)
	set @errorMessage = REPLACE(@errorMessage, 'XXX3', @param3)
	set @errorMessage = REPLACE(@errorMessage, 'XXX4', @param4)
	set @errorMessage = REPLACE(@errorMessage, 'XXX5', @param5)
	set @errorMessage = REPLACE(@errorMessage, 'XXX6', @param6)

	return @errorMessage

END






