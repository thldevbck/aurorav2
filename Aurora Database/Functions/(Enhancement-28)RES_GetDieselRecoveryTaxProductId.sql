--------------------------------------------------------------------  
--------------------------------------------------------------------  
-- RES_GetDieselRecoveryTaxProductId  
--  
-- Required for Enhancement #28 : DRT fee automation at Check-in  
--  
-- Initial Version : 22.7.8  
-- 22.7.8 : Shoel : Initial Version  
-- 24.11.8: Shoel : Now needs rental-id to determine brand :( 
--  
--------------------------------------------------------------------  
--------------------------------------------------------------------  
CREATE FUNCTION RES_GetDieselRecoveryTaxProductId(
@RentalId AS VARCHAR(64)
) --'3197261-'    
RETURNS VARCHAR(64)   
AS      
BEGIN      
   
 DECLARE @l_ProductId AS VARCHAR(64)  ,
@l_RentalPackageBrandCode AS VARCHAR(64)

 SELECT @l_RentalPackageBrandCode = PkgBrdCode 
 FROM Rental WITH (NOLOCK)
 INNER JOIN 
 Package WITH (NOLOCK)
 ON 
 Rental.RntPkgId = Package.PkgId
 WHERE
 Rental.RntId = @RentalId


 SELECT   
  @l_ProductId = PrdId
 FROM   
  Product  
 WITH  
  (NOLOCK)  
 WHERE   
  PrdShortName LIKE 'DTR'   
  AND  
  PrdIsActive = 1  
  AND   
  PrdIsVehicle = 0  
  AND
  (
    PrdBrdCode = @l_RentalPackageBrandCode 
    OR
    (PrdBrdCode = 'G' AND ( (@l_RentalPackageBrandCode = 'B') OR (@l_RentalPackageBrandCode = 'M') OR (@l_RentalPackageBrandCode = 'P')  ))
  )


  
 RETURN @l_ProductId  
  
END  



