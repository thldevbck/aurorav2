set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go





CREATE PROCEDURE [dbo].[Flex2_GetLocations] 
	@ComCode varchar(64),         
	@CtyCode AS varchar(64)
AS

/*
DECLARE @ComCode AS varchar(64)
DECLARE @CtyCode AS varchar(64)

SET @ComCode = 'THL'
SET @CtyCode = 'AU'
*/

SELECT 
	Location.LocComCode	AS ComCode,
	TownCity.TctCtyCode AS CtyCode,
	Location.LocCode,
	Location.LocName
FROM 
	Location
	INNER JOIN Code 
		ON Code.CodId = Location.LocCodTypId
		AND Code.CodCode = 'BRANCH'
	INNER JOIN TownCity 
		ON TownCity.TctCode = Location.LocTctCode 
WHERE
	Location.LocComCode = @ComCode
	AND Location.LocIsActive = 1
	AND TownCity.TctCtyCode = @CtyCode
ORDER BY 	
	Location.LocCode




