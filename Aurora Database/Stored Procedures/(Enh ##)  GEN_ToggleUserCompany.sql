CREATE PROCEDURE GEN_ToggleUserCompany 
	@UserCode AS VARCHAR(24)
AS

BEGIN
	SET NOCOUNT ON

	DECLARE @l_UserId AS VARCHAR(64)
	DECLARE @l_CurrentCompany VARCHAR(64)
	DECLARE @l_AltLocationCode AS VARCHAR(12)
	DECLARE @l_CurrLocationCode AS VARCHAR(12)

	SELECT     
		@l_CurrentCompany = UserCompany.ComCode,
		@l_UserId = UserCompany.UsrId,
		@l_CurrLocationCode = UserInfo.UsrLocCode,
		@l_AltLocationCode = UserInfo.UsrAltLocCode
	FROM         
		UserCompany WITH (NOLOCK) 
		INNER JOIN
		UserInfo WITH (NOLOCK) 
		ON 
		UserCompany.UsrId = UserInfo.UsrId
		INNER JOIN
		UserRole WITH (NOLOCK) 
		ON 
		UserInfo.UsrId = UserRole.UsrId
		INNER JOIN
		Role WITH (NOLOCK) 
		ON 
		UserRole.RolId = Role.RolId
	WHERE     
		UserInfo.UsrCode = @UserCode
		AND
		UserRole.RolId = Role.RolId
		AND
		Role.RolCode = 'TOGGLELOCN'

	IF (@l_AltLocationCode IS NULL) OR (LTRIM(RTRIM(@l_AltLocationCode))='')
	BEGIN
		SELECT 'ERROR/This user does not have an alternate location defined'
		GOTO Error
	END	

    --Role Permission Check
	IF NOT EXISTS (SELECT 1 FROM UserRole WITH (NOLOCK) INNER JOIN Role WITH (NOLOCK) ON UserRole.RolId = Role.RolId WHERE UserRole.UsrId = @l_UserId AND Role.RolCode = 'TOGGLELOCN')
	BEGIN
		SELECT 'ERROR/This user does not have permission to toggle locations'
		GOTO Error
	END

	print @l_CurrentCompany
	print @l_UserId
	print @l_CurrLocationCode
	print @l_AltLocationCode

	-- UPDATE COMPANY
	IF (LTRIM(RTRIM(@l_CurrentCompany)) = 'KXS' )
		BEGIN
			UPDATE UserCompany
			SET ComCode = 'THL',
			IntegrityNo = IntegrityNo + 1
			WHERE UsrId = @l_UserId
		END
	ELSE 
		BEGIN
			UPDATE UserCompany
			SET ComCode = 'KXS',
			IntegrityNo = IntegrityNo + 1
			WHERE UsrId = @l_UserId
		END
	-- UPDATE COMPANY
	IF @@ERROR <> 0
	BEGIN
		SELECT 'ERROR/Error Setting Company Code'
		GOTO Error
	END
	-- UPDATE USER TABLE


	UPDATE 
		UserInfo
	SET 
		UsrLocCode = @l_AltLocationCode,
		UsrAltLocCode = @l_CurrLocationCode,
		IntegrityNo = IntegrityNo + 1
	WHERE
		UsrId = @l_UserId
	IF @@ERROR <> 0
	BEGIN
		SELECT 'ERROR/Error Setting Alternate Location'
		GOTO Error
	END

	-- UPDATE USER TABLE
	SUCCESS:
		SELECT 'SUCCESS/' + DBO.getErrorString('GEN046', NULL, NULL, NULL, NULL, NULL, NULL)
		RETURN

	ERROR:
		RETURN
END