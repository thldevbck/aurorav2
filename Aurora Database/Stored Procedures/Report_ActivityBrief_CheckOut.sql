CREATE  PROCEDURE [dbo].[Report_ActivityBrief_CheckOut]
	@sCtyCode		VARCHAR(64),
	@sLocCode		VARCHAR(64) = '',
	@sFromDate		VARCHAR(12),
	@sToDate		VARCHAR(12),
	@sBrdCode		VARCHAR(10) = '',
	@sClaCode		VARCHAR(64) = '',
	@sTypCode		VARCHAR(12) = '',
	@sPrdShortName	VARCHAR(8) = ''
AS
SET NOCOUNT ON

/*
DECLARE	@sCtyCode		VARCHAR(64),
		@sLocCode		VARCHAR(64),
		@sFromDate		VARCHAR(12),
		@sToDate		VARCHAR(12),
		@sBrdCode		VARCHAR(10),
		@sClaCode		VARCHAR(64),
		@sTypCode		VARCHAR(12),
		@sPrdShortName	VARCHAR(8)

SET		@sCtyCode = 'NZ'
SET		@sLocCode = ''
SET		@sFromDate = '01/04/2008'
SET		@sToDate = '11/04/2008'
SET		@sBrdCode = ''
SET		@sClaCode = 'AV'
SET		@sTypCode = '4B'
SET		@sPrdShortName = '4BB'
*/
-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND EXISTS (SELECT Top 1 * FROM Product WHERE Product.PrdTypId = Type.TypId
										AND Product.PrdIsActive = 1 AND Product.PrdIsVehicle = 1)
							AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

DECLARE 	@sCodIdXfrm	VARCHAR(64),
			@sCodIdExt	VARCHAR(64)

declare @Ckotable table (ttprdshortname varchar(32), ttrntckowhen varchar(10), ttxmltext varchar(2000))
declare @Ckitable table (ttprdshortname varchar(32), ttrntckiwhen varchar(10), ttxmltext varchar(2000))

DECLARE @AU_THRIFTYPRDS VARCHAR(1000)
DECLARE @AU_THRIFTYEFFDT datetime

SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

declare @Result_CheckOut table
(
	CkoLocCode		VARCHAR(64),
	Brand 			VARCHAR(64),
	BkdVehicle		VARCHAR(64),
	UnitNum 		VARCHAR(64),
	SchedVehicle	VARCHAR(64),
	CustName		VARCHAR(64),
	IsVip			VARCHAR(1),
	Pax				VARCHAR(64),
	ArrivalRef		VARCHAR(64),
	ConnecDate		VARCHAR(64),
	FromDate		VARCHAR(20),
	ToDate			VARCHAR(20),
	Period			VARCHAR(20),
	PkgCode			VARCHAR(64),
	BookRental		VARCHAR(64),
	ActiveNote		VARCHAR(1)
)

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME,
		@sRentalId	VARCHAR(64),
		@ssBpdid    varchar(64)


SET @StartDate= CONVERT(DATETIME, @sFromDate,103)
SET @EndDate=CONVERT(DATETIME, @sToDate + ' 23:59:59' ,103)
SET @WkDATE=CONVERT(DATETIME,@sFromDate,103)


DECLARE @ssBrd 			VARCHAR(64),
		@sBooNum		VARCHAR(64),
		@sBooId			varchar(64),
		@sRntNum		VARCHAR(64),
		@sPrd			VARCHAR(64),
		@sPkgCode		VARCHAR(64),
		@ssCkoLocCode	VARCHAR(64),
		@dRntCkoWhen   	datetime,
		@dRntCkiWhen	datetime,
		@sXMLText		varchar(2000)

SELECT	@sCodIdXfrm = dbo.getCodeId(4, 'XFRM')
SELECT	@sCodIdEXT = dbo.getCodeId(4, 'EXT')

-- Declare a cursor
DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT	RntId
	 FROM 	Rental WITH(NOLOCK)
	 WHERE	RntStatus  IN ('KK','CO','CI')
	 AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate
	 AND	(RntCkoLocCode = @sLocCode OR (@sLocCode = ''
	 										AND	EXISTS(SELECT TOP 1 loccode
														FROM 	Location WITH(NOLOCK), TownCity WITH(NOLOCK)
														WHERE	RntCkoLocCode = LocCode
														AND		LocTctCode = TctCode
														AND		TctCtyCode = @sCtyCode)))
OPEN Cur_out

FETCH NEXT FROM Cur_out INTO @sRentalId

WHILE @@FETCH_STATUS=0
BEGIN
	SET @sBooNum = null
	SET @sBooid  = NULL

	select	@sBooNum = Boonum,
			@sBooId = Booid,
			@ssBrd 	 = PkgBrdCode,
			@sRntNum = RntNum,
			@sPkgCode = PkgCode,
			@ssCkoLocCode = RntCkoLocCode,
			@dRntCkoWhen  = RntCkoWhen
		From booking WITH (NOLOCK), dbo.rental WITH (NOLOCK), dbo.package (nolock)
		where rntid = @sRentalId and rntbooid = Booid
		AND rntpkgid = pkgid

	SELECT top 1 @ssBpdid = Bpdid
		From dbo.bookedproduct (nolock), dbo.saleableproduct (nolock), dbo.product (nolock)
		WHERE bpdrntid = @sRentalId
		and Bpdstatus = 'KK'
		and bpdprdisvehicle = 1
		and bpdsapid = sapid
		and sapprdid = prdid
		and (bpdcodtypeid is null or bpdcodtypeid <> @sCodIdXfrm)
		order by BpdCkoWhen

	select 	@sprd = prdshortname
	from dbo.product (nolock), rental (nolock)
	WHERE 	prdid 	= rntprdrequestedvehicleid
	and		rntid	= @sRentalId

	IF @ssBpdid is null or @ssBpdid = ''
		GOTO GETNEXTRENTALCKO

	IF @sPrdId is not null and @sPrdId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock)
					WHERE bpdid = @ssBpdid and bpdsapid = sapid and sapprdid = prdid and prdid <> @sPrdId)
			GOTO GETNEXTRENTALCKO
	END
	IF @sTypId is not null and @sTypId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock)
					WHERE bpdid = @ssBpdid and bpdsapid = sapid and sapprdid = prdid and prdtypid <> @sTypId)
			GOTO GETNEXTRENTALCKO
	END

	IF @sClaId is not null and @sClaId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock)
					WHERE bpdid = @ssBpdid and bpdsapid = sapid and sapprdid = prdid and prdtypid = typid and typclaid <> @sClaId)
			GOTO GETNEXTRENTALCKO
	END

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> '' and @sCtyCode = 'AU'
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @ssBpdid
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTRENTALCKO
	END


	IF @sBrdCode is not null and @sBrdCode <> '' AND @ssBrd <> @sBrdCode
		GOTO GETNEXTRENTALCKO


	DECLARE @BpdId 			VARCHAR(2000),
			@ReturnError 	VARCHAR(500),
			@sUnitNum 		VARCHAR(64) ,
			@sSchedVehicle	VARCHAR(64) ,
			@sCustName		VARCHAR(64),
			@sPax			int,
			@sArrivalRef	VARCHAR(64),
			@sDate			VARCHAR(64),
			@sConWhen		datetime,
			@sFrom			VARCHAR(20),
			@sTo			VARCHAR(20),
			@sRntCodUomId 	VARCHAR(64),
			@sPeriod		VARCHAR(20),
			@sBookRental		VARCHAR(64),
			@sBookingId		VARCHAR(64),
			@sCurrId 		VARCHAR(64),
			@sAmount		money

	SET 	@sSchedVehicle=''
	SET 	@sUnitNum=''
	SET		@sCustName=''
	SET		@sPax=0
	SET		@sArrivalRef=''
	SET		@sDate=''
	SET		@sRntCodUomId=''
	SET		@sPeriod=''
	SET		@sBookRental=''


	SET		@sBookRental = ISNULL(@sBooNum,'') + '/' + ISNULL(@sRntNum,'')

	SELECT  @sUnitNum = DraUnitNum FROM Dvs_RentalAssign WITH (NOLOCK)
		WHERE 	CtyCode = @sCtyCode AND DraRntNum=@sBookRental

	if @sunitnum is null
		SET @sunitnum = ''

	SELECT 	@sSchedVehicle = FleetModelCode
	 FROM 	aimsprod.dbo.FleetModel WITH (NOLOCK) INNER JOIN
			fleetassetview WITH (NOLOCK)
			ON aimsprod.dbo.FleetModel.FleetModelId  = fleetassetview.FleetModelId
	 WHERE 	UnitNumber = @sUnitNum

	SELECT	@sCustName = ISNULL(Customer.CusLastName,'')
	 FROM	Customer WITH (NOLOCK) , Traveller WITH (NOLOCK)
	 WHERE	Traveller.TrvIsPrimaryHirer = 1

	 AND	Customer.CusId = Traveller.TrvCusId
 	 AND	Traveller.TrvRntId = @sRentalId

	IF ISNULL(@sCustName,'')=''
		SELECT 	@sCustName = ISNULL(BooLastName,'')
		 FROM 	Rental WITH(NOLOCK), Booking WITH(NOLOCK)
		 WHERE	RntBooId = BooID
		 AND	RntId = @sRentalId

	SELECT	@sPax = ISNULL(RntNumOfAdults, 0) + ISNULL(RntNumOfInfants, 0) + ISNULL(RntNumOfChildren, 0) ,
			@sArrivalRef = ISNULL(RntArrivalConnectionRef,''),
			@sConWhen = isnull(RntArrivalConnectionWhen,''),
			@sFrom = RntCkoLocCode + '  ' + CONVERT(CHAR(10),RntCkoWhen,103),
			@sTo = RntCkiLocCode + '  ' + CONVERT(CHAR(10),RntCkiWhen,103),
			@sRntCodUomId = RntCodUomId,
			@sPeriod =  CAST(RntNumOfHirePeriods as VARCHAR) + ' Days',
			@sunitnum = CASE WHEN renallowvehspec is not null and renallowvehspec <> '' then  @sunitnum + '(F)' else @sunitnum end
	 FROM	Rental WITH (NOLOCK)
	 WHERE	RntId = @sRentalId

	SELECT 	@sDate = CONVERT(VARCHAR,@sConWhen,103) + ' ' + substring((CONVERT(VARCHAR,@sConWhen,108)),1,2)+substring((CONVERT(VARCHAR,@sConWhen,108)),4,2)
	IF 		@sDAte = '0' OR @sDAte is null OR @sDate like '%1900%'
		SET @sDate = ''

	-- Work out tabular results AJ May 2008
	DECLARE @sRntIsVip VARCHAR(1)
	SELECT	@sRntIsVip = (CASE RntIsVip WHEN '1' THEN 'Y' ELSE 'N' END)
	FROM	Rental WITH (NOLOCK)
	WHERE	RntId = @sRentalId

	DECLARE @sActiveNote VARCHAR(1)
	IF EXISTS(	SELECT  nteid
				 FROM 	Note WITH (NOLOCK)
				 WHERE 	Note.NteRntId = @sRentalId
				 AND	Note.ntebooid = @sBooId
				 AND	NteIsActive = 1 )
		SELECT	@sActiveNote  = 'Y'
	ELSE
		SELECT	@sActiveNote = 'N'

	insert into @Result_CheckOut values
	(
		LTRIM(RTRIM(@ssCkoLocCode)),
		@ssBrd,
		ISNULL(@sPrd, ''),
		ISNULL(@sUnitNum, ''),
		ISNULL(@sSchedVehicle,''),
		ISNULL(@sCustName, ''),
		@sRntIsVip,
		CAST(@sPax as VARCHAR),
		ISNULL(@sArrivalRef, ''),
		ISNULL(@sDate, ''),
		ISNULL(@sFrom, ''),
		ISNULL(@sTo, ''),
		ISNULL(@sPeriod, 0),
		ISNULL(@sPkgCode, ''),
		@sBookRental,
		@sActiveNote
	)

	GETNEXTRENTALCKO:
	SET @sPrd = null
	set @dRntCkoWhen = null
	set @sXMLText = null

	FETCH NEXT FROM Cur_out INTO @sRentalId
END

select *
from @Result_CheckOut
where Brand IN (		-- KX fix AJ May 2008
				SELECT	Brand.BrdCode
				FROM	UserInfo INNER JOIN
						UserCompany ON UserInfo.UsrId = UserCompany.UsrId INNER JOIN
						Brand ON UserCompany.ComCode = Brand.BrdComCode
				WHERE   (UserInfo.UsrCode = User)
				)






GO
