CREATE PROCEDURE [dbo].[Product_GetPopUpData]
	@case varchar(30)	=	'',
	@param1 varchar(64)	=	'',
	@param2 varchar(64)	=	'',
	@param3 varchar(500)	=	'',
	@param4 varchar(64)	=	'' ,
	@param5 varchar(64)  = '',
	@sUsrCode varchar(64) = ''
AS

IF(@case = 'PRODUCT_PACKAGE')
BEGIN
	SELECT DISTINCT
		POPUP.PkgId AS ID,
		POPUP.PkgCode AS CODE,
		POPUP.PkgName AS DESCRIPTION,
		PackageBrand.BrdCode + '-' + PackageBrand.BrdName AS BRAND,
		('' + POPUP.PkgIsActive) AS STATUS
	FROM
		Package AS POPUP
		INNER JOIN Brand AS PackageBrand ON POPUP.PkgBrdCode = PackageBrand .BrdCode

		INNER JOIN SaleableProduct ON POPUP.PkgCtyCode = SaleableProduct.SapCtyCode
		INNER JOIN Product ON SaleableProduct.SapPrdId = Product.PrdId
		INNER JOIN Brand AS ProductBrand ON Product.PrdBrdCode = ProductBrand.BrdCode

		INNER JOIN Brand ON (Brand.BrdCode = ProductBrand.BrdCode OR ProductBrand.BrdIsGeneric = 1) AND (Brand.BrdCode = PackageBrand.BrdCode OR PackageBrand.BrdIsGeneric = 1)
		INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
		INNER JOIN UserCompany ON UserCompany.ComCode = Company.ComCode
		INNER JOIN UserInfo ON UserInfo.UsrId = UserCompany.UsrId
	WHERE
		((POPUP.PkgCode LIKE (@param1 + '%')) OR (POPUP.PkgName LIKE (@param1 + '%')))
		AND SaleableProduct.SapId = @Param2
		AND UserInfo.UsrCode = @sUsrCode
	ORDER BY
		POPUP.PkgCode
	FOR XML AUTO, ELEMENTS

	RETURN
END


GO
