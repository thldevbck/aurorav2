---------------------------------------------------------------------------------------------------------------------
-- this script is intended for credit card security enhancement
-- REV:MIA 01Aug - created
---------------------------------------------------------------------------------------------------------------------

create procedure sp_ViewCreditCard
			     @sUserCode varchar(64)  
as

DECLARE @UserCode varchar(64)
DECLARE @Password varchar(64)
select @UserCode=UsrCode,@Password=UsrPsswd from UserInfo where usrcode = @sUserCode and UsrIsActive=1
execute dialog_checkUser @UserCode,@Password,'VIEWPMT'

go




/* 
			Summary: Add credit card to the query returned by the procedure
					This is primarily use in the Note Usercontrol

*/
Alter    PROCEDURE spgetNoteType  
 @sCodcdtnum         int  = default, /*Mandatory*/  
 @sCode    VARCHAR(24) =''  
AS  
SET NOCOUNT ON  
DECLARE @soryBy VARCHAR(60)  
  
 /*Get the order of display*/  
 SELECT @soryBy = CdtSortBy FROM CodeType WHERE CdtNum = @sCodcdtnum  
 IF (@soryBy = NULL)  
  SET @soryBy = ''  
  
 IF (@soryBy = '' OR @soryBy = 'CodCode')  
 BEGIN  
  SELECT CodId    AS 'ID',  
    CodCode  AS 'CODE',  
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum      = @sCodcdtnum  
   AND  (CodCode LIKE @sCode + '%' OR  
      CodDesc LIKE @sCode + '%')  
   AND  CodIsActive = 1  
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodCode  
   FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
  
 IF (@soryBy = 'CodDesc' )  
 BEGIN  
  SELECT CodId    AS 'ID',  
    CodCode  AS 'CODE',  
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum      = @sCodcdtnum  
   AND  (CodCode LIKE @sCode + '%' OR  
      CodDesc LIKE @sCode + '%')  
   AND  CodIsActive = 1  
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodDesc  
   FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
       
 IF (@soryBy = 'CodOrder' )  
 BEGIN  
  SELECT CodId    AS 'ID',  
    CodCode  AS 'CODE',  
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum      = @sCodcdtnum  
   AND  (CodCode LIKE @sCode + '%' OR  
      CodDesc LIKE @sCode + '%')  
   AND  CodIsActive = 1  
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodOrder  
   FOR XML AUTO,ELEMENTS  
  
  RETURN  
 END  
    RETURN  
  