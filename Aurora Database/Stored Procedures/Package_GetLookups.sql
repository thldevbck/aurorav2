CREATE PROCEDURE [dbo].[Package_GetLookups]
(
	@ComCode AS varchar(64)
)
AS

/*********************************************************************************************/
/* 0 - Brand */
/*********************************************************************************************/
SELECT
	Brand.*
FROM
	Brand
WHERE
	BrdComCode = @ComCode
ORDER BY
	Brand.BrdCode

/*********************************************************************************************/
/* 1 - Code */
/*********************************************************************************************/
SELECT
	Code.*
FROM
	Code
	INNER JOIN CodeType ON CodeType.CdtNum = Code.CodCdtNum
ORDER BY
	CodeType.CdtNum,
	Code.CodOrder,
	Code.CodDesc

/*********************************************************************************************/
/* 2 - Country */
/*********************************************************************************************/
SELECT
	Country.*
FROM
	Country
WHERE
	Country.CtyHasProducts = 1
ORDER BY
	Country.CtyCode

/*********************************************************************************************/
/* 3 - Location */
/*********************************************************************************************/
SELECT
	Location.*
FROM
	Location
ORDER BY
	Location.LocCode

/*********************************************************************************************/
/* 4 - Type */
/*********************************************************************************************/
SELECT
	Type.*
FROM
	Type
	INNER JOIN Class
		ON Class.ClaID = Type.TypClaID
ORDER BY
	Class.ClaOrder,
	Type.TypOrder,
	Type.TypCode

/*********************************************************************************************/
/* 5 - "FlexTravelYear" */
/*********************************************************************************************/
SELECT
	DISTINCT FwnTrvYearStart
FROM
	FlexWeekNumber
ORDER BY
	FwnTrvYearStart DESC

GO
