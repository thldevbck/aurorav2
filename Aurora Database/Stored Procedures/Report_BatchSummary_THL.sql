CREATE PROCEDURE [dbo].[Report_BatchSummary_THL]
(
	@BatchNum AS varchar(64),
	@BND AS varchar(64) = '',
	@CBD AS varchar(64) = '',
	@CDP AS varchar(64) = '',
	@CRC AS varchar(64) = '',
	@CRD AS varchar(64) = '',
	@DEP AS varchar(64) = '',
	@INV AS varchar(64) = '',
	@REC AS varchar(64) = '',
	@ALL AS varchar(64) = ''
)
AS

/*
DECLARE @BatchNum varchar(64)
DECLARE @BND AS varchar(64)
DECLARE @CBD AS varchar(64)
DECLARE @CDP AS varchar(64)
DECLARE @CRC AS varchar(64)
DECLARE @CRD AS varchar(64)
DECLARE @DEP AS varchar(64)
DECLARE @INV AS varchar(64)
DECLARE @REC AS varchar(64)
DECLARE @ALL AS varchar(64)
SET @BatchNum = '421'
SET @BND = NULL
SET @CBD = NULL
SET @CDP = NULL
SET @CRC = NULL
SET @CRD = NULL
SET @DEP = NULL
SET @INV = NULL
SET @REC = NULL
SET @ALL = 'ALL'
*/

Declare @QueryResult	Varchar(8000)
Declare @QueryPart1		Varchar(3000)
Declare @QueryPart2		Varchar(3000)
Declare @QueryPart3		Varchar(1000)

Set @QueryPart1 = '
	SELECT
		AthTranType AS TranType,
		AthCurrCode AS Currency,
  		CONVERT(VARCHAR(14),(REPLACE(AthBooNum,''x'','''') + ''-'' + convert(varchar(2),AthRntNum))) AS Booking,
		AthHashTotal AS InvoiceAmount,
  		CONVERT(VARCHAR(10),AthGLEffDate,103) AS EffectiveDate
	FROM
		AssTranHeader
	WHERE
		AthBatchNum < ''30126''
		AND AthBatchNum = ' + @BatchNum

Set @QueryPart3 = '
	ORDER BY
		athtrantype'

If @ALL = 'ALL'
	Set @QueryPart2 = ''
Else
	Begin
		Set @QueryPart2 = '
			AND ((AthTranType = ''' + IsNull(@BND, '') + ''')
			OR (AthTranType = ''' + IsNull(@CBD, '') + ''')
			OR (AthTranType = ''' + IsNull(@CDP, '') + ''')
			OR (AthTranType = ''' + IsNull(@CRC, '') + ''')
			OR (AthTranType = ''' + IsNull(@CRD, '') + ''')
			OR (AthTranType = ''' + IsNull(@DEP, '') + ''')
			OR (AthTranType = ''' + IsNull(@INV, '') + ''')
			OR (AthTranType = ''' + IsNull(@REC, '') + '''))'
	End

Set @QueryResult = @QueryPart1 + @QueryPart2 + @QueryPart3
Exec (@QueryResult)


GO
