CREATE PROCEDURE [dbo].[Report_Get]
	@repId AS varchar(64),
	@funCode AS varchar(12)
AS

SELECT
	Report.*
FROM
	Report
WHERE
	(@repId IS NULL OR Report.repId = @repId)
	AND (@funCode IS NULL OR Report.repFunCode = @funCode)
ORDER BY
	Report.repFunCode


SELECT
	ReportParam.*
FROM
	Report
	INNER JOIN ReportParam ON Report.repId = ReportParam.rppRepId
WHERE
	(@repId IS NULL OR Report.repId = @repId)
	AND (@funCode IS NULL OR Report.repFunCode = @funCode)
ORDER BY
	Report.repFunCode,
	ReportParam.rppOrder,
	ReportParam.rppName


SELECT
	ReportParamListItem.*
FROM
	Report
	INNER JOIN ReportParam ON Report.repId = ReportParam.rppRepId
	INNER JOIN ReportParamListItem ON ReportParam.rppId = ReportParamListItem.rpiRppId
WHERE
	(@repId IS NULL OR Report.repId = @repId)
	AND (@funCode IS NULL OR Report.repFunCode = @funCode)
ORDER BY
	Report.repFunCode,
	ReportParam.rppOrder,
	ReportParam.rppName,
	ReportParamListItem.rpiOrder,
	ReportParamListItem.rpiName
















GO
