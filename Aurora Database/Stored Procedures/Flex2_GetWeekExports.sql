USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetWeekExports]    Script Date: 11/19/2007 13:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Flex2_GetWeekExports] 
	@ComCode varchar(64),
	@FbwId int,
	@FwxId int
AS

SELECT 
	*
FROM 
	FlexWeekExportView
WHERE
	FlexWeekExportView.FbwComCode = @ComCode
	AND (@FbwId IS NULL OR FlexWeekExportView.FbwId = @FbwId)
	AND (@FwxId IS NULL OR FlexWeekExportView.FwxId = @FwxId)
ORDER BY
	FlexWeekExportView.FlxCode
