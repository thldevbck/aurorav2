set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_getNewsFlash] 

		@NwfId			varchar	(64)

AS
BEGIN
	SET NOCOUNT ON

	select	 
			NwfID								AS	NwfID ,
			NwfAudienceType						AS	Audience,
			NwfText							AS	NewflashText,
			CONVERT(VARCHAR(10),NwfEffectiveDate,103)		AS	EffFr ,
			CONVERT(VARCHAR(10),NwfTerminationDate,103)		AS	EffTo
	from	NewsFlash
	where	NwfID = @NwfId
	
END
