CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckOut]
	@sCtyCode		VARCHAR(64),
	@sLocCode		VARCHAR(64) = '',
	@sFromDate		VARCHAR(12),
	@sToDate		VARCHAR(12),
	@sBrdCode		VARCHAR(10) = '',
	@sClaCode		VARCHAR(12) = '',
	@sTypCode		VARCHAR(12) = '',
	@sPrdShortName	VARCHAR(8) = ''
AS


SET NOCOUNT ON
/*
DECLARE	@sCtyCode		VARCHAR(64),
		@sLocCode		VARCHAR(64),
		@sFromDate		VARCHAR(12),
		@sToDate		VARCHAR(12),
		@sBrdCode		VARCHAR(10),
		@sClaCode		VARCHAR(12),
		@sTypCode		VARCHAR(12),
		@sPrdShortName	VARCHAR(8)

SET		@sCtyCode = 'NZ'
SET		@sLocCode = 'AKL'
SET		@sFromDate = '01/04/2008'
SET		@sToDate = '11/04/2008'
SET		@sBrdCode = 'B'
SET		@sClaCode = 'AV'
SET		@sTypCode = '4B'
SET		@sPrdShortName = '4BB'
*/

-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND EXISTS (SELECT Top 1 * FROM Product WHERE Product.PrdTypId = Type.TypId
										AND Product.PrdIsActive = 1 AND Product.PrdIsVehicle = 1)
							AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

--SET DATEFORMAT dmy
DECLARE @AU_THRIFTYPRDS VARCHAR(1000),
		@AU_THRIFTYEFFDT datetime,
		@dRntCkoWhen   	datetime,
		@dRntCkiWhen	datetime

SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

-- Declare check out result variable table
declare @Result_CheckOut table
(
	BookRental		VARCHAR(64),
	CkoLocCode		VARCHAR(64),
	Brand 			VARCHAR(64),
	BkdVehicle		VARCHAR(64),
	UnitNum 		VARCHAR(64),
	SchedVehicle	VARCHAR(64),
	CustName		VARCHAR(64),
	IsVip			VARCHAR(1),
	Pax				VARCHAR(64),
	FromDate		VARCHAR(20),
	ToDate			VARCHAR(20),
	Period			VARCHAR(20),
	PkgCode			VARCHAR(64),
	Note			VARCHAR(4000)
)

-- Create or purge products table
IF NOT EXISTS
(
	SELECT TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = 'Report_ActDetCKOProducts'
)
	BEGIN
		CREATE TABLE Report_ActDetCKOProducts
		(
			BookRental		VARCHAR(64),
			PrdName			VARCHAR(64),
			Currs			VARCHAR(20),
			Amounts			VARCHAR(64)
		)
	END
ELSE
	DELETE reports.Report_ActDetCKOProducts

-- Create or purge payment table
IF NOT EXISTS
(
	SELECT TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = 'Report_ActDetCKOPayment'
)
	BEGIN
		CREATE TABLE Report_ActDetCKOPayment
		(
			BookRental		VARCHAR(64),
			Payment			VARCHAR(64),
			Curr			VARCHAR(64)
		)
	END
ELSE
	DELETE reports.Report_ActDetCKOPayment

-- Variables for Cur_Note
DECLARE @sNote		VARCHAR(2000),
		@sDateTime	VARCHAR(20),
		@Note		VARCHAR(4000)

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME,
		@sRentalId	VARCHAR(64)

SET @StartDate	=	CONVERT(DATETIME, @sFromDate, 103)
SET @EndDate	=	CONVERT(DATETIME, @sToDate+' 23:59:59', 103)
SET @WkDATE		=	CONVERT(DATETIME, @sFromDate, 103)

DECLARE @BpdId 			VARCHAR(1000),	@ReturnError 	VARCHAR(500),
		@sUnitNum 		VARCHAR(64),	@sSchedVehicle	VARCHAR(64),
		@sCustName		VARCHAR(64),	@sPax			int,
		@sArrivalRef	VARCHAR(64),	@sDate			VARCHAR(64),
		@sConWhen		DATETIME,		@sFrom			VARCHAR(20),
		@sTo			VARCHAR(20),	@sRntCodUomId 	VARCHAR(64),
		@sPeriod		VARCHAR(20),	@sPkgCode		VARCHAR(64),
		@sBooNum		VARCHAR(64),	@sRntNum		VARCHAR(64),
		@sBookRental	VARCHAR(64),	@sBookingId		VARCHAR(64),
		@sCurrId 		VARCHAR(64),	@sAmount		MONEY,
		@sBrd			VARCHAR(10),	@sPrd			VARCHAR(64)

CREATE TABLE #TT_CheckOut( RntId	VARCHAR(64))

DECLARE	@sql VARCHAR(5000)
SELECT @sql = 'INSERT INTO #TT_CheckOut (RntId)
						SELECT  RntId FROM Rental WITH(NOLOCK), Package WITH(NOLOCK)
							WHERE	RntPkgId = PkgId
							AND		RntStatus IN (''CO'',''KK'',''CI'')
							AND		RntCkoWhen BETWEEN ''' + @sFromDate + ''' AND ''' + @sToDate + ' 23:59:59'''

IF @sLocCode <> ''
	SET @sql = @sql + ' AND RntCkoLocCode = ''' + @sLocCode + ''''
ELSE
	SET @sql = @sql + ' AND	EXISTS(SELECT TOP 1 Location.loccode
									FROM Location WITH(NOLOCK), TownCity WITH(NOLOCK)
									WHERE	RntCkoLocCode = LocCode
									AND		LocTctCode = TctCode
									AND		TctCtyCode = ''' + @sCtyCode + ''')'

IF @sBrdCode <> ''
	SET @sql = @sql + ' AND PkgBrdCode = ''' + @sBrdCode + ''''

IF @sClaId <> '' OR @sTypId <> '' OR @sPrdId <> ''
BEGIN
	SET @sql = @sql + ' AND EXISTS(SELECT BpdId FROM dbo.BookedProduct WITH(NOLOCK), dbo.SaleableProduct WITH(NOLOCK), dbo.Product WITH(NOLOCK), dbo.Type WITH(NOLOCK)
										WHERE	BpdSapId = SapId
										AND		PrdId = SapPrdId
										AND		PrdTypId = TypId
										AND		BpdRntId = RntId
										AND		BpdPrdIsVehicle = 1
										AND 	BpdStatus = ''KK'' ' +
										CASE
											WHEN @sTypId <> '' THEN ' AND	PrdTypId = ''' + @sTypId + ''''
											ELSE ''
										END +
										CASE
											WHEN @sClaId <> '' THEN ' AND	TypClaId = ''' + @sClaId + ''''
											ELSE ''
										END +
										CASE
											WHEN @sPrdId <> '' THEN ' AND	PrdId = ''' + @sPrdId + ''''
											ELSE ''
										END +
										')'
END
SET @sql = @sql + ' ORDER BY RntCkoLocCode, RntCkoWhen, RntCkiLocCode, RntCkiWhen'
--	select @sql
EXECUTE (@sql)

DECLARE Cur_out CURSOR FAST_FORWARD LOCAL  FOR
	SELECT RntId FROM #TT_CheckOut

OPEN Cur_out

FETCH NEXT FROM Cur_out INTO @sRentalId

WHILE @@FETCH_STATUS=0
BEGIN
	SELECT	@BpdId 		=NULL,	@ReturnError 	=NULL,
			@sUnitNum 	='',	@sSchedVehicle	='',
			@sCustName	='',	@sPax			=0,
			@sArrivalRef='',	@sDate			='',
			@sFrom		='',	@sTo			='',
			@sRntCodUomId ='',	@sPeriod		='',
			@sPkgCode	='',	@sBooNum		='',
			@sRntNum	='',	@sBookRental	='',
			@sBookingId	='',	@sCurrId 		='',
			@sBrd		='',	@sPrd			='',
			@dRntCkoWhen = null

-- get book number and rental number
	SELECT 	@sRntNum = Rental.RntNum,
			@sBooNum = Booking.booNum,
			@sBrd = PkgBrdCode,
			@dRntCkoWhen = RntCkoWhen
	FROM	Rental WITH(NOLOCK), Booking WITH(NOLOCK), Package WITH(NOLOCK)
	WHERE	Rental.RntBooId = Booking.booId
	AND		PkgId = Rental.RntPkgId
	AND		Rental.RntId = @sRentalId

	SELECT 	@sBookRental = @sBooNum + '/' + @sRntNum

-- get BpdId
	SELECT 	@sBookingId = RntBooId,
			@sPax = ISNULL(RntNumOfAdults, 0) + ISNULL(RntNumOfInfants, 0) + ISNULL(RntNumOfChildren, 0)
	FROM	dbo.Rental WITH(NOLOCK)
	WHERE	RntId = @sRentalId
	SET 	@BpdId = NULL

	SELECT top 1
			@BpdId = Bpdid
		From dbo.bookedproduct (nolock)
		WHERE 	Bpdrntid = @sRentalId
		AND 	bpdStatus in ('KK','NN','WL','QN','IN')
		AND     bpdprdisvehicle = 1
		Order by BpdCkoWhen

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @BpdId
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTRENTALCKO
	END

	--get booked vehicle
	SET 	@sPrd = ''
	SET 	@BpdId = dbo.getSplitedData(@BpdId, ',', 1)

	SELECT 	@sPrd = product.PrdShortName
	FROM	dbo.Bookedproduct WITH(NOLOCK), dbo.Product WITH(NOLOCK), dbo.rental WITH(NOLOCK)
	WHERE  	Bpdid = @BpdId
	AND 	rntid=bpdrntid
	AND 	PrdId=rntprdrequestedvehicleid

	--get unit no
	SELECT  DISTINCT
			@sUnitNum = DraUnitNum
	FROM 	dbo.Dvs_RentalAssign WITH(NOLOCK)
	WHERE 	Ctycode = @sCtyCode
	AND     DraRntNum=@sBookRental

	-- get sched vehicle
	SELECT 	@sSchedVehicle = FleetModelCode
	 FROM 	aimsprod.dbo.FleetModel WITH (NOLOCK) INNER JOIN
			fleetassetview WITH (NOLOCK)
			ON aimsprod.dbo.FleetModel.FleetModelId  = fleetassetview.FleetModelId
	 WHERE 	UnitNumber = @sUnitNum

	--get hirer
	SELECT 	@sCustName = Customer.CusLastName,
			@sUnitNum = Case when Renallowvehspec is not null and renallowvehspec <> '' then @sUnitNum + '(F)' else @sUnitNum end
	FROM	dbo.Customer WITH(NOLOCK), dbo.Traveller WITH(NOLOCK), dbo.Rental WITH(NOLOCK)
	WHERE	Traveller.TrvRntId = Rental.RntId AND
			Traveller.TrvIsPrimaryHirer = 1 AND
			Customer.CusId = Traveller.TrvCusId AND
			Rental.RntId = @sRentalId

	-- get check-out location code
	DECLARE @sCkoLocCode VARCHAR(64)
	SELECT 	@sCkoLocCode = LTRIM(RTRIM(RntCkoLocCode))
	FROM 	Rental WITH(NOLOCK)
	WHERE 	RntId = @sRentalId

	-- get Vip
	DECLARE @sRntIsVip VARCHAR(1)
	SELECT	@sRntIsVip = (CASE RntIsVip WHEN '1' THEN 'Y' ELSE 'N' END)
	FROM	Rental WITH (NOLOCK)
	WHERE	RntId = @sRentalId

	-- get from
	SELECT 	@sFrom = RntCkoLocCode + '  ' + CONVERT(char(10),RntCkoWhen,103),
			@sTo = RntCkiLocCode + '  ' + CONVERT(char(10),RntCkiWhen,103),
			@sRntCodUomId = RntCodUomId,
			@sPkgCode = PkgCode,
			@sPeriod =  cast(ISNULL(RntNumOfHirePeriods,0) as varchar) + ' Days'
	FROM 	Rental WITH(NOLOCK), Package WITH(NOLOCK)
	WHERE 	Package.PkgId = Rental.RntPkgId
	AND 	RntId = @sRentalId

	-- get note
	SET @Note = ''
	DECLARE Cur_Note CURSOR FOR
		-- Line feed: char(10)
		SELECT 	 ISNULL(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(replace(note.ntedesc,char(150),char(45)),'''',''),'”',''),'á',''),'“',''),'’',''''), char(10), ' '), '') AS NteDesc,
					CONVERT(varchar,Note.AddDATETIME,103) + ' ' + SUBSTRING((CONVERT(varchar,Note.AddDATETIME,108)),1,2)+':' + SUBSTRING((CONVERT(varchar,Note.AddDATETIME,108)),4,2) AS [DATETIME]
		FROM    Note WITH(NOLOCK)
		WHERE  	NteBooId = @sBookingId
		AND (NteRntId = @sRentalId OR  NteRntId is null)
		AND NteIsActive = 1
		ORDER BY Note.AddDATETIME

	OPEN Cur_Note
	FETCH NEXT FROM Cur_Note INTO @sNote, @sDateTime
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @Note = @Note + @sNote + ' '
		FETCH NEXT FROM Cur_Note INTO @sNote, @sDateTime
	END
	CLOSE Cur_Note
	DEALLOCATE Cur_Note

	-- get check out list result
	insert into @Result_CheckOut values
	(
		@sBookRental,
		@sCkoLocCode,
		@sBrd,
		ISNULL(@sPrd, ''),
		ISNULL(@sUnitNum, ''),
		ISNULL(@sSchedVehicle,''),
		ISNULL(@sCustName, ''),
		@sRntIsVip,
		CAST(@sPax as VARCHAR),
		ISNULL(@sFrom, ''),
		ISNULL(@sTo, ''),
		ISNULL(@sPeriod, 0),
		ISNULL(@sPkgCode, ''),
		@Note
	)

	-- get products
	-- 1. Non Security Products
	DECLARE		@NonSecurityBpdId	VARCHAR(8000),
				@doc			INT
	-- get booked non security products
	SET 	@NonSecurityBpdId = NULL
	EXEC RES_getBookedProducts
			@sRntId = @sRentalId,
			@bSelectCurrentOnly = 1,
			@sIsSecurity = 'N',
			@sExclStatusList = 'XX,XN',
			@dCkoWhenFrom = @StartDate, -- add on 30/09/2002
			@dCkoWhenTo = @EndDate,-- add on 30/09/2002
			@sOrderBy = 'STD',
			@ReturnCSV = 2,
			@sBpdList = @NonSecurityBpdId		OUTPUT,
			@ReturnError = @ReturnError		OUTPUT

	WHILE	@NonSecurityBpdId IS NOT NULL
	AND	LEN(@NonSecurityBpdId) > 0
	BEGIN
		SET @bPdId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @NonSecurityBpdId,
			@psListEntry		= @bPdId			OUTPUT,
			@psUpdatedList		= @NonSecurityBpdId	OUTPUT

		IF @bPdId is not null and @bPdId <> ''
		BEGIN

			declare	@amount		VARCHAR(64),
					@Curr		VARCHAR(20),
					@nsPrdName	VARCHAR(64),
					@isPrepaid	bit

			set 	@amount = ''
			set		@Curr = ''
			set		@nsPrdName = ''

			-- get non-security product name
			SELECT 	@nsPrdName = product.PrdShortName,
					@Curr = dbo.getCodCode(BpdCodCurrId),
					@isPrepaid = BpdIsCusCharge
			FROM	Bookedproduct WITH(NOLOCK), Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)
			WHERE  	Bpdid = @bPdId
			AND 	SapId=BpdSapId
			AND 	PrdId=SapPrdId

			-- get non-security amount
			IF @isPrepaid = 0
				SET @amount = 'Prepaid'
			ELSE
				SELECT	@amount = CAST((BpdGrossAmt - BpdAgnDisAmt) AS VARCHAR)
				FROM	BookedProduct WITH(NOLOCK)
				WHERE	BpdId = @bPdId

			-- get non-security products result
			insert into reports.Report_ActDetCKOProducts values
			(
				@sBookRental,
				@nsPrdName,
				ISNULL(@Curr,''),
				cast(ISNULL(@amount,0) as varchar)
			)
		END
	END

	-- 2. Security Products
	DECLARE	@SecurityBpdId	VARCHAR(8000),
			@sReturnError	VARCHAR(500),
			@sdoc		INT
	SET @SecurityBpdId = NULL
	EXEC RES_getBookedProducts
			@sRntId = @sRentalId,
			@bSelectCurrentOnly = 1,
			@sIsSecurity = 'Y',
			@sExclStatusList = 'XX,XN',
			@sOrderBy = 'STD',
			@ReturnCSV = 2,
			@sBpdList = @SecurityBpdId		OUTPUT,
			@ReturnError = @sReturnError		OUTPUT

	WHILE	@SecurityBpdId IS NOT NULL 	AND	LEN(@SecurityBpdId) > 0
	BEGIN
		SET @bPdId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @SecurityBpdId,
			@psListEntry		= @bPdId			OUTPUT,
			@psUpdatedList		= @SecurityBpdId	OUTPUT

		IF @bPdId is not null and @bPdId <> ''
		BEGIN
			DECLARE		@amounts	VARCHAR(64),
					@Currs		VARCHAR(20),
					@ssPrdName	VARCHAR(64),
					@isPrepaids	bit

			set 		@amounts = ''
			set		@Currs = ''
			set		@ssPrdName = ''

			-- get security product name
			SELECT 	@ssPrdName = product.PrdShortName,
					@Currs = dbo.getCodCode(BpdCodCurrId),
					@isPrepaids = BpdIsCusCharge
			FROM	Bookedproduct WITH(NOLOCK), Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)
			WHERE  	Bpdid = @bPdId
			AND 	SapId=BpdSapId
			AND 	PrdId=SapPrdId

			-- get security amounts
			IF @isPrepaid = 0
				SET @amounts = 'Prepaid'
			ELSE
				SELECT	@amounts = CAST((ISNULL(BpdGrossAmt,0) -ISNULL( BpdAgnDisAmt,0)) AS VARCHAR)
				FROM	BookedProduct WITH(NOLOCK)
				WHERE	BpdId = @bPdId

			-- get security products result
			insert into reports.Report_ActDetCKOProducts values
			(
				@sBookRental,
				@ssPrdName,
				ISNULL(@Currs,''),
				cast(ISNULL(@amounts,0) as varchar)
			)
		END
	END

	--get Pymt Rcvd
	DECLARE @RcvdNonSecurityAmount		VARCHAR(64)
	DECLARE @RcvdNonSecurityCurr		VARCHAR(64)
	SELECT 	@RcvdNonSecurityAmount = CAST(SUM(ISNULL(RentalPayment.RptChargeAmt, 0)) AS varchar),
            @RcvdNonSecurityCurr = dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)
	FROM   	Rental WITH(NOLOCK) INNER JOIN
            RentalPayment WITH(NOLOCK) ON Rental.RntId = RentalPayment.RptRntId  INNER JOIN
            Payment WITH(NOLOCK) ON RentalPayment.RptPmtId = Payment.PmtId
	WHERE  	(Rental.RntId=@sRentalId)
	GROUP BY dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)
	ORDER BY dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)

	insert into reports.Report_ActDetCKOPayment values
	(
		@sBookRental,
		@RcvdNonSecurityAmount,
		@RcvdNonSecurityCurr
	)

	GETNEXTRENTALCKO:
	FETCH NEXT FROM Cur_out INTO @sRentalId
END

CLOSE Cur_out
DEALLOCATE Cur_out
DROP TABLE #TT_CheckOut

select *
from @Result_CheckOut
where Brand IN (		-- KX fix AJ May 2008
				SELECT	Brand.BrdCode
				FROM	UserInfo INNER JOIN
						UserCompany ON UserInfo.UsrId = UserCompany.UsrId INNER JOIN
						Brand ON UserCompany.ComCode = Brand.BrdComCode
				WHERE   (UserInfo.UsrCode = User)
				)
	AND BkdVehicle <> ''



GO
