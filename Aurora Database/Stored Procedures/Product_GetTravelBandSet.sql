CREATE PROCEDURE [dbo].[Product_GetTravelBandSet]
(
	@ComCode AS varchar(64),
	@TbsId AS varchar(64)
)
AS

/*
	This SP gets the Travel Bands for a specific Travel Set
*/

SELECT
	ProductRateTravelBand.*
FROM
	Brand
	INNER JOIN Product ON Product.PrdBrdCode = Brand.BrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN TravelBandSet ON TravelBandSet.TbsSapId = SaleableProduct.SapId
	INNER JOIN ProductRateTravelBand ON ProductRateTravelBand.PtbTbsId = TravelBandSet.TbsId
WHERE
	Brand.BrdComCode = @ComCode
	AND TravelBandSet.TbsId = @TbsId
ORDER BY
	ProductRateTravelBand.PtbTravelFromDate,
	ProductRateTravelBand.AddDateTime DESC
GO
