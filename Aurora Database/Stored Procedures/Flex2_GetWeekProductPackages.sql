USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetWeekProductPackages]    Script Date: 11/19/2007 13:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Flex2_GetWeekProductPackages] 
	@ComCode varchar(64),
	@FlpId int,
	@PkgId varchar(64)
AS

SELECT 
	*
FROM 
	FlexWeekProductPackageView
WHERE
	FlexWeekProductPackageView.ComCode = @ComCode
	AND FlexWeekProductPackageView.FlpId = @FlpId
	AND (@PkgId IS NULL OR FlexWeekProductPackageView.PkgId = @PkgId)
ORDER BY
	FlexWeekProductPackageView.PkgCode










