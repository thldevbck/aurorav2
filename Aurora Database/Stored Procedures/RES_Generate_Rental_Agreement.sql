set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go







ALTER	Procedure [dbo].[RES_Generate_Rental_Agreement] 
	@psRntId	VARCHAR(64)
AS

/******************************************************************************
**		File: 
**		Name: RES_Generate_Rental_Agreement.sql
**		Desc: 
**
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Martin Thompson
**		Date: 24 May 2002
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
SET NOCOUNT ON

--	Declaration Notes  --
--	Global (scope is Connection) cursors are given a GUID (Using NewID() but taken dashes out)
--	For the purpose of cursors using dynamic SQL 
--  Rental			Cursor Name is c4E7310733551437E9B6E22102743D1FA
--	Booked Products	Cursor Name is c733398BAD118443FBEF79B7B6E599011

DECLARE	@ABNNumber		varchar(64),
		@sLicensee		varchar(64),
		@CtyCode		varchar(12),
		@RentalStatus	varchar(64),
		@BooNum			varchar(64),
		@RntVoucNum		varchar(256),
		@RntNum			varchar(256),
		@Agncode		varchar(64),
		@AgnName		varchar(256),
		@sSQL			varchar(2000),
		@RntBooId		varchar(64),
		@sBpdList		varchar(2000),
		@ReturnError	varchar(2000),
		@sBpdAmt	varchar(64),
		@BpdId			varchar(2000),
		@BpdCodTypeId	varchar(64),
--20080729 JL return UviKey (company)
		@UviKey			varchar(64)

SET @BpdId = NULL
-- Validate
IF (	SELECT	COUNT(RntId)
		 FROM	Rental WITH (NOLOCK) 
		 WHERE	RntId=@psRntId) = 0
BEGIN
	SELECT 	'<error>No Records Found!</error>'
	RETURN
END

SELECT 	'<AgreementHeader>'

DECLARE @sAgnDebtStatus	VARCHAR(64)
DECLARE @bIsDirect		BIT		
-- KX Changes :RKS
DECLARE @sComCode		VARCHAR(64)	

SELECT	@CtyCode		= dbo.getCountryForLocation(RntCkoLocCode,NULL,NULL),  -- KX Changes :RKS
		@sComCode		= (SELECT LocComCode FROM dbo.Location (NOLOCK) WHERE LocCode = RntCkoLocCode), -- KX Changes :RKS
		@RntVoucNum		= RntVoucNum,
		@RentalStatus	= dbo.GEN_GetCodDesc('',42,RntStatus),
		@BooNum			= BooNum,
		@RntBooId		= RntBooId,
		@RntNum			= RntNum,
		@AgnCode		= AgnCode,
		@AgnName		= CASE 	WHEN AgnIsMisc=1
									THEN BooMiscAgentName
								ELSE AgnName
						  END,
		@bIsDirect = ISNULL(AtyIsDirect,0)
FROM	--Towncity 	WITH (NOLOCK),
		Rental 		WITH (NOLOCK),
		Booking 	WITH (NOLOCK),
		Agent 		WITH (NOLOCK),
		AgentType WITH(NOLOCK)
WHERE	--TctCode = RntCkoLocCode  AND	
RntId = @psRntId
 AND	BooId = RntBooId
 AND	BooAgnId = AgnId
 AND 	AgnAtyId = AtyId 

SET @CtyCode = ISNULL(@CtyCode,'')

DECLARE @sTypeCode VARCHAR(24)

SELECT 	TOP 1 
		@BpdId 		= BpdId, 
		@sTypeCode 	= ISNULL(DBO.GetCodCode(BpdCodTypeId),''),
		@BpdCodTypeId = BpdCodtypeId
 FROM 	BookedProduct WITH(NOLOCK)
 WHERE 	BpdRntId	 	= @psRntId
 AND   	BpdPrdIsVehicle = 1 
 AND   	BpdStatus NOT IN ('XX','XN')
ORDER BY BpdCkiWhen DESC

--20080729 JL get UviKey
SELECT	@sLicensee	= Uvivalue, 
		@UviKey = UviKey
FROM	UniversalInfo WITH (NOLOCK)
WHERE	UviKey = 'COMPNAME_' + @sComCode + '_' + @CtyCode

SELECT	'<Licensee>' + ISNULL(@sLicensee,'') + '</Licensee>'

--20080729 JL return UviKey
select '<Company>' + ISNULL(@UviKey,'') + '</Company>'

SELECT	@ABNNumber = Uvivalue
 FROM	UniversalInfo WITH (NOLOCK)
 WHERE	UviKey='ABNNUMBER_' + @sComCode + '_' + @CtyCode

SELECT	'<ABNNumber>' + ISNULL(@ABNNumber,'') + '</ABNNumber>'

SELECT	'<ContractNum>' + ISNULL(@BooNum,'') + ' / ' + ISNULL(@RntNum,'') + '</ContractNum>'

IF @sTypeCode = 'XTO'
	SET @RentalStatus=@RentalStatus + ' **'

SELECT	'<RentalStatus>'+ ISNULL(@RentalStatus,'') + '</RentalStatus>'

SELECT	'<Date>' + DateName(dd,CURRENT_TIMESTAMP) + ' ' + DateName(month,CURRENT_TIMESTAMP) + ' ' + DateName(yy,CURRENT_TIMESTAMP) + '</Date>'

SELECT	'<VoucherNum>' + ISNULL(@RntVoucNum,'') + '</VoucherNum>'

SELECT	'<Agent>' + ISNULL(@AgnCode,'') + '</Agent>'

SELECT	'<AgentName>' + ISNULL(REPLACE(@AgnName,'&','&amp;'),'') + '</AgentName>'

SELECT 	'</AgreementHeader>'

--  Hirer Details Start --
SELECT 	'<HirerDetails>'

SELECT	ISNULL(CusTitle, '') + ' ' + ISNULL(CusFirstName, '') + ' ' + ISNULL(CusLastName, '') 	AS Name,
		--dbo.RES_GetAddressParts('Customer',CusId,'physical',4847,'[NL]') AS Address,
		dbo.RES_GetAddressParts('Customer', CusId, 'physical', 1007, '[NL]') 					AS Address,
		dbo.res_GetPartAddress('Customer', CusId, 'physical', 0, 0, 0, 1) 						AS Country,
		dbo.res_getphone('Customer', CusId, 'Day', '') 											AS Phone,
		dbo.res_getphone('Customer', CusId, 'Mobile', '') 										AS Mobile
 FROM	Traveller 	WITH (NOLOCK),
		Customer 	WITH (NOLOCK)
 WHERE	TrvRntId	= @psRntId
 AND	TrvHirer	= 1
 AND	CusId		= TrvCusId
-- ORDER BY TrvIsPrimaryHirer DESC
FOR	XML	AUTO,Elements

SELECT 	'</HirerDetails>'
--  Hirer Details End  --
 
--  Driver Details Start --
SELECT 	'<DriverDetails>'

SELECT	ISNULL(CusTitle, '') + ' ' + ISNULL(CusFirstName, '') + ' ' + ISNULL(CusLastName, '')	AS Name,
		ISNULL(cusDOB, '') 																		AS DOB,
		ISNULL(CusIssuingAuthority,'') 															AS Type,
		CusDriversLicenceNum 																	AS LicenceNo,
		ISNULL(CONVERT(varchar,CusLicenceExpiryDate,103),'') 									AS LicExpiryDate
 FROM	Traveller AS DriverDetails 	WITH (NOLOCK),
		Customer 					WITH (NOLOCK)
 WHERE	TrvRntId	= @psRntId
 AND 	TrvDriver 	= 1
 AND	CusId 		= TrvCusId
FOR	XML	AUTO,Elements

SELECT '</DriverDetails>'
--  Driver Details End --

--  Travel Dates Start  --
SELECT 	'<TravelDates>'

SELECT	
--	dbo.getdatetime(RntCkoWhen, 0) 																		AS TDFrom,
--	dbo.getdatetime(RntCkiWhen, 0) 																		AS TDTo,
--		LEFT(DateName(dw,RntCkoWhen),3) + ' ' + DateName(dd,RntCkoWhen) + ' ' + DateName(month,RntCkoWhen) + ' ' + DateName(yy,RntCkoWhen)			AS TDFrom,
--		LEFT(DateName(dw,RntCkiWhen),3) +  ' ' + DateName(dd,RntCkiWhen) + ' ' + DateName(m,RntCkiWhen) + ' ' + DateName(yy,RntCkiWhen)			AS TDTo,
 LEFT(DateName(dw,RntCkoWhen),3) + ' ' + dbo.getdatetime(RntCkoWhen, 0) 																		AS TDFrom,
 LEFT(DateName(dw,RntCkiWhen),3) + ' ' + dbo.getdatetime(RntCkiWhen, 0) 																		AS TDTo,
		dbo.res_GetPartAddress('Location', RntCkiLocCode, 'physical', 1, 1, 0, 0) 							AS Address,
		-- KX Changes: RKS
		ISNULL((SELECT TctName FROM Towncity WITH (NOLOCK), Location WHERE TctCode =LocTctCode AND LocCode = RntCkoLocCode), '') 				AS LocFrom,
		-- KX Changes: RKS
		ISNULL((SELECT TctName FROM Towncity WITH (NOLOCK), Location WHERE TctCode =LocTctCode AND LocCode = RntCkiLocCode), '') 				AS LocTo,
		ISNULL(convert(varchar, RntNumofHirePeriods), '') + ' ' + ISNULL(DBO.getCodCode(RntCodUomId), '') 	AS HIRE,
		dbo.res_getphone('Location', RntCkiLocCode, 'Phone', '') 											AS Tel
 FROM	Rental 	WITH (NOLOCK)
 WHERE	RntId	= @psRntId
FOR	XML	AUTO,Elements

SELECT '</TravelDates>'
--  Travel Dates End  --

--  Vehicle Details Start  --
SELECT '<VehicleDetails>'
/*
SELECT	ISNULL(BpdRego, '')															AS Reg,
		ISNULL(BpdUnitNum, '') 														AS UnitNo,
		ISNULL(PrdName + ' ', '') + ISNULL(TypCode + ' ', '') + ISNULL(ClaCode, '') AS Vehicle,
		ISNULL(BpdOddometerOut, 0) 													AS OdometerOut,
		ISNULL(BpdOddometerIn, 0) 													AS OdometerIn
FROM	BookedProduct 	WITH (NOLOCK),
		SaleableProduct WITH (NOLOCK),
		Product 		WITH (NOLOCK),
		Type 			WITH (NOLOCK),
		Class 			WITH (NOLOCK)
WHERE	PrdId 			= SapPrdId
 AND	SapId 			= BpdSapId
 AND	TypId 			= PrdTypId
 AND	ClaId 			= typClaId
 AND 	BpdRntId 		= @psRntId 
 AND	PrdIsVehicle = 1
 AND	BpdStatus NOT IN ('XX','XN')
 AND	ISNULL(DBO.getCodCode(BpdCodTypeId), '') <> 'EXT' --exclude EXT vehicle modified by James on 28/03/2003
ORDER BY BpdCkoWhen 
FOR	XML	AUTO,Elements
*/


SELECT	'' + FASS.RegistrationNumber																	AS Reg,
		'' + FASS.UnitNumber																			AS UnitNo,
		'' + 
		CASE 
			WHEN EXISTS(SELECT PrdId 
							FROM 	Product WITH(NOLOCK)
							WHERE	PrdShortName = FMOD.FleetModelCode
							AND		PrdIsActive = 1)
				THEN	(SELECT DISTINCT PrdShortName + ' ' + TypCode + ' ' +  ClaCode
								FROM 	Product WITH(NOLOCK), Type WITH(NOLOCK), Class WITH(NOLOCK)
								WHERE	PrdShortName = FMOD.FleetModelCode
								AND		PrdTypId = TypId
								AND		TypClaId = ClaId
								AND		PrdIsActive = 1)
				ELSE	FMOD.FleetModelCode
		END																								AS Vehicle,
		'' + BookedProduct.StartOdometer																AS OdometerOut,
		'' + CASE WHEN BookedProduct.Completed = 0
				THEN ''
				ELSE CAST(BookedProduct.EndOdometer AS VARCHAR)
			END																							AS	OdometerIn 
	FROM	AimsProd.dbo.FleetActivity BookedProduct WITH(NOLOCK), 
			AimsProd.dbo.FleetAsset FASS WITH(NOLOCK), 
			AimsProd.dbo.FleetModel FMOD WITH(NOLOCK)
	WHERE	BookedProduct.FleetAssetId = FASS.FleetAssetId
	AND		FASS.FleetModelId = FMOD.FleetModelId
	AND		(ExternalRef = @BooNum + '/' + @RntNum OR ExternalRef LIKE @BooNum + '/' + @RntNum + 'x%')
	ORDER BY BookedProduct.StartDateTime
	FOR	XML	AUTO,Elements

SELECT '</VehicleDetails>'
--  Vehicle Details End  --

-- Rental Details Start  --
--Added on 08/01/2002 by James Liu
-------------------------------------------------
DECLARE	@sExBpds	VARCHAR(5000),
		@sRevBpdids VARCHAR(5000),
		@sNonRevAmt MONEY,
		@sRevAmt	MONEY,
		@sXfrmAmt   MONEY,
		@sXtoAmt    MONEY,
		@ssAmt 		MONEY

DECLARE @tt TABLE(	
				BpdDate 	VARCHAR(64),
				Product 	VARCHAR(64),
				Description VARCHAR(100),
				HirePeriod 	VARCHAR(100),
				Qty 		INT,
				Currency 	VARCHAR(64),
				Amount 		VARCHAR(64),
				Country		VARCHAR(12)
				
			)
--------------------------------------------------
--get BpdId for exchange, modified on 07/01/2003
DECLARE @iCount INT

SELECT 	@iCount = COUNT(BpdId)
 FROM 	BookedProduct WITH(NOLOCK)
 WHERE	BpdRntId 	= 	@psRntId
 AND 	BpdStatus 	<> 	'XX'
 AND	Bpdstatus 	<> 	'XN'
 AND	(ISNULL(DBO.GetCodCode(BpdCodTypeId), '') = 'XFRM' OR ISNULL(DBO.GetCodCode(BpdCodTypeId), '') = 'XTO')


IF @iCount > 0
BEGIN
	SELECT '<isExchange>true</isExchange>'
END
ELSE
	SELECT '<isExchange>false</isExchange>'


SET @sbpdlist = null
SELECT @sbpdlist  = case when @sbpdlist is null then bpdid else @sbpdlist + ',' + bpdid end
	from bookedproduct with (nolock)
	where bpdrntid = @psRntId and bpdstatus not in ('XX','XN')
and bpdprdisvehicle = 1
Order By bpdckowhen, BpdCkoLocCode

select @sbpdlist  = case when @sbpdlist is null then bpdid else @sbpdlist + ',' + bpdid end
from bookedproduct with (nolock), rental with (nolock) , saleableproduct with (nolock), product with (nolock), type with (nolock), class with (nolock)
where bpdsapid = sapid and sapprdid = prdid and prdtypid = typid and typclaid = claid and
bpdrntid = rntid and rntid = @psRntId and bpdstatus not in ('XX','XN')
and prdisvehicle = 0
Order By bpdckowhen desc , ClaOrder, TypOrder, rntckoloccode, PrdShortName, BookedProduct.AddDateTime


DECLARE	@TmpList 		VARCHAR(5000), 
		@sBpdIdCurrent 	VARCHAR(64), 
		@xtoDispReturn 	MONEY

SET @TmpList = @sBpdList

WHILE	@TmpList IS NOT NULL AND LEN(@TmpList) > 0
BEGIN
	SET @sBpdIdCurrent = NULL
	EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @TmpList,
			@psListEntry			= @sBpdIdCurrent	OUTPUT,
			@psUpdatedList			= @TmpList			OUTPUT
		
	SET @xtoDispReturn  = NULL
	EXEC	RES_GetChargeIfXTO 
			@BpdId 			= @sBpdIdCurrent,
			@sXtoDisplay 	= @xtoDispReturn OUTPUT
				
	INSERT 	INTO @tt
		SELECT 	CONVERT(CHAR(10),BookedProduct.AddDateTime,103), 
				PrdShortName, 
				PrdName,
				ISNULL(CAST(BpdHirePeriod AS VARCHAR), '0') + ' ' + ISNULL(DBO.getCodCode(BpdCodUomId), ''), 
				CAST(BpdQty AS INT),
				ISNULL(dbo.GetCodCode(BpdCodCurrId), ''),
				CASE WHEN BookedProduct.BpdIsCuscharge = 0 OR (DBO.GetCodCode(BpdCodTypeId) = 'XFRM' and @bIsDirect = 0)
						THEN 'Prepaid'
					 WHEN  BookedProduct.BpdIsCuscharge = 1 AND (DBO.GetCodCode(BpdCodTypeId) = 'XFRM' OR DBO.GetCodCode(BpdCodTypeId) = 'XTO')
						THEN ISNULL(CONVERT(VARCHAR, @xtoDispReturn), '')
					 WHEN BookedProduct.BpdIsCuscharge = 1 AND (DBO.GetCodCode(BpdCodTypeId) <> 'XFRM' AND DBO.GetCodCode(BpdCodTypeId) <> 'XTO')	
						THEN ISNULL(CONVERT(varchar, (ISNULL(BpdGrossAmt, 0) - ISNULL(BpdAgnDisAmt, 0))), '''')
				END,
				dbo.getCountryForLocation(BpdCkoLocCode,NULL,NULL)	
		 FROM 	BookedProduct 	WITH (NOLOCK),
			    SaleableProduct WITH (NOLOCK), 
				Product 		WITH (NOLOCK)
		 WHERE	BpdSapId 	= SapId
		 AND	SapprdId 	= PrdId     
		 AND	BpdId 		= @sBpdIdCurrent
END

SELECT 	'<RentalDetails>'
SELECT 	* FROM @tt AS Details 
FOR XML AUTO,ELEMENTS
SELECT 	'</RentalDetails>'
--modifying end
---------------------------------------------------------------------------------

-- Payment/ Refund Details  --
SELECT '<PaymentRefund>'

SELECT	CONVERT(VARCHAR, PmtDateTime, 103) 												AS PmtDate,
		PtmName 																		AS ModeOfPayment,
		CASE RptIsRefund
			WHEN 0 THEN ISNULL((SELECT	Top 1 PdtAccName
						FROM	PaymentDetail WITH (NOLOCK)
						WHERE	PdtPmtId=PmtId),'')
			ELSE	   ISNULL((SELECT	Top 1 ISNULL(PdtAccName, '') + ' ' + '**REFUND'
						FROM	PaymentDetail WITH (NOLOCK)
						WHERE	PdtPmtId=PmtId), '')
		END 																			AS CardHolderName,
		dbo.GetCodCode(RptchargeCurrId) 												AS Currency,
		RptChargeAmt 																	AS Amount
 INTO 	#TmpTablePmt
 FROM	RentalPayment 	WITH (NOLOCK),
		Payment 		WITH (NOLOCK),
		PaymentMethod 	WITH (NOLOCK)
WHERE	RptType		<>'B'
 AND	PmtId		= RptPmtId
 AND	PmtPtmId	= PtmId
 AND	RptRntId	= @psRntId

ORDER BY
		PmtDateTime,
		PmtPtmId

SELECT * FROM #TmpTablePmt AS PaymentMethod
FOR	XML	AUTO,Elements
delete from #TmpTablePmt


SELECT 	'</PaymentRefund>'

--  Vehicle Security / Bond  --
SELECT 	'<VehicleSecurity>'

SELECT	CONVERT(VARCHAR, PmtDateTime, 103) 												AS PmtDate,
		PtmName 																		AS ModeOfPayment,
		CASE RptIsRefund
			WHEN 0 THEN ISNULL((SELECT	Top 1 PdtAccName
						FROM	PaymentDetail WITH (NOLOCK)
						WHERE	PdtPmtId=PmtId),'')
			ELSE	   ISNULL((SELECT	Top 1 ISNULL(PdtAccName,'') + ' ' + '**REFUND'
						FROM	PaymentDetail WITH (NOLOCK)
						WHERE	PdtPmtId=PmtId),'')
		END 																			AS CardHolderName,
		dbo.GetCodCode(RptchargeCurrId) 												AS Currency,
		RptChargeAmt - IsNull(PmtSurLocAmt,0)											AS Amount
 INTO 	#TmpTableBond
 FROM	RentalPayment 		WITH (NOLOCK),
		Payment AS Payment 	WITH (NOLOCK),
		PaymentMethod 		WITH (NOLOCK)
 WHERE	RptType		= 'B'
 AND	PmtId		= RptPmtId
 AND	PmtPtmId	= PtmId
 AND	RptRntId	= @psRntId 

ORDER BY
		PmtDateTime,
		PmtPtmId

SELECT * FROM #TmpTableBond AS PaymentMethod
FOR	XML	AUTO,Elements
delete from #TmpTableBond

SELECT '</VehicleSecurity>'

-- Additional Text  --
SELECT	@sBpdAmt = CONVERT(VARCHAR, BpdGrossAmt - BpdAgnDisAmt)
 FROM	BookedProduct WITH (NOLOCK)
 WHERE	BpdId = @BpdId

SET @sBpdAmt=ISNULL(@sBpdAmt,'')

DECLARE @sRntStatus VARCHAR(12)
SELECT 	@sRntStatus = RntStatus 
 FROM 	Rental (nolock)
 WHERE 	RntId = @psRntId

/*****************************************************************************************/
--modified for PCO by James on 28/04/2003

DECLARE @sCurrent 			VARCHAR(64), 
		@sIncludeAll 		INT, 
		@sInclude 			INT, 
		@sAndPattern 		VARCHAR(64), 
		@sNonAnd			VARCHAR(64),
		@tmp 				VARCHAR(500),
		@sRanIdList			VARCHAR(2000),
		@sRanListExclude 	VARCHAR(2000),
		@sRanIdListExclude	VARCHAR(2000),
		@sNonAndList		VARCHAR(2000)

DECLARE @sList 	VARCHAR(5000) -- booked products in the rental

SELECT 	@sList = ISNULL(@sList + ',','') + Product FROM @tt

SET @sRanIdList 		= NULL
SET @sRanListExclude 	= NULL
SET @sRanIdListExclude 	= NULL
SET @sNonAndList		= NULL
--Select records from RentalAgreement which have AND in RanPrdShortNameList column

DECLARE @sId 	VARCHAR(64), 
		@sText 	VARCHAR(2000)

-- For each of record in @tttt 
DECLARE cur_Ran CURSOR FAST_FORWARD LOCAL FOR
	SELECT 	RanId, 
			RanPrdShortNameList
	 FROM 	rentalagreement WITH(NOLOCK)
	 WHERE 	RanCtyCode = @ctyCode and ISNULL(RanPrdShortNameList,'') like '%'+ ' AND ' + '%'
			-- KX Changes :RKS
			AND (RanComCode IS NULL OR RanComCode = @sComCode)
OPEN cur_Ran
	FETCH NEXT FROM cur_Ran INTO @sId, @sText
	WHILE @@FETCH_STATUS = 0
	BEGIN
		WHILE @sText IS NOT NULL AND LEN(@sText) > 0
  		BEGIN
   			SET @sCurrent = null
			SET @sAndPattern = NULL

			-- look at the text which has AND
   			EXECUTE GEN_GetNextEntryAndRemoveFromList
    				@psCommaSeparatedList 	= @sText,
    				@psListEntry  			= @sCurrent	OUTPUT,
    				@psUpdatedList  		= @sText 	OUTPUT

			SELECT 	@sAndPattern = @sCurrent 
			 WHERE 	@sCurrent LIKE '% AND %'
			
			SELECT @sNonAnd = @sCurrent 
			 WHERE @sCurrent NOT LIKE '% AND %'
			
			SELECT @sNonAndList = ISNULL(@sNonAndList + ',', '') + @sNonAnd
			
			-- Check if all of Prd in the AND phrase are in product list
			SET @sAndPattern = replace(@sAndPattern,' AND ',',')
			IF ISNULL(@sAndPattern, '') <> ''
			BEGIN
				SET @sIncludeAll = 1
				SET @tmp = @sAndPattern
				
				WHILE @sAndPattern IS NOT NULL AND LEN(@sAndPattern) > 0
  				BEGIN
   					SET @sCurrent = NULL
   					EXECUTE GEN_GetNextEntryAndRemoveFromList
    						@psCommaSeparatedList 	= @sAndPattern,
    						@psListEntry  			= @sCurrent		OUTPUT,
    						@psUpdatedList  		= @sAndPattern 	OUTPUT

					--check if each in the list, if yes = 1, else = 0
					SELECT @sInclude = CASE WHEN CHARINDEX(@sCurrent, @sList) > 0 THEN 1 ELSE 0 END

					--check if all in the list, if yes = 1, else = 0
					SELECT @sIncludeAll = @sIncludeAll * @sInclude
				END
				-- if all in the list
				IF @sIncludeAll > 0
				BEGIN
					-- match AND phrase, display AND related combined record 
					SELECT 	@sRanIdList 		= ISNULL(@sRanIdList + ',', '') 		+ @sId
					-- match AND phrase, not display AND related individual record
					SELECT 	@sRanListExclude 	= ISNULL(@sRanListExclude + ',', '')	+ @tmp
				END
				ELSE --not match, not display AND related combined record
					SELECT @sRanIdListExclude = ISNULL(@sRanIdListExclude + ',' ,'') 	+ @sId
			END
		END
	FETCH NEXT FROM cur_Ran INTO @sId, @sText
	END
Destroy_Cursor:

--modified end for PCO
/********************************************************************************************************/

--20080729 JL determine the first rental text
Declare @firstRanId varchar(64) 
set @firstRanId = ''

SELECT 	'<RntText>'
IF @sRntStatus = 'CI'
BEGIN
--20080729 JL determine the first rental text
	SELECT 	 top 1 @firstRanId = RanId
		 FROM	RentalAgreement AS RText	WITH (NOLOCK),
				SaleableProduct 			WITH (NOLOCK),
				BookedProduct 				WITH (NOLOCK),
				Product 					WITH (NOLOCK)
		 WHERE	BpdSapId	= SapId
		 AND	SapPrdId	= PrdId
		 AND	BpdRntId	= @psRntId
		 AND	RanCtyCode	= @CtyCode
		 AND 	BpdStatus NOT IN ('XX','XN')
		 --AND		(CHARINDEX(PrdshortName,RanPrdShortNameList) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
		 AND	((CHARINDEX(', '+PrdshortName+',',','+RanPrdShortNameList+',') > 0 
			 		AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 
					AND (CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0 OR CHARINDEX(PrdshortName, ISNULL(@sNonAndList,'')) > 0)) 
				OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
		 AND 	RanStatusCI = 1
		 -- KX Changes :RKS
		AND (RanComCode IS NULL OR RanComCode = @sComCode)
		and RanCusSign = 1
		ORDER BY RanOrder


	SELECT 	Distinct RanOrder,
			RanHeader 																					AS Header,
			REPLACE(RanText,'[Gross]',('$' + 
			ISNULL(CONVERT(varchar,(	SELECT	Sum(BpdGrossAmt - BpdAgnDisAmt)
						 				 FROM	BookedProduct Bpd1 WITH (NOLOCK),
												SaleableProduct sap1 WITH (NOLOCK),
												Product	Prd1 WITH (NOLOCK)
										 WHERE	bpd1.BpdSapId = sap1.SapId
									 	 AND	Sap1.SapPrdId = Prd1.PrdId
								 		 AND	Bpd1.BpdRntId = BookedProduct.bpdRntId
								 		 AND	CHARINDEX(Prd1.PrdShortName, RanPrdShortNameList) > 0)
								)
			,''))) 																						AS RanText,
			CASE RanCusSign
			 	WHEN 1 THEN 'Hirer''s Signature'
				ELSE ''
			END 																						AS HireSign,
			CASE RanLocSign
				WHEN 1 THEN 'Branch Signature'
				ELSE ''
			END 																						AS BranchSign,
		--20080729 JL determine the first rental text
			CASE RanId
			 	WHEN @firstRanId THEN 1
				ELSE 0
			END			AS optIn

	 FROM	RentalAgreement AS RText	WITH (NOLOCK),
			SaleableProduct 			WITH (NOLOCK),
			BookedProduct 				WITH (NOLOCK),
			Product 					WITH (NOLOCK)
	 WHERE	BpdSapId	= SapId
	 AND	SapPrdId	= PrdId
	 AND	BpdRntId	= @psRntId
	 AND	RanCtyCode	= @CtyCode
	 AND 	BpdStatus NOT IN ('XX','XN')
	 --AND		(CHARINDEX(PrdshortName,RanPrdShortNameList) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
	 AND	((CHARINDEX(', '+PrdshortName+',',','+RanPrdShortNameList+',') > 0 
			 	AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 
				AND (CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0 OR CHARINDEX(PrdshortName, ISNULL(@sNonAndList,'')) > 0)) 
			OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
	 AND 	RanStatusCI = 1
	 -- KX Changes :RKS
	AND (RanComCode IS NULL OR RanComCode = @sComCode)
	ORDER BY RanOrder
	FOR	XML	AUTO,Elements
END

IF @sRntStatus = 'CO' OR @sRntStatus = 'KK'
BEGIN
	--20080729 JL determine the first rental text
	set @firstRanId = ''

	SELECT	top 1 @firstRanId = RanId
	 FROM	RentalAgreement AS RText WITH (NOLOCK),
			SaleableProduct WITH (NOLOCK),
			BookedProduct WITH (NOLOCK),
			Product WITH (NOLOCK)
	 WHERE	BpdSapId=SapId
	 AND	SapPrdId=PrdId
	 AND	BpdRntId=@psRntId
	 AND	RanCtyCode=@CtyCode
	 AND 	BpdStatus NOT IN ('XX','XN')
	 --AND	(CHARINDEX(PrdshortName,RanPrdShortNameList)>0 OR ISNULL(RanPrdShortNameList,'')='')
	 --AND	((CHARINDEX(PrdshortName,RanPrdShortNameList) > 0 AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 AND CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0) OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
	 AND	((CHARINDEX(','+PrdshortName+',',','+RanPrdShortNameList+',') > 0 
			 	AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 
				AND (CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0 OR CHARINDEX(PrdshortName, ISNULL(@sNonAndList,'')) > 0)) 
			OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '') 
	AND 	RanStatusCO = 1	
	-- KX Changes :RKS
	AND (RanComCode IS NULL OR RanComCode = @sComCode)	
	ORDER BY RanOrder


	SELECT	DISTINCT RanOrder,
			RanHeader AS Header,
			REPLACE(RanText,'[Gross]',('$' + 
			ISNULL(CONVERT(varchar,(	SELECT	Sum(BpdGrossAmt-BpdAgnDisAmt)
					 					 FROM	BookedProduct Bpd1 WITH (NOLOCK),
												SaleableProduct sap1 WITH (NOLOCK),
												Product	Prd1 WITH (NOLOCK)
										 WHERE	bpd1.BpdSapId=sap1.SapId
								 		 AND	Sap1.SapPrdId=Prd1.PrdId
								 		 AND	Bpd1.BpdRntId=BookedProduct.bpdRntId
										 AND 	Bpd1.BpdStatus NOT IN ('XX','XN')
								 		 AND	CHARINDEX(Prd1.PrdShortName,RanPrdShortNameList)>0)
								)
			,''))) AS RanText,
			CASE RanCusSign
			 	WHEN 1 THEN 'Hirer''s Signature'
				ELSE ''
			END AS HireSign,
			CASE RanLocSign
				WHEN 1 THEN 'Branch Signature'
				ELSE ''
			END AS BranchSign,
			--20080729 JL determine the first rental text
			CASE RanId
			 	WHEN @firstRanId THEN 1
				ELSE 0
			END			AS optIn

	 FROM	RentalAgreement AS RText WITH (NOLOCK),
			SaleableProduct WITH (NOLOCK),
			BookedProduct WITH (NOLOCK),
			Product WITH (NOLOCK)
	 WHERE	BpdSapId=SapId
	 AND	SapPrdId=PrdId
	 AND	BpdRntId=@psRntId
	 AND	RanCtyCode=@CtyCode
	 AND 	BpdStatus NOT IN ('XX','XN')
	 --AND	(CHARINDEX(PrdshortName,RanPrdShortNameList)>0 OR ISNULL(RanPrdShortNameList,'')='')
	 --AND	((CHARINDEX(PrdshortName,RanPrdShortNameList) > 0 AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 AND CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0) OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '')
	 AND	((CHARINDEX(','+PrdshortName+',',','+RanPrdShortNameList+',') > 0 
			 	AND CHARINDEX(PrdshortName,isnull(@sRanListExclude,''))=0 
				AND (CHARINDEX(RanId, ISNULL(@sRanIdListExclude,''))=0 OR CHARINDEX(PrdshortName, ISNULL(@sNonAndList,'')) > 0)) 
			OR CHARINDEX(RanId, isnull(@sRanIdList,'')) > 0 OR ISNULL(RanPrdShortNameList,'') = '') 
	AND 	RanStatusCO = 1	
	-- KX Changes :RKS
	AND (RanComCode IS NULL OR RanComCode = @sComCode)	
	ORDER BY RanOrder
	FOR	XML	AUTO,Elements
END
SELECT '</RntText>'
RETURN




