USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetFlexProducts]    Script Date: 02/23/2008 13:50:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Flex2_GetFlexProducts] 
	@ComCode varchar(64)
AS

SELECT 
	FlexProduct.*,
	Product.PrdShortName
FROM 
	FlexProduct
	INNER JOIN Product ON FlexProduct.FxpPrdId = Product.PrdId
WHERE
	FxpComCode = @ComCode
