CREATE        Procedure [dbo].[paym_managePaymentDetail]
	@sPdtId				VARCHAR	(64) 	= NULL,
	@sPdtPmtId			VARCHAR	(64) 	= NULL,
	@sPdtBankNum		VARCHAR	(24) 	= NULL,
	@sPdtBranchNum		VARCHAR	(24) 	= NULL,
	@sPdtChequeNum		VARCHAR	(24) 	= NULL,
	--start REV:MIA increase size from 64 to 256
	@sPdtAccnum			VARCHAR	(256) 	= NULL,
	--end REV:MIA increase size from 64 to 256
	@sPdtAmount			VARCHAR	(18) 	= NULL,
	@sPdtAccName		VARCHAR	(256) 	= NULL,
	@sPdtApproval		VARCHAR	(256) 	= NULL,
	@sPdtExpiry			VARCHAR	(16) 	= NULL,
	@sDpsAuthCode		VARCHAR	(256)	= NULL ,
	@sDpsMerchRef		VARCHAR	(64)	= NULL ,
	@sDpsTxnRef			VARCHAR	(256)	= NULL ,
	@IntegrityNo		INT  			= 0,
	@AddUsrId			VARCHAR (64) 	= '',
	@ModUsrId			VARCHAR (64) 	= '',
	@AddPrgmName		VARCHAR (64) 	= ''
As
	DECLARE			@sPmtReversalofPmtId			VARCHAR (64),
					@sPdtDpsBrdCode					VARCHAR	(1)

	SELECT @sPmtReversalofPmtId = PmtReversalofPmtId FROM dbo.payment (nolock) WHERE PmtId = @sPdtPmtId


	IF ISNULL(@sPmtReversalofPmtId,'')<>''
		SELECT @sPdtDpsBrdCode = PdtDpsBrdCode FROM dbo.paymentDetail (nolock) WHERE PdtPmtId = @sPmtReversalofPmtId
	ELSE
		SET @sPdtDpsBrdCode = NULL

	IF (NOT EXISTS (SELECT PdtId FROM PaymentDetail  WITH (NOLOCK) WHERE PdtId = @sPdtId))
		BEGIN
			INSERT INTO
				PaymentDetail  WITH (ROWLOCK) (
							Pdtid,
							PdtPmtId,
							PdtBankNum,
							PdtBranchNum,
							PdtChequeNum,
							PdtAccNum,
							PdtAmt,
							PdtAccName,
							PdtApprovalRef,
							PdtExpiryDate,
							PdtDpsAuthCode ,
							PdtDpsMerchRef ,
							PdtDpsTxnRef ,
							PdtDpsBrdCode,
							IntegrityNo,
							AddUsrId,
							ModUsrId,
							AddDateTime,
							ModDateTime,
							AddPrgmName )

				VALUES (@sPdtId	,
						@sPdtPmtId ,
						@sPdtBankNum ,
						@sPdtBranchNum ,
						@sPdtChequeNum ,
						@sPdtAccnum ,
						@sPdtAmount ,
						@sPdtAccName ,
						@sPdtApproval ,
						@sPdtExpiry	,
						@sDpsAuthCode ,
						@sDpsMerchRef ,
						@sDpsTxnRef ,
						@sPdtDpsBrdCode,
						@IntegrityNo + 1 ,
						@AddUsrId ,
						@ModUsrId ,
						CURRENT_TIMESTAMP ,
						CURRENT_TIMESTAMP ,
						@AddPrgmName  )
		END
	ELSE
		BEGIN

			DECLARE @iNumber int
			DECLARE @sError varchar(254)


			SELECT @inumber =  IntegrityNo FROM  PaymentDetail  WITH (NOLOCK)
				WHERE PdtId = @sPdtId
			IF(@inumber = -10 )
			BEGIN
				SELECT @sError = smsdesc FROM messages  WITH (NOLOCK) WHERE smscode = 'GEN001'
				SELECT 'GEN001/ ' + @sError
       			RETURN
			END


			IF(@inumber <> @IntegrityNo )
			BEGIN
      			SELECT @sError = smsdesc FROM messages  WITH (NOLOCK) WHERE smscode = 'GEN002'
				SELECT 'GEN002/ ' + @sError
				RETURN
			END

			UPDATE PaymentDetail WITH (ROWLOCK)
				SET
					Pdtid 			= @sPdtId ,
					PdtPmtId 		= @sPdtPmtId ,
					PdtBankNum 		= @sPdtBankNum ,
					PdtBranchNum 	= @sPdtBranchNum ,
					PdtChequeNum 	= @sPdtChequeNum ,
					PdtAccNum 		= @sPdtAccnum ,
					PdtAmt 			= @sPdtAmount ,
					PdtAccName 		= @sPdtAccName ,
					PdtApprovalRef 	= @sPdtApproval ,
					PdtExpiryDate 	= @sPdtExpiry ,
					PdtDpsAuthCode 	= @sDpsAuthCode ,
					PdtDpsMerchRef 	= @sDpsMerchRef ,
					PdtDpsTxnRef 	= @sDpsTxnRef ,
					@sPdtDpsBrdCode	= @sPdtDpsBrdCode,
					IntegrityNo 	= @IntegrityNo + 1 ,
					ModDateTime 	= CURRENT_TIMESTAMP
				WHERE PdtId = @sPdtId
		END






GO
