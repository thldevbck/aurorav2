USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_FinalizeWeekExport]    Script Date: 11/19/2007 13:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[Flex2_FinalizeWeekExport] 
	@FwxId int,
	@FwxText ntext,
	@FwxContactsDescription ntext,
	@IntegrityNo int,
	@UsrId varchar(64)

AS

UPDATE FlexBookingWeekExport SET
	FlexBookingWeekExport.FwxStatus = 'sent',
	FlexBookingWeekExport.FwxText = @FwxText,
	FlexBookingWeekExport.FwxContactsDescription = @FwxContactsDescription,
	FlexBookingWeekExport.IntegrityNo = @IntegrityNo + 1,
	FlexBookingWeekExport.ModUsrId = @UsrId,
	FlexBookingWeekExport.ModDateTime = GETDATE()
FROM 
	FlexBookingWeekExport
WHERE
	FlexBookingWeekExport.FwxId = @FwxId
	AND FlexBookingWeekExport.IntegrityNo = @IntegrityNo
	AND FlexBookingWeekExport.FwxStatus = 'approved'

IF @@ROWCOUNT <= 0 RETURN

SELECT 
	* 
FROM
	FlexBookingWeekExport
WHERE
	FwxId = @FwxId











