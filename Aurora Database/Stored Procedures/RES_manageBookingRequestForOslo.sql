create                             PROCEDURE [dbo].[RES_manageBookingRequestForOslo]
	@xmlRequestIn	VARCHAR(4000),
	@ReturnError	VARCHAR(4000)	OUTPUT,
	@ReturnWarning	VARCHAR(4000)	OUTPUT,
	@ReturnXml	VARCHAR(4000)	OUTPUT
AS

SET NOCOUNT ON
--	++++++++++++++++++++++++++++++
--		LOCAL VARIABLES
--	++++++++++++++++++++++++++++++
DECLARE
	@sError			VARCHAR(500),
	@sWarn			VARCHAR(500),
	@sStatus		VARCHAR(64),
	@sBlrIdList		VARCHAR(4000),
	@iDocHandle		INTEGER,	--	HANDLE for OPENXML
	@sCountryCode		VARCHAR(64),
	@sCountryName		VARCHAR(128),
	@sXpath			VARCHAR(2000),
	@LoginUserID		VARCHAR(500),
	@ProgramName		VARCHAR(500),
	@BookedDate		DATETIME,
	@VhrID			VARCHAR(64),
	@xmlBkrID		VARCHAR(500),
	@xmlVhrID		VARCHAR(500),
	@xmlTemp		VARCHAR(4000),
	@intStartIndex		INTEGER,
	@sAgentCode		VARCHAR(200),
	@sRequestSourceId	VARCHAR(64),
	@sRentalId		VARCHAR(64),
	@sIgnoreWarningMsg	VARCHAR(16),
	@bIgnoreWarningMsg	BIT,
	@bDbStatus		BIT,
	@DbDateTime		DATETIME,
	@sButClick		VARCHAR(32),
	@dCkoTime		datetime,
	@dCkiTime		datetime,
--	++++++++++++++++++++++++++++++++++++++++
--		NEW field values
--	++++++++++++++++++++++++++++++++++++++++
	@IntegrityNo		INTEGER,
	@sBkrId			VARCHAR(64),
	@sBooId			VARCHAR(64),
	@sAgnId			VARCHAR(64),
	@sAgnCode		VARCHAR(64),
	@sConId			VARCHAR(64),
	@sConCode		VARCHAR(64),
	@sVhrAgnRef		VARCHAR(30),
	@sSapId			VARCHAR(64),
	@sPrdId			VARCHAR(64),
	@sBrdCode		VARCHAR(64),
	@sClaId			VARCHAR(64),
	@sPkgId			VARCHAR(64),
	@sPkgCode		VARCHAR(64),



	@sFtrId			VARCHAR(64),
	@sTypeId		VARCHAR(64),
	@sMiscName		VARCHAR(100),
	@sMiscContact		VARCHAR(100),
	@lNumVehicles		BIGINT,
	@lNumAdults		BIGINT,
	@lNumChildren		BIGINT,
	@lNumInfants		BIGINT,
	@sCkoLocation		VARCHAR(64),
	@stringCkoDate		VARCHAR(64),
	@dCkoDate		DATETIME,
	@sCkoDayPart		CHAR(2),
	@sCkiLocation		VARCHAR(64),
	@stringCkiDate		VARCHAR(64),
	@dCkiDate		DATETIME,
	@sCkiDayPart		CHAR(2),
	@lHirePeriod		BIGINT,
	@sHireUoM		VARCHAR(64),
	@sLastName		VARCHAR(128),
	@sFirstName		VARCHAR(128),
	@sTitle			VARCHAR(10),
	@sRequestSource		VARCHAR(64),
--	++++++++++++++++++++++++++++++++++++++++
--		OLD field values
--	++++++++++++++++++++++++++++++++++++++++
	@sBkrIdOld		VARCHAR(64),
	@sBooIdOld		VARCHAR(64),
	@sAgnIdOld		VARCHAR(64),
	@sAgnCodeOld		VARCHAR(64),
	@sConIdOld		VARCHAR(64),
	@sConCodeOld		VARCHAR(64),
	@sVhrAgnRefOld		VARCHAR(30),
	@sSapIdOld		VARCHAR(64),
	@sPrdIdOld		VARCHAR(64),
	@sBrdCodeOld		VARCHAR(64),
	@sClaIdOld		VARCHAR(64),
	@sPkgIdOld		VARCHAR(64),
	@sPkgCodeOld		VARCHAR(64),
	@sFtrIdOld		VARCHAR(64),
	@sTypeIdOld		VARCHAR(64),
	@sMiscNameOld		VARCHAR(100),
	@sMiscContactOld	VARCHAR(100),
	@lNumVehiclesOld	BIGINT,
	@lNumAdultsOld		BIGINT,
	@lNumChildrenOld	BIGINT,
	@lNumInfantsOld		BIGINT,
	@sCkoLocationOld	VARCHAR(64),
	@stringCkoDateOld	VARCHAR(64),
	@dCkoDateOld		DATETIME,
	@sCkoDayPartOld		CHAR(2),
	@sCkiLocationOld	VARCHAR(64),
	@stringCkiDateOld	VARCHAR(64),
	@dCkiDateOld		DATETIME,
	@sCkiDayPartOld		CHAR(2),
	@lHirePeriodOld		BIGINT,
	@sHireUoMOld		VARCHAR(64),
	@sLastNameOld		VARCHAR(128),
	@sFirstNameOld		VARCHAR(128),
	@sTitleOld		VARCHAR(10),
	@sRequestSourceOld	VARCHAR(64)


--	========================================
--	START INITIALISE SECTION
--	========================================


--	NEED bu lkcopy on
--SELECT	@DbName = DB_NAME()
--EXECUTE sp_dboption @DbName, 'select into/bul kcopy', 'ON'

--	COPY the request across
--	(preserve the original input)
	SELECT	@ReturnXml = @xmlRequestIn

--	LOAD the xml request into an [MS-]XML Document
--	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--	++							++
--	++	NOTE to the programmer:				++
--	++	-----------------------				++
--	++	If you are having trouble with "OPENXML"	++
--	++	and its implementation in SQL Server 2000,	++
--	++	look to SQL Server books online. In simplistic	++
--	++	terms it puts a rowset "veneer" over the top	++
--	++	of the XML document and this may be used as	++
--	++	the source of (say) the FROM clause of a	++
--	++	SELECT statement or similar.			++
--	++							++
--	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--	PREPARE xml doc to hold the document
EXEC sp_xml_preparedocument
		@iDocHandle	OUTPUT,
		@xmlRequestIn
IF(@@ERROR <> 0 ) GOTO	cleanup_and_leave	--	BYEeeee!

--	FETCH "system" information: USER ID and PROGRAM NAME
--		XPath defines all Aurora elements at any level
--		that own an AddModUserId element.
SELECT	@LoginUserID = AddModUserid
FROM	OPENXML(@iDocHandle, '//Aurora[AddModUserId]', 3)
WITH	(
		AddModUserId	VARCHAR(500)
	)
--		XPath defines all Aurora elements at any level
--		that own an AddProgName element.
SELECT	@ProgramName = AddProgName
FROM	OPENXML(@iDocHandle, '//Aurora[AddProgName]', 3)
WITH	(
		AddProgName	VARCHAR(500)
	)

--		XPath defines all Aurora elements at any level
--		that own an IgnoreWarningMessage element.
SELECT	@sIgnoreWarningMsg = IgnoreWarningMessage
FROM	OPENXML(@iDocHandle, '//Aurora[IgnoreWarningMessage]', 3)
WITH	(
		IgnoreWarningMessage	VARCHAR(16)
	)
IF	(@sIgnoreWarningMsg IS NOT NULL)
AND	(LEN(@sIgnoreWarningMsg) > 0)
AND	(UPPER(@sIgnoreWarningMsg) IN ('TRUE', '1', 'YES', 'ON', 'IGNORE', 'CONTINUE'))
	SELECT	@bIgnoreWarningMsg = CAST(1 AS BIT)
ELSE
	SELECT	@bIgnoreWarningMsg = CAST(0 AS BIT)


SELECT @sXpath = '/Root/NewBookingRequest[BkrId]'
SELECT
	@sBkrId = BkrId,
	@VhrId = VhrId
FROM
	OPENXML(@iDocHandle,'/Root/NewBookingRequest', 3)
	WITH	(
		BkrId	VARCHAR(64),
		VhrId	VARCHAR(64)
		)

IF	(@sBkrID IS NULL)
	SELECT	@sBkrId = ''

--	++++++++++++++++++++++++++++++++++++++++
--		NEW data values
--	++++++++++++++++++++++++++++++++++++++++
--	CONSTRUCT XPath expression
SELECT @sXpath = '/Root/NewBookingRequest[BkrId = ''' + @sBkrId + ''']'
--	FETCH all the fields
SELECT
	@sBooId		= BooId,
	@sBkrId		= BkrId,
	@sAgnCode	= AgnCode,
	@sConCode	= ConCode,
	@sVhrAgnRef	= VhrAgnRef,
	@sSapId		= SapId,
	@sPrdId		= PrdId,
	@sBrdCode	= BrdCode,
	@sClaId		= ClaId,
	@sPkgCode	= PkgCode,
	@sPkgId		= PkgId,
	@sTypeId	= TypeId,
	@IntegrityNo	= IntegrityNo,
	@sMiscName	= MiscAgentName,
	@sMiscContact	= MiscAgentContact,
	@lNumVehicles	= NumberOfVehicles,
	@lNumAdults	= NumberOfAdults,
	@lNumChildren	= NumberOfChildren,
	@lNumInfants	= NumberOfInfants,
	@sCkoLocation	= ckoLocation,
	@stringCkoDate	= ckoDate,
	@sCkoDayPart	= ckoDayPart,
	@sCkiLocation	= ckiLocation,
	@stringCkiDate	= ckiDate,
	@sCkiDayPart	= ckiDayPart,
	@lHirePeriod	= HirePeriod,
	@sHireUoM	= HireUOM,
	@sFirstName	= FirstName,
	@sLastName	= LastName,
	@sTitle		= Title,
	@sRequestSource	= RequestSource,
	@sButClick	= ButClick
FROM
	OPENXML(@iDocHandle, @sXpath, 3)
WITH	(
	BooId			VARCHAR(64),
	BkrId			VARCHAR(64),
	AgnCode			VARCHAR(64),
	ConCode			VARCHAR(64),
	VhrAgnRef		VARCHAR(30),
	SapId			VARCHAR(64),
	PrdId			VARCHAR(64),
	BrdCode			VARCHAR(64),
	ClaId			VARCHAR(64),
	PkgCode			VARCHAR(64),
	PkgId			VARCHAR(64),
	TypeId			VARCHAR(64),
	IntegrityNo		INTEGER,
	MiscAgentName		VARCHAR(100),
	MiscAgentContact	VARCHAR(100),
	NumberOfVehicles	BIGINT,
	NumberOfAdults		BIGINT,
	NumberOfChildren	BIGINT,
	NumberOfInfants		BIGINT,
	ckoLocation		VARCHAR(64),
	ckoDate			VARCHAR(64),
	ckoDayPart		CHAR(2),
	ckiLocation		VARCHAR(64),
	ckiDate			VARCHAR(64),
	ckiDayPart		CHAR(2),
	HirePeriod		BIGINT,
	HireUOM			VARCHAR(64),
	LastName		VARCHAR(128),
	FirstName		VARCHAR(128),
	Title			VARCHAR(10),
	RequestSource		VARCHAR(64),
	ButClick		VARCHAR(32)
	)

-- KX Changes :RKS
DECLARE	@sComCode	VARCHAR(64)
SET @sComCode = dbo.fun_getCompany
				(
					@LoginUserID, --@sUserCode	VARCHAR(64),
					NULL, --@sLocCode	VARCHAR(64),
					NULL, --@sBrdCode	VARCHAR(64),
					NULL, --@sPrdId		VARCHAR(64),
					NULL, --@sPkgId		VARCHAR(64),
					NULL, --@sdummy1	VARCHAR(64),
					NULL, --@sdummy2	VARCHAR(64),
					NULL --@sdummy3	VARCHAR(64)
				)

IF IsNull(@sCkoLocation,'')<>''
BEGIN
	IF NOT EXISTS
		(
		SELECT	LocCode
		FROM	Location WITH (NOLOCK)
		WHERE	LocCode = @sCkoLocation
				AND LocComCode = @sComCode)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sCkoLocation,
						'Check-out Location',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

IF IsNull(@sCkiLocation,'')<>''
BEGIN
	IF NOT EXISTS
		(
		SELECT	LocCode
		FROM	Location WITH (NOLOCK)
		WHERE	LocCode = @sCkiLocation
				AND LocComCode = @sComCode)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sCkiLocation,
						'Check-In Location',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

--	COERCE string date to DATETIME
IF LTRIM(RTRIM(@sClaId))=''
	SET @sClaId = NULL
IF RTRIM(LTRIM(@sBrdCode))=''
	SET @sBrdCode =NULL
IF LTRIM(RTRIM(@sTypeId))=''
	SET @sTypeId = NULL


SELECT
	@dCkoDate = @stringCkoDate,--CONVERT(DateTime, @stringCkoDate, 103),
	@dCkiDate = @stringCkiDate--CONVERT(DateTime, @stringCkiDate, 103)

--	UPDATE? (has additional validation & processing reqts.)
IF	(	@@FETCH_STATUS = 0
	AND	(@sBkrId	IS NOT NULL)
	AND	(@sBkrId	<> '')
	AND	(LEN(@sBkrId)	> 0)
	)
BEGIN
	--	++++++++++++++++++++++++++++++++++++++++
	--		VALIDATE optimistic locking
	--	++++++++++++++++++++++++++++++++++++++++
	IF	(@sBkrId IS NOT NULL)
	AND	(@sBkrId <> '')
	BEGIN
		--	DELETED already?
		--	* ignore integrity number
		IF NOT EXISTS
			(
			SELECT	BkrId
			FROM	BookingRequest WITH (NOLOCK)
			WHERE	BkrId = @sBkrId
			)
		BEGIN
			--	GEN008: row deleted
			EXECUTE
				GEN_getErrorString	'GEN008',
							'Booking Request',
							@sBkrId,
							@oparam1 	= @sError OUTPUT
		       	SELECT	@ReturnError ='GEN008/' + @sError
			GOTO	cleanup_and_leave
		END
		--	UPDATED already?
		--	* expect integrity has not changed
		/*
		IF NOT EXISTS
			(
			SELECT	BkrId
			FROM	BookingRequest WITH (NOLOCK)
			WHERE	BkrId = @sBkrId


			AND	IntegrityNo = @IntegrityNo
			)
		BEGIN
			--	GEN007: row modified
			EXECUTE
				GEN_getErrorString	'GEN007',
							'Booking Request',
							@sBkrId,
							@oparam1 	= @sError OUTPUT
		       	SELECT	@ReturnError ='GEN007/' + @sError
			GOTO	cleanup_and_leave
		END
		*/
	END

	--	++++++++++++++++++++++++++++++++++++++++
	--		OLD data values
	--	++++++++++++++++++++++++++++++++++++++++
	--	CONSTRUCT XPath expression
	SELECT @sXpath = '/Root/OldBookingRequest[BkrId = ''' + @sBkrId + ''']'
	--	FETCH all the other fields
	SELECT
		@sBooIdOld		= BooId,
		@sBkrIdOld		= BkrId,
		@sAgnCodeOld		= AgnCode,
		@sConCodeOld		= ConCode,
		@sVhrAgnRefOld		= VhrAgnRef,
		@sSapIdOld		= SapId,
		@sPrdIdOld		= PrdId,
		@sBrdCodeOld		= BrdCode,
		@sClaIdOld		= ClaId,
		@sPkgCodeOld		= PkgCode,
		@sPkgIdOld		= PkgId,
		@sTypeIdOld		= TypeId,
--		@IntegrityNo		= IntegrityNo,	<<<< ALREADY KNOWN
		@sMiscNameOld		= MiscAgentName,
		@sMiscContactOld	= MiscAgentContact,
		@lNumVehiclesOld	= NumberOfVehicles,
		@lNumAdultsOld		= NumberOfAdults,
		@lNumChildrenOld	= NumberOfChildren,
		@lNumInfantsOld		= NumberOfInfants,
		@sCkoLocationOld	= ckoLocation,
		@stringCkoDateOld	= ckoDate,
		@sCkoDayPartOld		= ckoDayPart,
		@sCkiLocationOld	= ckiLocation,
		@stringCkiDateOld	= ckiDate,

		@sCkiDayPartOld		= ckiDayPart,
		@lHirePeriodOld		= HirePeriod,
		@sHireUoMOld		= HireUOM,
		@sFirstNameOld		= FirstName,
		@sLastNameOld		= LastName,
		@sTitleOld		= Title,
		@sRequestSource	= RequestSource
	FROM
		OPENXML(@iDocHandle, @sXpath, 3)
	WITH	(
		BooId			VARCHAR(64),
		BkrId			VARCHAR(64),
		AgnCode			VARCHAR(64),
		ConCode			VARCHAR(64),
		VhrAgnRef		VARCHAR(30),
		SapId			VARCHAR(64),
		PrdId			VARCHAR(64),
		BrdCode			VARCHAR(64),
		ClaId			VARCHAR(64),
		PkgCode			VARCHAR(64),
		PkgId			VARCHAR(64),
		TypeId			VARCHAR(64),
		IntegrityNo		INTEGER,
		MiscAgentName		VARCHAR(100),
		MiscAgentContact	VARCHAR(100),
		NumberOfVehicles	BIGINT,
		NumberOfAdults		BIGINT,
		NumberOfChildren	BIGINT,
		NumberOfInfants		BIGINT,
		ckoLocation		VARCHAR(64),
		ckoDate			VARCHAR(64),
		ckoDayPart		CHAR(2),
		ckiLocation		VARCHAR(64),
		ckiDate			VARCHAR(64),
		ckiDayPart		CHAR(2),
		HirePeriod		BIGINT,
		HireUOM			VARCHAR(64),
		LastName		VARCHAR(128),
		FirstName		VARCHAR(128),
		Title			VARCHAR(10),
		RequestSource		VARCHAR(64)
		)

END	--	IF (row was fetched)


IF LTRIM(RTRIM(@sClaIdOld)) =''
	SET @sClaIdOld = NULL

IF LTRIM(RTRIM(@sBrdCodeOld))=''
	SET @sBrdCodeOld = NULL
IF LTRIM(RTRIM(@sTypeId))=''
	SET @sTypeId = NULL

/*
	========================================
	END INITIALISE SECTION
	========================================
*/


/*
	========================================
	START VALIDATION SECTION
	========================================
*/

--	DOMAIN VALIDATION ENTITIES (Foreign keys)
--	=========================================

--	++++++++++++++++++++++++++++++++++++++++
--	* Agent: provided as a code
--	++++++++++++++++++++++++++++++++++++++++
IF	(@sAgnCodeOld is null) OR (LEN(@sAgnCodeOld) = 0)
OR	(@sAgnCode <> @sAgnCodeOld)
BEGIN
	--	SPLIT on a hyphen
	IF	(CHARINDEX('-', @sAgnCode) = 0)
		SELECT	@sAgentCode = @sAgnCode
	ELSE
		SELECT	@sAgentCode = LTRIM(RTRIM(SUBSTRING(@sAgnCode, 1, CHARINDEX('-', @sAgnCode) - 1)))

	--	CONVERT code into id
	EXECUTE
		GEN_getAgentIdForInput
			@sAgentCode,
			@sAgentId	= @sAgnId	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
	       	SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	IF NOT EXISTS
		(
		SELECT	AgnId
		FROM	Agent WITH (NOLOCK)
		WHERE	AgnId = @sAgnId)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sAgnId,
						'Agent',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='GEN003/' + @sError
		GOTO	cleanup_and_leave
	END



END

--	++++++++++++++++++++++++++++++++++++++++
--	* Contact: provided as a code
--	++++++++++++++++++++++++++++++++++++++++
/*
	07Mar02:  ** take note **
	[Agent] Contact is only checked when
		    The input value is NOT (blank or null)
		AND (   this is a new Bkr
		     OR	the input has changed)
*/
IF	(	@sConCodeOld IS NULL
	OR	(LEN(@sConCodeOld) = 0)
	OR	(@sConCode <> @sConCodeOld)
	)
AND	(@sConCode IS NOT NULL)
AND	(LEN(@sConCode) > 0)
BEGIN
	--	CONVERT code into id (return if fails)
	EXECUTE
		GEN_getAgentContactIdForInput
			@sAgnId,
			'Reservation',
			@sConCode,
			@sContactID	= @sConId	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
	       	SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	IF NOT EXISTS
		(
		SELECT	ConId
		FROM	Contact WITH (NOLOCK)
		WHERE	ConId = @sConId)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sConId,
						'Contact',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Product: provided as GUID
--	++++++++++++++++++++++++++++++++++++++++
IF	(LEN(@sPrdIdOld) = 0)
OR	(@sPrdId <> @sPrdIdOld)
BEGIN
	EXECUTE
		GEN_getCountryForLocation
			@LocationCode	= @sCkoLocation,
			@CountryCode	= @sCountryCode	OUTPUT,
			@CountryName	= @sCountryName	OUTPUT,
			@ReturnError	= @sError		OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	--	CONVERT code into id (return if fails)
	EXECUTE
		GEN_getProductIdForInput
			@sProduct 	= @sPrdId,
			@sCtyCode	= @sCountryCode,
			@sProductID	= @sPrdId	OUTPUT,
			@ReturnError= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
       	SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	IF NOT EXISTS
		(
		SELECT	PrdId
		FROM	Product WITH (NOLOCK)
		WHERE	PrdId = @sPrdId)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sPrdId,
						'Product',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Check-out Location: provided as GUID
--	++++++++++++++++++++++++++++++++++++++++

IF	(LEN(IsNull(@sCkoLocationOld,'')) = 0)
OR	(@sCkoLocation <> @sCkoLocationOld)
BEGIN
	-- CONVERT entered value to unique
	SELECT	@sCkoLocationOld = @sCkoLocation
	EXECUTE
		GEN_getLocationCodeForInput
			@sCkoLocationOld,
			@sLocationCode	= @sCkoLocation	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
	       	SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	IF NOT EXISTS
		(
		SELECT	LocCode
		FROM	Location WITH (NOLOCK)
		WHERE	LocCode = @sCkoLocation)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sCkoLocationOld,
						'Check-out Location',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Check-in Location: provided as GUID
--	++++++++++++++++++++++++++++++++++++++++
IF	(LEN(IsNull(@sCkiLocationOld,'')) = 0)
OR	(@sCkiLocation <> @sCkiLocationOld)
BEGIN
	-- CONVERT entered value to unique
	SELECT	@sCkiLocationOld = @sCkiLocation
	EXECUTE
		GEN_getLocationCodeForInput
			@sCkiLocationOld,
			@sLocationCode	= @sCkiLocation	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
	       	SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END

	IF NOT EXISTS
		(
		SELECT	LocCode
		FROM	Location WITH (NOLOCK)
		WHERE	LocCode = @sCkiLocation)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sCkiLocationOld,
						'Check-in Location',

						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Package: provided as code
--	++++++++++++++++++++++++++++++++++++++++
--IF	(LEN(@sPkgCode) = 0)
--OR	(@sPkgCode <> @sPkgCodeOld)
/*
IF	(@sPkgCode <> @sPkgCodeOld)
BEGIN
	--	CONVERT from code to ID
	EXECUTE

		GEN_getPackageIDForInput
			@sPkgCode,
			@sPackageID	= @sPkgId	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF (@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError +'a'+ ISNULL(@sPkgCodeOld,'re') + 'a'
		GOTO	cleanup_and_leave
	END

	--	VERIFY ID
	IF NOT EXISTS
		(
		SELECT	PkgId
		FROM	Package WITH (NOLOCK)
		WHERE	PkgId = @sPkgId)
	BEGIN
		--	GEN003: value, but not valid
		EXECUTE
			GEN_getErrorString	'GEN003',
						@sPkgCode,
						'Package',
						'in the database.',
						@oparam1 	= @sError OUTPUT
	       	SET	@ReturnError ='GEN003/' + @sError
		GOTO cleanup_and_leave
	END
END
*/
--	++++++++++++++++++++++++++++++++++++++++
--	* Request Source: provided as GUID
--	++++++++++++++++++++++++++++++++++++++++
IF	(@sRequestSource IS NULL)
OR	(LEN(@sRequestSource) = 0)
OR	(@sRequestSource = '')
BEGIN

	--	GEN099: required
	EXECUTE
		GEN_getErrorString	'GEN099',
					'Request Source (Mode of Communication)',
					@oparam1 	= @sError OUTPUT
       	SELECT	@ReturnError ='GEN099/' + @sError
	GOTO cleanup_and_leave
END
ELSE
BEGIN
	--	MUST exist in CODE table
	IF NOT EXISTS
		(
		SELECT	CodId
		FROM	Code	WITH (NOLOCK)
		WHERE	CodId		= @sRequestSource
		AND	CodCdtNum	= 11 -- Mode of Communication
		AND	CodIsActive	<> 0
		)
	BEGIN
		--	TRANSLATE from a code?
		SELECT	@sRequestSourceId = CodId
		FROM	Code		WITH (NOLOCK)
		WHERE	CodCode		= @sRequestSource
		AND	CodCdtNum	= 11 -- Mode of Communication
		AND	CodIsActive	<> 0
		--	NULL or empty means "fail"
		IF	(@sRequestSourceId IS NULL)
		OR	(LEN(@sRequestSourceId) = 0)
		BEGIN
			--	GEN003: value, but not valid
			EXECUTE
				GEN_getErrorString	'GEN003',
							@sRequestSource,
							'Mode of Communication Code',
							'in the database.',
							@oparam1 	= @sError OUTPUT
		       	SET	@ReturnError ='GEN003/' + @sError
			GOTO cleanup_and_leave
		END
	END
	ELSE
	BEGIN
		SELECT	@sRequestSourceId = @sRequestSource
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Booking Request: ALL COLUMNS
--	++++++++++++++++++++++++++++++++++++++++
SELECT
	@sError = NULL,
	@sWarn	= NULL
EXECUTE
	RES_validateBookingRequest
		@xmlRequestIn,
		@ReturnError	= @sError	OUTPUT
--	OOPS! sucked a kumara this time.....
IF	(@sError IS NOT NULL)
BEGIN
	SELECT	@ReturnError = @sError
	GOTO	cleanup_and_leave
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Vehicle Request: ALL COLUMNS
--	++++++++++++++++++++++++++++++++++++++++
SELECT
	@sError = NULL,
	@sWarn	= NULL
EXECUTE
	RES_validateVehicleRequest
		@xmlRequestIn,
		@ReturnError	= @sError	OUTPUT
--	OOPS! sucked a kumara this time.....
IF	(@sError IS NOT NULL)
BEGIN
	SELECT	@ReturnError = @sError
	GOTO	cleanup_and_leave
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Check-Out/In Location: SAME COUNTRY
--	++++++++++++++++++++++++++++++++++++++++
IF	(@sCkoLocation IS NOT NULL)
AND	(@sCkoLocation <> '')
AND	(LEN(@sCkoLocation) > 0)
AND	(@sCkiLocation IS NOT NULL)
AND	(@sCkiLocation <> '')
AND	(LEN(@sCkiLocation) > 0)
BEGIN
	SELECT	@sError = NULL
	EXECUTE
		GEN_countriesMatchForLocations
			@sCkoLocation,
			@sCkiLocation,
			NULL,		-- @RentalID,
			@ReturnError	= @sError OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++++++++++++
--		ADJUST time based on user location
--	++++++++++++++++++++++++++++++++++++++++++++++++++
SELECT
	@BookedDate =
		dbo.GEN_getAdjustedTimeForUser
			(

				@LoginUserID,
				GETDATE()
			),

--	++++++++++++++++++++++++++++++++++++++++
--	* Check-Out/In Date: validate as pair
--	++++++++++++++++++++++++++++++++++++++++
	@sError = NULL

EXECUTE
	RES_validateDatePair
		@dCkoDate,
		@dCkiDate,
		@sCkoLocation,
		@BookedDate,
		@sCkoDayPart,
		@sCkiDayPart,
		@RentalID	= NULL,
		@ReturnError	= @sError	OUTPUT
IF	(@sError IS NOT NULL)
BEGIN
	SELECT	@ReturnError = @sError
	GOTO	cleanup_and_leave
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Multi-hire rentals: is this one?
--	++++++++++++++++++++++++++++++++++++++++
IF	(@sBooId IS NOT NULL)
AND	(LEN(@sBooId) > 0)

BEGIN

	SELECT
		@sError = NULL,
		@sRentalId = NULL

	--	++++++++++++++++++++++++++++++++++++++++
	--	* NEED country from location
	--	++++++++++++++++++++++++++++++++++++++++
	EXECUTE
		GEN_getCountryForLocation
			@sCkoLocation,
			@CountryCode	= @sCountryCode	OUTPUT,
			@CountryName	= @sCountryName	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END
	--	++++++++++++++++++++++++++++++++++++++++
	--	* This Rental: can add to multi-hire?
	--	++++++++++++++++++++++++++++++++++++++++

	EXECUTE
		RES_validateAddRentalToMultiHire
			@sBooId,
			@BookedDate,
			@sCountryCode,
			@dCkoDate,
			@dCkiDate,
			@sRentalId,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError
		GOTO	cleanup_and_leave
	END
END

--	++++++++++++++++++++++++++++++++++++++++
--	* Package: DETAILED VALIDATION
--	++++++++++++++++++++++++++++++++++++++++
IF	(@sPkgCode IS NOT NULL)
AND	(@sPkgCode <> '') AND (@sPkgID is null)
AND	(LEN(@sPkgCode) > 0)
BEGIN
	--	CONVERT from code to ID

	EXECUTE
		GEN_getPackageIDForInput
			@sPkgCode,
			@sPackageID	= @sPkgId	OUTPUT,
			@ReturnError	= @sError	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
		SELECT	@ReturnError = @sError + ' -- Gen_getPackageIdForInput' + isnull(@sPkgID, 'sub')
		GOTO	cleanup_and_leave
	END


	IF	(LEN(@sAgnId) = 0)
	OR	(@sAgnId IS NULL)
	BEGIN
		--	SPLIT on a hyphen
		IF	(CHARINDEX('-', @sAgnCode) = 0)
			SELECT	@sAgentCode = @sAgnCode
		ELSE
			SELECT	@sAgentCode = LTRIM(RTRIM(SUBSTRING(@sAgnCode, 1, CHARINDEX('-', @sAgnCode) - 1)))

		--	CONVERT code into id
		EXECUTE
			GEN_getAgentIdForInput
				@sAgentCode,
				@sAgentId	= @sAgnId	OUTPUT,
				@ReturnError	= @sError	OUTPUT
		IF	(@sError IS NOT NULL)
		BEGIN
		       	SELECT	@ReturnError = @sError
			GOTO	cleanup_and_leave
		END
	END	-- IF need to re-get agent

	--	USE checkPackage() for detailed checks
	EXECUTE
		RES_checkPackage
			@PackageID 				= @sPkgID,
			@AgentID				= @sAgnId,
			@RentalID				= NULL,
			@CheckOutLocationCode	= @sCkoLocation,
			@CheckInLocationCode	= @sCkiLocation,
			@CheckOutDate			= @dCkoDate,
			@CheckInDate			= @dCkiDate,
			@ProductID				= @sPrdId,
			@SaleableProductID		= @sSapId,
			@BookedDate				= @BookedDate,
			@NumberOfAdults			= @lNumAdults,
			@NumberOfChildren		= @lNumChildren,
			@NumberOfInfants		= @lNumInfants,
			@ReturnError			= @sError	OUTPUT,
			@ReturnWarning			= @sWarn	OUTPUT
	IF	(@sError IS NOT NULL)
	BEGIN
	       	SELECT	@ReturnError = @sError + '--Res_checkPackage'
		GOTO	cleanup_and_leave
	END
END	--	CHECK package valid

--	++++++++++++++++++++++++++++++++++++++++
--	* Blocking and Allowing rules
--	++++++++++++++++++++++++++++++++++++++++
SELECT	@sStatus = ''/*,
	@VhrId = NULL		-- ####VALIDATE THIS#####*/
EXECUTE	RES_manageBlockingAndAllowingRules
	@LoginUserID	=	@LoginUserID,
	@ProgramName	=	@ProgramName,
	@AgnId			=	@sAgnId,
	@CkoDate		=	@dCkoDate,
	@CkiDate		=	@dCkiDate,
	@HirePeriod		=	@lHirePeriod,
	@CkoLocation	=	@sCkoLocation,
	@CkiLocation	=	@sCkiLocation,
	@BookedDate		=	@BookedDate,
	@PrdId			=	@sPrdId,
	@PkgId			=	@sPkgId,
	@VhrId			=	@VhrId,
	@ReturnError	=	@sError		OUTPUT,
	@ReturnStatus	=	@sStatus	OUTPUT,
	@ReturnBlrIdList=	@sBlrIdList	OUTPUT

--	ONLY continue if no errors
IF	(@sError IS NOT NULL) AND (@sError <> '') AND (LEN(@sError) > 0)
BEGIN
/* ------------------------------------------------------------------------
	SELECT	@ReturnWarning	=
			'Request declined. ' +  @sError +
			' You may continue with your selection by' +
			' getting your logon status altered. ' +
			' Contact the scheduling team or your supervisor.'


	Changed By Subbaiah on 07/06/2002. Insted or Assigining the above message to
the variable directaly, we are calling "GEN_manageBlockingMessage" to get the same.
------------------------------------------------------------------------*/
	EXEC GEN_manageBlockingMessage
			@sMessage = @sError,
			@bWarningOnly = 1,
			@sRetryMethod = NULL,
			@sNextScreen = 'VehicleRequestMgt.asp',
			@sWarnings = @ReturnWarning OUTPUT
END

--	CONTINUE? or fail right here...
--	* ERROR(s) and we're outta here immediately (or sooner)
IF	(	(@ReturnError IS NOT NULL)

	AND	(@ReturnError <> '')
	)
--	*	(WARNING(s)
--	 AND	IGNORE=False) then we're outta here too...
OR
	(	(@ReturnWarning IS NULL)
	AND	(LEN(@ReturnWarning) > 0)
	AND	(@bIgnoreWarningMsg = CAST(0 AS BIT))
	)
BEGIN
	GOTO	cleanup_and_leave
END

/*
	========================================
	END VALIDATION SECTION
	========================================
*/

/*
	========================================
	START PROCESSING SECTION
	========================================
*/

/*
	NOTES:
	1.	"standard functionality applies..." appears to mean
		the XML document will be supplied with an empty BkrId
		element to indicate an INSERT operation is required.
		Otherwise, the BkrId will contain a GUID and this row
		in the database will be retrieved (to check
		integrity etc).
	2.	The insert/update needs to work across a group of
		related tables. The MASTER table is the Booking-Request
		table. The Bkr row owns one or more child Vehicle-
		Request rows. Each Vhr row may own zero, one or many
		Features (implemented as a bridge or link table between
		the Feature and Vehicle-Request tables
		(the Vehicle-Request-Feature table).
	3.	Note (2) means the SP must implement transactions and
		rollback on error.
*/

IF	(LEN(@sBkrId) = 0)
BEGIN

	--	GET GUIDs for Bk-Request & Vh-Request
	SELECT
		@sBkrId		= CAST(NEWID() AS VARCHAR(64)),
		@VhrID		= CAST(NEWID() AS VARCHAR(64)),
		@IntegrityNo	= CAST(1 AS INTEGER),
		@DbDateTime	= GETDATE()

	--	MULTIPLE table insert: start atomic unit of work
	-- BEGIN TRANSACTION

	--	IFF we get this far, commit changes to DB
	--	* BOOKING REQUEST row
	EXECUTE
		RES_insertBookingRequest
			@sBkrId,
			@sAgnId,
			@sConId,
			@sMiscName,
			@sMiscContact,
			@sLastName,
			@sFirstName,
			@sTitle,
			@IntegrityNo,	-- INTEGRITY starts at one
			@LoginUserID,
			@DbDateTime,
			@ProgramName,
			@bInsertSuccess		= @bDbStatus	OUTPUT

	IF	(@@ERROR <> 0)
	OR	(@bDbStatus = 0)
	BEGIN
		ROLLBACK TRANSACTION
		--	GEN118: DB table operation failed
		EXECUTE
			GEN_getErrorString	'GEN118',
						'Creating',
						'Booking Request',
						@oparam1 	= @sError OUTPUT
	       	SELECT
			@ReturnError ='GEN118/' + @sError,
			@ReturnXml =
				'<Root><Info>The BookingRequest insert failed (' +
				RTRIM(CAST(@@ERROR AS VARCHAR(16))) +
				').</Info></Root>'
		GOTO cleanup_and_leave
	END

	--	* VEHICLE REQUEST row
	INSERT INTO
		VehicleRequest WITH (ROWLOCK)

		(
			VhrId,
			VhrPrdId,
			VhrBkrId,
			VhrAgnRef,
			VhrCkoLocCode,
			VhrCkoDate,
			VhrCkoAmPm,
			VhrCkiDate,
			VhrCkiAmPm,
			VhrCkiLocCode,
			VhrHirePeriodUom,
			VhrHirePeriod,
			VhrNumOfAdults,
			VhrNumOfChildren,
			VhrNumOfInfants,
			VhrPkgId,
			VhrBrdCode,
			VhrClaId,
			VhrTypId,
			VhrNumOfVehicles,
			IntegrityNo,	-- INTEGRITY starts at one
			VhrCodSourceMocId,
			AddUsrId,
			VhrBlockingResult,
			AddDateTime,
			AddPrgmName,
			VhrRequestType
		)
		VALUES
		(

			@VhrID,
			@sPrdId,
			@sBkrId,

			@sVhrAgnRef,

			@sCkoLocation,
			@dCkoDate,
			@sCkoDayPart,
			@dCkiDate,
			@sCkiDayPart,
			@sCkiLocation,
			@sHireUoM,
			@lHirePeriod,
			@lNumAdults,
			@lNumChildren,
			@lNumInfants,
			CASE @sPkgId
				WHEN '' THEN NULL
				ELSE @sPkgId
			END,
			@sBrdCode,
			@sClaId,
			@sTypeId,
			@lNumVehicles,
			@IntegrityNo,	-- INTEGRITY starts at one
			@sRequestSourceId,
			@LoginUserID,
			CAST(@sStatus AS VARCHAR(12)),
			@DbDateTime,
			@ProgramName,
			@sButClick
		)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Vehicle Request',
						'INSERT',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END
	/* Initialising Back the user when new request is done*/
	UPDATE UserInfo WITH (ROWLOCK)
		SET UsrVehicleRequestCount = 0
	WHERE	UsrId = @LoginUserID
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'User Info',
						'Update',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	* VEHICLE REQUEST FEATURE row(s)
	INSERT INTO
		VehicleRequestFeature WITH (ROWLOCK)

		SELECT
			FtrId			'VrfFtrId',
			@VhrId			'VrfVhrId',
			@IntegrityNo		'IntegrityNo',	-- INTEGRITY starts at one
			@LoginUserID		'AddUsrId',
			NULL			'ModUsrId',
			@DbDateTime		'AddDateTime',
			NULL			'ModDateTime',
			@ProgramName		'AddPrgmName'
		FROM
			OPENXML(@iDocHandle, '//NewBookingRequest/Feature[FtrId]', 3)
		WITH	(
				FtrId	VARCHAR(64)
			)
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Vehicle Request Feature',
						'INSERT',


						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	OTHERWISE, commit it...
	-- COMMIT TRANSACTION

	--	RECORD the GUIDs in the xml document
	SELECT
		--	REVERT if we get to here
		@ReturnXml = @xmlRequestIn,
		@xmlBkrID = '<BkrId>' + @sBkrId + '</BkrId>',
		@xmlVhrID = '<VhrId>' + @VhrId + '</VhrId>',
		--	LOOK for "whole" tag first
		@intStartIndex = CHARINDEX('<BkrId>', @ReturnXml)
	--	* Booking Request ID
	IF	(@intStartIndex = 0)
	BEGIN
		--	THEN try "shorthand" tag
		SELECT
			@intStartIndex = CHARINDEX('<BkrId', @ReturnXml)
		IF	(@intStartIndex = 0)
		BEGIN
			SELECT
				@ReturnXml =
					REPLACE
					(
					@ReturnXml,
					'<NewBookingRequest>',
					'<NewBookingRequest>' + @xmlBkrID
					)
		END
		ELSE
		--	NOW look for />
		BEGIN
			--	THIS should give us the shorthand TAG
			SELECT
				@xmltemp = SUBSTRING(@ReturnXml, @intStartIndex, CHARINDEX('/>', @ReturnXml, @intStartIndex) + 2 - @intStartIndex)
			--	MUST be the only tag
			IF	(CHARINDEX('<', @xmlTemp, 2) = 0)
			BEGIN
				SELECT
					@ReturnXml =
						REPLACE
						(
						@ReturnXml,
						@xmlTemp,
						@xmlBkrID
						)
			END
		END
	END
	ELSE
	--	JUST insert the Bkr GUID between the open/close tags
	BEGIN
		SELECT
			@ReturnXml =
				REPLACE
				(
				@ReturnXml,
				'<BkrId></BkrId>',
				@xmlBkrID
				)
	END
	--	* Vehicle Request ID
	--	LOOK for "whole" tag first
	SELECT
		@intStartIndex = CHARINDEX('<VhrId>', @ReturnXml)
	IF	(@intStartIndex = 0)
	BEGIN
		--	THEN try "shorthand" tag
		SELECT
			@intStartIndex = CHARINDEX('<VhrId', @ReturnXml)
		IF	(@intStartIndex = 0)
		BEGIN
			SELECT
				@ReturnXml =
					REPLACE
					(
					@ReturnXml,
					'<NewBookingRequest>',
					'<NewBookingRequest>' + @xmlVhrID
					)
		END
		ELSE
		--	NOW look for />
		BEGIN
			--	THIS should give us the shorthand TAG
			SELECT
				@xmltemp = SUBSTRING(@ReturnXml, @intStartIndex, CHARINDEX('/>', @ReturnXml, @intStartIndex) + 2 - @intStartIndex)
			--	MUST be the only tag
			IF	(CHARINDEX('<', @xmlTemp, 2) = 0)
			BEGIN
				SELECT

					@ReturnXml =
						REPLACE
						(
						@ReturnXml,
						@xmlTemp,
						@xmlVhrID
						)
			END
		END
	END
	ELSE
	--	JUST insert the Vhr GUID between the open/close tags
	BEGIN
		SELECT
			@ReturnXml =
				REPLACE
				(
				@ReturnXml,
				'<VhrId></VhrId>',
				@xmlVhrID
				)
	END

END	-- IF <insert>
ELSE
BEGIN

	--	1. Booking Request table
	--		.1 increment integrity
	SELECT	@IntegrityNo	= IntegrityNo + 1
	FROM	BookingRequest	WITH (NOLOCK)
	WHERE	BkrId = @sBkrId
	IF @@ERROR <> 0
	BEGIN
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Integrity Number',
						'UPDATE',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	MULTIPLE table update: start atomic unit of work
	BEGIN TRANSACTION

	--		.2 Update the row
	UPDATE	BookingRequest	WITH (ROWLOCK)
	SET
		BkrId			= @sBkrId,
		BkrAgnId		= @sAgnId,
		BkrConId		= @sConId,
		BkrMiscAgentName	= @sMiscName,
		BkrMiscAgentContact	= @sMiscContact,
		BkrLastName		= @sLastName,
		BkrFirstName		= @sFirstName,
		BkrTitle		= @sTitle,
		IntegrityNo		= @IntegrityNo,
		ModUsrId		= @LoginUserID,
		ModDateTime		= GETDATE()
	WHERE	BkrId = @sBkrId

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',

						'Booking Request',
						'UPDATE',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	2. Vehicle Request
	--		.1 increment integrity
	SELECT	@IntegrityNo	= IntegrityNo + 1
	FROM	VehicleRequest	WITH (NOLOCK)
	WHERE	VhrId = @VhrId
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Integrity Number',
						'UPDATE',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END
	--		.2 Update row
	IF LTRIM(RTRIM(@VhrId))=''
	BEGIN
		SET @VhrID = CAST(NEWID() AS VARCHAR(64))
		SET @xmlVhrID = '<VhrId>' + @VhrId + '</VhrId>'
			-- Add Time to the CKO AND CKI Fields

		INSERT INTO
		VehicleRequest WITH (ROWLOCK)

		(
			VhrId,
			VhrPrdId,
			VhrBkrId,
			VhrAgnRef,
			VhrCkoLocCode,
			VhrCkoDate,
			VhrCkoAmPm,
			VhrCkiDate,
			VhrCkiAmPm,
			VhrCkiLocCode,
			VhrHirePeriodUom,
			VhrHirePeriod,
			VhrNumOfAdults,
			VhrNumOfChildren,
			VhrNumOfInfants,
			VhrPkgId,
			VhrBrdCode,
			VhrClaId,
			VhrTypId,
			VhrNumOfVehicles,
			IntegrityNo,	-- INTEGRITY starts at one
			VhrCodSourceMocId,
			AddUsrId,
			VhrBlockingResult,
			AddDateTime,
			AddPrgmName,
			VhrRequestType
		)
		VALUES
		(
			@VhrID,
			@sPrdId,
			@sBkrId,
			@sVhrAgnRef,
			@sCkoLocation,
			@dCkoDate,
			@sCkoDayPart,
			@dCkiDate,
			@sCkiDayPart,
			@sCkiLocation,
			@sHireUoM,
			@lHirePeriod,
			@lNumAdults,
			@lNumChildren,
			@lNumInfants,
			CASE @sPkgId
				WHEN '' THEN NULL
				ELSE @sPkgId
			END,
			@sBrdCode,
			@sClaId,
			@sTypeId,
			@lNumVehicles,
			@IntegrityNo,	-- INTEGRITY starts at one
			@sRequestSourceId,
			@LoginUserID,
			CAST(@sStatus AS VARCHAR(12)),
			current_timestamp,
			@ProgramName,
			@sButClick
		)

	SELECT
		@intStartIndex = CHARINDEX('<VhrId>', @ReturnXml)
	IF	(@intStartIndex = 0)
	BEGIN
		--	THEN try "shorthand" tag
		SELECT
			@intStartIndex = CHARINDEX('<VhrId', @ReturnXml)
		IF	(@intStartIndex = 0)
		BEGIN
			SELECT
				@ReturnXml =
					REPLACE
					(
					@ReturnXml,
					'<NewBookingRequest>',
					'<NewBookingRequest>' + @xmlVhrID
					)
		END
		ELSE
		--	NOW look for />
		BEGIN
			--	THIS should give us the shorthand TAG
			SELECT
				@xmltemp = SUBSTRING(@ReturnXml, @intStartIndex, CHARINDEX('/>', @ReturnXml, @intStartIndex) + 2 - @intStartIndex)
			--	MUST be the only tag

			IF	(CHARINDEX('<', @xmlTemp, 2) = 0)
			BEGIN
				SELECT
					@ReturnXml =
						REPLACE
						(
						@ReturnXml,
						@xmlTemp,
						@xmlVhrID
						)
			END
		END
	END
--select @ReturnXml
	END
	ELSE
	BEGIN

		UPDATE	VehicleRequest	WITH (ROWLOCK)
		SET
			VhrPrdId		= @sPrdId,
			VhrBkrId		= @sBkrId,
			VhrAgnRef		= @sVhrAgnRef,
			VhrCkoLocCode		= @sCkoLocation,
			VhrCkoDate		= @dCkoDate,
			VhrCkoAmPm		= @sCkoDayPart,
			VhrCkiDate		= @dCkiDate,
			VhrCkiAmPm		= @sCkiDayPart,
			VhrCkiLocCode		= @sCkiLocation,
			VhrHirePeriodUom	= @sHireUoM,
			VhrHirePeriod		= @lHirePeriod,
			VhrNumOfAdults		= @lNumAdults,
			VhrNumOfChildren	= @lNumChildren,
			VhrNumOfInfants		= @lNumInfants,
			VhrPkgId		= CASE @sPkgId
							WHEN '' THEN NULL
							ELSE	@sPkgId
						  END,
			VhrBrdCode		= @sBrdCode,
			VhrClaId		= @sClaId,
			VhrTypId		= @sTypeId,
			VhrNumOfVehicles	= @lNumVehicles,
			VhrCodSourceMocId	= @sRequestSourceId,
			VhrBlockingResult	= @sStatus,
			VhrRequestType		= @sButClick,
			IntegrityNo		= @IntegrityNo,
			ModUsrId		= @LoginUserID,
			ModDateTime		= GETDATE()
		WHERE	VhrId = @VhrId
	END

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Vehicle Request',
						'UPDATE',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	3. Vehicle Request Feature table
	--		.1 DELETE existing
	DELETE FROM

		VehicleRequestFeature WITH (ROWLOCK)
	WHERE	VrfVhrId = @VhrId

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Vehicle Request Feature',
						'DELETE before update',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--		.2 INSERT new feature rows
	INSERT INTO
		VehicleRequestFeature WITH (ROWLOCK)

		SELECT
			FtrId		'VrfFtrId',
			@VhrId		'VrfVhrId',
			CAST(1 AS INTEGER),	-- INTEGRITY starts at one
			@LoginUserID	'AddUsrId',
			NULL		'ModUsrId',
			GETDATE()	'AddDateTime',
			NULL		'ModDateTime',
			@ProgramName	'AddPrgmName'
		FROM
			OPENXML(@iDocHandle, '//NewBookingRequest/Feature[FtrId]', 3)
		WITH	(
				FtrId	VARCHAR(64)
			)
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		--	RES061: required
		EXECUTE
			GEN_getErrorString	'RES061',
						'Vehicle Request Feature',
						'UPDATE',
						@oparam1 	= @sError OUTPUT
	       	SELECT	@ReturnError ='RES061/' + @sError
		GOTO cleanup_and_leave
	END

	--	OTHERWISE, commit it...
	 COMMIT TRANSACTION

END	-- ELSE <update>

GOTO	cleanup_and_leave

--	CLEAN up time!
cleanup_and_leave:

	--	* DROP-KICK memory based XML Document...
	IF	(@iDocHandle IS NOT NULL)
		EXEC sp_xml_removedocument @iDocHandle
	RETURN

--	********** End RES_ManageBookingRequest() **********



GO
