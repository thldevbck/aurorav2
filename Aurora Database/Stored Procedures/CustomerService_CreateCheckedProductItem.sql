CREATE PROCEDURE [dbo].[CustomerService_CreateCheckedProductItem]
	@ComCode AS varchar(64),
	@LivId AS varchar(64),
	@PriUnitNum AS varchar(64),
	@IsVehicle AS bit,
	@UsrId AS varchar(64),
	@PrgrmName AS varchar(64)

AS

SET NOCOUNT ON

DECLARE @FleetCompanyId AS varchar(64)
DECLARE @PriId AS varchar(64)

-- check to see if the CheckedProductItem already exists
SELECT
	@PriId = CheckedProductItem.PriId
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
	INNER JOIN CheckedProductItem ON LocationInventory.LivId = CheckedProductItem.PriLivId
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId
	AND CheckedProductItem.PriUnitNum = @PriUnitNum
	AND CheckedProductItem.PriIsVehicle = @IsVehicle

-- if not, try and create
IF @PriId IS NULL
BEGIN
	-- get the AIMS fleet company id
	SELECT
		@FleetCompanyId = FleetCompanyId
	FROM
		Company
	WHERE
		Company.ComCode = @ComCode

	SET @PriId = NEWID()

	IF @IsVehicle = 1
	BEGIN
		INSERT CheckedProductItem
		(
			PriId,
			PriLivId,
			PriUnitNum,
			PriIsVehicle ,
			IntegrityNo,
			AddUsrId,
			AddDateTime,
			AddPrgmName
		)
		SELECT TOP 1
			@PriId AS PriId,
			@LivId AS PriLivId,
			AimsProd.dbo.FleetAsset.UnitNumber AS PriUnitNum,
			1 AS PriIsVehicle,
			1 AS IntegrityNo,
			@UsrId AS AddUsrId,
			GETDATE() AS AddDateTime,
			@PrgrmName AS AddPrgmName
		FROM
			AimsProd.dbo.FleetAsset
		WHERE
			(AimsProd.dbo.FleetAsset.UnitNumber = @PriUnitNum OR AimsProd.dbo.FleetAsset.RegistrationNumber = @PriUnitNum)
			AND CHARINDEX (CONVERT (varchar, AimsProd.dbo.FleetAsset.CompanyId), @FleetCompanyId) > 0
	END
	ELSE
	BEGIN
		INSERT CheckedProductItem
		(
			PriId,
			PriLivId,
			PriUnitNum,
			PriIsVehicle ,
			IntegrityNo,
			AddUsrId,
			AddDateTime,
			AddPrgmName
		)
		SELECT TOP 1
			@PriId AS PriId,
			@LivId AS PriLivId,
			AimsProd.dbo.AI_OtherAsset.AssetNr AS PriUnitNum,
			0 AS PriIsVehicle,
			1 AS IntegrityNo,
			@UsrId AS AddUsrId,
			GETDATE() AS AddDateTime,
			@PrgrmName AS AddPrgmName
		FROM
			AimsProd.dbo.AI_OtherAsset
			INNER JOIN AimsProd.dbo.FaAdditions ON AimsProd.dbo.FaAdditions.AssetId = AimsProd.dbo.AI_otherAsset.AssetId
		WHERE
			AimsProd.dbo.AI_OtherAsset.AssetNr = @PriUnitNum
			AND CHARINDEX (CONVERT (varchar, AimsProd.dbo.FaAdditions.CompanyId), @FleetCompanyId) > 0
	END

END

-- return a result if successful
SELECT
	CheckedProductItem.*
FROM
	CheckedProductItem
WHERE
	CheckedProductItem.PriId = @PriId




GO
