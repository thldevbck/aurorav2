USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetWeekProducts]    Script Date: 11/19/2007 13:44:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[Flex2_GetWeekProducts] 
	@ComCode varchar(64),
	@FbwId int,
	@FlpId int
AS

SELECT 
	*
FROM 
	FlexWeekProductView
WHERE
	FlexWeekProductView.ComCode = @ComCode
	AND FlexWeekProductView.FbwId = @FbwId
	AND (@FlpId IS NULL OR FlexWeekProductView.FlpId = @FlpId)
ORDER BY
	FlexWeekProductView.TravelYear,
	FlexWeekProductView.ComCode,
	FlexWeekProductView.CtyCode,
	FlexWeekProductView.BrdCode,
	FlexWeekProductView.PrdShortName











