CREATE PROCEDURE [dbo].[Report_Delete]
	@repId AS varchar(64)
AS

DELETE ReportParamListItem FROM
	Report
	INNER JOIN ReportParam ON Report.repId = ReportParam.rppRepId
	INNER JOIN ReportParamListItem ON ReportParam.rppId = ReportParamListItem.rpiRppId
WHERE
	Report.repId = @repId


DELETE ReportParam
FROM
	Report
	INNER JOIN ReportParam ON Report.repId = ReportParam.rppRepId
WHERE
	Report.repId = @repId


DELETE Report
FROM
	Report
WHERE
	Report.repId = @repId














GO
