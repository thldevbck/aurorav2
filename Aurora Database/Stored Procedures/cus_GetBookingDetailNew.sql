set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




-- 	QUERY FOR CUS_GETBOOKINGDETAIL.SQL

Create	PROCEDURE [dbo].[cus_GetBookingDetailNew] 
			@BooId			VARCHAR	 (64)	=	'',
			@Condition		VARCHAR	 (8000)	=	'',
			@RntId			VARCHAR	 (64)	=	'',
			@BooNum			VARCHAR	 (64)	=	'',
			-- KX Changes :RKS
			@sUsrCode		VARCHAR(64)		=   ''
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @sError		VARCHAR(4000),
			@RntNum 	VARCHAR(2),
			@BooNumTmp	VARCHAR(24)

	IF @Condition = 'BookingNum'
	BEGIN
		SELECT	BooNum		AS	BoNo
			FROM	Booking WITH (NOLOCK)
			WHERE 	BooId=@BooId
			FOR XML AUTO, ELEMENTS
		RETURN
	END	--	END OF @Condition = 'BookingNum'
	
	IF @Condition = 'RentalRecord'
	BEGIN
		SELECT
			RntId																		AS	RntId,
			RntNum																		AS	RentalNo,
			ISNULL((CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D') + ' ' +  RntCkoLocCode),'')		AS	'Check-Out',
			ISNULL((CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D') + ' ' +  RntCkiLocCode),'')		AS	'Check-In' ,
			ISNULL(RntNumOfHirePeriods, 0)															AS	HirePd ,
			ISNULL(Package.PkgCode,'')															AS	Package ,
			''+ ISNULL(BrdName,'')																		AS	Brand ,
			''+ ''+ ISNULL((SELECT TOP 1
							PrdShortName 
						FROM 
							Product WITH (NOLOCK),  
							SaleableProduct WITH (NOLOCK), 
							BookedProduct WITH (NOLOCK) 
						WHERE 	
							PrdId = SapPrdId 
							AND BpdSapId = SapId
							AND BpdRntId = RntId
							AND BpdPrdIsVehicle = 1
							AND BpdStatus NOT IN('XX','XN')
							AND BpdIsCurr = 1
						ORDER BY BpdCkoWhen DESC),'')							AS	Vehicle ,
			RntStatus																	AS	Status,
			'' + left(DateName(dw,RntCkoWhen),3)												AS	CkoDay ,
			'' + left(DateName(dw,RntCkiWhen),3)												AS	CkiDay ,
			--Added by Shoel on 6/11/7 > for the Integrity number issue
			'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo		



		FROM         
			Rental WITH (NOLOCK) LEFT OUTER JOIN
	                     	Package WITH (NOLOCK) ON Rental.RntPkgId = Package.PkgId LEFT OUTER JOIN
                      		Brand WITH (NOLOCK) ON Package.PkgBrdCode = Brand.BrdCode LEFT OUTER JOIN
                      		Product WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product.PrdId
		WHERE 
			RntBooId=@BooId
		-- KX Changes :RKS
		AND	(@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
											INNER JOIN dbo.Company (NOLOCK)	ON Brand.BrdComCode = Company.ComCode
											INNER JOIN dbo.UserCompany (NOLOCK)	ON UserCompany.ComCode = Company.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK)	ON UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode)
			)
		ORDER BY CAST(RntNum AS INT)
		FOR XML AUTO, ELEMENTS
		RETURN
	END	--	@Condition = 'RentalRecord'
	IF(@Condition = 'BookRntRec')
	BEGIN
--		UPDATE dbo.Rental WITH(ROWLOCK)
--			SET RntIsError = 1
--		WHERE RntId = @RntId

		SELECT
			RntId																		AS	RntId,
			RntNum																		AS	RentalNo,
			CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D') + ' ' +  RntCkoLocCode		AS	'Check-Out',
			CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D') + ' ' +  RntCkiLocCode		AS	'Check-In' ,
			ISNULL(RntNumOfHirePeriods, 0)															AS	HirePd ,
			ISNULL(Package.PkgCode,'')															AS	Package ,
			''+ ISNULL(BrdName,'')																		AS	Brand ,
			''+ ISNULL((SELECT TOP 1
							PrdShortName 
						FROM 
							Product WITH (NOLOCK)
						WHERE 	
							PrdId = RntPrdRequestedVehicleId),'')							AS	Vehicle ,
			RntStatus																	AS	Status ,
			''+ BooNum																		AS	BooNum ,
			''+ AgnCode + ' - ' + CASE 	
									WHEN ISNULL(BooMiscAgentName,'') = '' THEN AgnName
								  	ELSE	AgnName
								  END + ' - ' + dbo.GEN_getCodDesc(AgnCodDebtStatId,NULL,NULL)		AS	AgnName ,
--			''+ AgnCode + ' - ' + ISNBkrMiscAgentName															AS	AgnName ,
--		    '' + AgnCode + ' - ' + AgnName						AS AgnName,															AS	AgnName ,
			''+ ISNULL(BooLastName,'') + ', ' + ISNULL(BooTitle,'') +  ' ' + ISNULL(BooFirstName,'')
			/*ISNULL((
				SELECT TOP 1 
					Customer.CusLastName + ', ' + Customer.CusTitle + ' ' + Customer.CusFirstName
				FROM         
					Rental INNER JOIN Traveller ON 
					Rental.RntId = Traveller.TrvRntId 
						INNER JOIN Customer ON 
					Traveller.TrvCusId = Customer.CusId 
						INNER JOIN Booking ON 
					Rental.RntBooID = Booking.BooID
				WHERE     
				(Traveller.TrvHirer = 1) AND (Booking.BooID = @BooId)
			),'')*/																		AS	Hirer ,
			'' + ISNULL(BooStatus,'')													AS	BookingSt ,
			(SELECT COUNT(rntid) FROM Rental with (nolock) WHERE RntBooId = BooId)						AS	NoOfRnt,
			--Added by Shoel on 6/11/7 > for the Integrity number issue
			'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo		
			
		FROM
			Rental WITH (NOLOCK),
			Package  WITH (NOLOCK),
			Brand WITH (NOLOCK),
--			Product WITH (NOLOCK),
			Booking WITH (NOLOCK),
			Agent WITH (NOLOCK)
		WHERE 	BooAgnId = AgnId
		AND		RntBooId = BooId
		AND		RntBooId=@BooId
		AND		Package.PkgId = Rental.RntPkgId
		AND		Brand.BrdCode = Package.PkgBrdCode
--		AND		Product.PrdId = Rental.RntPrdRequestedVehicleId
		AND		Rental.RntId = @RntId
		FOR XML AUTO, ELEMENTS		

	END	--	End of @Condition = 'BookRntRec'

	IF(@Condition = 'BookRec')
	BEGIN
		SET @RntId = NULL
		SET @BooNumTmp = @BooNum
		SET @BooNum = dbo.getsplitedData(@BooNumTmp, '/', 1)
		IF CHARINDEX('/', @BooNumTmp) = 0
			SET @RntNum = NULL
		ELSE
			SET @RntNum = dbo.getsplitedData(@BooNumTmp, '/', 2)

		SELECT TOP 1 @RntId =RntId FROM Rental WITH (NOLOCK), Booking WITH (NOLOCK),dbo.Package (NOLOCK) WHERE  RntPkgId = PkgId and RntBooId = BooId AND BooNum = @BooNum AND (@RntNum IS NULL OR RntNum = @RntNum) 
		/*AND RntStatus NOT IN ('XX','XN')*/ 
		-- KX Changes :RKS
		AND	(@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
											INNER JOIN dbo.Company (NOLOCK)	ON Brand.BrdComCode = Company.ComCode
											INNER JOIN dbo.UserCompany (NOLOCK)	ON UserCompany.ComCode = Company.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK)	ON UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode)
			)
		ORDER BY RntStatus, RntNum
		IF @RntId is null
		BEGIN
			IF @RntNum IS NOT NULL AND @RntNum <> ''
				SET @BooNum = @BooNum + '/' + @RntNum
			EXEC sp_get_ErrorString  'GEN095', @BooNum, 'Booking Number', @oparam1 = @sError OUTPUT
			SELECT @sError
			RETURN
		END

--		UPDATE Rental WITH(ROWLOCK)
--			SET RntIsError = 1
--		WHERE RntId = @RntId

		SELECT	
			RntId																													AS	RntId ,
			RntNum																													AS	RentalNo ,
			CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D') + ' ' +  RntCkoLocCode	AS	'Check-Out',
			CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D') + ' ' +  RntCkiLocCode	AS	'Check-In' ,
			ISNULL(RntNumOfHirePeriods, 0)																							AS	HirePd ,
			ISNULL(Package.PkgCode,'')																								AS	Package ,
			''+ ISNULL(BrdName,'')																									AS	Brand ,
			''+ ISNULL((SELECT TOP 1	PrdShortName 
							FROM 	Product WITH (NOLOCK)
							WHERE 	PrdId = RntPrdRequestedVehicleId), '')																			AS	Vehicle ,
			RntStatus																												AS	Status ,
			''+ BooNum																												AS	BooNum ,
			''+ AgnCode + ' - ' + CASE 	WHEN ISNULL(BooMiscAgentName,'') = '' THEN
											AgnName
								  		ELSE
											AgnName
								  END + ' - ' + dbo.GEN_getCodDesc(AgnCodDebtStatId,NULL,NULL) AS	AgnName ,
			''+ ISNULL(BooLastName,'') + ', ' + ISNULL(BooTitle,'') +  ' ' + ISNULL(BooFirstName,'') 								AS	Hirer ,
			'' + ISNULL(BooStatus,'')																								AS	BookingSt ,
			'' + BooId																												AS	BooId ,
			(SELECT count(x.rntid) FROM Rental  x with (nolock) WHERE x.RntBooId = BooId)																	AS	NoOfRnt,
			--Added by Shoel on 6/11/7 > for the Integrity number issue
			'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo		
		FROM	Rental WITH (NOLOCK), Package WITH (NOLOCK), Brand WITH (NOLOCK), /*Product WITH (NOLOCK),*/ Booking WITH (NOLOCK), Agent WITH (NOLOCK)
		WHERE 	BooAgnId = AgnId
		AND		RntBooId = BooId
		AND		Package.PkgId = Rental.RntPkgId
		AND		Brand.BrdCode = Package.PkgBrdCode
		--AND		Product.PrdId = Rental.RntPrdRequestedVehicleId
		AND 	boonum = @BooNum
		AND		Rental.RntId = @RntId
		-- KX Changes :RKS
		AND	(@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
											INNER JOIN dbo.Company (NOLOCK)	ON Brand.BrdComCode = Company.ComCode
											INNER JOIN dbo.UserCompany (NOLOCK)	ON UserCompany.ComCode = Company.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK)	ON UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode)
			)
		
		FOR XML AUTO, ELEMENTS				
	END

	IF(@Condition = 'ModBooking')
	BEGIN
		SELECT     
			ISNULL(Booking.BooId,'')					AS	BooId ,
			ISNULL(Booking.BooFirstName,'') 		AS FName, 
			ISNULL(Booking.BooLastName,'') 		AS LName, 
			ISNULL(Booking.BooTitle,'') 				AS Title, 
			ISNULL(Booking.BooAgnId,'')			AS AgnId ,
			ISNULL(Booking.BooAgnId,'')			AS OLD_AgnId ,
			ISNULL(AgnIsMisc,0)					AS	AgnIsMisc,
			CASE ISNULL(AgnIsMisc,0)
				WHEN 0 
					THEN Agent.AgnCode + ' - ' +  Agent.AgnName +  
						CASE ISNULL(Agent.AgnIsVip,0) 
							WHEN 0 THEN ''
							ELSE ' - ' 
						END + 
						CASE ISNULL(Agent.AgnIsVip,0)
							WHEN 0  THEN ''
							ELSE 'VIP'
						END
				ELSE Agent.AgnCode + ' - ' + AgnName
			END														AS		AgnDet, 
			ISNULL(dbo.GEN_GetCodDesc('',42,BooStatus),'')					AS		ST, 
			ISNULL(CONVERT(VARCHAR,Booking.BooEarliestRentalCkoDate,103),'')		AS		CkoDt, 
			ISNULL(CONVERT(VARCHAR,Booking.BooLatestRentalCkiDate,103),'')		AS		CkiDt, 
			ISNULL(CONVERT(VARCHAR,Booking.BooXfrdToFinanWhen,103),'')		AS		FinWhen ,
			ISNULL(CONVERT(VARCHAR, Booking.BooCancelWhen,103),'')			AS		CanWhen, 
			ISNULL(Booking.BooOldBooNum,'')							AS		OldBooNum ,
			ISNULL(BooCodReferralTypId,'')							AS		RefType ,
			Booking.IntegrityNo										AS		IntNo ,
			ISNULL((SELECT 
						AtyIsDirect 
					FROM 
						Agent (NOLOCK), 
						AgentType (NOLOCK) 
					WHERE 
						AgnAtyId = AtyId 
						AND AgnId = BooAgnId),0) AS AgnIsDirect
										
		FROM         Booking WITH (NOLOCK) INNER JOIN                     Agent WITH (NOLOCK) ON Booking.BooAgnId = Agent.AgnId
		WHERE
			BooId = @BooId
		FOR XML AUTO, ELEMENTS	
	
	END		-- IF(@Condition = 'ModBooking')

	IF(@Condition = 'CKOCKIGET')
	BEGIN
		SELECT
			''+BooId																		AS	BoId,
			''+RntId																		AS	RntId,
			''+BooNum																		AS	BooNum,
			''+RntNum																		AS	RentalNo,
			''+RntStatus																	AS	Status,
			''+ISNULL(BooStatus,'')															AS	BookingSt,
			''+dbo.getCountryForLocation(RntCkoLocCode,NULL,'')								AS	CtyCode,
			''+CASE
				--WHEN CAST(CONVERT(VARCHAR(10), RntCkiWhen, 103) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103) AS DATETIME)
				WHEN RntCkiWhen <= GETDATE()
					THEN '0'
					ELSE '1'
			   END																			AS	DVASSCall,
			--Added by Shoel on 6/11/7 > for the Integrity number issue
			'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo		
		FROM	Rental WITH (NOLOCK), Booking WITH (NOLOCK)
		WHERE 	RntBooId = BooId
		AND		RntId = @RntId
		FOR XML AUTO, ELEMENTS		
	END	--	IF(@Condition = 'CKOCKIGET')
END	--	END OF MAIN BEGIN

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON



