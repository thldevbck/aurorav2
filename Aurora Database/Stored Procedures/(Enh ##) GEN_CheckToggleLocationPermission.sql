CREATE PROC GEN_CheckToggleLocationPermission
	@UserCode AS VARCHAR(24)
AS

BEGIN
	SET NOCOUNT ON
	
	DECLARE @l_UserId AS VARCHAR(64)
	
	SELECT     
		@l_UserId = UsrId
	FROM         
		UserInfo WITH (NOLOCK) 
	WHERE     
		UsrCode = @UserCode

	--Role Permission Check
	IF EXISTS (SELECT 1 FROM UserRole,Role WHERE UserRole.UsrId = @l_UserId AND UserRole.RolId = Role.RolId AND Role.RolCode = 'TOGGLELOCN')
		BEGIN
			SELECT 'YES'
		END
	ELSE
		BEGIN
			SELECT 'NO'
		END

	RETURN
END