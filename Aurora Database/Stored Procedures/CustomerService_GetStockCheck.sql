CREATE PROCEDURE [dbo].[CustomerService_GetStockCheck]
	@ComCode AS varchar(64),
	@LivId AS varchar(64)
AS

SET NOCOUNT ON

SELECT
	LocationInventory.*
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId


SELECT
	UserInfo.*
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
	INNER JOIN UserInfo ON LocationInventory.AddUsrId = UserInfo.UsrId OR LocationInventory.ModUsrId = UserInfo.UsrId
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId


SELECT
	CheckedProductItem.*
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
	INNER JOIN CheckedProductItem ON LocationInventory.LivId = CheckedProductItem.PriLivId
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId
ORDER BY
	CheckedProductItem.PriUnitNum


SELECT
	CheckedProductItem.PriId,
	FleetAsset.UnitNumber,
	FleetAsset.RegistrationNumber
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
	INNER JOIN CheckedProductItem ON LocationInventory.LivId = CheckedProductItem.PriLivId
	INNER JOIN AimsProd.dbo.FleetAsset AS FleetAsset ON CheckedProductItem.PriUnitNum = FleetAsset.UnitNumber
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId
ORDER BY
	CheckedProductItem.PriUnitNum


SELECT DISTINCT
	LocationInventory.LivId                                             AS LivId,
	ISNULL (FR.RepairId, '')											AS RepairId,
	ISNULL (FR.FleetAssetId, '')										AS FleetAssetId,
	FR.StartDateTime													AS StartDateTime,
	FR.EndDateTime														AS EndDateTime,
	ISNULL (FR.ReasonDescription, '')									AS ReasonDescription,
	ISNULL (FR.StartOdometer,0)											AS StartOdometer,
	ISNULL (FR.OtherProvider, '')										AS OtherProvider,
	ISNULL (FA.UnitNumber, '')											AS UnitNumber,
	ISNULL(FA.RegistrationNumber, '')									AS RegistrationNumber,
	ISNULL (CONVERT(varchar,SSS.OdometerDue) + 'K', '')					AS OdometerDue,
	ISNULL (RV.ReferenceDescription, '')								AS ReferenceDescription,
	ISNULL (FRR.RepairerName, '')										AS RepairerName
FROM
	Location
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
	INNER JOIN aimsprod.dbo.AI_Location AS LOC ON Location.LocCode = LOC.LocationCode
	INNER JOIN aimsprod.dbo.AI_FleetRepair AS FR ON LOC.LocationId = FR.LocationId
	LEFT JOIN aimsprod.dbo.AI_RefValues AS RV ON FR.ReasonCode = RV.ReferenceCode
	LEFT JOIN aimsprod.dbo.AI_ServiceScheduleService AS SSS ON SSS.ServiceScheduleServiceId = FR.ServiceId
	INNER JOIN aimsprod.dbo.FleetAsset AS FA ON FR.FleetAssetId = FA.FleetAssetId
	INNER JOIN aimsprod.dbo.AI_Repairer AS FRR ON FR.ServiceProvider = FRR.RepairerCode
WHERE
	Location.LocComCode = @ComCode
	AND LocationInventory.LivId = @LivId
	AND LocationInventory.LivStatus = 'In Progress'
	AND FR.Completed = 0
ORDER BY
	FR.EndDateTime



GO
