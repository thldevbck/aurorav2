USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetTravelYears]    Script Date: 11/19/2007 13:44:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Flex2_GetTravelYears] 
AS

SELECT 
	*
FROM 
	FlexTravelYearsView
GROUP BY
	TravelYear

