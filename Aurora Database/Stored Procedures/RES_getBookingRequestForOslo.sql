create	PROCEDURE [dbo].[RES_getBookingRequestForOslo]
	@vhrId		VARCHAR(64) = '',
	@booId		VARCHAR(64) = '',
	@sUsrCode	VARCHAR(24) = ''
AS

SET NOCOUNT ON

-- Local variables Start
DECLARE @comFtr		varchar(500),
		@nonComFtr	varchar(500),
		@BkrConId	varchar(64),
		@AgnIsMisc	bit
DECLARE	@BooNum			VARCHAR(64),	@BooAgnId	VARCHAR(64),
		@BooAgnCode 	VARCHAR(64),	@BooAgnName VARCHAR(124),
		@BooAgnIsMisc 	BIT
-- Local variables End

-- Assigining Default values for the local variables Start
SET @comFtr = ''
SET @nonComFtr = ''
-- Assigining Default values for the local variables End


-- If Vehicle Request Exists
IF EXISTS(SELECT VhrId FROM VehicleRequest WITH (NOLOCK)
			WHERE 	VhrId = @vhrId)
BEGIN
	SELECT @comFtr = @comFtr + ',' + VrfFtrId FROM VehicleRequestFeature WITH (NOLOCK)
							WHERE 	VrfVhrId = @vhrId
							AND	VrfFtrId IN (SELECT FtrId FROM Feature WITH (NOLOCK)
										WHERE 	FtrIsCommon = 1)
	SELECT @nonComFtr = @nonComFtr + ',' + VrfFtrId FROM VehicleRequestFeature WITH (NOLOCK)
							WHERE 	VrfVhrId = @vhrId
							AND	VrfFtrId IN (SELECT FtrId FROM Feature WITH (NOLOCK)
										WHERE 	FtrIsCommon = 0)
	SET @comFtr = SUBSTRING(@comFtr, 2, LEN(@comFtr))
	SET @nonComFtr = SUBSTRING(@nonComFtr, 2, LEN(@nonComFtr))

	SELECT	@BooNum = BooNum,
			@BooAgnId = BooAgnId,
			@BooAgnCode = AgnCode,
			@BooAgnName = AgnName,
			@BooAgnIsMisc = AgnIsMisc
		FROM	Booking WITH(NOLOCK), Agent WITH(NOLOCK)
		WHERE	BooAgnId = AgnId
		AND		BooId = @booId

	SELECT 	@AgnIsMisc = ISNULL(AgnIsMisc, 0),
			@BkrConId = BkrConId
		FROM 	Agent WITH (NOLOCK), BookingRequest WITH (NOLOCK), VehicleRequest WITH (NOLOCK)
		WHERE 	VhrId = @vhrId
		AND		BkrId = VhrBkrId
		AND		BkrAgnId = AgnId

	SELECT
		VhrId															AS	VhrId,
		ISNULL(@booId, '')												AS	booid,
		ISNULL(@BooNum, '')												AS	bookingnum,
		ISNULL(VhrBkrId, '')											AS	bkrid,
		''+BR.IntegrityNo												AS	bkrIntegrityNo,
		CASE WHEN ISNULL(@booId, '') = ''
			THEN	ISNULL(BR.BkrAgnId, '')
			ELSE	ISNULL(@BooAgnId, '')
		END																AS	agnId,
		CASE WHEN ISNULL(@booId, '') = ''
			THEN	ISNULL(AgnCode+' - '+AgnName, '')
			ELSE	ISNULL(@BooAgnCode+' - '+@BooAgnName, '')
		END																AS	agent,
		CASE WHEN ISNULL(@booId, '') = ''
			THEN	ISNULL(AgnIsMisc, 0)
			ELSE	ISNULL(@BooAgnIsMisc, 0)
		END																AS	agentIsMisc,
		ISNULL(BR.BkrMiscAgentName, '')									AS	miscAgnName,
		CASE
			WHEN @AgnIsMisc = 1
			THEN	ISNULL(BR.BkrMiscAgentContact,'')
			ELSE	ISNULL(@BkrConId, '')
		END																AS	contact,
		ISNULL(VhrAgnRef, '')											AS	referance,
		ISNULL(VhrPrdId, '')											AS	productid,
		ISNULL((SELECT PrdShortName + ' - ' + PrdName
					FROM 	Product WITH (NOLOCK)
					WHERE 	PrdId = VhrPrdId), '')						AS	productname,
		ISNULL(VhrNumOfVehicles, '')									AS 	numvehicle,
		ISNULL(VhrBrdCode, '')											AS	brand,
		ISNULL(VhrClaId, '')											AS	class,
		ISNULL(VhrTypId, '')											AS	type,
		@comFtr															AS	commonfeature,
		@nonComFtr														AS	noncommonfeature,
		ISNULL(	VhrCkoLocCode +' - '+ (SELECT LocName
										FROM Location WITH (NOLOCK)
										WHERE LocCode = VhrCkoLocCode), '')	AS	checkoutbranch,
		ISNULL(LEFT(DATENAME(DW, VhrCkoDate), 3), '')					AS 	coDw,

		--rev:mia
		--CONVERT(varchar(10), VhrCkoDate, 103) 							AS	checkoutdate,
		VhrCkoDate 							AS	checkoutdate,


		ISNULL(VhrCkoAmPm, '')											AS	checkoutampm,
		ISNULL(	VhrCkiLocCode +' - '+ (SELECT LocName
										FROM 	Location WITH (NOLOCK)
										WHERE 	LocCode = VhrCkiLocCode), '')	AS	checkinbranch,
		ISNULL(LEFT(DATENAME(DW, VhrCkiDate), 3), '')					AS 	ciDw,

		--rev:mia
		--CONVERT(varchar(10), VhrCkiDate, 103) 							AS	checkindate,
		VhrCkiDate 							AS	checkindate,




		ISNULL(VhrCkiAmPm, '')											As	checkinampm,
		ISNULL(VhrHirePeriod, '')										AS	hireperiod,
		ISNULL(VhrHirePeriodUom, '')									AS	hiretype,
		ISNULL(BR.BkrLastName, '')										AS	hirersurname,
		ISNULL(BR.BkrFirstName, '')										AS	hirerfirstname,
		ISNULL(BR.BkrTitle, '')											AS	hirertitle,
		CASE ISNULL(RTRIM(CAST(VhrNumOfAdults AS CHAR)), '')
			WHEN '0' THEN '0'
			ELSE ISNULL(RTRIM(CAST(VhrNumOfAdults AS CHAR)), '')
		END																AS	adult,
		CASE ISNULL(RTRIM(CAST(VhrNumOfChildren AS CHAR)), '')
			WHEN '0' THEN '0'
			ELSE ISNULL(RTRIM(CAST(VhrNumOfChildren AS CHAR)), '')
		END 															AS	childern,
		CASE ISNULL(RTRIM(CAST(VhrNumOfInfants AS CHAR)), '')
			WHEN '0' THEN '0'
			ELSE ISNULL(RTRIM(CAST(VhrNumOfInfants AS CHAR)), '')
		END 															AS	infants,
		ISNULL(VhrPkgId,'')												AS	pkgid,
		ISNULL((SELECT PkgCode+' - '+PkgName
					FROM	Package WITH (NOLOCK)
					WHERE 	PkgId = VhrPkgId), '')						AS	package,
		ISNULL(VhrCodSourceMocId, '')									AS	requestsource,
		ISNULL(VhrRequestType, '')										AS	buttonclicked,
		CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)					AS	today,
		ISNULL(DATEDIFF(DAY, CONVERT(DATETIME,CURRENT_TIMESTAMP,103), CONVERT(DATETIME,VhrCkoDate,103)),'') AS tillDays
	FROM VehicleRequest WITH(NOLOCK), BookingRequest BR WITH(NOLOCK), Agent WITH(NOLOCK)
	WHERE 	BkrId = VhrBkrId
	AND		BkrAgnId = AgnId
	AND		VhrId = @vhrId
	FOR XML AUTO,ELEMENTS
END
ELSE IF EXISTS(SELECT BooId
					FROM 	Booking WITH (NOLOCK)
					WHERE	BooId = @booId)
BEGIN
	SELECT	''																	AS	VhrId,
			@booId																AS	booid,
			BooNum																AS	bookingnum,
			''																	AS	bkrid,
			1																	AS	bkrIntegrityNo,
			BooAgnId															AS	agnId,
			ISNULL(AgnCode+' - '+AgnName, '')									AS	agent,
			ISNULL(AgnIsMisc, 0)												AS	agentIsMisc,
			ISNULL(BooMiscAgentName, '')										AS	miscAgnName,
			ISNULL(BooMiscAgentContact,'')										As	contact,
			''																	AS	referance,
			''																	AS	productid,
			''																	AS	productname,
			1																	AS	numvehicle,
			''																	AS	brand,
			''																	AS	class,
			''																	AS	type,
			''																	AS	commonfeature,
			''																	AS	noncommonfeature,
			''																	AS	checkoutbranch,
			''																	AS	checkoutdate,
			'AM'																AS	checkoutampm,
			''																	AS	checkinbranch,
			''																	AS	checkindate,
			'PM'																AS	checkinampm,
			''																	AS	hireperiod,
			'1'																	AS	hiretype,
			''																	AS	hirersurname,
			''																	AS	hirerfirstname,
			''																	AS	hirertitle,
			''																	As	adult,
			''																	AS	childern,
			''																	AS	infants,
			''																	As	pkgid,
			''																	AS	package,
			''																	AS	requestsource,
			''																	AS	buttonclicked,
			CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)						AS	today,
			''																	AS  tillDays
		FROM	Booking   VehicleRequest WITH (NOLOCK) LEFT OUTER JOIN Agent WITH (NOLOCK) ON AgnId = BooAgnId
		WHERE	BooId = @booId
		FOR XML AUTO, ELEMENTS
END -- ELSE IF EXISTS(SELECT IntegrityNo
ELSE
BEGIN
	UPDATE UserInfo WITH (ROWLOCK)
		SET UsrVehicleRequestCount = 0
	WHERE	UsrCode = @sUsrCode
	SELECT	'<VehicleRequest>
				<VhrId/>
				<booid/>
				<bookingnum/>
				<bkrid/>
				<bkrIntegrityNo/>
				<agnId/>
				<agent/>
				<agentIsMisc/>
				<miscAgnName/>
				<contact/>
				<referance/>
				<productid/>
				<productname/>
				<numvehicle>1</numvehicle>
				<brand/>
				<class/>
				<type/>
				<commonfeature/>
				<noncommonfeature/>
				<checkoutbranch/>
				<checkoutdate/>
				<checkoutampm>AM</checkoutampm>
				<checkinbranch/>
				<checkindate/>
				<checkinampm>PM</checkinampm>
				<hireperiod/>
				<hiretype>1</hiretype>
				<hirersurname/>
				<hirerfirstname/>
				<hirertitle/>
				<adult/>
				<childern/>
				<infants/>
				<pkgid/>
				<package/>
				<requestsource/>
				<buttonclicked/>
				<today>' + CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103) + '</today>
				<tillDays/>
			</VehicleRequest>'
END




GO
