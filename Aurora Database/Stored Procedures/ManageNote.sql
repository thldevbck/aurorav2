CREATE          Procedure [dbo].[ManageNote]
	@pNteId 		VARCHAR	(64) 	= '',
	@pNteBooId 		VARCHAR	(64)	= '',
	@pNteBooNo 		VARCHAR	(64)	= '',
	@pNteRntNo 		VARCHAR	(64) 	= '',
	@pNteCodTypId 	VARCHAR	(64)	= '',
 @pNteDesc   VARCHAR (2048) = '',  --REV:MIA: change from 1000 to 2048
	@pNteCodAudTypId 	VARCHAR	(64)	= '',
	@pNtePriority		INT,
	@pNteIsActive		BIT,
	@pIntegrityNo		INT,
	@pUserName		VARCHAR	(64),
	@pAddPrgmName		VARCHAR	(64)
AS
DECLARE @sErrMsg		VARCHAR	(255),
--		@NteBooNo 		VARCHAR	(64),
		@NteRntId 		VARCHAR	(64),
		@UserId			VARCHAR (64),
		@RetNteId		VARCHAR	(64),
		-- KX Changes :RKS
		@sComCode		VARCHAR	(64)


SET NOCOUNT ON
--Validate Booking and get booking id from booking number
IF (@pNteBooNo = '')
BEGIN
	EXEC sp_get_ErrorString  'GEN015','Booking No', @oparam1 = @sErrMsg OUTPUT
	SELECT 'GEN015/'+@sErrMsg
	RETURN
END
ELSE
BEGIN
	IF NOT (EXISTS(SELECT BooId FROM dbo.Booking WITH(NOLOCK)
				WHERE BooNum = @pNteBooNo))
	BEGIN
		EXEC sp_get_ErrorString  'GEN003', @pNteBooNo, 'Booking No', @oparam1 = @sErrMsg OUTPUT
		SELECT 'GEN003/'+@sErrMsg
		RETURN
	END
	SELECT @pNteBooID = BooId
		FROM dbo.Booking WITH(NOLOCK)
		WHERE BooNum = @pNteBooNo
END

-- KX Changes :RKS
SELECT @sComCode = dbo.fun_getCompany
						(
							@pUserName,	--@sUserCode	VARCHAR(64),
							NULL,		--@sLocCode	VARCHAR(64),
							NULL,		--@sBrdCode	VARCHAR(64),
							NULL,		--@sPrdId		VARCHAR(64),
							NULL,		--@sPkgId		VARCHAR(64),
							NULL,		--@sdummy1	VARCHAR(64),
							NULL,		--@sdummy2	VARCHAR(64),
							NULL		--@sdummy3	VARCHAR(64)
						)
--Validate Rental and get Rental id from rental number
IF @pNteRntNo = ''
BEGIN
	/*
	EXEC sp_get_ErrorString  'GEN015','Rental No', @oparam1 = @sErrMsg OUTPUT
	SELECT 'GEN015/'+@sErrMsg
	RETURN
	*/
	SET @NteRntId = NULL
END
ELSE
BEGIN
	IF NOT (EXISTS(SELECT RntId FROM dbo.Rental WITH(NOLOCK)
				WHERE Rntbooid = @pNteBooID and RntNum = @pNteRntNo))
	BEGIN
		EXEC sp_get_ErrorString  'GEN003',@pNteRntNo,'Rental No', @oparam1 = @sErrMsg OUTPUT
		SELECT 'GEN003/'+@sErrMsg
		RETURN
	END
	SELECT	@NteRntId = RntId
		FROM	dbo.Rental WITH (NOLOCK) , dbo.Booking  WITH (NOLOCK)
		WHERE	BooNum = @pNteBooNo
		AND	RntBooId = BooId
		AND	RntNum = @pNteRntNo
END
--Get user id from user code
SELECT	@UserId = UsrId
	FROM	dbo.UserInfo WITH(NOLOCK)
	WHERE	UsrCode = @pUserName

IF RTRIM(LTRIM(@pNteId)) = ''
BEGIN --Add Note
	SET @RetNteId	=	NEWID()
	INSERT INTO note  WITH (ROWLOCK) (
		NteId,
		NteBooID,
		NteRntId,
		NteCodTypId,
		NteDesc,
		NteCodAudTypId,
		NtePriority,
		NteIsActive,
		IntegrityNo,
		AddUsrId,
		AddDateTime,
		AddPrgmName,
		-- KX Changes :RKS
		NteComCode
	)
	values(
		@RetNteId,
		@pNteBooID,
		@NteRntId,
		@pNteCodTypId,
		@pNteDesc,
		@pNteCodAudTypId,
		@pNtePriority,
		@pNteIsActive,
		1,
		@UserId,
		dbo.GEN_getAdjustedTimeForUser(@UserId, CURRENT_TIMESTAMP),
		@pAddPrgmName,
		-- KX Changes :RKS
		@sComCode
	)
	EXEC sp_get_ErrorString  'GEN045', @oparam1 = @sErrMsg OUTPUT
	SELECT 'GEN045/'+@sErrMsg+'/'+@RetNteId
	RETURN
END
ELSE
BEGIN
	/*Update Note*/
	SET @RetNteId	=	@pNteId

	DECLARE @IntNo	int
	SET @IntNo = -10

	SELECT	@IntNo = IntegrityNo
	FROM		dbo.note  WITH (NOLOCK)
	WHERE		NteId = @pNteId

	IF(@IntNo=-10)
	/*Record has been deleted*/
	BEGIN
		EXEC sp_get_Errorstring 'GEN008', @pNteId, @oparam1 = @sErrMsg OUTPUT
		SELECT 'GEN008/' + @sErrMsg
		RETURN
	END

	IF (@IntNo<>@pIntegrityNo)
	/*Record has been updated by another user*/
	BEGIN
		EXEC sp_get_Errorstring 'GEN002',@oparam1 = @sErrMsg OUTPUT
		SELECT 'GEN002/' + @sErrMsg
		RETURN
	End

	/***************** Update the note Table *******/
	UPDATE	dbo.note  WITH (ROWLOCK)
	SET		NteBooID = @pNteBooID,
			NteRntId = @NteRntId,
			NteCodTypId = @pNteCodTypId,
			NteDesc = @pNteDesc,
			NteCodAudTypId = @pNteCodAudTypId,
			NtePriority = @pNtePriority,
			NteIsActive = @pNteIsActive,
			IntegrityNo = IntegrityNo + 1,
			ModUsrId = @UserId,
			ModDateTime = dbo.GEN_getAdjustedTimeForUser(@UserId, CURRENT_TIMESTAMP),
			AddPrgmName = @pAddPrgmName
	WHERE	NteId = @pNteId
	EXEC sp_get_ErrorString  'GEN046', @oparam1 = @sErrMsg OUTPUT
	SELECT 'GEN046/'+@sErrMsg+ '/'+@RetNteId
	RETURN
END
GO
