
create          PROCEDURE [dbo].[RES_CreateAvailableVehicle_OSLO]
		@sVhrId			VARCHAR(64)	=	'',
		@sCkoLocCode	VARCHAR(12)	=	'',
		@sCkoDate		VARCHAR(20)	=	'',
		@sCkoAmPm		VARCHAR(3)	=	'',
		@sCkiLocCode	VARCHAR(12)	=	'',
		@sCkiDate		VARCHAR(20)	=	'',
		@sCkiAmPm		VARCHAR(3)	=	'',
		@sHirePeriod	VARCHAR(64)	=	'',
		@sHirePeriodUom	VARCHAR(64)	=	'',
		@sUserCode		VARCHAR	(64)=	'TEST',
		@sPrgmName		VARCHAR	(2000)=	'TEST',
		@DVASSResult	TEXT		=	DEFAULT
AS
BEGIN
	SET NOCOUNT ON
	--	GENERAL VARIABLE DECLARATION

	DECLARE	@doc				INT,				@ID 		VARCHAR(64) ,		@ckoLoc 	VARCHAR(12), 
			@ckoDate 			VARCHAR(20),		@ckoAMPM 	VARCHAR(3),			@ckiLoc 	VARCHAR(12),
			@ckiDate 			VARCHAR(20),		@ckiAMPM	VARCHAR(3),			@NumAvail	INT,
			@rel				VARCHAR(64),		@pref		VARCHAR(64),		@AvvIsReverseDirection	BIT,
			@TotNoOfVehicle		INT, 				@VhrPrdId 	VARCHAR(64),		@VhrBrdCode	VARCHAR(1),
			@VhrClaID			VARCHAR(64),		@VhrTypID	VARCHAR(64),		@AvvBrdCode	VARCHAR(1),				
			@AvvPrdId			VARCHAR(64),		@AvvIsCriteriaMatched	BIT,	@AvvAvoidsRelocation	BIT,
			@AvvPriority		BIT,				@ErrStr		VARCHAR(2000),		@sUserId	VARCHAR(64),
			@DvsPrdId 			VARCHAR(64),		@DvsBrdCode VARCHAR(1),			@AvvClaId 	VARCHAR(64),
			@AvvTypId 			VARCHAR(64),		@AvvId		VARCHAR(64),		@VrfFtrId	VARCHAR(64),
			@dCkoTime			datetime,			@dCkiTime	datetime,			@dCkoDate	datetime,
			@dCkiDate			datetime,			@AgnId		VARCHAR(64),		@dCurrDate	DATETIME,
			@ReturnError		VARCHAR(124),		@ReturnStatus	VARCHAR(12),	@ReturnWarning	VARCHAR(124),
			@RtnBlrIdList		VARCHAR(500),		@iDateDiff  	int

	SELECT 	@dCkoDate = Convert(datetime,@sCkoDate),
			@dCkiDate = Convert(datetime,@sCkiDate),
			@dCurrDate= GETDATE()

	SELECT	@VhrPrdId 	= VhrPrdId,
			@VhrBrdCode = VhrBrdCode,
			@VhrClaId 	= VhrClaId,
			@VhrTypId 	= VhrTypId,
			@sCkoLocCode= VhrCkoLocCode,
			@sCkiLocCode= VhrCkiLocCode,
			@AgnId 		= BkrAgnId
		FROM	VehicleRequest WITH(NOLOCK), BookingRequest WITH(NOLOCK)
		WHERE	VhrBkrId = BkrId
		AND		VhrId  = @sVhrId
	--	End of General Variable Declaration

	IF not (cast(@sHirePeriod as int)  between datediff(dd,@dCkoDate,@dCkiDate) and datediff(dd,@dCkoDate,@dCkiDate)+1)
	BEGIN
		exec ADM_getDateDiffNew @sCkoDate, 
						@sCkoAmPm, 
						@sCkiDate, 
						@sCkiAmPm, 
						'' , 
						'1', 
						'0',
						@iDateDiff output
		IF @iDateDiff is not null and @iDateDiff <> 0
			SET @sHirePeriod = @iDateDiff
	END

	--	Fetch UserId from UserInfo table
	SELECT	@sUserId = UsrId
		FROM	UserInfo WITH (NOLOCK) 
		WHERE	UsrCode = @sUserCode
	SET @sUserId = ISNULL(@sUserId, 'subb')	
	--	ALTER  THE XML DOCUMENT
	EXEC SP_XML_PREPAREDOCUMENT @doc OUTPUT,@DVASSResult
	IF @@ERROR<>0 GOTO DestroyCursor

	--	FETCH DATA FROM XML DOCUMENT INTO CURSOR
	DECLARE Cur_DVASSResult CURSOR LOCAL FAST_FORWARD FOR
		SELECT ID, ckoLoc, ckoDate, ckoAMPM, ckiLoc, ckiDate, ckiAMPM, NumAvail, rel, pref
			FROM	OPENXML(@doc, '/Vehicle/veh',3)
			WITH(
				ID 			VARCHAR(64),
				ckoLoc 		VARCHAR(12), 
				ckoDate 	VARCHAR(20) ,
				ckoAMPM 	VARCHAR(3) ,
				ckiLoc 		VARCHAR(12) ,
				ckiDate 	VARCHAR(20) ,
				ckiAMPM		VARCHAR(3), 
				NumAvail 	INT,
				rel			VARCHAR(64) ,
				pref		VARCHAR(64)
				)

	--	OPEN CUSROR	
	OPEN Cur_DVASSResult

	--	REMOVE THE XML DOCUMENT
	EXEC SP_XML_REMOVEDOCUMENT @doc
	IF @@ERROR<>0 GOTO DestroyCursor
	
	FETCH NEXT FROM Cur_DVASSResult INTO @ID,@ckoLoc, @ckoDate, @ckoAMPM, @ckiLoc, @ckiDate, @ckiAMPM, @NumAvail, @rel ,  @pref
	WHILE @@FETCH_STATUS=0
	BEGIN
		--	FETCH PrdId FROM PRODUCT TABLE
		SELECT	@AvvPrdID = NULL,
				@ReturnError = NULL,
				@ReturnStatus = NULL,
				@RtnBlrIdList = NULL

		SELECT	@AvvPrdId = ISNULL(PrdId,'') ,
				@AvvBrdCode = ISNULL(PrdBrdCode,''),
				@AvvTypid = ISNULL(PrdTypID,''),
				@AvvClaID  = ISNull(TypClaID, '')
		FROM	Product WITH (NOLOCK) LEFT OUTER JOIN Type WITH (NOLOCK) ON PrdTypID = TypID
		WHERE	PrdDvassSeq = @ID
		--	Call Blocking/Allowing Rule
		EXECUTE	RES_manageBlockingAndAllowingRules
			@LoginUserID		=	@sUserCode,			--	VARCHAR(64),
			@ProgramName		=	@sPrgmName,			--	VARCHAR(500),
			@AgnId				=	@AgnId,				--	VARCHAR(64),
			@CkoDate			=	@sCkoDate,			--	DATETIME,
			@CkiDate			=	@sCkiDate,			--	DATETIME,
			@HirePeriod			=	@sHirePeriod,		--	BIGINT,
			@CkoLocation		=	@sCkoLocCode,		--	VARCHAR(64),
			@CkiLocation		=	@sCkiLocCode,		--	VARCHAR(64),
			@BookedDate			=	@dCurrDate,			--	DATETIME,
			@PrdId				=	@AvvPrdId,			--	VARCHAR(64),
			@PkgId				= 	NULL,
			@VhrId				= 	NULL,
			@ReturnError		=	@ReturnError	OUTPUT,	--	VARCHAR(500)	OUTPUT,
			@ReturnStatus		=	@ReturnStatus	OUTPUT,	--	VARCHAR(500)	OUTPUT,
			@ReturnBlrIdList	=	@RtnBlrIdList	OUTPUT	--	VARCHAR(500)	OUTPUT
	
		--	ONLY continue if no errors
		IF @ReturnError IS NOT NULL AND @ReturnError <> ''
		BEGIN
			EXECUTE GEN_manageBlockingMessage
				@sMessage 		= @ReturnError,
				@bWarningOnly 	= 1,
				@sRetryMethod 	= NULL,
				@sNextScreen 	= 'VehicleRequestMgt.asp',
				@sWarnings 		= @ReturnWarning OUTPUT
			IF (@ReturnWarning IS NOT NULL AND @ReturnWarning <> '')
			BEGIN
				INSERT INTO ZZLOG(ZZLOGTEXT) VALUES('SUBBAIAH ' + @ReturnError)
				SELECT @ReturnWarning
			END
			GOTO NextCursor
		END -- IF @sError IS NOT NULL AND @sError <> ''

		-- Determine if Reverse
		IF((@ckoLoc = @sCkiLocCode) AND (@ckiLoc = @sCkoLocCode) AND (@ckoloc <> @ckiloc))
			SET @AvvIsReverseDirection = 1
		ELSE
			SET @AvvIsReverseDirection = 0

		-- Check if vehicle is scheduled		
/*		IF dbo.RES_IsDVASSVehicleSchedule(@AvvPrdId, @sVhrId) = 1
		--IF 0 = 1
			SET @TotNoOfVehicle = 0
		ELSE */
			SET @TotNoOfVehicle = @numavail			

		/* The Criteria Matched  Fals should show true if any Product Filter Criteria selected on the Booking Request 
		Screen (Brand, Class, Type or features) are the same for the dvass returned Product */
		IF @VhrPrdID = @AvvPrdID
			SET @AvvIsCriteriaMatched = 1
		ELSE
			SET @AvvIsCriteriaMatched = 0
		IF @VhrBrdCode IS NOT NULL OR @VhrClaID IS NOT NULL OR @VhrTypID IS NOT NULL OR
			EXISTS (SELECT VrfVhrID FROM VehicleRequestFeature WITH (NOLOCK) WHERE VrfVhrID = @sVhrID)
		BEGIN
				IF @VhrBrdCode IS NOT NULL AND @VhrBrdCode <> @AvvBrdCode
					SET @AvvIsCriteriaMatched = 0
				IF @VhrTypID   IS NOT NULL AND @VhrTypID <> @AvvTypID
					SET @AvvIsCriteriaMatched = 0
				IF @VhrClaID   IS NOT NULL AND @VhrClaID <> @AvvClaID
					SET @AvvIsCriteriaMatched = 0
				IF EXISTS (SELECT VrfVhrID FROM VehicleRequestFeature WITH (NOLOCK) WHERE VrfVhrID = @sVhrID)
				BEGIN
					DECLARE CurVrf CURSOR local fast_forward FOR 
						SELECT VrfFtrID FROM VehicleRequestFeature WITH(NOLOCK) WHERE VrfVhrID = @sVhrID
					OPEN CurVrf
					FETCH NEXT FROM CurVrf INTO @VrfFtrID
					WHILE @@fetch_status > -1
					BEGIN
						IF NOT EXISTS (SELECT PftPrdID FROM ProductFeatures WITH (NOLOCK) WHERE PftPrdID = @AvvPrdID AND PftFtrId = @VrfFtrId)
							SET @AvvIsCriteriaMatched = 0
						FETCH NEXT FROM CurVrf INTO @VrfFtrID
					END
					CLOSE CurVrf
					DEALLOCATE CurVrf
				END -- IF EXISTS (SELECT VrfVhrID FROM VehicleRequestFeature
		END	-- IF @VhrBrdCode is NOT NULL OR
			
		IF @rel = 1
			SET @AvvAvoidsRelocation = 1
		ELSE
			SET @AvvAvoidsRelocation = 0

		IF @pref = 1
			SET @AvvPriority = 1
		ELSE
			SET @AvvPriority = 0
		IF NOT EXISTS (SELECT Avvid FROM AvailableVehicle  WITH (NOLOCK) 
							WHERE	AvvVhrID = @sVhrId
							AND		AvvPrdID = @AvvPrdId
							AND   	AvvIsReverseDirection = @AvvIsReverseDirection)
		BEGIN 		
			INSERT INTO AvailableVehicle  WITH(ROWLOCK)(
				AvvId,		AvvVhrId,				AvvPrdId,		AvvCkoLocCode,
				AvvCkoDate,	AvvCkoAmPm,				AvvCkiLocCode,	AvvCkiDate,
				AvvCkiAmPm,	AvvHirePeriod,			AvvUom,			AvvIsReverseDirection,
				AvvNumAvail,AvvIsCriteriaMatched,	AvvBrdCode,		AvvAvoidsRelocation,
				AvvPriority,IntegrityNo,			AddUsrId,		AddDateTime,
				AddPrgmName
			)
			VALUES(
				newid(),	@sVhrId,				@AvvPrdId,		@ckoLoc,
				@dCkoDate,	@sCkoAmPm,				@ckiLoc,		@dCkiDate,
				@sCkiAmPm,	@sHirePeriod,			@sHirePeriodUom,@AvvIsReverseDirection, 
				@TotNoOfVehicle,@AvvIsCriteriaMatched,@AvvBrdCode,	@AvvAvoidsRelocation ,
				@AvvPriority,1,						@sUserId,		CURRENT_TIMESTAMP,
				@sPrgmName
			)
		END
		ELSE 
		BEGIN
			SELECT @AvvId = Avvid 
				FROM 	AvailableVehicle  WITH (NOLOCK) 
				WHERE 	AvvVhrId = @sVhrID
				AND		AvvPrdID = @AvvPrdID
				AND   	AvvIsReverseDirection = @AvvIsReverseDirection

			SET @TotNoOfVehicle = @TotNoOfVehicle - (SELECT COUNT(slvid) FROM SelectedVehicle WITH (NOLOCK) WHERE SlvAvvID = @AvvId)
			IF @TotNoOfVehicle < 0 
				SET @TotNoOfVehicle = 0
			--PRINT 'AvailableVehicle UPDATE'
			UPDATE AvailableVehicle WITH (ROWLOCK)
				SET AvvIsReverseDirection 	= @AvvIsReverseDirection,
					AvvNumAvail 			= @TotNoOfVehicle,
					AvvIsCriteriaMatched 	= @AvvIsCriteriaMatched,
					AvvBrdCode 				= @AvvBrdCode,
					AvvAvoidsRelocation 	= @AvvAvoidsRelocation,
					AvvPriority 			= @AvvPriority,
				    AvvCkoDate			    = @dCkoDate, --ADDED MANNY
					AvvCkiDate				=@dCkiDate  --ADDED MANNY
			WHERE AvvID = @AvvId
			SET @AvvId = NULL
		END
		NextCursor:
		FETCH NEXT FROM Cur_DVASSResult INTO @ID,@ckoLoc, @ckoDate, @ckoAMPM, @ckiLoc, @ckiDate, @ckiAMPM, @NumAvail, @rel ,  @pref
	END -- WHILE @@FETCH_STATUS=0

	EXEC sp_get_ErrorString  'GEN045', @oparam1 = @ErrStr OUTPUT
	SELECT 'GEN045/' + @ErrStr	
	GOTO DestroyCursor
DestroyCursor:
	RETURN
END



-------------------------------------------------------------------
GO
-------------------------------------------------------------------


CREATE	PROCEDURE [dbo].[RES_GetAvailQueryXML_OSLO]
	@VhrId				VARCHAR(64),
	@DvassReturnedNo	INT = 0
As

SET NOCOUNT ON
DECLARE @AvvIsReverseDirection 	bit,		@AvvPrdId			VARCHAR(64),		@AvvId				VARCHAR(64),
		@PkgCode			VARCHAR(64),	@AvpPerPerson		BIT,				@AvpBrdCode			VARCHAR(24),
		@BrdName			VARCHAR(24),	@AvpCodUomId		VARCHAR(64),		@AvpCodCurrId		VARCHAR(64),
		@AvpPkgId			VARCHAR(64),	@AvpId				VARCHAR(64),		@PkgComments		VARCHAR(500),
		@AvpTotCost			MONEY,			@vdgAvpTotCost		MONEY,				@vdnAvpTotCost		money,
		@AvpQtySelected 	INT,			@count				INT,				@avlText			VARCHAR(64),
		@avlQty				VARCHAR(64),	@PkgISREloc			BIT,				@VhrPrdID			VARCHAR(64),
		@PrdShortname		Varchar(12),	@AvvNumAvail		INT,				@AvvCriteria		VARCHAR(3),
		@loop				int,			@PkgAvail			BIT,				@NoOfTravellersForCharging	int,
		@iHirePeriod		INT,			@VhrNumOfAdults		int,				@VhrNumOfChildren	int,
		@VhrNumOfInfants	int,			@sSapId				varchar(64),		@PkgDispPrice		bit,
		@ByPass				VARCHAR(1),		@tc					VARCHAR(12),		@pp					VARCHAR(12),
		@pd					VARCHAR(12),	@vc					VARCHAR(12),		@vdn				VARCHAR(12),
		@vdg				VARCHAR(12),	@AvvCkoDate			DATETIME,			@AvvCkiDate			DATETIME,
		@ReturnError		VARCHAR(124),	@sAvvIdList			varchar(2000),		@AprCodUomId		VARCHAR(64)

SET @loop = 2
SET @AvvIsReverseDirection = 0
SET @PkgAvail = 0

SELECT 	@VhrPrdID = VhrPrdID,
		@VhrNumOfAdults = VhrNumOfAdults,
		@VhrNumOfChildren = VhrNumOfChildren,		 
		@VhrNumOfInfants = VhrNumOfInfants,
		@ByPass = CASE WHEN ISNULL(VhrBlockingResult, '') = 'ByPass' THEN '1' ELSE '0' END
	FROM VehicleRequest WITH (NOLOCK)
	WHERE VhrId =  @VhrId	

SELECT '<Forward BP="' + @ByPass + '" >'
--	Vehicle Tag Starts
SELECT '<Vehicle>'

	SELECT '<veh ID="' + AvvId +  '" ckoLoc="' + AvvCkoLocCode + '" ckoDate="' + CONVERT(VARCHAR(20), AvvCkoDate, 126) + '" ckoAMPM="' + AvvCkoAmPm + '" ckiLoc="' + AvvCkiLocCode + '" ckiDate="' + CONVERT(VARCHAR(20), AvvCkiDate, 126) + '" ckiAMPM="' + AvvCkiAmPm + '">' + BrdName +  ' ' + PrdShortname + '</veh>'
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId = VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection

	SELECT '<veh ID="' + AvvId +  '" ckoLoc="' + AvvCkoLocCode + '" ckoDate="' + CONVERT(VARCHAR(20), AvvCkoDate, 126) + '" ckoAMPM="' + AvvCkoAmPm + '" ckiLoc="' + AvvCkiLocCode + '" ckiDate="' + CONVERT(VARCHAR(20), AvvCkiDate, 126) + '" ckiAMPM="' + AvvCkiAmPm + '">' + BrdName +  ' ' + PrdShortname + '</veh>'
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId <> VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection
	
SELECT '</Vehicle>'
--	Vehicle Tag Ends

-- Avail Tag Starts
SELECT '<Avail>'

	SELECT '<av>' + CONVERT(VARCHAR(4), AvvNumAvail) +'</av>' 
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId = VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection

	SELECT '<av>' + CONVERT(VARCHAR(4), AvvNumAvail) +'</av>' 
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId <> VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection

SELECT '</Avail>'
-- Avail Tag Ends

-- Crit Tag Starts
SELECT '<Crit>'

	SELECT '<cr>' + CASE ISNULL(AvvIsCriteriaMatched, 0) WHEN 1 THEN 'Yes' ELSE 'No' END + '</cr>' 
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId = VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection

	SELECT '<cr>' + CASE ISNULL(AvvIsCriteriaMatched, 0) WHEN 1 THEN 'Yes' ELSE 'No' END + '</cr>' 
	FROM AvailableVehicle WITH (NOLOCK)
		LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
		LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
		LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
	WHERE	VhrId = @VhrId
	AND		AvvPrdId <> VhrPrdID
	AND		AvvIsReverseDirection = @AvvIsReverseDirection

SELECT '</Crit>'
-- Crit Tag Ends

--	Package Tag Starts
DECLARE curPackageTag CURSOR LOCAL FAST_FORWARD FOR 
	SELECT Distinct AvpPkgId, PkgIsRelocatable, PkgCode
		FROM AvailableVehiclePackage  WITH (NOLOCK)
			LEFT OUTER JOIN package WITH (NOLOCK) ON AvpPkgid = PkgID 
			LEFT OUTER JOIN AvailableVehicle WITH (NOLOCK) ON AvpAvvid = AvvId
			LEFT OUTER JOIN Brand WITH (NOLOCK) on PkgBrdCode = BrdCode
		WHERE AvvVhrID = @VhrId
		ORDER BY PkgIsRelocatable Desc, PkgCode

SELECT '<Package>'
OPEN curPackageTag
FETCH NEXT FROM curPackageTag INTO @AvpPkgid , @PkgisReloc, @pkgCode
WHILE @@Fetch_Status <> -1
BEGIN
	SET @PkgAvail = 1
	SELECT	@PkgDispPrice = PkgDispPrice,
			@avpid = AvpId, 
			@pkgComments = dbo.getValidXmlString(PkgComments), 
			@AvpPerPerson = AvpPerPerson, 
			@AvpBrdCode =  (SELECT BrdName from Brand WITH (NOLOCK) WHERE BrdCode = AvpBrdCode), -- AvpBrdCode, 
			@AvpCodUOMID = (SELECT CodDesc from Code WITH (NOLOCK) WHERE CodId = AvpCodUOMID), 
			@AvpCodCurrID = AvpCodCurrID 
	FROM AvailableVehiclePackage WITH (NOLOCK)
		LEFT OUTER JOIN availablevehicle WITH (NOLOCK) on AvpAvvid = AvvId
		LEFT OUTER JOIN package WITH (NOLOCK) ON AvpPkgid = PkgID 
	WHERE	AvvVhrID = @VhrId
	AND		AvpPkgid = @AvpPkgID

	SELECT '<Detail>
			<pkg id="' + ISNULL(@AvpId,'') + '" pkgid="' + ISNULL(@AvpPkgId, '') + '" pkgcom="' + ISNULL(@PkgComments, '') + '">' + ISNULL(@PkgCode,'') +  '</pkg>
			<pp>' + ISNULL(CONVERT( CHAR(1), @AvpPerPerson), '0') +  '</pp>
			<br>' + ISNULL(@AvpBrdCode,'') + '</br>
			<uom>' + ISNULL(@AvpCodUomId,'') + '</uom>
			<cur>' + ISNULL(dbo.GetCodCode(@AvpCodCurrId), '') + '</cur>'
	
	-- Vehicle Quantiry Starts
	SELECT '<Valqty>'

--	DECLARE CurAvv1 CURSOR LOCAL FAST_FORWARD FOR 
--	OPEN CurAvv1
--	FETCH NEXT FROM CurAvv1 INTO @AvvId
--	WHILE @@Fetch_Status <> -1
--	BEGIN

	SET @sAvvIdList = null
	SELECT @sAvvIdList = case when @sAvvIdList is null then AvvId else @sAvvIdList + ',' + avvid end
			FROM AvailableVehicle WITH (NOLOCK)
		WHERE	AvvvhrID = @VhrId 
		AND		AvvPrdId = @VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection

	WHILE	@sAvvIdList IS NOT NULL AND	LEN(@sAvvIdList) > 0
	BEGIN
		SET @AvvId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @sAvvIdList,
			@psListEntry		= @AvvId	OUTPUT,
			@psUpdatedList		= @sAvvIdList		OUTPUT
		
		IF @AvvId is not null and @AvvId <> ''
		BEGIN
			IF EXISTS ( SELECT AvpId from AvailableVehiclePackage  WITH (NOLOCK)
							WHERE Avpavvid = @AvvId 
							AND AvpPkgId = @AvpPkgID)
			BEGIN
				SELECT	@AvpTotCost = AvpTotCost,
						@avlQty = ISNULL ( CONVERT( VARCHAR(4), AvpQtySelected), '')
				FROM	AvailableVehiclePackage  WITH (NOLOCK)
				WHERE	Avpavvid = @AvvId 
				AND		AvpPkgId = @AvpPkgID
	
				SELECT	@AvvCkoDate = AvvCkoDate,
						@AvvCkiDate = AvvCkiDate
					FROM	AvailableVehicle WITH(NOLOCK) 
					WHERE	AvvId = @AvvId
				
				-- Total Cost Start
				SET @tc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Total Cost Ends
				
				-- Find Per Person Cost Starts
				SELECT TOP 1 @sSapId = SapId 
					FROM AvailableVehicle WITH (NOLOCK), SaleableProduct WITH (NOLOCK)
					WHERE	AvvId = @AvvId
					AND		AvvPrdId = SapPrdId
					AND		EXISTS(SELECT PplPkgId
									FROM PackageProduct WITH (NOLOCK)
									WHERE	PplPkgId = @AvpPkgId
									AND		PplSapId = SapId)
				SET @NoOfTravellersForCharging = 1
				EXECUTE RES_getNoOfTravellers
					@psSapId 	= @sSapId,
					@piAdults 	= @VhrNumOfAdults,
					@piChildren = @VhrNumOfChildren,
					@piInfants 	= @VhrNumOfInfants,
					@NoOfTravellersForCharging = @NoOfTravellersForCharging OUTPUT
				IF (@NoOfTravellersForCharging IS NULL OR @NoOfTravellersForCharging = 0)
					SET @NoOfTravellersForCharging = 1
				SET @pp = ISNULL(CAST(@AvpTotCost / @NoOfTravellersForCharging AS VARCHAR), '')
				-- Find Per Person Cost Ends
			
				-- Per Day Cost Starts
				IF EXISTS(SELECT AvvId FROM AvailableVehicle WITH(NOLOCK)
							WHERE	AvvId = @AvvId
							AND		AvvCodHirePeriodUOMId IS NOT NULL
							AND		dbo.getCodCode(AvvCodHirePeriodUOMId) = 'DAYS')
					SELECT @iHirePeriod = AvvHirePeriod
						FROM 	AvailableVehicle WITH(NOLOCK)
						WHERE	AvvId = @AvvId
				ELSE
				BEGIN
					SET @AprCodUomId = NULL
					SELECT	@AprCodUomId = AprCodUomId
						FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
								AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
						WHERE 	AprAvpId = AvpId
						AND		AprSapId = SapId
						AND		SapPrdId = PrdId
						AND		PrdIsVehicle = 1
						AND		AvcAprId = AprId
						AND		AvpAvvId = @AvvId
						AND		AvpPkgId = @AvpPkgID
	
					EXECUTE	RES_calculateHirePeriod			
						@CheckOutDate	= @AvvCkoDate,
						@CheckInDate	= @AvvCkiDate,
						@OdometerOut	= NULL,
						@OdometerIn		= NULL,
						@FixedQuantity	= NULL,
						@HireUOM		= @AprCodUomId,
						@HireQuantity	= @iHirePeriod	OUTPUT,
						@ReturnError	= @ReturnError	OUTPUT
				END
					IF (@ReturnError IS NULL OR @ReturnError = '') AND @iHirePeriod IS NOT NULL AND @iHirePeriod <> 0
						SET @pd = ISNULL(CAST(@AvpTotCost / @iHirePeriod AS VARCHAR), '')
				-- Per Day Cost Ends
				
				-- Vehicle Cost Starts
				SELECT @AvpTotCost = 0, @vdgAvpTotCost = 0, @vdnAvpTotCost = 0

				SELECT	@AvpTotCost 	= @AvpTotCost + (AvcGrossAmt - AvcAgnDiscAmt),
						@vdgAvpTotCost  = @vdgAvpTotCost + AvcRate,
						@vdnAvpTotCost	= @vdnAvpTotCost + (AvcRate - (AvcRate * ISNULL(AvcAgnDisPerc, 0)/100))
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID


				SET @vc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				SET @vdg = ISNULL(CAST(@vdgAvpTotCost AS VARCHAR), '')			
				SET @vdn = ISNULL(CAST(@vdnAvpTotCost AS VARCHAR), '')			
				--	Vehicle Daily Net Value Ends
	
				-- If package displayPrice is false then price should not be shown
				SET @tc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@tc, '') ELSE '' END
				SET @pp = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pp, '') ELSE '' END
				SET @pd = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pd, '') ELSE '' END
				SET @vc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vc, '') ELSE '' END
				SET @vdn = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdn, '') ELSE '' END
				SET @vdg = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdg, '') ELSE '' END
			END
			ELSE
			BEGIN
				-- If package displayPrice is false then price should not be shown
				SET @tc = 'NA'
				SET @pp = 'NA'
				SET @pd = 'NA'
				SET @vc = 'NA'
				SET @vdn = 'NA'
				SET @vdg = 'NA'
				SET @avlQty = ''
			END
	
			SELECT '<Value>
					<val tc="' + @tc + '" pp="' + @pp + '" pd="' + @pd + '" vc="' + @vc + '" vdn="' + @vdn + '" vdg="' + @vdg + '" />
					<qty>' + @avlQty + '</qty>
					</Value>'
		END
--		FETCH NEXT FROM CurAvv1 INTO @AvvId
	END
--	CLOSE CurAvv1
--	DEALLOCATE CurAvv1
	-- Vehicle Quantiy Ends

	-- Vehicle Quantiy Starts
--	DECLARE CurAvv2 CURSOR LOCAL FAST_FORWARD FOR 
--	OPEN CurAvv2
--	FETCH NEXT FROM CurAvv2 INTO @AvvId
--	WHILE @@Fetch_Status <> -1
--	BEGIN

	SET @sAvvIdList = null
	SELECT @sAvvIdList = case when @sAvvIdList is null then AvvId else @sAvvIdList + ',' + avvid end
			FROM AvailableVehicle WITH (NOLOCK)
		WHERE	AvvvhrID = @VhrId
		AND		AvvPrdId <> @VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection

	WHILE	@sAvvIdList IS NOT NULL AND	LEN(@sAvvIdList) > 0
	BEGIN
		SET @AvvId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @sAvvIdList,
			@psListEntry		= @AvvId	OUTPUT,
			@psUpdatedList		= @sAvvIdList		OUTPUT
		
		IF @AvvId is not null and @AvvId <> ''
		BEGIN

			IF EXISTS ( SELECT AvpId from AvailableVehiclePackage  WITH (NOLOCK)
							WHERE Avpavvid = @AvvId 
							AND AvpPkgId = @AvpPkgID)
			BEGIN
				SELECT	@AvpTotCost = AvpTotCost,
						@avlQty = ISNULL ( CONVERT( VARCHAR(4), AvpQtySelected), '')
				FROM	AvailableVehiclePackage  WITH (NOLOCK)
				WHERE	Avpavvid = @AvvId 
				AND		AvpPkgId = @AvpPkgID
	
				SELECT	@AvvCkoDate = AvvCkoDate,
						@AvvCkiDate = AvvCkiDate
					FROM	AvailableVehicle WITH(NOLOCK) 
					WHERE	AvvId = @AvvId
				
				-- Total Cost Start
				SET @tc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Total Cost Ends
				
				-- Find Per Person Cost Starts
				SELECT TOP 1 @sSapId = SapId 
					FROM AvailableVehicle WITH (NOLOCK), SaleableProduct WITH (NOLOCK)
					WHERE	AvvId = @AvvId
					AND		AvvPrdId = SapPrdId
					AND		EXISTS(SELECT PplPkgId
									FROM PackageProduct WITH (NOLOCK)
									WHERE	PplPkgId = @AvpPkgId
									AND		PplSapId = SapId)
				SET @NoOfTravellersForCharging = 1
				EXECUTE RES_getNoOfTravellers
					@psSapId 	= @sSapId,
					@piAdults 	= @VhrNumOfAdults,
					@piChildren = @VhrNumOfChildren,
					@piInfants 	= @VhrNumOfInfants,
					@NoOfTravellersForCharging = @NoOfTravellersForCharging OUTPUT
				IF (@NoOfTravellersForCharging IS NULL OR @NoOfTravellersForCharging = 0)
					SET @NoOfTravellersForCharging = 1
				SET @pp = ISNULL(CAST(@AvpTotCost / @NoOfTravellersForCharging AS VARCHAR), '')
				-- Find Per Person Cost Ends
				
				-- Per Day Cost Starts
				IF EXISTS(SELECT AvvId FROM AvailableVehicle WITH(NOLOCK)
							WHERE	AvvId = @AvvId
							AND		AvvCodHirePeriodUOMId IS NOT NULL
							AND		dbo.getCodCode(AvvCodHirePeriodUOMId) = 'DAYS')
					SELECT @iHirePeriod = AvvHirePeriod
						FROM 	AvailableVehicle WITH(NOLOCK)
						WHERE	AvvId = @AvvId
				ELSE
				BEGIN
					SET @AprCodUomId = NULL
					SELECT	@AprCodUomId = AprCodUomId
						FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
								AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
						WHERE 	AprAvpId = AvpId
						AND		AprSapId = SapId
						AND		SapPrdId = PrdId
						AND		PrdIsVehicle = 1
						AND		AvcAprId = AprId
						AND		AvpAvvId = @AvvId
						AND		AvpPkgId = @AvpPkgID
	
					EXECUTE	RES_calculateHirePeriod			
						@CheckOutDate	= @AvvCkoDate,
						@CheckInDate	= @AvvCkiDate,
						@OdometerOut	= NULL,
						@OdometerIn		= NULL,
						@FixedQuantity	= NULL,
						@HireUOM		= @AprCodUomId,
						@HireQuantity	= @iHirePeriod	OUTPUT,
						@ReturnError	= @ReturnError	OUTPUT
				END
				IF (@ReturnError IS NULL OR @ReturnError = '') AND @iHirePeriod IS NOT NULL AND @iHirePeriod <> 0
					SET @pd = ISNULL(CAST(@AvpTotCost / @iHirePeriod AS VARCHAR), '')
				-- Per Day Cost Ends
				
				SELECT @AvpTotCost = 0, @vdgAvpTotCost = 0, @vdnAvpTotCost = 0

				SELECT	@AvpTotCost 	= @AvpTotCost + (AvcGrossAmt - AvcAgnDiscAmt),
						@vdgAvpTotCost  = @vdgAvpTotCost + AvcRate,
						@vdnAvpTotCost	= @vdnAvpTotCost + (AvcRate - (AvcRate * ISNULL(AvcAgnDisPerc, 0)/100))
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID


				SET @vc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				SET @vdg = ISNULL(CAST(@vdgAvpTotCost AS VARCHAR), '')			
				SET @vdn = ISNULL(CAST(@vdnAvpTotCost AS VARCHAR), '')			
	
				-- If package displayPrice is false then price should not be shown
				SET @tc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@tc, '') ELSE '' END
				SET @pp = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pp, '') ELSE '' END
				SET @pd = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pd, '') ELSE '' END
				SET @vc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vc, '') ELSE '' END
				SET @vdn = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdn, '') ELSE '' END
				SET @vdg = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdg, '') ELSE '' END
			END
			ELSE
			BEGIN
				-- If package displayPrice is false then price should not be shown
				SET @tc = 'NA'
				SET @pp = 'NA'
				SET @pd = 'NA'
				SET @vc = 'NA'
				SET @vdn = 'NA'
				SET @vdg = 'NA'
				SET @avlQty = ''
			END
	
			SELECT '<Value>
					<val tc="' + @tc + '" pp="' + @pp + '" pd="' + @pd + '" vc="' + @vc + '"  vdn="' + @vdn + '" vdg="' + @vdg + '" />
					<qty>' + @avlQty + '</qty>
					</Value>'
		END
--		FETCH NEXT FROM CurAvv2 INTO @AvvId
	END	
--	CLOSE CurAvv2
--	DEALLOCATE CurAvv2

	SELECT '</Valqty>'
	-- Vehicle Quantiry Starts
	
	SELECT '</Detail>'
	FETCH NEXT FROM curPackageTag INTO @AvpPkgid , @PkgisReloc, @pkgCode
END

CLOSE curPackageTag
DEALLOCATE curPackageTag

----START----------------------------------------------------------------------
-- This Section Puts in a Blank ROW in the Grid if there are no packages 
-- availabe for the products listed
------------------------------------------------------------------------------
IF @PkgAvail = 0 
BEGIN
	SELECT '<Detail>
			<pkg id="' + '' + '" pkgid="' + '' + '" pkgcom="' + '' + '">' + '' +  '</pkg>
			<pp>' + '0' +  '</pp>
			<br>' + '' + '</br>
			<uom>' + '' + '</uom>
			<cur>' + ''  + '</cur>
			<Valqty>'

	SELECT '<Value><val tc="NA" pp="NA" pd="NA" vc="NA" vdn="NA" vdg="NA"/><qty></qty></Value>'
		FROM AvailableVehicle WITH (NOLOCK)
	WHERE	AvvvhrID = @VhrId
	AND		AvvIsReverseDirection = @AvvIsReverseDirection
	SELECT '</Valqty>'
	-- Vehicle Quantiry Starts
	SELECT '</Detail>'
END			
-----END----------------------------------------------------------------------
-- This Section Puts in a Blank ROW in the Grid if there are no packages 
-- availabe for the products listed
------------------------------------------------------------------------------
SELECT '</Package>'
SELECT '</Forward>'

--   Forward Tag End

--	Reverse Tag Starts
SET @AvvIsReverseDirection = 1
IF EXISTS( select avvid from availableVehicle WITH (NOLOCK) Where AvvVhrID = @VhrID AND AvvIsReverseDirection = @AvvIsReverseDirection)
BEGIN
	
	
	SELECT '<Reverse>'
	--	Vehicle Tag Starts
	SELECT '<Vehicle>'

		SELECT '<veh ID="' + AvvId +  '" ckoLoc="' + AvvCkoLocCode + '" ckoDate="' + CONVERT(VARCHAR(20), AvvCkoDate, 126) + '" ckoAMPM="' + AvvCkoAmPm + '" ckiLoc="' + AvvCkiLocCode + '" ckiDate="' + CONVERT(VARCHAR(20), AvvCkiDate, 126) + '" ckiAMPM="' + AvvCkiAmPm + '">' + BrdName +  ' ' + PrdShortname + '</veh>'
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId = VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection

		SELECT '<veh ID="' + AvvId +  '" ckoLoc="' + AvvCkoLocCode + '" ckoDate="' + CONVERT(VARCHAR(20), AvvCkoDate, 126) + '" ckoAMPM="' + AvvCkoAmPm + '" ckiLoc="' + AvvCkiLocCode + '" ckiDate="' + CONVERT(VARCHAR(20), AvvCkiDate, 126) + '" ckiAMPM="' + AvvCkiAmPm + '">' + BrdName +  ' ' + PrdShortname + '</veh>'
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId <> VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection
	
	SELECT '</Vehicle>'
	--	Vehicle Tag Ends
	
	-- Avail Tag Starts
	SELECT '<Avail>'

		SELECT '<av>' + CONVERT(VARCHAR(4), AvvNumAvail) +'</av>' 
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId = VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection

		SELECT '<av>' + CONVERT(VARCHAR(4), AvvNumAvail) +'</av>' 
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId <> VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection
	
	SELECT '</Avail>'
	-- Avail Tag Ends
	
	-- Crit Tag Starts
	SELECT '<Crit>'

		SELECT '<cr>' + CASE ISNULL(AvvIsCriteriaMatched, 0) WHEN 1 THEN 'Yes' ELSE 'No' END +'</cr>' 
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId = VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection
	
		SELECT '<cr>' + CASE ISNULL(AvvIsCriteriaMatched, 0) WHEN 1 THEN 'Yes' ELSE 'No' END +'</cr>' 
		FROM AvailableVehicle WITH (NOLOCK)
			LEFT OUTER JOIN Product WITH (NOLOCK) ON AvvPrdId = Prdid
			LEFT OUTER JOIN Brand  WITH (NOLOCK) ON PrdBrdCode = BrdCode
			LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) on AvvVhrID = VhrId
		WHERE	VhrId = @VhrId
		AND		AvvPrdId <> VhrPrdID
		AND		AvvIsReverseDirection = @AvvIsReverseDirection
		
	SELECT '</Crit>'
	-- Crit Tag Ends
	
	--	Package Tag Starts
	DECLARE curPackageTag CURSOR LOCAL FAST_FORWARD FOR 
		SELECT Distinct AvpPkgId, PkgIsRelocatable, PkgCode
			FROM AvailableVehiclePackage WITH (NOLOCK)
				LEFT OUTER JOIN package WITH (NOLOCK) ON AvpPkgid = PkgID 
				LEFT OUTER JOIN AvailableVehicle WITH (NOLOCK) ON AvpAvvid = AvvId
				LEFT OUTER JOIN Brand WITH (NOLOCK) on PkgBrdCode = BrdCode
			WHERE AvvVhrID = @VhrId
			ORDER BY PkgIsRelocatable Desc, PkgCode
	
	SELECT '<Package>'
	OPEN curPackageTag
	FETCH NEXT FROM curPackageTag INTO @AvpPkgid , @PkgisReloc, @pkgCode
	WHILE @@Fetch_Status <> -1
	BEGIN
		SELECT	@PkgDispPrice = PkgDispPrice,
				@avpid = AvpId, 
				@pkgComments = PkgComments, 
				@AvpPerPerson = AvpPerPerson, 
				@AvpBrdCode = (SELECT BrdName from Brand WITH (NOLOCK) WHERE BrdCode = AvpBrdCode), -- AvpBrdCode, 
				@AvpCodUOMID = (SELECT CodDesc from Code WITH (NOLOCK) WHERE CodId = AvpCodUOMID), 
				@AvpCodCurrID = AvpCodCurrID 
		FROM AvailableVehiclePackage WITH (NOLOCK)
			LEFT OUTER JOIN availablevehicle WITH (NOLOCK) on AvpAvvid = AvvId
			LEFT OUTER JOIN package WITH (NOLOCK) ON AvpPkgid = PkgID 
		WHERE	AvvVhrID = @VhrId
		AND		AvpPkgid = @AvpPkgID
	
		SELECT '<Detail>
				<pkg id="' + ISNULL(@AvpId,'') + '" pkgid="' + ISNULL(@AvpPkgId, '') + '" pkgcom="' + ISNULL(@PkgComments, '') + '">' + ISNULL(@PkgCode,'') +  '</pkg>
				<pp>' + ISNULL(CONVERT( CHAR(1), @AvpPerPerson), '0') +  '</pp>
				<br>' + ISNULL(@AvpBrdCode,'') + '</br>
				<uom>' + ISNULL(@AvpCodUomId,'') + '</uom>
				<cur>' + ISNULL(dbo.GetCodCode(@AvpCodCurrId), '') + '</cur>'
		
		-- Vehicle Quantiry Starts
		SELECT '<Valqty>'
	
		DECLARE CurAvv1 CURSOR LOCAL FAST_FORWARD FOR 
			SELECT AvvId 
				FROM AvailableVehicle WITH (NOLOCK)
			WHERE	AvvvhrID = @VhrId 
			AND		AvvPrdId = @VhrPrdID
			AND		AvvIsReverseDirection = @AvvIsReverseDirection
		OPEN CurAvv1
		FETCH NEXT FROM CurAvv1 INTO @AvvId
		WHILE @@Fetch_Status <> -1
		BEGIN
			IF EXISTS ( SELECT AvpId from AvailableVehiclePackage  WITH (NOLOCK)
							WHERE Avpavvid = @AvvId 
							AND AvpPkgId = @AvpPkgID)
			BEGIN
				SELECT	@AvpTotCost = AvpTotCost,
						@avlQty = ISNULL ( CONVERT( VARCHAR(4), AvpQtySelected), '')
				FROM	AvailableVehiclePackage  WITH (NOLOCK)
				WHERE	Avpavvid = @AvvId 
				AND		AvpPkgId = @AvpPkgID
	
				SELECT	@AvvCkoDate = AvvCkoDate,
						@AvvCkiDate = AvvCkiDate
					FROM	AvailableVehicle WITH(NOLOCK) 
					WHERE	AvvId = @AvvId
				
				-- Total Cost Start
				SET @tc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Total Cost Ends
				
				-- Find Per Person Cost Starts
				SELECT TOP 1 @sSapId = SapId 
					FROM AvailableVehicle WITH (NOLOCK), SaleableProduct WITH (NOLOCK)
					WHERE	AvvId = @AvvId
					AND		AvvPrdId = SapPrdId
					AND		EXISTS(SELECT PplPkgId
									FROM PackageProduct WITH (NOLOCK)
									WHERE	PplPkgId = @AvpPkgId
									AND		PplSapId = SapId)
				SET @NoOfTravellersForCharging = 1
				EXECUTE RES_getNoOfTravellers
					@psSapId 	= @sSapId,
					@piAdults 	= @VhrNumOfAdults,
					@piChildren = @VhrNumOfChildren,
					@piInfants 	= @VhrNumOfInfants,
					@NoOfTravellersForCharging = @NoOfTravellersForCharging OUTPUT
				IF (@NoOfTravellersForCharging IS NULL OR @NoOfTravellersForCharging = 0)
					SET @NoOfTravellersForCharging = 1
				SET @pp = ISNULL(CAST(@AvpTotCost / @NoOfTravellersForCharging AS VARCHAR), '')
				-- Find Per Person Cost Ends
				
				-- Per Day Cost Starts
				IF EXISTS(SELECT AvvId FROM AvailableVehicle WITH(NOLOCK)
							WHERE	AvvId = @AvvId
							AND		AvvCodHirePeriodUOMId IS NOT NULL
							AND		dbo.getCodCode(AvvCodHirePeriodUOMId) = 'DAYS')
					SELECT @iHirePeriod = AvvHirePeriod
						FROM 	AvailableVehicle WITH(NOLOCK)
						WHERE	AvvId = @AvvId
				ELSE
				BEGIN
					SET @AprCodUomId = NULL
					SELECT	@AprCodUomId = AprCodUomId
						FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
								AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
						WHERE 	AprAvpId = AvpId
						AND		AprSapId = SapId
						AND		SapPrdId = PrdId
						AND		PrdIsVehicle = 1
						AND		AvcAprId = AprId
						AND		AvpAvvId = @AvvId
						AND		AvpPkgId = @AvpPkgID
					EXECUTE	RES_calculateHirePeriod			
						@CheckOutDate	= @AvvCkoDate,
						@CheckInDate	= @AvvCkiDate,
						@OdometerOut	= NULL,
						@OdometerIn		= NULL,
						@FixedQuantity	= NULL,
						@HireUOM		= @AprCodUomId,
						@HireQuantity	= @iHirePeriod	OUTPUT,
						@ReturnError	= @ReturnError	OUTPUT
				END
				IF (@ReturnError IS NULL OR @ReturnError = '') AND @iHirePeriod IS NOT NULL AND @iHirePeriod <> 0
					SET @pd = ISNULL(CAST(@AvpTotCost / @iHirePeriod AS VARCHAR), '')
				-- Per Day Cost Ends
				
				-- Vehicle Cost Starts
				SELECT	@AvpTotCost = SUM(AvcGrossAmt - AvcAgnDiscAmt)
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Vehicle Cost Ends

				--	Vehicle Daily Gross Value Start
				SET @AvpTotCost = NULL
				SELECT	@AvpTotCost = SUM(AvcRate)
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vdg = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')			
				--	Vehicle Daily Gross Value Ends
	
				--	Vehicle Daily Net Value Start
				SET @AvpTotCost = NULL
				SELECT	@AvpTotCost = SUM(AvcRate - (AvcRate * ISNULL(AvcAgnDisPerc, 0)/100))
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vdn = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')			
				--	Vehicle Daily Net Value Ends

				-- If package displayPrice is false then price should not be shown
				SET @tc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@tc, '') ELSE '' END
				SET @pp = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pp, '') ELSE '' END
				SET @pd = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pd, '') ELSE '' END
				SET @vc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vc, '') ELSE '' END
				SET @vdn = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdn, '') ELSE '' END
				SET @vdg = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdg, '') ELSE '' END
			END
			ELSE
			BEGIN
				-- If package displayPrice is false then price should not be shown
				SET @tc = 'NA'
				SET @pp = 'NA'
				SET @pd = 'NA'
				SET @vc = 'NA'
				SET @vdn = 'NA'
				SET @vdg = 'NA'
				SET @avlQty = ''
			END
	
			SELECT '<Value>
					<val tc="' + @tc + '" pp="' + @pp + '" pd="' + @pd + '" vc="' + @vc + '" vdn="' + @vdn + '" vdg="' + @vdg + '" />
					<qty>' + @avlQty + '</qty>
					</Value>'
			FETCH NEXT FROM CurAvv1 INTO @AvvId
		END
		CLOSE CurAvv1
		DEALLOCATE CurAvv1
		-- Vehicle Quantiy Ends
	
		-- Vehicle Quantiy Starts
		DECLARE CurAvv2 CURSOR LOCAL FAST_FORWARD FOR 
			SELECT AvvId 
				FROM AvailableVehicle WITH (NOLOCK)
			WHERE	AvvvhrID = @VhrId
			AND		AvvPrdId <> @VhrPrdID
			AND		AvvIsReverseDirection = @AvvIsReverseDirection
		OPEN CurAvv2
		FETCH NEXT FROM CurAvv2 INTO @AvvId
		WHILE @@Fetch_Status <> -1
		BEGIN
			IF EXISTS ( SELECT AvpId from AvailableVehiclePackage  WITH (NOLOCK)
							WHERE Avpavvid = @AvvId 
							AND AvpPkgId = @AvpPkgID)
			BEGIN
				SELECT	@AvpTotCost = AvpTotCost,
						@avlQty = ISNULL ( CONVERT( VARCHAR(4), AvpQtySelected), '')
				FROM	AvailableVehiclePackage  WITH (NOLOCK)
				WHERE	Avpavvid = @AvvId 
				AND		AvpPkgId = @AvpPkgID
	
				SELECT	@AvvCkoDate = AvvCkoDate,
						@AvvCkiDate = AvvCkiDate
					FROM	AvailableVehicle WITH(NOLOCK) 
					WHERE	AvvId = @AvvId
				
				-- Total Cost Start
				SET @tc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Total Cost Ends
				
				-- Find Per Person Cost Starts
				SELECT TOP 1 @sSapId = SapId 
					FROM AvailableVehicle WITH (NOLOCK), SaleableProduct WITH (NOLOCK)
					WHERE	AvvId = @AvvId
					AND		AvvPrdId = SapPrdId
					AND		EXISTS(SELECT PplPkgId
									FROM PackageProduct WITH (NOLOCK)
									WHERE	PplPkgId = @AvpPkgId
									AND		PplSapId = SapId)
				SET @NoOfTravellersForCharging = 1
				EXECUTE RES_getNoOfTravellers
					@psSapId 	= @sSapId,
					@piAdults 	= @VhrNumOfAdults,
					@piChildren = @VhrNumOfChildren,
					@piInfants 	= @VhrNumOfInfants,
					@NoOfTravellersForCharging = @NoOfTravellersForCharging OUTPUT
				IF (@NoOfTravellersForCharging IS NULL OR @NoOfTravellersForCharging = 0)
					SET @NoOfTravellersForCharging = 1
				SET @pp = ISNULL(CAST(@AvpTotCost / @NoOfTravellersForCharging AS VARCHAR), '')
				-- Find Per Person Cost Ends
				
				-- Per Day Cost Starts
				IF EXISTS(SELECT AvvId FROM AvailableVehicle WITH(NOLOCK)
							WHERE	AvvId = @AvvId
							AND		AvvCodHirePeriodUOMId IS NOT NULL
							AND		dbo.getCodCode(AvvCodHirePeriodUOMId) = 'DAYS')
					SELECT @iHirePeriod = AvvHirePeriod
						FROM 	AvailableVehicle WITH(NOLOCK)
						WHERE	AvvId = @AvvId
				ELSE
				BEGIN
					SET @AprCodUomId = NULL
					SELECT	@AprCodUomId = AprCodUomId
						FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
								AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
						WHERE 	AprAvpId = AvpId
						AND		AprSapId = SapId
						AND		SapPrdId = PrdId
						AND		PrdIsVehicle = 1
						AND		AvcAprId = AprId
						AND		AvpAvvId = @AvvId
						AND		AvpPkgId = @AvpPkgID
					EXECUTE	RES_calculateHirePeriod			
						@CheckOutDate	= @AvvCkoDate,
						@CheckInDate	= @AvvCkiDate,
						@OdometerOut	= NULL,
						@OdometerIn		= NULL,
						@FixedQuantity	= NULL,
						@HireUOM		= @AprCodUomId,
						@HireQuantity	= @iHirePeriod	OUTPUT,
						@ReturnError	= @ReturnError	OUTPUT
				END
				IF (@ReturnError IS NULL OR @ReturnError = '') AND @iHirePeriod IS NOT NULL AND @iHirePeriod <> 0
					SET @pd = ISNULL(CAST(@AvpTotCost / @iHirePeriod AS VARCHAR), '')
				-- Per Day Cost Ends
				
				-- Vehicle Cost Starts
				SELECT	@AvpTotCost = SUM(AvcGrossAmt - AvcAgnDiscAmt)
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vc = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')
				-- Vehicle Cost Ends

				--	Vehicle Daily Gross Value Start
				SET @AvpTotCost = NULL
				SELECT	@AvpTotCost = SUM(AvcRate)
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vdg = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')			
				--	Vehicle Daily Gross Value Ends
	
				--	Vehicle Daily Net Value Start
				SET @AvpTotCost = NULL
				SELECT	@AvpTotCost = SUM(AvcRate - (AvcRate * ISNULL(AvcAgnDisPerc, 0)/100))
					FROM	AvailableVehiclePackage WITH(NOLOCK), AvailableVehicleProduct WITH(NOLOCK), 
							AvailableVehicleCharge WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
					WHERE 	AprAvpId = AvpId
					AND		AprSapId = SapId
					AND		SapPrdId = PrdId
					AND		PrdIsVehicle = 1
					AND		AvcAprId = AprId
					AND		AvpAvvId = @AvvId
					AND		AvpPkgId = @AvpPkgID
				SET @vdn = ISNULL(CAST(@AvpTotCost AS VARCHAR), '')			
				--	Vehicle Daily Net Value Ends
	
				-- If package displayPrice is false then price should not be shown
				SET @tc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@tc, '') ELSE '' END
				SET @pp = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pp, '') ELSE '' END
				SET @pd = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@pd, '') ELSE '' END
				SET @vc = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vc, '') ELSE '' END
				SET @vdn = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdn, '') ELSE '' END
				SET @vdg = CASE WHEN ISNULL(@PkgDispPrice, 1) = 1 THEN ISNULL(@vdg, '') ELSE '' END
			END
			ELSE
			BEGIN
				-- If package displayPrice is false then price should not be shown
				SET @tc = 'NA'
				SET @pp = 'NA'
				SET @pd = 'NA'
				SET @vc = 'NA'
				SET @vdn = 'NA'
				SET @vdg = 'NA'
				SET @avlQty = ''
			END
	
			SELECT '<Value>
					<val tc="' + @tc + '" pp="' + @pp + '" pd="' + @pd + '" vc="' + @vc + '" vdn="' + @vdn + '" vdg="' + @vdg + '" />
					<qty>' + @avlQty + '</qty>
					</Value>'
			FETCH NEXT FROM CurAvv2 INTO @AvvId
		END
		CLOSE CurAvv2
		DEALLOCATE CurAvv2

		SELECT '</Valqty>'
		-- Vehicle Quantiry Starts
		
		SELECT '</Detail>'
		FETCH NEXT FROM curPackageTag INTO @AvpPkgid , @PkgisReloc, @pkgCode
	END
	CLOSE curPackageTag
	DEALLOCATE curPackageTag	

	----START----------------------------------------------------------------------
	-- This Section Puts in a Blank ROW in the Grid if there are no packages 
	-- availabe for the products listed
	------------------------------------------------------------------------------
	IF @PkgAvail = 0 
	BEGIN
			SELECT '<Detail>
					<pkg id="' + '' + '" pkgid="' + '' + '" pkgcom="' + '' + '">' + '' +  '</pkg>
					<pp>' + '0' +  '</pp>
					<br>' + '' + '</br>
					<uom>' + '' + '</uom>
					<cur>' + ''  + '</cur>
					<Valqty>'

			SELECT '<Value><val tc="NA" pp="NA" pd="NA" vc="NA" vdn="NA" vdg="NA"/><qty></qty></Value>'
				FROM AvailableVehicle WITH (NOLOCK)
			WHERE	AvvvhrID = @VhrId
			AND		AvvIsReverseDirection = @AvvIsReverseDirection
		SELECT '</Valqty>'
		-- Vehicle Quantiry Starts
		SELECT '</Detail>'
	END			
	-----END----------------------------------------------------------------------
	-- This Section Puts in a Blank ROW in the Grid if there are no packages 
	-- availabe for the products listed
	------------------------------------------------------------------------------
	
	SELECT '</Package>'
	SELECT '</Reverse>'
	--	Reverse Tag Ends
	--	Package Tag Ends 
END	



-------------------------------------------------------------------
GO
-------------------------------------------------------------------

CREATE	PROCEDURE [dbo].[RES_SummaryOfChosenVehicles_OSLO]
		@psBkrId		VARCHAR(64),
		@psBooId		VARCHAR(64) = NULL,
		@psBpdId		VARCHAR(64) = NULL,
		@psNextScreen	VARCHAR(64)
AS

SET NOCOUNT ON
	
/*
	========================================
	START INITIALISE SECTION
	========================================
*/

--	++++++++++++++++++++++++++++++
--		LOCAL VARIABLES
--	++++++++++++++++++++++++++++++
DECLARE
	@sError			VARCHAR(500),
	@sBooNum		VARCHAR(64),
	@ttlCost		money,
	@value			varchar(500),
	@AgnCode		VARCHAR(24),
	@AgnCurr		VARCHAR(24),
	@UviValue		VARCHAR(500),
	@start			INT,
	@count			INT,
	@EnabNewBoo		VARCHAR(1),
	@AtyIsDirect	BIT,
	@Msg			VARCHAR(500)

/*  Validation handled in the processing section  */
-- ------ Provisional / Normal Button Section Start --------------
DECLARE @sAvvLocCode		varchar(64), 
	@sAvpPkgId		varchar(64),
	@sCtyCode		varchar(64),
	@sCtyName		varchar(64),
	@sAvpBlockingResult	varchar(64),
	@sBypass		varchar(64),
	@sProvBut		varchar(10),
	@sNormBut		varchar(10),
	@psBooNum		VARCHAR(64)
DECLARE @tLocCty	table (CtyCode	varchar(24))

IF ISNULL(@psBooId, '') = ''
	EXEC Res_getBookingNumber 
			@psBkrId	= @psBkrid,			-- 	VARCHAR(64),
			@psUserId	= 'test',			-- 	VARCHAR(64),
			@psBooNum	= @psBooNum OUTPUT	-- 	varchar(64) OUTPUT
ELSE
	SELECT @psBooNum = BooNum 
		FROM   Booking WITH (NOLOCK)
		WHERE  BooId = @psBooId	

DECLARE curProvBut CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
SELECT AvvCkoLocCode, AvpPkgId, VhrBlockingResult --AvpBlockingResult
	FROM	SelectedVehicle  WITH (NOLOCK)
		    LEFT OUTER JOIN AvailableVehiclePackage WITH (NOLOCK) ON SlvAvpId = AvpId 
		    LEFT OUTER JOIN Availablevehicle WITH (NOLOCK) ON AvpAvvId = AvvId 
			LEFT OUTER JOIN product WITH (NOLOCK) ON prdid = avvprdid
		    LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrId = VhrId 
		    LEFT OUTER JOIN BookingRequest WITH (NOLOCK) ON VhrBkrid = Bkrid 
	WHERE  	BkrId = @psBkrid
	AND		PrdIsVehicle = 1

OPEN curProvBut
SET @sBypass = ''
FETCH NEXT FROM curProvBut INTO  @sAvvLocCode, @sAvpPkgId, @sAvpBlockingResult
WHILE(@@FETCH_STATUS = 0)
BEGIN	
	IF (@sAvpBlockingResult = 'Bypass')
		SET @sBypass = 'Bypass'

	EXECUTE
		GEN_getCountryForLocation
			@sAvvLocCode,
			@sAvpPkgId,
			@sCtyCode OUTPUT,
			@sCtyName OUTPUT,
			@sError OUTPUT

	IF NOT EXISTS(SELECT CtyCode FROM @tLocCty WHERE CtyCode = @sCtyCode)
		INSERT INTO @tLocCty VALUES (@sCtyCode)
	FETCH NEXT FROM curProvBut INTO  @sAvvLocCode, @sAvpPkgId, @sAvpBlockingResult
END

IF EXISTS(SELECT UviKey FROM UniversalInfo WITH (NOLOCK)
			WHERE 	UviKey IN (SELECT CtyCode+'ProvRentAllow' FROM @tLocCty)
			AND		UviValue = 'FALSE')	
	SET @sProvBut = ''
ELSE
	SET @sProvBut = 'EXIST'

IF EXISTS (SELECT VhrId FROM VehicleRequest WITH (NOLOCK)
			WHERE 	VhrBkrId = @psBkrid
			AND	VhrRequestType = 'No Availability Required')
BEGIN
	IF (@sBypass = 'Bypass')
		SET @sNormBut = 'EXIST'
	ELSE
		SET @sNormBut = ''
END
ELSE
	SET @sNormBut = 'EXIST'

-- ------ Provisional / Normal Button Section End ----------------

/*========================================
	START PROCESSING SECTION
========================================*/
IF	@psBooId IS NOT NULL
	SELECT	@sBooNum=BooNum 
		FROM	Booking WITH (NOLOCK)
		WHERE	BooId=@psBooId
ELSE IF @psBpdId IS NOT NULL
	SELECT	TOP 1 @sBooNum = BooNum 
		FROM	Booking WITH (NOLOCK), Rental WITH (NOLOCK), BookedProduct WITH (NOLOCK)
		WHERE	BooId=RntBooId
		AND		BpdRntId=RntId
		AND		BpdId=@psBpdId

SELECT  @TtlCost=SUM(ISNULL(AvpTotCost,0))
	FROM	SelectedVehicle WITH (NOLOCK)
		    LEFT OUTER JOIN AvailableVehiclePackage WITH (NOLOCK) ON SlvAvpId = AvpId 
		    LEFT OUTER JOIN Availablevehicle WITH (NOLOCK) ON AvpAvvId = AvvId 
			LEFT OUTER JOIN product WITH (NOLOCK) ON prdid = avvprdid
		    LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrId = VhrId 
		    LEFT OUTER JOIN BookingRequest WITH (NOLOCK) ON VhrBkrid = Bkrid 
	WHERE  	BkrId = @psBkrid
	AND 	PrdIsVehicle = 1

IF @psBooId IS NOT NULL AND EXISTS(SELECT BooId FROM Booking WITH(NOLOCK)
										WHERE	BooId = @psBooId)
BEGIN
	SELECT 	@value = 'You have selected for client ' + BooLastName + ' on behalf of agent ' + ISNULL(REPLACE(AgnName, '&', '&amp;'), ''),
			@AgnCode = AgnCode,
			@AtyIsDirect = AtyIsDirect
		FROM 	Booking WITH (NOLOCK), Agent WITH (NOLOCK), AgentType WITH (NOLOCK)
		WHERE	BooAgnId = AgnId
		AND		AgnAtyId = AtyId
		AND 	BooId = @psBooId
END
ELSE
BEGIN
	SELECT 	@value = 'You have selected for client ' + BkrLastName + ' on behalf of agent ' + ISNULL(REPLACE(AgnName, '&', '&amp;'), ''),
			@AgnCode = AgnCode,
			@AtyIsDirect = AtyIsDirect
		FROM 	BookingRequest WITH (NOLOCK), Agent WITH (NOLOCK), AgentType WITH (NOLOCK)
		WHERE	BkrAgnId = AgnId
		AND		AgnAtyId = AtyId
		AND 	Bkrid = @psBkrid
END

IF ISNULL(@AtyIsDirect, 0) = 0 
	SET @Msg = dbo.getErrorString('GEN178', NULL, NULL, NULL, NULL, NULL, NULL)

SELECT @UviValue = ISNULL(UviValue, '')
	FROM 	UniversalInfo 
	WHERE 	UviKey = 'ONLINEWHOLESALERS'

SET @EnabNewBoo = '0'
SET @start = 1
SET @count = dbo.getEntry(@UviValue, ',') + 1
WHILE @start <= @count
BEGIN
	SET @AgnCurr = LTRIM(RTRIM(dbo.getSplitedData(@UviValue, ',', @start)))
	SET @start = @start + 1
	IF @AgnCurr <> '' AND @AgnCurr = @AgnCode
	BEGIN
		SET @EnabNewBoo = '1'
		BREAK
	END
END -- WHILE @start <= @count

SELECT '<Header BooNum="' + ISNULL(@sBooNum, '') + '" id="' + @psBkrid + '" value="' + @value + '" ProvBut="' + ISNULL(@sProvBut, '') + '" NormBut="' + ISNULL(@sNormBut, '') + '" ByPass="' + @sBypass + '"/>'
SELECT '<SelectedVehicles>'
SELECT  
	SlvId													AS SlvId, 
	'' + PrdDvassSeq 										As PrdId, 
	ISNULL(CONVERT(VARCHAR,@psBooNum), '') + '/' + 
	ISNULL(CONVERT(VARCHAR,SlvRntNum),'') 					As RentalId,
	ISNULL(LEFT(DATENAME(DW, AvvCkoDate), 3), '')			AS CoDw,
	ISNULL(Convert(varchar, AvvCkoDate, 126), '') 			AS CoDate, 
	ISNULL(AvvCkoLocCode, '')								AS CoLoc, 
	ISNULL(AvvCkoAmPm, '') 									AS CkoDayPart,
	ISNULL(LEFT(DATENAME(DW, AvvCkiDate), 3), '')			AS CiDw,
	ISNULL(Convert(varchar, AvvCkiDate, 126), '')			AS CiDate, 
	ISNULL(AvvCkiLocCode, '') 								AS CiLoc, 
   	ISNULL(AvvCkiAmPm, '') 									AS CkiDayPart,
	ISNULL(Convert(varchar(4), AvpHirePeriod), '')+' '+
	ISNULL(dbo.getCodCode(AvpCodUomId), '')					AS Hper, 
	ISNULL(PkgCode, '')										AS Hpkg, 
	ISNULL(BrdName, '')										AS Veh, 
	ISNULL(PrdShortName, '')								AS VehSel, 		
/*
	CASE
		WHEN VhrRequestType IS NOT NULL AND
			 VhrRequestType = 'No Availablity Required' 
			THEN 'NA'
		WHEN VhrRequestType IS NOT NULL AND
			 VhrRequestType <> 'No Availablity Required' AND
			 SlvIsAvailable=1 
			THEN 'Yes'
			Else 'No'
	END 														AS Avail,
*/
	CASE
		WHEN ISNULL(VhrBlockingResult, '') = 'Bypass' OR SlvIsAvailable = 1	
			THEN 'Yes'
			Else 'No'
	END 														AS Avail,
	ISNULL(SlvAgentReference, ISNULL(VhrAgnRef, ''))			AS AgnRef,
	ISNULL(SlvCodSourceMocId, ISNULL(VhrCodSourceMocId, '')) 	AS ReqSrc,
	ISNULL(Convert(varchar, AvpTotCost), '')					AS Cost,
	ISNULL(dbo.getCodCode(AvpCodCurrId), '')					AS Curr,
	ISNULL(SlvStatus, '')										AS Status,
	'0' 														AS Chk,
	ISNULL(SlvIsPartOfConvoy, '0')								AS Poc,
	ISNULL(AvpBlockingResult, '')								AS AvpBlocking,
	ISNULL(SlvIsForced, '0')									AS Force,
	'' 															AS VehicleRanges,
	'10' 														AS Visibility,
	'1' 														AS Priority,
	ISNULL(CONVERT(VARCHAR,AvpTotCost), '') 					AS Revenue,
	ISNULL(SlvDvassNotified, 0)									AS DvassNotified,
	dbo.getIsUpdateDvass(AvvCkoLocCode, AvvPrdId, AvvCkoDate)  	AS IsThriftyUpdateDvass,
	dbo.getCountryForLocation(AvvCkoLocCode, '', '')			AS CtyCode
FROM SelectedVehicle  WITH (NOLOCK)
    LEFT OUTER JOIN AvailableVehiclePackage WITH (NOLOCK) ON SlvAvpId = AvpId 
	LEFT OUTER JOIN package WITH (NOLOCK) ON pkgid = avppkgid
    LEFT OUTER JOIN Availablevehicle WITH (NOLOCK) ON AvpAvvId = AvvId 
	LEFT OUTER JOIN product WITH (NOLOCK) ON prdid = avvprdid
	LEFT OUTER JOIN brand WITH (NOLOCK) ON brdcode = prdbrdcode
    LEFT OUTER JOIN VehicleRequest WITH (NOLOCK) ON AvvVhrId = VhrId 
    LEFT OUTER JOIN BookingRequest WITH (NOLOCK) ON VhrBkrid = Bkrid
WHERE	BkrId = @psBkrid
AND		prdisvehicle = 1
ORDER BY
	AvvCkoDate,
	AvvCkiDate,
	AvpCodCurrID, 
	prdshortname,
	PkgCode
FOR	XML AUTO,Elements

IF @@ROWCOUNT=0
SELECT	'<SelectedVehicle>
			<SlvId></SlvId>
			<PrdId></PrdId>
			<RentalId></RentalId>
			<CoDw/>
			<CoDate></CoDate>
			<CoLoc></CoLoc>
			<CkoDayPart> </CkoDayPart>
			<CiDw/>
			<CiDate></CiDate>
			<CiLoc></CiLoc>
			<CkiDayPart> </CkiDayPart>
			<Hper></Hper>
			<Hpkg></Hpkg>
			<Veh></Veh>
			<VehSel></VehSel>
			<Avail></Avail>
			<AgnRef></AgnRef>
			<ReqSrc></ReqSrc>
			<Cost></Cost>
			<Curr></Curr>
			<Status></Status>
			<Chk>0</Chk>
			<Poc>0</Poc>
			<AvpBlocking></AvpBlocking>
			<Force>0</Force>
			<VehicleRanges> </VehicleRanges>
			<Visibility> </Visibility>
			<Priority> </Priority>
			<Revenue> </Revenue>
			<DvassNotified>0</DvassNotified>
			<IsThriftyUpdateDvass>0</IsThriftyUpdateDvass>
			<CtyCode></CtyCode>
		</SelectedVehicle>'
SELECT 	'</SelectedVehicles>' + 
		'<EnabNewBoo>' + @EnabNewBoo + '</EnabNewBoo>' + 
		'<AgnDir>' + CAST(@AtyIsDirect AS VARCHAR) + '</AgnDir>' + 
		'<Msg>' + ISNULL(@Msg, '') + '</Msg>' + 
		'<UserCode> </UserCode>' + 
		'<PrgName> </PrgName>' + 
		'<Cty>' + ISNULL(@sCtyCode, '') + '</Cty>'
RETURN





-------------------------------------------------------------------
GO
-------------------------------------------------------------------



CREATE                Procedure [dbo].[RES_CreateBookingFromRequest_OSLO]
	@psBkrId	VARCHAR(64),		--booking Request ID
	@psUserId	VARCHAR(64),		--User ID (for create record)
	@psBooNum	varchar(64),		--Booking Number
	@psBooId	varchar(64) OUTPUT	--Booking Id
AS  


SET NOCOUNT ON
----------------------------------
--	START INITIALISE SECTION	--
----------------------------------

--	LOCAL VARIABLES	--
DECLARE
	@sBooNum				VARCHAR(64),
	@BkrAgnId				VARCHAR(64),
	@BkrConId				VARCHAR(64),
	@BkrTitle				VARCHAR(64),
	@BkrFirstName			VARCHAR(64),
	@BkrLastName			VARCHAR(64),
	@BkrMiscAgentName		VARCHAR(64),
	@BkrMiscAgentcontact	VARCHAR(64),
	@SlvStatus 				VARCHAR(64),
	@AvvCkoDate 			VARCHAR(64),
	@AvvCkiDate				VARCHAR(64),
	@sAvvCkiAmPm			varchar(3),
	@sAvvCkoAmPm			varchar(3),
	@sCkiLoc					varchar(3),
	@sCkoLoc					varchar(3),
	@dLocCkiTime			datetime,
	@dLocCkoTime			datetime

IF EXISTS (SELECT	SlvId
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK),
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('NN'))
	SET @SlvStatus = 'NN'
ELSE IF EXISTS (SELECT	SlvId 
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK),
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('KK'))
	SET @SlvStatus = 'KK'
ELSE IF EXISTS (SELECT	SlvId 
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK),
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('WL'))
	SET @SlvStatus = 'WL'
ELSE IF EXISTS (SELECT	SlvId 
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK),
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('QN'))
	SET @SlvStatus = 'QN'
ELSE IF EXISTS (SELECT	SlvId 
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK),
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('IN'))
	SET @SlvStatus = 'IN'
ELSE IF EXISTS (SELECT	SlvId 
			FROM	SelectedVehicle WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK), 
			Vehiclerequest WITH (NOLOCK)
			WHERE	SlvAvvId=AvvId
			AND	AvvVhrId=VhrId
			AND	VhrBkrId= @psBkrid
			AND	UPPER(SlvStatus) = ('KB'))
	SET @SlvStatus = 'KB'

SELECT	@AvvCkoDate = MIN(AvvCkoDate), 
			@AvvCkiDate = MAX(AvvCkiDate)
			FROM	AvailableVehicle WITH (NOLOCK) , VehicleRequest WITH (NOLOCK) 			
			WHERE	VhrBkrId=@psBkrId
			AND 	AvvVhrid = Vhrid

SELECT	@sAvvCkiAmPm = AvvCkiAmPm,
			@sAvvCkoAmPm = AvvCkoAmPm,
			@sCkiLoc     = AvvCkiLocCode,
			@sCkoLoc		 = AvvCkoLocCode	
			FROM	AvailableVehicle WITH (NOLOCK) , VehicleRequest WITH (NOLOCK) 			
			WHERE	VhrBkrId=@psBkrId
			AND 	AvvVhrid = Vhrid

Select @BkrConId = BkrConId, @BkrAgnId = BkrAgnId, 
			@BkrMiscAgentName = BkrMiscAgentName, @BkrMiscAgentContact = BkrMiscAgentContact, @BkrTitle = BkrTitle,
			@BkrFirstName = BkrFirstName, @BkrLastName =  BkrLastName
		FROM BookingRequest WITH (NOLOCK)
		WHERE BkrID = @psBkrId

SELECT	@dLocCkiTime	= dbo.RES_GetDefaultTime(@sCkiLoc, @sAvvCkiAmPm,'CKI'),
			@dLocCkoTime	= dbo.RES_GetDefaultTime(@sCkoLoc, @sAvvCkoAmPm,'CKO')
--	THEN we will need to amend the date(s) to be date-times
--rev:mia test..put comment on this one
--	@AvvCkiDate	= dbo.GEN_AddTimeToDate(@AvvCkiDate, @dLocCkiTime),
--	@AvvCkoDate	= dbo.GEN_AddTimeToDate(@AvvCkoDate, @dLocCkoTime)


/*  Get New Boooking ID and Insert Booking Record  */
SELECT	@psBooId				= @psBooNum --REPLACE(NewId(),'-','')
IF NOT EXISTS (select BooID FROM Booking WITH (NOLOCK) WHERE BooNum = @psBooNum)
BEGIN
	INSERT 
		INTO	Booking WITH (ROWLOCK) (	BooID, BooNum,	BooStatus,BooAgnId,BooMiscAgentName,BooMiscAgentContact,BooEarliestRentalCkoDate,BooLatestRentalCkiDate,IntegrityNo,BooTitle,	AddUsrId,BooFirstName,BooLastName,boosoundex,AddDateTime,AddPrgmName)
	VALUES (@psBooId, @psBooNum, @SlvStatus, @BkrAgnId, @BkrMiscAgentName, @BkrMiscAgentContact, @AvvCkoDate, @AvvCkiDate, 1, @BkrTitle, @psUserId, @BkrFirstName, @BkrLastName, soundex(@BkrLastName), CURRENT_TIMESTAMP, 'BookingRequest')
END

------------------------------
--	END PROCESSING SECTION	--
------------------------------


-------------------------------------------------------------------
GO
-------------------------------------------------------------------

create                       Procedure [dbo].[RES_CreateRentalFromRequest_OSLO]
		@psBooId		VARCHAR(64),				--Booking Id
		@psNextRntNum	VARCHAR(64),				--Next Rental Number for row add to table
		@psSlvId		VARCHAR(64),				--Selected vehicle Id
		@psRntId		VARCHAR(64)		OUTPUT,		--New Rental Id returned
		@psUsrId		VARCHAR(64),				--User Id for Table Insert
		@psProgramName	VARCHAR(64)					--Program Id for Table Insert
AS  

SET NOCOUNT ON


----------------------------------
--	START INITIALISE SECTION	--
----------------------------------

DECLARE	@CURRENT_TIMESTAMP		DATETIME,
		@AvvVhrId				VARCHAR(64),
		@AvvCkoLocCode			VARCHAR(12),
		@AvvCkiLocCode			VARCHAR(12),
		@AvvCkoAMPM				VARCHAR(2),
		@AvvCkiAMPM				VARCHAR(2),			
		@AvvCkoDate				DATETIME,
		@AvvCkiDate				DATETIME,
		@dCkoTime				DATETIME,
		@dCkiTime				DATETIME,
		@RntIsVip				BIT,
		@RntNumOfAdults			INT,
		@RntNumOfChildren		INT,
		@RntNumOfInfants		INT,
		@RntSSXfrTriggerDate	DATETIME,
		@sSql					VARCHAR(50)

SET @CURRENT_TIMESTAMP = cast(convert(varchar(10),CURRENT_TIMESTAMP,103) as datetime)

SET	@psRntId = @psBooId + '-' + @psNextRntNum

------------------------------
--	END INITIALISE SECTION	--
------------------------------

----------------------------------
--	Start Processing Section	--
----------------------------------

	
	IF EXISTS(	SELECT	TOP 1 IntegrityNo
				FROM	Agent WITH (NOLOCK)
				WHERE	AgnIsVip=1
				 AND	AgnId=(	SELECT	BooAgnID
								FROM	Booking WITH (NOLOCK)
								WHERE	BooId=@psBooId))
		SET @RntIsVip=1
	ELSE
		SET @RntIsVip=0
		
	SELECT	TOP 1 
			@AvvVhrId=AvvVhrId,
			@AvvCkoLocCode=AvvCkoLocCode,
			@AvvCkiLocCode=AvvCkiLocCode,
			@AvvCkoAMPM=AvvCkoAMPM,
			@AvvCkiAMPM=AvvCkiAMPM,
			@AvvCkoDate=AvvCkoDate,
			@AvvCkiDate=AvvCkiDate
	FROM	AvailableVehicle WITH (NOLOCK),
			SelectedVehicle WITH (NOLOCK)
	WHERE	AvvId=SlvAvvId
	 AND	SlvId=@psSlvId

		SELECT  @dCkoTime = DBO.RES_GetDefaultTime(@AvvCkoLocCode,@AvvCkoAmPm,'CKO'),
				@dCkiTime = DBO.RES_GetDefaultTime(@AvvCkiLocCode,@AvvCkiAmPm,'CKI')

	SELECT	TOP 1 
			@RntNumOfAdults=VhrNumOfAdults,
			@RntNumOfChildren=VhrNumOfChildren,
			@RntNumOfInfants=VhrNumOfInfants
	FROM	VehicleRequest WITH (NOLOCK)
	WHERE	VhrId=@AvvVhrId

	IF EXISTS (SELECT	agnid
				FROM	Agent WITH (NOLOCK)
				WHERE	AgnId=(	SELECT	BooAgnID
								FROM	Booking WITH (NOLOCK)
								WHERE	BooId=@psBooId)	
				AND     AgnCodDebtStatId = (select Codid from code WITH (NOLOCK) where CodCode = 'Unapproved' And CodCdtnum = 44))
	BEGIN
			SELECT	@RntSSXfrTriggerDate=@AvvCkoDate-convert(int,UviValue)
			FROM	UniversalInfo WITH (NOLOCK)
			WHERE	uviKey=DBO.GetCountryForLocation(@AvvCkoLocCode,'','')+'TriggerDaysB4Cko'
	END
	ELSE
		SET @RntSSXfrTriggerDate	=	@AvvCkoDate

IF NOT EXISTS (select rntid from Rental WITH (NOLOCK) WHERE RntBooID = @psBooId AND RntNum = @psNextRntNum)
BEGIN
	INSERT
	INTO	Rental(
		RntId,
		RntBooID,
		RntPrdRequestedVehicleId,
		RntNum,
		RntPkgId,
		RntIniStatus,
		RntAgencyRef,
		RntStatus,
		RntCodSourceMocId,
		RntBookedWhen,
		RntCkoLocCode,
		RntCkiLocCode,
		RntCkoWhen,
		RntCkiWhen,
		RntNumOfChildren,
		RntNumOfInfants,
		RntNumOfAdults,
		RntIsConvoy,
		RntNumOfHirePeriods,
		RntCodKnckTypId,
		IntegrityNo,
		AddUsrId,
		ModUsrId,
		AddDateTime,
		RntIsVip,
		ModDateTime,
		RntFollowUpWhen,
		AddPrgmName,
		RntSSXfrTriggerDate,
		RntCodUomId)
		
SELECT
		@psRntId,
		@psBooId,
		AvvPrdid,
--		RntConId,
		@psNextRntNum,
		AvpPkgId,
		SlvStatus,
		SlvAgentReference,
		SlvStatus,
		SlvCodSourceMocId,
		@Current_Timestamp,
		AvvCkoLocCode,
		AvvCkiLocCode,
		CASE WHEN exists (select usrid from dbo.userinfo (nolock) where usrcode = @psUsrId AND usrIsAgentuser = 1) OR exists (select usrid from dbo.userinfo (nolock) where usrcode = @psUsrId AND usrIsAgentuser = 1) 
			 THEN AvvCkoDate
			 ELSE AvvCkoDate--+@dCkoTime --rev:mia sept17
		END,
		CASE WHEN exists (select usrid from dbo.userinfo (nolock) where usrcode = @psUsrId AND usrIsAgentuser = 1) OR exists (select usrid from dbo.userinfo (nolock) where usrcode = @psUsrId AND usrIsAgentuser = 1) 
			 THEN AvvCkiDate
			 ELSE AvvCkiDate--+@dCkiTime--rev:mia sept17
		END,
--		RntArrivalConnectionRef,
--		RntArrivalConnectionWhen,
--		RntDeptConnectionRef,
--		RntDeptConnectionWhen,
		@RntNumOfChildren,
--		RntCancellationWhen,
		@RntNumOfInfants,
		@RntNumOfAdults,
--		RntVoucNum,
--		RntBookedWhenOffset,
		SlvIsPartOfConvoy,
--		RntCodCancTypId,
		AvpHirePeriod,
		SlvCodKnockbackTypeId,
--		RntOldRntNum,
		1,
--		RntCodCancelResonId,
		@psUsrId,
		@psUsrId,
--		RntCodCancelMocId,
--		RntXfrdToFinanWhen,
		CURRENT_TIMESTAMP,
		@RntIsVip,
		CURRENT_TIMESTAMP,
		--DBO.RES_GetFollowUpDate(@CURRENT_TIMESTAMP, @AvvCkoLocCode, SlvStatus) + @dCkoTime,
		--DBO.RES_GetFollowUpDate(@Current_Timestamp, @AvvCkoLocCode, SlvStatus) + @dCkoTime,
		--DBO.RES_GetFollowUpDate(@Current_Timestamp, @AvvCkoLocCode, SlvStatus),
		--DBO.RES_GetFollowUpDate(@Current_Timestamp, @AvvCkoLocCode, SlvStatus) + ' ' + convert(varchar(10), getdate(),108),
		case when SlvStatus = 'QN' then getdate() + 7 
			 when SlvStatus = 'NN' then getdate() + 1 
			 else getdate()
		end,
--		RntAreaUse,
		@psProgramName,
--		RntIsRepeatCustomer,
--		RntIsFirstUomFoc,
		@RntSSXfrTriggerDate,
		AvpCodUomId
FROM	SelectedVehicle WITH (NOLOCK),
		AvailableVehiclePackage WITH (NOLOCK),
		availablevehicle WITH (NOLOCK)
WHERE	SlvId=@psSlvId
 AND	SlvAvpId=AvpId
 AND  Avvid = SlvAvvid
END
--added manny
else
begin
	--print 'execute update in rental'
	declare @tempAvvCkoDate varchar(20)
    declare @tempAvvCkiDate varchar(20)
    select @tempAvvCkoDate = AvvCkoDate,@tempAvvCkiDate = AvvCkiDate FROM	SelectedVehicle WITH (NOLOCK),
		AvailableVehiclePackage WITH (NOLOCK),
		availablevehicle WITH (NOLOCK)
		WHERE	SlvId=@psSlvId
		AND	SlvAvpId=AvpId
		AND  Avvid = SlvAvvid

	update rental
     set 
		RntCkoWhen =@tempAvvCkoDate,
		RntCkiWhen =@tempAvvCkiDate,
		ModDateTime=CURRENT_TIMESTAMP,
        ModUsrId=@psUsrId
	
		WHERE RntBooID = @psBooId AND RntNum = @psNextRntNum
end


IF @@ERROR <> 0
	SELECT 'ERROR IN Assign'
------------------------------
--	End Processing Section	--
------------------------------
RETURN




-------------------------------------------------------------------
GO
-------------------------------------------------------------------



alter	Procedure [dbo].[RES_ManageCreateBookingFromRequest_OSLO] 
	@psBkrId		VARCHAR(64),	--booking Request ID
	@psInpBooId		VARCHAR(64) = NULL,
	@psNewBooNum	VARCHAR(10) = '',
	@psUsrId		VARCHAR(64),
	@psProgramName	VARCHAR(256)
	--@psBooNum		VARCHAR(64)	OUTPUT,
	--@psBooID		VARCHAR(64)	OUTPUT,
	--@psRntId		VARCHAR(64)	OUTPUT,
	--@psBpdId		VARCHAR(64)	OUTPUT,
	--@psChgId		VARCHAR(64)	OUTPUT,
	--@psChdId		VARCHAR(64)	OUTPUT,
	--@psNteId		VARCHAR(64)	OUTPUT,
	--@psMessage	VARCHAR(500) OUTPUT
AS  
SET NOCOUNT ON
SET ANSI_Warnings OFF

DECLARE @psBooNum		VARCHAR(64),
		@psBooID		VARCHAR(64),
		@psNteId		varchar(5000),
		@psMessage		varchar(500)

-- it is requaired for com
SET @psMessage = ''

----------------------------------
--	START INITIALISE SECTION	--
----------------------------------

DECLARE
	@SlvId			VARCHAR(64),
	@AprId			VARCHAR(64),
	--@NextRentalNum	INT,
	@RntId			VARCHAR(64),
	@SlvIsForced	BIT,
	@BpdId			VARCHAR(64),
	@sError			VARCHAR(500),
	@ReturnMessage	VARCHAR(500),
	@NteId			VARCHAR(64),
	@rRnhId			VARCHAR(64),
	@sSlvIdList		VARCHAR(8000),
	@sAprIdList		VARCHAR(8000),
	@SlvRntNum		int,
	@BookedWhen		datetime

--	LOCAL VARIABLES	--

SET @Bookedwhen = current_timestamp

DECLARE @d datetime
set @d = getdate()
--Insert into zzlog (zzlogtext) values (convert(varchar,current_timestamp,109) + '   ' + @psUsrId + ': ---' + 'Starting')


SELECT @sslvIdList = Case When @sSlvIdList is null then SlvId Else @sSlvIdList + ',' + SlvId END
	FROM	SelectedVehicle WITH (NOLOCK),
			VehicleRequest WITH (NOLOCK),
			AvailableVehicle WITH (NOLOCK)
	WHERE	SlvAvvId=AvvId
	 AND	AvvVhrId=VhrId
	 AND	VhrBkrId=@psBkrId


------------------------------
--	END INITIALISE SECTION	--
------------------------------

IF @psInpBooId IS NOT NULL AND LEN(@psInpBooId) > 0
BEGIN
	IF NOT EXISTS(SELECT BooId From Booking WITH (NOLOCK) where BooId = @psInpBooId)
	BEGIN
		SELECT 'INVALID @psBooId Passed into RES_ManageCreateBookingFromRequest'
		RETURN	
	END
END	
----------------------------------
--	START PROCESSING SECTION	--
----------------------------------


-- IF we are adding Rentals to existing Booking there is no necessity to ALTER  bookingNumber Record
IF @psInpBooId Is NULL OR @psInpBooId = ''
BEGIN
	IF ISNULL(@psNewBooNum, '') = ''
		--	Generate New Booking No	--
		EXEC RES_getBookingNumber
			@psBkrId=@psBkrId,
			@psUserId=@psUsrId,
			@psBooNum=@psBooNUM	OUTPUT
	ELSE
		SET @psBooNUM = @psNewBooNum
	
	--	Generate New Booking	--
	EXEC RES_CreateBookingFromRequest_OSLO
		@psBkrId	=	@psBkrId,
		@psUserId	=	@psUsrId,
		@psBooNum	=	@psBooNum,
		@psBooId	=	@PsBooId OUTPUT
	--SET @NextRentalNum=1
END
ELSE 
	SELECT @psBooID = BooId,
		   @psBooNum = BooNum 
	FROM   Booking WITH (NOLOCK)
	WHERE  BooId = @psInpBooID



WHILE @sslvIdList is not null and @sslvIdList <> ''
BEGIN

	SET @slvId = NULL
	EXECUTE	GEN_GetNextEntryAndRemoveFromList
		@psCommaSeparatedList	= @sslvIdList,
		@psListEntry		= @SlvId	OUTPUT,
		@psUpdatedList		= @sslvIdList	OUTPUT

	SELECT @SlvIsForced = SlvIsForced,	
		   @SlvRntNum = SlvRntNum
	FROM dbo.selectedVehicle (nolock)
	WHERE SlvId = @slvid
	
	
		--Allocate a rental Number & ALTER  A note	--
	IF EXists(select rntid from dbo.rental (nolock) where rntbooid = @psBooId and rntnum = @SlvRntNum)
		begin 
		EXEC RES_CreateRentalFromRequest_OSLO
			@psBooId=@psBooId,
			@psNextRntNum=@SlvRntNum,
			@psSlvId=@SlvId,
			@psRntId=@RntId OUTPUT,
			@psUsrId=@psUsrId,
			@psProgramName=@psProgramName
	
		RETURN
		end
		

		
		EXEC RES_CreateRentalFromRequest_OSLO
			@psBooId=@psBooId,
			@psNextRntNum=@SlvRntNum,
			@psSlvId=@SlvId,
			@psRntId=@RntId OUTPUT,
			@psUsrId=@psUsrId,
			@psProgramName=@psProgramName
		
	EXEC RES_ManageCreateCustomerFromRequest
		@psBkrId		,
		@psUsrId		,
		@RntId		
	
		--  insert Note  --
	EXEC	RES_CreateNotes
		@psPkgNoteRntId=@RntId,
		@psUsrId=@psUsrId,
		@psProgramName=@psProgramName,
		@psNteID=@NteId output

		--  Add NoteId to list of Notes  --
	IF @psNteId=''
		SET	@psNteId=@NteId
	ELSE
		SET	@psNteId=','+@psNteId

	-- 10/12/2003 ALTER  agent notes
	EXECUTE	RES_CreateNotes
		@psAgnNoteRntId	=	@RntId,
		@psUsrId		=	@psUsrId,
		@psProgramName	=	@psProgramName,
		@psNteID		=	@NteId 	OUTPUT
		
	--  Add NoteId to list of Notes  --
	IF @psNteId=''
		SET	@psNteId=@NteId
	ELSE
		SET	@psNteId=','+@psNteId

	--	If a VIP rental was created, generate an			--
	--	Internal note highlighting that it is a VIP rental	--
	IF EXISTS(	SELECT	TOP 1 NULL
				FROM	RENTAL WITH(nolock)
				WHERE	RntId=@RntId
				 AND	RntIsVip=1 )
		EXEC RES_CreateNotes
			@psVIPRntId=@RntId,
			@psUsrId=@psUsrId,
			@psProgramName=@psProgramName,
			@psNteID=@NteId OUTPUT
	
	IF @psNteId=''
		SET	@psNteId=@NteId
	ELSE
		SET	@psNteId=','+@psNteId

		
		--	insert  a booked product for each					--
		--	AvailableVehicleProduct of this selected vehicle	--

	SELECT @sAprIdList = Case When @sAprIdList is null then AprId Else @sAprIdList + ',' + AprId END
		FROM	AvailableVehicleProduct WITH (NOLOCK),
				SelectedVehicle WITH (NOLOCK)
		WHERE	SlvId=@SlvId
		 AND	AprAvpId=slvAvpId		
		
		WHILE @sAprIdList is not null and @sAprIdList <> ''
		BEGIN
			SET @AprId = NULL
			EXECUTE	GEN_GetNextEntryAndRemoveFromList
				@psCommaSeparatedList	= @sAprIdList,
				@psListEntry		= @AprId	OUTPUT,
				@psUpdatedList		= @sAprIdList	OUTPUT

			IF @AprId is not null and @AprId <> ''
			BEGIN
				SELECT @BpdId = null
				SELECT @sError = null
				
				EXEC RES_ManageBookedProductCreate
					@psAprID		=@AprId,
					@psSlvID		=@SlvId,
					@psRntID		=@RntId,
					@psCkoStatus	='Before Check Out',
					@pdBookedWhen	= @bookedwhen,
					--@pbDvassIsForced=@SlvIsForced,
					@psProgramName 	= 'RES_ManageCreateBookingFromRequest',
					@psUserID 		= @psUsrId,		
					@psBpdIdNew		=@BpdId OUTPUT,
					@pReturnError	=@sError OUTPUT,
					@pReturnMessage	=@ReturnMessage OUTPUT
				
			END
		
		END

		EXECUTE  RES_checkRentalHistory 
			@sBooId			= @psBooId,			
			@sRntId			= @RntId,
			@sChangeEvent	= 'ADD',
			@sUserCode 		= @psUsrId,
			@sAddPrgmName	= @psProgramName
END

------------------------------
--	END PROCESSING SECTION	--
------------------------------


--Insert into zzlog (zzlogtext) values (convert(varchar,current_timestamp,109) + '   ' + @psUsrId + ': ---' + 'Completed ' + @psBooNum + ' in ' + convert(varchar(5),datediff(ms,@d,getdate())))

RETURN

------------------------------
Go
------------------------------


-- 	QUERY FOR GEN_GETPOPUPDATA.SQL						
/*-------------------------------------------------------------------------------------------
	If Making Changes here, please make same change in SP_GetPopUpData
-------------------------------------------------------------------------------------------*/
ALTER	PROCEDURE [dbo].[GEN_GetPopUpData] @case	VARCHAR(30)	=	'',
	@param1	VARCHAR(64)	=	'',
	@param2	VARCHAR(64)	=	'',
	@param3	VARCHAR(500)	=	'',
	@param4 VARCHAR(64)	=	'' ,
	@param5 VARCHAR(64)  = '',
	-- KX Changes :RKS
	@sUsrCode varchar(64) = ''
AS
SET NOCOUNT ON
declare @stable table (spkgid varchar(64))
DECLARE	@RecCount	BIGINT,		@ErrStr			VARCHAR(200), 	@soryBy		VARCHAR(64),
		@start		INT,		@count			INT,			@startTmp	INT,	
		@ReturnPackageIDList varchar(8000),						@ReturnPackageIDList1	VARCHAR(8000),
		@countTmp	INT,		@pkgsErr		VARCHAR(2000),	@sloccode   varchar(12),
		@slocName   varchar(24),@bLocIsActive 	bit,			@iDoc		int,
		@agnId 		varchar(64),@BookedDate 	datetime,		@prdId 		varchar(64),
		@coDate		DATETIME,	@coLoc	VARCHAR(64),			@CiLoc  	VARCHAR(64),
		@iAd		INTEGER,	@iCh	INTEGER,				@iIn		INTEGER,
		@ciDate		DATETIME,	@RntId	VARCHAR(64)

-- KX Changes :RKS
DECLARE	@sFleetCompanyId	VARCHAR(64),
		@sComCode			VARCHAR(64)

-- Added : RKS 28/08 For Modify Rental Screen
IF(@case = 'CONTACTPOPUP')
BEGIN
	SELECT 
		ConId									AS	'CODE', 
		ConLastName + ', ' + ConFirstName  	AS 'DESCRIPTION'
	FROM 
		Booking WITH (NOLOCK), 
		Contact WITH (NOLOCK)
	WHERE 
		ConPrntId = BooAgnId 
		AND 
		ConPrntTableName='AGENT' 
		AND 
		BooId = @param1
	ORDER BY ConLastName
	FOR XML AUTO, ELEMENTS
	RETURN
END	-- IF(@case = 'CONTACTPOPUP')

IF(@case = 'CHKILOC')
BEGIN
	SELECT @sComCode = dbo.fun_getCompany
						(
							@sUsrCode,	-- @sUserCode	VARCHAR(64),
							NULL,		-- @sLocCode	VARCHAR(64),
							NULL,		-- @sBrdCode	VARCHAR(64),
							NULL,		-- @sPrdId		VARCHAR(64),
							NULL,		-- @sPkgId		VARCHAR(64),
							NULL,		-- @sdummy1	VARCHAR(64),
							NULL,		-- @sdummy2	VARCHAR(64),
							NULL		-- @sdummy3	VARCHAR(64)
						)
	SELECT
		LocCode	AS	'CODE', 
		LocName	AS	'DESCRIPTION'
	FROM
		Location WITH (NOLOCK)
	WHERE
		LocCode IN (SELECT DISTINCT RntCkiLocCode FROM Rental)
		AND LocComCode = @sComCode
	FOR XML AUTO, ELEMENTS
	RETURN
END


IF(@case = 'LocForBPD')
BEGIN
	--	@param1	LocationCode
	--	@param2	Country
	--	@param3	Product,
	--	@param4	RentalId ( This is to get the RntBookedWhen )
	--	@param5	Other Location

	DECLARE @bIsThriftyBpd			BIT,
			@sErrorsBpd			VARCHAR(500),
			@sThriftyLocTypeBpd	VARCHAR(500)

	SELECT @param3 = PrdId
		FROM	Product WITH(NOLOCK) 
		WHERE 	PrdShortName = @param3
		OR		PrdId = @param3

	SELECT	LocCode	AS	'CODE', 
			LocName	AS	'DESCRIPTION'
	INTO #TT_LocBpd
	FROM    Location WITH (NOLOCK) INNER JOIN
           	TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode,
			Code WITH (NOLOCK)
	WHERE	(LocCode LIKE @param1 + '%' OR LocName LIKE @param1 + '%')
	AND		LocIsActive	 = 	1
	AND		TownCity.TctCtyCode = @param2
	AND		LocCodTypId = CodId
	AND		DBO.GetCodCode(ISNULL(LocCodTypId,'')) <> 'CC' -- for Issue 381 on 09/07/2003
	AND		DBO.GetCodCode(ISNULL(LocCodTypId,'')) <> 'HQ' -- for Issue 381 on 09/07/2003
	-- KX Changes :RKS
	AND (@sUsrCode = '' OR LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
										  INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode
										  INNER JOIN dbo.UserInfo ON UserCompany.UsrId = UserInfo.UsrId
										  AND UsrCode = @sUsrCode)
		)
	IF ISNULL(@param5, '') <> '' AND ISNULL(@param3, '') <> '' AND ISNULL(@param4, '') <> ''
	BEGIN
		DECLARE	@sTriftyCtyBpd VARCHAR(12)
		SET @sTriftyCtyBpd = dbo.getCountryForLocation(@param5, NULL, NULL)
		SELECT	@BookedDate = RntBookedWhen
			FROM	Rental WITH(NOLOCK)
			WHERE	RntId = @param4
		EXECUTE	GEN_IsThrifty
			@sCtyCode			= 	@sTriftyCtyBpd, 
			@sPrdId				= 	@param3,
			@dCkoWhen			= 	NULL,
			@dBookedWhen		=	@BookedDate, 
			@sCkoLocCode		=	NULL, 
			@sCkiLocCode		=	NULL,
			@sRntNum			=	NULL,
			--	OutPut Parameters
			@bIsThrifty			=	@bIsThriftyBpd		OUTPUT,
			@sErrors 			=	@sErrorsBpd			OUTPUT,
			@sThriftyLocType	=	@sThriftyLocTypeBpd	OUTPUT	
	
		IF @bIsThriftyBpd = 1
		BEGIN
			DELETE FROM #TT_LocBpd
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode <> @sThriftyLocTypeBpd)
		END
		ELSE
		BEGIN
			DELETE FROM #TT_LocBpd
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode = @sThriftyLocTypeBpd)
		END
	END	--	IF ISNULL(@param3, '') <> '' AND ISNULL(@param1, '') <> ''  AND ISNULL(@param4, '') <> '' 

	SELECT * FROM #TT_LocBpd AS POPUP 
		ORDER BY 2
	FOR XML AUTO,ELEMENTS
	DROP TABLE #TT_LocBpd
	RETURN
END

IF(@case = 'LOCATIONFORVEHICLEREQ')
BEGIN
	--	This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp

	--	@param1 = CountryCode (CtyCode)
	--	@param2 = LocationCode
	--	@param3 = Product
	--	@param4 = CheckOutDate
	--	@param5 = Other Location
	DECLARE @bIsThrifty			BIT,
			@sErrors			VARCHAR(500),
			@sThriftyLocType	VARCHAR(12)
	
	IF @param5 <> '' AND EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param5)
		SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)

	IF @param3 <> '' AND @param5 <> ''
	BEGIN
		DECLARE	@sTriftyCty VARCHAR(12)
		SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)
		SET @BookedDate = CURRENT_TIMESTAMP

		SELECT @param3 = PrdId FROM Product WITH(NOLOCK) WHERE PrdIsActive = 1 AND  PrdId = @param3
		SELECT @param3 = PrdId FROM	Product WITH(NOLOCK) WHERE PrdIsActive = 1 AND  PrdShortName = @param3

		EXECUTE	GEN_IsThrifty
			@sCtyCode			= 	@sTriftyCty,
			@sPrdId				= 	@param3,
			@dCkoWhen			= 	NULL,
			@dBookedWhen		=	@BookedDate,
			@sCkoLocCode		=	NULL, 
			@sCkiLocCode		=	NULL,
			@sRntNum			=	NULL,
			--	OutPut Parameters
			@bIsThrifty			=	@bIsThrifty			OUTPUT,
			@sErrors 			=	@sErrors			OUTPUT,
			@sThriftyLocType	=	@sThriftyLocType	OUTPUT		

		declare @codId varchar(64)
		BEGIN
			IF @bIsThrifty = 1
				SET @codId = dbo.getCodeId(9, 'THRIFTY')
			ELSE IF  EXISTS(SELECT LocCode FROM dbo.Location (NOLOCK) WHERE LocCode = @param5 AND LocCodTypId = dbo.getCodeId(9,'GARNERS'))
				SET @codId = dbo.getCodeId(9, 'GARNERS')
			ELSE
				SET @codId = dbo.getCodeId(9, 'BRANCH')

			IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) 
						WHERE 	LocCode = @param2
						-- KX Changes :RKS
						AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
														  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																				INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																				INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
															)
							 )
							
						
				)
				SELECT 	LocCode	AS 'CODE',
						LocName AS 'DESCRIPTION'
					FROM Location AS POPUP WITH (NOLOCK)
					WHERE	LocIsActive = 1 
					AND		LocCode = @param2
					AND 	LocCodTypId = @codId
					-- KX Changes :RKS
					AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
													  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																			INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																			INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
														)
						)

				FOR XML AUTO,ELEMENTS				
			ELSE
				IF @param1 <> '' AND EXISTS(SELECT CtyCode FROM Country (NOLOCK) WHERE CtyCode = @param1)
					SELECT 	LocCode	AS 'CODE',
							LocName AS 'DESCRIPTION'
						FROM Location AS POPUP WITH (NOLOCK)
						WHERE	(LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
						AND		(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
						AND		LocIsActive	 = 	1
						AND		LocCodTypId = @codId
						-- KX Changes :RKS
						AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
														  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																				INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																				INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
															)
						)

					ORDER BY LocTctCode,LocCode
					FOR XML AUTO,ELEMENTS
				ELSE
					SELECT 	LocCode	AS 'CODE',
							LocName AS 'DESCRIPTION'
						FROM Location AS POPUP WITH (NOLOCK)
						WHERE	(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
						AND		LocIsActive	 = 	1
						AND		LocCodTypId = @codId
					-- KX Changes :RKS
						AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
														  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																				INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																				INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
															)
						)

					ORDER BY LocCode
					FOR XML AUTO,ELEMENTS
		END
	END	--	IF @param3 <> '' AND @param5 <> ''
	ELSE
	BEGIN
		IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) 
					WHERE	LocIsActive = 1 
					AND		LocCode = @param2)
			SELECT 	LocCode	AS 'CODE',
					LocName AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	LocIsActive = 1 
				AND		LocCode = @param2
				-- KX Changes :RKS
				AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
												  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																		INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
													)
					)

			FOR XML AUTO,ELEMENTS	
		ELSE
			SELECT 	LocCode	AS 'CODE',
					LocName AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
				AND		LocIsActive	 = 	1
				-- KX Changes :RKS
				AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
												  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																		INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
													)
				)

			ORDER BY LocCode
		FOR XML AUTO,ELEMENTS
	END
	RETURN	
END

IF(@case = 'LOCATIONFORVEHICLEREQ1')
BEGIN
	--	This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp
	--	@param1 = CountryCode (CtyCode)
	--	@param2 = LocationCode
	--	@param3 = Product
	--	@param4 = CheckOutDate
	--	@param5 = Other Location
/*
	DECLARE @bIsThrifty			BIT,
			@sErrors			VARCHAR(500),
			@sThriftyLocType	VARCHAR(12)
*/	
	SELECT @param3 = PrdId
		FROM	Product WITH(NOLOCK) 
		WHERE 	PrdShortName = @param3
		OR		PrdId = @param3
	
	IF @param5 <> '' AND EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param5)
		SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)

	SELECT	LocCode  	AS 'CODE',
			LocName 	AS 'DESCRIPTION'
	INTO #TT_LocVhr
	FROM Location AS POPUP WITH (NOLOCK)
	WHERE	1=2

	IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param2)
		INSERT INTO #TT_LocVhr
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	(@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
				AND		LocCode = @param2
	ELSE
		INSERT INTO #TT_LocVhr
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	(@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
				AND		(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
				AND		LocIsActive	 = 	1
				AND		DBO.GetCodCode(LocCodTypId) <> 'CC'
				AND		DBO.GetCodCode(LocCodTypId) <> 'HQ'

	IF @param3 <> '' AND @param5 <> ''
	BEGIN
		--DECLARE	@sTriftyCty VARCHAR(12)
		SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)
		SET @BookedDate = CURRENT_TIMESTAMP
		EXECUTE	GEN_IsThrifty
			@sCtyCode			= 	@sTriftyCty,
			@sPrdId				= 	@param3,
			@dCkoWhen			= 	NULL,
			@dBookedWhen		=	@BookedDate,
			@sCkoLocCode		=	NULL, 
			@sCkiLocCode		=	NULL,
			@sRntNum			=	NULL,
			--	OutPut Parameters
			@bIsThrifty			=	@bIsThrifty			OUTPUT,
			@sErrors 			=	@sErrors			OUTPUT,
			@sThriftyLocType	=	@sThriftyLocType	OUTPUT		

		IF @bIsThrifty = 1
		BEGIN
			DELETE FROM #TT_LocVhr 
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode <> @sThriftyLocType)
		END
		ELSE
		BEGIN
			DELETE FROM #TT_LocVhr 
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode = @sThriftyLocType)
		END
	END
	
	SELECT * FROM #TT_LocVhr AS POPUP 
		ORDER BY 1
		FOR XML AUTO,ELEMENTS
	DROP TABLE #TT_LocVhr
	RETURN
END
/*
IF(@case = 'LOCATIONFORVEHICLEREQ2')
BEGIN
	--	This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp
	--	@param1 = CountryCode (CtyCode)
	--	@param2 = LocationCode
	--	@param3 = Product
	--	@param4 = CheckOutDate
	--	@param5 = Other Location
	DECLARE @bIsThrifty			BIT,
			@sErrors			VARCHAR(500),
			@sThriftyLocType	VARCHAR(12)
	
	IF EXISTS(select prdid from dbo.product (nolock) where PrdShortName = @param3)
		SELECT @param3 = PrdId FROM	dbo.Product WITH(NOLOCK)  WHERE PrdShortName = @param3
	ELSE
		SELECT @param3 = PrdId FROM	dbo.Product WITH(NOLOCK)  WHERE PrdId = @param3

	
	IF @param5 <> '' AND EXISTS(SELECT LocCode FROM dbo.Location WITH(NOLOCK) WHERE LocCode = @param5)
		SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)

	IF @Param1 is not null and @Param1 = 'NZ'
	BEGIN
		IF exists( select loccode From dbo.location (nolock) where loccode = @param2)
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
			FROM dbo.Location AS POPUP WITH (NOLOCK)
			WHERE LocCode = @param2
			FOR XML AUTO,ELEMENTS
		ELSE
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
			FROM dbo.Location AS POPUP WITH (NOLOCK)
			WHERE (LocCode like @param2 + '%'
			OR 	  LocName like @param2 + '%')
			AND   Exists (select codid from dbo.code (nolock) where Codid = LocCodTypid and CodCode = 'Branch')
			FOR XML AUTO,ELEMENTS
		RETURN
	END
	
	IF (@Param5 is null OR @Param5 = '') 
		AND @param2 is not null and @param2 <> ''
	BEGIN
		IF EXISTS(SELECT LocCode FROM dbo.Location WITH(NOLOCK) WHERE LocCode = @param2)
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
			FROM dbo.Location AS POPUP WITH (NOLOCK)
			WHERE LocCode = @param2
			FOR XML AUTO,ELEMENTS
		ELSE
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
			FROM dbo.Location AS POPUP WITH (NOLOCK)
			WHERE (LocCode like @param2 + '%'
			OR 	  LocName like @param2 + '%')
			AND   Exists (select codid from dbo.code (nolock) where Codid = LocCodTypid and CodCode in ('Branch','Thrifty'))
			FOR XML AUTO,ELEMENTS
		return
	END		

	SELECT	LocCode  	AS 'CODE',
			LocName 	AS 'DESCRIPTION'
	INTO #TT_LocVhr

	FROM Location AS POPUP WITH (NOLOCK)
	WHERE	1=2

	IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param2)
		INSERT INTO #TT_LocVhr
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	(@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
				AND		LocCode = @param2
	ELSE
		INSERT INTO #TT_LocVhr
			SELECT	LocCode  	AS 'CODE',
					LocName 	AS 'DESCRIPTION'
				FROM Location AS POPUP WITH (NOLOCK)
				WHERE	(@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
				AND		(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
				AND		LocIsActive	 = 	1
				AND		DBO.GetCodCode(LocCodTypId) <> 'CC'
				AND		DBO.GetCodCode(LocCodTypId) <> 'HQ'

	IF @param3 <> '' AND @param5 <> ''
	BEGIN
		DECLARE	@sTriftyCty VARCHAR(12)
		SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)
		SET @BookedDate = CURRENT_TIMESTAMP
		EXECUTE	GEN_IsThrifty
			@sCtyCode			= 	@sTriftyCty,
			@sPrdId				= 	@param3,
			@dCkoWhen			= 	NULL,
			@dBookedWhen		=	@BookedDate,
			@sCkoLocCode		=	NULL, 
			@sCkiLocCode		=	NULL,
			@sRntNum			=	NULL,
			--	OutPut Parameters
			@bIsThrifty			=	@bIsThrifty			OUTPUT,
			@sErrors 			=	@sErrors			OUTPUT,
			@sThriftyLocType	=	@sThriftyLocType	OUTPUT		

		IF @bIsThrifty = 1
		BEGIN
			DELETE FROM #TT_LocVhr 
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode <> @sThriftyLocType)
		END
		ELSE
		BEGIN
			DELETE FROM #TT_LocVhr 
				WHERE	CODE IN (SELECT	LocCode
									FROM Location WITH(NOLOCK), Code WITH(NOLOCK)
									WHERE	LocCodTypId = CodId
									AND		CodCode = @sThriftyLocType)
		END
	END
	
	SELECT * FROM #TT_LocVhr AS POPUP 

		ORDER BY 1
		FOR XML AUTO,ELEMENTS
	DROP TABLE #TT_LocVhr
	RETURN
END
*/
/*
IF(@case = 'LocForBPD')
BEGIN
	IF EXISTS(select loccode from dbo.location (nolock) where loccode = @param1 AND	LocIsActive	 = 	1)
	BEGIN
		SELECT
			LocCode	AS	'CODE', 
			LocName	AS	'DESCRIPTION'
		FROM         
				Location WITH (NOLOCK) INNER JOIN
	           	TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
				inner Join Code (nolock) on Codid = LocCodtypid	
		WHERE LocCode 	= 	@param1 
		AND	  LocIsActive	 = 	1
		AND CodCode in ('Branch','Thrifty')
		AND	  TownCity.TctCtyCode = @param2
		FOR XML AUTO, ELEMENTS
		RETURN
	END
	ELSE BEGIN	
		SELECT
			LocCode	AS	'CODE', 
			LocName	AS	'DESCRIPTION'
		FROM         
				Location WITH (NOLOCK) INNER JOIN
	           	TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
				inner Join Code (nolock) on Codid = LocCodtypid	
		WHERE
			(LocCode 	LIKE	@param1 + '%' OR
		     LocName 	LIKE	@param1 + '%' )
			AND		LocIsActive	 = 	1
			AND CodCode in ('Branch','Thrifty')
			AND		TownCity.TctCtyCode = @param2
		FOR XML AUTO, ELEMENTS
		RETURN
	END
END*/

IF(@case = 'checkLocation')
BEGIN
	DECLARE @iCount INT
	SELECT 	@iCount = COUNT(integrityNo) 
	 FROM 	Location WITH(NOLOCK) 
	 WHERE 	LocCode = @param1 
	 AND 	DBO.GetCodCode(ISNULL(LocCodTypId,'')) IN ('HQ','CC')
	 -- KX Changes :RKS
	 AND (@sUsrCode = '' OR LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
											INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode))
	IF @iCount > 0
		SELECT '<isHQCC>1</isHQCC>'
	ELSE
		SELECT '<isHQCC>0</isHQCC>'
	RETURN 
END

IF(@case = 'CHKOLOC')
BEGIN
	SELECT @sComCode = dbo.fun_getCompany
						(
							@sUsrCode,	-- @sUserCode	VARCHAR(64),
							NULL,		-- @sLocCode	VARCHAR(64),
							NULL,		-- @sBrdCode	VARCHAR(64),
							NULL,		-- @sPrdId		VARCHAR(64),
							NULL,		-- @sPkgId		VARCHAR(64),
							NULL,		-- @sdummy1	VARCHAR(64),
							NULL,		-- @sdummy2	VARCHAR(64),
							NULL		-- @sdummy3	VARCHAR(64)
						)

	SELECT
		LocCode	AS	'CODE', 
		LocName	AS	'DESCRIPTION'
	FROM
		Location WITH (NOLOCK)
	WHERE
		EXISTS (select codid from Code WITH (NOLOCK) where Codcdtnum = 9 and CodCode = 'branch' and CodId = LocCodTypId)
		AND LocComCode = @sComCode
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF(@case = 'NEWAGENTGROUP')
BEGIN
	/* Time beeing it is in issue to akl*/
	SELECT AgpCode	AS	'CODE',
		AgpDesc	AS	'DESCRIPTION'

	FROM	AgentGroup WITH (NOLOCK)
	WHERE AgpCode LIKE	@param1 + '%'
	union	
	SELECT AgpCode	AS	'CODE',
		AgpDesc	AS	'DESCRIPTION'
	FROM	AgentGroup WITH (NOLOCK)

	WHERE 
		AgpDesc  LIKE	@param1 + '%'		
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case='GETDATAFORUSER')
BEGIN
	SELECT @sComCode = dbo.fun_getCompany
						(
							@sUsrCode,	-- @sUserCode	VARCHAR(64),
							NULL,		-- @sLocCode	VARCHAR(64),
							NULL,		-- @sBrdCode	VARCHAR(64),
							NULL,		-- @sPrdId		VARCHAR(64),
							NULL,		-- @sPkgId		VARCHAR(64),
							NULL,		-- @sdummy1	VARCHAR(64),
							NULL,		-- @sdummy2	VARCHAR(64),
							NULL		-- @sdummy3	VARCHAR(64)
						)

	SELECT
		UsrCode		AS		'DESCRIPTION',
		UsrName		AS		'CODE'
	FROM
		UserInfo WITH (NOLOCK),
		dbo.UserCompany (NOLOCK)
	WHERE
		UserInfo.UsrId = UserCompany.UsrId
			AND
		UsrIsActive=1		
			AND
		 (UsrCode LIKE @param1+'%' OR UsrName LIKE @param1+'%')
		AND (@sUsrCode = '' OR UserCompany.ComCode  = @sComCode)
		
	--UNION
	--SELECT
	--	UsrCode		AS		CODE,
	--	UsrName		AS		'DESCRIPTION'
	--FROM
	--	UserInfo WITH (NOLOCK)
	--WHERE
	--	UsrIsActive=1		
	--		AND
	--	(UsrName LIKE @param1+'%')
	FOR XML AUTO, ELEMENTS
	RETURN
END


IF(@case	=	'SALEABLEPRODUCT')
BEGIN
	DECLARE @varCtyCode 	VARCHAR(24)
	DECLARE @sPkgBrdCode	VARCHAR(5)
	SELECT 	@varCtyCode = pkgctyCode, 
		@sPkgBrdCode= pkgBrdCode 
		FROM package WITH (NOLOCK) 
		WHERE pkgid= @param3

	IF(@param1 <> '' AND @param2 <> '')
	BEGIN
		SELECT 	PrdName						AS ProductName,
			PrdShortName + ' - ' + CAST (SapSuffix AS varchar(3))	AS ShortSuffix
		FROM Product  WITH (NOLOCK), Saleableproduct WITH (NOLOCK)
			WHERE PrdShortName = @param1
			AND	 CAST (SapSuffix AS varchar(3)) = @param2 
			AND	SapPrdId	= 	PrdId
			AND	SapCtyCode	=	@varCtyCode
			AND	(PrdBrdCode = @sPkgBrdCode OR
							 1 IN (SELECT BrdIsGeneric FROM Brand WITH (NOLOCK)
								   WHERE BrdCode = PrdBrdCode
							         )	
							)
			AND SapStatus = 'Active'
			-- KX Changes : RKS
			AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
												 WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
												)
				)
		FOR XML AUTO,ELEMENTS

	END
	ELSE
	BEGIN
		SELECT PrdName						AS ProductName,
			PrdShortName + ' - ' + CAST (SapSuffix AS varchar(3))	 AS ShortSuffix
		FROM Product WITH (NOLOCK) ,Saleableproduct WITH (NOLOCK)
			WHERE PrdShortName LIKE 	@param1 + '%'
			AND	SapPrdId	= 	PrdId
			AND	SapCtyCode	=	@varCtyCode
			AND	(PrdBrdCode = @sPkgBrdCode OR
							 1 IN (SELECT BrdIsGeneric FROM Brand WITH (NOLOCK)
								   WHERE BrdCode = PrdBrdCode
							         )	
							)
			AND SapStatus = 'Active'
			-- KX Changes : RKS
			AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
												 WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
												)
				)
		FOR XML AUTO,ELEMENTS
		
	END
	RETURN
END

/*	Class PopUp	Retrive ClassCode,ClassDescription	*/

IF(@case	=	'CLASS')
BEGIN
	SELECT	ClaCode	AS	'CODE',
			ClaDesc	AS	'DESCRIPTION'
		FROM 	Class	AS	POPUP  WITH (NOLOCK)
		WHERE	ClaCode	LIKE	@param1+'%'
		AND		ClaIsActive	=	1
		FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'PACKAGE')
BEGIN
	SELECT	
			PkgId		AS	'ID',
			PkgCode	AS	'CODE',
			PkgName	AS	'DESCRIPTION'
	FROM 	
			Package	AS	POPUP WITH (NOLOCK)
	WHERE	
			(PkgCode	LIKE	@param1+'%'
		OR	PkgName	LIKE	@param1+'%')
		AND	PkgIsActive	=	'Active'
		AND	ISNULL(PkgCodTypId,'')	LIKE ISNULL(@param2, '')+'%'
		AND	PkgCode > @param3
		-- KX Changes : RKS
		AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
											 WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
											)
			)
	FOR XML AUTO,ELEMENTS
	RETURN
END
---------------------------------------------------------------------------------
--Modified for AgentPackageMgt.asp. 26/09/2002 by James Liu
IF(@case	=	'PACKAGE_AGENT_PACKAGETYPE')
BEGIN
	SELECT	PkgId		AS	'ID',
			PkgCode	AS	'CODE',
			PkgName	AS	'DESCRIPTION'
	FROM 	Package	AS	POPUP WITH (NOLOCK)
	WHERE	(PkgCode LIKE @param1+'%' OR PkgName LIKE @param1+'%' OR @param1 = '')
	AND		PkgIsActive	= 'Active'
	AND		(isnull(PkgCodTypId,'') LIKE @param2+'%' OR @param2 = '')
	--AND		CONVERT(CHAR(10),PkgBookedToDate,103) >= CONVERT(CHAR(10),CURRENT_TIMESTAMP,103)
	-- KX Changes : RKS
	AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
										 WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
										)
		)
	ORDER BY PkgCode
	FOR XML AUTO,ELEMENTS
	RETURN
END
-----------------------------------------------------------------------------------

IF(@case	=	'PACKAGEPRODUCT')
BEGIN
	DECLARE @sPkgBrd VARCHAR(10)
	DECLARE @sPkgCty VARCHAR(24)
	
	SELECT 	@sPkgBrd = PkgBrdCode,	@sPkgCty = PkgCtyCode
		FROM 	Package WITH (NOLOCK)
		WHERE 	PkgId = @param1
	
	SELECT	PkgCode	AS	'CODE',
		PkgName	AS	'DESCRIPTION'
		FROM 	Package	AS	POPUP WITH (NOLOCK)
		WHERE	PkgId 		<> 	@param1
		AND	(PkgCode		LIKE	@param2 + '%'
		OR	 PkgName		LIKE	@param2 + '%')
		AND	PkgBrdCode 	= 	@sPkgBrd
		AND	PkgCtyCode 	= 	@sPkgCty
		AND	PkgIsActive	=	'Active'
		-- KX Changes :RKS
		AND PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
							INNER JOIN dbo.Company (NOLOCK) ON BrdComCode = ComCode
							INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
							INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
							AND UsrCode = @sUsrCode)
		FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'POPUPCOUNTRY')
BEGIN
	IF EXISTS (SELECT 	CtyCode 
				FROM 	Country WITH (NOLOCK)	
				WHERE 	CtyCode    = @param1
				AND		CtyIsActive = 1)
	BEGIN
		SELECT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		 FROM 	Country WITH (NOLOCK)	
		 WHERE 	CtyCode    = @param1
		 AND	CtyIsActive = 1
		FOR XML AUTO,ELEMENTS	
	END
	ELSE
	BEGIN
		SELECT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		 FROM 	Country WITH (NOLOCK)	
		 WHERE 	(CtyCode LIKE @param1+'%')
		 AND	CtyIsActive = 1
		UNION
		SELECT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		 FROM 	Country WITH (NOLOCK)	
		 WHERE 	(CtyName LIKE @param1+'%')
		 AND	CtyIsActive = 1	
		ORDER BY CtyName  -- JPL issue # 324
		FOR XML AUTO,ELEMENTS	
	END
	RETURN
END

IF(@case	=	'GETCOUNTRY_NEW')
BEGIN	
	SELECT DISTINCT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country	AS	POPUP WITH (NOLOCK)
		WHERE	(CtyCode  = @param1 OR CtyName = @param1)
		AND	CtyIsActive		=	1	
		ORDER BY CtyName
		FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'AGNENTCOUNTRY')
BEGIN
	SELECT DISTINCT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country	AS	POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)
		WHERE CtyCode LIKE @param1+'%'	
		AND	AddCtyCode IS NOT NULL
		AND	CtyCode = AddCtyCode
		AND	AddPrntTableName	=	'Agent'
		AND	CtyIsActive		=	1
	UNION
	SELECT DISTINCT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country	AS	POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)
		WHERE	CtyName  LIKE @param1+'%'
		AND	AddCtyCode IS NOT NULL
		AND	CtyCode = AddCtyCode
		AND	AddPrntTableName	=	'Agent'
		AND	CtyIsActive		=	1	
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'CUSTOMERCOUNTRY')
BEGIN
	SELECT DISTINCT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country	AS	POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)

		WHERE(CtyCode LIKE @param1+'%'
		OR	CtyName  LIKE @param1+'%')
		AND	AddCtyCode IS NOT NULL
		AND	CtyCode = AddCtyCode
		AND	AddPrntTableName	=	'Customer'
		AND	CtyIsActive		=	1
		ORDER BY CtyCode
		FOR XML AUTO,ELEMENTS	
	RETURN

END

IF(@case	=	'AGENT')
BEGIN
	IF NOT EXISTS(select agncode from dbo.Agent (nolock) where Agncode = @param1)
	begin
		SELECT @RecCount = COUNT(agnid) FROM Agent AS POPUP WITH (NOLOCK) WHERE (AgnCode	LIKE	@param1+'%') AND AgnIsActive = 1
		SELECT @RecCount  = @RecCount +  COUNT(agnid) FROM Agent AS POPUP WITH (NOLOCK) WHERE (AgnName LIKE @param1+'%') AND AgnIsActive = 1

		IF @RecCount>200 
		BEGIN
			SET @param1 = '"' + @param1 + '"'
	        	Exec sp_get_ErrorString 'GEN022','Search Criteria', @param1, @oparam1 = @ErrStr OUTPUT
		    	SELECT '<Error><ErrStatus>GEN022</ErrStatus><ErrCode>GEN022</ErrCode><ErrDesc>' + @ErrStr + '</ErrDesc></Error>'
	            	RETURN
		END

    END
    	

	IF EXISTS(SELECT agnid FROM AGENT WITH (NOLOCK) WHERE AgnCode = @param1 AND AgnIsActive = 0)
	BEGIN
		SET @param1 = '"' + @param1 + '"'
        SELECT '<Error><ErrStatus></ErrStatus><ErrCode></ErrCode><ErrDesc>This Agent is inactive. Bookings may not be made for this Agent.</ErrDesc></Error>'
        RETURN
	END
		
	IF EXISTS(SELECT agnid FROM AGENT WITH (NOLOCK) WHERE AgnCode = @param1)
		SELECT DISTINCT 	
				AgnId		AS	'ID',
				AgnCode		AS	'CODE',
				AgnName	 	AS	'DESCRIPTION',
				AgnIsMisc	AS	'ISMISC'
		FROM Agent AS POPUP WITH (NOLOCK)
		WHERE 	AgnCode = @param1
		AND		AgnIsActive = 1
		FOR XML AUTO,ELEMENTS	
	ELSE
		SELECT DISTINCT 	
				AgnId		AS	'ID',
				AgnCode		AS	'CODE',
				AgnName	 	AS	'DESCRIPTION',
				AgnIsMisc	AS	'ISMISC'
		FROM Agent AS POPUP WITH (NOLOCK)
		WHERE 	(AgnCode	LIKE	@param1+'%'
			OR	AgnName 	LIKE	@param1+'%')
		AND	AgnIsActive = 1
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'AGENTGROUP')
BEGIN
	SELECT DISTINCT AgpCode	AS	'CODE',
			AgpDesc AS	'DESCRIPTION'
		FROM AgentGroup AS POPUP WITH (NOLOCK)
		WHERE 	AgpCode LIKE	@param1+'%'
		OR	AgpDesc LIKE	@param1+'%'
		ORDER BY 1
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'AGNENTSUBGROUP')
BEGIN
	SELECT DISTINCT 	''+AgpCode	AS	'GrpCode',
				''+AgpDesc	AS	'GrpDesc',
				''+AsgCode 	AS	'SubGrpCode',
				''+AsgDesc	AS	'SubGrpDesc'
		FROM AgentGroup AS POPUP WITH (NOLOCK), AgentSubgroup WITH (NOLOCK)
		WHERE (AgpCode LIKE	@param1+'%'
		OR	  AgpDesc LIKE	@param1+'%')
		AND	  AgpId = AsgAgpId
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'SUBGROUP')
BEGIN
	SELECT DISTINCT ''+AsgId	AS	'ID',
			''+AgpCode	AS	'GrpCode',
			''+AgpDesc	AS	'GrpDesc',
			''+AsgCode 	AS	'SubGrpCode',
			''+AsgDesc	AS	'SubGrpDesc'
		FROM AgentGroup AS POPUP WITH (NOLOCK), AgentSubgroup WITH (NOLOCK)
		WHERE (AgpCode LIKE	@param1+'%'
		OR	  AgpDesc LIKE	@param1+'%')
		AND	  AgpId = AsgAgpId

		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'AGENTFORSUBGROUP')
BEGIN
	SELECT DISTINCT ''+AgnId	AS	'ID',
			''+AgnCode	AS	'CODE',
			''+AgnName	AS	'DESCRIPTION'
		FROM Agent AS POPUP WITH (NOLOCK)
		WHERE 	(  AgnCode LIKE	@param1+'%'
			OR AgnName LIKE	@param1+'%')
		AND	AgnAtyId LIKE	@param2+'%'
		AND	AgnAsgId LIKE	@param3+'%'
		AND	AgnCode > @param4
		ORDER BY 2
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'AGENTPACKAGE4BOOKINGREQ')
BEGIN
	IF (@param3 <> '')
	BEGIN 

		declare @sPkgidCurrent varchar(64)
		SET @ReturnPackageIDList = ''
		--SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)
		SET @BookedDate = Current_TimeSTAMP
		SELECT 	@agnId = dbo.getSplitedData(@param3, ',', 1),
			@prdId = dbo.getSplitedData(@param3, ',', 2),
			@coDate = dbo.getSplitedData(@param3, ',', 3),
			@coLoc = dbo.getSplitedData(@param3, ',', 4),
			@CiLoc = dbo.getSplitedData(@param3, ',', 5),
			@iAd = dbo.getSplitedData(@param3, ',', 6),
			@iCh = dbo.getSplitedData(@param3, ',', 7),
			@iIn = dbo.getSplitedData(@param3, ',', 8),
			@ciDate = dbo.getSplitedData(@param3, ',', 9)

		IF (@coDate = '')	SET @coDate = NULL
		IF (@ciDate = '') 	SET @ciDate = NULL
		IF (@coLoc = '')	SET @coLoc = NULL
		IF (@CiLoc = '')	SET @CiLoc = NULL
		IF (@iAd = '')		SET @iAd = NULL
		IF (@iCh = '')		SET @iCh = NULL
		IF (@iIn = '')		SET @iIn = NULL


--	MTS 20JUN02 : Added "named parameters" after my changes

--			to RES_getValidPackages() made this call fail.
		--select @agnId, @BookedDate, @prdId, @coDate, @ciDate, @coLoc, @CiLoc, @iAd, @iCh, @iIn
		EXECUTE RES_getValidPackages
			@AgentID				= @agnId,
			@BookedDate				= @BookedDate,
			@ProductID				= @prdId,
			@checkOutDate			= @coDate,
			@checkInDate			= @ciDate,
			@CheckOutLocationCode	= @coLoc,
			@CheckInLocationCode	= @CiLoc,
			@NumberOfAdults			= @iAd,
			@NumberOfChildren		= @iCh,
			@NumberOfInfants		= @iIn,
			@VehicleRequestID		= NULL,
			@BlockingRuleIDList		= NULL,
			-- KX Changes :RKS
			@sUsrCode				= @sUsrCode,
			@ReturnPackageIDList	= @ReturnPackageIDList	OUTPUT

--	MTS 06AUG02 : removed
--			@psErrors				= @pkgsErr	OUTPUT

		IF ISNULL(@pkgsErr, '') <> '' AND ISNULL(@ReturnPackageIDList, '') = ''
		BEGIN
			SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @pkgsErr + '</ErrDescription></Error>'			
		END
		ELSE IF ISNULL(@pkgsErr, '') = '' AND ISNULL(@ReturnPackageIDList, '') = ''
		BEGIN
			SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'
		END
		ELSE
		BEGIN
			
--			EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @ReturnPackageIDList
--			IF (@@ERROR <> 0 ) RETURN
		SELECT	@ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '<Data><PackageID>', ''),
		--	2. strip the trailing elements off
			@ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '</PackageID></Data>', ''),
		--	3. Change the intermediate ones to a COMMA
			@ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '</PackageID><PackageID>', ',')
		WHILE	@ReturnPackageIDList IS NOT NULL
		AND	LEN(@ReturnPackageIDList) > 0
		BEGIN
			SET @sPkgidCurrent = null
			EXECUTE	GEN_GetNextEntryAndRemoveFromList
					@psCommaSeparatedList	= @ReturnPackageIDList,
					@psListEntry		= @sPkgidCurrent	OUTPUT,
					@psUpdatedList		= @ReturnPackageIDList	OUTPUT
				

			IF @sPkgidCurrent is not null and @sPkgidCurrent <> ''
				insert into @stable values (@sPkgidCurrent)
		END

			IF EXISTS(SELECT pkgid
						FROM Package POPUP WITH (NOLOCK) , @stable
						WHERE POPUP.PkgId = spkgid
						AND PkgCode LIKE @param2 + '%'
						)
			BEGIN
				SELECT	PkgId	AS	'ID',
						PkgCode	AS	'CODE',
						PkgName	AS	'DESCRIPTION'
				FROM Package POPUP WITH (NOLOCK), @stable
				WHERE POPUP.PkgId = spkgid
				AND PkgCode LIKE @param2 + '%'
				FOR XML AUTO,ELEMENTS		
			END
			ELSE
			BEGIN
				SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'
			END
--			EXECUTE sp_xml_removedocument @iDoc
--			IF(@@ERROR <> 0 ) RETURN
		END
	END
	ELSE
	BEGIN
		SELECT	PkgId	AS	'ID',
				PkgCode	AS	'CODE',
				PkgName	AS	'DESCRIPTION'
		FROM 	AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId 		WHERE	ApkAgnId = (SELECT AgnId FROM Agent WITH (NOLOCK) 
								WHERE AgnCode = dbo.getSplitedData(@param1,'-',1))
		AND 	PkgCode LIKE @param2+'%'
		AND		PkgCtyCode LIKE	@param4+'%'
		FOR XML AUTO,ELEMENTS
	END
	RETURN

END

IF(@case	=	'AGENTPACKAGE')
BEGIN
	
	IF (@param3 <> '')
	BEGIN 
		SET @ReturnPackageIDList = ''
		DECLARE		@sPrdIdList				VARCHAR	 (8000) ,
					@sPrdIdCurrent			VARCHAR	 (64)
					
		--SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)
		SET @BookedDate = Current_TimeSTAMP
		SELECT 	@agnId 	= dbo.getSplitedData(@param3, ',', 1),
				@prdId 	= dbo.getSplitedData(@param3, ',', 2),
				@coDate = dbo.getSplitedData(@param3, ',', 3),
				@coLoc 	= dbo.getSplitedData(@param3, ',', 4),
				@CiLoc 	= dbo.getSplitedData(@param3, ',', 5),
				@iAd 	= dbo.getSplitedData(@param3, ',', 6),
				@iCh 	= dbo.getSplitedData(@param3, ',', 7),
				@iIn 	= dbo.getSplitedData(@param3, ',', 8),
				@ciDate = dbo.getSplitedData(@param3, ',', 9),
				@RntId 	= dbo.getSplitedData(@param3, ',', 10)

		IF (@coDate = '')	SET @coDate = NULL
		IF (@ciDate = '') 	SET @ciDate = NULL
		IF (@coLoc = '')	SET @coLoc = NULL
		IF (@CiLoc = '')	SET @CiLoc = NULL
		IF (@iAd = '')		SET @iAd = NULL
		IF (@iCh = '')		SET @iCh = NULL
		IF (@iIn = '')		SET @iIn = NULL

		SELECT	@BookedDate = RntBookedWhen
			FROM	dbo.Rental WITH(NOLOCK)
			WHERE	RntId = @RntId
		
		IF @param2<>'' AND (SELECT COUNT(PkgCode) FROM dbo.Package WITH(NOLOCK) 
								WHERE 	Pkgcode like @param2 + '%' 
								AND 	@coDate between PkgTravelFromdate and pkgtraveltodate
								AND		PkgIsActive = 'Active'
								-- KX Changes :RKS
								AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
																		INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode
																		INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
																		INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
																		AND UsrCode = @sUsrCode)
									)
								) = 1
		BEGIN
			
			SELECT 	@param1 = PkgId, 
					@param2  = pkgCode, 
					@param3 = PkgName 
				FROM dbo.Package WITH(NOLOCK)
				WHERE 	PkgCode like @param2 + '%' 
				AND 	@coDate between PkgTravelFromdate and pkgtraveltodate
				AND		PkgIsActive = 'Active'
			EXEC RES_checkPackage
				@PackageID				= @param1 ,
				@AgentID				= @agnId ,
				@RentalID				= NULL ,
				@CheckOutLocationCode	= @coLoc ,
				@CheckInLocationCode	= @CiLoc ,
				@CheckOutDate			= @coDate ,
				@CheckInDate			= @ciDate ,
				@ProductID				= @prdId ,
				@SaleableProductID		= NULL,
				@BookedDate				= @BookedDate,
				@NumberOfAdults			= @iAd ,
				@NumberOfChildren		= @iCh ,
				@NumberOfInfants		= @iIn ,
				@ReturnError			= @ErrStr	OUTPUT,
				@ReturnWarning			= NULL
			
			IF ISNULL(@ErrStr,'')<>''
				SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @ErrStr + '</ErrDescription></Error>'			
			ELSE
				SELECT '<POPUP><ID>' + @param1 + '</ID><CODE>' + @param2 + '</CODE><DESCRIPTION>' + @param3 + '</DESCRIPTION></POPUP>'
			RETURN
		END	--	IF @param2<>'' AND (SELECT COUNT(pkgcode) FROM dbo.Package (NOLOCK) 

		
		DECLARE @prdtypid varchar(64)
		SELECT @prdtypid = PrdTypId
			FROM	dbo.Product WITH(NOLOCK) 
			WHERE	PrdId = @prdId
		SELECT	@sPrdIdList = ISNULL(@sPrdIdList + ',', '') + PrdId 
			FROM	dbo.Product WITH(NOLOCK)
			WHERE	PrdTypId = @prdtypid
			-- KX Changes :RKS
            AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
													INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode
													INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
													INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
													AND UsrCode = @sUsrCode)
				)
		SET @start = 1
		SET @count = CASE
					WHEN @sPrdIdList IS NOT NULL AND @sPrdIdList <> '' 
						THEN dbo.getEntry(@sPrdIdList, ',') + 1
						ELSE 0
				 END
		--	2. START an iterative loop
		WHILE @start <= @count
		BEGIN		
			SET @sPrdIdCurrent = dbo.getSplitedData(@sPrdIdList, ',', @start)
			SET @start = @start + 1
			IF @sPrdIdCurrent <> ''
			BEGIN
				SET @ReturnPackageIDList1 = NULL
				EXECUTE RES_getValidPackages
					@AgentID				= @agnId,
					@BookedDate				= @BookedDate,
					@ProductID				= @sPrdIdCurrent ,		--@prdId,
					@checkOutDate			= @coDate,
					@checkInDate			= @ciDate,
					@CheckOutLocationCode	= @coLoc,
					@CheckInLocationCode	= @CiLoc,
					@NumberOfAdults			= @iAd,
					@NumberOfChildren		= @iCh,
					@NumberOfInfants		= @iIn,
					@VehicleRequestID		= NULL,
					@BlockingRuleIDList		= NULL,
					@ReturnPackageIDList	= @ReturnPackageIDList1	OUTPUT
				SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '<Data><PackageID>', '')
				SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '</PackageID></Data>', '')
				SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '</PackageID><PackageID>', ',')
				IF ISNULL(@ReturnPackageIDList1, '')<>''
				BEGIN
					IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''
						SET @ReturnPackageIDList = @ReturnPackageIDList1
					ELSE
					BEGIN
						SET @startTmp = 1
						SET @countTmp = CASE
									WHEN @ReturnPackageIDList1 IS NOT NULL AND @ReturnPackageIDList1 <> '' 
										THEN dbo.getEntry(@ReturnPackageIDList1, ',') + 1
										ELSE 0
								 END					
						WHILE @startTmp <= @countTmp
						BEGIN
							SET @sPrdIdCurrent = dbo.getSplitedData(@ReturnPackageIDList1, ',', @startTmp)
							SET @startTmp = @startTmp + 1
							IF CHARINDEX(@sPrdIdCurrent, @ReturnPackageIDList) = 0 
								SET @ReturnPackageIDList = 	@ReturnPackageIDList + ',' + @sPrdIdCurrent
						END	--	WHILE @startTmp <= @countTmp
					END	--	IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''
				END	--	IF ISNULL(@ReturnPackageIDList1, '')<>''
			END	-- IF @sPrdIdCurrent <> ''
		END	-- WHILE @start <= @count
		IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''
			SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'
		ELSE
		BEGIN
			SET @start = 1
			SET @count = CASE
						WHEN @ReturnPackageIDList IS NOT NULL AND @ReturnPackageIDList <> '' 
							THEN dbo.getEntry(@ReturnPackageIDList, ',') + 1
							ELSE 0
					 END
			WHILE @start <= @count
			BEGIN
				SET @sPrdIdCurrent = dbo.getSplitedData(@ReturnPackageIDList, ',', @start)
				SET @start = @start + 1
				SELECT	PkgId	AS	'ID',
						PkgCode	AS	'CODE',
						PkgName	AS	'DESCRIPTION'
					FROM Package POPUP WITH (NOLOCK)
					WHERE PkgId = @sPrdIdCurrent
					AND PkgCode LIKE @param2 + '%'
					-- KX Changes :RKS
                    AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
															INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode
															INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
															INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
															AND UsrCode = @sUsrCode)
						)
				FOR XML AUTO,ELEMENTS	
			END	--	WHILE @start <= @count
		END	-- else IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''
	END	--	IF (@param3 <> '')
	ELSE
	BEGIN
		SET @param1 = dbo.getSplitedData(@param1,'-',1)
		SELECT	PkgId	AS	'ID',
				PkgCode	AS	'CODE',
				PkgName	AS	'DESCRIPTION'
		FROM 	AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId
		WHERE	ApkAgnId = (SELECT AgnId FROM dbo.Agent WITH (NOLOCK) 
								WHERE AgnCode = @param1)
		AND 	PkgCode LIKE @param2+'%'
		AND		(PkgCtyCode LIKE @param4+'%' OR ISNULL(@param4,'') = '')
		-- KX Changes :RKS
        AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
												INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode
												INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
												INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
												AND UsrCode = @sUsrCode)
			)
		FOR XML AUTO,ELEMENTS
	END
	RETURN
END


IF(@case	=	'AGENTPACKAGE4BooMgt')
BEGIN
	IF (@param3 <> '')
	BEGIN 
		SET @ReturnPackageIDList = ''
		--SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)
		DECLARE		@cNonRevB4XchgBpdIds		VARCHAR	 (8000) ,
					@sLastVehBpdId 				VARCHAR	 (64) ,
					@sRntId						VARCHAR	 (64) ,
					@sUsrId						VARCHAR	 (64)
		
		SET @BookedDate = Current_TimeSTAMP
		SELECT 	@agnId = dbo.getSplitedData(@param3, ',', 1),
				@prdId = dbo.getSplitedData(@param3, ',', 2),
				@coDate = dbo.getSplitedData(@param3, ',', 3),
				@coLoc = dbo.getSplitedData(@param3, ',', 4),
				@CiLoc = dbo.getSplitedData(@param3, ',', 5),
				@iAd = dbo.getSplitedData(@param3, ',', 6),
				@iCh = dbo.getSplitedData(@param3, ',', 7),
				@iIn = dbo.getSplitedData(@param3, ',', 8),
				@ciDate = dbo.getSplitedData(@param3, ',', 9) ,
				@sRntId = dbo.getSplitedData(@param3, ',', 10) ,
				@sUsrId = dbo.getSplitedData(@param3, ',', 11)

		SET @BookedDate = dbo.GEN_getAdjustedTimeForUser(@sUsrId,@BookedDate)

		SELECT @coDate = RntCkoWhen, @ciDate = RntCkiWhen FROM Rental (NOLOCK) WHERE RntId = @sRntId
		
		Exec RES_GetExchangeBpds
				@sRntId					=	@sRntId ,
				@sBpdIdList				=	NULL ,
				@cNonRevB4XchgBpdIds	=	@cNonRevB4XchgBpdIds OUTPUT,
				@cRevB4XchgBpdIds		=	NULL ,
				@cRntAgrList			=	NULL ,
				@cNonRevB4XchgAmt		=	NULL ,
				@cRevB4XchgAmt			=	NULL ,
				@cXfrmAmt				=	NULL ,
				@cXtoAmt				=	NULL
		
		IF @cNonRevB4XchgBpdIds IS NULL
		BEGIN
			SELECT TOP 1 @prdId = SapPrdId 
			FROM BookedProduct (NOLOCK), SaleableProduct (NOLOCK)
			WHERE BpdRntId = @sRntId AND BpdSapId  = SapId AND BpdPrdIsVehicle = 1 AND BpdIsCurr = 1
			ORDER BY BpdCkoWhen DESC	 
		END
		ELSE
		BEGIN
			SET @sLastVehBpdId  =  dbo.getSplitedData (@cNonRevB4XchgBpdIds, ',' , dbo.getEntry(@cNonRevB4XchgBpdIds, ',') + 1)
			SELECT @prdId = SapPrdId 
			FROM BookedProduct (NOLOCK), SaleableProduct (NOLOCK)
			WHERE BpdSapId = SapId AND BpdId = @sLastVehBpdId			
		END
		
			
		/*select @prdId, @BookedDate, @coDate '@coDate', @ciDate as '@ciDate', @coLoc '@coLoc', @CiLoc '@CiLoc' ,
				@iAd '@iAd', @iCh '@iCh', @iIn '@iIn'*/
		IF (@coDate = '')	SET @coDate = NULL
		IF (@ciDate = '') 	SET @ciDate = NULL
		IF (@coLoc = '')	SET @coLoc = NULL
		IF (@CiLoc = '')	SET @CiLoc = NULL
		IF (@iAd = '')		SET @iAd = NULL
		IF (@iCh = '')		SET @iCh = NULL
		IF (@iIn = '')		SET @iIn = NULL

		
--	MTS 20JUN02 : Added "named parameters" after my changes

--			to RES_getValidPackages() made this call fail.

		EXECUTE RES_getValidPackages
			@AgentID				= @agnId,
			@BookedDate				= @BookedDate,
			@ProductID				= @prdId,
			@checkOutDate			= @coDate,
			@checkInDate			= @ciDate,

			@CheckOutLocationCode	= @coLoc,
			@CheckInLocationCode	= @CiLoc,
			@NumberOfAdults			= @iAd,
			@NumberOfChildren		= @iCh,
			@NumberOfInfants		= @iIn,
			@VehicleRequestID		= NULL,
			@BlockingRuleIDList		= NULL,
			@ReturnPackageIDList	= @ReturnPackageIDList	OUTPUT
--	MTS 06AUG02 : removed
--			@psErrors				= @pkgsErr	OUTPUT

		IF ISNULL(@pkgsErr, '') <> '' AND ISNULL(@ReturnPackageIDList, '') = ''
		BEGIN
			SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @pkgsErr + '</ErrDescription></Error>'			
		END
		ELSE IF ISNULL(@pkgsErr, '') = '' AND ISNULL(@ReturnPackageIDList, '') = ''
		BEGIN
			SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'
		END
		ELSE
		BEGIN
			EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @ReturnPackageIDList
			IF (@@ERROR <> 0 ) RETURN
			IF EXISTS(SELECT pkgid
						FROM Package POPUP WITH (NOLOCK)
						WHERE PkgId IN ( SELECT PackageID FROM
											OPENXML (@iDoc, '/Data/PackageID', 3)
											WITH ( PackageID		VARCHAR(64) 'text()')
										)
						AND PkgCode LIKE @param2 + '%'
						)
			BEGIN
				SELECT	PkgId	AS	'ID',
						PkgCode	AS	'CODE',
						PkgName	AS	'DESCRIPTION'
				FROM Package POPUP WITH (NOLOCK)
				WHERE PkgId IN ( SELECT PackageID FROM
									OPENXML (@iDoc, '/Data/PackageID', 3)
									WITH ( PackageID		VARCHAR(64) 'text()')
								)
				AND PkgCode LIKE @param2 + '%'
				FOR XML AUTO,ELEMENTS		
			END
			ELSE
			BEGIN
				SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'
			END
			EXECUTE sp_xml_removedocument @iDoc
			IF(@@ERROR <> 0 ) RETURN
		END
	END
	ELSE
	BEGIN
		SELECT	PkgId	AS	'ID',
				PkgCode	AS	'CODE',
				PkgName	AS	'DESCRIPTION'
		FROM 	AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId 		WHERE	ApkAgnId = (SELECT AgnId FROM Agent WITH (NOLOCK) 
								WHERE AgnCode = dbo.getSplitedData(@param1,'-',1))
		AND 	PkgCode LIKE @param2+'%'
		AND		PkgCtyCode LIKE	@param4+'%'
		FOR XML AUTO,ELEMENTS
	END
	RETURN
END

 
IF(@case = 'BookedProduct')
BEGIN
	SELECT 
		DISTINCT Product.PrdShortName AS CODE, Product.PrdName AS DESCRIPTION

	FROM 
	        SaleableProduct WITH (NOLOCK) INNER JOIN
                      Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN
                      BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId
	WHERE 	(PrdShortName 	LIKE	@param1+'%'
		OR	 PrdName	LIKE	@param1+'%')
	AND BpdRntId = @param2
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF(@case	=	'PRODUCT')
BEGIN
	IF (SELECT COUNT(PrdId) FROM Product WITH (NOLOCK), dbo.Brand (NOLOCK)
			WHERE	PrdBrdCode = BrdCode AND (PrdShortName = @param1
			OR		PrdName = @param1)
			-- KX Changes :RKS
			AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
										WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))
			AND		PrdIsActive = 1) = 1
		SELECT 	PrdId			AS	'ID',
			 	PrdShortName	AS	'CODE',
			 	PrdName 		AS	'DESCRIPTION'
			FROM Product AS POPUP WITH (NOLOCK), dbo.Brand (NOLOCK)
			WHERE	PrdBrdCode = BrdCode AND (PrdShortName = @param1
			OR		PrdName = @param1)
			-- KX Changes :RKS
			AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
										WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))
			AND		PrdIsActive = 1
			FOR XML AUTO,ELEMENTS	
	ELSE
		SELECT 	PrdId			AS	'ID',
			 	PrdShortName	AS	'CODE',
			 	PrdName 		AS	'DESCRIPTION'
			FROM Product AS POPUP WITH (NOLOCK), dbo.Brand (NOLOCK)
			WHERE 	PrdBrdCode = BrdCode AND (PrdShortName 	LIKE	@param1+'%'
			OR	 	PrdName	LIKE	@param1+'%')
			-- KX Changes :RKS
			AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
										WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))
			AND		PrdIsActive = 1
			FOR XML AUTO,ELEMENTS	
	RETURN
END

-- for issue 429
IF(@case	=	'Product4BookingSearch')
BEGIN
	SELECT  PrdShortName	AS	'CODE',
		 			PrdName 	AS 'DESCRIPTION'
	 FROM 	Product AS POPUP WITH (NOLOCK)
	 WHERE 	(PrdShortName 	LIKE	@param1+'%' OR PrdName	LIKE	@param1+'%')
	 -- KX Changes
	 AND (
		   @sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) INNER JOIN dbo.Company (NOLOCK)
											ON Brand.BrdComCode = Company.ComCode INNER JOIN dbo.UserCompany (NOLOCK)
											ON Company.ComCode = UserCompany.ComCode INNER JOIN dbo.UserInfo (NOLOCK)
											ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode
											)
	)
    AND	PrdIsActive = 1
	FOR XML AUTO,ELEMENTS	
	RETURN
END
-- end for issue 429

IF(@case	=	'PRODUCT4BPD')
BEGIN	
	declare		@dBpdCkoWhen		datetime ,
				@sUvValue			varchar	 (64) ,
				@sRntPkgId			varchar	 (64)

	select 	@dBpdCkoWhen = BpdCkoWhen , 
			@sRntId		 = BpdRntId
	from BookedProduct (nolock) where BpdId = @param3

	select @sRntPkgId = RntPkgId from Rental with (nolock) where RntId = @sRntId

	set @sUvValue = 0
	select @sUvValue = UviValue from dbo.UniversalInfo (nolock) where UviKey  = @param2 + 'MinCheckDays'
	

	SELECT DISTINCT
		PrdId			AS	'ID',
		 PrdShortName	AS	'CODE',
		'' + BrdName		AS 'BRAND', 
		 PrdName 			AS	'DESCRIPTION' --,
--		'' + CAST(SapSuffix AS VARCHAR) AS	'Suffix'
	FROM
		Product AS POPUP WITH (NOLOCK) INNER JOIN
        Brand WITH (NOLOCK) ON POPUP.PrdBrdCode = Brand.BrdCode 
	WHERE	exists (select sapid 
					from saleableproduct WITH (NOLOCK), 
					PackageProduct with (nolock) ,
					Package with (nolock)

					where pplPkgId = @sRntPkgId and sapprdid = popup.prdid 
					and SapStatus = 'Active' 
					AND SaleableProduct.SapCtyCode = @param2 and SapIsBase=1
					and PplpkgId  = PkgId
					--and SapId = pplSapId
					and (PkgTravelToDate + cast(@sUvValue as int))>=@dBpdCkoWhen
					and cast(convert(varchar(10),current_timestamp,103) as datetime)<=(PkgTravelToDate + cast(@sUvValue as int))
					)
	AND 	(PrdShortName 	LIKE	@param1+'%'
			OR	 PrdName	LIKE	@param1+'%')
	AND	PrdIsActive = 1
	AND 	PrdIsVehicle = @param4			
	-- KX Changes :RKS
	AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
										  INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode
										  INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
										  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId
										  AND UsrCode = @sUsrCode
										 )
	)
	FOR XML AUTO, ELEMENTS	
	RETURN
END

IF(@case	=	'PRODUCTVEHICLE')
BEGIN
	SELECT PrdId		AS	'Id',
		 PrdShortName	AS	'ShortName',
		 PrdName 	AS	'Name'
		FROM Product AS PRODUCT WITH (NOLOCK)
		WHERE 	(PrdShortName 	LIKE	@param1+'%'
		OR	 PrdName	LIKE	@param1+'%')
		AND	PrdIsActive = 1
		AND	PrdIsVehicle = @param2
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF (@case = 'BookedProductType')
BEGIN
SELECT 
	DISTINCT 
	Type.TypCode		AS	'CODE', 
	Type.TypDesc		AS	'DESCRIPTION'
FROM         
	Product WITH (NOLOCK) INNER JOIN
             Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId
WHERE
	(Type.TypCode 	LIKE	@param1+'%'
	OR	 Type.TypDesc	 LIKE	@param1+'%')
	AND
	PrdId IN (
		SELECT 
			DISTINCT 
			Product.PrdId
		FROM 
	 		SaleableProduct WITH (NOLOCK) INNER JOIN
     				Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN
     				BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId	
		-- KX Changes :RKS
		WHERE BpdRntId = @param2
	)
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF (@case = 'BookedProductClass')
BEGIN
	SELECT  
		DISTINCT Class.ClaCode 	AS	'CODE', 
		Class.ClaDesc 			AS 	'DESCRIPTION'
	FROM         
		Product WITH (NOLOCK) INNER JOIN
             		Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN
   		 Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID
	WHERE
		(Class.ClaCode 	LIKE	@param1+'%'
		OR	 Class.ClaDesc	LIKE	@param1+'%')
		AND
		PrdId IN (
			SELECT 
				DISTINCT 
				Product.PrdId
			FROM 
		 		SaleableProduct WITH (NOLOCK) 
				INNER JOIN Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId 
				INNER JOIN BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId
			-- KX Changes :RKS
			WHERE BpdRntId = @param2
		)
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF @case = 'PRODUCT4EXCHANGE'
BEGIN
	SELECT DISTINCT 
				PrdId 			AS	'ID',
				PrdShortName 	AS 	'CODE',  
				PrdName			AS	'DESCRIPTION'
	FROM	Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)
	WHERE 	SapPrdId = PrdId 
	AND 	PrdTypId =@param2
	AND 	SapStatus = 'Active'
	AND 	PrdIsVehicle = 1
	AND 	PrdIsActive = 1
	AND 	SapCtyCode = @param3
	AND 	((SapId IN (SELECT PplSapId 
							FROM 	PackageProduct WITH(NOLOCK)
							WHERE	PplPkgId  = (SELECT RntPkgId FROM Rental WITH(NOLOCK) WHERE RntId =@param4)
						))
			 OR SapIsBase = 1)
	AND		(PrdShortName	LIKE	@param1+'%'
			OR	 PrdShortName 	LIKE	@param1+'%')
	-- KX Changes :RKS
	AND (
			@sUsrCode = '' OR PrdBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
											 INNER JOIN dbo.UserCompany (NOLOCK) ON Brand.BrdComCode = UserCompany.ComCode
											 INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
											 AND UserInfo.UsrCode =  @sUsrCode
											)
		)
		FOR XML AUTO, ELEMENTS
END

IF @case = 'PRDTYPE4EXCH'
BEGIN
	SELECT 
			TypId		AS	'CODE',
			TypDesc 	AS	'DESCRIPTION',
			TypOrder	AS	'ORDER1',
			ClaOrder	AS	'ORDER2'
		FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)
		WHERE TypClaId = ClaId	
		and ClaIsVehicle = 1
		ORDER BY ClaOrder,TypOrder
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF @case = 'PRDTYPE4LATEFEE'
BEGIN
	SELECT 
		DISTINCT 	
				TypId		AS	'CODE',
				TypDesc 	AS	'DESCRIPTION',
				TypOrder	AS	'ORDER1',
				ClaOrder	AS	'ORDER2'
		FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)
		WHERE TypClaId = ClaId	
		--and ClaIsVehicle = 1
		ORDER BY ClaOrder,TypOrder
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'CLASSTYPE')
BEGIN
/*
	SELECT DISTINCT 	TypCode	AS	'CODE',
				TypDesc 	AS	'DESCRIPTION'
		FROM Type AS POPUP
		where 	TypClaId	LIKE	@param1+'%'

		AND	(TypCode 	LIKE	@param2+'%'
		OR	TypDesc	LIKE	@param2+'%')
		FOR XML AUTO,ELEMENTS	
*/
	SELECT DISTINCT 	TypCode	AS	'CODE',
				TypDesc 	AS	'DESCRIPTION',
				TypOrder	AS	'ORDER1',
				ClaOrder	AS	'ORDER2'
		FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)
		WHERE TypClaId = ClaId	
		AND	TypClaId	LIKE	@param1+'%'
		AND	(TypCode 	LIKE	@param2+'%'
		OR	TypDesc	LIKE	@param2+'%')
		ORDER BY ClaOrder,TypOrder
		FOR XML AUTO,ELEMENTS	
	RETURN
END

IF(@case	=	'TYPE')
BEGIN
	SELECT DISTINCT 	TypCode	AS	'CODE',
				TypDesc 	AS	'DESCRIPTION', 				TypOrder	AS	'ORDER1',
				ClaOrder	AS	'ORDER2'
		FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)
		WHERE 	TypClaId = ClaId 
		AND	(TypCode 	LIKE	@param1+'%'
		OR	TypDesc	LIKE	@param1+'%')
		AND	TypIsActive	=	1
		ORDER BY ClaOrder,TypOrder
		FOR XML AUTO,ELEMENTS

	RETURN
END

IF(@case	=	'CODCODE')
BEGIN
	EXEC sp_get_CodeCodeType @param1,@param2
	RETURN
END

IF(@case	=	'OPLOGBRANCH')
BEGIN
	Declare @sQuery varchar(400)
	SET @sQuery=''
	SELECT DISTINCT 	LocationCode 	AS CODE,
				LocationName 	AS DESCRIPTION
		FROM aimsprod.dbo.AI_Location AS POPUP WITH (NOLOCK)
		WHERE LocationCode	LIKE	@param1+'%'
		OR	LocationName	LIKE	@param1+'%'
		ORDER BY 	DESCRIPTION 
		FOR XML AUTO,ELEMENTS

/*
	SELECT DISTINCT 	LivLocCode 	AS 	'CODE'	,
		 		''+LocName	AS 	'DESCRIPTION'	
		
		 FROM LocationInventory AS POPUP, Location

		WHERE 	LivLocCode	=	LocCode

		AND		(LivLocCode 	LIKE 	@param1+'%'
		OR	 	 LocName		LIKE 	@param1+'%')
		AND		LocIsActive		=	1
		ORDER BY 	'DESCRIPTION' 
		FOR XML AUTO,ELEMENTS
*/
	RETURN
END

IF(@case	=	'OPLOGFLEETREPBRANCH')
BEGIN
	SELECT Distinct 
			LocationCode 	AS 'CODE',
			LocationName 	AS 'DESCRIPTION'
		FROM  	aimsprod.dbo.AI_Location AS POPUP WITH (NOLOCK)
		WHERE (LocationCode 	LIKE 	@param1 + '%'
		OR	  LocationName	LIKE 	@param1 + '%')
		-- KX Changes :RKS
		AND (@sUsrCode = '' OR LocationCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
											  INNER JOIN  dbo.Company (NOLOCK) ON LocComCode = Company.ComCode
											  INNER JOIN dbo.UserCompany (NOLOCK)ON UserCompany.ComCode = Company.ComCode
											  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId
											  AND UsrCode = @sUsrCode))
		ORDER BY 2 
		FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'OPLOGFLEETREPPROVIDER')
BEGIN
	SELECT 	Distinct 	
			LTRIM(RTRIM(RepairerCode))  AS 'CODE',
			LTRIM(RTRIM(RepairerName)) 	AS 'DESCRIPTION'
		FROM 	aimsprod.dbo.AI_Repairer AS POPUP WITH (NOLOCK)
		WHERE (RepairerCode LIKE 	@param1 + '%'
		OR	  RepairerName	LIKE 	@param1 + '%')
		ORDER BY 2 
		FOR XML AUTO,ELEMENTS
	RETURN
END 

IF(@case	=	'OPLOGFLEETREPPROVIDERID')
BEGIN
	SELECT 	Distinct 	
			LTRIM(RTRIM(RepairerId))  	AS 'ID',
			LTRIM(RTRIM(RepairerCode))  AS 'CODE',
			LTRIM(RTRIM(RepairerName)) 	AS 'DESCRIPTION'
	FROM 	AIMSProd.dbo.AI_Repairer AS POPUP WITH (NOLOCK)
	WHERE RepairerCode 	 = @param1
	ORDER BY 2 
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'LOCATION')
BEGIN
	IF EXISTS(select loccode from dbo.location (nolock) where loccode = @param1 and LocisActive = 1)
	BEGIN
	
		SELECT	LocCode  	AS 'CODE',
				LocName 	AS 'DESCRIPTION'
		FROM Location AS POPUP WITH (NOLOCK)
		WHERE	(LocCode 	= @param1)
		AND		LocIsActive	 = 	1
		-- KX Changes :RKS
		AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
										  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
											)
			 )
		ORDER BY LocName
		FOR XML AUTO,ELEMENTS
		RETURN
	END
	ELSE BEGIN
		SELECT	LocCode  	AS 'CODE',
				LocName 	AS 'DESCRIPTION'
		FROM Location AS POPUP WITH (NOLOCK)
		WHERE	(LocCode 	LIKE	@param1 + '%' 
			OR	 LocName	LIKE	@param1 + '%' )
		AND		LocIsActive	 = 	1
		-- KX Changes :RKS
		AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK) 
										  WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)
																INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
																INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)
											)
			 )
		ORDER BY LocName
		FOR XML AUTO,ELEMENTS
		RETURN
	END
END

IF(@case	=	'LOCATIONFORSTOCK')
BEGIN
	DECLARE @sCty 	varchar(64)
	SELECT 
		@sCty = UsrCtyCode 
	FROM 
		UserInfo WITH (NOLOCK) 
	WHERE 
		UsrCode = @param2

	SELECT     
		LocCode				AS CODE, 
		LocName				AS DESCRIPTION
	FROM       
		Location AS POPUP WITH (NOLOCK)  
	WHERE     
		(LocIsActive = 1) AND 
		(LocCode LIKE @param1 + '%' OR @param1='' ) AND 
		LocCode IN( 
				SELECT     
					Location.LocCode
				FROM         
					Location WITH (NOLOCK) INNER JOIN
                      			TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
				WHERE     
					TownCity.TctCtyCode = @sCty AND Location.LocCode IN
					(
						SELECT
						LocationCode
						FROM
						aimsprod.DBO.AI_Location
					)
			)
	-- KX Changes: RKS
	AND (@param2 = '' OR LocComCode IN (SELECT UserCompany.ComCode FROM dbo.UserCompany (NOLOCK)
										INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
										AND UsrCode = @param2)
		)
	ORDER BY LocName
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'LOCATIONFORCOUNTRY')

BEGIN
	IF EXISTS(select locCode from dbo.location where Loccode = @param2 and LocisActive =1
			  AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)
												WHERE Loccode = @param2 AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode 
												AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode
												)
					)
			)
	begin
		SELECT	LocCode  	AS 'CODE',
				LocName 	AS 'DESCRIPTION'
			FROM Location AS POPUP WITH (NOLOCK)
			WHERE	(ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
			AND	LocCode LIKE	@param2 
			AND	LocIsActive	 = 	1
			AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)
												WHERE Loccode = @param2 AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode 
												AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode)
					)
	--	ORDER BY 2

		FOR XML AUTO,ELEMENTS
		RETURN
	END
	ELSE begin
		SELECT	LocCode  	AS 'CODE',
				LocName 	AS 'DESCRIPTION'
			FROM Location AS POPUP WITH (NOLOCK)
			WHERE	(ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
			AND	(LocCode 	LIKE	@param2 + '%' 
			OR	 LocName	LIKE	@param2 + '%')
			AND	LocIsActive	 = 	1
			AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)
												WHERE (LocCode 	LIKE	@param2 + '%' 
														OR	 LocName	LIKE	@param2 + '%') AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode 
												AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode)
					)
	--	ORDER BY 2
		FOR XML AUTO,ELEMENTS
		RETURN
	end
END

IF(@case	=	'LOCATIONFORCOUNTRY_NEW')
BEGIN
	SELECT	LocCode  	AS 'CODE',
			LocName 	AS 'DESCRIPTION'
		FROM Location AS POPUP WITH (NOLOCK)
		WHERE	(ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))
		AND	(LocCode LIKE @param2 + '%' OR LocName LIKE	@param2 + '%')
		AND	LocIsActive	 = 	1
		AND	DBO.GetCodCode(LocCodTypId) <> 'CC'
		AND	DBO.GetCodCode(LocCodTypId) <> 'HQ'
	ORDER BY 2
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case	=	'OPLOGGETFLEETMODEL')
BEGIN
	
	--SET @sFleetCompanyId = ''
	IF @sUsrCode<>''
		SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode
	
	SELECT	LTRIM(RTRIM(FleetModelCode))  		AS 'CODE',
			LTRIM(RTRIM(FleetModelDescription))	AS 'DESCRIPTION'
		 FROM AIMSProd.dbo.AI_FleetModel AS POPUP WITH (NOLOCK)
		WHERE	(FleetModelCode 	LIKE	@param1 + '%'
		OR	FleetModelDescription	LIKE	@param1 + '%')
		-- KX Changes :RKS
		AND (@sUsrCode='' OR POPUP.FleetModelId IN (SELECT distinct AIF.FleetModelId FROM AIMSProd.dbo.FleetAsset Fleet (NOLOCK), aimsprod.dbo.ai_fleetasset AIF (nolock)
							 WHERE CHARINDEX(CONVERT(VARCHAR,fleet.CompanyId),'65,66')>0
							 and   AIF.fleetassetid = fleet.fleetassetid ))
	ORDER BY 2
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF(@case = 'PRODUCTCOUNTRY')
BEGIN
	IF EXISTS (SELECT CtyCode FROM Country WITH (NOLOCK)
			WHERE 	CtyCode    = @param1
			AND		CtyIsActive = 1)
	BEGIN
		SELECT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country WITH (NOLOCK)
		WHERE 	CtyCode    	= 	@param1
		AND 	CtyHasProducts	=	1
		AND		CtyIsActive 	= 	1
		FOR XML AUTO,ELEMENTS	
	END
	ELSE
	BEGIN
		SELECT 	CtyCode	AS	'CODE',
				CtyName	AS	'DESCRIPTION'
		FROM Country WITH (NOLOCK)	
		WHERE 	(CtyCode LIKE @param1+'%'
		OR		CtyName LIKE @param1+'%')
		AND 		CtyHasProducts		=	1
		AND		CtyIsActive 		= 1
		FOR XML AUTO,ELEMENTS	
	END

	RETURN
END

IF(@case = 'POPUPSALEABLEPRODUCT')
BEGIN
	SELECT 	PrdName						AS 'CODE',
		PrdShortName +  ' - ' + CAST(SapSuffix AS varchar(4))	AS 'DESCRIPTION'
	FROM  Product AS POPUP WITH (NOLOCK), Saleableproduct WITH (NOLOCK)
	WHERE POPUP.prdid 			= 	Saleableproduct.SapPrdId
	AND	(PrdShortName			LIKE	@param1 + '%' 
	OR	 PrdName			LIKE	@param1 + '%') /*
	AND	Saleableproduct.SapId		IN 	(SELECT PatSapId FROM ProductAttribute)	*/
	-- KX Changes : RKS
	AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
										 WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
										)
		)
	FOR XML AUTO,ELEMENTS
	RETURN
END


IF(@case = 'POPUPSALEABLEPRODUCTID')
BEGIN
	SELECT 	''+SapId			AS 'ID',
		--PrdName			AS 'CODE',
		PrdShortName			AS 'CODE',
		CAST(SapSuffix AS varchar(4))	AS 'DESCRIPTION'
	FROM  Product AS POPUP WITH (NOLOCK), Saleableproduct WITH (NOLOCK)
	WHERE prdid 			= 	SapPrdId
	AND	(PrdShortName		LIKE	@param1 + '%' 
	OR	 PrdName		LIKE	@param1 + '%')
	AND	SapSuffix >= @param2
	AND	PrdShortName >= @param3 		
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF @case  = 'BOOKINGAGN'
BEGIN
/*
	SELECT DISTINCT 
		Agent.AgnCode		AS	CODE, 
		Agent.AgnName	AS	'DESCRIPTION'
	FROM	Booking WITH (NOLOCK) INNER JOIN Agent WITH (NOLOCK) ON Booking.BooAgnId = Agent.AgnId
	AND		(Agent.AgnCode 	LIKE	@param1 + '%'	
		OR	Agent.AgnName	LIKE    @param1 + '%'	)
	FOR XML AUTO, ELEMENTS
*/
	SELECT DISTINCT 
		Agent.AgnCode		AS	CODE, 
		Agent.AgnName	AS	'DESCRIPTION'
	FROM	Agent WITH (NOLOCK) 
	WHERE	(Agent.AgnCode 	LIKE	@param1 + '%'	
		OR	Agent.AgnName	LIKE    @param1 + '%'	)
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF @case  = 'PRD4CTY'
BEGIN
	SELECT 
		PrdId														AS 'ID', 	-- Added : RKS 29/07
		PrdShortName /*+  ' - ' + CAST(SapSuffix AS varchar(4))*/	AS 'CODE',
		PrdName														AS 'DESCRIPTION'

FROM
	Product WITH (NOLOCK) 
WHERE
	(PrdShortName			LIKE	@param1 + '%' 

	OR	 
	PrdName			LIKE	 @param1 + '%') 
	AND
	PrdIsVehicle = 0
	AND
	PrdIsActive =1
	AND
	 EXISTS(SELECT	 sapid
				FROM
					 SaleableProduct WITH (NOLOCK) 
   				WHERE
						SapPrdId = PrdId 
						AND
						SapStatus = 'ACTIVE'
   						AND  (EXISTS(SELECT	 PplSapId
										FROM
											 PackageProduct WITH (NOLOCK) 
										WHERE
												PplSapId = SapId 
												AND 
												PplPkgId = (SELECT	RntPkgId
																FROM
																	Rental WITH (NOLOCK) 
																WHERE
																	RntId = @param3 )								) 
   						OR  (sapIsBase = 1 AND SapCtyCode = @param2)
			)
 		)
	-- KX Changes :RKS
	AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
										INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode
										INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
										INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode)
	)
	ORDER BY PrdShortName
	FOR XML AUTO,ELEMENTS
	RETURN
END

IF @case  = 'GST'
BEGIN
	SELECT 
		GstCode	AS	'CODE', 
		GstDesc	AS	'DESCRIPTION'
	FROM Gst WITH (NOLOCK)
	WHERE 	GstCode LIKE @param1 + '%'
	OR	GstDesc LIKE @param1 + '%'
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF @case  = 'CODE'
BEGIN
	SELECT 	CodId	AS	'ID',
		CodCode	AS	'CODE', 
		CodDesc	AS	'DESCRIPTION'
	FROM Code POPUP WITH (NOLOCK)
	WHERE 	CodCdtNum = @param2
	AND	CodIsActive = 1
	AND	(CodCode LIKE @param1 + '%'
	OR	CodDesc LIKE @param1 + '%')
	ORDER BY CodDesc
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF @case  = 'IncentiveScheme'
BEGIN
	DECLARE @sUsrCtyCode VARCHAR(64)
    --SELECT @sUsrCtyCode =  FROM UserInfo WHERE UsrCode = @param2
	SELECT	@sUsrCtyCode = TctCtyCode
	 FROM   Rental		WITH(NOLOCK),
            Location	WITH(NOLOCK),
			TownCity	WITH(NOLOCK)
	 WHERE	RntCkoLocCode = LocCode 
	 AND	LocTctCode = TctCode
	 AND	RntId = @param2

	SELECT 	IscCode AS	'CODE', 
		IscDesc AS	'DESCRIPTION'
	FROM IncentiveScheme POPUP WITH (NOLOCK)
	WHERE 	
	IscIsActive = 1
	AND	(IscCode LIKE @param1 + '%' OR IscDesc LIKE @param1 + '%')
    AND (IscCtyCode = @sUsrCtyCode OR ISNULL(IscCtyCode,'')='')
	ORDER BY IscCode
	FOR XML AUTO, ELEMENTS
	RETURN
END


IF(@case='GetVehicleData')
BEGIN
	IF EXISTS(SELECT POPUP.prdid
				FROM Product POPUP WITH (NOLOCK), SaleableProduct WITH (NOLOCK)
				WHERE 	
					PrdIsActive = 1
					AND 	SaleableProduct.SapCtyCode = @param2
					AND PrdShortName = @param1
			)
		SELECT DISTINCT
			PrdId			AS	'ID',
		 	PrdShortName + ' - ' + BrdName +  ' - '  +  PrdName 			AS	'DESCRIPTION',
			PrdIsVehicle as PrdIsVehicle
	FROM
		Product AS POPUP WITH (NOLOCK) INNER JOIN
            Brand WITH (NOLOCK) ON POPUP.PrdBrdCode = Brand.BrdCode INNER JOIN
            SaleableProduct WITH (NOLOCK) ON POPUP.PrdId = SaleableProduct.SapPrdId
	WHERE
		 	PrdIsActive = 1
			AND 	SaleableProduct.SapCtyCode = @param2
			AND PrdShortName = @param1
			-- KX Changes :RKS
			AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
												  INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode
												  INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode
												  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId
												  AND UsrCode = @sUsrCode
												 )
			)
	FOR XML AUTO, ELEMENTS	
	RETURN
END

IF(@case='UNIVERSALINFO')
BEGIN
	IF EXISTS(SELECT uvikey FROM dbo.UniversalInfo WITH (NOLOCK) WHERE	UviKey = @param1)
		SELECT 	UviValue 	'VAL'
			FROM	UniversalInfo WITH (NOLOCK)
			WHERE	UviKey = @param1
			FOR XML AUTO, ELEMENTS
	ELSE
		SELECT 	TOP 1 'UviValueNot Set' 	'VAL'
			FROM	UniversalInfo WITH (NOLOCK)
			FOR XML AUTO, ELEMENTS		
	RETURN
END

IF(@case='GETUVI')
BEGIN
	SELECT dbo.getUviValue(UsrCtyCode + @param2) FROM UserInfo WITH (NOLOCK) WHERE USrCode = @param1
END

IF(@case='tmpreport')
BEGIN
	EXEC Tmp_Report @param1
	RETURN
END

IF(@case = 'tmpRMreport')
BEGIN
	SET @sFleetCompanyId = ''
	IF @sUsrCode<>''
		SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode

	IF @param1 = ''
		SET @param1 = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)

	IF @param2 = 'LE' AND @param1 <> ''
		SELECT 	'' + FA.UnitNumber AS 'A', 
				''+RegistrationNumber AS 'B', 
				''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',
				''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',
				''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',
				''+ISNULL(StartOdometer, '')  AS 'F',
				''+ISNULL(EndOdometer, '')  AS 'G',
				''+ISNULL(OtherRepairer, '')  AS 'H',
				''+ISNULL(ServiceProvider, '') AS 'I',
				''+ISNULL(ScheduledService, '') AS 'J'
		FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
		WHERE 	FR.FleetAssetId = FA.FleetAssetId
		AND		FM.FleetModelId = FA.FleetModelId
		AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) <= CAST(@param1 AS datetime)
		AND		Completed = 0
		AND		FA.LastLocation = FL.LocationId
		AND		(@param3 = '' OR FL.LocationCode = @param3)
		-- KX Changes :RKS
		AND		(@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)
		FOR XML AUTO
	ELSE IF @param2 = 'GE'  AND @param1 <> ''
		SELECT 	'' + FA.UnitNumber AS 'A', 
				''+RegistrationNumber AS 'B', 
				''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',
				''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',
				''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',
				''+ISNULL(StartOdometer, '')  AS 'F',
				''+ISNULL(EndOdometer, '')  AS 'G',
				''+ISNULL(OtherRepairer, '')  AS 'H',
				''+ISNULL(ServiceProvider, '') AS 'I',
				''+ISNULL(ScheduledService, '') AS 'J'
		FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
		WHERE 	FR.FleetAssetId = FA.FleetAssetId
		AND		FM.FleetModelId = FA.FleetModelId
		AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) >= CAST(@param1 AS datetime)
		AND		Completed = 0
		AND		FA.LastLocation = FL.LocationId
		AND		(@param3 = '' OR FL.LocationCode = @param3)
		-- KX Changes :RKS
		AND		(@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)
		FOR XML AUTO
	ELSE IF  @param1 <> ''
		SELECT 	'' + FA.UnitNumber AS 'A', 
				''+RegistrationNumber AS 'B', 
				''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',
				''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',
				''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',
				''+ISNULL(StartOdometer, '')  AS 'F',
				''+ISNULL(EndOdometer, '')  AS 'G',
				''+ISNULL(OtherRepairer, '')  AS 'H',
				''+ISNULL(ServiceProvider, '') AS 'I',
				''+ISNULL(ScheduledService, '') AS 'J'
		FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
		WHERE 	FR.FleetAssetId = FA.FleetAssetId
		AND		FM.FleetModelId = FA.FleetModelId
		AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) = CAST(@param1 AS datetime)
		AND		Completed = 0
		AND		FA.LastLocation = FL.LocationId
		AND		(@param3 = '' OR FL.LocationCode = @param3)
		-- KX Changes :RKS
		AND		(@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)
		FOR XML AUTO
	RETURN	

END

IF(@case = 'COREPORT')
BEGIN
	IF @param1 = ''
		SET @param1 = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)
	IF @param3 IS NULL OR @param3 = ''
		SET @param3 = 0
	SELECT	''+ExternalRef	AS 'a',		 
			''+LTRIM(RTRIM(FleetModelCode)) + ' - ' + LTRIM(RTRIM(FleetModelDescription)) 	AS 'b',	
			''+UnitNumber	AS 'c',
			''+RegistrationNumber	AS 'd',
			''+StartOdometer	AS 'e',
			''+/*LTRIM(RTRIM(LocationCode)) + ' - ' +*/ LTRIM(RTRIM(LocationName)) AS 'f',
			''+(SELECT BooLastName FROM Booking WITH(NOLOCK) WHERE BooNum = dbo.getSplitedData(ExternalRef, '/', 1)) AS 'g',
			''+(SELECT BooId FROM Booking WITH(NOLOCK) 
					WHERE	BooNum = dbo.getSplitedData(ExternalRef, '/', 1))	AS	BooId,
			''+(SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK) 
					WHERE	BooId = RntBooId
					AND		BooNum = dbo.getSplitedData(ExternalRef, '/', 1)
					AND		RntNum = dbo.getSplitedData(dbo.getSplitedData(ExternalRef, '/', 2), 'x', 1))	AS	RntId

	FROM	AIMSPROD.dbo.FleetActivity FAT WITH(NOLOCK), AIMSPROD.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetModel FM WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
	WHERE	FAT.FleetAssetId = FA.FleetAssetId
	AND		FA.FleetModelId = FM.FleetModelId
	AND		FAT.StartLocation = FL.LocationId
	AND		FAT.ActivityType = 'Rental'
	--AND		FAT.Completed = @param3
	AND		CAST(CONVERT(VARCHAR(10), StartDateTime, 103) AS DATETIME) = CAST(@param1 AS DATETIME)
	AND		(@param2 = '' OR FL.LocationCode = @param2)
	ORDER BY FleetModelCode, UnitNumber
	FOR XML AUTO	
END

IF(@case = 'BONDREPORT')
BEGIN
	SET @param3 = @param3 + ' 23:59:59'
	IF EXISTS(	SELECt	RptId
					FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)
							-- KX Changes :RKS
							, Package (NOLOCK)
							
					WHERE	PmtBooId  = BooId
					AND		RntBooId  = BooId
					AND		RptType = 'B'
					AND		PmtId 	= RptPmtId
					AND		RntId	= RptRntId
					AND 	dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1
					AND 	PmtDateTime BETWEEN @param2 AND @param3
					AND 	PmtPtmId = PtmId 
					AND 	(@param4 = '1' or PtmName NOT LIKE '%imprint%')
					-- KX Changes :RKS 
					AND		RntPkgId = PkgId
					AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
														INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
														INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
														AND UsrCode = @sUsrCode)
						)
				)				
	BEGIN
		SELECt	'<tr bgcolor="white"><td>' + 
				CONVERT(VARCHAR(12), BooNum+'/'+RntNum)  	+ '</td>' + '<td>' + 
				CONVERT(varchar(25), Ptmname) 				+ '</td>' + '<td>' + 
				CONVERT(VARCHAR(10), RntCkoWhen, 103) 		+ '</td>' + '<td>' + 
				CONVERT(VARCHAR(10), RntCkiWhen, 103) 		+ '</td>' + '<td align="right">' + 
				CAST(ISNULL(RptLocalCurrAmt,0) AS VARCHAR(10))		+ '</td>' + '<td align="center">' + 
				''+RntStatus 								+ '</td></tr>'
		FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)
				-- KX Changes :RKS
				, Package (NOLOCK)
		WHERE	PmtBooId  = BooId
		AND		RntBooId  = BooId
		AND	  	RptType = 'B'
		AND		PmtId 	= RptPmtId
		AND		RntId	= RptRntId
		AND 	dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1
		AND 	PmtDateTime BETWEEN @param2 AND @param3
		AND 	PmtPtmId = PtmId 
		AND 	(@param4 = '1' or PtmName NOT LIKE '%imprint%') 
		-- KX Changes :RKS 
		AND		RntPkgId = PkgId
		AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
											INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode)
			)
		ORDER BY Rntckowhen, BooNum+'/'+RntNum, PmtDateTime
	
		SELECT	'<tr bgcolor="lightblue"><td COLSPAN="4" align="right"><b>Total Gross Amount</b></td>' + 
				'<td align="right"><b>' + CAST(SUM(ISNULL(RptLocalCurrAmt, 0)) AS VARCHAR(12))	+ '</b></td><td></td></tr>'
		FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)
				-- KX Changes :RKS
				, Package (NOLOCK)
		WHERE	PmtBooId  = BooId
		AND		RntBooId  = BooId
		AND	  	RptType = 'B'
		AND		PmtId 	= RptPmtId
		AND		RntId	= RptRntId
		AND 	dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1
		AND 	PmtDateTime BETWEEN @param2 AND @param3
		AND 	PmtPtmId = PtmId 
		AND 	(@param4 = '1' or PtmName NOT LIKE '%imprint%') 
		-- KX Changes :RKS 
		AND		RntPkgId = PkgId
		AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
											INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
											INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
											AND UsrCode = @sUsrCode)
			)
	END
	ELSE
		SELECT	'<tr bgcolor="white"><td>&#160;</td><td></td><td></td><td></td><td align="right"></td><td align="center"></td></tr>'
END -- IF(@case = 'BONDREPORT')

IF(@case = 'BONDREPORT1')
BEGIN
	SET @param3 = @param3 + ' 23:59:59'
	DECLARE @TT TABLE (	BooNum 	varchar(12),
						PmtMet	varchar(48),
						CkoDate	varchar(10),
						CkiDate	varchar(10),
						Amount	varchar(12),
						Status	varchar(12))
	INSERT INTO @TT
		SELECT	CONVERT(VARCHAR(12), BooNum+'/'+RntNum) 	AS BkgNo,
				CONVERT(VARCHAR(25),Ptmname)				AS Prod, 
				CONVERT(VARCHAR(10), RntCkoWhen, 103) 		As Cko, 
				CONVERT(VARCHAR(10), RntCkiWhen, 103) 		AS Cki, 
				CAST(RptLocalCurrAmt AS VARCHAR)			AS GrossAmt,
				''+RntStatus 								AS Status
			FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), 
					Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK), Location WITH(NOLOCK), 
					TownCity WITH(NOLOCK)
					-- KX Changes :RKS
					, Package (NOLOCK)
			WHERE	Pmtbooid  = Booid
			AND		Rntbooid  = Booid
			AND		RntCkoLocCode = LocCode
			AND		LocTctCode = TctCode
			AND		TctCtyCode = @param1
			AND		RptType = 'B'
			AND		Pmtid 	= RptPmtId
			AND		RntId		= RptRntId
			AND		PmtDateTime BETWEEN @param2 AND @param3
			AND 	PmtPtmId = PtmId 
			AND 	(@param4 = '1' or PtmName NOT LIKE '%Imprint%') 
			-- KX Changes :RKS 
			AND		RntPkgId = PkgId
			AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
												INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
												INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
												AND UsrCode = @sUsrCode)
				)
			ORDER BY RntCkoWhen, BkgNo, PmtDateTime
	
	IF EXISTS(SELECT TOP 1 BooNum FROM @TT)
	BEGIN

		INSERT INTO @TT(BooNum, PmtMet, CkoDate, CkiDate, Amount, Status)
			SELECT BooNum, 'TOTAL', '', '', CAST(sum(cast(Amount as money)) AS VARCHAR), '' from @TT	
			GROUP BY BooNum

		DELETE FROM @TT
			WHERE BooNum IN (SELECT BooNum FROM @TT WHERE CAST(Amount AS MONEY) = 0)

		SELECT	CASE WHEN CkoDate <> '' 
					THEN
						'<tr bgcolor="white"><td>' + 
						BooNum  	+ '</td>' + '<td>' + 
						PmtMet		+ '</td>' + '<td>' + 
						CkoDate		+ '</td>' + '<td>' + 
						CkiDate		+ '</td>' + '<td align="right">' + 
						CAST(ISNULL(Amount,0) AS VARCHAR(10))		+ '</td>' + '<td align="center">' + 
						Status 								+ '</td></tr>'
					ELSE
						'<tr bgcolor="lightblue"><td></td><td></td><td></td>' + '<td align="right"><b>Sub Total</b></td>' + '<td align="right"><b>' + 
						CAST(ISNULL(Amount,0) AS VARCHAR(10))		+ '</b></td>' + '<td align="center"></td></tr>'
				END
			FROM	@TT AS Bond
			ORDER BY	BooNum, CkoDate DESC

	
			SELECT	'<tr bgcolor="lightblue"><td COLSPAN="4" align="right"><b>Total Gross Amount</b></td>' + 
					'<td align="right"><b>' + CAST(SUM(ISNULL(RptLocalCurrAmt, 0)) AS VARCHAR(12))	+ '</b></td><td></td></tr>'
			FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)
			-- KX Changes :RKS
			, Package (NOLOCK)
			WHERE	PmtBooId  = BooId
			AND		RntBooId  = BooId
			AND	  	RptType = 'B'
			AND		PmtId 	= RptPmtId
			AND		RntId	= RptRntId
			AND 	dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1
			AND 	PmtDateTime BETWEEN @param2 AND @param3
			AND 	PmtPtmId = PtmId 
			AND 	(@param4 = '1' or PtmName NOT LIKE '%imprint%') 
			-- KX Changes :RKS 
			AND		RntPkgId = PkgId
			AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
												INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
												INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
												AND UsrCode = @sUsrCode)
				)
	END
	ELSE
		SELECT	'<tr bgcolor="white"><td>&#160;</td><td></td><td></td><td></td><td align="right"></td><td align="center"></td></tr>'
	RETURN
END -- IF(@case = 'BONDREPORT1')

IF (@case = 'SELBOOPRD')
BEGIN
	DECLARE @RntPkgId	VARCHAR(64),
			@sCtyCode	varchar(24)

	SELECT 	@RntPkgId = RntPkgId,
			@sCtyCode = ISNULL(dbo.getCountryForLocation(RntCkoLocCode, NULL, ''), '')
	FROM 	Rental WITH (NOLOCK) 
	WHERE	RntId = @param1
	IF @param3 = 'p'
		SELECT 	PrdId			AS	'Id',
			 	PrdShortName	AS	'ShortName',
			 	PrdName 		AS	'Name'
		FROM	PackageProduct WITH (NOLOCK) 
				LEFT OUTER JOIN SaleableProduct WITH (NOLOCK) ON pplsapid = sapid 
				LEFT OUTER JOIN Product PRODUCT WITH (NOLOCK) ON sapprdid = prdid
		WHERE 	PrdShortName LIKE @param2 + '%'
		AND		PrdIsVehicle = 0
		AND		PplPkgId = @RntPkgId
		AND		SapStatus = 'Active'
		AND		SapCtyCode = @sCtyCode
		-- KX Changes :RKS
		AND		PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
							   WHERE BrdComCode = Company.ComCode	
								AND Company.ComCode = UserCompany.ComCode 
								AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
												)
		ORDER BY PrdShortName
		FOR XML AUTO, ELEMENTS	
	ELSE
		SELECT 	PrdId			AS	'Id',
			 	PrdShortName	AS	'ShortName',
			 	PrdName 		AS	'Name'
		FROM SaleableProduct WITH (NOLOCK) LEFT OUTER JOIN Product PRODUCT WITH (NOLOCK) ON SapPrdId = PrdId
		WHERE 	PrdShortName LIKE @param2 + '%'
		AND		PrdIsVehicle = 0
		AND		SapStatus = 'Active'
		AND		SapCtyCode = @sCtyCode
		AND		SapIsBase = 1
		-- KX Changes :RKS
		AND		PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)
							   WHERE BrdComCode = Company.ComCode	
								AND Company.ComCode = UserCompany.ComCode 
								AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode
												)
		ORDER BY PrdShortName
		FOR XML AUTO, ELEMENTS			
	RETURN
END

IF (@case = 'VALIDATEBOOKING')
BEGIN
	DECLARE @sError varchar(8000),
			@sWarn	varchar(8000)
	IF @param2 = '' SET @param2 = NULL

	IF ISNULL(@param1, '') <> '' OR ISNULL(@param2, '') <> ''
	BEGIN
		EXECUTE RES_BookingValidation
			@psBooId	= @param1,			-- VARCHAR(64),
			@psRntId	= @param2,			-- VARCHAR(64),
			@psUsrId	= @param3,			-- VARCHAR(64),
			@psErrors	= @sError  	OUTPUT,	-- VARCHAR(8000)	OUTPUT
			@psWarn		= @sWarn	OUTPUT	-- VARCHAR(8000)	OUTPUT
	END
	ELSE
		SELECT	@sError = 'Both BooId and RntId are Blank',
				@sWarn = ''

	SELECT '<Error>' + ISNULL(@sError, '') + '</Error><Wrn>' + ISNULL(@sWarn, '') + '</Wrn>'
	RETURN
END

IF (@case = 'CHKBOORENT')
BEGIN
	IF NOT EXISTS(SELECT booid FROM Booking WITH (NOLOCK) WHERE BooNum = @param1)
	BEGIN
		SELECT 'ERROR^' + dbo.getErrorString('GEN003', @param1,'Booking Number','','','','')
		RETURN
	END
	IF NOT EXISTS(SELECT RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId 
					WHERE BooNum = @param1
					AND		RntNum = @param2)
	BEGIN
		SELECT 'ERROR^' + dbo.getErrorString('GEN003', @param2 , 'Rental Number For Booking "' + @param1 + '"', '', '','','')
		RETURN
	END
	SELECT 'SUCCESS^'
	SELECT TOP 1
		''+BooNum + '/' + RntNum 					AS	'RntId',
		''+PrdDvassSeq								AS	'PrdSeq',
		CASE dbo.getCountryForLocation(RntCkoLocCode, NULL, '') 
			WHEN 'AU'	THEN 0
			WHEN 'NZ'	THEN 1
			ELSE	-1
		END											AS 'Cty',
		''+UPPER(RntCkoLocCode)						AS	'CkoLoc',
		''+CONVERT(VARCHAR(10), RntCkoWhen, 103)	AS	'CkoDate',
		CASE dbo.getAmPmFromDate(RntCkoWhen)
			WHEN 'AM' 	THEN 0
			WHEN 'PM'	THEN 1			
			ELSE	-1
		END											AS	'CkoDayPart',


		UPPER(RntCkiLocCode)						AS	'CkiLoc',
		CONVERT(VARCHAR(10), RntCkiWhen, 103)		AS	'CkiDate',	
		CASE dbo.getAmPmFromDate(RntCkiWhen)
			WHEN 'AM' 	THEN 0

			WHEN 'PM'	THEN 1			
			ELSE	-1
		END											AS	'CkiDayPart',
		1											AS	'PRIORITY',
		10											AS	'VISIBILITY',
		1											AS	'FORCEFLAG'	
	FROM BookedProduct DVASS WITH (NOLOCK)
				LEFT OUTER JOIN SaleableProduct WITH (NOLOCK) ON SapId = BpdSapId
				LEFT OUTER JOIN Product WITH (NOLOCK) ON SapPrdId = PrdId
				LEFT OUTER JOIN Rental WITH (NOLOCK) ON BpdRntId = RntId
				LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId
	WHERE 	BooNum = @param1
	AND		RntNum = @param2
	AND 	PrdIsVehicle = 1
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF (@case = 'UPDTALLVEHSP')
BEGIN
	UPDATE Rental WITH (ROWLOCK)
		SET RenAllowVehSpec = 	CASE @param3
									WHEN '' THEN NULL
									ELSE @param3
								END
	WHERE RntId IN (SELECT TOP 1 RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId
								WHERE 	BooNum = @Param1
								AND 	RntNum = @Param2)
	IF (@@ERROR <> 0)
	BEGIN
		SELECT 'ERROR^ErrorIn Updating Rental (' + @Param1 + '/' + @Param2 + ')'
		RETURN
	END
	SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')
	RETURN
END

IF (@case = 'FLEXAGENTEMAIL')
BEGIN
	SELECT PhnEmailAddress AS 'Email'
	FROM Contact with(NOLOCK), PhoneNumber WITH(NOLOCK), Agent WITH(NOLOCK), Code WITH(NOLOCK)
	WHERE  ConPrntId = AgnId
	AND  ConPrntTableName = 'AGENT'
	AND  ConCodContactId = CodId
	AND  PhnPrntTableName = 'Contact'
	AND  PhnPrntId = ConId
	AND  CodCdtNum = 25
	AND  CodCode = 'FLEX'
	AND  PhnCodPhoneId IN (SELECT CodId FROM Code WITH(NOLOCK) WHERE CodCdtNum = 11 AND CodCode = 'Email')
	FOR XML AUTO, ELEMENTS
	RETURN
END

IF (@case = 'RENTADD')
BEGIN
	SET @param2 = dbo.getSplitedData(@param1, '/', 1)
	SET @param3 = dbo.getSplitedData(@param1, '/', 2)
	SELECT	TOP 1 
			''+@param1	 								AS	'RentalId',
			''+PrdDvassSeq								AS	'PrdId',

			''+RntCkoLocCode							AS	'CoLoc',
			''+CONVERT(VARCHAR(10), RntCkoWhen, 103)	AS	'CoDate',
			''+dbo.GetAmPmFromDate(RntCkoWhen) 			AS	'CkoDayPart',
			''+RntCkiLocCode							AS	'CiLoc',
			''+CONVERT(VARCHAR(10), RntCkiWhen, 103)	AS	'CiDate',
			''+dbo.GetAmPmFromDate(RntCkiWhen) 			AS	'CkiDayPart',
			''+'0'										AS 	'Revenue',
			''+'1' 										AS 	'Priority',
			''+'10' 									AS 	'Visibility',
			''+'' 										AS 	'VehicleRanges',
			''+'1'										AS 	'Force'
		FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK), BookedProduct WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)
		WHERE	BooId = RntBooId
		AND		BpdRntId = RntId
		AND		BpdPrdIsVehicle = 1
		AND		BpdIsCurr = 1
		AND		SapId = BpdSapId
		AND		PrdId = SapPrdId
		AND		BooNum = @param2
		AND		RntNum = @param3
		ORDER BY BpdCkiWhen DESC
		FOR XML AUTO, ELEMENTS
	RETURN
END

IF @case = 'VEHASSGET_GRID'
BEGIN
	SET @param3 = @param1
	SET @param2 = NULL
	SET @param1 = dbo.getSplitedData(@param3, '/', 1)
	
	SELECT @Param1 = BooId FROM Booking WITH(NOLOCK)
		WHERE	BooNum = @Param1
	IF CHARINDEX('/', @param3) <> 0
	BEGIN
		SET @param2 = dbo.getSplitedData(@param3, '/', 2)
		SELECT @Param2 = RntId FROM Rental WITH(NOLOCK)
			WHERE	RntBooId = @Param1
			AND		RntNum = @Param2
	END
	
	SET @sComCode = dbo.fun_getCompany
							(
								@sUsrCode,		--@sUserCode	VARCHAR(64),
								NULL,				--@sLocCode	VARCHAR(64),
								NULL,				--@sBrdCode	VARCHAR(64),
								NULL,				--@sPrdId		VARCHAR(64),
								NULL,				--@sPkgId		VARCHAR(64),
								NULL,				--@sdummy1	VARCHAR(64),
								NULL,				--@sdummy2	VARCHAR(64),
								NULL				--@sdummy3	VARCHAR(64)
							)
	IF NOT EXISTS(SELECT RntId FROM Rental WITH(NOLOCK)  INNER JOIN Package (NOLOCK) ON RntPkgId = PkgId
				WHERE	RntBooId = @Param1
				AND		RntId = @param2
				-- KX Changes: RKS
				AND	PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) WHERE BrdComCode = @sComCode)
				AND RntStatus = 'KK'
				)
	BEGIN
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrCode>GEN</ErrCode><ErrDesc>' + @param3 + '  - Not a valid Booking Number for company - ' + @sComCode + '</ErrDesc></Error>'
		RETURN
	END

	SET @param3 = dbo.getCodeId(13, 'Veh_Assign')
	
	IF EXISTS(SELECT NteId FROM	Note WITH(NOLOCK) 
					WHERE	NteBooId = @param1
					AND		NteRntId = @Param2
					AND		NteCodTypId = @param3)
		SELECT	NteId							'Id',
				NteDesc							'Desc',
				dbo.getCodCode(NteCodAudTypId)	'AudTyp',			
				NtePriority						'Pri',
				NteIsActive						'Act',
				CASE 
					WHEN NteIsActive = 1 THEN 'Yes'
					ELSE 'No'
				END								'Active',
				IntegrityNo						'Int'
			FROM	Note WITH(NOLOCK) 
			WHERE	NteBooId = @param1
			AND		NteRntId = @Param2
			AND		NteCodTypId = @param3
			ORDER BY NtePriority
			FOR XML AUTO, ELEMENTS
	ELSE
		SELECT '<Note><Id/><Desc/><AudTypId>' + dbo.getCodeId(26, 'All') + '</AudTypId><AudTyp></AudTyp><Pri/><Act>1</Act><Int/></Note>'
	RETURN
END -- IF @case = 'VEHASSGET_GRID'

IF @case = 'VALIDATEVEHICLE'
BEGIN
	DECLARE	@sTempComCode	varchar(64)

	/*IF @sUsrCode<>''
		SELECT 
				@sFleetCompanyId = FleetCompanyId, 
				@sTempComCode	 = UserCompany.ComCode
		FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode
	
	IF NOT EXISTS(
				SELECT FleetAssetId FROM AimsProd.dbo.FleetAsset (NOLOCK)
				WHERE UnitNumber = @param1 AND CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0
	)
	BEGIN
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrCode>GEN</ErrCode><ErrDesc>' + @param1 + '  - Not a valid unit number for company - ' + @sTempComCode + '</ErrDesc></Error>'
		RETURN
	END
	ELSE*/
		SELECT '<root></root>'
END -- IF @case = 'VALIDATEVEHICLE'

IF @case = 'VEHASSUPDT'
BEGIN
	IF dbo.getSplitedData(@param3, '~', 1) = ''
	BEGIN
		SET @sUsrCode = dbo.getSplitedData(@param3, '~', 7)
		SELECT @sComCode = dbo.fun_getCompany
						(
							@sUsrCode,	-- @sUserCode	VARCHAR(64),
							NULL,		-- @sLocCode	VARCHAR(64),
							NULL,		-- @sBrdCode	VARCHAR(64),
							NULL,		-- @sPrdId		VARCHAR(64),
							NULL,		-- @sPkgId		VARCHAR(64),
							NULL,		-- @sdummy1	VARCHAR(64),
							NULL,		-- @sdummy2	VARCHAR(64),
							NULL		-- @sdummy3	VARCHAR(64)
						)

		INSERT INTO Note (	NteId,
							NteBooID,
							NteRntId,
							NteCodTypId,
							NteDesc,
							NteCodAudTypId,
							NtePriority,
							NteIsActive,
							IntegrityNo,
							AddUsrId,
							AddDateTime,
							AddPrgmName,
							-- KX Changes :RKS
							NteComCode
						)
			SELECT 	NEWID(),
					(SELECT BooId FROM Booking WITH(NOLOCK) WHERE BooNum = @param1),
					(SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK) WHERE BooId = RntBooId AND BooNum = @param1 AND RntNum = @param2),
					dbo.getCodeId(13, 'Veh_Assign'),
					dbo.getSplitedData(@param3, '~', 2),
					dbo.getCodeId(26, dbo.getSplitedData(@param3, '~', 4)),
					dbo.getSplitedData(@param3, '~', 3),
					dbo.getSplitedData(@param3, '~', 5),
					1,
					(SELECT UsrId FROM UserInfo with (nolock) WHERE UsrCode = dbo.getSplitedData(@param3, '~', 7)),
					CURRENT_TIMESTAMP,
					'Vehicle_Assign',
					-- KX Changes :RKS
					@sComCode
		IF (@@ERROR <> 0)
		BEGIN
			SELECT 'ERROR^ErrorIn Updating Note For Vehicle Assign(' + @Param1 + '/' + @Param2 + ')'
			RETURN
		END
		SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')
		RETURN
	END -- IF dbo.getSplitedData(@param3, '~', 1) = ''
	ELSE
	BEGIN
		Update Note WITH(ROWLOCK)
			SET NteDesc = dbo.getSplitedData(@param3, '~', 2),
				NteCodAudTypId = dbo.getCodeId(26, dbo.getSplitedData(@param3, '~', 4)),
				NtePriority = dbo.getSplitedData(@param3, '~', 3),
				NteIsActive = dbo.getSplitedData(@param3, '~', 5),
				IntegrityNo = IntegrityNo + 1,
				ModUsrId = (SELECT UsrId FROM UserInfo  with (nolock) WHERE UsrCode = dbo.getSplitedData(@param3, '~', 7)),
				ModDateTime = CURRENT_TIMESTAMP
		WHERE	NteId = dbo.getSplitedData(@param3, '~', 1)
		IF (@@ERROR <> 0)
		BEGIN
			SELECT 'ERROR^ErrorIn Updating Note For Vehicle Assign(' + @Param1 + '/' + @Param2 + ')'
			RETURN
		END
		SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')
		RETURN
	END		
	RETURN
END -- IF @case = 'VEHASSUPDT'

IF (@case = 'getBpdStatus')
BEGIN
	DECLARE @iValue	VARCHAR	 (12)
	SET @iValue = ISNULL(dbo.getProductAttributeValue(@param2,'BkgRef'),'0')
	SELECT @soryBy = CdtSortBy FROM CodeType WHERE CdtNum = 42
	IF (@soryBy = NULL)
		SET @soryBy = ''

	IF @param1 IN ('KK','NN')
	BEGIN
		IF (@soryBy = '' OR @soryBy = 'CodCode')
		BEGIN
			SELECT	CodId 				AS 'ID',
					CodCode				AS 'CODE',
					ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
			FROM Code WITH (NOLOCK)
			WHERE	Codcdtnum     	= 42
			AND		CodIsActive		= 1
			AND 	CodCode IN ('KK','NN')
			ORDER BY CodCode
			FOR XML AUTO,ELEMENTS
			--RETURN
		END
		
		IF (@soryBy = 'CodDesc' )
		BEGIN
			SELECT	CodId 				AS 'ID',
					CodCode				AS 'CODE',
					ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
			FROM Code WITH (NOLOCK)
			WHERE	Codcdtnum     	= 42
			AND		CodIsActive	= 1
			AND 		CodCode IN ('KK','NN')
			ORDER BY CodDesc
			FOR XML AUTO,ELEMENTS
			--RETURN
		END
	     
		IF (@soryBy = 'CodOrder' )
		BEGIN
			SELECT	CodId 			AS 'ID',
					CodCode		AS 'CODE',
					ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
			FROM Code WITH (NOLOCK)
			WHERE	Codcdtnum   = 42
			AND		CodIsActive	= 1
			AND 	CodCode IN ('KK','NN')
			ORDER BY CodOrder
			FOR XML AUTO,ELEMENTS
			--RETURN
		END	
--		DECLARE		@sRntId		VARCHAR(64)
		SELECT @sRntId = BpdRntId FROM Bookedproduct WHERE BpdId = @param3

		IF @param1 = 'NN' AND NOT Exists(SELECT BpdId FROM dbo.BookedProduct (NOLOCK)
											 WHERE BpdStatus = 'XX' 
											 AND BpdRntId = @sRntId)
			SELECT	CodId 			AS 'ID',
					CodCode		AS 'CODE',
					ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
			FROM Code WITH (NOLOCK)
			WHERE	Codcdtnum   = 42
			AND		CodIsActive	= 1
			AND 	CodCode IN ('QN')
			ORDER BY CodOrder
			FOR XML AUTO,ELEMENTS
			RETURN	
		RETURN
	END	
	ELSE
	BEGIN
		IF @iValue = '1'
		BEGIN
			IF (@soryBy = '' OR @soryBy = 'CodCode')
			BEGIN
				SELECT	CodId 					AS 'ID',
						CodCode				AS 'CODE',
						ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
				FROM Code WITH (NOLOCK)
				WHERE	Codcdtnum     	= 42
				AND		CodIsActive	= 1
				AND 	CodCode NOT IN ('XX','XN','CO','CI')
				ORDER BY CodCode
				FOR XML AUTO,ELEMENTS
				RETURN
			END
		
			IF (@soryBy = 'CodDesc' )
			BEGIN
				SELECT	CodId 				AS 'ID',
						CodCode				AS 'CODE',
						ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
					 FROM Code WITH (NOLOCK)
					WHERE	Codcdtnum     	= 42
					AND		CodIsActive	= 1
					AND 		CodCode NOT IN ('XX','XN','CO','CI')
					ORDER BY CodDesc
					FOR XML AUTO,ELEMENTS
				RETURN
			END
		     
			IF (@soryBy = 'CodOrder' )
			BEGIN
				SELECT	CodId 			AS 'ID',
						CodCode		AS 'CODE',
						ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
				FROM Code WITH (NOLOCK)
				WHERE	Codcdtnum     	= 42
				AND		CodIsActive	= 1
				AND 		CodCode NOT IN ('XX','XN','CO','CI')
				ORDER BY CodOrder
				FOR XML AUTO,ELEMENTS
				RETURN
			END
			RETURN

		END	--	IF @iValue = '1'
		ELSE
		BEGIN
			IF (@soryBy = '' OR @soryBy = 'CodCode')
			BEGIN
				SELECT	CodId 					AS 'ID',
							CodCode				AS 'CODE',
							ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
				FROM Code WITH (NOLOCK)
				WHERE	Codcdtnum     	= 42
				AND		CodIsActive	= 1
				AND 	CodCode NOT IN ('XX','XN','CO','CI','TK')
				ORDER BY CodCode
				FOR XML AUTO,ELEMENTS
				RETURN
			END
		
			IF (@soryBy = 'CodDesc' )
			BEGIN
				SELECT	CodId 				AS 'ID',
						CodCode				AS 'CODE',
						ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
				FROM Code WITH (NOLOCK)
				WHERE	Codcdtnum     	= 42
				AND		CodIsActive	= 1
				AND 	CodCode NOT IN ('XX','XN','CO','CI','TK')
				ORDER BY CodDesc
				FOR XML AUTO,ELEMENTS
				RETURN
			END
		     
			IF (@soryBy = 'CodOrder' )
			BEGIN
				SELECT	CodId 			AS 'ID',
						CodCode		AS 'CODE',
						ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
				FROM Code WITH (NOLOCK)
				WHERE	Codcdtnum     	= 42
				AND		CodIsActive	= 1
				AND 		CodCode NOT IN ('XX','XN','CO','CI','TK')
				ORDER BY CodOrder
				FOR XML AUTO,ELEMENTS
				RETURN
			END
		END		--	ELSE IF @iValue = '1'
	END
	RETURN
END

IF(@case = 'CTYOFOPR')
BEGIN
	DECLARE @sUviValue AS	varchar(500)
	DECLARE @iLen	 AS 	int
	DECLARE @scount  AS	int
	DECLARE @sstring AS	varchar(100)
	
	SET @sstring = ''
	SET @scount = 0
	SET @sUviValue = dbo.getUviValue('CTYOFOPR')
	SET @iLen = LEN(@sUviValue)
	
	WHILE(@scount <= @iLen)
	BEGIN	
		IF(SUBSTRING(@sUviValue,@scount,1) = ',')
		BEGIN
			IF EXISTS (SELECT ctycode FROM  country WITH (NOLOCK)
						WHERE ctycode = @sstring)
			BEGIN
				SELECT	ctyCode		AS	'CODE',
						CtyName		AS	'DESCRIPTION'
				FROM Country AS POPUP WITH (NOLOCK)
				WHERE ctycode = @sstring
				AND (	ctycode LIKE @param1 + '%'
				OR	ctyName LIKE @param1 + '%')
				FOR XML AUTO,ELEMENTS
			END 					
			SET @sstring = ''
			GOTO skipLoop
		END
		ELSE
		BEGIN
			SET @sstring = @sstring + SUBSTRING(@sUviValue,@scount,1)			
		END
	
		skipLoop:
			IF(@scount = @iLen)
			BEGIN
				IF EXISTS (SELECT ctycode FROM  country WITH (NOLOCK)
					WHERE ctycode = @sstring)
				BEGIN				
					SELECT	ctyCode		AS	'CODE',
						CtyName		AS	'DESCRIPTION'
					FROM Country AS POPUP WITH (NOLOCK)
					WHERE ctycode = @sstring
					AND (	ctycode LIKE @param1 + '%' 
					OR	ctyName LIKE @param1 + '%')
					FOR XML AUTO,ELEMENTS
				END 
			END
			SET @scount = @scount + 1		
	END
	RETURN
END
-- Issue 879: RKS : 18-MAY-2006
-- START
IF(@case	=	'FleetModel4BookingSearch')
BEGIN
	
	--SET @sFleetCompanyId = ''
	IF @sUsrCode<>''
		SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode
	
	SELECT  LTRIM(RTRIM(FleetModelCode))			AS	'CODE',
		 	LTRIM(RTRIM(FleetModelDescription))		AS 'DESCRIPTION'
		FROM 	AimsProd.dbo.FleetModel AS POPUP WITH (NOLOCK)
		WHERE 	(FleetModelCode 	LIKE	@param1+'%' OR FleetModelDescription LIKE @param1+'%')
		AND	InactiveDate IS NULL
		-- KX Changes :RKS
		AND (@sUsrCode='' OR POPUP.FleetModelId IN (SELECT Fleet.FleetModelId FROM AIMSProd.dbo.FleetAsset Fleet (NOLOCK)
							 WHERE CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0))
		FOR XML AUTO,ELEMENTS	
	RETURN
END
IF @case = 'ChangeUserCompany'
BEGIN
	SELECT @sComCode = dbo.fun_getCompany
						(
							@param1,	-- @sUserCode	VARCHAR(64),
							NULL,		-- @sLocCode	VARCHAR(64),
							NULL,		-- @sBrdCode	VARCHAR(64),
							NULL,		-- @sPrdId		VARCHAR(64),
							NULL,		-- @sPkgId		VARCHAR(64),
							NULL,		-- @sdummy1	VARCHAR(64),
							NULL,		-- @sdummy2	VARCHAR(64),
							NULL		-- @sdummy3	VARCHAR(64)
						)
	IF @sComCode = 'KXS'
	BEGIN
		UPDATE dbo.UserCompany WITH (ROWLOCK)
			SET ComCode = 'THL' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)
		IF @param1 = 'GRAYS'
			UPDATE dbo.UserInfo WITH (ROWLOCK)
			SET UsrCtyCode = 'NZ', UsrLocCode = 'HQZ' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)
		ELSE
			UPDATE dbo.UserInfo WITH (ROWLOCK)
				SET UsrCtyCode = 'NZ', UsrLocCode = 'AKL' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)
	END	
	ELSE
	BEGIN
		UPDATE dbo.UserCompany WITH (ROWLOCK)
			SET ComCode = 'KXS' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)
		
		UPDATE dbo.UserInfo WITH (ROWLOCK)
			SET UsrCtyCode = 'NZ', UsrLocCode = 'AK1' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)
	END

END

-- END
/*--found in the akl-kxdev-001 but not exist to the thlmel015 - manny (march 12,07)*/

-- REV:MIA 090908 Added Manny 
IF(@case = 'getProduct')  
BEGIN  

exec RES_getProduct 
	 @param1,   -- productname
     @param2,   -- b for britz and so on 
	 @param4,   -- ac nk or 
	 @param5,   -- type
     '',        -- branch
	 @param3,   -- features 
	 '',        -- ckidate	
	 @sUsrCode
 RETURN  
END           


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON





-------------
GO
-------------


CREATE   PROCEDURE [dbo].[RES_CreateAvailableVehiclePackage_OSLO]
		@psVhrId		VARCHAR(64),
		@psBooId		VARCHAR(64) = '',
		@psBpdId		VARCHAR(64) = '',
		@psUserId		VARCHAR(64),
		@psProgramName	VARCHAR(64)
AS  

--	========================================
--	START INITIALISE SECTION
--	========================================

--	SQL server interferes with the record sets otherwise
SET NOCOUNT ON

--	ADD an acronym for this program so as a "call stack"
--	type of concept is reflected in the program name for
--	error handling purposes.
SELECT	@psProgramName = ISNULL(@psProgramName, '') + '-CrAVP'

--	++++++++++++++++++++++++++++++
--		LOCAL VARIABLES
--	++++++++++++++++++++++++++++++
--
--	The variables are grouped by DATA TYPE
--	and then sorted alphabetically.
DECLARE	@bAvpPerPerson		BIT,		@bPkgDispPrice		BIT,		@bFalseValue		BIT,
		@bTrueValue			BIT,		@dAddedTimeStamp	DATETIME,	@dAvvCkiDate		DATETIME,
		@dAvvCkoDate		DATETIME,	@dCkiTime			DATETIME,	@dCkoTime			DATETIME,
		@dCkiWhen			DATETIME,	@dCkoWhen			DATETIME,	@dVhrCkiDate		DATETIME,			
		@dVhrCkoDate		DATETIME,	@iAvvHirePeriod		INTEGER,	@iAvvHireUOM		INTEGER,
		@iDoc				INTEGER,	@iVhrNumOfAdults	INTEGER,	@iVhrNumOfChildren	INTEGER,
		@iVhrNumOfInfants	INTEGER,	@cAvpTotCost		MONEY,		@sAvpCodCurrId		VARCHAR(64),
		@sAvpCodUomId		VARCHAR(64),@sAvpUomCode		VARCHAR(64),@sAvpId				VARCHAR(64),
		@sAvvCkiAmPm		VARCHAR(64),@sAvvCkoAmPm		VARCHAR(64),@sAvvCkiLocCode		VARCHAR(64),
		@sAvvCkoLocCode		VARCHAR(64),@sAvvId				VARCHAR(64),@sAvvIdCurrent		VARCHAR(64),
		@sAvvPrdId			VARCHAR(64),@sAvvVhrId			VARCHAR(64),@sBapId				VARCHAR(64),
		@sBlrIdList			VARCHAR(64),@sError				VARCHAR(1000),@sErrorWarning	VARCHAR(1000),
		@sMessage			VARCHAR(1000),@sPkgId			VARCHAR(64),@sPkgBrdCode		VARCHAR(1),
		@sPkgCodCurrId		VARCHAR(64),@sPplPkgId			VARCHAR(64),@sPplSapId			VARCHAR(64),
		@sSapId				VARCHAR(64),@sSapIdCurrent		VARCHAR(64),@sTemp				VARCHAR(128),
		@sVhrCkiLocCode		VARCHAR(64),@sVhrCkoLocCode		VARCHAR(64),@sVhrPkgId			VARCHAR(64),
		@sWeekendDayNames	VARCHAR(32),@sAvvIdList			VARCHAR(2000),@sPackageIdList	VARCHAR(2000),
		@sPtbIdList			VARCHAR(64),@sSapIdList			VARCHAR(8000),@sFailedSapIds	varchar(8000),
		@bWriteToLogFile	BIT,		@sPatValue			VARCHAR(64),@strLogFileName		VARCHAR(512),
		@sAgnId				VARCHAR(64),@strLogMessageText	VARCHAR(2000),@sSapIdListAddToBkg VARCHAR(8000),
		@sSapIdAddBkgList VARCHAR(8000)

--	SET bit constants
SELECT	@bTrueValue		= CAST(1 AS BIT),
		@bFalseValue	= CAST(0 AS BIT),
		@sWeekendDayNames= 'Saturday,Sunday',
		@dAddedTimeStamp = CURRENT_TIMESTAMP

--	PREPARE for debug
SELECT	@strLogFileName	= 'CreateAvp Subbaiah'
SELECT	@bWriteToLogFile = @bFalseValue

-- Get the Agent Id
IF ISNULL(@psBooId, '') <> '' AND EXISTS(SELECT BooId FROM Booking WITH(NOLOCK) WHERE BooId = @psBooId)
	SELECT @sAgnId = BooAgnId
		FROM	Booking WITH(NOLOCK)
		WHERE	BooId = @psBooId
ELSE IF ISNULL(@psBpdId, '') <> '' AND EXISTS(SELECT BpdId FROM BookedProduct WITH(NOLOCK) WHERE BpdId = @psBpdId)
	SELECT @sAgnId = BooAgnId
		FROM	Booking WITH(NOLOCK), Rental WITH(NOLOCK), BookedProduct WITH(NOLOCK)
		WHERE	BooId = RntBooId
		AND		RntId = BpdRntId
		AND		BpdId = @psBpdId
ELSE
	SELECT	@sAgnId	= BkrAgnId
		FROM	BookingRequest WITH (NOLOCK), VehicleRequest WITH (NOLOCK)
		WHERE	BkrId = VhrBkrId
		AND		VhrId	= @psVhrId

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IF	@bWriteToLogFile = @bTrueValue
BEGIN
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 ========================================', @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 RES_CreateAvailableVehiclePackage()', @bWriteToLogFile
	SELECT	@strLogMessageText = '001     ' + (CASE WHEN @psProgramName IS NULL THEN 'NULL' WHEN LEN(@psProgramName) = 0 THEN '{empty}' ELSE @psProgramName END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 ========================================', @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<', @bWriteToLogFile
	SELECT	@strLogMessageText = '001 Vhr ID ' + (CASE WHEN @psVhrId IS NULL OR LEN(@psVhrId) = 0 THEN 'NULL' ELSE @psVhrId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '001 Boo ID ' + (CASE WHEN @psBooId IS NULL OR LEN(@psBooId) = 0 THEN 'NULL' ELSE @psBooId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '001 Bpd ID ' + (CASE WHEN @psBpdId IS NULL OR LEN(@psBpdId) = 0 THEN 'NULL' ELSE @psBpdId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '001 Agent ID ' + (CASE WHEN @sAgnId IS NULL OR LEN(@sAgnId) = 0 THEN 'NULL' ELSE @sAgnId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<', @bWriteToLogFile
END -- Debug
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

--	MTS 21AUG02 : START ADD : ++++++++++++++++++++++++++++++++++++++++
SELECT	@sAvvIdList =	CASE WHEN @sAvvIdList IS NULL OR LEN(@sAvvIdList) = 0
							THEN	AvvId
							ELSE	@sAvvIdList + ',' + AvvId
						END -- Case
	FROM	VehicleRequest WITH (NOLOCK) LEFT OUTER JOIN AvailableVehicle WITH (NOLOCK)	ON VhrId = AvvVhrId
	WHERE	VhrId = @psVhrID

--	START a while loop to walk the list
WHILE @sAvvIdList IS NOT NULL AND LEN(@sAvvIdList) > 0
BEGIN
	--	GET the first entry
	EXECUTE	GEN_GetNextEntryAndRemoveFromList
		@psCommaSeparatedList	= @sAvvIdList,
		@psListEntry		= @sAvvIdCurrent OUTPUT,
		@psUpdatedList		= @sAvvIdList	 OUTPUT

	--	MAINTAIN referential integrity
	IF	@sAvvIdCurrent IS NOT NULL AND LEN(@sAvvIdCurrent) > 0
--	AND	EXISTS	( SELECT	AvvId FROM	AvailableVehicle WITH (NOLOCK) WHERE	AvvId = @sAvvIdCurrent)
	BEGIN
		--	RETRIEVE other information from Vhr Parent
		SELECT	@sVhrPkgId	   		= VhrPkgId,
				@iVhrNumOfAdults   	= VhrNumOfAdults,
				@iVhrNumOfChildren 	= VhrNumOfChildren,
				@iVhrNumOfInfants  	= VhrNumOfInfants,
				@dVhrCkoDate	   	= VhrCkoDate,
				@dVhrCkiDate	   	= VhrCkiDate,
				@sVhrCkoLocCode	   	= VhrCkoLocCode,
				@sVhrCkiLocCode	   	= VhrCkiLocCode,
				@sAvvId		   		= AvvId,
				@sAvvVhrId	   		= VhrId,
				@sAvvPrdId	   		= AvvPrdId,
				@sAvvCkoLocCode	   	= AvvCkoLocCode,
				@sAvvCkiLocCode	   	= AvvCkiLocCode,
				@dAvvCkoDate	   	= AvvCkoDate ,
				@dAvvCkiDate	   	= AvvCkiDate,
				@sAvvCkoAmPm	   	= AvvCkoAmPm,
				@sAvvCkiAmPm	   	= AvvCkiAmPm,
				@iAvvHirePeriod	   	= AvvHirePeriod,
				@iAvvHireUOM	   	= AvvUOM
		FROM AvailableVehicle WITH (NOLOCK), VehicleRequest WITH (NOLOCK)
		WHERE	AvvId = @sAvvIdCurrent
		AND		VhrId = AvvVhrId

--	MTS 21AUG02 :   END ADD : ++++++++++++++++++++++++++++++++++++++++

			--	FIRST thing we need is the default times
			--	(Cko/Cki) for the location and AM or PM
		SELECT	@dCkoTime	= dbo.RES_GetDefaultTime(@sAvvCkoLocCode,@sAvvCkoAmPm,'CKO'),
				@dCkiTime	= dbo.RES_GetDefaultTime(@sAvvCkiLocCode,@sAvvCkiAmPm,'CKI'),
						--	ADD tehse onto the end of the AvailableVehicle
						--	check-out/in dates.....
				@dCkoWhen	= @dAvvCkoDate,--dbo.GEN_AddTimeToDate(@dAvvCkoDate, @dCkoTime),
				@dCkiWhen	= @dAvvCkiDate--dbo.GEN_AddTimeToDate(@dAvvCkiDate, @dCkiTime)

		IF	@sVhrPkgId IS NOT NULL
		BEGIN
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			IF	@bWriteToLogFile = @bTrueValue
			BEGIN
				EXECUTE SYS_WriteLogFileMessage @strLogFileName, '001 Vhr.PkgId NOT NULL', @bWriteToLogFile
				SELECT	@strLogMessageText = '001 CKO Date ' + (CASE WHEN @dAvvCkoDate IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(19), @dAvvCkoDate) END)
				EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				SELECT	@strLogMessageText = '001 CKi Date ' + (CASE WHEN @dAvvCkiDate IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(19), @dAvvCkiDate) END)
				EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile				
			END
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			--	CHECK the package works for agent et al

			EXECUTE RES_CheckPackage
				@PackageID				= @sVhrPkgId,
				@AgentID				= @sAgnId,
				@RentalID				= NULL,
				@CheckOutLocationCode	= @sAvvCkoLocCode,
				@CheckInLocationCode	= @sAvvCkiLocCode,
				@CheckOutDate			= @dCkoWhen,
				@CheckInDate			= @dCkiWhen,
				@ProductID				= @sAvvPrdId,
				@SaleableProductID		= NULL,
				@BookedDate				= @dAddedTimeStamp,
				@NumberOfAdults			= @iVhrNumOfAdults,
				@NumberOfChildren		= @iVhrNumOfChildren,
				@NumberOfInfants		= @iVhrNumOfInfants,
				@pbProduceDebugTrace	= @bWriteToLogFile,
				@ReturnError			= @sError			OUTPUT,
				@ReturnWarning			= @sErrorWarning	OUTPUT

			--	CAN we use this package? (NULL or empty string means "yes!")
			IF	@sError IS NULL OR	LEN(@sError) = 0
			BEGIN
				SET @sFailedSapIds = NULL
				EXEC RES_ManageProductAutoAdd
					@sPkgId					=	@sVhrPkgId ,
					@sVehPrdId				=	@sAvvPrdId, 
					@sPrdAttrName			=	'AutoAddBkg',
					@dCkoWhen				=	@dCkoWhen ,
					@dCkiWhen				=	@dCkiWhen, 
					@sCkoLocCode			=	@sAvvCkoLocCode, 
					@sCkiLocCode			=	@sAvvCkiLocCode, 
					@sAgnId					=	@sAgnId,
					@pbProduceDebugTrace	=	0,
					@sSapIdsToAdd			=	NULL,
					@sFailedSapIds			=	@sFailedSapIds OUTPUT

				--select PkgCode, @sFailedSapIds from Package where PkgId = @PackageID
				IF @sFailedSapIds IS NULL
					SELECT	@sPackageIdList	= '<Data><PackageID>' + @sVhrPkgId + '</PackageID></Data>'
			END
			ELSE
			BEGIN
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 CheckPackage.Error=[' + @sError + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			END
		END
		ELSE
		BEGIN
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			IF	@bWriteToLogFile = @bTrueValue
			BEGIN
				SELECT	@strLogMessageText = '001 Vhr.PkgId IS NULL'
				EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
			END
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			--	BUILD comma-separated list of blocking rules
			--		to give to getValidPackages()
			--	MTS 07MAY02 : added case to build list in one pass
			SELECT	@sBlrIdList	=	CASE
										WHEN	LEN(@sBlrIdList) > 0
										THEN	@sBlrIdList + ',' + BlrId
										ELSE	BlrId
									END -- Case
				FROM	VehicleRequestBlockingRule	WITH (NOLOCK)
				WHERE	VhrId	= @psVhrId
			--	MTS 07MAY02 : not needed (now managed in ^^ select ^^)
			--			IF	LEN(@sBlrIdList) > 1
			--				SET	@sBlrIdList=SUBSTRING(@sBlrIdList,2,LEN(@sBlrIdList)-1)
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			IF	@bWriteToLogFile = @bTrueValue
			BEGIN
				SELECT	@strLogMessageText = '001 Vhr.BlrList=[' + (CASE WHEN @sBlrIdList IS NULL OR LEN(@sBlrIdList) = 0 THEN 'NULL' ELSE @sBlrIdList END) + ']'
				EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
			END
			-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			EXECUTE RES_GetValidPackages
				@AgentId			= @sAgnId,
				@BookedDate			= @dAddedTimeStamp,
				@ProductID			= @sAvvPrdId,
				@CheckOutDate		= @dCkoWhen,	--@dAvvCkoDate,
				@CheckInDate		= @dCkiWhen,	--@dAvvCkiDate,				
				@CheckOutLocationCode= @sAvvCkoLocCode,
				@CheckInLocationCode= @sAvvCkiLocCode,
				@NumberOfAdults		= @iVhrNumOfAdults,
				@NumberOfChildren	= @iVhrNumOfChildren,
				@NumberOfInfants	= @iVhrNumOfInfants,
				@VehicleRequestID	= @psVhrId,
				@BlockingRuleIDList	= @sBlrIdList,
				@pbProduceDebugTrace= @bWriteToLogFile,
				@ReturnPackageIDList= @sPackageIdList OUTPUT

		END
		-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		IF	@bWriteToLogFile = @bTrueValue
		BEGIN
			SELECT	@strLogMessageText = '001 Vhr.PkgList=[' + (CASE WHEN @sPackageIdList IS NULL OR LEN(@sPackageIdList) = 0 THEN 'NULL' ELSE @sPackageIdList END) + ']'
			EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
		END
		-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		--	MTS 03MAY02 : SKIP resource-hungry OPENXML...
		--			...it also seems to be sooooo sloooooow...
		IF	@sPackageIdList IS NULL OR LEN(@sPackageIdList) = 0
			GOTO	fetch_next_AvailableVehicle

		--	MTS 21AUG02 : START ADD : ++++++++++++++++++++++++++++++++++++++++
		--	INSTEAD of using OPENXML, the "XML string" will
		--	be converted to a comma-separated list and
		--	processed that way.
		--	1. strip the leading elements off
		SELECT	@sPackageIdList = REPLACE(@sPackageIdList, '<Data><PackageID>', ''),
				--	2. strip the trailing elements off
				@sPackageIdList = REPLACE(@sPackageIdList, '</PackageID></Data>', ''),
				--	3. Change the intermediate ones to a COMMA
				@sPackageIdList = REPLACE(@sPackageIdList, '</PackageID><PackageID>', ',')

		--	START the loop
		WHILE	@sPackageIdList IS NOT NULL AND LEN(@sPackageIdList) > 0
		BEGIN
			--	RETRIEVE the first item from the list
			EXECUTE	GEN_GetNextEntryAndRemoveFromList
				@psCommaSeparatedList= @sPackageIdList,
				@psListEntry		= @sPkgId		OUTPUT,
				@psUpdatedList		= @sPackageIdList	OUTPUT

			--	MAINTAIN ref integrity
			IF	@sPkgId IS NOT NULL AND	LEN(@sPkgId) > 0
--			AND	EXISTS (SELECT	PkgId FROM	Package WITH (NOLOCK) WHERE	PkgId = @sPkgId)
			BEGIN
				--	MTS 21AUG02 :   END ADD : ++++++++++++++++++++++++++++++++++++++++
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Vhr.PkgId=[' + (CASE WHEN @sPkgId IS NULL OR LEN(@sPkgId) = 0 THEN 'NULL' ELSE @sPkgId END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	FIND Package details integrated with (efficiency)
				--	find Saleable Product details
				SELECT	@sSapId	= NULL
				SELECT	TOP 1	@sSapId			= SapId,
								@sPkgBrdCode	= PkgBrdCode, 
								@sPkgCodCurrId	= PkgCodCurrId, 
								@bPkgDispPrice	= PkgDispPrice
				FROM	SaleableProduct	WITH (NOLOCK), PackageProduct WITH (NOLOCK), Package WITH (NOLOCK)
				WHERE	SapPrdId	= @sAvvPrdId
				AND		PplSapId	= SapId
				AND		PplPkgId	= @sPkgId
				AND		PkgId		= @sPkgId
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Vhr.SapId=[' + (CASE WHEN @sSapId IS NULL OR LEN(@sSapId) = 0 THEN 'NULL' ELSE @sSapId END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	WASTE no more time
				IF	@sSapId IS NULL
					GOTO	fetch_next_package

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@sTemp = NULL
					SELECT	@sTemp = PrdShortName + '-' + CAST(SapSuffix AS VARCHAR(3))
					FROM	SaleableProduct	WITH (NOLOCK),
						Product		WITH (NOLOCK)
					WHERE	SapId	= @sSapId
					AND	PrdId	= SapPrdId
					SELECT	@strLogMessageText = '001 Vhr.Sap=[' + (CASE WHEN @sTemp IS NULL OR LEN(@sTemp) = 0 THEN 'NULL' ELSE @sTemp END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				--	WE will use the Ptb to resolve the UoM for hire period calculation
				EXECUTE	GEN_getProductTravelBand
					@sSapID		= @sSapId,
					@dCkoDate	= @dAvvCkoDate,
					@dBookedDate	= @dAddedTimeStamp,
					@sPkgID		= @sPkgId,
					@bFindFirst	= 1,
					@dCkiDate	= @dAvvCkiDate,
					@PtbIdList	= @sPtbIdList	OUTPUT,
					@ReturnError	= @sError	OUTPUT
				
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Vhr.PtbList=[' + (CASE WHEN @sPtbIdList IS NULL OR LEN(@sPtbIdList) = 0 THEN 'NULL' ELSE @sPtbIdList END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	WASTE no more time
				IF	@sError IS NOT NULL
				BEGIN
					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					IF	@bWriteToLogFile = @bTrueValue
					BEGIN
						SELECT	@strLogMessageText = '001 GEN.GetPTB @sError=[' + (CASE WHEN @sError IS NULL OR LEN(@sError) = 0 THEN 'NULL' ELSE @sError END) + ']'
						EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					END
					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					GOTO fetch_next_package
				END

				SELECT	@sAvpUomCode = dbo.RES_GetProductTravelBandUnitOfMeasureCode(@sPtbIdList, 1)

				--	SO as we can calculate hire period here properly
				--	to cover up th emissing number in the AvailableVehicle
				--	row.....reminds you of "band-aids for broken legs"...
				--	...hobble...limp...hobble...stagger...S P L A T !
				EXECUTE	RES_calculateHirePeriod
					@CheckOutDate	= @dCkoWhen,
					@CheckInDate	= @dCkiWhen,
					@HireUOM		= @sAvpUomCode,
					@HireQuantity	= @iAvvHirePeriod 	OUTPUT,
					@ReturnError	= @sError 			OUTPUT
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 CHP.Error=[' + (CASE WHEN @sError IS NULL OR LEN(@sError) = 0 THEN 'NULL' ELSE @sError END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	WASTE no more time
				IF	@sError IS NOT NULL
					GOTO	fetch_next_package

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 CHP.Qty=[' + RTRIM(CAST(@iAvvHirePeriod AS VARCHAR(10))) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	** Insert Available Vehicle Package row at this stage **
				--	ONLY do the insert if the row does not already exist
				IF	NOT EXISTS(SELECT TOP 1 AvpId
								FROM	AvailableVehiclePackage WITH (NOLOCK)
								WHERE	AvpAvvid = @sAvvId
								AND		AvpPkgId = @sPkgId)
				BEGIN
					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					IF	@bWriteToLogFile = @bTrueValue
					BEGIN
						SELECT	@strLogMessageText = '001 NOT Exists(Avp)'
						EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					END
					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					--	MTS 07MAY02 : MOVED to immediately prior to INSERT
					--	MTS 07MAY02 : MERGED 4 into 1 select statement
					--	MTS 06MAY02 : MOVED newid() call into loop
					--		      to eliminate duplicate keys
					--Select @sAvpId = @sAvvid + convert(varchar(3),(count(avpavvid) + 1)) from dbo.availablevehiclepackage (nolock) where avpavvid = @sAvvid
					SELECT	--@sAvpId			= REPLACE(NewId(),'-',''),
						@sAvpid = newid(),
							@sAvpCodCurrId	= @sPkgCodCurrId,
							@bAvpPerPerson	= dbo.GetProductAttributeValue(@sSapId, 'PPPRODUCT'),
							@sAvpCodUomId	= PtbCodUomId
						FROM	ProductRateTravelBand	WITH (NOLOCK)
						WHERE	PtbId	= dbo.RES_GetFirstProductTravelBandGuid(@sPtbIdList)

					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					IF	@bWriteToLogFile = @bTrueValue
					BEGIN
						SELECT	@strLogMessageText = '001 Avp.Id=[' + (CASE WHEN @sAvpId IS NULL OR LEN(@sAvpId) = 0 THEN 'NULL' ELSE @sAvpId END) + ']'
						EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
						SELECT	@strLogMessageText = '001 Avp.UoM=[' + dbo.RES_GetProductTravelBandUnitOfMeasureCode(@sPtbIdList, 1) + ']'
						EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					END
					-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					INSERT	INTO AvailableVehiclePackage WITH (ROWLOCK)(
						AvpId,				AvpAvvId,				AvpPkgId,
						AvpBrdCode,			AvpCodUomId,			AvpCodCurrId,
						AvpPerPerson,		AvpHirePeriod,			IntegrityNo,
						AddUsrId,			AddDateTime,			AddPrgmName
					)
					VALUES(
						@sAvpId,			@sAvvId,				@sPkgId,
						@sPkgBrdCode,		@sAvpCodUomId,			@sAvpCodCurrId,
						@bAvpPerPerson,		@iAvvHirePeriod,		1,
						@psUserId,			@dAddedTimeStamp,		@psProgramName
					)
					--	WASTE no more time
					IF	@@ERROR <> 0
						GOTO	fetch_next_package
				END -- Insert new row?
				ELSE
					SELECT	TOP 1 @sAvpId = AvpId
						FROM	AvailableVehiclePackage WITH (NOLOCK)
						WHERE	AvpAvvid = @sAvvId
						AND		AvpPkgId = @sPkgId

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@sTemp = NULL
					SELECT	@sTemp = PrdShortName + '-' + CAST(SapSuffix AS VARCHAR(3))
					FROM	SaleableProduct	WITH (NOLOCK),
						Product		WITH (NOLOCK)
					WHERE	SapId	= @sSapId
					AND	PrdId	= SapPrdId
					SELECT	@strLogMessageText = '001 CalcRatesBkr.Sap=[' + (CASE WHEN @sTemp IS NULL OR LEN(@sTemp) = 0 THEN 'NULL' ELSE @sTemp END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	WHERE package is defined to display price
				--	AND Package is not blocked,
				--	find Rate for Vehicle Product
				
				EXECUTE RES_CalculateRatesForBookingRequest_OSLO
					@sAvpId			= @sAvpId,
					@sSapId			= @sSapId,
					@sBooId			= NULL,
					@sBpdId			= NULL,
					@sUserId		= @psUserId, 
					@sProgramName	= @psProgramName,
					@ReturnError	= @sError	OUTPUT,
					@ReturnMessage	= @sMessage	OUTPUT	

				--	WHEN there is an error calculating the rates,
				--	we should not continue processing for this
				--	package. Rather we skip to the next package.
				IF	@sError IS NOT NULL AND LEN(@sError) > 0
					GOTO	fetch_next_package
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Product.AutoAdd()'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	FIND any "auto add at booking" products
				--		for this package and calculate
				--		rates for these
				SET @sSapIdAddBkgList = null
				EXECUTE	RES_ManageProductAutoAdd
					@sPkgId				= @sPkgId,
					@sVehPrdId			= @sAvvPrdId,
					@sPrdAttrName		= 'AutoAddBkg',
					@dCkoWhen			= @dAvvCkoDate,
					@dCkiWhen			= @dAvvCkiDate,
					@sCkoLocCode		= @sAvvCkoLocCode,
					@sCkiLocCode		= @sAvvCkiLocCode,
					@sAgnId				= @sAgnId,
					@pbProduceDebugTrace= @bWriteToLogFile,
					@sSapIdsToAdd		= @sSapIdAddBkgList	OUTPUT,
					@sFailedSapIds		= NULL

-- New Code for autoaddtobkg
				--	FIND any "auto add at booking" products
				--		for this package and calculate
				--		rates for these
				SELECT @sSapIdListAddToBkg = null
				EXECUTE	RES_ManageProductAutoAdd
					@sPkgId				= @sPkgId,
					@sVehPrdId			= @sAvvPrdId,
					@sPrdAttrName		= 'AutoAddToBkg',
					@dCkoWhen			= @dAvvCkoDate,
					@dCkiWhen			= @dAvvCkiDate,
					@sCkoLocCode		= @sAvvCkoLocCode,
					@sCkiLocCode		= @sAvvCkiLocCode,
					@sAgnId				= @sAgnId,
					@pbProduceDebugTrace= @bWriteToLogFile,
					@sSapIdsToAdd		= @sSapIdListAddToBkg	OUTPUT,
					@sFailedSapIds		= NULL
				
				SELECT @sSapIdList = null		
				IF (@sSapIdListAddToBkg is null OR @sSapIdListAddToBkg = '') AND @sSapIdAddBkgList is not null and @sSapIdAddBkgList <> ''
					SET @sSapIdList = @sSapIdAddBkgList
				ELSE IF (@sSapIdAddBkgList is null OR @sSapIdAddBkgList = '') AND @sSapIdListAddToBkg is not null and @sSapIdListAddToBkg <> ''
					SET @sSapIdList = @sSapIdListAddToBkg
				ELSE IF @sSapIdAddBkgList is NOT null AND @sSapIdAddBkgList <> '' AND @sSapIdListAddToBkg is not null and @sSapIdListAddToBkg <> ''
				BEGIN
					WHILE	@sSapIdAddBkgList IS NOT NULL AND LEN(@sSapIdAddBkgList) > 0
					BEGIN
						--	FIRST entry from list
						SET @sSapIdCurrent = null
						EXECUTE	GEN_GetNextEntryAndRemoveFromList
							@psCommaSeparatedList	= @sSapIdAddBkgList,
							@psListEntry			= @sSapIdCurrent	OUTPUT,
							@psUpdatedList			= @sSapIdAddBkgList	OUTPUT
	
						--	PROCESS current?
	 					IF	(@sSapIdCurrent IS NOT NULL) AND (LEN(@sSapIdCurrent) > 0)
						BEGIN
							IF 	charindex(@sSapIdCurrent,@sSapIdListAddToBkg) = 0
								SELECT @sSapIdList = CASE WHEN @sSapIdList is null or @sSapIdList = '' 
															THEN @sSapIdCurrent 
															ELSE @sSapIdList + ',' + @sSapIdCurrent 
													 END
						END	--	IF	(@sSapIdCurrent IS NOT NULL) AND (LEN(@sSapIdCurrent) > 0)
					END	--	WHILE	@sSapIdAddBkgList IS NOT NULL AND LEN(@sSapIdAddBkgList) > 0
					SELECT @sSapIdList = CASE WHEN @sSapIdList is null or @sSapIdList = '' 
												THEN @sSapIdListAddToBkg 
												ELSE @sSapIdList + ',' + @sSapIdListAddToBkg
										 END
				END	--	ELSE IF @sSapIdAddBkgList is NOT null AND @sSapIdAddBkgList <> '' AND @sSapIdListAddToBkg is not null and @sSapIdListAddToBkg <> ''
--End of new code  for autoaddtobkg

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 AutoAdd.SapList=[' + (CASE WHEN @sSapIdList IS NULL OR LEN(@sSapIdList) = 0 THEN 'NULL' ELSE @sSapIdList END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				--	PROCESS the Sap IDs to be added
				WHILE @sSapIdList IS NOT NULL AND LEN(@sSapIdList) > 0
				BEGIN
					--	FIRST entry from list
					EXECUTE	GEN_GetNextEntryAndRemoveFromList
						@psCommaSeparatedList	= @sSapIdList,
						@psListEntry		= @sSapIdCurrent	OUTPUT,
						@psUpdatedList		= @sSapIdList		OUTPUT

					--	PROCESS current?
					IF	(@sSapIdCurrent IS NOT NULL) AND (LEN(@sSapIdCurrent) > 0) AND EXISTS( SELECT SapId
																								FROM	SaleableProduct WITH (NOLOCK)
																								WHERE	SapId = @sSapIdCurrent)
					BEGIN
						-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						IF	@bWriteToLogFile = @bTrueValue
						BEGIN
							SELECT	@sTemp = NULL
							SELECT	@sTemp = PrdShortName + '-' + CAST(SapSuffix AS VARCHAR(3))
							FROM	SaleableProduct	WITH (NOLOCK),
								Product		WITH (NOLOCK)
							WHERE	SapId	= @sSapIdCurrent
							AND	PrdId	= SapPrdId
							SELECT	@strLogMessageText = '001 CalcRatesBkr.AutoSap=[' + (CASE WHEN @sTemp IS NULL OR LEN(@sTemp) = 0 THEN 'NULL' ELSE @sTemp END) + ']'
							EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
						END
						-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						--	ALTER  the Apr etc for this SapId
						EXECUTE	RES_CalculateRatesForBookingRequest_OSLO
							@sAvpId			= @sAvpId,
							@sSapId			= @sSapIdCurrent,
							@sBooId			= @psBooId,
							@sBpdId			= @psBpdId,
							@sUserId		= @psUserId,
							@sProgramName	= @psProgramName,
							@ReturnError	= @sError	OUTPUT,
							@ReturnMessage	= @sMessage	OUTPUT
					END -- IF	(@sSapIdCurrent IS NOT NULL) AND (LEN(@sSapIdCurrent) > 0)
				END -- WHILE @sSapIdList IS NOT NULL AND LEN(@sSapIdList) > 0

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Avp.Id=[' + (CASE WHEN @sAvpId IS NULL OR LEN(@sAvpId) = 0 THEN 'NULL' ELSE @sAvpId END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					SELECT	@cAvpTotCost = COUNT(AvcId)
					FROM	AvailableVehicleCharge	WITH (NOLOCK),
						AvailableVehicleProduct	WITH (NOLOCK)
					WHERE	AprAvpId	= @sAvpId 
					AND	AprId		= AvcAprId
					SELECT	@strLogMessageText = '001 Avc.Count=[' + (CASE WHEN @cAvpTotCost IS NULL THEN 'NULL' ELSE RTRIM(CAST(@cAvpTotCost AS VARCHAR(10))) END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					SELECT	@cAvpTotCost = SUM(ISNULL(AvcGrossAmt, 0))
					FROM	AvailableVehicleCharge	WITH (NOLOCK),
						AvailableVehicleProduct	WITH (NOLOCK)
					WHERE	AprAvpId	= @sAvpId 
					AND	AprId		= AvcAprId
					SELECT	@strLogMessageText = '001 Avc.Gross=[' + (CASE WHEN @cAvpTotCost IS NULL THEN 'NULL' ELSE RTRIM(CAST(@cAvpTotCost AS VARCHAR(10))) END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
					SELECT	@cAvpTotCost = SUM(ISNULL(AvcAgnDiscAmt, 0))
					FROM	AvailableVehicleCharge	WITH (NOLOCK),
						AvailableVehicleProduct	WITH (NOLOCK)
					WHERE	AprAvpId	= @sAvpId 
					AND	AprId		= AvcAprId
					SELECT	@strLogMessageText = '001 Avc.AgnDiscountAmount=[' + (CASE WHEN @cAvpTotCost IS NULL THEN 'NULL' ELSE RTRIM(CAST(@cAvpTotCost AS VARCHAR(10))) END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				--	AGGREGATE Gross-AgentDiscount as total cost
				SELECT	@cAvpTotCost = SUM(ISNULL(AvcGrossAmt,0) - ISNULL(AvcAgnDiscAmt,0))
					FROM	AvailableVehicleCharge	WITH (NOLOCK), AvailableVehicleProduct WITH (NOLOCK)
				WHERE	AprAvpId	= @sAvpId 
				AND		AprId		= AvcAprId

				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				IF	@bWriteToLogFile = @bTrueValue
				BEGIN
					SELECT	@strLogMessageText = '001 Avc.TotCost=[' + (CASE WHEN @cAvpTotCost IS NULL THEN 'NULL' ELSE RTRIM(CAST(@cAvpTotCost AS VARCHAR(10))) END) + ']'
					EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
				END
				-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				--	JUST in case...
				SELECT	@cAvpTotCost	= ISNULL(@cAvpTotCost, 0)
				--	MTS 06MAY02 : START ADD : correction to update Avp with total cost
				UPDATE	AvailableVehiclePackage	WITH (ROWLOCK)
					SET	AvpTotCost = @cAvpTotCost
				WHERE	AvpId	   = @sAvpId
				--	MTS 06MAY02 :   END ADD : correction to update Avp with total cost
			END -- If package exists
fetch_next_package:
		END -- While more packages
	END -- If AvailableVehicle

fetch_next_AvailableVehicle:

END -- While AvailableVehicle
--	CLEAN up cursors et al

cleanup_and_leave:
RETURN
--	********** End RES_CreateAvailableVehiclePackage() **********





-----------
GO
-----------





CREATE PROCEDURE [dbo].[RES_CalculateRatesForBookingRequest_OSLO]
	@sAvpId			VARCHAR(64),
	@sSapId			VARCHAR(64),
	@sBooId			VARCHAR(64),
	@sBpdId			VARCHAR(64),
	@sUserId		VARCHAR(64),
	@sProgramName	VARCHAR(256),
	@ReturnError	VARCHAR(2000)	OUTPUT,
	@ReturnMessage	VARCHAR(2000)	OUTPUT

AS

--	========================================
--	START INITIALISE SECTION
--	========================================

--	THIS option stops SQL Server interfering with
--	the record sets by spitting out the "xx rows affected"
--	message when it is not needed.
SET	NOCOUNT	ON

--	ADD an acronym for this program so as a "call stack"
--	type of concept is reflected in the program name for
--	error handling purposes.
SELECT	@sProgramName = ISNULL(@sProgramName, '') + '-CR4Bkr'

--	++++++++++++++++++++++++++++++
--		LOCAL VARIABLES
--	++++++++++++++++++++++++++++++
DECLARE
	@DbName				VARCHAR(200),
	@sError				VARCHAR(2000),
	@sMessage			VARCHAR(2000),

	@sRchId				VARCHAR(64),
	@dCkoWhen			DATETIME,
	@dCkiWhen			DATETIME,
	@sCkoLocationCode	VARCHAR(64),
	@sCkiLocationCode	VARCHAR(64),
	@dDBDateTime		DATETIME,
	@sCancelBpdId		VARCHAR(64),
	@iQuantity			INTEGER,
	@cRate				MONEY,
	@sSlvId				VARCHAR(64),
	@sAprId				VARCHAR(64),
	@dPreviousCkoWhen	DATETIME,
	@dPreviousCkiWhen	DATETIME,
	@sPreviousSapId		VARCHAR(64),
	@iKmTravelled		INTEGER,
	@bApplyOriginalRates	BIT,
	@sTableName			VARCHAR(500),
	@sTableId			VARCHAR(64),
	@sAvcId				VARCHAR(64),
-- RSM 051002 GST
	@sAgnid				VARCHAR(64),
	@sPkgId				VARCHAR(64),
	@bIsCusCharge		BIT,
	-- BIT constants
	@bFalseValue		BIT,
	@bTrueValue			BIT,
	-- DATETIME
	@dLocCkiTime		DATETIME,
	@dLocCkoTime		DATETIME,
	-- STRING
	@sAvvCkiAmPm		VARCHAR(64),
	@sAvvCkoAmPm		VARCHAR(64),
	-- WORKING variables
	@bWriteToLogFile	BIT,		-- TRUE means write it out
	@sPatValue			VARCHAR(64),	-- Product attribute value
	@sTemp				VARCHAR(2000),
	@sTemp0				VARCHAR(2000),
	@sTemp1				VARCHAR(2000),
	@sRchIdList			VARCHAR(2000),
	@strLogFileName		VARCHAR(512),	-- Used for logging to file
	@strLogMessageText	VARCHAR(2000)	-- ditto


--	** SQL Books Online note sp_dboption is only included for
--		compatibility with previous versions and may be
--		"removed in the future". ALTER DATABASE is
--		provided in its stead.

--	SET bit constants
SELECT	@bTrueValue		= CAST(1 AS BIT),
		@bFalseValue	= CAST(0 AS BIT)

--	PREPARE for debug
--SELECT	@strLogFileName	= dbo.SYS_FindDebugFileName('107', @sUserId, CURRENT_TIMESTAMP)
SELECT	@strLogFileName	= 'CalculateRatesForBookingRequest'



SELECT	@bWriteToLogFile = @bFalseValue

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IF	@bWriteToLogFile = @bTrueValue
BEGIN
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '007 ========================================', @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '007 RES_CalculateRatesForBookingRequest()', @bWriteToLogFile
	SELECT	@strLogMessageText = '007     ' + (CASE WHEN @sProgramName IS NULL THEN 'NULL' WHEN LEN(@sProgramName) = 0 THEN '{empty}' ELSE @sProgramName END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '007 ========================================', @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '007 PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<', @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Avp ID ' + (CASE WHEN @sAvpId IS NULL THEN 'NULL' WHEN LEN(@sAvpId) = 0 THEN '{empty}' ELSE @sAvpId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Sap ID ' + (CASE WHEN @sSapId IS NULL THEN 'NULL' WHEN LEN(@sSapId) = 0 THEN '{empty}' ELSE @sSapId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@sTemp = NULL
	SELECT	@sTemp = PrdShortName + '-' + CAST(SapSuffix AS VARCHAR(3))
	FROM	dbo.SaleableProduct	WITH (NOLOCK),
		dbo.Product		WITH (NOLOCK)
	WHERE	SapId	= @sSapId
	AND	PrdId	= SapPrdId
	SELECT	@strLogMessageText = '007 Sap ' + (CASE WHEN @sTemp IS NULL THEN 'NULL' WHEN LEN(@sTemp) = 0 THEN '{empty}' ELSE @sTemp END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Boo ID ' + (CASE WHEN @sBooId IS NULL THEN 'NULL' WHEN LEN(@sBooId) = 0 THEN '{empty}' ELSE @sBooId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Bpd ID ' + (CASE WHEN @sBpdId IS NULL THEN 'NULL' WHEN LEN(@sBpdId) = 0 THEN '{empty}' ELSE @sBpdId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage @strLogFileName, '007 PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<', @bWriteToLogFile
END -- Debug
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

--	========================================
--	END INITIALISE SECTION
--	========================================


--	========================================
--	START VALIDATION SECTION
--	========================================
--	MUST be provided
IF	@sAvpId IS NULL
OR	LEN(@sAvpId) = 0
BEGIN
	--	GEN099: mandatory field(s) not provided
	EXECUTE	GEN_getErrorString	'GEN099',
					'Available Vehicle Package ID',
					@oparam1 	= @sError OUTPUT
       	SELECT	@ReturnError ='GEN099/' + @sError
	RETURN
END


--	========================================
--	END VALIDATION SECTION
--	========================================

--	========================================
--	START PROCESSING SECTION
--	========================================

--	DETERMINE table-name and id
--	MTS 20AUG02 : START ADD : ++++++++++++++++++++++++++++++++++++++++
SELECT	@sTableName =
	CASE
		--	OTHERWISE, if booked product is not passed
		--	use saleable product as parent
		WHEN	(	@sBpdId IS NULL  OR	LEN(@sBpdId) = 0 )
		THEN	'SAP'
		--	BOOKED product is Custer's last stand...
		ELSE	'BPD'
	END, -- Case
	@sTableId   =
	CASE
		--	OTHERWISE, if booked product is not passed
		--	use saleable product as parent
		WHEN	(	@sBpdId IS NULL OR	LEN(@sBpdId) = 0)
		THEN	@sSapId
		--	BOOKED product is Custer's last stand...
		ELSE	@sBpdId
	END  -- Case

--	RETRIEVE cko/i stuff from available vehicle
--	* use Available...Package to get AvailableVehicle
SELECT	@dDBDateTime		= GETDATE(),
		@dCkoWhen		= AvvCkoDate,
		@dCkiWhen		= AvvCkidate,
		@sCkoLocationCode	= AvvCkoLocCode,
		@sCkiLocationCode	= AvvCkiLocCode,
		@sAvvCkiAmPm		= AvvCkiAmPm,
		@sAvvCkoAmPm		= AvvCkoAmPm
FROM	dbo.AvailableVehicle	WITH (NOLOCK),
	dbo.AvailableVehiclePackage	WITH (NOLOCK)
WHERE	AvpId	= @sAvpId
AND	AvvId	= AvpAvvId

--	NEED to use location and AM/PM indicator to fetch the
--	default time defined for this location (branch).
SELECT	@dLocCkiTime	= dbo.RES_GetDefaultTime(@sCkiLocationCode, @sAvvCkiAmPm, 'CKI'),
		@dLocCkoTime	= dbo.RES_GetDefaultTime(@sCkoLocationCode, @sAvvCkoAmPm, 'CKO'),
--	THEN we will need to amend the date(s) to be date-times
	@dCkiWhen = 
		CASE WHEN exists (select usrid from dbo.userinfo (nolock) where usrcode = @sUserId AND usrIsAgentuser = 1) OR exists (select usrid from dbo.userinfo (nolock) where usrcode = @sUserId AND usrIsAgentuser = 1) 
			 THEN @dCkiWhen
			 ELSE @dCkiWhen -- dbo.GEN_AddTimeToDate(@dCkiWhen, @dLocCkiTime) rev:manny
		END,
	@dCkoWhen = 
		CASE WHEN exists (select usrid from dbo.userinfo (nolock) where usrcode = @sUserId AND usrIsAgentuser = 1) OR exists (select usrid from dbo.userinfo (nolock) where usrcode = @sUserId AND usrIsAgentuser = 1) 
			 THEN @dCkoWhen
			 ELSE @dCkoWhen--dbo.GEN_AddTimeToDate(@dCkoWhen, @dLocCkoTime)rev:manny
		END

--	@dCkiWhen	= dbo.GEN_AddTimeToDate(@dCkiWhen, @dLocCkiTime),
--	@dCkoWhen	= dbo.GEN_AddTimeToDate(@dCkoWhen, @dLocCkoTime)
print @dCkoWhen
print @dCkiWhen

SELECT @sAgnid = BkrAgnId ,
	   @sPkgid	= AvpPkgid
FROM 	dbo.bookingRequest WITH (NOLOCK) , 
		dbo.VehicleRequest WITH (NOLOCK), 
		dbo.AvailableVehicle WITH (NOLOCK), 
		dbo.AvailableVehiclePackage WITH (NOLOCK)
WHERE  	BkrId = VhrBkrId
AND		VhrId = AvvVhrId
AND 	AvvId = AvpAvvId
AND 	AvpId = @sAvpId	
	
-- RSM GST CHANGES 05/10/02
--insert into zzlog(zzlogtext) values (ISNULL(@sAgnId,'NULL')  + '][' + ISNULL(@sSapId,'NULL') + '][' + ISNULL(@sPkgId,'NULL') + '][' +  convert(varchar(12),@dCkoWhen,103) + '][' +  convert(varchar(10),@dDBDateTime,103) + '][ Ravi - Calculateratesforbookingrequest')

SELECT @bIsCusCharge = dbo.RES_GetIsCusCharge(@sAgnId,@sSapId,@sPkgId,@dCkoWhen,@dDBDateTime,'Before Check-Out', null,NULL)


--	RETRIEVE first (in add order) selected vehicle row
SELECT	TOP 1
	@sSlvId	= SlvId
FROM	dbo.SelectedVehicle	WITH (NOLOCK)
WHERE	SlvAvpId	= @sAvpId
ORDER	BY	AddDateTime	

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IF	@bWriteToLogFile = @bTrueValue
BEGIN
	SELECT	@strLogMessageText = '007 Table ' + (CASE WHEN @sTableName IS NULL OR LEN(@sTableName) = 0 THEN 'NULL' ELSE CAST(@sTableName AS VARCHAR(3)) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKO Date ' + (CASE WHEN @dCkoWhen IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(19), @dCkoWhen, 120) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKO Am/Pm ' + (CASE WHEN @sAvvCkoAmPm IS NULL THEN 'NULL' WHEN LEN(@sAvvCkoAmPm) = 0 THEN '{empty}' ELSE CAST(@sAvvCkoAmPm AS VARCHAR(2)) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKO Time ' + (CASE WHEN @dLocCkoTime IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(8), @dLocCkoTime, 108) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKI Date ' + (CASE WHEN @dCkiWhen IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(19), @dCkiWhen, 120) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKI Am/Pm ' + (CASE WHEN @sAvvCkiAmPm IS NULL THEN 'NULL' WHEN LEN(@sAvvCkiAmPm) = 0 THEN '{empty}' ELSE CAST(@sAvvCkiAmPm AS VARCHAR(2)) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKI Time ' + (CASE WHEN @dLocCkiTime IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR(8), @dLocCkiTime, 108) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKO Loc ' + (CASE WHEN @sCkoLocationCode IS NULL THEN 'NULL' WHEN LEN(@sCkoLocationCode) = 0 THEN '{empty}' ELSE CAST(@sCkoLocationCode AS VARCHAR(5)) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 CKI Loc ' + (CASE WHEN @sCkiLocationCode IS NULL THEN 'NULL' WHEN LEN(@sCkiLocationCode) = 0 THEN '{empty}' ELSE CAST(@sCkiLocationCode AS VARCHAR(5)) END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, '007 ManageCalculateRates():', @bWriteToLogFile
	SELECT	@strLogMessageText = '007    Bpd ID ' + (CASE WHEN @sBpdId IS NULL THEN 'NULL' WHEN LEN(@sBpdId) = 0 THEN '{empty}' ELSE @sBpdId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007    Sap ID ' + (CASE WHEN @sSapId IS NULL THEN 'NULL' WHEN LEN(@sSapId) = 0 THEN '{empty}' ELSE @sSapId END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@sTemp = NULL
	SELECT	@sTemp = PrdShortName + '-' + CAST(SapSuffix AS VARCHAR(3))
	FROM	SaleableProduct	WITH (NOLOCK),
		Product		WITH (NOLOCK)
	WHERE	SapId	= @sSapId
	AND	PrdId	= SapPrdId
	SELECT	@strLogMessageText = '007    Sap ' + (CASE WHEN @sTemp IS NULL OR LEN(@sTemp) = 0 THEN 'NULL' ELSE @sTemp END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
END -- Debug
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
--	JUST make sure of nulls
IF	@sBpdId IS NOT NULL AND	LEN(@sBpdId) = 0
	SELECT	@sBpdId = NULL
IF	@sSapId IS NOT NULL AND	LEN(@sSapId) = 0
	SELECT	@sSapId = NULL


--	RES_ManageCalculateRates is responsible for
--	creating the [temp] charge and discount rows
--	using RES_CalculateRates.

IF EXISTS(select slvid from dbo.tProcessedBpds WITH (NOLOCK) WHERE slvid = @sAvpId and spid = @@spid)
	DELETE FROM dbo.tProcessedBpds WITH(ROWLOCK)  WHERE slvid = @sAvpId and spid = @@spid

EXECUTE	RES_ManageCalculateRates

	@sBpdId			= @sBpdId,		-- Booked Product
	@sSapId			= @sSapId,		-- Saleable Product
	@dCkoWhen		= @dCkoWhen,		-- CKO date
	@sCkoLocationCode	= @sCkoLocationCode,	-- CKO location
	@dCkiWhen		= @dCkiWhen,		-- CKI date
	@sCkiLocationCode	= @sCkiLocationCode,	-- Cki location
	@dDBTDate		= @dDBDateTime,		-- Day Before Travel date
	@sBooId			= @sBooId,		-- Booking
	@sCancelBpdId		= NULL,			-- Cancel Bpd
	@iQuantity		= NULL,			-- Quantity
	@cRate			= NULL,			-- Rate
	@sSlvId			= @sSlvId,		-- Selected Vehicle (if known)
	@sAvpId			= @sAvpId,		-- Available vehicle package (if known)
	@dPreviousCkoWhen	= NULL,			-- Previous CKO date
	@dPreviousCkiWhen	= NULL,			-- Previous CKI date
	@sPreviousSapId		= NULL,			-- Previous Sap
	@iKmTravelled		= NULL,			-- Km travelled
	@bApplyOriginalRates	= 0,			-- Apply original rates?
	@bIsCusCharge		= @bIsCusCharge, -- RSM 051002 GST CHANGES
	@sApplyPtdId		= NULL,			-- Person Type Discount (to apply)
	@sPrrIdList		= NULL,			-- Per person rate ID
	@sPrrQtyList		= NULL,			-- Per person rate Qty
	@sPrrRateList		= NULL,			-- Per person rate $rate
	@sUserId		= @sUserID,
	@sProgramName		= @sProgramName,
	@ReturnError		= @sError	OUTPUT,
	@ReturnMessage		= @sMessage	OUTPUT

--	KEEP messages from below...
IF	@sMessage IS NOT NULL
AND	LEN(@sMessage) > 0
	SELECT	@ReturnMessage	=
		CASE
			WHEN	LEN(@ReturnMessage) > 0
			THEN	@ReturnMessage + '|' + @sMessage

			ELSE	@sMessage
		END -- CASE

--	WHAT happened?
IF	@sError IS NOT NULL AND	LEN(@sError) > 0
BEGIN
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IF	@bWriteToLogFile = @bTrueValue
BEGIN
	SELECT	@strLogMessageText = '007 ** ERROR ** ** ERROR ** ** ERROR ** ** ERROR ** ** ERROR ** '
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 ManageCalculateRates().Error=[' + (CASE WHEN @sError IS NULL OR LEN(@sError) = 0 THEN 'NULL' ELSE @sError END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 ** ERROR ** ** ERROR ** ** ERROR ** ** ERROR ** ** ERROR ** '
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
END
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	--	We're out-a here!
	SELECT	@ReturnError	= @sError + 'RES_CalculateRatesForBookingRequest.MCR'
	RETURN
END

print 'hello1'

SET @sRchIdList = null
SELECT @sRchIdList = CASE WHEN @sRchIdList IS NULL THEN RchId ELSE @sRchIdList + ',' + RchId END
	FROM	dbo.RateCalc	WITH (NOLOCK),
			dbo.RateCharge	WITH (NOLOCK)
	WHERE	RccTblName	= @sTableName
	AND		RccTblId	= @sTableId
	AND		RchRccId	= RccId
	

WHILE	@sRchIdList IS NOT NULL AND	LEN(@sRchIdList) > 0
BEGIN
print 'GEN_GetNextEntryAndRemoveFromList'
	--	GET the first entry
	EXECUTE	GEN_GetNextEntryAndRemoveFromList
		@psCommaSeparatedList	= @sRchIdList,
		@psListEntry		= @sRchid OUTPUT,
		@psUpdatedList		= @sRchIdList	 OUTPUT

	--	MAINTAIN referential integrity
	IF	@sRchid IS NOT NULL AND	LEN(@sRchid) > 0
	BEGIN

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IF	@bWriteToLogFile = @bTrueValue
BEGIN
print '@bWriteToLogFile = @bTrueValue'
	SELECT	@strLogMessageText = '007 Rch.Id=[' + (CASE WHEN @sRchId IS NULL THEN 'NULL' WHEN LEN(@sRchId) = 0 THEN '{empty}' ELSE @sRchId END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Avp.Id=[' + (CASE WHEN @sAvpId IS NULL THEN 'NULL' WHEN LEN(@sAvpId) = 0 THEN '{empty}' ELSE @sAvpId END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@sTemp = NULL
	SELECT	@sTemp = AprId
	FROM	RateCharge		WITH (NOLOCK),
		AvailableVehicleProduct	WITH (NOLOCK)
	WHERE	RchId		= @sRchId
	AND	AprSapId	= RchSapId
	AND	AprCodCurrId	= RchCodCurrId
	AND	AprAvpId	= @sAvpId
	SELECT	@strLogMessageText = '007 Apr.Exists=[' + (CASE WHEN @sTemp IS NULL OR LEN(@sAvpId) = 0 THEN '** FALSE **' ELSE ' True ' END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
END
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		--	ADD new Available Vehicle Product rows
		--	    where required

		IF	NOT EXISTS
				(
				SELECT	AprId
				FROM	dbo.RateCharge		WITH (NOLOCK),
						dbo.AvailableVehicleProduct	WITH (NOLOCK)
				WHERE	RchId		= @sRchId
				AND		AprSapId	= RchSapId
				AND		AprCodCurrId= RchCodCurrId
				AND		AprAvpId	= @sAvpId
				)
		BEGIN
			--SELECT	@sAprId = NEWID()
			if exists(select null from dbo.availablevehicleproduct (nolock) where apravpid = @sAvpId)
				SELECT @saprid = @savpid + convert(varchar(3),count(aprid)) from dbo.AvailableVehicleProduct (nolock) where apravpid = @sAvpId
			else
				set @saprid = @savpid + '0'

			print 'INSERT	INTO AvailableVehicleProduct'
			INSERT	INTO
				dbo.AvailableVehicleProduct	WITH (ROWLOCK)
				(
					AprId,
					AprSapId,
					AprAvpId,
					AprCodCurrId,
					AprCodUomId,
					AprIsCusCharge,  --- P2 changes: line added					
					IntegrityNo,
					AddDateTime,
					AddUsrId,
					AddPrgmName
				)
				SELECT
					@sAprId			'AprId',
					RchSapId		'AprSapId',
					@sAvpId			'AprAvpId',
					RchCodCurrId		'AprCodCurrId',
					RchCodUomId		'AprCodUomId',
					@bIsCusCharge,  -- p2 changes: line Added
					CAST(1 AS INTEGER)	'IntegrityNo',
					CURRENT_TIMESTAMP	'AddDateTime',
					@sUserId		'AddUsrId',
					@sProgramName		'AddPrgmName'
				FROM	dbo.RateCharge	WITH (NOLOCK)
				WHERE	RchId		= @sRchId
			IF	@@Error <> 0
			BEGIN
				SET @ReturnError = ISNULL(@ReturnError, '') + CONVERT(VARCHAR, @@ERROR)
				GOTO	cleanup_and_leave
			END
		END	--	AVAILABLE VEHICLE PRODUCT ROW does not exist
		ELSE
			SELECT	@sAprId = AprId
			FROM	dbo.RateCharge		WITH (NOLOCK),
				dbo.AvailableVehicleProduct	WITH (NOLOCK)
			WHERE	RchId		= @sRchId
			AND	AprSapId		= RchSapId
			AND	AprCodCurrId	= RchCodCurrId
			AND	AprAvpId		= @sAvpId

		--	ESTABLISH guid for AVC table
		print 'not INSERT	INTO AvailableVehicleProduct'		
		
		if exists(select null from dbo.availablevehiclecharge (nolock) where avcaprid = @saprid)
			SELECT @sAvcid = @saprid + convert(varchar(3),count(avcid)) 
				from dbo.availablevehiclecharge (nolock) where avcaprid = @saprid
		ELSE
			SELECT @sAvcid = @saprid + '0'
		if @savcid is null 
			SELECT	@sAvcId	= @saprid 



	print '1@bWriteToLogFile = @bTrueValue'

	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	IF	@bWriteToLogFile = @bTrueValue
	BEGIN
	print 'inside @bWriteToLogFile = @bTrueValue'
	SELECT	@strLogMessageText = '007 AprId=[' + (CASE WHEN @sAprId IS NULL THEN 'NULL' WHEN LEN(@sAprId) = 0 THEN '{empty}' ELSE @sAprId END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@sTemp  = NULL,
		@sTemp0 = NULL,
		@sTemp1 = NULL
	SELECT	@sTemp  =
		'Avc=[' + ISNULL(@sAvcId, 'NULL') +
		'], Rch.UoM=[' + ISNULL((SELECT CodCode FROM Code WITH (nolock) WHERE CodId = RchCodUomId), '') +
		'], Apr=[' + ISNULL(@sAprId, 'NULL') +
		'], Ptb=[' + ISNULL(RchPtbId, 'NULL') +

		'], DateFrom=[' + (CASE WHEN RchFromWhen IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR, RchFromWhen, 120) END) +
		'], DateTo=[' + (CASE WHEN RchToWhen IS NULL THEN 'NULL' ELSE CONVERT(VARCHAR, RchToWhen, 120) END) +
		'], From=[' + CAST(ISNULL(RchFrom, -1) AS VARCHAR) +
		'], To=[' + CAST(ISNULL(RchTo, -1) AS VARCHAR) + ']',
		@sTemp0 =
		'Rate=[' + CAST(ISNULL(RchRate, -1) AS VARCHAR) +
		'], Qty=[' + CAST(ISNULL(RchQty, -1) AS VARCHAR) +
		'], Table=[' + ISNULL(RchBaseTableName, 'NULL') +
		'], Id=[' + ISNULL(RchBaseId, 'NULL') +
		'], BaseRate=[' + CAST(ISNULL(RchBaseRate, -1) AS VARCHAR) +
		'], CusDisc=[' + CAST(ISNULL(RchCusDiscAmt, -1) AS VARCHAR) +
		'], Gross=[' + CAST(ISNULL(RchGrossAmt, -1) AS VARCHAR) +
		'], ChgHP=[' + CAST(ISNULL(RchChgHirePeriod, -1) AS VARCHAR) +
		'], AgnDisc=[' + CAST(ISNULL(RchAgnDiscAmt, -1) AS VARCHAR) + ']',
		@sTemp1 =
		'Gst=[' + CAST(ISNULL(RchGstAmt, -1) AS VARCHAR) +
		'], Foc=[' + (CASE WHEN RchIsFoc = @bTrueValue THEN ' True ' ELSE '**FALSE**' END) +
		'], GrossOver=[' + CAST(ISNULL(RchGrossOverride, -1) AS VARCHAR) +
		'], MinChg=[' + CAST(ISNULL(RchMinCharge, -1) AS VARCHAR) +
		'], MaxChg=[' + CAST(ISNULL(RchMaxCharge, -1) AS VARCHAR) +
		'], Zero=[' + (CASE WHEN RchIsZeroCost = @bTrueValue THEN ' True ' ELSE '**FALSE**' END) +
		'], Curr=[' + ISNULL((SELECT CodCode FROM Code WITH (nolock) WHERE CodId = RchCodCurrId), '') + ']'
	FROM	dbo.RateCharge WITH (NOLOCK)
	WHERE	RchId = @sRchId
	
	
	SELECT	@strLogMessageText = '007 Avc.Insert(1)=' + (CASE WHEN @sTemp IS NULL THEN 'NULL' WHEN LEN(@sTemp) = 0 THEN '{empty}' ELSE @sTemp END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Avc.Insert(2)=' + (CASE WHEN @sTemp0 IS NULL THEN 'NULL' WHEN LEN(@sTemp0) = 0 THEN '{empty}' ELSE @sTemp0 END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	SELECT	@strLogMessageText = '007 Avc.Insert(3)=' + (CASE WHEN @sTemp1 IS NULL THEN 'NULL' WHEN LEN(@sTemp1) = 0 THEN '{empty}' ELSE @sTemp1 END)
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	END
	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
		print 'insert AvailableVehicleCharge '

		--	INSERT a new AVAILABLE VEHICLE CHARGE row
		INSERT INTO
			dbo.AvailableVehicleCharge	WITH (ROWLOCK)
			(
				AvcId,
				AvcCodUomId,
				AvcAprId,
				AvcPtbId,
				AvcFromWhen,
				AvcToWhen,
				AvcFrom,
				AvcTo,
				AvcRate,
				AvcQty,
				AvcBaseTableName,
				AvcBaseId,
				AvcBaseRate,
				AvcCusDiscAmt,
				AvcGrossAmt,
				AvcChgHirePeriod,
				AvcAgnDiscAmt,
				AvcAgnDisPerc,
				AvcGstAmt,
				AvcIsFoc,
				AvcGrossOverride,
				AvcMinCharge,
				AvcMaxCharge,
				AvcIsZeroCost,
				AvcCodCurrId,
				IntegrityNo,
				AddDateTime,
				AddUsrId,
				AddPrgmName
			)
			SELECT
				@sAvcId				'AvcId',
				RchCodUOMId			'AvcCodUOMId',
				@sAprId				'AvcAprId',
				RchPtbId			'AvcPtbId',
				RchFromWhen			'AvcFromWhen',
				RchToWhen			'AvcToWhen',
				RchFrom				'AvcFrom',
				RchTo				'AvcTo',
				ISNULL(RchRate, 0)		'AvcRate',
				ISNULL(RchQty, 0)		'AvcQty',
				RchBaseTableName		'AvcBaseTableName',
				RchBaseId			'AvcBaseId',
				ISNULL(RchBaseRate, 0)		'AvcBaseRate',
				ISNULL(RchCusDiscAmt, 0)	'AvcCusDiscAmt',
				ISNULL(RchGrossAmt, 0)		'AvcGrossAmt',
				ISNULL(RchChgHirePeriod, 0)	'AvcChgHirePeriod',
				ISNULL(RchAgnDiscAmt, 0)	'AvcAgnDiscAmt',
				ISNULL(RchAgnDisPerc, 0)	'AvcAgnDisPerc',
				ISNULL(RchGstAmt, 0)		'AvcGstAmt',
				RchIsFoc			'AvcIsFoc',
				RchGrossOverride		'AvcGrossOverride',
				RchMinCharge			'AvcMinCharge',
				RchMaxCharge			'AvcMaxCharge',
				RchIsZeroCost			'AvcIsZeroCost',
				RchCodCurrId			'AvcCodCurrId',
				CAST(1 AS INTEGER)		'IntegrityNo',
				CURRENT_TIMESTAMP		'AddDateTime',
				@sUserId			'AddUsrId',
				@sProgramName			'AddPrgmName'
			FROM	dbo.RateCharge WITH (NOLOCK)
			WHERE	RchId = @sRchId
			print @sRchId
--		select RchFromWhen,RchToWhen from dbo.RateCharge WITH (NOLOCK)
--			WHERE	RchId = @sRchId
		IF	@@Error <> 0
		BEGIN
	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	IF	@bWriteToLogFile = @bTrueValue
	BEGIN
	SELECT	@strLogMessageText = '007 Avc.New @@ERROR=[' + (CASE WHEN @@ERROR IS NULL THEN 'NULL' ELSE CAST(@@ERROR AS VARCHAR(10)) END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	END
	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			--	WHAT errors required?
	--		ROLLBACK TRANSACTION
			GOTO	cleanup_and_leave
		END
	
		--	WALK rate discounts of this rate charge
		INSERT INTO
			dbo.AvailableVehicleDiscount	WITH (ROWLOCK)
			(
				AvdId,
				AvdAvcId,
				AvdPdsId,
				AvdCodRtyId,
				AvdPtdId,
				AvdGrpDiscPerc,
				AvdDiscAmt,
				IntegrityNo,
				AddDateTime,
				AddUsrId,
				AddPrgmName
			)
			SELECT
				rdcid		'AvdId',
				@sAvcId			'AvdAvcId',
				RdcPdsId		'AvdPdsId',
				RdcCodRtyId		'AvdCodRtyId',
				RdcPtdId		'AvdPtdId',
				RdcGrpDisc		'AvdGrpDisc',
				RdcDiscAmt		'AvdDiscAmt',
				CAST(1 AS INTEGER)	'IntegrityNo',
				CURRENT_TIMESTAMP	'AddDateTime',
				@sUserId		'AddUsrId',
				@sProgramName		'AddPrgmName'
			FROM	dbo.RateDiscount WITH (NOLOCK)
			WHERE	RdcRchId = @sRchId
		IF	(@@Error <> 0)
		BEGIN
	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	IF	@bWriteToLogFile = @bTrueValue
	BEGIN
	SELECT	@strLogMessageText = '007 Avd.New @@ERROR=[' + (CASE WHEN @@ERROR IS NULL THEN 'NULL' ELSE CAST(@@ERROR AS VARCHAR(10)) END) + ']'
	EXECUTE SYS_WriteLogFileMessage	@strLogFileName, @strLogMessageText, @bWriteToLogFile
	END
	-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			--	WHAT errors required?
	--		ROLLBACK TRANSACTION
			GOTO	cleanup_and_leave
		END
	
		--	KEEP going...
	--	FETCH NEXT FROM	csrRateCharge	INTO	@sRchId
	END
END	--	WHILE {more rows to process}



GOTO	cleanup_and_leave

--	========================================
--	END PROCESSING SECTION
--	========================================

--	CLEAN up time!
cleanup_and_leave:

RETURN
--	********** End RES_CalculateRatesForBookingRequest() **********























