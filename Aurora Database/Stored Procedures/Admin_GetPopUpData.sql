CREATE PROCEDURE [dbo].[Admin_GetPopUpData]
	@case varchar(30)	=	'',
	@param1 varchar(64)	=	'',
	@param2 varchar(64)	=	'',
	@param3 varchar(500)	=	'',
	@param4 varchar(64)	=	'' ,
	@param5 varchar(64)  = '',
	@sUsrCode varchar(64) = ''
AS

IF(@case = 'ADMIN_GETUSERINFO')
BEGIN
	-- EXEC Admin_GetPopUpData 'ADMIN_GETUSERINFO', 'wv', NULL, NULL, NULL, NULL, 'wv1'

	SELECT
		UsrId	AS 'ID',
		UsrCode	AS 'CODE',
		UsrName	AS 'DESCRIPTION'
	FROM
		UserInfo AS POPUP
	WHERE
		UsrIsActive = 1
		AND (UsrCode LIKE @param1+'%' OR UsrName LIKE @param1+'%')
	FOR XML AUTO, ELEMENTS

	RETURN
END



GO
