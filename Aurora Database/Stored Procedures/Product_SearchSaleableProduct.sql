CREATE PROCEDURE [dbo].[Product_SearchSaleableProduct]
(
	@ComCode AS varchar(64),
	@PrdId AS varchar(64)
)
AS

--EXEC [Product_SearchSaleableProduct] 'THL', 'B3A026A7-7CA6-4FB5-89F0-CB0EBDF89D6B'

/*********************************************************************************************/
/* 0 - SaleableProduct */
/*********************************************************************************************/
SELECT SaleableProduct.*
FROM
	Product
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
WHERE
	Brand.BrdComCode = @ComCode
	AND Product.PrdId = @PrdId
ORDER BY
	SaleableProduct.SapSuffix DESC

/*********************************************************************************************/
/* 1 - ProductAttribute */
/*********************************************************************************************/
SELECT
	ProductAttribute.*
FROM
	Product
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN ProductAttribute ON ProductAttribute.PatSapId = SaleableProduct.SapId
	INNER JOIN Attribute ON Attribute.AttId = ProductAttribute.PatAttId
WHERE
	Brand.BrdComCode = @ComCode
	AND Product.PrdId = @PrdId
ORDER BY
	SaleableProduct.SapSuffix DESC,
	Attribute.AttDataType,
	Attribute.AttOrder,
	Attribute.AttDesc

/*********************************************************************************************/
/* 2 - Package */
/*********************************************************************************************/
SELECT DISTINCT
	Package.*
FROM
	Product
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN PackageProduct ON PackageProduct.PplSapId = SaleableProduct.SapId
	INNER JOIN Package ON Package.PkgId = PackageProduct.PplPkgId
WHERE
	Brand.BrdComCode = @ComCode
	AND Product.PrdId = @PrdId

/*********************************************************************************************/
/* 3 - PackageProduct */
/*********************************************************************************************/
SELECT
	PackageProduct.*
FROM
	Product
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN PackageProduct ON PackageProduct.PplSapId = SaleableProduct.SapId
WHERE
	Brand.BrdComCode = @ComCode
	AND Product.PrdId = @PrdId

/*********************************************************************************************/
/* Calculate the current ProductRateTravelBand */
/*********************************************************************************************/
DECLARE @Bands table (
	ComCode varchar(64),
	PrdId varchar(64),
	SapId varchar(64),
	TbsId varchar(64),
	PtbId varchar(64)
)

INSERT INTO @Bands
(
	ComCode,
	PrdId,
	SapId,
	TbsId,
	PtbId
)
SELECT
	Brand.BrdComCode AS ComCode,
	Product.PrdId,
	SaleableProduct.SapId,
	TravelBandSet.TbsId AS TbsId,
	ProductRateTravelBand.PtbId AS PtbId
FROM
	Product
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN
	(
		SELECT
			SaleableProduct.SapId,
			MAX (TravelBandSet.TbsEffDateTime) AS TbsEffDateTime
		FROM
			Product
			INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
			INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
			INNER JOIN TravelBandSet ON TravelBandSet.TbsSapId = SaleableProduct.SapId
		WHERE
			Brand.BrdComCode = @ComCode
			AND Product.PrdId = @PrdId
			AND TravelBandSet.TbsEffDateTime <= GETDATE()
		GROUP BY
			SaleableProduct.SapId
	) AS MaxTbsEffDateTime ON MaxTbsEffDateTime.SapId = SaleableProduct.SapId
	INNER JOIN TravelBandSet
		ON TravelBandSet.TbsSapId = SaleableProduct.SapId
		AND TravelBandSet.TbsEffDateTime = MaxTbsEffDateTime.TbsEffDateTime
	INNER JOIN
	(
		SELECT
			TravelBandSet.TbsId,
			MAX (ProductRateTravelBand.PtbTravelFromDate) AS PtbTravelFromDate
		FROM
			Product
			INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
			INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
			INNER JOIN TravelBandSet ON TravelBandSet.TbsSapId = SaleableProduct.SapId
			INNER JOIN ProductRateTravelBand ON ProductRateTravelBand.PtbTbsId = TravelBandSet.TbsId
		WHERE
			Brand.BrdComCode = @ComCode
			AND Product.PrdId = @PrdId
			AND ProductRateTravelBand.PtbTravelFromDate <= GETDATE()
		GROUP BY
			TravelBandSet.TbsId
	) AS MaxPtbTravelFromDate ON MaxPtbTravelFromDate.TbsId = TravelBandSet.TbsId
	INNER JOIN ProductRateTravelBand
		ON ProductRateTravelBand.PtbTbsId = TravelBandSet.TbsId
		AND ProductRateTravelBand.PtbTravelFromDate = MaxPtbTravelFromDate.PtbTravelFromDate

/*********************************************************************************************/
/* 2 - TravelBandSet */
/*********************************************************************************************/
SELECT
	TravelBandSet.*
FROM
	TravelBandSet
	INNER JOIN
	(
		SELECT DISTINCT b.TbsId FROM @Bands AS b
	) AS Bands ON Bands.TbsId = TravelBandSet.TbsId

/*********************************************************************************************/
/* 3 - ProductRateTravelBand */
/*********************************************************************************************/
SELECT
	ProductRateTravelBand.*
FROM
	ProductRateTravelBand
	INNER JOIN @Bands AS b ON b.PtbId = ProductRateTravelBand.PtbId

/*********************************************************************************************/
/* 4 - DiscountProduct (Product) */
/*********************************************************************************************/
SELECT
	Product.*
FROM
	Product
WHERE
	EXISTS
	(
		SELECT 1
		FROM
			ProductRateDiscount
			INNER JOIN @Bands AS b ON b.PtbId = ProductRateDiscount.PdsPtbId
		WHERE
			ProductRateDiscount.PdsFreeNonProduct = Product.PrdId
			OR ProductRateDiscount.PdsPrdFreeId = Product.PrdId
	)

/*********************************************************************************************/
/* 5 - ProductRateDiscount */
/*********************************************************************************************/
SELECT
	ProductRateDiscount.*
FROM
	ProductRateDiscount
	INNER JOIN Code ON Code.CodId = ProductRateDiscount.PdsCodRtyId
	INNER JOIN @Bands AS b ON b.PtbId = ProductRateDiscount.PdsPtbId
ORDER BY
	ProductRateDiscount.PdsPtbId,
	Code.CodCode,
	ProductRateDiscount.PdsQty,
	ProductRateDiscount.PdsFromDayOfWeek,
	ProductRateDiscount.PdsFromHire

/*********************************************************************************************/
/* 6 - PersonRate */
/*********************************************************************************************/
SELECT
	PersonRate.*
FROM
	PersonRate
	INNER JOIN Code ON Code.CodId = PersonRate.PrrCodPrtId
	INNER JOIN @Bands AS b ON b.PtbId = PersonRate.PrrPtbId
ORDER BY
	PersonRate.PrrPtbId,
	Code.CodCode,
	PersonRate.PrrRate

/*********************************************************************************************/
/* 7 - ProductRateBookingBand */
/*********************************************************************************************/
SELECT
	ProductRateBookingBand.*
FROM
	ProductRateBookingBand
	INNER JOIN @Bands AS b ON b.PtbId = ProductRateBookingBand.BapPtbId
ORDER BY
	ProductRateBookingBand.BapPtbId,
	ProductRateBookingBand.BapBookedFromDate

/*********************************************************************************************/
/* 8 - LocationDependentRate */
/*********************************************************************************************/
SELECT
	LocationDependentRate.*
FROM
	LocationDependentRate
	INNER JOIN ProductRateBookingBand ON ProductRateBookingBand.BapId = LocationDependentRate.LdrBapId
	INNER JOIN @Bands AS b ON b.PtbId = ProductRateBookingBand.BapPtbId
ORDER BY
	ProductRateBookingBand.BapPtbId,
	ProductRateBookingBand.BapBookedFromDate,
	ISNULL (LocationDependentRate.LdrLocFromCode, ''),
	ISNULL (LocationDependentRate.LdrLocToCode, '')

/*********************************************************************************************/
/* 9 - VehicleDependentRate */
/*********************************************************************************************/
SELECT
	VehicleDependentRate.*
FROM
	VehicleDependentRate
	LEFT JOIN Product ON Product.PrdId = VehicleDependentRate.VdrPrdId
	INNER JOIN @Bands AS b ON b.PtbId = VehicleDependentRate.VdrPtbId
ORDER BY
	VehicleDependentRate.VdrPtbId,
	ISNULL (Product.PrdShortName, '')

/*********************************************************************************************/
/* 10 - FixedRate */
/*********************************************************************************************/
SELECT
	FixedRate.*
FROM
	FixedRate
	INNER JOIN @Bands AS b ON b.PtbId = FixedRate.FfrPtbId
ORDER BY
	FixedRate.FfrPtbId

/*********************************************************************************************/




GO
