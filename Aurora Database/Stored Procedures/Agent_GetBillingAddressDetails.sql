USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Agent_GetBillingAddressDetails]    Script Date: 06/06/2008 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Agent_GetBillingAddressDetails] 
AS

/*********************************************************************************************/
/* 0 - AddressDetails */
/*********************************************************************************************/
SELECT     
	AddId, AddPrntTableName, ISNULL(AddAddress1, '') AS AddAddress1, AddCodAddressId, AddPrntID, ISNULL(AddAddress2, '') AS AddAddress2, 
	ISNULL(AddPostcode, '') AS AddPostcode, ISNULL(AddCtyCode, '') AS AddCtyCode, 
    ISNULL(AddAddress3, '') AS AddAddress3, ISNULL(AddState, '') AS AddState, AddressDetails.IntegrityNo, AddressDetails.AddUsrId, AddressDetails.ModUsrId, 
	AddressDetails.AddDateTime, AddressDetails.ModDateTime, AddressDetails.AddPrgmName
FROM         
	AddressDetails 
	INNER JOIN Code ON AddressDetails.AddCodAddressId = Code.CodId
WHERE     
	(AddressDetails.AddPrntTableName = 'Agent') 
	AND (Code.CodCdtNum = 1)
	AND (AddressDetails.AddCodAddressId =
                          (SELECT     CodId
                            FROM          Code
                            WHERE      (CodCdtNum = 1) AND (CodCode = 'Billing'))) 
ORDER BY
	AddressDetails.AddCtyCode, AddressDetails.AddAddress3

