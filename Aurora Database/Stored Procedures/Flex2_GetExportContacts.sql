USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetExportContacts]    Script Date: 11/19/2007 13:43:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Flex2_GetExportContacts]
	@ComCode varchar(64),
	@FlxId int = NULL,
	@AgnId varchar(64) = NULL,
	@UsrId varchar(64) = NULL,
	@IsActive bit = 1

AS

SELECT 
	* 
FROM
	FlexExportContactView
WHERE
	(@ComCode IS NULL OR FlexExportContactView.ComCode = @ComCode)
	AND (@FlxId IS NULL OR FlexExportContactView.FlxId = @FlxId)
	AND (@AgnId IS NULL OR FlexExportContactView.AgnId = @AgnId)
	AND (@UsrId IS NULL OR FlexExportContactView.UsrId = @UsrId)
	AND (@IsActive IS NULL OR FlexExportContactView.IsActive = @IsActive)
ORDER BY 
	CASE WHEN FlexExportContactView.AgnId IS NULL THEN 1 ELSE 0 END, -- users first, then agents
	FlexExportContactView.ComCode,
	FlexExportContactView.FlxCode,
	FlexExportContactView.Name
