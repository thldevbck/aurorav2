CREATE  PROCEDURE [dbo].[Report_BranchVehClassSummary]
	@sCtyCode		varchar(64),
	@sLocCode		varchar(64),
	@sFromDate		varchar(12),
	@sToDate		varchar(12),
	@sBrdCode		varchar(10),
	@sClaCode		varchar(64),
	@sTypCode		varchar(64),
	@sPrdShortName	varchar(64)
AS

SET NOCOUNT ON
/*
DECLARE @sCtyCode		varchar(64),
		@sLocCode		varchar(64),
		@sFromDate		varchar(12),
		@sToDate		varchar(12),
		@sBrdCode		varchar(10),
		@sClaCode		varchar(64),
		@sTypCode		varchar(64),
		@sPrdShortName	varchar(64)

SET @sCtyCode = 'NZ'
SET @sLocCode = ''
SET @sFromDate = '01/04/2007'
SET @sToDate = '02/04/2007'
SET @sBrdCode = ''
SET @sClaCode = ''
SET @sTypCode = '4W'
SET @sPrdShortName = ''
*/
-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND (Class.ClaIsVehicle = 1)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

DECLARE		@sClassCode		varchar(12)
DECLARE 	@sPrdName		varchar(30)
DECLARE 	@sBpdIdCurrent	varchar(64)
DECLARE 	@sBpdSapid		varchar(64)
DECLARE		@dRntCkoWhen    datetime
DECLARE		@sRentalId		VARCHAR(64)

DECLARE @AU_THRIFTYPRDS VARCHAR(1000)
DECLARE @AU_THRIFTYEFFDT datetime
SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

DECLARE @tCko TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 int,
	a6 varchar(54),
	a7 varchar(64)
)

IF @sClaCode = ''
	SET @sClassCode = 'All'
ELSE
	SET	@sClassCode = @sClaCode

IF @sTypCode = ''
	SET @sTypCode = 'All'

IF @sPrdShortName = ''
	SET @sPrdName = 'All'
ELSE
	SET	@sPrdName = @sPrdShortName

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME

SET @StartDate	= CONVERT(DATETIME, @sFromDate,103)
SET @EndDate	= CONVERT(DATETIME,(@sToDate + ' 23:59:59') ,103)
SET @WkDATE		= CONVERT(DATETIME,@sFromDate,103)

-- Location (AJ: KX fix)
DECLARE @Location TABLE (
	LocCode	varchar(64)
)
IF @sLocCode = ''
	BEGIN
		Insert Into @Location (LocCode)
		SELECT DISTINCT UPPER(dbo.Rental.RntCkoLocCode) AS LocCode
		FROM         dbo.Rental WITH (NOLOCK)
							INNER JOIN dbo.Location WITH (NOLOCK) ON dbo.Rental.RntCkoLocCode = dbo.Location.LocCode
							INNER JOIN dbo.TownCity WITH (NOLOCK) ON dbo.Location.LocTctCode = dbo.TownCity.TctCode
							INNER JOIN dbo.Package ON dbo.Rental.RntPkgId = dbo.Package.PkgId
		WHERE     (dbo.TownCity.TctCtyCode = @sCtyCode) AND (dbo.Package.PkgBrdCode IN
								  (SELECT     dbo.Brand.BrdCode
									FROM      dbo.UserInfo
													INNER JOIN dbo.UserCompany ON dbo.UserInfo.UsrId = dbo.UserCompany.UsrId
													INNER JOIN dbo.Brand ON dbo.UserCompany.ComCode = dbo.Brand.BrdComCode
									WHERE      (dbo.UserInfo.UsrCode = USER)))
	END
ELSE
	Insert Into @Location (LocCode) Values (@sLocCode)

-- Generate Cur_out to get check out vehicle count
IF @sLocCode  <> ''
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   Rental.RntCkoLocCode = @sLocCode
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate
ELSE
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkoLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate

OPEN Cur_out
FETCH NEXT FROM Cur_out INTO @sRentalId
WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sBpdIdCurrent = null
		SET @sBpdSapid = null
		SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
					 @sBpdSapId		 = BpdSapid ,
					 @dRntCkoWhen    = RntCkoWhen
			FROM dbo.Bookedproduct with (nolock), rental (nolock)
			WHERE Bpdrntid = @sRentalId
			and   bpdrntid = rntid
			AND Bpdprdisvehicle = 1
			and Bpdiscurr = 1
			and BpdStatus = 'KK'
			ORder BY BpdCkiWhen

		--CLASS
		IF @sClassCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock), dbo.type with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		Typid = PrdTypId
							AND		@sClaId = typclaid)
				GOTO GetNextRentalCKO
		END

		-- Type
		IF @sTypCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select Sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		PrdTypid = @sTypId)
				GOTO GetNextRentalCKO
		END

		--Brand
		IF @sBrdCode <> ''
		BEGIN
			IF NOT EXISTS(select rntid from dbo.rental with (nolock), dbo.package with (nolock)
							where 	rntid = @sRentalId
							AND		rntpkgid = Pkgid
							AND		PkgBrdCode = @sBrdCode)
				GOTO GetNextRentalCKO
		END

		-- Product
		IF @sPrdName <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	SapPrdId = @sPrdId)

				GOTO GetNextRentalCKO
		END

		-- Thrifty Check
		IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
		BEGIN
			IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
										dbo.saleableproduct (nolock),
										dbo.product (nolock),
										dbo.type (nolock),
										dbo.class (nolock)
						WHERE bpdid = @sBpdIdCurrent
						and bpdsapid = sapid
						and sapprdid = prdid
						and prdtypid = typid
						and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkoWhen >= @AU_THRIFTYEFFDT
				GOTO GetNextRentalCKO
		END

		INSERT INTO @tCko
		SELECT  UPPER(Rental.RntCkoLocCode),
				CONVERT(VARCHAR, Rntckowhen, 106),
				ISNULL(Class.ClaCode, ''),
				Type.TypCode,
				'1',
				boonum,
				Rntnum
		FROM	Product  WITH (NOLOCK) ,
               	Type  WITH (NOLOCK) ,
               	Class  WITH (NOLOCK),
               	SaleableProduct  WITH (NOLOCK) ,
               	BookedProduct  WITH (NOLOCK) ,
              	Rental  WITH (NOLOCK) ,
				booking with (nolock)
		WHERE   bpdid = @sBpdIdCurrent
		AND		bpdrntid = Rntid
		AND		rntbooid = Booid
		AND		BpdSapid = Sapid
		AND		SapPrdid = Prdid
		ANd		prdtypid = Typid
		And		TypClaId = Claid

		GetNextRentalCKO:
		FETCH NEXT FROM Cur_out INTO @sRentalId
	END
CLOSE Cur_out
DEALLOCATE cur_out

-- Total check out vehicles by class, date and location
SELECT	LTRIM(RTRIM(a1)) AS RntCkoLocCode,
		a2	AS	RntDate,
		a3	AS	Class,
		Sum(a5)	AS	ClCount
Into #CkoVehicle
FROM @tCko
Group By a1, a2, a3

-- Associate location with rental dates
DECLARE @RentalDates TABLE (
	WkDate	varchar(20),
	WkDay	varchar(10)
)

SET @WkDate = @StartDate
WHILE @WkDate < @EndDate
BEGIN
	Insert Into @RentalDates values (CONVERT(VARCHAR, @WkDate, 106), DATENAME(dw, @WkDate))
	SET @WKDate = @WKDate + 1
END

Select lo.*, rd.*
Into #RentalDates
From @Location lo
Cross Join @RentalDates rd
Order by lo.LocCode

-- Associate location, dates with check out vehicle class
DECLARE @CkoVehiclesByClass TABLE (
	LocCode	varchar(64),
	WkDate	varchar(20),
	AC		int,
	AV		int
)

DECLARE	@RntLocCode		varchar(64),
		@RntDate		varchar(64),
		@Class			varchar(64),
		@ClCount		int

DECLARE	@PreRntLocCode	varchar(64),
		@PreRntDate		varchar(64)

DECLARE Cur_tCko CURSOR FOR
   SELECT RntCkoLocCode, RntDate, Class, ClCount FROM #CkoVehicle
OPEN Cur_tCko
FETCH NEXT FROM Cur_tCko INTO @RntLocCode, @RntDate, @Class, @ClCount

If @@FETCH_STATUS = 0
	Begin
		If @Class = 'AC'
			Insert Into @CkoVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), @ClCount, '0')
		Else If @Class = 'AV'
			Insert Into @CkoVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), '0', @ClCount)
	End

WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @PreRntLocCode = @RntLocCode
		SET @PreRntDate = @RntDate
		FETCH NEXT FROM Cur_tCko INTO @RntLocCode, @RntDate, @Class, @ClCount
		If @@FETCH_STATUS = 0
			Begin
				If @PreRntLocCode = @RntLocCode And @PreRntDate = @RntDate
					Begin
						If @Class = 'AC'
							Update @CkoVehiclesByClass Set AC = @ClCount Where LocCode = @RntLocCode And WkDate = @RntDate
						Else If @Class = 'AV'
							Update @CkoVehiclesByClass Set AV = @ClCount Where LocCode = @RntLocCode And WkDate = @RntDate
					End
				Else
					Begin
						If @Class = 'AC'
							Insert Into @CkoVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), @ClCount, '0')
						Else If @Class = 'AV'
							Insert Into @CkoVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), '0', @ClCount)
					End
			End
	END

CLOSE Cur_tCko
DEALLOCATE Cur_tCko
Drop Table #CkoVehicle

-- Work out check in
DECLARE @tCki TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 int,
	a6 varchar(64),
	a7 varchar(64)
)

DECLARE	@sRentalIdIn	VARCHAR(64)
DECLARE @dRntCkiWhen    datetime
IF @sLocCode  <> ''
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   (Rental.RntCkiLocCode = @sLocCode)
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate

ELSE
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkiLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate


OPEN Cur_in
FETCH NEXT FROM Cur_in INTO @sRentalIdIn
WHILE @@FETCH_STATUS=0
BEGIN

	SET @sBpdIdCurrent = null
	SET @sBpdSapid = null
	SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
				 @sBpdSapId		 = BpdSapid,
				 @dRntCkiWhen    = RntCkiWhen,
				 @dRntCkoWhen 	 = RntCkoWhen
		FROM dbo.Bookedproduct with (nolock) , dbo.rental (nolock)
		WHERE Bpdrntid = @sRentalIdIn
		AND  bpdrntid= rntid
		AND Bpdprdisvehicle = 1
		and Bpdiscurr = 1
		and BpdStatus = 'KK'
		ORder BY BpdCkiWhen desc

	--CLASS
	IF @sClassCode <> 'ALL'
	BEGIN

		IF NOT EXISTS(select sapid from Saleableproduct with (nolock), product with (nolock), type with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	Prdid = SapPrdId
						AND		Typid = PrdTypId
						AND		@sClaId = typclaid)
			GOTO GETNEXTCKI
	END

	-- Type
	IF @sTypCode <> 'ALL'
	BEGIN

		IF NOT EXISTS(select Sapid from Saleableproduct with (nolock), product with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	Prdid = SapPrdId
						AND		PrdTypid = @sTypId)
			GOTO GETNEXTCKI
	END

	--Brand
	IF @sBrdCode <> ''
	BEGIN
		IF NOT EXISTS(select rntid from dbo.rental with (nolock), package with (nolock)
						where 	rntid = @sRentalIdIn
						AND		rntpkgid = Pkgid
						AND		PkgBrdCode = @sBrdCode)

			GOTO GETNEXTCKI
	END

	-- Product
	IF @sPrdName <> 'ALL'
	BEGIN

		IF NOT EXISTS(select sapid from Saleableproduct with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	SapPrdId = @sPrdId)

			GOTO GETNEXTCKI
	END

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @sBpdIdCurrent
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0
					AND  @dRntCkiWhen >= @AU_THRIFTYEFFDT and @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTCKI
	END


	INSERT INTO @tCki
	SELECT  UPPER(bpdCkiLocCode),
			CONVERT(VARCHAR, RntCkiWhen, 106),
			Class.ClaCode,
			Type.TypCode,
			1,
			boonum,
			rntnum
	FROM	Product  WITH (NOLOCK) ,
           	Type  WITH (NOLOCK) ,
           	Class  WITH (NOLOCK) ,
           	SaleableProduct  WITH (NOLOCK) ,
           	BookedProduct  WITH (NOLOCK),
			rental with (nolock),
			booking with (nolock)
	WHERE   bpdid = @sBpdIdCurrent
	AND		bpdsapid = Sapid
	AND		SApPrdid = Prdid
	AND		Prdtypid = Typid
	AND		Typclaid = Claid
	AND		bpdrntid = rntid
	AND		rntbooid = booid

	GETNEXTCKI:
	FETCH NEXT FROM Cur_in INTO @sRentalIdIn
END

-- Total check in vehicles by class, date and location
SELECT	LTRIM(RTRIM(a1)) AS RntCkiLocCode,
		a2	AS	RntDate,
		a3	AS	Class,
		Sum(a5)	AS	ClCount
Into #CkiVehicle
FROM @tCki
Group By a1, a2, a3

-- Associate location, dates with check in vehicle class
DECLARE @CkiVehiclesByClass TABLE (
	LocCode	varchar(64),
	WkDate	varchar(20),
	AC		int,
	AV		int
)

DECLARE Cur_tCki CURSOR FOR
   SELECT RntCkiLocCode, RntDate, Class, ClCount FROM #CkiVehicle
OPEN Cur_tCki
FETCH NEXT FROM Cur_tCki INTO @RntLocCode, @RntDate, @Class, @ClCount

If @@FETCH_STATUS = 0
	Begin
		If @Class = 'AC'
			Insert Into @CkiVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), @ClCount, '0')
		Else If @Class = 'AV'
			Insert Into @CkiVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), '0', @ClCount)
	End

WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @PreRntLocCode = @RntLocCode
		SET @PreRntDate = @RntDate
		FETCH NEXT FROM Cur_tCki INTO @RntLocCode, @RntDate, @Class, @ClCount
		If @@FETCH_STATUS = 0
			Begin
				If @PreRntLocCode = @RntLocCode And @PreRntDate = @RntDate
					Begin
						If @Class = 'AC'
							Update @CkiVehiclesByClass Set AC = @ClCount Where LocCode = @RntLocCode And WkDate = @RntDate
						Else If @Class = 'AV'
							Update @CkiVehiclesByClass Set AV = @ClCount Where LocCode = @RntLocCode And WkDate = @RntDate
					End
				Else
					Begin
						If @Class = 'AC'
							Insert Into @CkiVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), @ClCount, '0')
						Else If @Class = 'AV'
							Insert Into @CkiVehiclesByClass values (@RntLocCode, CONVERT(VARCHAR, @RntDate, 106), '0', @ClCount)
					End
			End
	END

CLOSE Cur_tCki
DEALLOCATE Cur_tCki
Drop Table #CkiVehicle

-- Associate location, date and day with check out and check in vehicle class
Select rd.*, IsNull(cko.AC, '0') as CkoAC, IsNull(cko.AV, '0') as CkoAV, IsNull(cki.AC, '0') as CkiAC, IsNull(cki.AV, '0') as CkiAV
From #RentalDates rd
	Left Join @CkoVehiclesByClass cko
		on rd.LocCode = cko.LocCode And rd.WkDate = cko.WkDate
	Left Join @CkiVehiclesByClass cki
		on rd.LocCode = cki.LocCode And rd.WkDate = cki.WkDate
Order by rd.LocCode, rd.WkDate
Drop Table #RentalDates



GO
