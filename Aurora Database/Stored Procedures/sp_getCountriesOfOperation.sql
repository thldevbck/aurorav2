CREATE PROCEDURE [dbo].[sp_getCountriesOfOperation] 
AS
BEGIN
	SET NOCOUNT ON


SELECT 
	DISTINCT '' + PkgCtyCode AS [CODE],
		     '' + CtyName AS [NAME]
	FROM  
	Package AS [COUNTRYOFOPERATION] WITH (NOLOCK)
	LEFT OUTER JOIN 
	Country WITH (NOLOCK) 
	ON CtyCode=PkgCtyCode 
	FOR XML AUTO,ELEMENTS

END