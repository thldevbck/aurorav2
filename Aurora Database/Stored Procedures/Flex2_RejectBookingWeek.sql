USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_RejectBookingWeek]    Script Date: 11/19/2007 13:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_RejectBookingWeek] 
	@FbwId int,
	@IntegrityNo tinyint,
	@UsrId varchar(64)

AS

UPDATE FlexBookingWeek SET
	FlexBookingWeek.FbwStatus = 'rejected',
	FlexBookingWeek.IntegrityNo = @IntegrityNo + 1,
	FlexBookingWeek.ModUsrId = @UsrId,
	FlexBookingWeek.ModDateTime = GETDATE()
WHERE
	FlexBookingWeek.FbwId = @FbwId
	AND FlexBookingWeek.IntegrityNo = @IntegrityNo
	AND FlexBookingWeek.FbwStatus IN ('approved', 'sent')

IF @@ROWCOUNT <= 0 RETURN

UPDATE FlexBookingWeekExport SET
	FlexBookingWeekExport.FwxStatus = 'rejected',  -- dont update the version, thats used for scheduler logic
	FlexBookingWeekExport.ModUsrId = @UsrId,
	FlexBookingWeekExport.ModDateTime = GETDATE()
WHERE
	FlexBookingWeekExport.FwxFbwId = @FbwId

SELECT 
	* 
FROM
	FlexBookingWeek
WHERE
	FbwId = @FbwId






