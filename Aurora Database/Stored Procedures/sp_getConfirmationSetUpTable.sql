set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- 	QUERY FOR SP_GETCONFIRMATIONSETUP.SQL
alter     PROCEDURE [dbo].[sp_getConfirmationSetUpTable] 
	@DefaultUser		AS	VARCHAR	 (64),
	@CtyCode			AS	VARCHAR	 (64)	= NULL
AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@CtyName		VARCHAR	 (64),
				@TodaysDate		VARCHAR	 (10)

	Declare		@RentalStatus	table (CrsId varchar(64), Status varchar(50), RntText varchar(5000))
	

	SET @TodaysDate = CONVERT(VARCHAR(10),CURRENT_TIMESTAMP,103)
	IF IsNull(@CtyCode,'')=''
		SELECT
			@CtyCode = CtyCode ,		
			@CtyName = CtyName
		FROM
			Country WITH (NOLOCK) ,
			UserInfo WITH (NOLOCK) 
		WHERE
			UsrCtyCode = CtyCode
			AND UsrCode = @DefaultUser
	ELSE
		SELECT
			@CtyName = CtyName
		FROM
			Country WITH (NOLOCK) 
		WHERE
			CtyCode = @CtyCode

	DECLARE @sComCode VARCHAR(12)
	IF @DefaultUser<>''
		SELECT @sComCode = Company.ComCode FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @DefaultUser
	ELSE
		SET @sComCode = ''
	
	SELECT  @CtyCode as 'CtyCode',
			@CtyName as 'Country'


	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'KK'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Confirmed',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId			AS	CrsId ,
				'Confirmed'		AS	Status,
				CrsText			AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'KK' 
				AND (@DefaultUser='' OR CrsComCode = @sComCode)


	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'NN'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Provisional',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId			AS	CrsId,
				'Provisional'		AS	Status,
				CrsText			AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'NN'
				AND (@DefaultUser='' OR CrsComCode = @sComCode)

	
	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'WL'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Waitlist',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId			AS	CrsId ,
				'Waitlist'		AS	Status,
				CrsText			AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'WL'
				AND (@DefaultUser='' OR CrsComCode = @sComCode)


	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'KB'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Knockback',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId			AS	CrsId ,
				'Knockback'		AS	Status,
				CrsText			AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'KB'
				AND (@DefaultUser='' OR CrsComCode = @sComCode)
	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'QN'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Quote',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId		AS	CrsId ,
				'Quote'		AS	Status,
				CrsText		AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'QN'
				AND (@DefaultUser='' OR CrsComCode = @sComCode)

	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'XX'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Cancelled (after confirmation)',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId				AS	CrsId,
				'Cancelled (after confirmation)'	AS	Status,
				CrsText				AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'XX'
				AND (@DefaultUser='' OR CrsComCode = @sComCode)

	IF NOT EXISTS(
				SELECT
					CrsId
				FROM
					ConfirmationRentalStatus WITH (NOLOCK) 
				WHERE
					CrsCtyCode=@CtyCode
						AND
					CrsRntStatus = 'XN'
					AND (@DefaultUser='' OR CrsComCode = @sComCode)
			)
		Insert into @RentalStatus
		SELECT '','Cancelled (before confirmation)',''
	ELSE
		Insert into @RentalStatus
		SELECT
				CrsId				AS	CrsId ,
				'Cancelled (before confirmation)'	AS	Status,
				CrsText				AS	RntText
		FROM
				ConfirmationRentalStatus	AS	RntSt WITH (NOLOCK) 
		WHERE
				CrsCtyCode=@CtyCode
					AND
				CrsRntStatus = 'XN'	
				AND (@DefaultUser='' OR CrsComCode = @sComCode)

	select * from @RentalStatus


--	IF NOT EXISTS (
--				SELECT 
--					NwfID
--				FROM 
--					NewsFlash WITH (NOLOCK) 
--				WHERE 
--					NwfCtyCode=@CtyCode
--						AND
--					NwfTerminationDate > = CURRENT_TIMESTAMP
--					AND (@DefaultUser='' OR NwfComCode = @sComCode)
--	)
--		
--		SELECT 	'' AS	NwfID ,
--				'' AS	Audience,
--				'' AS	NewflashText,
--				@TodaysDate AS	EffFr ,
--				'' AS	EffTo
--	ELSE
--	BEGIN
		SELECT 
			NwfID								AS	NwfID ,
			NwfAudienceType						AS	Audience,
			NwfText							AS	NewflashText,
			CONVERT(VARCHAR(10),NwfEffectiveDate,103)		AS	EffFr ,
			CONVERT(VARCHAR(10),NwfTerminationDate,103)		AS	EffTo
		FROM 
			NewsFlash		AS	NewsFlash WITH (NOLOCK) 
		WHERE 
			NwfCtyCode=@CtyCode
				AND
			NwfTerminationDate > = CURRENT_TIMESTAMP
			-- KX Changes :RKS
			AND (@DefaultUser='' OR NwfComCode = @sComCode)

		ORDER BY NwfAudienceType, NwfEffectiveDate, NwfTerminationDate
--	END

END


--sp_getConfirmationSetUpTable 'TEST'


