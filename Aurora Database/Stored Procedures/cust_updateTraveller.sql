set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go






















ALTER   PROCEDURE [dbo].[cust_updateTraveller] 
	@CusID varchar(64),
	@rntCode varchar(64),
	@trvDriver bit,
	@trvHirer bit,
	@AddUsrId varchar (64) = '',
	@ModUsrId varchar (64) = '',
	@AddPrgmName varchar(64) = '',
	--JL 2008/07/10
	--Allow to update trvPrimaryHirer
	@trvPrimaryHirer bit = null
AS

	DECLARE @sError  varchar(100)
	DECLARE @RntCtyCode varchar(8) 
	DECLARE @inumber int

	IF (NOT EXISTS(SELECT TrvCusId 
						FROM 	Traveller  WITH (NOLOCK) 
						WHERE 	TrvCusId = @CusId 
						AND 	TrvRntId = @rntCode))
		BEGIN
			INSERT INTO Traveller  WITH (ROWLOCK) ( 
						TrvRntId,
						TrvCusId ,
						TrvDriver,
						IntegrityNo,
						TrvHirer ,
						--JL 2008/07/10
						--Allow to insert trvPrimaryHirer
						TrvIsPrimaryHirer,

						AddUsrId,
						AddDateTime ,
						AddPrgmName)
					VALUES
					( 	@rntCode,
						@CusID,
						@trvDriver,
						1,
						@trvHirer,
						--JL 2008/07/10
						--Allow to update trvPrimaryHirer
						--when insert, false by default
						isnull(@trvPrimaryHirer,0),

						@AddUsrId,
						CURRENT_TIMESTAMP,
						@AddPrgmName)
					
					RETURN
		END 
	ELSE
	BEGIN
	/*
		IF NOT EXISTS(SELECT IntegrityNo FROM  Traveller
						WHERE 	TrvCusId = @CusId 
						AND 	TrvRntId = @rntCode)
		BEGIN
			SELECT @sError = smsdesc FROM messages WHERE smscode = 'GEN001'
			SELECT 'GEN001/ ' + @sError
	       	RETURN
		END
	*/
		IF EXISTS(SELECT TrvCusId 
					FROM 	Traveller  WITH (NOLOCK) 
					WHERE 	TrvCusId = @CusId 
					AND 	TrvRntId = @rntCode)
		BEGIN		
			UPDATE Traveller WITH (ROWLOCK) 
				SET	
					TrvDriver = @trvDriver,
					IntegrityNo = IntegrityNo + 1,
					TrvHirer = @trvHirer,
					ModUsrId = @ModUsrId,
					ModDateTime = CURRENT_TIMESTAMP

			WHERE 	TrvCusId= @CusId 
			AND 	TrvRntId = @rntCode	

			--JL 2008/07/10
			--Allow to update trvPrimaryHirer
			IF 	@trvPrimaryHirer is not null
			BEGIN
				UPDATE Traveller WITH (ROWLOCK) 
				SET	
					TrvIsPrimaryHirer = @trvPrimaryHirer
				WHERE 	TrvCusId = @CusId 
				AND 	TrvRntId = @rntCode	
			END
			--End JL 2008/07/10

		END
	END
















