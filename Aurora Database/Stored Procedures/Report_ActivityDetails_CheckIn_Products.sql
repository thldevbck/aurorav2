CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckIn_Products]
	@BookRentalIn		VARCHAR(64)
AS

SET NOCOUNT ON

/*
DECLARE	@BookRentalIn		VARCHAR(64)
SET		@BookRentalIn = '3256642/1'
*/

select PrdNameIn, CurrsIn, Amounts
from reports.Report_ActDetCKIProducts
where BookRentalIn = @BookRentalIn



GO
