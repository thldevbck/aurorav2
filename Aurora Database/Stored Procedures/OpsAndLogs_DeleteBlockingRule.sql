CREATE PROCEDURE [dbo].[OpsAndLogs_DeleteBlockingRule]
	@comCode AS varchar(64),
	@ctyCode AS varchar(2),
	@blrId AS varchar(64)
AS


DELETE BlockingRuleAgent
FROM
	BlockingRuleAgent
	INNER JOIN BlockingRule ON BlockingRuleAgent.BraBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRuleAgentCategory
FROM
	BlockingRuleAgentCategory
	INNER JOIN BlockingRule ON BlockingRuleAgentCategory.BacBlrId = BlockingRule.BlrID
	INNER JOIN Code ON BlockingRuleAgentCategory.BacCodAgentCtgyId = Code.CodId
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRuleAgentCountry
FROM
	BlockingRuleAgentCountry
	INNER JOIN BlockingRule ON BlockingRuleAgentCountry.BrcBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRuleBrand
FROM
	BlockingRuleBrand
	INNER JOIN BlockingRule ON BlockingRuleBrand.BlbBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRuleLocation
FROM
	BlockingRuleLocation
	INNER JOIN BlockingRule ON BlockingRuleLocation.BrlBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRulePackage
FROM
	BlockingRulePackage
	INNER JOIN BlockingRule ON BlockingRulePackage.BrpBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRuleProduct
FROM
	BlockingRuleProduct
	INNER JOIN BlockingRule ON BlockingRuleProduct.BlpBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId


DELETE BlockingRule
FROM
	BlockingRule
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND BlockingRule.BlrID = @blrId




GO
