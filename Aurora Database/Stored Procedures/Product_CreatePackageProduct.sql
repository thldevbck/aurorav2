CREATE PROCEDURE [dbo].[Product_CreatePackageProduct]
	@PplPkgId	varchar(64),
	@PplSapId	varchar(64),
	@PplCopyCreateLink varchar(12) = 'Link',
	@UsrId varchar(64),
	@PrgmName varchar(64)
AS

	EXEC Package_CreatePackageProduct @PplPkgId, @PplSapId, @PplCopyCreateLink, @UsrId, @PrgmName

GO
