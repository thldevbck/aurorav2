CREATE        PROCEDURE [dbo].[OPL_updateSubstitutionRuleProductList]
			@tXmlData		text = default
AS


SET NOCOUNT ON
-- Local Variable Declaration Starts
DECLARE	@iDoc		AS	INT,
	@NewRec		AS	BIT,
	@SubId		AS	varchar(64),
	@SubPrdId	AS	varchar(64),
	@SubIntNo	AS	int,
	@SrpId		AS	varchar(64),
	@FlmDesc	AS	varchar(64),
	@SrpStDt	AS	varchar(10),
	@SrpEnDt	AS	varchar(10),
	@SrpFxCst	AS	varchar(8),
	@SrpVrCst	AS	varchar(8),
	@SrpVsLvl	AS	varchar(64),
	@SrpAvl		AS	varchar(24),
	@SrpActive	AS	bit,
	@SrpIntNo	AS	int,
	@TctId		AS	varchar(500),
	@TctDesc	AS	varchar(500),
	@TctIntNo	AS	varchar(500),
	@Rem		AS	bit,
	@ctyCode	AS	varchar(24),
	@UserCode 	AS	varchar(64),
	@PrgmName	AS	varchar(64),
	@PrdCode	AS	VARCHAR	(200),

	@NewId		AS	varchar(64),
	@ErrStr		AS	varchar(500),
	@FlmId		AS	varchar(64),
	@TctCost	AS	varchar(500),
	@cost		AS	varchar(24),
	@halfDay	AS	varchar(24)

-- KX Changes :RKS
DECLARE	@sComCode			VARCHAR(64),
		@sFleetCompanyId	VARCHAR(256)

-- Local Variable Declaration Ends
SET @ErrStr = ''
SET @FlmId = ''
SET @TctCost = ''
SET @cost = ''
SET @halfDay = ''
SET @NewRec = 0
EXEC SP_XML_PREPAREDOCUMENT @iDoc OUTPUT, @tXmlData

IF @@Error<>0	GOTO DestroyCursor

DECLARE Cur_SubstitutionRule CURSOR SCROLL FOR
	SELECT	SubId, SubPrdId, SubIntNo, SrpId, FlmDesc, SrpStDt, SrpEnDt, SrpFxCst, SrpVrCst, SrpVsLvl, SrpAvl, SrpActive, SrpIntNo, TctId, TctDesc, TctIntNo, Rem
		FROM
			OPENXML(@iDoc, '/Root/SubRuleList/SubRulePeriod',3)
		WITH
			(
			SubId		varchar(64),
			SubPrdId	varchar(64),
			SubIntNo	int,
			SrpId		varchar(64),
			FlmDesc		varchar(64),
			SrpStDt		varchar(10),
			SrpEnDt		varchar(10),
			SrpFxCst	varchar(8),
			SrpVrCst	varchar(8),
			SrpVsLvl	varchar(64),
			SrpAvl		varchar (24),
			SrpActive	bit,
			SrpIntNo	int,
			TctId		varchar(500),
			TctDesc		varchar(500),
			TctIntNo	varchar(500),
			Rem		bit
			)


	SELECT	@UserCode 	=	UsrId,
		@PrgmName	=	PrgmName
	FROM	OPENXML(@iDoc, '/Root',3)
	WITH	(
		UsrId		VARCHAR	 (64),
		PrgmName	VARCHAR	 (64)
		)

	SELECT @sComCode = dbo.fun_getCompany
							(
								@UserCode,	-- @sUserCode	VARCHAR(64),
								NULL,		-- @sLocCode	VARCHAR(64),
								NULL,		-- @sBrdCode	VARCHAR(64),
								NULL,		-- @sPrdId		VARCHAR(64),
								NULL,		-- @sPkgId		VARCHAR(64),
								NULL,		-- @sdummy1	VARCHAR(64),
								NULL,		-- @sdummy2	VARCHAR(64),
								NULL		-- @sdummy3	VARCHAR(64)
							)

	SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK) WHERE ComCode = @sComCode

	SELECT 	@ctyCode	= code
	FROM 	OPENXML(@iDoc,'/Root/Cop',3)
	WITH 	(
		code		VARCHAR	 (24)
	      	)
OPEN 	Cur_SubstitutionRule
EXEC  SP_XML_RemoveDocument @iDoc
IF(@@ERROR<>0)	GOTO DestroyCursor

FETCH FIRST FROM Cur_SubstitutionRule INTO @SubId, @SubPrdId, @SubIntNo, @SrpId, @FlmDesc, @SrpStDt, @SrpEnDt, @SrpFxCst, @SrpVrCst, @SrpVsLvl, @SrpAvl, @SrpActive, @SrpIntNo, @TctId, @TctDesc, @TctIntNo, @Rem

SELECT
	@PrdCode = PrdShortName
FROM
	Product WITH (NOLOCK)
WHERE
	PrdId = @SubPrdId

WHILE @@FETCH_STATUS=0
BEGIN
	-- Removeing SubstitutionRule / SubstituteRulePeriod / TurnAroundCost starts
	IF( @Rem = '1')
	BEGIN
		DELETE FROM TurnAroundCost   WITH (ROWLOCK)
			WHERE TacSrpId = @SrpId
		DELETE FROM SubstituteRulePeriod  WITH (ROWLOCK)
			WHERE SrpId = @SrpId
		/*
		IF NOT EXISTS(SELECT SrpSubId FROM SubstituteRulePeriod
				WHERE SrpSubId = @SubId)
		BEGIN
			DELETE FROM SubstitutionRule
				WHERE SubId = @SubId
		END
		*/
		SET @ErrStr = 'D'
	END
	-- Removeing SubstitutionRule / SubstituteRulePeriod / TurnAroundCost Ends
	ELSE
	BEGIN

		-- Check for valid product and fleet model
		IF NOT EXISTS(SELECT PrdId FROM Product  WITH (NOLOCK)
					WHERE 	PrdId = @SubPrdId
					AND 	PrdIsActive = 1)
		BEGIN
			EXEC sp_get_Errorstring 'GEN003', 'Product', 'Product', @oparam1 = @ErrStr  OUTPUT
			SELECT 'GEN003/' + @ErrStr
			GOTO DestroyCursor
		END
		SELECT @FlmId = dbo.getFleetModelId(@FlmDesc)

		IF NOT EXISTS (SELECT FleetAssetId FROM AimsProd.dbo.FleetAsset
						WHERE FleetModelId = @FlmId AND CHARINDEX(CAST(CompanyId AS VARCHAR), @sFleetCompanyId)>0
						)
			SET @FlmId = ''

		IF (@FlmId = '')
		BEGIN
			EXEC sp_get_ErrorString  'GEN003', @FlmDesc, 'Fleet Model' ,@oparam1 = @ErrStr OUTPUT
			SELECT 'GEN003/' + @ErrStr
			GOTO DestroyCursor
		END
/*
		IF(dbo.getFleetModelId(@PrdCode)='')
		BEGIN
			EXEC sp_get_ErrorString  'GEN003', @PrdCode, 'Bookable Product' ,@oparam1 = @ErrStr OUTPUT
			SELECT 'GEN003/' + @ErrStr
			GOTO DestroyCursor
		END
*/
		-- Insert /update SubstitutionRule
		IF (@SubId <> '' )
		BEGIN
			DECLARE @IntNo	int
			SET @IntNo = -10
			/*
			-- Check for SubstituteRule
			SELECT 	@IntNo = IntegrityNo
				FROM 	SubstitutionRule
				WHERE 	SubId = @SubId
			IF(@IntNo=-10)
			BEGIN
				EXEC sp_get_Errorstring 'GEN008', '', @oparam1 = @ErrStr  OUTPUT
				SELECT 'GEN008/' + @ErrStr
				GOTO DestroyCursor
			END
			print @IntNo
			print @SubIntNo
			IF (@IntNo <> @SubIntNo)
			BEGIN
				EXEC sp_get_Errorstring 'GEN007','' , @oparam1 = @ErrStr OUTPUT
				SELECT 'GEN007/' + @ErrStr
				GOTO DestroyCursor
			END
			*/
			UPDATE SubstitutionRule  WITH (ROWLOCK)
				SET	ModUsrId	=	@UserCode,
					ModDateTime	=	CURRENT_TIMESTAMP
			WHERE	SubId = @SubId
			SET @ErrStr = 'U'
		END -- IF (@SubId <> '')

		IF (@SubId = '')
		BEGIN
			SELECT @SubId = SubId FROM SubstitutionRule WITH (NOLOCK)
				WHERE  SubPrdId		= @SubPrdId
				AND    SubCtyCode	= @ctyCode
			IF @SubId <> ''
			BEGIN
				UPDATE SubstitutionRule  WITH (ROWLOCK)
					SET	ModUsrId	=	@UserCode,
						ModDateTime	=	CURRENT_TIMESTAMP
				WHERE	SubId = @SubId
			END
			ELSE
			BEGIN
				SELECT @SubId = NEWID()
				INSERT INTO SubstitutionRule WITH (ROWLOCK)
					(
						SubId,
						SubFlmId,
						SubPrdId,
						SubCtyCode,
						IntegrityNo,
						AddUsrId,
						AddDateTime,
						AddPrgmName
					)
				VALUES(
						@SubId,
						dbo.getFleetModelId(@PrdCode),
						@SubPrdId,
						@ctyCode,
						1,
						@UserCode,
						CURRENT_TIMESTAMP,
						@PrgmName
					)
				SET @ErrStr = 'I'
			END
			SET @NewRec = 1
		END -- End for SubstitutionRule Insert

		-- Insert /update SubstituteRulePeriod
		IF (@srpId <> '')
		BEGIN
			-- Check for SubstituteRulePeriod
			SELECT 	@IntNo = IntegrityNo
				FROM 	SubstituteRulePeriod WITH (NOLOCK)
				WHERE 	SrpId = @SrpId
			IF(@IntNo=-10)
			BEGIN
				EXEC sp_get_Errorstring 'GEN008', '', @oparam1 = @ErrStr  OUTPUT
				SELECT 'GEN008/' + @ErrStr
				GOTO DestroyCursor
			END

			IF (@IntNo <> @SrpIntNo)
			BEGIN
				EXEC sp_get_Errorstring 'GEN007','' , @oparam1 = @ErrStr OUTPUT
				SELECT 'GEN007/' + @ErrStr
				GOTO DestroyCursor
			END

			IF ( CAST(@SrpEnDt AS DATETIME) < CAST(CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103) AS DATETIME) )
			BEGIN
				EXEC sp_get_Errorstring 'GEN090', 'Effective To', 'greater than current date', @oparam1 = @ErrStr OUTPUT
				SELECT 'GEN090/' + @ErrStr
				GOTO DestroyCursor
			END

			UPDATE SubstituteRulePeriod  WITH (ROWLOCK)
				SET	SrpFlmId		=	@FlmId,
					SrpStartDate		=	@SrpStDt,
					SrpEndDate		=	@SrpEnDt,
					SrpFixedCost		=	CAST(@SrpFxCst AS MONEY),
					SrpVariableDailyCost	=	CAST(@SrpVrCst AS MONEY),
					SrpIsActive		=	@SrpActive,
					SrpCodVisLvlId		=	@SrpVsLvl,
					SrpAppliesToAvailability=	@SrpAvl,
					--IntegrityNo		=	IntegrityNo + 1,
					ModUsrId		=	@UserCode,
					ModDateTime		=	CURRENT_TIMESTAMP
			WHERE	SrpId = @SrpId
			SET @ErrStr = 'U'
		END --IF (@srpId <> '')
		ELSE
		BEGIN
			SELECT @SrpId = NEWID()
			INSERT INTO SubstituteRulePeriod WITH (ROWLOCK)
				(
					SrpId,
					SrpSubId,
					SrpFlmId,
					SrpStartDate,
					SrpEndDate,
					SrpFixedCost,
					SrpVariableDailyCost,

					SrpCodVisLvlId,
					SrpAppliesToAvailability,
					IntegrityNo,
					SrpIsActive,
					AddUsrId,
					AddDateTime,
					AddPrgmName
				)
			VALUES	(
					@SrpId,
					@SubId,
					@FlmId,
					@SrpStDt,
					@SrpEnDt,
					CAST(@SrpFxCst AS MONEY),
					CAST(@SrpVrCst AS MONEY),
					@SrpVsLvl,
					@SrpAvl,
					1,
					@SrpActive,
					@UserCode,
					CURRENT_TIMESTAMP,
					@PrgmName
				)
			SET @ErrStr = 'I'
		END -- End for SubstituteRulePeriod Insert

		-- Insert into TurnAroundCost
		IF (@TctId = '' AND @TctDesc <> '')
		BEGIN
			SET @TctCost = @TctDesc
			SET @TctCost =  '<Tct><TctDesc>' +  REPLACE(@TctCost, ',', '</TctDesc><TctDesc>') +  '</TctDesc></Tct>'

			EXEC SP_XML_PREPAREDOCUMENT @iDoc OUTPUT, @TctCost
			IF @@Error<>0	GOTO DestroyCursor

			IF( (SELECT COUNT(cursor_handle) FROM master.dbo.syscursors  WITH (NOLOCK) WHERE cursor_name = 'Cur_TurnAroundCost') != 0 )
			BEGIN
				CLOSE Cur_TurnAroundCost
				DEALLOCATE Cur_TurnAroundCost
			END
			DECLARE Cur_TurnAroundCost CURSOR SCROLL FOR
				SELECT	TctDesc
					FROM OPENXML(@iDoc, '/Tct/TctDesc',3)
					WITH(
						TctDesc		varchar(64) 'text()'
					)
			OPEN 	Cur_TurnAroundCost

			EXEC  SP_XML_RemoveDocument @iDoc
			IF @@Error<>0	GOTO DestroyCursor

			DELETE FROM TurnAroundCost  WITH (ROWLOCK)
				WHERE TacSrpId = @SrpId

			FETCH FIRST FROM Cur_TurnAroundCost INTO @TctDesc
			WHILE @@FETCH_STATUS=0
			BEGIN
				SET @cost = dbo.getSplitedData(@TctDesc,'-',2)
				SET @cost = SUBSTRING(@cost,2,LEn(@cost))
				SET @halfDay = dbo.getSplitedData(@TctDesc,'-',1)
				INSERT INTO TurnAroundCost WITH (ROWLOCK)
					(
						TacId,
						TacSrpId,
						TacNumOfHalfDays,
						TacCost,
						IntegrityNo,
						AddUsrId,
						AddDateTime,
						AddPrgmName
					)
				VALUES(
						NewId(),
						@SrpId,
						CAST(@halfDay AS INT),
						CAST(@cost AS MONEY),
						1,
						@UserCode,
						CURRENT_TIMESTAMP,
						@PrgmName
					)
				SET @ErrStr = 'I'
				FETCH NEXT FROM Cur_TurnAroundCost INTO @TctDesc
			END --WHILE @@FETCH_STATUS=0
		END --IF (@TctId = '' AND @TctDesc <> '')
	END -- other than remove option

	FETCH NEXT FROM Cur_SubstitutionRule INTO @SubId, @SubPrdId, @SubIntNo, @SrpId, @FlmDesc, @SrpStDt, @SrpEnDt, @SrpFxCst, @SrpVrCst, @SrpVsLvl, @SrpAvl, @SrpActive, @SrpIntNo, @TctId, @TctDesc, @TctIntNo, @Rem
END

IF (@ErrStr = 'I')
BEGIN
	EXEC sp_get_Errorstring 'GEN045', '', @oparam1 = @ErrStr  OUTPUT
	SELECT 'GEN045/'+@ErrStr
END
ELSE IF (@ErrStr = 'D')
BEGIN
	EXEC sp_get_Errorstring 'GEN047', '', @oparam1 = @ErrStr  OUTPUT
	SELECT 'GEN047/'+@ErrStr
END
ELSE
BEGIN
	EXEC sp_get_Errorstring 'GEN046', '', @oparam1 = @ErrStr  OUTPUT
	SELECT 'GEN046/'+@ErrStr
END

GOTO DestroyCursor
DestroyCursor:

	IF( (SELECT COUNT(cursor_handle) FROM master.dbo.syscursors  WITH (NOLOCK) WHERE cursor_name = 'Cur_SubstitutionRule') != 0 )
	BEGIN
		CLOSE Cur_SubstitutionRule
		DEALLOCATE Cur_SubstitutionRule
	END
	IF( (SELECT COUNT(cursor_handle) FROM master.dbo.syscursors  WITH (NOLOCK) WHERE cursor_name = 'Cur_TurnAroundCost') != 0 )
	BEGIN
		CLOSE Cur_TurnAroundCost
		DEALLOCATE Cur_TurnAroundCost
	END

	RETURN


GO
