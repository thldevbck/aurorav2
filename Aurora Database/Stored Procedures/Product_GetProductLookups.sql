CREATE PROCEDURE [dbo].[Product_GetProductLookups]
(
	@ComCode AS varchar(64)
)
AS

/*********************************************************************************************/
/* 0 - Brand */
/*********************************************************************************************/
SELECT
	Brand.*
FROM
	Brand
WHERE
	BrdComCode = @ComCode
ORDER BY
	Brand.BrdCode

/*********************************************************************************************/
/* 1 - Feature */
/*********************************************************************************************/
SELECT
	Feature.*
FROM
	Feature
ORDER BY
	Feature.FtrOrder,
	Feature.FtrDesc

/*********************************************************************************************/
/* 2 - Type */
/*********************************************************************************************/
SELECT
	Type.*
FROM
	Type
	INNER JOIN Class
		ON Class.ClaID = Type.TypClaID
ORDER BY
	Class.ClaOrder,
	Type.TypOrder,
	Type.TypCode

GO
