CREATE PROCEDURE [dbo].[Agent_GetPopUpData]
	@case varchar(30)	=	'',
	@param1 varchar(64)	=	'',
	@param2 varchar(64)	=	'',
	@param3 varchar(500)	=	'',
	@param4 varchar(64)	=	'' ,
	@param5 varchar(64)  = '',
	@sUsrCode varchar(64) = ''
AS

IF(@case = 'MARKETCODE')
BEGIN
	-- EXEC Agent_GetPopUpData 'MARKETCODE', '', NULL, NULL, NULL, NULL, 'wv1'

	DECLARE @sortBy AS varchar(60)

	/*Get the order of display*/
	SELECT	@sortBy = CdtSortBy
	FROM CodeType
	WHERE CdtNum = 10
	IF (@sortBy = NULL)
	SET @sortBy = ''

	SELECT
		CodId 				AS 'ID',
		CodCode				AS 'CODE',
		ISNULL(CodDesc,'') 	AS 'DESCRIPTION'
	FROM
		Code
	WHERE
		Codcdtnum = 10
		AND	(CodCode LIKE @param1 + '%' OR CodDesc LIKE @param1 + '%')
		AND (CodCode not like @sUsrCode + '%' or @sUsrCode = '' or @sUsrCode is null )
		AND	CodIsActive	= 1
	ORDER BY
		CASE
			WHEN (@sortBy = 'CodDesc') THEN CAST (CodDesc AS varchar(100))
			WHEN (@sortBy = 'CodOrder') THEN CAST (CodOrder AS varchar(100))
			ELSE CAST (CodCode AS varchar(100))
		END

	FOR XML AUTO,ELEMENTS

	RETURN

END




GO
