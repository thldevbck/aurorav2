CREATE PROCEDURE [dbo].[CustomerService_DeleteCheckedProductItem]
	@ComCode AS varchar(64),
	@LivId AS varchar(64),
	@PriUnitNum AS varchar(64),
	@IsVehicle AS bit

AS

SET NOCOUNT OFF

-- get the AIMS fleet company id
DECLARE @FleetCompanyId AS varchar(64)
SELECT
	@FleetCompanyId = FleetCompanyId
FROM
	Company
WHERE
	Company.ComCode = @ComCode

DECLARE @Result AS int

IF @IsVehicle = 1
BEGIN
	DELETE CheckedProductItem
	FROM
		Location
		INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
		INNER JOIN CheckedProductItem ON LocationInventory.LivId = CheckedProductItem.PriLivId
		LEFT JOIN AimsProd.dbo.FleetAsset ON AimsProd.dbo.FleetAsset.UnitNumber = CheckedProductItem.PriUnitNum
			AND CHARINDEX (CONVERT (varchar, AimsProd.dbo.FleetAsset.CompanyId), @FleetCompanyId) > 0
	WHERE
		Location.LocComCode = @ComCode
		AND LocationInventory.LivId = @LivId
		AND (CheckedProductItem.PriUnitNum = @PriUnitNum OR ISNULL (AimsProd.dbo.FleetAsset.RegistrationNumber, '') = @PriUnitNum)
		AND CheckedProductItem.PriIsVehicle = @IsVehicle

	SET @Result = @@ROWCOUNT
END
ELSE
BEGIN
	DELETE CheckedProductItem
	FROM
		Location
		INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
		INNER JOIN CheckedProductItem ON LocationInventory.LivId = CheckedProductItem.PriLivId
		LEFT JOIN AimsProd.dbo.AI_OtherAsset ON AimsProd.dbo.AI_OtherAsset.AssetNr = CheckedProductItem.PriUnitNum
		LEFT JOIN AimsProd.dbo.FaAdditions ON AimsProd.dbo.FaAdditions.AssetId = AimsProd.dbo.AI_otherAsset.AssetId
			AND CHARINDEX (CONVERT (varchar, AimsProd.dbo.FaAdditions.CompanyId), @FleetCompanyId) > 0
	WHERE
		Location.LocComCode = @ComCode
		AND LocationInventory.LivId = @LivId
		AND (CheckedProductItem.PriUnitNum = @PriUnitNum OR ISNULL (AimsProd.dbo.AI_OtherAsset.AssetNr, '') = @PriUnitNum)
		AND CheckedProductItem.PriIsVehicle = @IsVehicle

	SET @Result = @@ROWCOUNT
END

SELECT @Result



GO
