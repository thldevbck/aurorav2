CREATE	PROCEDURE [dbo].[Report_BondOld]
	@countryCode		VARCHAR(2),
	@dateFrom			VARCHAR(20),
	@dateTo				VARCHAR(20),
	@includeImprints	bit = '1'
AS

/*
DECLARE @countryCode VARCHAR(2)
DECLARE @dateFrom VARCHAR(20)
DECLARE @dateTo VARCHAR(20)
DECLARE @includeImprints bit
SET @countryCode = 'NZ'
SET @dateFrom = '01/04/2008'
SET @dateTo = '02/04/2008'
SET @includeImprints = '1'
*/

SET @dateTo = @dateTo + ' 23:59:59'
SELECT	CONVERT(VARCHAR(12), BooNum + '/' + RntNum)		AS Booking,
		CONVERT(varchar(25), Ptmname)					AS PaymentMethod,
		CONVERT(VARCHAR(10), RntCkoWhen, 103)			As CkoDate,
		CONVERT(VARCHAR(10), RntCkiWhen, 103)			AS CkiDate,
		ISNULL(RptLocalCurrAmt,0)						AS GrossAmount,
		RntStatus 										AS Status
FROM	Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK),
		Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)
		-- KX Changes :RKS
		, Package (NOLOCK)
WHERE	PmtBooId  = BooId
AND		RntBooId  = BooId
AND	  	RptType = 'B'
AND		PmtId 	= RptPmtId
AND		RntId	= RptRntId
AND 	dbo.getcountryforlocation(RntCkoLocCode,'',1) = @countryCode
AND 	PmtDateTime BETWEEN @dateFrom AND @dateTo
AND 	PmtPtmId = PtmId
AND 	(@includeImprints = '1' or PtmName NOT LIKE '%imprint%')
-- KX Changes :RKS
AND		RntPkgId = PkgId
AND		(PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)
						INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode
						INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId
						AND UsrCode = user)
		)
ORDER BY Rntckowhen, Booking, PmtDateTime


GO
