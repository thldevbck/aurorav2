USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetEntities]    Script Date: 11/19/2007 13:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableEditor_GetEntities] 
	@Code AS varchar(64)
AS

SELECT 
	TableEditorEntity.*
FROM 
	TableEditorEntity
	INNER JOIN TableEditorFunction ON TableEditorEntity.tefId = TableEditorFunction.tefId
WHERE
	TableEditorFunction.tefCode = @Code
ORDER BY
	TableEditorEntity.teeOrder, 
	TableEditorEntity.teeId 