USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[RES_UpdateBPDSerialNumbers]    Script Date: 05/20/2008 12:25:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RES_UpdateBPDSerialNumbers]
@BPDId VARCHAR(64) = '',
@SerialNumber VARCHAR(256) = '',
@sModUser VARCHAR(64) = '',
@ProductName VARCHAR(64) = '',
@sProgramName VARCHAR(64) = ''

AS
BEGIN
	SET NOCOUNT ON
	UPDATE dbo.BookedProduct WITH (ROWLOCK)
		 SET	BpdPlannedSerialCode	=	@SerialNumber,
				IntegrityNo				= 	IntegrityNo + 1,
				ModUsrId				= 	@sModUser,
				ModDateTime				=  	CURRENT_TIMESTAMP
		 WHERE	BpdId = @BPDId

	IF @@ERROR <> 0 
	BEGIN
		SELECT 'ERROR/Unable to update Serial Number for Booked Product'
		RETURN
	END
	ELSE
	BEGIN
		-- note stuff
		DECLARE @pNteCodTypId AS VARCHAR(64)
		DECLARE @pNteCodAudTypId AS VARCHAR(64)
		DECLARE @pNteDesc AS VARCHAR(64)
		DECLARE @pBooId AS VARCHAR(64)
		DECLARE @pBooNum AS VARCHAR(64)
		DECLARE @pRntNum AS VARCHAR(64)

		SET @pNteCodTypId = dbo.getCodeId(13,'Rental')
		SET @pNteCodAudTypId = dbo.getCodeId(26,'Internal')
		SET @pNteDesc = 'Product : "' + @ProductName + '" has the Serial Number : "' + @SerialNumber + '"'

		SELECT     
			@pRntNum = dbo.Rental.RntNum, 
			@pBooNum = dbo.Booking.BooNum, 
			@pBooId = dbo.Booking.BooID
		FROM         
			dbo.BookedProduct WITH (NOLOCK) 
			INNER JOIN
			dbo.Rental WITH (NOLOCK) 
			ON 
			dbo.BookedProduct.BpdRntId = dbo.Rental.RntId 
			INNER JOIN
			dbo.Booking WITH (NOLOCK) 
			ON 
			dbo.Rental.RntBooID = dbo.Booking.BooID	
		WHERE 
			dbo.BookedProduct.BpdId = @BPDId

		Exec ManageNote
					  @pNteId                 = '',
					  @pNteBooId              = @pBooId,
					  @pNteBooNo              = @pBooNum,
					  @pNteRntNo              = @pRntNum,
					  @pNteCodTypId           = @pNteCodTypId,
					  @pNteDesc               = @pNteDesc,
					  @pNteCodAudTypId		  = @pNteCodAudTypId,
					  @pNtePriority           = 3,
					  @pNteIsActive           = 1,
					  @pIntegrityNo           = 1,
					  @pUserName              = @sModUser,
					  @pAddPrgmName           = @sProgramName

		IF @@ERROR <> 0 
		BEGIN
			SELECT 'ERROR/Unable to create Note for Booked Product Serial Number change'
			RETURN
		END
		ELSE
		BEGIN
			Select 'SUCCESS/ ' + 'GEN046 - '+ smsMsgText from Messages  WITH (NOLOCK) where smsCode= 'GEN046'
			RETURN	
		END
	END
END


