    --  QUERY FOR GEN_GETPOPUPDATA.SQL        
/*-------------------------------------------------------------------------------------------  
 If Making Changes here, please make same change in SP_GetPopUpData  
-------------------------------------------------------------------------------------------*/  
ALTER PROCEDURE [dbo].[GEN_GetPopUpData] @case VARCHAR(30) = '',  
 @param1 VARCHAR(64) = '',  
 @param2 VARCHAR(64) = '',  
 @param3 VARCHAR(500) = '',  
 @param4 VARCHAR(64) = '' ,  
 @param5 VARCHAR(64)  = '',  
 -- KX Changes :RKS  
 @sUsrCode varchar(64) = ''  
AS  
  
SET NOCOUNT ON  
declare @stable table (spkgid varchar(64))  
DECLARE @RecCount BIGINT,  @ErrStr   VARCHAR(200),  @soryBy  VARCHAR(64),  
  @start  INT,  @count   INT,   @startTmp INT,   
  @ReturnPackageIDList varchar(8000),      @ReturnPackageIDList1 VARCHAR(8000),  
  @countTmp INT,  @pkgsErr  VARCHAR(2000), @sloccode   varchar(12),  
  @slocName   varchar(24),@bLocIsActive  bit,   @iDoc  int,  
  @agnId   varchar(64),@BookedDate  datetime,  @prdId   varchar(64),  
  @coDate  DATETIME, @coLoc VARCHAR(64),   @CiLoc   VARCHAR(64),  
  @iAd  INTEGER, @iCh INTEGER,    @iIn  INTEGER,  
  @ciDate  DATETIME, @RntId VARCHAR(64)  
  
-- KX Changes :RKS  
DECLARE @sFleetCompanyId VARCHAR(64),  
  @sComCode   VARCHAR(64)  
  
-- Added : RKS 28/08 For Modify Rental Screen  
IF(@case = 'CONTACTPOPUP')  
BEGIN  
 SELECT   
  ConId         AS 'CODE',   
  ConLastName + ', ' + ConFirstName   AS 'DESCRIPTION'  
 FROM   
  Booking WITH (NOLOCK),   
  Contact WITH (NOLOCK)  
 WHERE   
  ConPrntId = BooAgnId   
  AND   
  ConPrntTableName='AGENT'   
  AND   
  BooId = @param1  
 ORDER BY ConLastName  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END -- IF(@case = 'CONTACTPOPUP')  
  
IF(@case = 'CHKILOC')  
BEGIN  
 SELECT @sComCode = dbo.fun_getCompany  
      (  
       @sUsrCode, -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       NULL,  -- @sPkgId  VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
 SELECT  
  LocCode AS 'CODE',   
  LocName AS 'DESCRIPTION'  
 FROM  
  Location WITH (NOLOCK)  
 WHERE  
  LocCode IN (SELECT DISTINCT RntCkiLocCode FROM Rental)  
  AND LocComCode = @sComCode  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
  
IF(@case = 'LocForBPD')  
BEGIN  
 -- @param1 LocationCode  
 -- @param2 Country  
 -- @param3 Product,  
 -- @param4 RentalId ( This is to get the RntBookedWhen )  
 -- @param5 Other Location  
  
 DECLARE @bIsThriftyBpd   BIT,  
   @sErrorsBpd   VARCHAR(500),  
   @sThriftyLocTypeBpd VARCHAR(500)  
  
 SELECT @param3 = PrdId  
  FROM Product WITH(NOLOCK)   
  WHERE  PrdShortName = @param3  
  OR  PrdId = @param3  
  
 SELECT LocCode AS 'CODE',   
   LocName AS 'DESCRIPTION'  
 INTO #TT_LocBpd  
 FROM    Location WITH (NOLOCK) INNER JOIN  
            TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode,  
   Code WITH (NOLOCK)  
 WHERE (LocCode LIKE @param1 + '%' OR LocName LIKE @param1 + '%')  
 AND  LocIsActive  =  1  
 AND  TownCity.TctCtyCode = @param2  
 AND  LocCodTypId = CodId  
 AND  DBO.GetCodCode(ISNULL(LocCodTypId,'')) <> 'CC' -- for Issue 381 on 09/07/2003  
 AND  DBO.GetCodCode(ISNULL(LocCodTypId,'')) <> 'HQ' -- for Issue 381 on 09/07/2003  
 -- KX Changes :RKS  
 AND (@sUsrCode = '' OR LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
            INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode  
            INNER JOIN dbo.UserInfo ON UserCompany.UsrId = UserInfo.UsrId  
            AND UsrCode = @sUsrCode)  
  )  
 IF ISNULL(@param5, '') <> '' AND ISNULL(@param3, '') <> '' AND ISNULL(@param4, '') <> ''  
 BEGIN  
  DECLARE @sTriftyCtyBpd VARCHAR(12)  
  SET @sTriftyCtyBpd = dbo.getCountryForLocation(@param5, NULL, NULL)  
  SELECT @BookedDate = RntBookedWhen  
   FROM Rental WITH(NOLOCK)  
   WHERE RntId = @param4  
  EXECUTE GEN_IsThrifty  
   @sCtyCode   =  @sTriftyCtyBpd,   
   @sPrdId    =  @param3,  
   @dCkoWhen   =  NULL,  
   @dBookedWhen  = @BookedDate,   
   @sCkoLocCode  = NULL,   
   @sCkiLocCode  = NULL,  
   @sRntNum   = NULL,  
   -- OutPut Parameters  
   @bIsThrifty   = @bIsThriftyBpd  OUTPUT,  
   @sErrors    = @sErrorsBpd   OUTPUT,  
   @sThriftyLocType = @sThriftyLocTypeBpd OUTPUT   
   
  IF @bIsThriftyBpd = 1  
  BEGIN  
   DELETE FROM #TT_LocBpd  
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode <> @sThriftyLocTypeBpd)  
  END  
  ELSE  
  BEGIN  
   DELETE FROM #TT_LocBpd  
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode = @sThriftyLocTypeBpd)  
  END  
 END -- IF ISNULL(@param3, '') <> '' AND ISNULL(@param1, '') <> ''  AND ISNULL(@param4, '') <> ''   
  
 SELECT * FROM #TT_LocBpd AS POPUP   
  ORDER BY 2  
 FOR XML AUTO,ELEMENTS  
 DROP TABLE #TT_LocBpd  
 RETURN  
END  
  
IF(@case = 'LOCATIONFORVEHICLEREQ')  
BEGIN  
 -- This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp  
  
 -- @param1 = CountryCode (CtyCode)  
 -- @param2 = LocationCode  
 -- @param3 = Product  
 -- @param4 = CheckOutDate  
 -- @param5 = Other Location  
 DECLARE @bIsThrifty   BIT,  
   @sErrors   VARCHAR(500),  
   @sThriftyLocType VARCHAR(12)  
   
 IF @param5 <> '' AND EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param5)  
  SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)  
  
 IF @param3 <> '' AND @param5 <> ''  
 BEGIN  
  DECLARE @sTriftyCty VARCHAR(12)  
  SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)  
  SET @BookedDate = CURRENT_TIMESTAMP  
  
  SELECT @param3 = PrdId FROM Product WITH(NOLOCK) WHERE PrdIsActive = 1 AND  PrdId = @param3  
  SELECT @param3 = PrdId FROM Product WITH(NOLOCK) WHERE PrdIsActive = 1 AND  PrdShortName = @param3  
  
  EXECUTE GEN_IsThrifty  
   @sCtyCode   =  @sTriftyCty,  
   @sPrdId    =  @param3,  
   @dCkoWhen   =  NULL,  
   @dBookedWhen  = @BookedDate,  
   @sCkoLocCode  = NULL,   
   @sCkiLocCode  = NULL,  
   @sRntNum   = NULL,  
   -- OutPut Parameters  
   @bIsThrifty   = @bIsThrifty   OUTPUT,  
   @sErrors    = @sErrors   OUTPUT,  
   @sThriftyLocType = @sThriftyLocType OUTPUT    
  
  declare @codId varchar(64)  
  BEGIN  
   IF @bIsThrifty = 1  
    SET @codId = dbo.getCodeId(9, 'THRIFTY')  
   ELSE   
    SET @codId = dbo.getCodeId(9, 'BRANCH')  
  
   IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK)   
      WHERE  LocCode = @param2  
      -- KX Changes :RKS  
      AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
                WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                    INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                    INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
               )  
        )  
         
        
    )  
    SELECT  LocCode AS 'CODE',  
      LocName AS 'DESCRIPTION'  
     FROM Location AS POPUP WITH (NOLOCK)  
     WHERE LocIsActive = 1   
     AND  LocCode = @param2  
     AND  LocCodTypId = @codId  
     -- KX Changes :RKS  
     AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
               WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                   INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                   INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
              )  
      )  
  
    FOR XML AUTO,ELEMENTS      
   ELSE  
    IF @param1 <> '' AND EXISTS(SELECT CtyCode FROM Country (NOLOCK) WHERE CtyCode = @param1)  
     SELECT  LocCode AS 'CODE',  
       LocName AS 'DESCRIPTION'  
      FROM Location AS POPUP WITH (NOLOCK)  
      WHERE (LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
      AND  (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
      AND  LocIsActive  =  1  
      AND  LocCodTypId = @codId  
      -- KX Changes :RKS  
      AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
                WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                    INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                    INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
               )  
      )  
  
     ORDER BY LocTctCode,LocCode  
     FOR XML AUTO,ELEMENTS  
    ELSE  
     SELECT  LocCode AS 'CODE',  
       LocName AS 'DESCRIPTION'  
      FROM Location AS POPUP WITH (NOLOCK)  
      WHERE (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
      AND  LocIsActive  =  1  
      AND  LocCodTypId = @codId  
     -- KX Changes :RKS  
      AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
                WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                    INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                    INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
               )  
      )  
  
     ORDER BY LocCode  
     FOR XML AUTO,ELEMENTS  
  END  
 END -- IF @param3 <> '' AND @param5 <> ''  
 ELSE  
 BEGIN  
  IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK)   
     WHERE LocIsActive = 1   
     AND  LocCode = @param2)  
   SELECT  LocCode AS 'CODE',  
     LocName AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE LocIsActive = 1   
    AND  LocCode = @param2  
    -- KX Changes :RKS  
    AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
              WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                  INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
             )  
     )  
  
   FOR XML AUTO,ELEMENTS   
  ELSE  
   SELECT  LocCode AS 'CODE',  
     LocName AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
    AND  LocIsActive  =  1  
    -- KX Changes :RKS  
    AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
              WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                  INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
             )  
    )  
  
   ORDER BY LocCode  
  FOR XML AUTO,ELEMENTS  
 END  
 RETURN   
END  
  
IF(@case = 'LOCATIONFORVEHICLEREQ1')  
BEGIN  
 -- This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp  
 -- @param1 = CountryCode (CtyCode)  
 -- @param2 = LocationCode  
 -- @param3 = Product  
 -- @param4 = CheckOutDate  
 -- @param5 = Other Location  
/*  
 DECLARE @bIsThrifty   BIT,  
   @sErrors   VARCHAR(500),  
   @sThriftyLocType VARCHAR(12)  
*/   
 SELECT @param3 = PrdId  
  FROM Product WITH(NOLOCK)   
  WHERE  PrdShortName = @param3  
  OR  PrdId = @param3  
   
 IF @param5 <> '' AND EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param5)  
  SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)  
  
 SELECT LocCode   AS 'CODE',  
   LocName  AS 'DESCRIPTION'  
 INTO #TT_LocVhr  
 FROM Location AS POPUP WITH (NOLOCK)  
 WHERE 1=2  
  
 IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param2)  
  INSERT INTO #TT_LocVhr  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE (@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
    AND  LocCode = @param2  
 ELSE  
  INSERT INTO #TT_LocVhr  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE (@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
    AND  (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
    AND  LocIsActive  =  1  
    AND  DBO.GetCodCode(LocCodTypId) <> 'CC'  
    AND  DBO.GetCodCode(LocCodTypId) <> 'HQ'  
  
 IF @param3 <> '' AND @param5 <> ''  
 BEGIN  
  --DECLARE @sTriftyCty VARCHAR(12)  
  SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)  
  SET @BookedDate = CURRENT_TIMESTAMP  
  EXECUTE GEN_IsThrifty  
   @sCtyCode   =  @sTriftyCty,  
   @sPrdId    =  @param3,  
   @dCkoWhen   =  NULL,  
   @dBookedWhen  = @BookedDate,  
   @sCkoLocCode  = NULL,   
   @sCkiLocCode  = NULL,  
   @sRntNum   = NULL,  
   -- OutPut Parameters  
   @bIsThrifty   = @bIsThrifty   OUTPUT,  
   @sErrors    = @sErrors   OUTPUT,  
   @sThriftyLocType = @sThriftyLocType OUTPUT    
  
  IF @bIsThrifty = 1  
  BEGIN  
   DELETE FROM #TT_LocVhr   
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode <> @sThriftyLocType)  
  END  
  ELSE  
  BEGIN  
   DELETE FROM #TT_LocVhr   
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode = @sThriftyLocType)  
  END  
 END  
   
 SELECT * FROM #TT_LocVhr AS POPUP   
  ORDER BY 1  
  FOR XML AUTO,ELEMENTS  
 DROP TABLE #TT_LocVhr  
 RETURN  
END  
/*  
IF(@case = 'LOCATIONFORVEHICLEREQ2')  
BEGIN  
 -- This ia called only from VehicleRequestMgt.asp and VehicleRequestResultList.asp  
 -- @param1 = CountryCode (CtyCode)  
 -- @param2 = LocationCode  
 -- @param3 = Product  
 -- @param4 = CheckOutDate  
 -- @param5 = Other Location  
 DECLARE @bIsThrifty   BIT,  
   @sErrors   VARCHAR(500),  
   @sThriftyLocType VARCHAR(12)  
   
 IF EXISTS(select prdid from dbo.product (nolock) where PrdShortName = @param3)  
  SELECT @param3 = PrdId FROM dbo.Product WITH(NOLOCK)  WHERE PrdShortName = @param3  
 ELSE  
  SELECT @param3 = PrdId FROM dbo.Product WITH(NOLOCK)  WHERE PrdId = @param3  
  
   
 IF @param5 <> '' AND EXISTS(SELECT LocCode FROM dbo.Location WITH(NOLOCK) WHERE LocCode = @param5)  
  SET @param1 = dbo.getCountryForLocation(@param5, NULL, NULL)  
  
 IF @Param1 is not null and @Param1 = 'NZ'  
 BEGIN  
  IF exists( select loccode From dbo.location (nolock) where loccode = @param2)  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
   FROM dbo.Location AS POPUP WITH (NOLOCK)  
   WHERE LocCode = @param2  
   FOR XML AUTO,ELEMENTS  
  ELSE  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
   FROM dbo.Location AS POPUP WITH (NOLOCK)  
   WHERE (LocCode like @param2 + '%'  
   OR    LocName like @param2 + '%')  
   AND   Exists (select codid from dbo.code (nolock) where Codid = LocCodTypid and CodCode = 'Branch')  
   FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
   
 IF (@Param5 is null OR @Param5 = '')   
  AND @param2 is not null and @param2 <> ''  
 BEGIN  
  IF EXISTS(SELECT LocCode FROM dbo.Location WITH(NOLOCK) WHERE LocCode = @param2)  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
   FROM dbo.Location AS POPUP WITH (NOLOCK)  
   WHERE LocCode = @param2  
   FOR XML AUTO,ELEMENTS  
  ELSE  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
   FROM dbo.Location AS POPUP WITH (NOLOCK)  
   WHERE (LocCode like @param2 + '%'  
   OR    LocName like @param2 + '%')  
   AND   Exists (select codid from dbo.code (nolock) where Codid = LocCodTypid and CodCode in ('Branch','Thrifty'))  
   FOR XML AUTO,ELEMENTS  
  return  
 END    
  
 SELECT LocCode   AS 'CODE',  
   LocName  AS 'DESCRIPTION'  
 INTO #TT_LocVhr  
  
 FROM Location AS POPUP WITH (NOLOCK)  
 WHERE 1=2  
  
 IF EXISTS(SELECT LocCode FROM Location WITH(NOLOCK) WHERE LocCode = @param2)  
  INSERT INTO #TT_LocVhr  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE (@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
    AND  LocCode = @param2  
 ELSE  
  INSERT INTO #TT_LocVhr  
   SELECT LocCode   AS 'CODE',  
     LocName  AS 'DESCRIPTION'  
    FROM Location AS POPUP WITH (NOLOCK)  
    WHERE (@param1 = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
    AND  (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
    AND  LocIsActive  =  1  
    AND  DBO.GetCodCode(LocCodTypId) <> 'CC'  
    AND  DBO.GetCodCode(LocCodTypId) <> 'HQ'  
  
 IF @param3 <> '' AND @param5 <> ''  
 BEGIN  
  DECLARE @sTriftyCty VARCHAR(12)  
  SET @sTriftyCty = dbo.getCountryForLocation(@param5, NULL, NULL)  
  SET @BookedDate = CURRENT_TIMESTAMP  
  EXECUTE GEN_IsThrifty  
   @sCtyCode   =  @sTriftyCty,  
   @sPrdId    =  @param3,  
   @dCkoWhen   =  NULL,  
   @dBookedWhen  = @BookedDate,  
   @sCkoLocCode  = NULL,   
   @sCkiLocCode  = NULL,  
   @sRntNum   = NULL,  
   -- OutPut Parameters  
   @bIsThrifty   = @bIsThrifty   OUTPUT,  
   @sErrors    = @sErrors   OUTPUT,  
   @sThriftyLocType = @sThriftyLocType OUTPUT    
  
  IF @bIsThrifty = 1  
  BEGIN  
   DELETE FROM #TT_LocVhr   
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode <> @sThriftyLocType)  
  END  
  ELSE  
  BEGIN  
   DELETE FROM #TT_LocVhr   
    WHERE CODE IN (SELECT LocCode  
         FROM Location WITH(NOLOCK), Code WITH(NOLOCK)  
         WHERE LocCodTypId = CodId  
         AND  CodCode = @sThriftyLocType)  
  END  
 END  
   
 SELECT * FROM #TT_LocVhr AS POPUP   
  
  ORDER BY 1  
  FOR XML AUTO,ELEMENTS  
 DROP TABLE #TT_LocVhr  
 RETURN  
END  
*/  
/*  
IF(@case = 'LocForBPD')  
BEGIN  
 IF EXISTS(select loccode from dbo.location (nolock) where loccode = @param1 AND LocIsActive  =  1)  
 BEGIN  
  SELECT  
   LocCode AS 'CODE',   
   LocName AS 'DESCRIPTION'  
  FROM           
    Location WITH (NOLOCK) INNER JOIN  
             TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode  
    inner Join Code (nolock) on Codid = LocCodtypid   
  WHERE LocCode  =  @param1   
  AND   LocIsActive  =  1  
  AND CodCode in ('Branch','Thrifty')  
  AND   TownCity.TctCtyCode = @param2  
  FOR XML AUTO, ELEMENTS  
  RETURN  
 END  
 ELSE BEGIN   
  SELECT  
   LocCode AS 'CODE',   
   LocName AS 'DESCRIPTION'  
  FROM           
    Location WITH (NOLOCK) INNER JOIN  
             TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode  
    inner Join Code (nolock) on Codid = LocCodtypid   
  WHERE  
   (LocCode  LIKE @param1 + '%' OR  
       LocName  LIKE @param1 + '%' )  
   AND  LocIsActive  =  1  
   AND CodCode in ('Branch','Thrifty')  
   AND  TownCity.TctCtyCode = @param2  
  FOR XML AUTO, ELEMENTS  
  RETURN  
 END  
END*/  
  
IF(@case = 'checkLocation')  
BEGIN  
 DECLARE @iCount INT  
 SELECT  @iCount = COUNT(integrityNo)   
  FROM  Location WITH(NOLOCK)   
  WHERE  LocCode = @param1   
  AND  DBO.GetCodCode(ISNULL(LocCodTypId,'')) IN ('HQ','CC')  
  -- KX Changes :RKS  
  AND (@sUsrCode = '' OR LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
           INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
           INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId  
           AND UsrCode = @sUsrCode))  
 IF @iCount > 0  
  SELECT '<isHQCC>1</isHQCC>'  
 ELSE  
  SELECT '<isHQCC>0</isHQCC>'  
 RETURN   
END  
  
IF(@case = 'CHKOLOC')  
BEGIN  
 SELECT @sComCode = dbo.fun_getCompany  
      (  
       @sUsrCode, -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       NULL,  -- @sPkgId  VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
  
 SELECT  
  LocCode AS 'CODE',   
  LocName AS 'DESCRIPTION'  
 FROM  
  Location WITH (NOLOCK)  
 WHERE  
  EXISTS (select codid from Code WITH (NOLOCK) where Codcdtnum = 9 and CodCode = 'branch' and CodId = LocCodTypId)  
  AND LocComCode = @sComCode  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF(@case = 'NEWAGENTGROUP')  
BEGIN  
 /* Time beeing it is in issue to akl*/  
 SELECT AgpCode AS 'CODE',  
  AgpDesc AS 'DESCRIPTION'  
  
 FROM AgentGroup WITH (NOLOCK)  
 WHERE AgpCode LIKE @param1 + '%'  
 union   
 SELECT AgpCode AS 'CODE',  
  AgpDesc AS 'DESCRIPTION'  
 FROM AgentGroup WITH (NOLOCK)  
  
 WHERE   
  AgpDesc  LIKE @param1 + '%'    
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case='GETDATAFORUSER')  
BEGIN  
 SELECT @sComCode = dbo.fun_getCompany  
      (  
       @sUsrCode, -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       NULL,  -- @sPkgId  VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
  
 SELECT  
  UsrCode  AS  'DESCRIPTION',  
  UsrName  AS  'CODE'  
 FROM  
  UserInfo WITH (NOLOCK),  
  dbo.UserCompany (NOLOCK)  
 WHERE  
  UserInfo.UsrId = UserCompany.UsrId  
   AND  
  UsrIsActive=1    
   AND  
   (UsrCode LIKE @param1+'%' OR UsrName LIKE @param1+'%')  
  AND (@sUsrCode = '' OR UserCompany.ComCode  = @sComCode)  
    
 --UNION  
 --SELECT  
 -- UsrCode  AS  CODE,  
 -- UsrName  AS  'DESCRIPTION'  
 --FROM  
 -- UserInfo WITH (NOLOCK)  
 --WHERE  
 -- UsrIsActive=1    
 --  AND  
 -- (UsrName LIKE @param1+'%')  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
  
IF(@case = 'SALEABLEPRODUCT')  
BEGIN  
 DECLARE @varCtyCode  VARCHAR(24)  
 DECLARE @sPkgBrdCode VARCHAR(5)  
 SELECT  @varCtyCode = pkgctyCode,   
  @sPkgBrdCode= pkgBrdCode   
  FROM package WITH (NOLOCK)   
  WHERE pkgid= @param3  
  
 IF(@param1 <> '' AND @param2 <> '')  
 BEGIN  
  SELECT  PrdName      AS ProductName,  
   PrdShortName + ' - ' + CAST (SapSuffix AS varchar(3)) AS ShortSuffix  
  FROM Product  WITH (NOLOCK), Saleableproduct WITH (NOLOCK)  
   WHERE PrdShortName = @param1  
   AND  CAST (SapSuffix AS varchar(3)) = @param2   
   AND SapPrdId =  PrdId  
   AND SapCtyCode = @varCtyCode  
   AND (PrdBrdCode = @sPkgBrdCode OR  
        1 IN (SELECT BrdIsGeneric FROM Brand WITH (NOLOCK)  
           WHERE BrdCode = PrdBrdCode  
                )   
       )  
   AND SapStatus = 'Active'  
   -- KX Changes : RKS  
   AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
             WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
            )  
    )  
  FOR XML AUTO,ELEMENTS  
  
 END  
 ELSE  
 BEGIN  
  SELECT PrdName      AS ProductName,  
   PrdShortName + ' - ' + CAST (SapSuffix AS varchar(3))  AS ShortSuffix  
  FROM Product WITH (NOLOCK) ,Saleableproduct WITH (NOLOCK)  
   WHERE PrdShortName LIKE  @param1 + '%'  
   AND SapPrdId =  PrdId  
   AND SapCtyCode = @varCtyCode  
   AND (PrdBrdCode = @sPkgBrdCode OR  
        1 IN (SELECT BrdIsGeneric FROM Brand WITH (NOLOCK)  
           WHERE BrdCode = PrdBrdCode  
                )   
       )  
   AND SapStatus = 'Active'  
   -- KX Changes : RKS  
   AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
             WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
            )  
    )  
  FOR XML AUTO,ELEMENTS  
    
 END  
 RETURN  
END  
  
/* Class PopUp Retrive ClassCode,ClassDescription */  
  
IF(@case = 'CLASS')  
BEGIN  
 SELECT ClaCode AS 'CODE',  
   ClaDesc AS 'DESCRIPTION'  
  FROM  Class AS POPUP  WITH (NOLOCK)  
  WHERE ClaCode LIKE @param1+'%'  
  AND  ClaIsActive = 1  
  FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'PACKAGE')  
BEGIN  
 SELECT   
   PkgId  AS 'ID',  
   PkgCode AS 'CODE',  
   PkgName AS 'DESCRIPTION'  
 FROM    
   Package AS POPUP WITH (NOLOCK)  
 WHERE   
   (PkgCode LIKE @param1+'%'  
  OR PkgName LIKE @param1+'%')  
  AND PkgIsActive = 'Active'  
  AND ISNULL(PkgCodTypId,'') LIKE ISNULL(@param2, '')+'%'  
  AND PkgCode > @param3  
  -- KX Changes : RKS  
  AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
            WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
           )  
   )  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
---------------------------------------------------------------------------------  
--Modified for AgentPackageMgt.asp. 26/09/2002 by James Liu  
IF(@case = 'PACKAGE_AGENT_PACKAGETYPE')  
BEGIN  
 SELECT PkgId  AS 'ID',  
   PkgCode AS 'CODE',  
   PkgName AS 'DESCRIPTION'  
 FROM  Package AS POPUP WITH (NOLOCK)  
 WHERE (PkgCode LIKE @param1+'%' OR PkgName LIKE @param1+'%' OR @param1 = '')  
 AND  PkgIsActive = 'Active'  
 AND  (isnull(PkgCodTypId,'') LIKE @param2+'%' OR @param2 = '')  
 --AND  CONVERT(CHAR(10),PkgBookedToDate,103) >= CONVERT(CHAR(10),CURRENT_TIMESTAMP,103)  
 -- KX Changes : RKS  
 AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
           WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
          )  
  )  
 ORDER BY PkgCode  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
-----------------------------------------------------------------------------------  
  
IF(@case = 'PACKAGEPRODUCT')  
BEGIN  
 DECLARE @sPkgBrd VARCHAR(10)  
 DECLARE @sPkgCty VARCHAR(24)  
   
 SELECT  @sPkgBrd = PkgBrdCode, @sPkgCty = PkgCtyCode  
  FROM  Package WITH (NOLOCK)  
  WHERE  PkgId = @param1  
   
 SELECT PkgCode AS 'CODE',  
  PkgName AS 'DESCRIPTION'  
  FROM  Package AS POPUP WITH (NOLOCK)  
  WHERE PkgId   <>  @param1  
  AND (PkgCode  LIKE @param2 + '%'  
  OR  PkgName  LIKE @param2 + '%')  
  AND PkgBrdCode  =  @sPkgBrd  
  AND PkgCtyCode  =  @sPkgCty  
  AND PkgIsActive = 'Active'  
  -- KX Changes :RKS  
  AND PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)   
       INNER JOIN dbo.Company (NOLOCK) ON BrdComCode = ComCode  
       INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
       INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
       AND UsrCode = @sUsrCode)  
  FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'POPUPCOUNTRY')  
BEGIN  
 IF EXISTS (SELECT  CtyCode   
    FROM  Country WITH (NOLOCK)   
    WHERE  CtyCode    = @param1  
    AND  CtyIsActive = 1)  
 BEGIN  
  SELECT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
   FROM  Country WITH (NOLOCK)   
   WHERE  CtyCode    = @param1  
   AND CtyIsActive = 1  
  FOR XML AUTO,ELEMENTS   
 END  
 ELSE  
 BEGIN  
  SELECT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
   FROM  Country WITH (NOLOCK)   
   WHERE  (CtyCode LIKE @param1+'%')  
   AND CtyIsActive = 1  
  UNION  
  SELECT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
   FROM  Country WITH (NOLOCK)   
   WHERE  (CtyName LIKE @param1+'%')  
   AND CtyIsActive = 1   
  ORDER BY CtyName  -- JPL issue # 324  
  FOR XML AUTO,ELEMENTS   
 END  
 RETURN  
END  
  
IF(@case = 'GETCOUNTRY_NEW')  
BEGIN   
 SELECT DISTINCT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country AS POPUP WITH (NOLOCK)  
  WHERE (CtyCode  = @param1 OR CtyName = @param1)  
  AND CtyIsActive  = 1   
  ORDER BY CtyName  
  FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'AGNENTCOUNTRY')  
BEGIN  
 SELECT DISTINCT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country AS POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)  
  WHERE CtyCode LIKE @param1+'%'   
  AND AddCtyCode IS NOT NULL  
  AND CtyCode = AddCtyCode  
  AND AddPrntTableName = 'Agent'  
  AND CtyIsActive  = 1  
 UNION  
 SELECT DISTINCT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country AS POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)  
  WHERE CtyName  LIKE @param1+'%'  
  AND AddCtyCode IS NOT NULL  
  AND CtyCode = AddCtyCode  
  AND AddPrntTableName = 'Agent'  
  AND CtyIsActive  = 1   
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'CUSTOMERCOUNTRY')  
BEGIN  
 SELECT DISTINCT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country AS POPUP WITH (NOLOCK), Addressdetails WITH (NOLOCK)  
  
  WHERE(CtyCode LIKE @param1+'%'  
  OR CtyName  LIKE @param1+'%')  
  AND AddCtyCode IS NOT NULL  
  AND CtyCode = AddCtyCode  
  AND AddPrntTableName = 'Customer'  
  AND CtyIsActive  = 1  
  ORDER BY CtyCode  
  FOR XML AUTO,ELEMENTS   
 RETURN  
  
END  
  
IF(@case = 'AGENT')  
BEGIN  
 IF NOT EXISTS(select agncode from dbo.Agent (nolock) where Agncode = @param1)  
 begin  
  SELECT @RecCount = COUNT(agnid) FROM Agent AS POPUP WITH (NOLOCK) WHERE (AgnCode LIKE @param1+'%') AND AgnIsActive = 1  
  SELECT @RecCount  = @RecCount +  COUNT(agnid) FROM Agent AS POPUP WITH (NOLOCK) WHERE (AgnName LIKE @param1+'%') AND AgnIsActive = 1  
  
  IF @RecCount>200   
  BEGIN  
   SET @param1 = '"' + @param1 + '"'  
          Exec sp_get_ErrorString 'GEN022','Search Criteria', @param1, @oparam1 = @ErrStr OUTPUT  
       SELECT '<Error><ErrStatus>GEN022</ErrStatus><ErrCode>GEN022</ErrCode><ErrDesc>' + @ErrStr + '</ErrDesc></Error>'  
              RETURN  
  END  
  
    END  
       
  
 IF EXISTS(SELECT agnid FROM AGENT WITH (NOLOCK) WHERE AgnCode = @param1 AND AgnIsActive = 0)  
 BEGIN  
  SET @param1 = '"' + @param1 + '"'  
        SELECT '<Error><ErrStatus></ErrStatus><ErrCode></ErrCode><ErrDesc>This Agent is inactive. Bookings may not be made for this Agent.</ErrDesc></Error>'  
        RETURN  
 END  
    
 IF EXISTS(SELECT agnid FROM AGENT WITH (NOLOCK) WHERE AgnCode = @param1)  
  SELECT DISTINCT    
    AgnId  AS 'ID',  
    AgnCode  AS 'CODE',  
    AgnName   AS 'DESCRIPTION',  
    AgnIsMisc AS 'ISMISC'  
  FROM Agent AS POPUP WITH (NOLOCK)  
  WHERE  AgnCode = @param1  
  AND  AgnIsActive = 1  
  FOR XML AUTO,ELEMENTS   
 ELSE  
  SELECT DISTINCT    
    AgnId  AS 'ID',  
    AgnCode  AS 'CODE',  
    AgnName   AS 'DESCRIPTION',  
    AgnIsMisc AS 'ISMISC'  
  FROM Agent AS POPUP WITH (NOLOCK)  
  WHERE  (AgnCode LIKE @param1+'%'  
   OR AgnName  LIKE @param1+'%')  
  AND AgnIsActive = 1  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'AGENTGROUP')  
BEGIN  
 SELECT DISTINCT AgpCode AS 'CODE',  
   AgpDesc AS 'DESCRIPTION'  
  FROM AgentGroup AS POPUP WITH (NOLOCK)  
  WHERE  AgpCode LIKE @param1+'%'  
  OR AgpDesc LIKE @param1+'%'  
  ORDER BY 1  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'AGNENTSUBGROUP')  
BEGIN  
 SELECT DISTINCT  ''+AgpCode AS 'GrpCode',  
    ''+AgpDesc AS 'GrpDesc',  
    ''+AsgCode  AS 'SubGrpCode',  
    ''+AsgDesc AS 'SubGrpDesc'  
  FROM AgentGroup AS POPUP WITH (NOLOCK), AgentSubgroup WITH (NOLOCK)  
  WHERE (AgpCode LIKE @param1+'%'  
  OR   AgpDesc LIKE @param1+'%')  
  AND   AgpId = AsgAgpId  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'SUBGROUP')  
BEGIN  
 SELECT DISTINCT ''+AsgId AS 'ID',  
   ''+AgpCode AS 'GrpCode',  
   ''+AgpDesc AS 'GrpDesc',  
   ''+AsgCode  AS 'SubGrpCode',  
   ''+AsgDesc AS 'SubGrpDesc'  
  FROM AgentGroup AS POPUP WITH (NOLOCK), AgentSubgroup WITH (NOLOCK)  
  WHERE (AgpCode LIKE @param1+'%'  
  OR   AgpDesc LIKE @param1+'%')  
  AND   AgpId = AsgAgpId  
  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'AGENTFORSUBGROUP')  
BEGIN  
 SELECT DISTINCT ''+AgnId AS 'ID',  
   ''+AgnCode AS 'CODE',  
   ''+AgnName AS 'DESCRIPTION'  
  FROM Agent AS POPUP WITH (NOLOCK)  
  WHERE  (  AgnCode LIKE @param1+'%'  
   OR AgnName LIKE @param1+'%')  
  AND AgnAtyId LIKE @param2+'%'  
  AND AgnAsgId LIKE @param3+'%'  
  AND AgnCode > @param4  
  ORDER BY 2  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'AGENTPACKAGE4BOOKINGREQ')  
BEGIN  
 IF (@param3 <> '')  
 BEGIN   
  
  declare @sPkgidCurrent varchar(64)  
  SET @ReturnPackageIDList = ''  
  --SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)  
  SET @BookedDate = Current_TimeSTAMP  
  SELECT  @agnId = dbo.getSplitedData(@param3, ',', 1),  
   @prdId = dbo.getSplitedData(@param3, ',', 2),  
   @coDate = dbo.getSplitedData(@param3, ',', 3),  
   @coLoc = dbo.getSplitedData(@param3, ',', 4),  
   @CiLoc = dbo.getSplitedData(@param3, ',', 5),  
   @iAd = dbo.getSplitedData(@param3, ',', 6),  
   @iCh = dbo.getSplitedData(@param3, ',', 7),  
   @iIn = dbo.getSplitedData(@param3, ',', 8),  
   @ciDate = dbo.getSplitedData(@param3, ',', 9)  
  
  IF (@coDate = '') SET @coDate = NULL  
  IF (@ciDate = '')  SET @ciDate = NULL  
  IF (@coLoc = '') SET @coLoc = NULL  
  IF (@CiLoc = '') SET @CiLoc = NULL  
  IF (@iAd = '')  SET @iAd = NULL  
  IF (@iCh = '')  SET @iCh = NULL  
  IF (@iIn = '')  SET @iIn = NULL  
  
  
-- MTS 20JUN02 : Added "named parameters" after my changes  
  
--   to RES_getValidPackages() made this call fail.  
  --select @agnId, @BookedDate, @prdId, @coDate, @ciDate, @coLoc, @CiLoc, @iAd, @iCh, @iIn  
  EXECUTE RES_getValidPackages  
   @AgentID    = @agnId,  
   @BookedDate    = @BookedDate,  
   @ProductID    = @prdId,  
   @checkOutDate   = @coDate,  
   @checkInDate   = @ciDate,  
   @CheckOutLocationCode = @coLoc,  
   @CheckInLocationCode = @CiLoc,  
   @NumberOfAdults   = @iAd,  
   @NumberOfChildren  = @iCh,  
   @NumberOfInfants  = @iIn,  
   @VehicleRequestID  = NULL,  
   @BlockingRuleIDList  = NULL,  
   -- KX Changes :RKS  
   @sUsrCode    = @sUsrCode,  
   @ReturnPackageIDList = @ReturnPackageIDList OUTPUT  
  
-- MTS 06AUG02 : removed  
--   @psErrors    = @pkgsErr OUTPUT  
  
  IF ISNULL(@pkgsErr, '') <> '' AND ISNULL(@ReturnPackageIDList, '') = ''  
  BEGIN  
   SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @pkgsErr + '</ErrDescription></Error>'     
  END  
  ELSE IF ISNULL(@pkgsErr, '') = '' AND ISNULL(@ReturnPackageIDList, '') = ''  
  BEGIN  
   SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'  
  END  
  ELSE  
  BEGIN  
     
--   EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @ReturnPackageIDList  
--   IF (@@ERROR <> 0 ) RETURN  
  SELECT @ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '<Data><PackageID>', ''),  
  -- 2. strip the trailing elements off  
   @ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '</PackageID></Data>', ''),  
  -- 3. Change the intermediate ones to a COMMA  
   @ReturnPackageIDList = REPLACE(@ReturnPackageIDList, '</PackageID><PackageID>', ',')  
  WHILE @ReturnPackageIDList IS NOT NULL  
  AND LEN(@ReturnPackageIDList) > 0  
  BEGIN  
   SET @sPkgidCurrent = null  
   EXECUTE GEN_GetNextEntryAndRemoveFromList  
     @psCommaSeparatedList = @ReturnPackageIDList,  
     @psListEntry  = @sPkgidCurrent OUTPUT,  
     @psUpdatedList  = @ReturnPackageIDList OUTPUT  
      
  
   IF @sPkgidCurrent is not null and @sPkgidCurrent <> ''  
    insert into @stable values (@sPkgidCurrent)  
  END  
  
   IF EXISTS(SELECT pkgid  
      FROM Package POPUP WITH (NOLOCK) , @stable  
      WHERE POPUP.PkgId = spkgid  
      AND PkgCode LIKE @param2 + '%'  
      )  
   BEGIN  
    SELECT PkgId AS 'ID',  
      PkgCode AS 'CODE',  
      PkgName AS 'DESCRIPTION'  
    FROM Package POPUP WITH (NOLOCK), @stable  
    WHERE POPUP.PkgId = spkgid  
    AND PkgCode LIKE @param2 + '%'  
    FOR XML AUTO,ELEMENTS    
   END  
   ELSE  
   BEGIN  
    SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'  
   END  
--   EXECUTE sp_xml_removedocument @iDoc  
--   IF(@@ERROR <> 0 ) RETURN  
  END  
 END  
 ELSE  
 BEGIN  
  SELECT PkgId AS 'ID',  
    PkgCode AS 'CODE',  
    PkgName AS 'DESCRIPTION'  
  FROM  AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId   WHERE ApkAgnId = (SELECT AgnId FROM Agent WITH (NOLOCK)   
        WHERE AgnCode = dbo.getSplitedData(@param1,'-',1))  
  AND  PkgCode LIKE @param2+'%'  
  AND  PkgCtyCode LIKE @param4+'%'  
  FOR XML AUTO,ELEMENTS  
 END  
 RETURN  
  
END  
  
IF(@case = 'AGENTPACKAGE')  
BEGIN  
   
 IF (@param3 <> '')  
 BEGIN   
  SET @ReturnPackageIDList = ''  
  DECLARE  @sPrdIdList    VARCHAR  (8000) ,  
     @sPrdIdCurrent   VARCHAR  (64)  
       
  --SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)  
  SET @BookedDate = Current_TimeSTAMP  
  SELECT  @agnId  = dbo.getSplitedData(@param3, ',', 1),  
    @prdId  = dbo.getSplitedData(@param3, ',', 2),  
    @coDate = dbo.getSplitedData(@param3, ',', 3),  
    @coLoc  = dbo.getSplitedData(@param3, ',', 4),  
    @CiLoc  = dbo.getSplitedData(@param3, ',', 5),  
    @iAd  = dbo.getSplitedData(@param3, ',', 6),  
    @iCh  = dbo.getSplitedData(@param3, ',', 7),  
    @iIn  = dbo.getSplitedData(@param3, ',', 8),  
    @ciDate = dbo.getSplitedData(@param3, ',', 9),  
    @RntId  = dbo.getSplitedData(@param3, ',', 10)  
  
  IF (@coDate = '') SET @coDate = NULL  
  IF (@ciDate = '')  SET @ciDate = NULL  
  IF (@coLoc = '') SET @coLoc = NULL  
  IF (@CiLoc = '') SET @CiLoc = NULL  
  IF (@iAd = '')  SET @iAd = NULL  
  IF (@iCh = '')  SET @iCh = NULL  
  IF (@iIn = '')  SET @iIn = NULL  
  
  SELECT @BookedDate = RntBookedWhen  
   FROM dbo.Rental WITH(NOLOCK)  
   WHERE RntId = @RntId  
    
  IF @param2<>'' AND (SELECT COUNT(PkgCode) FROM dbo.Package WITH(NOLOCK)   
        WHERE  Pkgcode like @param2 + '%'   
        AND  @coDate between PkgTravelFromdate and pkgtraveltodate  
        AND  PkgIsActive = 'Active'  
        -- KX Changes :RKS  
        AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
                  INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode  
                  INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
                  INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
                  AND UsrCode = @sUsrCode)  
         )  
        ) = 1  
  BEGIN  
     
   SELECT  @param1 = PkgId,   
     @param2  = pkgCode,   
     @param3 = PkgName   
    FROM dbo.Package WITH(NOLOCK)  
    WHERE  PkgCode like @param2 + '%'   
    AND  @coDate between PkgTravelFromdate and pkgtraveltodate  
    AND  PkgIsActive = 'Active'  
   EXEC RES_checkPackage  
    @PackageID    = @param1 ,  
    @AgentID    = @agnId ,  
    @RentalID    = NULL ,  
    @CheckOutLocationCode = @coLoc ,  
    @CheckInLocationCode = @CiLoc ,  
    @CheckOutDate   = @coDate ,  
    @CheckInDate   = @ciDate ,  
    @ProductID    = @prdId ,  
    @SaleableProductID  = NULL,  
    @BookedDate    = @BookedDate,  
    @NumberOfAdults   = @iAd ,  
    @NumberOfChildren  = @iCh ,  
    @NumberOfInfants  = @iIn ,  
    @ReturnError   = @ErrStr OUTPUT,  
    @ReturnWarning   = NULL  
     
   IF ISNULL(@ErrStr,'')<>''  
    SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @ErrStr + '</ErrDescription></Error>'     
   ELSE  
    SELECT '<POPUP><ID>' + @param1 + '</ID><CODE>' + @param2 + '</CODE><DESCRIPTION>' + @param3 + '</DESCRIPTION></POPUP>'  
   RETURN  
  END -- IF @param2<>'' AND (SELECT COUNT(pkgcode) FROM dbo.Package (NOLOCK)   
  
    
  DECLARE @prdtypid varchar(64)  
  SELECT @prdtypid = PrdTypId  
   FROM dbo.Product WITH(NOLOCK)   
   WHERE PrdId = @prdId  
  SELECT @sPrdIdList = ISNULL(@sPrdIdList + ',', '') + PrdId   
   FROM dbo.Product WITH(NOLOCK)  
   WHERE PrdTypId = @prdtypid  
   -- KX Changes :RKS  
            AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
             INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode  
             INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
             INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
             AND UsrCode = @sUsrCode)  
    )  
  SET @start = 1  
  SET @count = CASE  
     WHEN @sPrdIdList IS NOT NULL AND @sPrdIdList <> ''   
      THEN dbo.getEntry(@sPrdIdList, ',') + 1  
      ELSE 0  
     END  
  -- 2. START an iterative loop  
  WHILE @start <= @count  
  BEGIN    
   SET @sPrdIdCurrent = dbo.getSplitedData(@sPrdIdList, ',', @start)  
   SET @start = @start + 1  
   IF @sPrdIdCurrent <> ''  
   BEGIN  
    SET @ReturnPackageIDList1 = NULL  
    EXECUTE RES_getValidPackages  
     @AgentID    = @agnId,  
     @BookedDate    = @BookedDate,  
     @ProductID    = @sPrdIdCurrent ,  --@prdId,  
     @checkOutDate   = @coDate,  
     @checkInDate   = @ciDate,  
     @CheckOutLocationCode = @coLoc,  
     @CheckInLocationCode = @CiLoc,  
     @NumberOfAdults   = @iAd,  
     @NumberOfChildren  = @iCh,  
     @NumberOfInfants  = @iIn,  
     @VehicleRequestID  = NULL,  
     @BlockingRuleIDList  = NULL,  
     @ReturnPackageIDList = @ReturnPackageIDList1 OUTPUT  
    SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '<Data><PackageID>', '')  
    SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '</PackageID></Data>', '')  
    SET @ReturnPackageIDList1 = REPLACE(@ReturnPackageIDList1, '</PackageID><PackageID>', ',')  
    IF ISNULL(@ReturnPackageIDList1, '')<>''  
    BEGIN  
     IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''  
      SET @ReturnPackageIDList = @ReturnPackageIDList1  
     ELSE  
     BEGIN  
      SET @startTmp = 1  
      SET @countTmp = CASE  
         WHEN @ReturnPackageIDList1 IS NOT NULL AND @ReturnPackageIDList1 <> ''   
          THEN dbo.getEntry(@ReturnPackageIDList1, ',') + 1  
          ELSE 0  
         END       
      WHILE @startTmp <= @countTmp  
      BEGIN  
       SET @sPrdIdCurrent = dbo.getSplitedData(@ReturnPackageIDList1, ',', @startTmp)  
       SET @startTmp = @startTmp + 1  
       IF CHARINDEX(@sPrdIdCurrent, @ReturnPackageIDList) = 0   
        SET @ReturnPackageIDList =  @ReturnPackageIDList + ',' + @sPrdIdCurrent  
      END -- WHILE @startTmp <= @countTmp  
     END -- IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''  
    END -- IF ISNULL(@ReturnPackageIDList1, '')<>''  
   END -- IF @sPrdIdCurrent <> ''  
  END -- WHILE @start <= @count  
  IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''  
   SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'  
  ELSE  
  BEGIN  
   SET @start = 1  
   SET @count = CASE  
      WHEN @ReturnPackageIDList IS NOT NULL AND @ReturnPackageIDList <> ''   
       THEN dbo.getEntry(@ReturnPackageIDList, ',') + 1  
       ELSE 0  
      END  
   WHILE @start <= @count  
   BEGIN  
    SET @sPrdIdCurrent = dbo.getSplitedData(@ReturnPackageIDList, ',', @start)  
    SET @start = @start + 1  
    SELECT PkgId AS 'ID',  
      PkgCode AS 'CODE',  
      PkgName AS 'DESCRIPTION'  
     FROM Package POPUP WITH (NOLOCK)  
     WHERE PkgId = @sPrdIdCurrent  
     AND PkgCode LIKE @param2 + '%'  
     -- KX Changes :RKS  
                    AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
               INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode  
               INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
               INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
               AND UsrCode = @sUsrCode)  
      )  
    FOR XML AUTO,ELEMENTS   
   END -- WHILE @start <= @count  
  END -- else IF @ReturnPackageIDList IS NULL OR @ReturnPackageIDList = ''  
 END -- IF (@param3 <> '')  
 ELSE  
 BEGIN  
  SET @param1 = dbo.getSplitedData(@param1,'-',1)  
  SELECT PkgId AS 'ID',  
    PkgCode AS 'CODE',  
    PkgName AS 'DESCRIPTION'  
  FROM  AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId  
  WHERE ApkAgnId = (SELECT AgnId FROM dbo.Agent WITH (NOLOCK)   
        WHERE AgnCode = @param1)  
  AND  PkgCode LIKE @param2+'%'  
  AND  (PkgCtyCode LIKE @param4+'%' OR ISNULL(@param4,'') = '')  
  -- KX Changes :RKS  
        AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
            INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = Brand.BrdComCode  
            INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
            INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
            AND UsrCode = @sUsrCode)  
   )  
  FOR XML AUTO,ELEMENTS  
 END  
 RETURN  
END  
  
  
IF(@case = 'AGENTPACKAGE4BooMgt')  
BEGIN  
 IF (@param3 <> '')  
 BEGIN   
  SET @ReturnPackageIDList = ''  
  --SET @BookedDate = CONVERT(varchar(10), Current_TimeSTAMP, 103)  
  DECLARE  @cNonRevB4XchgBpdIds  VARCHAR  (8000) ,  
     @sLastVehBpdId     VARCHAR  (64) ,  
     @sRntId      VARCHAR  (64) ,  
     @sUsrId      VARCHAR  (64)  
    
  SET @BookedDate = Current_TimeSTAMP  
  SELECT  @agnId = dbo.getSplitedData(@param3, ',', 1),  
    @prdId = dbo.getSplitedData(@param3, ',', 2),  
    @coDate = dbo.getSplitedData(@param3, ',', 3),  
    @coLoc = dbo.getSplitedData(@param3, ',', 4),  
    @CiLoc = dbo.getSplitedData(@param3, ',', 5),  
    @iAd = dbo.getSplitedData(@param3, ',', 6),  
    @iCh = dbo.getSplitedData(@param3, ',', 7),  
    @iIn = dbo.getSplitedData(@param3, ',', 8),  
    @ciDate = dbo.getSplitedData(@param3, ',', 9) ,  
    @sRntId = dbo.getSplitedData(@param3, ',', 10) ,  
    @sUsrId = dbo.getSplitedData(@param3, ',', 11)  
  
  SET @BookedDate = dbo.GEN_getAdjustedTimeForUser(@sUsrId,@BookedDate)  
  
  SELECT @coDate = RntCkoWhen, @ciDate = RntCkiWhen FROM Rental (NOLOCK) WHERE RntId = @sRntId  
    
  Exec RES_GetExchangeBpds  
    @sRntId     = @sRntId ,  
    @sBpdIdList    = NULL ,  
    @cNonRevB4XchgBpdIds = @cNonRevB4XchgBpdIds OUTPUT,  
    @cRevB4XchgBpdIds  = NULL ,  
    @cRntAgrList   = NULL ,  
    @cNonRevB4XchgAmt  = NULL ,  
    @cRevB4XchgAmt   = NULL ,  
    @cXfrmAmt    = NULL ,  
    @cXtoAmt    = NULL  
    
  IF @cNonRevB4XchgBpdIds IS NULL  
  BEGIN  
   SELECT TOP 1 @prdId = SapPrdId   
   FROM BookedProduct (NOLOCK), SaleableProduct (NOLOCK)  
   WHERE BpdRntId = @sRntId AND BpdSapId  = SapId AND BpdPrdIsVehicle = 1 AND BpdIsCurr = 1  
   ORDER BY BpdCkoWhen DESC    
  END  
  ELSE  
  BEGIN  
   SET @sLastVehBpdId  =  dbo.getSplitedData (@cNonRevB4XchgBpdIds, ',' , dbo.getEntry(@cNonRevB4XchgBpdIds, ',') + 1)  
   SELECT @prdId = SapPrdId   
   FROM BookedProduct (NOLOCK), SaleableProduct (NOLOCK)  
   WHERE BpdSapId = SapId AND BpdId = @sLastVehBpdId     
  END  
    
     
  /*select @prdId, @BookedDate, @coDate '@coDate', @ciDate as '@ciDate', @coLoc '@coLoc', @CiLoc '@CiLoc' ,  
    @iAd '@iAd', @iCh '@iCh', @iIn '@iIn'*/  
  IF (@coDate = '') SET @coDate = NULL  
  IF (@ciDate = '')  SET @ciDate = NULL  
  IF (@coLoc = '') SET @coLoc = NULL  
  IF (@CiLoc = '') SET @CiLoc = NULL  
  IF (@iAd = '')  SET @iAd = NULL  
  IF (@iCh = '')  SET @iCh = NULL  
  IF (@iIn = '')  SET @iIn = NULL  
  
    
-- MTS 20JUN02 : Added "named parameters" after my changes  
  
--   to RES_getValidPackages() made this call fail.  
  
  EXECUTE RES_getValidPackages  
   @AgentID    = @agnId,  
   @BookedDate    = @BookedDate,  
   @ProductID    = @prdId,  
   @checkOutDate   = @coDate,  
   @checkInDate   = @ciDate,  
  
   @CheckOutLocationCode = @coLoc,  
   @CheckInLocationCode = @CiLoc,  
   @NumberOfAdults   = @iAd,  
   @NumberOfChildren  = @iCh,  
   @NumberOfInfants  = @iIn,  
   @VehicleRequestID  = NULL,  
   @BlockingRuleIDList  = NULL,  
   @ReturnPackageIDList = @ReturnPackageIDList OUTPUT  
-- MTS 06AUG02 : removed  
--   @psErrors    = @pkgsErr OUTPUT  
  
  IF ISNULL(@pkgsErr, '') <> '' AND ISNULL(@ReturnPackageIDList, '') = ''  
  BEGIN  
   SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @pkgsErr + '</ErrDescription></Error>'     
  END  
  ELSE IF ISNULL(@pkgsErr, '') = '' AND ISNULL(@ReturnPackageIDList, '') = ''  
  BEGIN  
   SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'  
  END  
  ELSE  
  BEGIN  
   EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @ReturnPackageIDList  
   IF (@@ERROR <> 0 ) RETURN  
   IF EXISTS(SELECT pkgid  
      FROM Package POPUP WITH (NOLOCK)  
      WHERE PkgId IN ( SELECT PackageID FROM  
           OPENXML (@iDoc, '/Data/PackageID', 3)  
           WITH ( PackageID  VARCHAR(64) 'text()')  
          )  
      AND PkgCode LIKE @param2 + '%'  
      )  
   BEGIN  
    SELECT PkgId AS 'ID',  
      PkgCode AS 'CODE',  
      PkgName AS 'DESCRIPTION'  
    FROM Package POPUP WITH (NOLOCK)  
    WHERE PkgId IN ( SELECT PackageID FROM  
         OPENXML (@iDoc, '/Data/PackageID', 3)  
         WITH ( PackageID  VARCHAR(64) 'text()')  
        )  
    AND PkgCode LIKE @param2 + '%'  
    FOR XML AUTO,ELEMENTS    
   END  
   ELSE  
   BEGIN  
    SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>0</ErrNumber><ErrType>Application</ErrType><ErrDescription>No Records Found</ErrDescription></Error>'  
   END  
   EXECUTE sp_xml_removedocument @iDoc  
   IF(@@ERROR <> 0 ) RETURN  
  END  
 END  
 ELSE  
 BEGIN  
  SELECT PkgId AS 'ID',  
    PkgCode AS 'CODE',  
    PkgName AS 'DESCRIPTION'  
  FROM  AgentPackage WITH (NOLOCK) RIGHT OUTER JOIN Package AS POPUP WITH (NOLOCK) ON ApkPkgId = PkgId   WHERE ApkAgnId = (SELECT AgnId FROM Agent WITH (NOLOCK)   
        WHERE AgnCode = dbo.getSplitedData(@param1,'-',1))  
  AND  PkgCode LIKE @param2+'%'  
  AND  PkgCtyCode LIKE @param4+'%'  
  FOR XML AUTO,ELEMENTS  
 END  
 RETURN  
END  
  
   
IF(@case = 'BookedProduct')  
BEGIN  
 SELECT   
  DISTINCT Product.PrdShortName AS CODE, Product.PrdName AS DESCRIPTION  
  
 FROM   
         SaleableProduct WITH (NOLOCK) INNER JOIN  
                      Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN  
                      BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId  
 WHERE  (PrdShortName  LIKE @param1+'%'  
  OR  PrdName LIKE @param1+'%')  
 AND BpdRntId = @param2  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF(@case = 'PRODUCT')  
BEGIN  
 IF (SELECT COUNT(PrdId) FROM Product WITH (NOLOCK), dbo.Brand (NOLOCK)  
   WHERE PrdBrdCode = BrdCode AND (PrdShortName = @param1  
   OR  PrdName = @param1)  
   -- KX Changes :RKS  
   AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
          WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))  
   AND  PrdIsActive = 1) = 1  
  SELECT  PrdId   AS 'ID',  
     PrdShortName AS 'CODE',  
     PrdName   AS 'DESCRIPTION'  
   FROM Product AS POPUP WITH (NOLOCK), dbo.Brand (NOLOCK)  
   WHERE PrdBrdCode = BrdCode AND (PrdShortName = @param1  
   OR  PrdName = @param1)  
   -- KX Changes :RKS  
   AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
          WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))  
   AND  PrdIsActive = 1  
   FOR XML AUTO,ELEMENTS   
 ELSE  
  SELECT  PrdId   AS 'ID',  
     PrdShortName AS 'CODE',  
     PrdName   AS 'DESCRIPTION'  
   FROM Product AS POPUP WITH (NOLOCK), dbo.Brand (NOLOCK)  
   WHERE  PrdBrdCode = BrdCode AND (PrdShortName  LIKE @param1+'%'  
   OR   PrdName LIKE @param1+'%')  
   -- KX Changes :RKS  
   AND (@sUsrCode='' OR BrdComCode IN (SELECT ComCode FROM dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
          WHERE  UserCompany.UsrId = UserInfo.UsrId AND UserInfo.UsrCode = @sUsrCode))  
   AND  PrdIsActive = 1  
   FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
-- for issue 429  
IF(@case = 'Product4BookingSearch')  
BEGIN  
 SELECT  PrdShortName AS 'CODE',  
      PrdName  AS 'DESCRIPTION'  
  FROM  Product AS POPUP WITH (NOLOCK)  
  WHERE  (PrdShortName  LIKE @param1+'%' OR PrdName LIKE @param1+'%')  
  -- KX Changes  
  AND (  
     @sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) INNER JOIN dbo.Company (NOLOCK)  
           ON Brand.BrdComCode = Company.ComCode INNER JOIN dbo.UserCompany (NOLOCK)  
           ON Company.ComCode = UserCompany.ComCode INNER JOIN dbo.UserInfo (NOLOCK)  
           ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode  
           )  
 )  
    AND PrdIsActive = 1  
	--Added by Shoel for Enh : Prod Expiry
	AND NOT EXISTS(SELECT 1 FROM NonAvailabilityProfile WITH (NOLOCK)
					WHERE NapPrdId = PrdId AND CURRENT_TIMESTAMP BETWEEN NapBookedFromDate AND NapBookedToDate)
	--Added by Shoel for Enh : Prod Expiry
 FOR XML AUTO,ELEMENTS   
 RETURN  
END  
-- end for issue 429  
  
IF(@case = 'PRODUCT4BPD')  
BEGIN   
 declare  @dBpdCkoWhen  datetime ,  
    @sUvValue   varchar  (64) ,  
    @sRntPkgId   varchar  (64)  
  
 select  @dBpdCkoWhen = BpdCkoWhen ,   
   @sRntId   = BpdRntId  
 from BookedProduct (nolock) where BpdId = @param3  
  
 select @sRntPkgId = RntPkgId from Rental with (nolock) where RntId = @sRntId  
  
 set @sUvValue = 0  
 select @sUvValue = UviValue from dbo.UniversalInfo (nolock) where UviKey  = @param2 + 'MinCheckDays'  
   
  
 SELECT DISTINCT  
  PrdId   AS 'ID',  
   PrdShortName AS 'CODE',  
  '' + BrdName  AS 'BRAND',   
   PrdName    AS 'DESCRIPTION' --,  
--  '' + CAST(SapSuffix AS VARCHAR) AS 'Suffix'  
 FROM  
  Product AS POPUP WITH (NOLOCK) INNER JOIN  
        Brand WITH (NOLOCK) ON POPUP.PrdBrdCode = Brand.BrdCode   
 WHERE exists (select sapid   
     from saleableproduct WITH (NOLOCK),   
     PackageProduct with (nolock) ,  
     Package with (nolock)  
  
     where pplPkgId = @sRntPkgId and sapprdid = popup.prdid   
     and SapStatus = 'Active'   
     AND SaleableProduct.SapCtyCode = @param2 and SapIsBase=1  
     and PplpkgId  = PkgId  
     --and SapId = pplSapId  
     and (PkgTravelToDate + cast(@sUvValue as int))>=@dBpdCkoWhen  
     and cast(convert(varchar(10),current_timestamp,103) as datetime)<=(PkgTravelToDate + cast(@sUvValue as int))  
     )  
 AND  (PrdShortName  LIKE @param1+'%'  
   OR  PrdName LIKE @param1+'%')  
 AND PrdIsActive = 1  
 AND  PrdIsVehicle = @param4     
 -- KX Changes :RKS  
 AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
            INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode  
            INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
            INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId  
            AND UsrCode = @sUsrCode  
           )  
 )  
 FOR XML AUTO, ELEMENTS   
 RETURN  
END  
  
IF(@case = 'PRODUCTVEHICLE')  
BEGIN  
 SELECT PrdId  AS 'Id',  
   PrdShortName AS 'ShortName',  
   PrdName  AS 'Name'  
  FROM Product AS PRODUCT WITH (NOLOCK)  
  WHERE  (PrdShortName  LIKE @param1+'%'  
  OR  PrdName LIKE @param1+'%')  
  AND PrdIsActive = 1  
  AND PrdIsVehicle = @param2  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF (@case = 'BookedProductType')  
BEGIN  
SELECT   
 DISTINCT   
 Type.TypCode  AS 'CODE',   
 Type.TypDesc  AS 'DESCRIPTION'  
FROM           
 Product WITH (NOLOCK) INNER JOIN  
             Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId  
WHERE  
 (Type.TypCode  LIKE @param1+'%'  
 OR  Type.TypDesc  LIKE @param1+'%')  
 AND  
 PrdId IN (  
  SELECT   
   DISTINCT   
   Product.PrdId  
  FROM   
    SaleableProduct WITH (NOLOCK) INNER JOIN  
         Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN  
         BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId   
  -- KX Changes :RKS  
  WHERE BpdRntId = @param2  
 )  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF (@case = 'BookedProductClass')  
BEGIN  
 SELECT    
  DISTINCT Class.ClaCode  AS 'CODE',   
  Class.ClaDesc    AS  'DESCRIPTION'  
 FROM           
  Product WITH (NOLOCK) INNER JOIN  
               Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN  
      Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID  
 WHERE  
  (Class.ClaCode  LIKE @param1+'%'  
  OR  Class.ClaDesc LIKE @param1+'%')  
  AND  
  PrdId IN (  
   SELECT   
    DISTINCT   
    Product.PrdId  
   FROM   
     SaleableProduct WITH (NOLOCK)   
    INNER JOIN Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId   
    INNER JOIN BookedProduct WITH (NOLOCK) ON SaleableProduct.SapId = BookedProduct.BpdSapId  
   -- KX Changes :RKS  
   WHERE BpdRntId = @param2  
  )  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF @case = 'PRODUCT4EXCHANGE'  
BEGIN  
 SELECT DISTINCT   
    PrdId    AS 'ID',  
    PrdShortName  AS  'CODE',    
    PrdName   AS 'DESCRIPTION'  
 FROM Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)  
 WHERE  SapPrdId = PrdId   
 AND  PrdTypId =@param2  
 AND  SapStatus = 'Active'  
 AND  PrdIsVehicle = 1  
 AND  PrdIsActive = 1  
 AND  SapCtyCode = @param3  
 AND  ((SapId IN (SELECT PplSapId   
       FROM  PackageProduct WITH(NOLOCK)  
       WHERE PplPkgId  = (SELECT RntPkgId FROM Rental WITH(NOLOCK) WHERE RntId =@param4)  
      ))  
    OR SapIsBase = 1)  
 AND  (PrdShortName LIKE @param1+'%'  
   OR  PrdShortName  LIKE @param1+'%')  
 -- KX Changes :RKS  
 AND (  
   @sUsrCode = '' OR PrdBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
            INNER JOIN dbo.UserCompany (NOLOCK) ON Brand.BrdComCode = UserCompany.ComCode  
            INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
            AND UserInfo.UsrCode =  @sUsrCode  
           )  
  )  
  FOR XML AUTO, ELEMENTS  
END  
  
IF @case = 'PRDTYPE4EXCH'  
BEGIN  
 SELECT   
   TypId  AS 'CODE',  
   TypDesc  AS 'DESCRIPTION',  
   TypOrder AS 'ORDER1',  
   ClaOrder AS 'ORDER2'  
  FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)  
  WHERE TypClaId = ClaId   
  and ClaIsVehicle = 1  
  ORDER BY ClaOrder,TypOrder  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF @case = 'PRDTYPE4LATEFEE'  
BEGIN  
 SELECT   
  DISTINCT    
    TypId  AS 'CODE',  
    TypDesc  AS 'DESCRIPTION',  
    TypOrder AS 'ORDER1',  
    ClaOrder AS 'ORDER2'  
  FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)  
  WHERE TypClaId = ClaId   
  --and ClaIsVehicle = 1  
  ORDER BY ClaOrder,TypOrder  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'CLASSTYPE')  
BEGIN  
/*  
 SELECT DISTINCT  TypCode AS 'CODE',  
    TypDesc  AS 'DESCRIPTION'  
  FROM Type AS POPUP  
  where  TypClaId LIKE @param1+'%'  
  
  AND (TypCode  LIKE @param2+'%'  
  OR TypDesc LIKE @param2+'%')  
  FOR XML AUTO,ELEMENTS   
*/  
 SELECT DISTINCT  TypCode AS 'CODE',  
    TypDesc  AS 'DESCRIPTION',  
    TypOrder AS 'ORDER1',  
    ClaOrder AS 'ORDER2'  
  FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)  
  WHERE TypClaId = ClaId   
  AND TypClaId LIKE @param1+'%'  
  AND (TypCode  LIKE @param2+'%'  
  OR TypDesc LIKE @param2+'%')  
  ORDER BY ClaOrder,TypOrder  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
  
IF(@case = 'TYPE')  
BEGIN  
 SELECT DISTINCT  TypCode AS 'CODE',  
    TypDesc  AS 'DESCRIPTION',     TypOrder AS 'ORDER1',  
    ClaOrder AS 'ORDER2'  
  FROM Type AS POPUP WITH (NOLOCK), Class WITH (NOLOCK)  
  WHERE  TypClaId = ClaId   
  AND (TypCode  LIKE @param1+'%'  
  OR TypDesc LIKE @param1+'%')  
  AND TypIsActive = 1  
  ORDER BY ClaOrder,TypOrder  
  FOR XML AUTO,ELEMENTS  
  
 RETURN  
END  
  
IF(@case = 'CODCODE')  
BEGIN  
 EXEC sp_get_CodeCodeType @param1,@param2  
 RETURN  
END  
  
IF(@case = 'OPLOGBRANCH')  
BEGIN  
 Declare @sQuery varchar(400)  
 SET @sQuery=''  
 SELECT DISTINCT  LocationCode  AS CODE,  
    LocationName  AS DESCRIPTION  
  FROM aimsprod.dbo.AI_Location AS POPUP WITH (NOLOCK)  
  WHERE LocationCode LIKE @param1+'%'  
  OR LocationName LIKE @param1+'%'  
  ORDER BY  DESCRIPTION   
  FOR XML AUTO,ELEMENTS  
  
/*  
 SELECT DISTINCT  LivLocCode  AS  'CODE' ,  
     ''+LocName AS  'DESCRIPTION'   
    
   FROM LocationInventory AS POPUP, Location  
  
  WHERE  LivLocCode = LocCode  
  
  AND  (LivLocCode  LIKE  @param1+'%'  
  OR    LocName  LIKE  @param1+'%')  
  AND  LocIsActive  = 1  
  ORDER BY  'DESCRIPTION'   
  FOR XML AUTO,ELEMENTS  
*/  
 RETURN  
END  
  
IF(@case = 'OPLOGFLEETREPBRANCH')  
BEGIN  
 SELECT Distinct   
   LocationCode  AS 'CODE',  
   LocationName  AS 'DESCRIPTION'  
  FROM   aimsprod.dbo.AI_Location AS POPUP WITH (NOLOCK)  
  WHERE (LocationCode  LIKE  @param1 + '%'  
  OR   LocationName LIKE  @param1 + '%')  
  -- KX Changes :RKS  
  AND (@sUsrCode = '' OR LocationCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
             INNER JOIN  dbo.Company (NOLOCK) ON LocComCode = Company.ComCode  
             INNER JOIN dbo.UserCompany (NOLOCK)ON UserCompany.ComCode = Company.ComCode  
             INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId  
             AND UsrCode = @sUsrCode))  
  ORDER BY 2   
  FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'OPLOGFLEETREPPROVIDER')  
BEGIN  
 SELECT  Distinct    
   LTRIM(RTRIM(RepairerCode))  AS 'CODE',  
   LTRIM(RTRIM(RepairerName))  AS 'DESCRIPTION'  
  FROM  aimsprod.dbo.AI_Repairer AS POPUP WITH (NOLOCK)  
  WHERE (RepairerCode LIKE  @param1 + '%'  
  OR   RepairerName LIKE  @param1 + '%')  
  ORDER BY 2   
  FOR XML AUTO,ELEMENTS  
 RETURN  
END   
  
IF(@case = 'OPLOGFLEETREPPROVIDERID')  
BEGIN  
 SELECT  Distinct    
   LTRIM(RTRIM(RepairerId))   AS 'ID',  
   LTRIM(RTRIM(RepairerCode))  AS 'CODE',  
   LTRIM(RTRIM(RepairerName))  AS 'DESCRIPTION'  
 FROM  AIMSProd.dbo.AI_Repairer AS POPUP WITH (NOLOCK)  
 WHERE RepairerCode   = @param1  
 ORDER BY 2   
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'LOCATION')  
BEGIN  
 IF EXISTS(select loccode from dbo.location (nolock) where loccode = @param1 and LocisActive = 1)  
 BEGIN  
   
  SELECT LocCode   AS 'CODE',  
    LocName  AS 'DESCRIPTION'  
  FROM Location AS POPUP WITH (NOLOCK)  
  WHERE (LocCode  = @param1)  
  AND  LocIsActive  =  1  
  -- KX Changes :RKS  
  AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
            WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
           )  
    )  
  ORDER BY LocName  
  FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
 ELSE BEGIN  
  SELECT LocCode   AS 'CODE',  
    LocName  AS 'DESCRIPTION'  
  FROM Location AS POPUP WITH (NOLOCK)  
  WHERE (LocCode  LIKE @param1 + '%'   
   OR  LocName LIKE @param1 + '%' )  
  AND  LocIsActive  =  1  
  -- KX Changes :RKS  
  AND (@sUsrCode = '' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK)   
            WHERE LocComCode IN (SELECT Company.ComCode FROM dbo.Company (NOLOCK)  
                INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
                INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode)  
           )  
    )  
  ORDER BY LocName  
  FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
END  
  
IF(@case = 'LOCATIONFORSTOCK')  
BEGIN  
 DECLARE @sCty  varchar(64)  
 SELECT   
  @sCty = UsrCtyCode   
 FROM   
  UserInfo WITH (NOLOCK)   
 WHERE   
  UsrCode = @param2  
  
 SELECT       
  LocCode    AS CODE,   
  LocName    AS DESCRIPTION  
 FROM         
  Location AS POPUP WITH (NOLOCK)    
 WHERE       
  (LocIsActive = 1) AND   
  (LocCode LIKE @param1 + '%' OR @param1='' ) AND   
  LocCode IN(   
    SELECT       
     Location.LocCode  
    FROM           
     Location WITH (NOLOCK) INNER JOIN  
                         TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode  
    WHERE       
     TownCity.TctCtyCode = @sCty AND Location.LocCode IN  
     (  
      SELECT  
      LocationCode  
      FROM  
      aimsprod.DBO.AI_Location  
     )  
   )  
 -- KX Changes: RKS  
 AND (@param2 = '' OR LocComCode IN (SELECT UserCompany.ComCode FROM dbo.UserCompany (NOLOCK)  
          INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId  
          AND UsrCode = @param2)  
  )  
 ORDER BY LocName  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'LOCATIONFORCOUNTRY')  
  
BEGIN  
 IF EXISTS(select locCode from dbo.location where Loccode = @param2 and LocisActive =1  
     AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)  
            WHERE Loccode = @param2 AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode   
            AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode  
            )  
     )  
   )  
 begin  
  SELECT LocCode   AS 'CODE',  
    LocName  AS 'DESCRIPTION'  
   FROM Location AS POPUP WITH (NOLOCK)  
   WHERE (ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
   AND LocCode LIKE @param2   
   AND LocIsActive  =  1  
   AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)  
            WHERE Loccode = @param2 AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode   
            AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode)  
     )  
 -- ORDER BY 2  
  
  FOR XML AUTO,ELEMENTS  
  RETURN  
 END  
 ELSE begin  
  SELECT LocCode   AS 'CODE',  
    LocName  AS 'DESCRIPTION'  
   FROM Location AS POPUP WITH (NOLOCK)  
   WHERE (ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
   AND (LocCode  LIKE @param2 + '%'   
   OR  LocName LIKE @param2 + '%')  
   AND LocIsActive  =  1  
   AND (@sUsrCode='' OR LocCode IN (SELECT LocCode FROM dbo.Location (NOLOCK), Company  (NOLOCK), UserInfo  (NOLOCK), UserCompany  (NOLOCK)  
            WHERE (LocCode  LIKE @param2 + '%'   
              OR  LocName LIKE @param2 + '%') AND LocisActive =1 AND LocComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode   
            AND UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @sUsrCode)  
     )  
 -- ORDER BY 2  
  FOR XML AUTO,ELEMENTS  
  RETURN  
 end  
END  
  
IF(@case = 'LOCATIONFORCOUNTRY_NEW')  
BEGIN  
 SELECT LocCode   AS 'CODE',  
   LocName  AS 'DESCRIPTION'  
  FROM Location AS POPUP WITH (NOLOCK)  
  WHERE (ISNULL(@param1,'') = '' OR LocTctCode  IN (SELECT TctCode FROM TownCity WITH (NOLOCK) WHERE TctCtyCode = @param1))  
  AND (LocCode LIKE @param2 + '%' OR LocName LIKE @param2 + '%')  
  AND LocIsActive  =  1  
  AND DBO.GetCodCode(LocCodTypId) <> 'CC'  
  AND DBO.GetCodCode(LocCodTypId) <> 'HQ'  
 ORDER BY 2  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'OPLOGGETFLEETMODEL')  
BEGIN  
   
 --SET @sFleetCompanyId = ''  
 IF @sUsrCode<>''  
  SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK)   
  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode  
   
 SELECT LTRIM(RTRIM(FleetModelCode))    AS 'CODE',  
   LTRIM(RTRIM(FleetModelDescription)) AS 'DESCRIPTION'  
   FROM AIMSProd.dbo.AI_FleetModel AS POPUP WITH (NOLOCK)  
  WHERE (FleetModelCode  LIKE @param1 + '%'  
  OR FleetModelDescription LIKE @param1 + '%')  
  -- KX Changes :RKS  
  AND (@sUsrCode='' OR POPUP.FleetModelId IN (SELECT Fleet.FleetModelId FROM AIMSProd.dbo.FleetAsset Fleet (NOLOCK)  
        WHERE CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0))  
 ORDER BY 2  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF(@case = 'PRODUCTCOUNTRY')  
BEGIN  
 IF EXISTS (SELECT CtyCode FROM Country WITH (NOLOCK)  
   WHERE  CtyCode    = @param1  
   AND  CtyIsActive = 1)  
 BEGIN  
  SELECT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country WITH (NOLOCK)  
  WHERE  CtyCode     =  @param1  
  AND  CtyHasProducts = 1  
  AND  CtyIsActive  =  1  
  FOR XML AUTO,ELEMENTS   
 END  
 ELSE  
 BEGIN  
  SELECT  CtyCode AS 'CODE',  
    CtyName AS 'DESCRIPTION'  
  FROM Country WITH (NOLOCK)   
  WHERE  (CtyCode LIKE @param1+'%'  
  OR  CtyName LIKE @param1+'%')  
  AND   CtyHasProducts  = 1  
  AND  CtyIsActive   = 1  
  FOR XML AUTO,ELEMENTS   
 END  
  
 RETURN  
END  
  
IF(@case = 'POPUPSALEABLEPRODUCT')  
BEGIN  
 SELECT  PrdName      AS 'CODE',  
  PrdShortName +  ' - ' + CAST(SapSuffix AS varchar(4)) AS 'DESCRIPTION'  
 FROM  Product AS POPUP WITH (NOLOCK), Saleableproduct WITH (NOLOCK)  
 WHERE POPUP.prdid    =  Saleableproduct.SapPrdId  
 AND (PrdShortName   LIKE @param1 + '%'   
 OR  PrdName   LIKE @param1 + '%') /*  
 AND Saleableproduct.SapId  IN  (SELECT PatSapId FROM ProductAttribute) */  
 -- KX Changes : RKS  
 AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
           WHERE BrdComCode = Company.ComCode AND Company.ComCode = UserCompany.ComCode AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
          )  
  )  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
  
IF(@case = 'POPUPSALEABLEPRODUCTID')  
BEGIN  
 SELECT  ''+SapId   AS 'ID',  
  --PrdName   AS 'CODE',  
  PrdShortName   AS 'CODE',  
  CAST(SapSuffix AS varchar(4)) AS 'DESCRIPTION'  
 FROM  Product AS POPUP WITH (NOLOCK), Saleableproduct WITH (NOLOCK)  
 WHERE prdid    =  SapPrdId  
 AND (PrdShortName  LIKE @param1 + '%'   
 OR  PrdName  LIKE @param1 + '%')  
 AND SapSuffix >= @param2  
 AND PrdShortName >= @param3     
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF @case  = 'BOOKINGAGN'  
BEGIN  
/*  
 SELECT DISTINCT   
  Agent.AgnCode  AS CODE,   
  Agent.AgnName AS 'DESCRIPTION'  
 FROM Booking WITH (NOLOCK) INNER JOIN Agent WITH (NOLOCK) ON Booking.BooAgnId = Agent.AgnId  
 AND  (Agent.AgnCode  LIKE @param1 + '%'   
  OR Agent.AgnName LIKE    @param1 + '%' )  
 FOR XML AUTO, ELEMENTS  
*/  
 SELECT DISTINCT   
  Agent.AgnCode  AS CODE,   
  Agent.AgnName AS 'DESCRIPTION'  
 FROM Agent WITH (NOLOCK)   
 WHERE (Agent.AgnCode  LIKE @param1 + '%'   
  OR Agent.AgnName LIKE    @param1 + '%' )  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF @case  = 'PRD4CTY'  
BEGIN  
 SELECT   
  PrdId              AS 'ID',  -- Added : RKS 29/07  
  PrdShortName /*+  ' - ' + CAST(SapSuffix AS varchar(4))*/ AS 'CODE',  
  PrdName              AS 'DESCRIPTION'  
  
FROM  
 Product WITH (NOLOCK)   
WHERE  
 (PrdShortName   LIKE @param1 + '%'   
  
 OR    
 PrdName   LIKE  @param1 + '%')   
 AND  
 PrdIsVehicle = 0  
 AND  
 PrdIsActive =1  
 AND  
  EXISTS(SELECT  sapid  
    FROM  
      SaleableProduct WITH (NOLOCK)   
       WHERE  
      SapPrdId = PrdId   
      AND  
      SapStatus = 'ACTIVE'  
         AND  (EXISTS(SELECT  PplSapId  
          FROM  
            PackageProduct WITH (NOLOCK)   
          WHERE  
            PplSapId = SapId   
            AND   
            PplPkgId = (SELECT RntPkgId  
                FROM  
                 Rental WITH (NOLOCK)   
                WHERE  
                 RntId = @param3 )        )   
         OR  (sapIsBase = 1 AND SapCtyCode = @param2)  
   )  
   )  
 -- KX Changes :RKS  
 AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)   
          INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode  
          INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
          INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode)  
 )  
 --Added by Shoel for Enh : Prod Expiry
 AND NOT EXISTS(SELECT 1 FROM NonAvailabilityProfile WITH (NOLOCK)
 				WHERE NapPrdId = PrdId AND CURRENT_TIMESTAMP BETWEEN NapBookedFromDate AND NapBookedToDate)
 --Added by Shoel for Enh : Prod Expiry
 ORDER BY PrdShortName  
 FOR XML AUTO,ELEMENTS  
 RETURN  
END  
  
IF @case  = 'GST'  
BEGIN  
 SELECT   
  GstCode AS 'CODE',   
  GstDesc AS 'DESCRIPTION'  
 FROM Gst WITH (NOLOCK)  
 WHERE  GstCode LIKE @param1 + '%'  
 OR GstDesc LIKE @param1 + '%'  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF @case  = 'CODE'  
BEGIN  
 SELECT  CodId AS 'ID',  
  CodCode AS 'CODE',   
  CodDesc AS 'DESCRIPTION'  
 FROM Code POPUP WITH (NOLOCK)  
 WHERE  CodCdtNum = @param2  
 AND CodIsActive = 1  
 AND (CodCode LIKE @param1 + '%'  
 OR CodDesc LIKE @param1 + '%')  
 ORDER BY CodDesc  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF @case  = 'IncentiveScheme'  
BEGIN  
 DECLARE @sUsrCtyCode VARCHAR(64)  
    --SELECT @sUsrCtyCode =  FROM UserInfo WHERE UsrCode = @param2  
 SELECT @sUsrCtyCode = TctCtyCode  
  FROM   Rental  WITH(NOLOCK),  
            Location WITH(NOLOCK),  
   TownCity WITH(NOLOCK)  
  WHERE RntCkoLocCode = LocCode   
  AND LocTctCode = TctCode  
  AND RntId = @param2  
  
 SELECT  IscCode AS 'CODE',   
  IscDesc AS 'DESCRIPTION'  
 FROM IncentiveScheme POPUP WITH (NOLOCK)  
 WHERE    
 IscIsActive = 1  
 AND (IscCode LIKE @param1 + '%' OR IscDesc LIKE @param1 + '%')  
    AND (IscCtyCode = @sUsrCtyCode OR ISNULL(IscCtyCode,'')='')  
 ORDER BY IscCode  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
  
IF(@case='GetVehicleData')  
BEGIN  
 IF EXISTS(SELECT POPUP.prdid  
    FROM Product POPUP WITH (NOLOCK), SaleableProduct WITH (NOLOCK)  
    WHERE    
     PrdIsActive = 1  
     AND  SaleableProduct.SapCtyCode = @param2  
     AND PrdShortName = @param1  
   )  
  SELECT DISTINCT  
   PrdId   AS 'ID',  
    PrdShortName + ' - ' + BrdName +  ' - '  +  PrdName    AS 'DESCRIPTION',  
   PrdIsVehicle as PrdIsVehicle  
 FROM  
  Product AS POPUP WITH (NOLOCK) INNER JOIN  
            Brand WITH (NOLOCK) ON POPUP.PrdBrdCode = Brand.BrdCode INNER JOIN  
            SaleableProduct WITH (NOLOCK) ON POPUP.PrdId = SaleableProduct.SapPrdId  
 WHERE  
    PrdIsActive = 1  
   AND  SaleableProduct.SapCtyCode = @param2  
   AND PrdShortName = @param1  
   -- KX Changes :RKS  
   AND (@sUsrCode = '' OR PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)  
              INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = Company.ComCode  
              INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.ComCode = Company.ComCode  
              INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId  
              AND UsrCode = @sUsrCode  
             )  
   )  
 FOR XML AUTO, ELEMENTS   
 RETURN  
END  
  
IF(@case='UNIVERSALINFO')  
BEGIN  
 IF EXISTS(SELECT uvikey FROM dbo.UniversalInfo WITH (NOLOCK) WHERE UviKey = @param1)  
  SELECT  UviValue  'VAL'  
   FROM UniversalInfo WITH (NOLOCK)  
   WHERE UviKey = @param1  
   FOR XML AUTO, ELEMENTS  
 ELSE  
  SELECT  TOP 1 'UviValueNot Set'  'VAL'  
   FROM UniversalInfo WITH (NOLOCK)  
   FOR XML AUTO, ELEMENTS    
 RETURN  
END  
  
IF(@case='GETUVI')  
BEGIN  
 SELECT dbo.getUviValue(UsrCtyCode + @param2) FROM UserInfo WITH (NOLOCK) WHERE USrCode = @param1  
END  
  
IF(@case='tmpreport')  
BEGIN  
 EXEC Tmp_Report @param1  
 RETURN  
END  
  
IF(@case = 'tmpRMreport')  
BEGIN  
 SET @sFleetCompanyId = ''  
 IF @sUsrCode<>''  
  SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK)   
  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode  
  
 IF @param1 = ''  
  SET @param1 = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)  
  
 IF @param2 = 'LE' AND @param1 <> ''  
  SELECT  '' + FA.UnitNumber AS 'A',   
    ''+RegistrationNumber AS 'B',   
    ''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',  
    ''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',  
    ''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',  
    ''+ISNULL(StartOdometer, '')  AS 'F',  
    ''+ISNULL(EndOdometer, '')  AS 'G',  
    ''+ISNULL(OtherRepairer, '')  AS 'H',  
    ''+ISNULL(ServiceProvider, '') AS 'I',  
    ''+ISNULL(ScheduledService, '') AS 'J'  
  FROM  aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)  
  WHERE  FR.FleetAssetId = FA.FleetAssetId  
  AND  FM.FleetModelId = FA.FleetModelId  
  AND  CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) <= CAST(@param1 AS datetime)  
  AND  Completed = 0  
  AND  FA.LastLocation = FL.LocationId  
  AND  (@param3 = '' OR FL.LocationCode = @param3)  
  -- KX Changes :RKS  
  AND  (@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)  
  FOR XML AUTO  
 ELSE IF @param2 = 'GE'  AND @param1 <> ''  
  SELECT  '' + FA.UnitNumber AS 'A',   
    ''+RegistrationNumber AS 'B',   
    ''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',  
    ''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',  
    ''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',  
    ''+ISNULL(StartOdometer, '')  AS 'F',  
    ''+ISNULL(EndOdometer, '')  AS 'G',  
    ''+ISNULL(OtherRepairer, '')  AS 'H',  
    ''+ISNULL(ServiceProvider, '') AS 'I',  
    ''+ISNULL(ScheduledService, '') AS 'J'  
  FROM  aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)  
  WHERE  FR.FleetAssetId = FA.FleetAssetId  
  AND  FM.FleetModelId = FA.FleetModelId  
  AND  CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) >= CAST(@param1 AS datetime)  
  AND  Completed = 0  
  AND  FA.LastLocation = FL.LocationId  
  AND  (@param3 = '' OR FL.LocationCode = @param3)  
  -- KX Changes :RKS  
  AND  (@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)  
  FOR XML AUTO  
 ELSE IF  @param1 <> ''  
  SELECT  '' + FA.UnitNumber AS 'A',   
    ''+RegistrationNumber AS 'B',   
    ''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'C',  
    ''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'D',  
    ''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'E',  
    ''+ISNULL(StartOdometer, '')  AS 'F',  
    ''+ISNULL(EndOdometer, '')  AS 'G',  
    ''+ISNULL(OtherRepairer, '')  AS 'H',  
    ''+ISNULL(ServiceProvider, '') AS 'I',  
    ''+ISNULL(ScheduledService, '') AS 'J'  
  FROM  aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)  
  WHERE  FR.FleetAssetId = FA.FleetAssetId  
  AND  FM.FleetModelId = FA.FleetModelId  
  AND  CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) = CAST(@param1 AS datetime)  
  AND  Completed = 0  
  AND  FA.LastLocation = FL.LocationId  
  AND  (@param3 = '' OR FL.LocationCode = @param3)  
  -- KX Changes :RKS  
  AND  (@sUsrCode = '' OR CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0)  
  FOR XML AUTO  
 RETURN   
  
END  
  
IF(@case = 'COREPORT')  
BEGIN  
 IF @param1 = ''  
  SET @param1 = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)  
 IF @param3 IS NULL OR @param3 = ''  
  SET @param3 = 0  
 SELECT ''+ExternalRef AS 'a',     
   ''+LTRIM(RTRIM(FleetModelCode)) + ' - ' + LTRIM(RTRIM(FleetModelDescription))  AS 'b',   
   ''+UnitNumber AS 'c',  
   ''+RegistrationNumber AS 'd',  
   ''+StartOdometer AS 'e',  
   ''+/*LTRIM(RTRIM(LocationCode)) + ' - ' +*/ LTRIM(RTRIM(LocationName)) AS 'f',  
   ''+(SELECT BooLastName FROM Booking WITH(NOLOCK) WHERE BooNum = dbo.getSplitedData(ExternalRef, '/', 1)) AS 'g',  
   ''+(SELECT BooId FROM Booking WITH(NOLOCK)   
     WHERE BooNum = dbo.getSplitedData(ExternalRef, '/', 1)) AS BooId,  
   ''+(SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK)   
     WHERE BooId = RntBooId  
     AND  BooNum = dbo.getSplitedData(ExternalRef, '/', 1)  
     AND  RntNum = dbo.getSplitedData(dbo.getSplitedData(ExternalRef, '/', 2), 'x', 1)) AS RntId  
  
 FROM AIMSPROD.dbo.FleetActivity FAT WITH(NOLOCK), AIMSPROD.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetModel FM WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)  
 WHERE FAT.FleetAssetId = FA.FleetAssetId  
 AND  FA.FleetModelId = FM.FleetModelId  
 AND  FAT.StartLocation = FL.LocationId  
 AND  FAT.ActivityType = 'Rental'  
 --AND  FAT.Completed = @param3  
 AND  CAST(CONVERT(VARCHAR(10), StartDateTime, 103) AS DATETIME) = CAST(@param1 AS DATETIME)  
 AND  (@param2 = '' OR FL.LocationCode = @param2)  
 ORDER BY FleetModelCode, UnitNumber  
 FOR XML AUTO   
END  
  
IF(@case = 'BONDREPORT')  
BEGIN  
 SET @param3 = @param3 + ' 23:59:59'  
 IF EXISTS( SELECt RptId  
     FROM Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)  
       -- KX Changes :RKS  
       , Package (NOLOCK)  
         
     WHERE PmtBooId  = BooId  
     AND  RntBooId  = BooId  
     AND  RptType = 'B'  
     AND  PmtId  = RptPmtId  
     AND  RntId = RptRntId  
     AND  dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1  
     AND  PmtDateTime BETWEEN @param2 AND @param3  
     AND  PmtPtmId = PtmId   
     AND  (@param4 = '1' or PtmName NOT LIKE '%imprint%')  
     -- KX Changes :RKS   
     AND  RntPkgId = PkgId  
     AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
              INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode  
              INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId  
              AND UsrCode = @sUsrCode)  
      )  
    )      
 BEGIN  
  SELECt '<tr bgcolor="white"><td>' +   
    CONVERT(VARCHAR(12), BooNum+'/'+RntNum)   + '</td>' + '<td>' +   
    CONVERT(varchar(25), Ptmname)     + '</td>' + '<td>' +   
    CONVERT(VARCHAR(10), RntCkoWhen, 103)   + '</td>' + '<td>' +   
    CONVERT(VARCHAR(10), RntCkiWhen, 103)   + '</td>' + '<td align="right">' +   
    CAST(ISNULL(RptLocalCurrAmt,0) AS VARCHAR(10))  + '</td>' + '<td align="center">' +   
    ''+RntStatus         + '</td></tr>'  
  FROM Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)  
    -- KX Changes :RKS  
    , Package (NOLOCK)  
  WHERE PmtBooId  = BooId  
  AND  RntBooId  = BooId  
  AND    RptType = 'B'  
  AND  PmtId  = RptPmtId  
  AND  RntId = RptRntId  
  AND  dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1  
  AND  PmtDateTime BETWEEN @param2 AND @param3  
  AND  PmtPtmId = PtmId   
  AND  (@param4 = '1' or PtmName NOT LIKE '%imprint%')   
  -- KX Changes :RKS   
  AND  RntPkgId = PkgId  
  AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
           INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode  
           INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId  
           AND UsrCode = @sUsrCode)  
   )  
  ORDER BY Rntckowhen, BooNum+'/'+RntNum, PmtDateTime  
   
  SELECT '<tr bgcolor="lightblue"><td COLSPAN="4" align="right"><b>Total Gross Amount</b></td>' +   
    '<td align="right"><b>' + CAST(SUM(ISNULL(RptLocalCurrAmt, 0)) AS VARCHAR(12)) + '</b></td><td></td></tr>'  
  FROM Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)  
    -- KX Changes :RKS  
    , Package (NOLOCK)  
  WHERE PmtBooId  = BooId  
  AND  RntBooId  = BooId  
  AND    RptType = 'B'  
  AND  PmtId  = RptPmtId  
  AND  RntId = RptRntId  
  AND  dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1  
  AND  PmtDateTime BETWEEN @param2 AND @param3  
  AND  PmtPtmId = PtmId   
  AND  (@param4 = '1' or PtmName NOT LIKE '%imprint%')   
  -- KX Changes :RKS   
  AND  RntPkgId = PkgId  
  AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
           INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode  
           INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId  
           AND UsrCode = @sUsrCode)  
   )  
 END  
 ELSE  
  SELECT '<tr bgcolor="white"><td>&#160;</td><td></td><td></td><td></td><td align="right"></td><td align="center"></td></tr>'  
END -- IF(@case = 'BONDREPORT')  
  
IF(@case = 'BONDREPORT1')  
BEGIN  
 SET @param3 = @param3 + ' 23:59:59'  
 DECLARE @TT TABLE ( BooNum  varchar(12),  
      PmtMet varchar(48),  
      CkoDate varchar(10),  
      CkiDate varchar(10),  
      Amount varchar(12),  
      Status varchar(12))  
 INSERT INTO @TT  
  SELECT CONVERT(VARCHAR(12), BooNum+'/'+RntNum)  AS BkgNo,  
    CONVERT(VARCHAR(25),Ptmname)    AS Prod,   
    CONVERT(VARCHAR(10), RntCkoWhen, 103)   As Cko,   
    CONVERT(VARCHAR(10), RntCkiWhen, 103)   AS Cki,   
    CAST(RptLocalCurrAmt AS VARCHAR)   AS GrossAmt,  
    ''+RntStatus         AS Status  
   FROM Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK),   
     Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK), Location WITH(NOLOCK),   
     TownCity WITH(NOLOCK)  
     -- KX Changes :RKS  
     , Package (NOLOCK)  
   WHERE Pmtbooid  = Booid  
   AND  Rntbooid  = Booid  
   AND  RntCkoLocCode = LocCode  
   AND  LocTctCode = TctCode  
   AND  TctCtyCode = @param1  
   AND  RptType = 'B'  
   AND  Pmtid  = RptPmtId  
   AND  RntId  = RptRntId  
   AND  PmtDateTime BETWEEN @param2 AND @param3  
   AND  PmtPtmId = PtmId   
   AND  (@param4 = '1' or PtmName NOT LIKE '%Imprint%')   
   -- KX Changes :RKS   
   AND  RntPkgId = PkgId  
   AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
            INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode  
            INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId  
            AND UsrCode = @sUsrCode)  
    )  
   ORDER BY RntCkoWhen, BkgNo, PmtDateTime  
   
 IF EXISTS(SELECT TOP 1 BooNum FROM @TT)  
 BEGIN  
  
  INSERT INTO @TT(BooNum, PmtMet, CkoDate, CkiDate, Amount, Status)  
   SELECT BooNum, 'TOTAL', '', '', CAST(sum(cast(Amount as money)) AS VARCHAR), '' from @TT   
   GROUP BY BooNum  
  
  DELETE FROM @TT  
   WHERE BooNum IN (SELECT BooNum FROM @TT WHERE CAST(Amount AS MONEY) = 0)  
  
  SELECT CASE WHEN CkoDate <> ''   
     THEN  
      '<tr bgcolor="white"><td>' +   
      BooNum   + '</td>' + '<td>' +   
      PmtMet  + '</td>' + '<td>' +   
      CkoDate  + '</td>' + '<td>' +   
      CkiDate  + '</td>' + '<td align="right">' +   
      CAST(ISNULL(Amount,0) AS VARCHAR(10))  + '</td>' + '<td align="center">' +   
      Status         + '</td></tr>'  
     ELSE  
      '<tr bgcolor="lightblue"><td></td><td></td><td></td>' + '<td align="right"><b>Sub Total</b></td>' + '<td align="right"><b>' +   
      CAST(ISNULL(Amount,0) AS VARCHAR(10))  + '</b></td>' + '<td align="center"></td></tr>'  
    END  
   FROM @TT AS Bond  
   ORDER BY BooNum, CkoDate DESC  
  
   
   SELECT '<tr bgcolor="lightblue"><td COLSPAN="4" align="right"><b>Total Gross Amount</b></td>' +   
     '<td align="right"><b>' + CAST(SUM(ISNULL(RptLocalCurrAmt, 0)) AS VARCHAR(12)) + '</b></td><td></td></tr>'  
   FROM Payment WITH(NOLOCK), RentalPayment WITH(NOLOCK), Booking WITH(NOLOCK), Rental WITH(NOLOCK), PaymentMethod WITH(NOLOCK)  
   -- KX Changes :RKS  
   , Package (NOLOCK)  
   WHERE PmtBooId  = BooId  
   AND  RntBooId  = BooId  
   AND    RptType = 'B'  
   AND  PmtId  = RptPmtId  
   AND  RntId = RptRntId  
   AND  dbo.getcountryforlocation(RntCkoLocCode,'',1) = @param1  
   AND  PmtDateTime BETWEEN @param2 AND @param3  
   AND  PmtPtmId = PtmId   
   AND  (@param4 = '1' or PtmName NOT LIKE '%imprint%')   
   -- KX Changes :RKS   
   AND  RntPkgId = PkgId  
   AND (@sUsrCode = '' OR PkgBrdCode IN (SELECT Brand.BrdCode FROM dbo.Brand (NOLOCK)  
            INNER JOIN dbo.UserCompany (NOLOCK) on Brand.BrdComCode = UserCompany.ComCode  
            INNER JOIN dbo.UserInfo (NOLOCK) on UserInfo.UsrId = UserCompany.UsrId  
            AND UsrCode = @sUsrCode)  
    )  
 END  
 ELSE  
  SELECT '<tr bgcolor="white"><td>&#160;</td><td></td><td></td><td></td><td align="right"></td><td align="center"></td></tr>'  
 RETURN  
END -- IF(@case = 'BONDREPORT1')  
  
IF (@case = 'SELBOOPRD')  
BEGIN  
 DECLARE @RntPkgId VARCHAR(64),  
   @sCtyCode varchar(24)  
  
 SELECT  @RntPkgId = RntPkgId,  
   @sCtyCode = ISNULL(dbo.getCountryForLocation(RntCkoLocCode, NULL, ''), '')  
 FROM  Rental WITH (NOLOCK)   
 WHERE RntId = @param1  
 IF @param3 = 'p'  
  SELECT  PrdId   AS 'Id',  
     PrdShortName AS 'ShortName',  
     PrdName   AS 'Name'  
  FROM PackageProduct WITH (NOLOCK)   
    LEFT OUTER JOIN SaleableProduct WITH (NOLOCK) ON pplsapid = sapid   
    LEFT OUTER JOIN Product PRODUCT WITH (NOLOCK) ON sapprdid = prdid  
  WHERE  PrdShortName LIKE @param2 + '%'  
  AND  PrdIsVehicle = 0  
  AND  PplPkgId = @RntPkgId  
  AND  SapStatus = 'Active'  
  AND  SapCtyCode = @sCtyCode  
  -- KX Changes :RKS  
  AND  PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
          WHERE BrdComCode = Company.ComCode   
        AND Company.ComCode = UserCompany.ComCode   
        AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
            )  
  ORDER BY PrdShortName  
  FOR XML AUTO, ELEMENTS   
 ELSE  
  SELECT  PrdId   AS 'Id',  
     PrdShortName AS 'ShortName',  
     PrdName   AS 'Name'  
  FROM SaleableProduct WITH (NOLOCK) LEFT OUTER JOIN Product PRODUCT WITH (NOLOCK) ON SapPrdId = PrdId     WHERE  PrdShortName LIKE @param2 + '%'  
  AND  PrdIsVehicle = 0  
  AND  SapStatus = 'Active'  
  AND  SapCtyCode = @sCtyCode  
  AND  SapIsBase = 1  
  -- KX Changes :RKS  
  AND  PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK), dbo.Company (NOLOCK), dbo.UserCompany (NOLOCK), dbo.UserInfo (NOLOCK)  
          WHERE BrdComCode = Company.ComCode   
        AND Company.ComCode = UserCompany.ComCode   
        AND UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @sUsrCode  
            )  
  ORDER BY PrdShortName  
  FOR XML AUTO, ELEMENTS     
 RETURN  
END  
  
IF (@case = 'VALIDATEBOOKING')  
BEGIN  
 DECLARE @sError varchar(8000),  
   @sWarn varchar(8000)  
 IF @param2 = '' SET @param2 = NULL  
  
 IF ISNULL(@param1, '') <> '' OR ISNULL(@param2, '') <> ''  
 BEGIN  
  EXECUTE RES_BookingValidation  
   @psBooId = @param1,   -- VARCHAR(64),  
   @psRntId = @param2,   -- VARCHAR(64),  
   @psUsrId = @param3,   -- VARCHAR(64),  
   @psErrors = @sError   OUTPUT, -- VARCHAR(8000) OUTPUT  
   @psWarn  = @sWarn OUTPUT -- VARCHAR(8000) OUTPUT  
 END  
 ELSE  
  SELECT @sError = 'Both BooId and RntId are Blank',  
    @sWarn = ''  
  
 SELECT '<Error>' + ISNULL(@sError, '') + '</Error><Wrn>' + ISNULL(@sWarn, '') + '</Wrn>'  
 RETURN  
END  
  
IF (@case = 'CHKBOORENT')  
BEGIN  
 IF NOT EXISTS(SELECT booid FROM Booking WITH (NOLOCK) WHERE BooNum = @param1)  
 BEGIN  
  SELECT 'ERROR^' + dbo.getErrorString('GEN003', @param1,'Booking Number','','','','')  
  RETURN  
 END  
 IF NOT EXISTS(SELECT RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId   
     WHERE BooNum = @param1  
     AND  RntNum = @param2)  
 BEGIN  
  SELECT 'ERROR^' + dbo.getErrorString('GEN003', @param2 , 'Rental Number For Booking "' + @param1 + '"', '', '','','')  
  RETURN  
 END  
 SELECT 'SUCCESS^'  
 SELECT TOP 1  
  ''+BooNum + '/' + RntNum      AS 'RntId',  
  ''+PrdDvassSeq        AS 'PrdSeq',  
  CASE dbo.getCountryForLocation(RntCkoLocCode, NULL, '')   
   WHEN 'AU' THEN 0  
   WHEN 'NZ' THEN 1  
   ELSE -1  
  END           AS 'Cty',  
  ''+UPPER(RntCkoLocCode)      AS 'CkoLoc',  
  ''+CONVERT(VARCHAR(10), RntCkoWhen, 103) AS 'CkoDate',  
  CASE dbo.getAmPmFromDate(RntCkoWhen)  
   WHEN 'AM'  THEN 0  
   WHEN 'PM' THEN 1     
   ELSE -1  
  END           AS 'CkoDayPart',  
  
  
  UPPER(RntCkiLocCode)      AS 'CkiLoc',  
  CONVERT(VARCHAR(10), RntCkiWhen, 103)  AS 'CkiDate',   
  CASE dbo.getAmPmFromDate(RntCkiWhen)  
   WHEN 'AM'  THEN 0  
  
   WHEN 'PM' THEN 1     
   ELSE -1  
  END           AS 'CkiDayPart',  
  1           AS 'PRIORITY',  
  10           AS 'VISIBILITY',  
  1           AS 'FORCEFLAG'   
 FROM BookedProduct DVASS WITH (NOLOCK)  
    LEFT OUTER JOIN SaleableProduct WITH (NOLOCK) ON SapId = BpdSapId  
    LEFT OUTER JOIN Product WITH (NOLOCK) ON SapPrdId = PrdId  
    LEFT OUTER JOIN Rental WITH (NOLOCK) ON BpdRntId = RntId  
    LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId  
 WHERE  BooNum = @param1  
 AND  RntNum = @param2  
 AND  PrdIsVehicle = 1  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF (@case = 'UPDTALLVEHSP')  
BEGIN  
 UPDATE Rental WITH (ROWLOCK)  
  SET RenAllowVehSpec =  CASE @param3  
         WHEN '' THEN NULL  
         ELSE @param3  
        END  
 WHERE RntId IN (SELECT TOP 1 RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId  
        WHERE  BooNum = @Param1  
        AND  RntNum = @Param2)  
 IF (@@ERROR <> 0)  
 BEGIN  
  SELECT 'ERROR^ErrorIn Updating Rental (' + @Param1 + '/' + @Param2 + ')'  
  RETURN  
 END  
 SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')  
 RETURN  
END  
  
IF (@case = 'FLEXAGENTEMAIL')  
BEGIN  
 SELECT PhnEmailAddress AS 'Email'  
 FROM Contact with(NOLOCK), PhoneNumber WITH(NOLOCK), Agent WITH(NOLOCK), Code WITH(NOLOCK)  
 WHERE  ConPrntId = AgnId  
 AND  ConPrntTableName = 'AGENT'  
 AND  ConCodContactId = CodId  
 AND  PhnPrntTableName = 'Contact'  
 AND  PhnPrntId = ConId  
 AND  CodCdtNum = 25  
 AND  CodCode = 'FLEX'  
 AND  PhnCodPhoneId IN (SELECT CodId FROM Code WITH(NOLOCK) WHERE CodCdtNum = 11 AND CodCode = 'Email')  
 FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF (@case = 'RENTADD')  
BEGIN  
 SET @param2 = dbo.getSplitedData(@param1, '/', 1)  
 SET @param3 = dbo.getSplitedData(@param1, '/', 2)  
 SELECT TOP 1   
   ''+@param1          AS 'RentalId',  
   ''+PrdDvassSeq        AS 'PrdId',  
  
   ''+RntCkoLocCode       AS 'CoLoc',  
   ''+CONVERT(VARCHAR(10), RntCkoWhen, 103) AS 'CoDate',  
   ''+dbo.GetAmPmFromDate(RntCkoWhen)    AS 'CkoDayPart',  
   ''+RntCkiLocCode       AS 'CiLoc',  
   ''+CONVERT(VARCHAR(10), RntCkiWhen, 103) AS 'CiDate',  
   ''+dbo.GetAmPmFromDate(RntCkiWhen)    AS 'CkiDayPart',  
   ''+'0'          AS  'Revenue',  
   ''+'1'           AS  'Priority',  
   ''+'10'          AS  'Visibility',  
   ''+''           AS  'VehicleRanges',  
   ''+'1'          AS  'Force'  
  FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK), BookedProduct WITH(NOLOCK), SaleableProduct WITH(NOLOCK), Product WITH(NOLOCK)  
  WHERE BooId = RntBooId  
  AND  BpdRntId = RntId  
  AND  BpdPrdIsVehicle = 1  
  AND  BpdIsCurr = 1  
  AND  SapId = BpdSapId  
  AND  PrdId = SapPrdId  
  AND  BooNum = @param2  
  AND  RntNum = @param3  
  ORDER BY BpdCkiWhen DESC  
  FOR XML AUTO, ELEMENTS  
 RETURN  
END  
  
IF @case = 'VEHASSGET_GRID'  
BEGIN  
 SET @param3 = @param1  
 SET @param2 = NULL  
 SET @param1 = dbo.getSplitedData(@param3, '/', 1)  
   
 SELECT @Param1 = BooId FROM Booking WITH(NOLOCK)  
  WHERE BooNum = @Param1  
 IF CHARINDEX('/', @param3) <> 0  
 BEGIN  
  SET @param2 = dbo.getSplitedData(@param3, '/', 2)  
  SELECT @Param2 = RntId FROM Rental WITH(NOLOCK)  
   WHERE RntBooId = @Param1  
   AND  RntNum = @Param2  
 END  
   
 SET @sComCode = dbo.fun_getCompany  
       (  
        @sUsrCode,  --@sUserCode VARCHAR(64),  
        NULL,    --@sLocCode VARCHAR(64),  
        NULL,    --@sBrdCode VARCHAR(64),  
        NULL,    --@sPrdId  VARCHAR(64),  
        NULL,    --@sPkgId  VARCHAR(64),  
        NULL,    --@sdummy1 VARCHAR(64),  
        NULL,    --@sdummy2 VARCHAR(64),  
        NULL    --@sdummy3 VARCHAR(64)  
       )  
 IF NOT EXISTS(SELECT RntId FROM Rental WITH(NOLOCK)  INNER JOIN Package (NOLOCK) ON RntPkgId = PkgId  
    WHERE RntBooId = @Param1  
    AND  RntId = @param2  
    -- KX Changes: RKS  
    AND PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) WHERE BrdComCode = @sComCode)  
    AND RntStatus = 'KK'  
    )  
 BEGIN  
  SELECT '<Error><ErrStatus>True</ErrStatus><ErrCode>GEN</ErrCode><ErrDesc>' + @param3 + '  - Not a valid Booking Number for company - ' + @sComCode + '</ErrDesc></Error>'  
  RETURN  
 END  
  
 SET @param3 = dbo.getCodeId(13, 'Veh_Assign')  
   
 IF EXISTS(SELECT NteId FROM Note WITH(NOLOCK)   
     WHERE NteBooId = @param1  
     AND  NteRntId = @Param2  
     AND  NteCodTypId = @param3)  
  SELECT NteId       'Id',  
    NteDesc       'Desc',  
    dbo.getCodCode(NteCodAudTypId) 'AudTyp',     
    NtePriority      'Pri',  
    NteIsActive      'Act',  
    CASE   
     WHEN NteIsActive = 1 THEN 'Yes'  
     ELSE 'No'  
    END        'Active',  
    IntegrityNo      'Int'  
   FROM Note WITH(NOLOCK)   
   WHERE NteBooId = @param1  
   AND  NteRntId = @Param2  
   AND  NteCodTypId = @param3  
   ORDER BY NtePriority  
   FOR XML AUTO, ELEMENTS  
 ELSE  
  SELECT '<Note><Id/><Desc/><AudTypId>' + dbo.getCodeId(26, 'All') + '</AudTypId><AudTyp></AudTyp><Pri/><Act>1</Act><Int/></Note>'  
 RETURN  
END -- IF @case = 'VEHASSGET_GRID'  
  
IF @case = 'VALIDATEVEHICLE'  
BEGIN  
 DECLARE @sTempComCode varchar(64)  
  
 /*IF @sUsrCode<>''  
  SELECT   
    @sFleetCompanyId = FleetCompanyId,   
    @sTempComCode  = UserCompany.ComCode  
  FROM dbo.Company (NOLOCK)   
  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode  
   
 IF NOT EXISTS(  
    SELECT FleetAssetId FROM AimsProd.dbo.FleetAsset (NOLOCK)  
    WHERE UnitNumber = @param1 AND CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0  
 )  
 BEGIN  
  SELECT '<Error><ErrStatus>True</ErrStatus><ErrCode>GEN</ErrCode><ErrDesc>' + @param1 + '  - Not a valid unit number for company - ' + @sTempComCode + '</ErrDesc></Error>'  
  RETURN  
 END  
 ELSE*/  
  SELECT '<root></root>'  
END -- IF @case = 'VALIDATEVEHICLE'  
  
IF @case = 'VEHASSUPDT'  
BEGIN  
 IF dbo.getSplitedData(@param3, '~', 1) = ''  
 BEGIN  
  SET @sUsrCode = dbo.getSplitedData(@param3, '~', 7)  
  SELECT @sComCode = dbo.fun_getCompany  
      (  
       @sUsrCode, -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       NULL,  -- @sPkgId  VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
  
  INSERT INTO Note ( NteId,  
       NteBooID,  
       NteRntId,  
       NteCodTypId,  
       NteDesc,  
       NteCodAudTypId,  
       NtePriority,  
       NteIsActive,  
       IntegrityNo,  
       AddUsrId,  
       AddDateTime,  
       AddPrgmName,  
       -- KX Changes :RKS  
       NteComCode  
      )  
   SELECT  NEWID(),  
     (SELECT BooId FROM Booking WITH(NOLOCK) WHERE BooNum = @param1),  
     (SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK) WHERE BooId = RntBooId AND BooNum = @param1 AND RntNum = @param2),  
     dbo.getCodeId(13, 'Veh_Assign'),  
     dbo.getSplitedData(@param3, '~', 2),  
     dbo.getCodeId(26, dbo.getSplitedData(@param3, '~', 4)),  
     dbo.getSplitedData(@param3, '~', 3),  
     dbo.getSplitedData(@param3, '~', 5),  
     1,  
     (SELECT UsrId FROM UserInfo with (nolock) WHERE UsrCode = dbo.getSplitedData(@param3, '~', 7)),  
     CURRENT_TIMESTAMP,  
     'Vehicle_Assign',  
     -- KX Changes :RKS  
     @sComCode  
  IF (@@ERROR <> 0)  
  BEGIN  
   SELECT 'ERROR^ErrorIn Updating Note For Vehicle Assign(' + @Param1 + '/' + @Param2 + ')'  
   RETURN  
  END  
  SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')  
  RETURN  
 END -- IF dbo.getSplitedData(@param3, '~', 1) = ''  
 ELSE  
 BEGIN  
  Update Note WITH(ROWLOCK)  
   SET NteDesc = dbo.getSplitedData(@param3, '~', 2),  
    NteCodAudTypId = dbo.getCodeId(26, dbo.getSplitedData(@param3, '~', 4)),  
    NtePriority = dbo.getSplitedData(@param3, '~', 3),  
    NteIsActive = dbo.getSplitedData(@param3, '~', 5),  
    IntegrityNo = IntegrityNo + 1,  
    ModUsrId = (SELECT UsrId FROM UserInfo  with (nolock) WHERE UsrCode = dbo.getSplitedData(@param3, '~', 7)),  
    ModDateTime = CURRENT_TIMESTAMP  
  WHERE NteId = dbo.getSplitedData(@param3, '~', 1)  
  IF (@@ERROR <> 0)  
  BEGIN  
   SELECT 'ERROR^ErrorIn Updating Note For Vehicle Assign(' + @Param1 + '/' + @Param2 + ')'  
   RETURN  
  END  
  SELECT 'SUCCESS^' + dbo.getErrorString('GEN046', '(' + @Param1 + '/' + @Param2 + ')', '', '', '', '', '')  
  RETURN  
 END    
 RETURN  
END -- IF @case = 'VEHASSUPDT'  
  
IF (@case = 'getBpdStatus')  
BEGIN  
 DECLARE @iValue VARCHAR  (12)  
 SET @iValue = ISNULL(dbo.getProductAttributeValue(@param2,'BkgRef'),'0')  
 SELECT @soryBy = CdtSortBy FROM CodeType WHERE CdtNum = 42  
 IF (@soryBy = NULL)  
  SET @soryBy = ''  
  
 IF @param1 IN ('KK','NN')  
 BEGIN  
  IF (@soryBy = '' OR @soryBy = 'CodCode')  
  BEGIN  
   SELECT CodId     AS 'ID',  
     CodCode    AS 'CODE',  
     ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
   FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum      = 42  
   AND  CodIsActive  = 1  
   AND  CodCode IN ('KK','NN')  
   ORDER BY CodCode  
   FOR XML AUTO,ELEMENTS  
   --RETURN  
  END  
    
  IF (@soryBy = 'CodDesc' )  
  BEGIN  
   SELECT CodId     AS 'ID',  
     CodCode    AS 'CODE',  
     ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
   FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum      = 42  
   AND  CodIsActive = 1  
   AND   CodCode IN ('KK','NN')  
   ORDER BY CodDesc  
   FOR XML AUTO,ELEMENTS  
   --RETURN  
  END  
        
  IF (@soryBy = 'CodOrder' )  
  BEGIN  
   SELECT CodId    AS 'ID',  
     CodCode  AS 'CODE',  
     ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
   FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum   = 42  
   AND  CodIsActive = 1  
   AND  CodCode IN ('KK','NN')  
   ORDER BY CodOrder  
   FOR XML AUTO,ELEMENTS  
   --RETURN  
  END   
--  DECLARE  @sRntId  VARCHAR(64)  
  SELECT @sRntId = BpdRntId FROM Bookedproduct WHERE BpdId = @param3  
  
  IF @param1 = 'NN' AND NOT Exists(SELECT BpdId FROM dbo.BookedProduct (NOLOCK)  
            WHERE BpdStatus = 'XX'   
            AND BpdRntId = @sRntId)  
   SELECT CodId    AS 'ID',  
     CodCode  AS 'CODE',  
     ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
   FROM Code WITH (NOLOCK)  
   WHERE Codcdtnum   = 42  
   AND  CodIsActive = 1  
   AND  CodCode IN ('QN')  
   ORDER BY CodOrder  
   FOR XML AUTO,ELEMENTS  
   RETURN   
  RETURN  
 END   
 ELSE  
 BEGIN  
  IF @iValue = '1'  
  BEGIN  
   IF (@soryBy = '' OR @soryBy = 'CodCode')  
   BEGIN  
    SELECT CodId      AS 'ID',  
      CodCode    AS 'CODE',  
      ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
    WHERE Codcdtnum      = 42  
    AND  CodIsActive = 1  
    AND  CodCode NOT IN ('XX','XN','CO','CI')  
    ORDER BY CodCode  
    FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
    
   IF (@soryBy = 'CodDesc' )  
   BEGIN  
    SELECT CodId     AS 'ID',  
      CodCode    AS 'CODE',  
      ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
      FROM Code WITH (NOLOCK)  
     WHERE Codcdtnum      = 42  
     AND  CodIsActive = 1  
     AND   CodCode NOT IN ('XX','XN','CO','CI')  
     ORDER BY CodDesc  
     FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
         
   IF (@soryBy = 'CodOrder' )  
   BEGIN  
    SELECT CodId    AS 'ID',  
      CodCode  AS 'CODE',  
      ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
    WHERE Codcdtnum      = 42  
    AND  CodIsActive = 1  
    AND   CodCode NOT IN ('XX','XN','CO','CI')  
    ORDER BY CodOrder  
    FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
   RETURN  
  
  END -- IF @iValue = '1'  
  ELSE  
  BEGIN  
   IF (@soryBy = '' OR @soryBy = 'CodCode')  
   BEGIN  
    SELECT CodId      AS 'ID',  
       CodCode    AS 'CODE',  
       ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
    WHERE Codcdtnum      = 42  
    AND  CodIsActive = 1  
    AND  CodCode NOT IN ('XX','XN','CO','CI','TK')  
    ORDER BY CodCode  
    FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
    
   IF (@soryBy = 'CodDesc' )  
   BEGIN  
    SELECT CodId     AS 'ID',  
      CodCode    AS 'CODE',  
      ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
    WHERE Codcdtnum      = 42  
    AND  CodIsActive = 1  
    AND  CodCode NOT IN ('XX','XN','CO','CI','TK')  
    ORDER BY CodDesc  
    FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
         
   IF (@soryBy = 'CodOrder' )  
   BEGIN  
    SELECT CodId    AS 'ID',  
      CodCode  AS 'CODE',  
      ISNULL(CodDesc,'')  AS 'DESCRIPTION'  
    FROM Code WITH (NOLOCK)  
    WHERE Codcdtnum      = 42  
    AND  CodIsActive = 1  
    AND   CodCode NOT IN ('XX','XN','CO','CI','TK')  
    ORDER BY CodOrder  
    FOR XML AUTO,ELEMENTS  
    RETURN  
   END  
  END  -- ELSE IF @iValue = '1'  
 END  
 RETURN  
END  
  
IF(@case = 'CTYOFOPR')  
BEGIN  
 DECLARE @sUviValue AS varchar(500)  
 DECLARE @iLen  AS  int  
 DECLARE @scount  AS int  
 DECLARE @sstring AS varchar(100)  
   
 SET @sstring = ''  
 SET @scount = 0  
 SET @sUviValue = dbo.getUviValue('CTYOFOPR')  
 SET @iLen = LEN(@sUviValue)  
   
 WHILE(@scount <= @iLen)  
 BEGIN   
  IF(SUBSTRING(@sUviValue,@scount,1) = ',')  
  BEGIN  
   IF EXISTS (SELECT ctycode FROM  country WITH (NOLOCK)  
      WHERE ctycode = @sstring)  
   BEGIN  
    SELECT ctyCode  AS 'CODE',  
      CtyName  AS 'DESCRIPTION'  
    FROM Country AS POPUP WITH (NOLOCK)  
    WHERE ctycode = @sstring  
    AND ( ctycode LIKE @param1 + '%'  
    OR ctyName LIKE @param1 + '%')  
    FOR XML AUTO,ELEMENTS  
   END        
   SET @sstring = ''  
   GOTO skipLoop  
  END  
  ELSE  
  BEGIN  
   SET @sstring = @sstring + SUBSTRING(@sUviValue,@scount,1)     
  END  
   
  skipLoop:  
   IF(@scount = @iLen)  
   BEGIN  
    IF EXISTS (SELECT ctycode FROM  country WITH (NOLOCK)  
     WHERE ctycode = @sstring)  
    BEGIN      
     SELECT ctyCode  AS 'CODE',  
      CtyName  AS 'DESCRIPTION'  
     FROM Country AS POPUP WITH (NOLOCK)  
     WHERE ctycode = @sstring  
     AND ( ctycode LIKE @param1 + '%'   
     OR ctyName LIKE @param1 + '%')  
     FOR XML AUTO,ELEMENTS  
    END   
   END  
   SET @scount = @scount + 1    
 END  
 RETURN  
END  
-- Issue 879: RKS : 18-MAY-2006  
-- START  
IF(@case = 'FleetModel4BookingSearch')  
BEGIN  
   
 --SET @sFleetCompanyId = ''  
 IF @sUsrCode<>''  
  SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK)   
  INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode  
  INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @sUsrCode  
   
 SELECT  LTRIM(RTRIM(FleetModelCode))   AS 'CODE',  
    LTRIM(RTRIM(FleetModelDescription))  AS 'DESCRIPTION'  
  FROM  AimsProd.dbo.FleetModel AS POPUP WITH (NOLOCK)  
  WHERE  (FleetModelCode  LIKE @param1+'%' OR FleetModelDescription LIKE @param1+'%')  
  AND InactiveDate IS NULL  
  -- KX Changes :RKS  
  AND (@sUsrCode='' OR POPUP.FleetModelId IN (SELECT Fleet.FleetModelId FROM AIMSProd.dbo.FleetAsset Fleet (NOLOCK)  
        WHERE CHARINDEX(CONVERT(VARCHAR,CompanyId),@sFleetCompanyId)>0))  
  FOR XML AUTO,ELEMENTS   
 RETURN  
END  
IF @case = 'ChangeUserCompany'  
BEGIN  
 SELECT @sComCode = dbo.fun_getCompany  
      (  
       @param1, -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       NULL,  -- @sPkgId  VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
 IF @sComCode = 'KXS'  
 BEGIN  
  UPDATE dbo.UserCompany WITH (ROWLOCK)  
   SET ComCode = 'THL' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)  
    
  UPDATE dbo.UserInfo WITH (ROWLOCK)  
   SET UsrCtyCode = 'NZ', UsrLocCode = 'AKL' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)  
 END   
 ELSE  
 BEGIN  
  UPDATE dbo.UserCompany WITH (ROWLOCK)  
   SET ComCode = 'KXS' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)  
    
  UPDATE dbo.UserInfo WITH (ROWLOCK)  
   SET UsrCtyCode = 'NZ', UsrLocCode = 'AK1' WHERE UsrId IN (SELECT UsrId from dbo.UserInfo (NOLOCK) where UsrCode = @param1)  
 END  
  
END  
-- END  
/*--found in the akl-kxdev-001 but not exist to the thlmel015 - manny (march 12,07)*/  
set ANSI_NULLS ON  
set QUOTED_IDENTIFIER ON  
  
  
  
  
  