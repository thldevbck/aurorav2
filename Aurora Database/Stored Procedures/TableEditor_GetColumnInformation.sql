USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetColumnInformation]    Script Date: 11/19/2007 13:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	
CREATE PROCEDURE [dbo].[TableEditor_GetColumnInformation]
(
	@tableName AS nvarchar(256)
)
AS

SELECT DISTINCT
	c.ORDINAL_POSITION AS OrdinalPosition,
	c.COLUMN_NAME AS ColumnName, 
	CAST (CASE 
		WHEN EXISTS 
		(
			SELECT 1 
			FROM
				INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
				INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc 
				 ON kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME 
				 AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
			WHERE
				c.COLUMN_NAME = kcu.COLUMN_NAME 
				AND c.TABLE_NAME = kcu.TABLE_NAME 
		) THEN 1 
		ELSE 0 
	END AS bit) AS IsPrimaryKey,
	CAST (CASE WHEN c.IS_NULLABLE = 'NO' THEN 0 ELSE 1 END AS bit) AS IsNullable, 
	COLUMNPROPERTY (OBJECT_ID (c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') AS IsIdentity,
	ISNULL (c.DATA_TYPE, '') AS DataType, 
	ISNULL (c.CHARACTER_MAXIMUM_LENGTH, 0) AS MaxLength
	
FROM 
	INFORMATION_SCHEMA.COLUMNS c 
WHERE 
	c.TABLE_NAME = @tableName 
ORDER BY 
	IsPrimaryKey DESC,
	c.ORDINAL_POSITION	

