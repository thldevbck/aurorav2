USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_UpdateWeekExport]    Script Date: 11/19/2007 13:45:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Flex2_UpdateWeekExport] 
	@FwxId int,
	@FwxScheduledTime datetime,
	@IntegrityNo tinyint,
	@UsrId varchar(64)

AS

UPDATE FlexBookingWeekExport SET
	FlexBookingWeekExport.FwxScheduledTime = @FwxScheduledTime,
	FlexBookingWeekExport.IntegrityNo = @IntegrityNo + 1,
	FlexBookingWeekExport.ModUsrId = @UsrId,
	FlexBookingWeekExport.ModDateTime = GETDATE()
FROM 
	FlexBookingWeekExport
WHERE
	FlexBookingWeekExport.FwxId = @FwxId
	AND FlexBookingWeekExport.IntegrityNo = @IntegrityNo
	AND FlexBookingWeekExport.FwxStatus IN ('created', 'rejected', 'approved')

IF @@ROWCOUNT <= 0 RETURN

SELECT 
	* 
FROM
	FlexBookingWeekExport
WHERE
	FwxId = @FwxId









