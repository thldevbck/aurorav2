USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetBookingWeek]    Script Date: 11/19/2007 13:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Flex2_GetBookingWeek] 
	@ComCode varchar(64),
	@FbwId int
AS

SELECT 
	*
FROM 
	FlexBookingWeek
WHERE
	FbwComCode = @ComCode
	AND FbwId = @FbwId
