CREATE  PROCEDURE [dbo].[Report_BranchVehTypeSummary]
	@sCtyCode		varchar(64),
	@sLocCode		varchar(64),
	@sFromDate		varchar(12),
	@sToDate		varchar(12),
	@sBrdCode		varchar(10),
	@sClaCode		varchar(64),
	@sTypCode		varchar(64),
	@sPrdShortName	varchar(64)
AS

SET NOCOUNT ON
/*
DECLARE @sCtyCode		varchar(64),
		@sLocCode		varchar(64),
		@sFromDate		varchar(12),
		@sToDate		varchar(12),
		@sBrdCode		varchar(10),
		@sClaCode		varchar(64),
		@sTypCode		varchar(64),
		@sPrdShortName	varchar(64)

SET @sCtyCode = 'NZ'
SET @sLocCode = ''
SET @sFromDate = '01/04/2007'
SET @sToDate = '02/04/2007'
SET @sBrdCode = ''
SET @sClaCode = ''
SET @sTypCode = ''
SET @sPrdShortName = ''
*/
-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND (Class.ClaIsVehicle = 1)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

DECLARE		@sClassCode		varchar(12)
DECLARE 	@sPrdName		varchar(30)
DECLARE 	@sBpdIdCurrent	varchar(64)
DECLARE 	@sBpdSapid		varchar(64)
DECLARE		@dRntCkoWhen    datetime
DECLARE		@sRentalId		VARCHAR(64)

DECLARE @AU_THRIFTYPRDS VARCHAR(1000)
DECLARE @AU_THRIFTYEFFDT datetime
SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

DECLARE @tCko TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 int,
	a6 varchar(54),
	a7 varchar(64)
)

IF @sClaCode = ''
	SET @sClassCode = 'All'
ELSE
	SET	@sClassCode = @sClaCode

IF @sTypCode = ''
	SET @sTypCode = 'All'

IF @sPrdShortName = ''
	SET @sPrdName = 'All'
ELSE
	SET	@sPrdName = @sPrdShortName

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME

SET @StartDate	= CONVERT(DATETIME, @sFromDate,103)
SET @EndDate	= CONVERT(DATETIME,(@sToDate + ' 23:59:59') ,103)
SET @WkDATE		= CONVERT(DATETIME,@sFromDate,103)

-- Location (AJ: KX fix)
DECLARE @Location TABLE (
	LocCode	varchar(64)
)
IF @sLocCode = ''
	BEGIN
		Insert Into @Location (LocCode)
		SELECT DISTINCT UPPER(dbo.Rental.RntCkoLocCode) AS LocCode
		FROM         dbo.Rental WITH (NOLOCK)
							INNER JOIN dbo.Location WITH (NOLOCK) ON dbo.Rental.RntCkoLocCode = dbo.Location.LocCode
							INNER JOIN dbo.TownCity WITH (NOLOCK) ON dbo.Location.LocTctCode = dbo.TownCity.TctCode
							INNER JOIN dbo.Package ON dbo.Rental.RntPkgId = dbo.Package.PkgId
		WHERE     (dbo.TownCity.TctCtyCode = @sCtyCode) AND (dbo.Package.PkgBrdCode IN
								  (SELECT     dbo.Brand.BrdCode
									FROM      dbo.UserInfo
													INNER JOIN dbo.UserCompany ON dbo.UserInfo.UsrId = dbo.UserCompany.UsrId
													INNER JOIN dbo.Brand ON dbo.UserCompany.ComCode = dbo.Brand.BrdComCode
									WHERE      (dbo.UserInfo.UsrCode = USER)))
	END
ELSE
	Insert Into @Location (LocCode) Values (@sLocCode)

-- Generate Cur_out to get check out vehicle count
IF @sLocCode  <> ''
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   Rental.RntCkoLocCode = @sLocCode
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate
ELSE
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkoLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate

OPEN Cur_out
FETCH NEXT FROM Cur_out INTO @sRentalId
WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sBpdIdCurrent = null
		SET @sBpdSapid = null
		SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
					 @sBpdSapId		 = BpdSapid ,
					 @dRntCkoWhen    = RntCkoWhen
			FROM dbo.Bookedproduct with (nolock), rental (nolock)
			WHERE Bpdrntid = @sRentalId
			and   bpdrntid = rntid
			AND Bpdprdisvehicle = 1
			and Bpdiscurr = 1
			and BpdStatus = 'KK'
			ORder BY BpdCkiWhen

		--CLASS
		IF @sClassCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock), dbo.type with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		Typid = PrdTypId
							AND		@sClaId = typclaid)
				GOTO GetNextRentalCKO
		END

		-- Type
		IF @sTypCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select Sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		PrdTypid = @sTypId)
				GOTO GetNextRentalCKO
		END

		--Brand
		IF @sBrdCode <> ''
		BEGIN
			IF NOT EXISTS(select rntid from dbo.rental with (nolock), dbo.package with (nolock)
							where 	rntid = @sRentalId
							AND		rntpkgid = Pkgid
							AND		PkgBrdCode = @sBrdCode)
				GOTO GetNextRentalCKO
		END

		-- Product
		IF @sPrdName <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	SapPrdId = @sPrdId)

				GOTO GetNextRentalCKO
		END

		-- Thrifty Check
		IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
		BEGIN
			IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
										dbo.saleableproduct (nolock),
										dbo.product (nolock),
										dbo.type (nolock),
										dbo.class (nolock)
						WHERE bpdid = @sBpdIdCurrent
						and bpdsapid = sapid
						and sapprdid = prdid
						and prdtypid = typid
						and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkoWhen >= @AU_THRIFTYEFFDT
				GOTO GetNextRentalCKO
		END

		INSERT INTO @tCko
		SELECT  UPPER(Rental.RntCkoLocCode),
				CONVERT(VARCHAR, Rntckowhen, 106),
				ISNULL(Class.ClaCode, ''),
				Type.TypCode,
				'1',
				boonum,
				Rntnum
		FROM	Product  WITH (NOLOCK) ,
               	Type  WITH (NOLOCK) ,
               	Class  WITH (NOLOCK),
               	SaleableProduct  WITH (NOLOCK) ,
               	BookedProduct  WITH (NOLOCK) ,
              	Rental  WITH (NOLOCK) ,
				booking with (nolock)
		WHERE   bpdid = @sBpdIdCurrent
		AND		bpdrntid = Rntid
		AND		rntbooid = Booid
		AND		BpdSapid = Sapid
		AND		SapPrdid = Prdid
		ANd		prdtypid = Typid
		And		TypClaId = Claid

		GetNextRentalCKO:
		FETCH NEXT FROM Cur_out INTO @sRentalId
	END
CLOSE Cur_out
DEALLOCATE cur_out

-- Total check out vehicles by location, class and type
SELECT	LTRIM(RTRIM(a1))	AS  RntCkoLocCode,
		a3  AS  Class,
		a4  AS	Type,
		Sum(a5)	AS	COCount
Into #CkoVehicle
FROM @tCko
Group By a1, a3, a4
Order By a1

	-- Retrieve all vehicle classes and types
SELECT     dbo.Class.ClaCode, dbo.Type.TypCode
INTO	#ClassTypeCode
FROM         dbo.Class INNER JOIN
                      dbo.Type ON dbo.Class.ClaID = dbo.Type.TypClaId
WHERE     (dbo.Class.ClaIsVehicle = 1)
ORDER BY dbo.Class.ClaCode

-- Combine all locations with all vehicle classes and types
Select lo.LocCode, cto.ClaCode, cto.TypCode
Into #LocationClassType
From @Location lo
Cross Join #ClassTypeCode cto
Order by lo.LocCode
drop table #ClassTypeCode

-- Work out check in
DECLARE @tCki TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 int,
	a6 varchar(64),
	a7 varchar(64)
)

DECLARE	@sRentalIdIn	VARCHAR(64)
DECLARE @dRntCkiWhen    datetime
IF @sLocCode  <> ''
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   (Rental.RntCkiLocCode = @sLocCode)
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate

ELSE
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkiLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate


OPEN Cur_in
FETCH NEXT FROM Cur_in INTO @sRentalIdIn
WHILE @@FETCH_STATUS=0
BEGIN

	SET @sBpdIdCurrent = null
	SET @sBpdSapid = null
	SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
				 @sBpdSapId		 = BpdSapid,
				 @dRntCkiWhen    = RntCkiWhen,
				 @dRntCkoWhen 	 = RntCkoWhen
		FROM dbo.Bookedproduct with (nolock) , dbo.rental (nolock)
		WHERE Bpdrntid = @sRentalIdIn
		AND  bpdrntid= rntid
		AND Bpdprdisvehicle = 1
		and Bpdiscurr = 1
		and BpdStatus = 'KK'
		ORder BY BpdCkiWhen desc

	--CLASS
	IF @sClassCode <> 'ALL'
	BEGIN

		IF NOT EXISTS(select sapid from Saleableproduct with (nolock), product with (nolock), type with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	Prdid = SapPrdId
						AND		Typid = PrdTypId
						AND		@sClaId = typclaid)
			GOTO GETNEXTCKI
	END

	-- Type
	IF @sTypCode <> 'ALL'
	BEGIN

		IF NOT EXISTS(select Sapid from Saleableproduct with (nolock), product with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	Prdid = SapPrdId
						AND		PrdTypid = @sTypId)
			GOTO GETNEXTCKI
	END

	--Brand
	IF @sBrdCode <> ''
	BEGIN
		IF NOT EXISTS(select rntid from dbo.rental with (nolock), package with (nolock)
						where 	rntid = @sRentalIdIn
						AND		rntpkgid = Pkgid
						AND		PkgBrdCode = @sBrdCode)

			GOTO GETNEXTCKI
	END

	-- Product
	IF @sPrdName <> 'ALL'
	BEGIN

		IF NOT EXISTS(select sapid from Saleableproduct with (nolock)
						WHERE 	Sapid = @sBpdSapId
						AND  	SapPrdId = @sPrdId)

			GOTO GETNEXTCKI
	END

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @sBpdIdCurrent
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0
					AND  @dRntCkiWhen >= @AU_THRIFTYEFFDT and @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTCKI
	END


	INSERT INTO @tCki
	SELECT  UPPER(bpdCkiLocCode),
			CONVERT(VARCHAR, RntCkiWhen, 106),
			Class.ClaCode,
			Type.TypCode,
			1,
			boonum,
			rntnum
	FROM	Product  WITH (NOLOCK) ,
           	Type  WITH (NOLOCK) ,
           	Class  WITH (NOLOCK) ,
           	SaleableProduct  WITH (NOLOCK) ,
           	BookedProduct  WITH (NOLOCK),
			rental with (nolock),
			booking with (nolock)
	WHERE   bpdid = @sBpdIdCurrent
	AND		bpdsapid = Sapid
	AND		SApPrdid = Prdid
	AND		Prdtypid = Typid
	AND		Typclaid = Claid
	AND		bpdrntid = rntid
	AND		rntbooid = booid

	GETNEXTCKI:
	FETCH NEXT FROM Cur_in INTO @sRentalIdIn
END

-- Total check in vehicles by location, class and type
SELECT	LTRIM(RTRIM(a1)) AS RntCkiLocCode,
		a3	AS	Class,
		a4	AS	Type,
		Sum(a5)	AS	CICount
Into #CkiVehicle
FROM @tCki
Group By a1, a3, a4
Order By a1

-- Associate location, class and type with check out and check in vehicles
Select lct.LocCode, lct.ClaCode, lct.TypCode, IsNull(cko.COCount, '0') AS COCount, IsNull(cki.CICount, '0') AS CICount
From #LocationClassType lct
	Left Join #CkoVehicle cko
		on lct.LocCode = cko.RntCkoLocCode AND lct.ClaCode = cko.Class AND lct.TypCode = cko.Type
	Left Join #CkiVehicle cki
		on lct.LocCode = cki.RntCkiLocCode AND lct.ClaCode = cki.Class AND lct.TypCode = cki.Type
Order by lct.LocCode, lct.ClaCode

drop table #CkoVehicle
drop table #CkiVehicle
drop table #LocationClassType


GO
