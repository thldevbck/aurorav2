set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================
-- Author:		JackLeong
-- Create date: 2007-10-11
-- Description:	Get function permission for an user
-- =============================================
Create PROCEDURE [dbo].[GetFunctionPermission]
	 @UsrID nvarchar(64), 	
	 @FunID nvarchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		CASE count(1)
			WHEN 0 THEN 0
			ELSE 1
		END as 'HasPermission'
	from userInfo u 
		join UserRole ur on u.usrId = ur.usrId
		join roleFunction rf on rf.rolId = ur.rolId
		join functions f on f.FunId = rf.FunId
	where u.usrCode = @UsrID
		and f.FunCode = @FunID

END
