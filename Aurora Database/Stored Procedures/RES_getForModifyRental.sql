set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go







ALTER	PROCEDURE [dbo].[RES_getForModifyRental]
	@BooId		VARCHAR(64)='',
	@RntId		VARCHAR(64)='',
	@pUsrCode	VARCHAR(64)=''
AS
BEGIN
	SET NOCOUNT ON	
	DECLARE @sUviKey VARCHAR(24),
			@bPkgChgAvail BIT
	SET @bPkgChgAvail = 1

	IF dbo.RES_getCheckOutStatus(@RntId) = 'After Check-Out'
	BEGIN		
		-- Find the Accounts User Role
		SELECT @sUviKey = LTRIM(RTRIM(UsrCtyCode)) + 'ACCTS_ROLE'
			FROM	USerInfo WITH(NOLOCK)
			WHERE	UsrCode = @pUsrCode
			OR		UsrId = @pUsrCode
		SELECT @bPkgChgAvail = dbo.GEN_checkUserRole(@pUsrCode, '',	@sUviKey)
	END -- IF dbo.RES_getCheckOutStatus(@RntId) = 'After Check-Out'

	SELECT
			RntId																										AS	RntId,
			RntNum																										AS	Rental,
			CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D') + ' ' +  RntCkoLocCode	AS	'Check-Out',
			CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D') + ' ' +  RntCkiLocCode	AS	'Check-In' ,
			ISNULL(RntNumOfHirePeriods, 0)																				AS	HirePd ,
			'' + ISNULL(PkgCode,'')																						AS 	PkgCode ,
			'' + ISNULL(BrdName,'')																						AS 	BrdName ,
			''+ ISNULL(PrdShortName,'')																					AS	Vehicle ,
			ISNULL(RntStatus,'')																						AS	Status ,
			ISNULL(Package.PkgCode,'') + ' - ' + ISNULL(BrdName,'') + ' - ' + ISNULL(Package.Pkgname,'') 				AS 	Package ,
			dbo.getCountryForLocation(RntCkoLocCode,RntPkgId,'')														AS 	CtyCode ,										
			ISNULL(AgnIsMisc,0)																							AS	MiscAgn ,
			LEFT(DateName(dw,RntCkoWhen),3)																				AS	CkoDay ,
			LEFT(DateName(dw,RntCkiWhen),3)																				AS	CkiDay , 
			'' + BooNum																									AS	BooNum ,
			'' + AgnCode + ' - ' + AgnName + ' - ' + dbo.GEN_getCodDesc(AgnCodDebtStatId,NULL,NULL)						AS	AgnName ,
			'' + AgnId																									AS 	AgnId ,
			'' + ISNULL(BooLastName,'') + ', ' + ISNULL(BooTitle,'') +  ' ' + ISNULL(BooFirstName,'')					AS	Hirer ,
			'' + ISNULL(dbo.GEN_GetCodDesc('',42,BooStatus),'')															AS	BookingSt , 
			'' + ISNULL(Rental.RntIniStatus,'')																			AS	IniStatus ,
			'' + ISNULL(RntArrivalConnectionRef,'')																		AS	ArrRef ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntArrivalConnectionWhen,103),'')											AS	ArrDate ,
			'' + ISNULL(LEFT(CONVERT(VARCHAR,RntArrivalConnectionWhen,108),5),'')										AS	ArrTm ,
			'' + ISNULL(RntDeptConnectionRef,'')																		AS	DeptRef ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntDeptConnectionWhen,103),'')												AS	DeptDate ,
			'' + ISNULL(LEFT(CONVERT(VARCHAR,RntDeptConnectionWhen,108),5),'')											AS	DeptTm ,
			'' + CONVERT(VARCHAR,ISNULL(RntNumOfAdults,0) )																AS	NoOfAd ,
			'' + CONVERT(VARCHAR,ISNULL(RntNumOfChildren,0) )															AS	NoOfCh ,
			'' + CONVERT(VARCHAR,ISNULL(RntNumOfInfants,0) )															AS	NoOfIn ,
			'' + CONVERT(VARCHAR,ISNULL(RntIsVip,0) )																	AS	IsVIP ,
			'' + CONVERT(VARCHAR,ISNULL(RntIsRepeatCustomer,0) )														AS	RepeatCus ,
			'' + CONVERT(VARCHAR,ISNULL(RntIsConvoy,0) )																AS	IsCon ,
			'' + CONVERT(VARCHAR,ISNULL(RntAgencyRef,''))																AS	AgnRef ,
			'' + CONVERT(VARCHAR,ISNULL(RntVoucNum,''))																	AS	VoucNum ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntBookedWhen,103),'')														AS	BookedWhen ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntFollowUpWhen,103),'')													AS	FollowUpWhen ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntSSXfrTriggerDate,103),'')												AS	TriggerDate ,
			'' + ISNULL(CONVERT(VARCHAR(10),RntXfrdToFinanWhen,103),'')													AS	Transferred ,
			'' + ISNULL(RntConId,'')																					AS 	Contact ,
			'' + ISNULL(RntCodSourceMocId,'')																			AS	RntSource ,
			'' + ISNULL(RntCodKnckTypId,'')																				AS	KnockBack ,
			'' + ISNULL(RntOldRntNum,'')																				AS	OldRntNum ,
			ISNULL(RntCkoLocCode,'')																					AS 	RntCkoLocCode ,	
			ISNULL(RntCkiLocCode,'')																					AS 	RntCkiLocCode ,
			ISNULL(CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D'),'')	AS 	RntCkoWhen ,
			ISNULL(CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D'),'')	AS 	RntCkiWhen ,
			''+ISNULL(PrdId,'')																							AS 	PrdId  ,
			ISNULL(RntPkgId,'')																							AS 	PkgId ,
			ISNULL(CONVERT(VARCHAR(10),Rental.AddDateTime,103),'')														AS	AddWhen ,
			CONVERT(VARCHAR(10),CURRENT_TIMESTAMP, 103)																	AS	TodaysDate,
			@bPkgChgAvail																								AS 	AvailPopup,
			--Added by Shoel on 6/11/7 > for the Integrity number issue
			'' + CAST(Rental.IntegrityNo AS VARCHAR)																	AS  RntIntNo,
			'' + CAST(Booking.IntegrityNo AS VARCHAR)																	AS	BooIntNo,
			--Added by Jack on 14/12/2007 - add RntNum field	
			RntNum as RntNum																						
--		FROM
--			Rental WITH(NOLOCK), Package WITH(NOLOCK), Brand WITH(NOLOCK),
--			Product WITH(NOLOCK), Booking WITH(NOLOCK), Agent WITH(NOLOCK)
--		WHERE 	BooAgnId = AgnId--		AND		RntBooId = BooId
--		AND		RntBooId=@BooId
--		AND		Package.PkgId = Rental.RntPkgId
--		AND		Brand.BrdCode = Package.PkgBrdCode
--		AND		Product.PrdId =* Rental.RntPrdRequestedVehicleId
--		AND		Rental.RntId = @RntId
--660F6C89-B987-436D-AA8F-26EF49162D88
-- Commented as part of SQL Server 2005 upgrade Shoel: 23-May-2008
FROM         
	Product WITH (NOLOCK) 
	RIGHT OUTER JOIN
    Rental WITH (NOLOCK) 
	INNER JOIN
    Agent WITH (NOLOCK) 
	INNER JOIN
    Booking WITH (NOLOCK) 
	ON 
	Agent.AgnId = Booking.BooAgnId 
	ON 
	Rental.RntBooID = Booking.BooID 
	INNER JOIN
	Package WITH (NOLOCK) 
	ON 
	Rental.RntPkgId = Package.PkgId 
	INNER JOIN
	Brand WITH (NOLOCK) 
	ON 
	Package.PkgBrdCode = Brand.BrdCode 
	ON 
	Product.PrdId = Rental.RntPrdRequestedVehicleId
WHERE     
	Rental.RntBooID = @BooId 
	AND 
	Rental.RntId = @RntId
		FOR XML AUTO, ELEMENTS
END







