set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_getConfirmationRentalStatus] 

		@CrsId			varchar	(64)

AS
BEGIN
	SET NOCOUNT ON

	SELECT
		CrsId			AS	CrsId ,			
		CASE CrsRntStatus 

			WHEN 'KK' THEN 'Confirmed'
			WHEN 'NN' THEN 'Provisional'
			WHEN 'WL' THEN 'Waitlist'
			WHEN 'KB' THEN 'Knockback'
			WHEN 'QN' THEN 'Quote'
			WHEN 'XX' THEN 'Cancelled (after confirmation)'
			WHEN 'XN' THEN 'Cancelled (before confirmation)'
			ELSE '' 
		End		AS	Status,
		CrsText			AS	CrsText
	from	ConfirmationRentalStatus
	where	CrsId = @CrsId

END


