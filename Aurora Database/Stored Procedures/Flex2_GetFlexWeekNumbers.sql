USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetFlexWeekNumbers]    Script Date: 11/19/2007 13:43:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_GetFlexWeekNumbers] 
	@TravelYear AS datetime
AS

SELECT 
	*
FROM 
	FlexWeekNumber
WHERE
	FwnTrvYearStart = @TravelYear
ORDER BY
	FwnTrvWkStart