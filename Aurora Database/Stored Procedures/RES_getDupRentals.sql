set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




ALTER PROCEDURE [dbo].[RES_getDupRentals] 
	@sAgnId			varchar(64),
	@sLastName		varchar(64),
	@dCkoDate		varchar(64),
	
	--Optional fields 	-	
	@sVhrId			varchar(64) = '',	-- SUB Issue 847
	@sCkoLocCode	varchar(64) = '',
	@sCkiLocCode	varchar(64) = '',
	@sAgnRef		varchar(64) = '',
	@sCty			varchar(24) = ''
AS
SET DATEFORMAT dmy
SET NOCOUNT ON
--Local Variables
DECLARE @DupRntDays		int,		@ChkBranch	varchar(3),
		@chkDupRental 	varchar(64),@dupNoDays 	varchar(64),
		@expquote 		Int,		@bLog		bit, 
		@iSoundexLName	Varchar(64)

DECLARE	@VhrPkgId	VARCHAR(64),	@AgnId		VARCHAR(64),
		@RntId	VARCHAR(64),	@dToday		DATETIME,
		@Error	VARCHAR(500),	@Message	VARCHAR(500)
DECLARE @tt_Rental TABLE  (tt_RntId	VARCHAR(64))
DECLARE @tt_ErrorFreeRental TABLE  (tt_RntId	VARCHAR(64),
									tt_Error	VARCHAR(1),
									tt_ErrMsg	VARCHAR(500))

SET @iSoundexLName = Soundex(@sLastName)

-- KX Changes :RKS
DECLARE @sComCode	VARCHAR(64)
SELECT	@VhrPkgId = VhrPkgId,
		-- KX Changes :RKS
		@sComCode = dbo.fun_getCompany(AddUsrId,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
	FROM	VehicleRequest WITH(NOLOCK)
	WHERE	VhrId = @sVhrId
--RSM
SELECT @expquote = UviValue FROM dbo.universalinfo (nolock) where uvikey = @sCty + '_ExpQuote'
IF @expquote is  null or @expquote = 0
	SET @expquote = 30

SET @chkDupRental 	= @sCty+'CHKDUPRENTBRANCH'
SET @dupNoDays 		= @sCty+'DUPRENTNODAYS'
SET @DupRntDays 	= 0
SET @ChkBranch 		= '0'
SET @dToday			= GETDATE()

EXEC GEN_getUviValue @chkDupRental, @sCty, @UviValue = @ChkBranch OUTPUT
EXEC GEN_getUviValue @dupNoDays, @sCty, @UviValue = @DupRntDays OUTPUT

IF (@ChkBranch = 'Yes')	
	SET @ChkBranch = '1'
ELSE IF (@ChkBranch = 'No') 
	SET @ChkBranch = '0'

IF @ChkBranch = '0' OR @sCkoLocCode = ''
BEGIN	
	INSERT INTO @tt_Rental
		SELECT 	RntId
			FROM 	Rental WITH (NOLOCK), Booking WITH (NOLOCK), Agent WITH (NOLOCK)
			WHERE	RntBooID = BooId
			AND		AgnId = BooAgnId
			AND		BooLastName = @sLastName
			AND		CAST(CONVERT(varchar(10),rntckowhen,103) AS DATETIME) = @dCkoDate
			AND		RntStatus IN ('IN','QN','WL','KB','NN','KK')
			--AND		(@ChkBranch = 0 OR ((@sCkoLocCode = '' OR RntCkoLocCode = @sCkoLocCode)
			--/*AND	(@sCkiLocCode = '' OR RntCkiLocCode = @sCkiLocCode)*/))
			--AND		(@sAgnRef = '' OR RntAgencyRef = @sAgnRef)

	INSERT INTO @tt_Rental
		SELECT 	RntId
			FROM 	Rental WITH (NOLOCK), Booking WITH (NOLOCK), Agent WITH (NOLOCK)
			WHERE 	RntBooID = BooId
--			AND 	boosoundex = @iSoundexLName	
			AND 	DIFFERENCE(boosoundex, @iSoundexLName) >= 3		-- AJ: multilanguage fix
			AND 	BooLastName <> @sLastName
			AND 	AgnId = BooAgnId
			AND 	NOT (BooLastName = @sLastName AND CAST (CONVERT(varchar(10),rntckowhen,103) AS DATETIME) = @dCkoDate)
			AND 	RntCkoWhen BETWEEN CONVERT(DATETIME,@dCkoDate) - @DupRntDays AND CONVERT(DATETIME,@dCkoDate) + @DupRntDays
			AND 	RntStatus IN ('IN','QN','WL','KB','NN','KK')
			--AND (@ChkBranch = 0 OR ((@sCkoLocCode = '' OR RntCkoLocCode = @sCkoLocCode)
			--/*AND	(@sCkiLocCode = '' OR RntCkiLocCode = @sCkiLocCode)*/))
			--AND (@sAgnRef = '' OR RntAgencyRef = @sAgnRef)

	WHILE (SELECT COUNT(*) FROM @tt_Rental) <> 0
	BEGIN
		SELECT	@AgnId	= NULL, @RntId = NULL, @Error = NULL, @Message = NULL
		SELECT TOP 1 @RntId = tt_RntId
			FROM	@tt_Rental
		DELETE FROM @tt_Rental WHERE tt_RntId = @RntId
		
		SELECT	@AgnId	= BooAgnId
			FROM	Rental WITH(NOLOCK), Booking WITH(NOLOCK)
			WHERE	RntBooId = BooId
			AND		RntId = @RntId
		
		IF ISNULL(@VhrPkgId, '') <> ''
			EXECUTE	RES_checkPackage
				@PackageID				= 	@VhrPkgId,
				@AgentID				=	@AgnId,
				@RentalID				=	@RntId,
				@BookedDate				=	@dToday,
				@CheckOutDate			=	@dCkoDate,
				@ReturnError			=	@Error		OUTPUT,
				@ReturnWarning			=	@Message	OUTPUT

		IF @Error IS NULL OR @Error = ''
			INSERT INTO @tt_ErrorFreeRental(tt_RntId, tt_Error, tt_ErrMsg) VALUES(@RntId, '0', @Error)
		ELSE
			INSERT INTO @tt_ErrorFreeRental(tt_RntId, tt_Error, tt_ErrMsg) VALUES(@RntId, '1', @Error)
	END	--	WHILE (SELECT COUNT(*) FROM @tt_Rental) <> 0

	SELECT 	RntBooID																		AS	BooId,
			RntId 																			AS 	RntId,
			'' + BooNum																		AS	BooNum,
			BooNum + ' - ' + RntNum															AS	BookingRental,
			''+AgnId																		AS	AgnId,
			AgnCode + ' - ' + AgnName														AS	Agent,
			ISNULL(BooLastName, '') + ',' + ISNULL(BooFirstname, '') + ',' + ISNULL(BooTitle, '')	AS	Hirer,
			AgnName + '^' + BooLastName + ',' + BooFirstname + ',' + BooTitle				AS	AgentHirer,
			ISNULL(RntAgencyRef, '') + '^'+ (SELECT PkgCode 
												FROM 	Package  WITH (NOLOCK)
												WHERE 	Pkgid = RntPkgId)					AS	AgentRefPackage,
			CONVERT(CHAR(10), RntCkoWhen, 103) + '^' + RntCkoLocCode						AS	CheckOut,
			CONVERT(CHAR(10), RntCkiWhen, 103) + '^' + RntCkiLocCode						AS	CheckIn,
			CAST(ISNULL(RntNumOfHirePeriods, 0) AS VARCHAR) + ' ' + 
			ISNULL((SELECT CodCode 
					FROM Code WITH (NOLOCK)
					Where CodId = RntCodUOMId), '') + '^' + 
			SUBSTRING(RntStatus, 1, 2) + CASE WHEN Rntstatus = 'QN' and Rntbookedwhen < (getdate() - @expquote) THEN ' EXP ' ELSE '' end	AS HirePdStatus,
			ISNULL((SELECT PrdShortName 
					FROM Product WITH (NOLOCK)
					WHERE PrdId = RntPrdRequestedVehicleId), '')							AS	Vehicle,
			ISNULL(dbo.getCountryForLocation(RntCkoLocCode,'','1'), '') + ' ' +
			ISNULL(CONVERT(VARCHAR(10), dbo.getNetRentalrentalValue(RntId, (SELECT CtyCodCurrID 
										FROM Country WITH (NOLOCK)
										WHERE CtyCode = dbo.getCountryForLocation(RntCkoLocCode,'','1')))), '')	AS	RentalValue,
			'' + tt_Error																						AS	HasError,
			'' + ISNULL(tt_ErrMsg, '')																			AS	ErrMsg
		FROM 	Rental WITH (NOLOCK), @tt_ErrorFreeRental, Booking WITH (NOLOCK), Agent WITH (NOLOCK)
		WHERE 	RntId = tt_RntId
		AND		RntBooId = BooId
		AND 	AgnId = BooAgnId
		ORDER BY  CAST(CONVERT(varchar(10),rntckowhen,103) as datetime) , rntckoloccode , CAST(CONVERT(varchar(10),rntckiwhen,103) AS Datetime), rntckiloccode
		FOR XML AUTO, ELEMENTS
END
ELSE
BEGIN
	INSERT INTO @tt_Rental
	SELECT 	RntId
		FROM Rental WITH (NOLOCK), Booking WITH (NOLOCK), Agent WITH (NOLOCK)
		WHERE 	RntBooID = BooId
		AND 	BooLastName = @sLastName
		AND 	CAST(CONVERT(varchar(10),rntckowhen,103) AS DATETIME) = @dCkoDate
		AND 	AgnId = BooAgnId
		AND 	RntStatus IN ('IN','QN','WL','KB','NN','KK')
		AND 	RntCkoLocCode = @sCkoLocCode

	INSERT INTO @tt_Rental
	SELECT 	RntId
		FROM 	Rental WITH (NOLOCK), Booking WITH (NOLOCK), Agent WITH (NOLOCK)
		WHERE 	RntBooID = BooId
--		AND 	boosoundex = @iSoundexLName	
		AND 	DIFFERENCE(boosoundex, @iSoundexLName) >= 3		-- AJ: multilanguage fix
		AND 	NOT (BooLastName = @sLastName AND CAST (CONVERT(varchar(10),rntckowhen,103) AS DATETIME) = @dCkoDate)
		AND 	AgnId = BooAgnId
		AND 	RntCkoWhen BETWEEN CONVERT(DATETIME,@dCkoDate) - @DupRntDays AND CONVERT(DATETIME,@dCkoDate) + @DupRntDays
		AND 	RntStatus IN ('IN','QN','WL','KB','NN','KK')
		AND 	RntCkoLocCode = @sCkoLocCode

	WHILE (SELECT COUNT(*) FROM @tt_Rental) <> 0
	BEGIN
		SELECT	@AgnId	= NULL, @RntId = NULL, @Error = NULL, @Message = NULL
		SELECT TOP 1 @RntId = tt_RntId
			FROM	@tt_Rental
		DELETE FROM @tt_Rental WHERE tt_RntId = @RntId
		
		SELECT	@AgnId	= BooAgnId
			FROM	Rental WITH(NOLOCK), Booking WITH(NOLOCK)
			WHERE	RntBooId = BooId
			AND		RntId = @RntId

		IF ISNULL(@VhrPkgId, '') <> ''
			EXECUTE	RES_checkPackage
				@PackageID				= 	@VhrPkgId,
				@AgentID				=	@AgnId,
				@RentalID				=	@RntId,
				@BookedDate				=	@dToday,
				@CheckOutDate			=	@dCkoDate,
				@ReturnError			=	@Error		OUTPUT,
				@ReturnWarning			=	@Message	OUTPUT

--if 	@bLog = 0 select @Error = 'test', @bLog = 1 else select @Error = '', @bLog = 0
		IF @Error IS NULL OR @Error = ''
			INSERT INTO @tt_ErrorFreeRental(tt_RntId, tt_Error, tt_ErrMsg) VALUES(@RntId, '0', @Error)
		ELSE
			INSERT INTO @tt_ErrorFreeRental(tt_RntId, tt_Error, tt_ErrMsg) VALUES(@RntId, '1', @Error)
	END	--	WHILE (SELECT COUNT(*) FROM @tt_Rental) <> 0

	SELECT 	RntBooID																		AS	BooId,
			RntId 																			AS 	RntId,
			'' + BooNum																		AS	BooNum,
			BooNum + ' - ' + RntNum															AS	BookingRental,
			''+AgnId																		AS	AgnId,
			AgnCode + ' - ' + AgnName														AS	Agent,
			ISNULL(BooLastName, '') + ',' + ISNULL(BooFirstname, '') + ',' + ISNULL(BooTitle, '')	AS	Hirer,
			AgnName + '^' + BooLastName + ',' + BooFirstname + ',' + BooTitle				AS	AgentHirer,
			ISNULL(RntAgencyRef, '') + '^'+ (SELECT PkgCode 
												FROM 	Package  WITH (NOLOCK)
												WHERE 	Pkgid = RntPkgId)					AS	AgentRefPackage,
			CONVERT(CHAR(10), RntCkoWhen, 103) + '^' + RntCkoLocCode						AS	CheckOut,
			CONVERT(CHAR(10), RntCkiWhen, 103) + '^' + RntCkiLocCode						AS	CheckIn,
			CAST(ISNULL(RntNumOfHirePeriods, 0) AS VARCHAR) + ' ' + 
			ISNULL((SELECT CodCode 
					FROM Code WITH (NOLOCK)
					Where CodId = RntCodUOMId), '') + '^' + 
			SUBSTRING(RntStatus, 1, 2) + CASE WHEN Rntstatus = 'QN' and Rntbookedwhen < (getdate() - @expquote) THEN ' EXP ' ELSE '' end	AS HirePdStatus,
			ISNULL((SELECT PrdShortName 
					FROM Product WITH (NOLOCK)
					WHERE PrdId = RntPrdRequestedVehicleId), '')							AS	Vehicle,
			ISNULL(dbo.getCountryForLocation(RntCkoLocCode,'','1'), '') + ' ' +
			ISNULL(CONVERT(VARCHAR(10), dbo.getNetRentalrentalValue(RntId, (SELECT CtyCodCurrID 
										FROM Country WITH (NOLOCK)
										WHERE CtyCode = dbo.getCountryForLocation(RntCkoLocCode,'','1')))), '')	AS	RentalValue,
			'' + tt_Error																						AS	HasError,
			'' + ISNULL(tt_ErrMsg, '')																			AS	ErrMsg
		FROM 	Rental WITH (NOLOCK), @tt_ErrorFreeRental, Booking WITH (NOLOCK), Agent WITH (NOLOCK)
				-- KX Changes :RKS
				, Package (NOLOCK)
		WHERE 	RntId = tt_RntId
		AND		RntBooId = BooId
		AND 	AgnId = BooAgnId
		-- KX Changes :RKS
		AND		RntPkgId = PkgId
		AND		PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) WHERE BrdComCode = @sComCode)
		ORDER BY  CAST(CONVERT(varchar(10),rntckowhen,103) as datetime) , rntckoloccode , CAST(CONVERT(varchar(10),rntckiwhen,103) AS Datetime), rntckiloccode
		FOR XML AUTO, ELEMENTS
END


