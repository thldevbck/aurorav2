CREATE PROCEDURE [dbo].[OPL_VehicleAssignSaveAssign]
     @BookingNo VARCHAR(64) = '',
     @RentalNo VARCHAR(64) = '',
     @RentalAllowVehicleSpec VARCHAR(64) = '',
	 @UserCode VARCHAR(64) = ''
AS

BEGIN
	SET NOCOUNT ON


	 UPDATE Rental WITH (ROWLOCK)
	  SET RenAllowVehSpec =  CASE @RentalAllowVehicleSpec
			 WHEN '' THEN NULL
			 ELSE @RentalAllowVehicleSpec
			END  ,
		  ModUsrId = @UserCode ,
		  ModDateTime = CURRENT_TIMESTAMP
	 WHERE RntId IN (SELECT TOP 1 RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId
			WHERE  BooNum = @BookingNo
			AND  RntNum = @RentalNo)
	 IF (@@ERROR <> 0)
	 BEGIN
	  SELECT 'ERROR/ErrorIn Updating Rental (' + @BookingNo + '/' + @RentalNo + ')'
	  RETURN
	 END
	 SELECT 'SUCCESS/' + dbo.getErrorString('GEN046', '(' + @BookingNo + '/' + @RentalNo + ')', '', '', '', '', '')
	 RETURN

END
GO
