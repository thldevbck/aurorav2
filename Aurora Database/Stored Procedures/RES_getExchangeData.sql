CREATE                              PROCEDURE [dbo].[RES_getExchangeData]
 @sBooId    VARCHAR  (64) = '',
 @sRntId    VARCHAR  (64) = '',
 @sDefaultUser  VARCHAR  (64) = ''
AS
BEGIN
 SET NOCOUNT ON
 -- General Variable Declaration
 DECLARE
    @PrdList     VARCHAR  (4000) ,
    @Err      VARCHAR  (200) ,
    @doc      INT ,
    @bPdId     VARCHAR  (64) ,
    @sBpdUnitNumber  VARCHAR  (64) ,
    @sRego     VARCHAR  (64) ,
    @sBpdOddometerOut  BIGINT ,
    @sBpdOddometerIn  BIGINT ,
    @sUviKey     VARCHAR  (64) ,
    @sFleetModelCode  VARCHAR  (128) ,
    @sBpdUnitNum   VARCHAR  (64) ,
    @sBpdOddometerDue VARCHAR  (64) ,
    @sBpdSapId    VARCHAR  (64) ,
    @sCtyCode    VARCHAR  (64) ,
    @sCtyName    VARCHAR  (64) ,
    @sRntPkgId    VARCHAR  (64) ,
    @sRntCkoLocCode  VARCHAR  (64) ,
    @sErrStr     VARCHAR  (64) ,
    @NoOfExchange   BIGINT ,
    @bCrossHire    BIT ,
    @sPrdShortName   VARCHAR  (64) ,
    @sCodTypeId    VARCHAR  (64) ,
    @sUviKeyForOddo   VARCHAR  (256) ,
    @dTodate    DATETIME,
    @sCodTypeIdXFRM   varchar(64)

--JL 2008/02/20 declare @sFleetModelCode
 set @sFleetModelCode = ''

 -- Not part
 SET @sCodTypeId = dbo.GetCodeID('4','EXT')
 SET @sCodTypeIdXFRM = dbo.GetCodeID('4','XFRM')

 --select @sCodTypeId
 SET @bCrossHire = 0
 EXEC RES_getBookedProducts
    @sBooId     =  @sBooId,
    @sRntId     = @sRntId  ,
    @bSelectVehicleOnly =  1,
    @bSelectCurrentOnly =  1,
    @sExclStatusList   =  'XN,XX' ,
--    @sExclCodTypList = @sCodTypeId ,
    @sOrderBy    =  'BookedProduct.BpdCkoWhen DESC' ,
    @sBpdList    =  @PrdList   OUTPUT,
    @ReturnError    =  @Err    OUTPUT

/* SELECT
  @PrdList = '<bpid>' + BpdId + '</bpid>'
 FROM
  BookedProduct
 WHERE
  Current_TimeStamp >= BpdCkoWhen
  and Current_TimeStamp<=BpdCkiWhen
  and BpdRntId = @sRntId
  and BpdIsCurr = 1
  and BpdStatus NOT IN('XX','XN')

 SET @PrdList = '<Data>' + @PrdList + '</Data>'*/
 SELECT
  @NoOfExchange = COUNT(BpdId)
 FROM
  dbo.BookedProduct  WITH(NOLOCK)
 WHERE BpdStatus NOT IN ('XX','XN')
  and bpdPrdIsVehicle = 1
  and BpdIsCurr = 1
  and BpdRntId IN  (SELECT  RntId  FROM  dbo.Rental  WITH(NOLOCK)
         WHERE RntBooId = (SELECT  BooId   FROM  dbo.Booking  WITH(NOLOCK) WHERE BooId = @sBooId)
        AND RntId = @sRntId)
  and bpdCodTypeId is not null and bpdcodtypeid = @sCodTypeIdXFRM


 --SELECT @PrdList

 IF @Err IS NULL
 BEGIN
  SET  @PrdList = '<root>' + @PrdList + '</root>'
  EXEC SP_XML_PREPAREDOCUMENT @doc OUTPUT, @PrdList

  IF @@ERROR<>0
   RETURN
  DECLARE Cur_BookedProduct
     CURSOR FORWARD_ONLY  LOCAL FOR
        SELECT
         bpid
        FROM
         OPENXML(@doc, '/root/Data/bpid',3)
        WITH
        (
         bpid  VARCHAR  (64)  'text()'
        )
  OPEN Cur_BookedProduct
  EXEC SP_XML_REMOVEDOCUMENT @doc

  FETCH NEXT FROM Cur_BookedProduct INTO @bPdId


  IF @@FETCH_STATUS=0
  BEGIN

   /* GET AIMS Vehicle name if unit is not a Crosshire */
--   SELECT * FROM BookedProduct WHERE BpdId = @bPdId

   SELECT
    -- Issue 879: RKS : 18-MAY-2006
    -- START
    @NoOfExchange  = IsNull(BpdExtRef,0),
    -- END
    @sRntPkgId   = RntPkgId ,
    @sRntCkoLocCode = RntCkoLocCode ,
    @sBpdSapId   = ISNULL(BpdSapId,'') ,
    @sBpdUnitNumber  = ISNULL(BpdUnitNum,'') ,
    @sRego    = ISNULL(BpdRego,'') ,
    @sBpdOddometerOut = ISNULL(BpdOddometerOut,0) ,
    @sBpdOddometerIn = ISNULL(BpdOddometerIn,0) ,
    @sPrdShortName  = ISNULL(PrdShortName,'') + ' - ' + ISNULL(PrdName,'')
   FROM
    dbo.BookedProduct (NOLOCK),
    dbo.Rental  (NOLOCK) ,
    dbo.Product (NOLOCK),
    dbo.SaleableProduct  (NOLOCK)
   WHERE
    BpdSapId = SapId
    and PrdId = SapPrdId
    and BpdRntId = RntId
    and BpdId = @bPdId

   --SELECT @sBpdUnitNumber
   SET @sUviKey = 'CROSSHIRE_' + ISNULL((SELECT UsrCtyCode FROM dbo.UserInfo  WITH (NOLOCK) WHERE UsrCode = @sDefaultUser),'')

   IF @sBpdUnitNumber <> ''
   BEGIN
    IF EXISTS (
       SELECT
       UviKey
      FROM
       dbo.UniversalInfo (NOLOCK)
      WHERE
       UviKey = @sUviKey
        AND
       UviValue = @sBpdUnitNumber
     )
    BEGIN
     SET @bCrossHire  = 1
     SET @sFleetModelCode = '** Crosshire Vehicle'

    END
    ELSE
     IF @sRego<>''
     BEGIN
      SELECT
       @sFleetModelCode    =   RTRIM(LTRIM(FleetModelCode))  + ' - ' + LTRIM(FleetModelDescription) ,
       @sBpdOddometerDue =   ISNULL(CONVERT(VARCHAR,OdometerDue),'0.00')

      FROM
       aimsprod.dbo.FleetModel (NOLOCK),
       dbo.fleetassetview  (NOLOCK)
      WHERE
       fleetassetview.rego = @sRego
       AND fleetassetview.FleetModelId  = aimsprod.dbo.FleetModel.FleetModelId
       AND UnitNumber = RTRIM(LTRIM(@sBpdUnitNumber))

/*      SELECT
       @sFleetModelCode    = RTRIM(LTRIM(FleetModelCode))  + ' - ' + LTRIM(FleetModelDescription) ,
       @sBpdOddometerDue = ISNULL(CONVERT(VARCHAR,OdometerDue),'0.00')
      FROM
       aimsprod.dbo.FleetModel WITH(NOLOCK),
       aimsprod.dbo.FleetAsset  WITH(NOLOCK)
      WHERE
       aimsprod.dbo.FleetAssetRegistrationNumber  = @sRego
       AND aimsprod.dbo.FleetAsset.FleetModelId  = aimsprod.dbo.FleetModel.FleetModelId
       AND UnitNumber = @sBpdUnitNumber*/
     END -- IF @sRego<>''
     ELSE
      SET @sFleetModelCode = '** INVALID Vehicle'
   END -- IF @sBpdUnitNumber <> ''

   EXEC GEN_getCountryForLocation
       @LocationCode  = @sRntCkoLocCode ,
       @PackageId   = @sRntPkgId ,
       @CountryCode  = @sCtyCode  OUTPUT,
       @CountryName  = @sCtyName  OUTPUT,
       @ReturnError   = @sErrStr   OUTPUT

   SET @dTodate = dbo.GEN_getAdjustedTimeForLocation(@sRntCkoLocCode,CURRENT_TIMESTAMP)

   SET @sUviKeyForOddo =  @sCtyCode + 'CKOI_SUPROLE'
   SELECT
    RntStatus                     AS RntStCode ,
    RntId                      AS RntId ,
    @NoOfExchange                   AS NoOfExch ,
    (SELECT BooNum FROM Booking with (nolock) WHERE BooId = RntBooId) + '/' + RntNum   AS BooRntNum ,
    @bCrossHire                    AS OldCrossHire ,
    RntCkiLocCode                   AS RntCkiLocCode ,
    RntBooId                     AS BooId ,
    dbo.RES_getCheckOutStatus(RntId)              AS CheckOutSt ,
    @bPdId                     AS BpdId ,
    dbo.GEN_getCodDesc('',42,RntStatus)             AS RntStDes ,
    CASE
      WHEN RntStatus IN ('KK','NN') THEN 'Customer Request'
      ELSE 'Breakdown'
    END                      AS ExchangeTyp ,
    (SELECT UsrLocCode FROM UserInfo with (nolock) WHERE UsrCode =   @sDefaultUser)   AS Branch ,
    @sCtyCode                    AS CtyCode,
    CONVERT(VARCHAR(10), @dTodate, 103)        AS CurrDt ,
    LEFT(CONVERT(VARCHAR, @dTodate, 108),5)      AS CurrTm ,
    CONVERT(VARCHAR(10), @dTodate, 103)        AS VCurrDt ,
    LEFT(CONVERT(VARCHAR, @dTodate, 108),5)      AS VCurrTm ,
    @sBpdUnitNumber                  AS UnitNum ,
    @sRego                     AS RegNum ,
    CASE
--      WHEN @sBooId = '6163687' THEN '4BM - Spirit 4'
      WHEN RntStatus IN ('KK','NN') THEN @sPrdShortName
      ELSE  @sFleetModelCode
    END                      AS Vehicle,
    @sBpdOddometerOut                  AS OdoOut ,
    @sBpdOddometerIn                  AS OdoIn ,
    ISNULL(@sBpdOddometerDue,'')               AS OdoDue ,
    @sBpdSapId                    AS OldSapId ,
    ISNULL((SELECT UviValue FROM UniversalInfo WITH(NOLOCK) WHERE UviKey = @sUviKey),'') AS CrossHrCode ,
    RntCkoLocCode                   AS CkoLoc ,
    CONVERT(VARCHAR(10),RntCkoWhen,103) + ' ' + LEFT(CONVERT(VARCHAR,RntCkoWhen,108),5)  AS RntCkoWhen ,
    dbo.GetAmPmFromDate(RntCkoWhen)               AS RntCkoDatePart ,
    LEFT(CONVERT(VARCHAR,RntCkoWhen,108),5)             AS RntCkoTm ,
    CONVERT(VARCHAR(10),RntCkiWhen,103) + ' ' + LEFT(CONVERT(VARCHAR(10),RntCkiWhen,108),5) AS RntCkiWhen ,
    dbo.GetAmPmFromDate(RntCkiWhen)               AS RntCkiDatePart ,
    LEFT(CONVERT(VARCHAR,RntCkoWhen,108),5)             AS RntCkiTm ,
    ''                      AS CrossHire ,
    ''                      AS RUnitNum ,
    ''                      AS RRegNum ,
    ''                      AS RVehicle ,
    ''                      AS ROdoOut ,
    1                      AS AppPreRates ,
    isnull(dbo.GEN_checkUserRole(@sDefaultUser, @sCtyCode,@sUviKeyForOddo),0)    AS UviOddo,
    ''                      AS NoteText,
    ''                      AS Rate  ,
	--Added by Shoel on 19/11/7 > for the Integrity number issue
	'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo,
            '' + CAST(Booking.IntegrityNo AS VARCHAR) AS BooIntNo
   FROM
    Rental (NOLOCK)
INNER JOIN
	Booking WITH (NOLOCK)
	ON
	Rental.RntBooId = Booking.BooId
   WHERE RntId = @sRntId
   AND RntBooId = @sBooId
   FOR XML AUTO, ELEMENTS

  END -- IF @@FETCH_STATUS=0
 END -- IF @Err IS NULL
END


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON



GO
