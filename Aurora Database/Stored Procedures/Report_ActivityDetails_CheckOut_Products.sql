CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckOut_Products]
	@BookRental		VARCHAR(64)
AS

SET NOCOUNT ON

/*
DECLARE	@BookRental		VARCHAR(64)
SET		@BookRental = '3294349/1'
*/

select PrdName, Currs, Amounts
from reports.Report_ActDetCKOProducts
where BookRental = @BookRental



GO
