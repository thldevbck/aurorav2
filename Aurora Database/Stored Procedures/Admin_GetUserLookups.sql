CREATE PROCEDURE [dbo].[Admin_GetUserLookups]
AS

SELECT
	Company.*
FROM
	Company
ORDER BY
	Company.ComCode


SELECT
	Country.*
FROM
	Country
ORDER BY
	Country.CtyCode


SELECT
	Location.LocComCode,
	Country.CtyCode,
	Location.LocCode,
	Location.LocName,
	Location.LocIsActive
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
	INNER JOIN Country ON TownCity.TctCtyCode = Country.CtyCode
ORDER BY
	Location.LocCode


SELECT
	FlexExportType.*
FROM
	FlexExportType
ORDER BY
	FlexExportType.FlxComCode,
	FlexExportType.FlxCode,
	FlexExportType.FlxName


SELECT
	Role.*
FROM
	Role
ORDER BY
	Role.RolCode


SELECT
	ChildMenu.MenId,
	ISNULL (ParentMenu.MenLabel, '') + ' \ ' + ISNULL (ChildMenu.MenLabel, '') AS Text
FROM
	Menu AS ChildMenu
	INNER JOIN Menu AS ParentMenu ON ChildMenu.MenParentMenuId = ParentMenu.MenId
	INNER JOIN Functions ON ChildMenu.MenFunId = Functions.FunId
ORDER BY
	ParentMenu.MenOrder,
	ParentMenu.MenLabel,
	ChildMenu.MenOrder,
	ChildMenu.MenLabel



GO
