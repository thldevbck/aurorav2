set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[Flex2_GetFlexLevels] 
	@PkgId varchar(64),
	@SapId varchar(64),
	@TravelYrStart datetime
AS

/*
DECLARE @PkgId varchar(64)
DECLARE @SapId varchar(64)
DECLARE @TravelYrStart datetime

SET @PkgId = '642D18AD-0810-4E14-8AD8-19908E893D2A'
SET @SapId = 'EF53A20C-CC17-473D-9DAC-BA5D3237588A'
SET @TravelYrStart = '2004-01-04'
*/

SELECT 
	*
FROM 
	(
		SELECT 
			MAX (FlxEffDate) AS FlxEffDate
		FROM 
			FlexLevel
		WHERE
			FlexLevel.FlxPkgId = @PkgId
			AND FlexLevel.FlxSapId = @SapId
			AND FlexLevel.FlxTravelYrStart = @TravelYrStart
			AND FlxEffDate <= GETDATE()
	) AS MaxEffDate
	INNER JOIN FlexLevel
		ON FlexLevel.FlxEffDate = MaxEffDate.FlxEffDate
		AND FlexLevel.FlxPkgId = @PkgId
		AND FlexLevel.FlxSapId = @SapId
		AND FlexLevel.FlxTravelYrStart = @TravelYrStart
ORDER BY 
	FlxNum