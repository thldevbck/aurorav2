CREATE PROCEDURE [dbo].[schV_getMyVehicleCities] @sCtyCode	VARCHAR(64),
	@sUsrCode	VARCHAR(64)
AS
BEGIN
	DECLARE	@sComCode	VARCHAR(64),
			@sCompanyId	VARCHAR(128)

	SET @sComCode = dbo.fun_getCompany
						(
							@sUsrCode,	--@sUserCode	VARCHAR(64),
							NULL,		--@sLocCode		VARCHAR(64),
							NULL,		--@sBrdCode		VARCHAR(64),
							NULL,		--@sPrdId		VARCHAR(64),
							NULL,		--@sPkgId		VARCHAR(64),
							NULL,		--@sdummy1		VARCHAR(64),
							NULL,		--@sdummy2		VARCHAR(64),
							NULL		--@sdummy3		VARCHAR(64)
						)

	SELECT @sCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK) WHERE ComCode = @sComCode

	SELECT
		v.locationCode
--		fa.UnitNumber,
--		m.FleetModelCode,
--		v.NextDateTime,
--		fa.Rego as RegistrationNumber,
--		isnull(fa.FirstOnFleetDate,dateadd(d,-30,getdate())) as FirstOnFleetDate,
--		fa.LastOdometer ,
--		v.LocationCode,
--		fa.FleetModelId, *
	FROM
		AIMSProd.dbo.AI_FleetAsset fa
		INNER JOIN AIMSProd.dbo.FleetModel m ON fa.FleetModelId = m.FleetModelId
		INNER JOIN AIMSProd.dbo.Dvs_Vehicle v ON fa.UnitNumber = v.UnitNumber
	WHERE
		(v.CtyCode = @sCtyCode)
		AND EXISTS( SELECT FAT.FleetAssetId
					FROM AIMSProd.dbo.FleetAsset FAT (NOLOCK)
					WHERE FAT.FleetAssetId = fa.FleetAssetId
					AND CHARINDEX(CONVERT(VARCHAR,FAT.CompanyId),@sCompanyId)>0
				   )
		group by v.locationCode
	--ORDER BY m.FleetModelCode, CONVERT(bigint, v.UnitNumber)
END


--exec schV_getMyVehicleCities 'AU','rs1'




GO
