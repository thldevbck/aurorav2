USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_FinalizeBookingWeek]    Script Date: 11/19/2007 13:42:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Flex2_FinalizeBookingWeek] 
	@FbwId int,
	@IntegrityNo int,
	@UsrId varchar(64)

AS

UPDATE FlexBookingWeek SET
	FlexBookingWeek.FbwStatus = 'sent',
	FlexBookingWeek.IntegrityNo = @IntegrityNo + 1,
	FlexBookingWeek.ModUsrId = @UsrId,
	FlexBookingWeek.ModDateTime = GETDATE()
WHERE
	FlexBookingWeek.FbwId = @FbwId
	AND FlexBookingWeek.IntegrityNo = @IntegrityNo
	AND FlexBookingWeek.FbwStatus = 'approved'

IF @@ROWCOUNT <= 0 RETURN

SELECT 
	* 
FROM 
	FlexBookingWeek 
WHERE	
	FbwId = @FbwId



