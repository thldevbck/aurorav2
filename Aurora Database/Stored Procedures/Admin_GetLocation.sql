CREATE PROCEDURE [dbo].[Admin_GetLocation]
	@ComCode AS varchar(64),
	@LocCode AS varchar(12),
	@LocCodeSearch AS varchar(12),
	@LocCodTypId AS varchar(64),
	@CtyCode AS varchar(12),
	@TctCode AS varchar(12),
	@ConName AS varchar(64),
	@LocIsActive AS bit

AS

-- EXEC Admin_GetLocation 'THL', 'HBT', NULL, NULL, NULL, NULL, NULL, NULL

SELECT
	Location.*
INTO
	#locationSearch
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
WHERE
	(@ComCode IS NULL OR Location.LocComCode = @ComCode)
	AND (@LocCode IS NULL OR Location.LocCode = @LocCode)
	AND (@LocCodeSearch IS NULL OR Location.LocCode LIKE (@LocCodeSearch + '%'))
	AND (@LocCodTypId IS NULL OR Location.LocCodTypId = @LocCodTypId)
	AND (@CtyCode IS NULL OR TownCity.TctCtyCode = @CtyCode)
	AND (@TctCode IS NULL OR TownCity.TctCode = @TctCode)
	AND (@ConName IS NULL OR EXISTS (SELECT 1 FROM Contact WHERE Contact.ConPrntId = Location.LocCode AND Contact.ConPrntTableName = 'Location' AND Contact.ConName LIKE ('%' + @ConName + '%')))
	AND (@LocIsActive IS NULL OR Location.LocIsActive = @LocIsActive)


SELECT
	#locationSearch.*
FROM
	#locationSearch
ORDER BY
	#locationSearch.LocCode


SELECT
	AddressDetails.*
FROM
	#locationSearch
	INNER JOIN AddressDetails ON AddressDetails.AddPrntID = #locationSearch.LocCode AND AddressDetails.AddPrntTableName = 'Location'
ORDER BY
	#locationSearch.LocCode


SELECT
	Contact.*
FROM
	#locationSearch
	INNER JOIN Contact ON Contact.ConPrntId = #locationSearch.LocCode AND Contact.ConPrntTableName = 'Location'
ORDER BY
	#locationSearch.LocCode


SELECT
	PhoneNumber.*
FROM
	#locationSearch
	INNER JOIN PhoneNumber ON PhoneNumber.PhnPrntId = #locationSearch.LocCode AND PhoneNumber.PhnPrntTableName IN ('Location', 'LocationAir', 'LocationDomAir', 'LocationThird')


SELECT
	LocationCheckInOutTime.*
FROM
	#locationSearch
	INNER JOIN LocationCheckInOutTime ON LocationCheckInOutTime.lioLocCode = #locationSearch.LocCode


DROP TABLE #locationSearch


GO
