CREATE PROCEDURE [dbo].[Product_GetRates]
(
	@ComCode AS varchar(64),
	@PtbId AS varchar(64)
)
AS

/*
	This SP gets the rates for a specific Travel Band
*/

DECLARE @Params table (
	ComCode varchar(64),
	PrdId varchar(64),
	SapId varchar(64),
	TbsId varchar(64),
	PtbId varchar(64)
)

INSERT INTO @Params
(
	ComCode,
	PrdId,
	SapId,
	TbsId,
	PtbId
)
SELECT
	Brand.BrdComCode AS ComCode,
	Product.PrdId,
	SaleableProduct.SapId,
	TravelBandSet.TbsId AS TbsId,
	ProductRateTravelBand.PtbId AS PtbId
FROM
	Brand
	INNER JOIN Product ON Product.PrdBrdCode = Brand.BrdCode
	INNER JOIN SaleableProduct ON SaleableProduct.SapPrdId = Product.PrdId
	INNER JOIN TravelBandSet ON TravelBandSet.TbsSapId = SaleableProduct.SapId
	INNER JOIN ProductRateTravelBand ON ProductRateTravelBand.PtbTbsId = TravelBandSet.TbsId
WHERE
	Brand.BrdComCode = @ComCode
	AND ProductRateTravelBand.PtbId = @PtbId

/*********************************************************************************************/
/* 0 - Agent */
/*********************************************************************************************/
SELECT
	Agent.*
FROM
	Agent
WHERE
	EXISTS
	(
		SELECT 1
		FROM
			AgentDependentRate
			INNER JOIN @Params AS p ON p.PtbId = AgentDependentRate.AdrPtbId
			WHERE
				AgentDependentRate.AdrAgnId = Agent.AgnId
	)

/*********************************************************************************************/
/* 1 - AgentDependentRate */
/*********************************************************************************************/
SELECT
	AgentDependentRate.*
FROM
	AgentDependentRate
	INNER JOIN @Params AS p ON p.PtbId = AgentDependentRate.AdrPtbId

/*********************************************************************************************/
/* 2 - PersonRate */
/*********************************************************************************************/
SELECT
	PersonRate.*
FROM
	PersonRate
	INNER JOIN @Params AS p ON p.PtbId = PersonRate.PrrPtbId

/*********************************************************************************************/
/* 3 - PersonDependentRate */
/*********************************************************************************************/
SELECT
	PersonDependentRate.*
FROM
	PersonDependentRate
	INNER JOIN @Params AS p ON p.PtbId = PersonDependentRate.PdrPtbId

/*********************************************************************************************/
/* 4 - DiscountProduct (Product) */
/*********************************************************************************************/
SELECT
	Product.*
FROM
	Product
WHERE
	EXISTS
	(
		SELECT 1
		FROM
			ProductRateDiscount
			INNER JOIN @Params AS p ON p.PtbId = ProductRateDiscount.PdsPtbId
		WHERE
			ProductRateDiscount.PdsFreeNonProduct = Product.PrdId
			OR ProductRateDiscount.PdsPrdFreeId = Product.PrdId
	)

/*********************************************************************************************/
/* 5 - ProductRateDiscount */
/*********************************************************************************************/
SELECT
	ProductRateDiscount.*
FROM
	ProductRateDiscount
	INNER JOIN Code ON Code.CodId = ProductRateDiscount.PdsCodRtyId
	INNER JOIN @Params AS p ON p.PtbId = ProductRateDiscount.PdsPtbId
ORDER BY
	ProductRateDiscount.PdsPtbId,
	Code.CodCode,
	ProductRateDiscount.PdsQty,
	ProductRateDiscount.PdsFromDayOfWeek,
	ProductRateDiscount.PdsFromHire

/*********************************************************************************************/
/* 6 - ProductRateBookingBand */
/*********************************************************************************************/
SELECT
	ProductRateBookingBand.*
FROM
	ProductRateBookingBand
	INNER JOIN @Params AS p ON p.PtbId = ProductRateBookingBand.BapPtbId
ORDER BY
	ProductRateBookingBand.BapBookedFromDate DESC

/*********************************************************************************************/
/* 7 - LocationDependentRate */
/*********************************************************************************************/
SELECT
	LocationDependentRate.*
FROM
	LocationDependentRate
	INNER JOIN ProductRateBookingBand ON ProductRateBookingBand.BapId = LocationDependentRate.LdrBapId
	INNER JOIN @Params AS p ON p.PtbId = ProductRateBookingBand.BapPtbId
ORDER BY
	ProductRateBookingBand.BapBookedFromDate DESC,
	ISNULL (LocationDependentRate.LdrLocFromCode, ''),
	ISNULL (LocationDependentRate.LdrLocToCode, '')

/*********************************************************************************************/
/* 8 - VehicleDependentRate */
/*********************************************************************************************/
SELECT
	VehicleDependentRate.*
FROM
	VehicleDependentRate
	LEFT JOIN Product ON Product.PrdId = VehicleDependentRate.VdrPrdId
	INNER JOIN @Params AS p ON p.PtbId = VehicleDependentRate.VdrPtbId
ORDER BY
	ISNULL (Product.PrdShortName, '')


/*********************************************************************************************/
/* 9 - FixedRate */
/*********************************************************************************************/
SELECT
	FixedRate.*
FROM
	FixedRate
	INNER JOIN @Params AS p ON p.PtbId = FixedRate.FfrPtbId

/*********************************************************************************************/



GO
