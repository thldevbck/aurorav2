set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[Flex2_RejectWeekProduct] 
	@FlpId AS int,
	@IntegrityNo AS tinyint,
	@UsrId AS varchar(64)
AS

/* ===========================================================================*/
/* Update the Product */
/* ===========================================================================*/

DECLARE @FbwId AS int

UPDATE FlexBookingWeekProduct
SET 
	@FbwId = FlpFbwId,
	FlpStatus = 'unapproved',
	IntegrityNo = @IntegrityNo + 1,
	ModUsrId = @UsrId,
	ModDateTime = GETDATE()
FROM
	FlexBookingWeekProduct 
WHERE
	FlexBookingWeekProduct.FlpId = @FlpId
	AND FlexBookingWeekProduct.IntegrityNo = @IntegrityNo

IF @@ROWCOUNT <= 0 RETURN

/* ===========================================================================*/
/* Calculate Previous Booking Week Id */
/* ===========================================================================*/

PRINT 'Calculate Previous Booking Week Id'

DECLARE @PrevFbwId AS int

SELECT TOP 1
	@PrevFbwId = PrevFlexBookingWeek.FbwId
FROM 
	FlexBookingWeek
	INNER JOIN FlexBookingWeek AS PrevFlexBookingWeek 
		ON PrevFlexBookingWeek.FbwBookStart < FlexBookingWeek.FbwBookStart
		AND PrevFlexBookingWeek.FbwComCode = FlexBookingWeek.FbwComCode
WHERE
	FlexBookingWeek.FbwId = @FbwId
ORDER BY 
	PrevFlexBookingWeek.FbwBookStart DESC


/* ===========================================================================*/
/* Delete the Previous Rates and Exceptions */
/* ===========================================================================*/
DELETE FROM FlexBookingWeekRate WHERE FlrFlpId = @FlpId
DELETE FROM FlexBookingWeekException WHERE FleFlpId = @FlpId

/* ===========================================================================*/
/* Create Product Rates (from previous week, or else default to A1) */
/* ===========================================================================*/

PRINT 'Create Product Rates'

INSERT INTO FlexBookingWeekRate
(
	FlrFlpId,
	FlrWkNo,
	FlrTravelFrom,
	FlrFlexNum,
	FlrChanged
)
SELECT 
	FlexBookingWeekProduct.FlpId AS FlrFlpId,
	FlexWeekNumber.FwnWkNo AS FlrWkNo,
	FlexWeekNumber.FwnTrvWkStart AS FlrTravelFrom,
	CASE WHEN PrevFlexBookingWeekRate.FlrId IS NOT NULL THEN PrevFlexBookingWeekRate.FlrFlexNum ELSE 11 END AS FlrFlexNum,
	CASE WHEN PrevFlexBookingWeekRate.FlrId IS NOT NULL THEN 0 ELSE 1 END AS FlrChanged
FROM 
	(SELECT 	
		@FbwId AS FbwId,
		@FlpId AS FlpId,
		@PrevFbwId AS PrevFbwId,
		@UsrId AS UsrId
	) Params
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpId = Params.FlpId
	INNER JOIN FlexWeekNumber
		ON FlexWeekNumber.FwnTrvYearStart = FlexBookingWeekProduct.FlpTravelYear
	LEFT JOIN FlexBookingWeekProduct AS PrevFlexBookingWeekProduct
		ON PrevFlexBookingWeekProduct.FlpFbwId = @PrevFbwId
		AND PrevFlexBookingWeekProduct.FlpComCode = FlexBookingWeekProduct.FlpComCode
		AND PrevFlexBookingWeekProduct.FlpCtyCode = FlexBookingWeekProduct.FlpCtyCode
		AND PrevFlexBookingWeekProduct.FlpBrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND PrevFlexBookingWeekProduct.FlpTravelYear = FlexBookingWeekProduct.FlpTravelYear
		AND PrevFlexBookingWeekProduct.FlpPrdId = FlexBookingWeekProduct.FlpPrdId  
	LEFT JOIN FlexBookingWeekRate AS PrevFlexBookingWeekRate
		ON PrevFlexBookingWeekRate.FlrFlpId = PrevFlexBookingWeekProduct.FlpId
		AND PrevFlexBookingWeekRate.FlrWkNo = FlexWeekNumber.FwnWkNo
ORDER BY
	FlexBookingWeekProduct.FlpId,
	FlexWeekNumber.FwnWkNo

/* ===========================================================================*/
/* Create Exceptions (from previous week) */
/* ===========================================================================*/

PRINT 'Create Exceptions'

INSERT INTO FlexBookingWeekException
(
	FleFlpId,
	FleBookStart,
	FleBookEnd,
	FleTravelStart,
	FleTravelEnd,
	FleLocCodeFrom,
	FleLocCodeTo,
	FleDelta
)
SELECT 
	FlexBookingWeekProduct.FlpId AS FleFlpId,
	PrevFlexBookingWeekException.FleBookStart AS FleBookStart,  
	PrevFlexBookingWeekException.FleBookEnd AS FleBookEnd,    
	PrevFlexBookingWeekException.FleTravelStart AS FleTravelStart,
	PrevFlexBookingWeekException.FleTravelEnd AS FleTravelEnd,  
	PrevFlexBookingWeekException.FleLocCodeFrom AS FleLocCodeFrom,
	PrevFlexBookingWeekException.FleLocCodeTo AS FleLocCodeTo,  
	PrevFlexBookingWeekException.FleDelta AS FleDelta
FROM 
	(SELECT 	
		@FbwId AS FbwId,
		@FlpId AS FlpId,
		@PrevFbwId AS PrevFbwId,
		@UsrId AS UsrId
	) Params
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpId = Params.FlpId
	INNER JOIN FlexBookingWeekProduct AS PrevFlexBookingWeekProduct
		ON PrevFlexBookingWeekProduct.FlpFbwId = Params.PrevFbwId
		AND PrevFlexBookingWeekProduct.FlpComCode = FlexBookingWeekProduct.FlpComCode
		AND PrevFlexBookingWeekProduct.FlpCtyCode = FlexBookingWeekProduct.FlpCtyCode
		AND PrevFlexBookingWeekProduct.FlpBrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND PrevFlexBookingWeekProduct.FlpTravelYear = FlexBookingWeekProduct.FlpTravelYear
		AND PrevFlexBookingWeekProduct.FlpPrdId = FlexBookingWeekProduct.FlpPrdId  
	INNER JOIN FlexBookingWeekException AS PrevFlexBookingWeekException
		ON PrevFlexBookingWeekException.FleFlpId = PrevFlexBookingWeekProduct.FlpId
ORDER BY 
	FlexBookingWeekProduct.FlpId,
	PrevFlexBookingWeekException.FleLocCodeFrom,
	PrevFlexBookingWeekException.FleLocCodeTo

/* ===========================================================================*/
/* Return Product */
/* ===========================================================================*/

SELECT 
	* 
FROM 
	FlexBookingWeekProduct 
WHERE 
	FlpId = @FlpId













