CREATE PROCEDURE [dbo].[Package_GetPopUpData]
	@case varchar(30)	=	'',
	@param1 varchar(64)	=	'',
	@param2 varchar(64)	=	'',
	@param3 varchar(500)	=	'',
	@param4 varchar(64)	=	'' ,
	@param5 varchar(64)  = '',
	@sUsrCode varchar(64) = ''
AS

IF (@case = 'PACKAGE_SALEABLEPRODUCT') AND (LTRIM (ISNULL (@param2, '')) = '')
BEGIN
	SELECT
		POPUP.SapId AS ID,
		Product.PrdShortName + '-' + CAST (POPUP.SapSuffix AS varchar(3)) AS CODE,
		('' + Product.PrdName) AS DESCRIPTION,
		Brand.BrdCode + '-' + Brand.BrdName AS BRAND,
		('' + POPUP.SapStatus) AS STATUS,
		Product.PrdShortName,
		POPUP.SapSuffix
	FROM
		SaleableProduct AS POPUP
		INNER JOIN Product ON POPUP.SapPrdId = Product.PrdId
		INNER JOIN Brand ON Product.PrdBrdCode = Brand.BrdCode
		INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
		INNER JOIN UserCompany ON UserCompany.ComCode = Company.ComCode
		INNER JOIN UserInfo ON UserInfo.UsrId = UserCompany.UsrId
	WHERE
		((Product.PrdShortName + '-' + CAST (POPUP.SapSuffix AS varchar(3))) LIKE (@param1 + '%') OR (Product.PrdName LIKE (@param1 + '%')))
		AND UserInfo.UsrCode = @sUsrCode
	ORDER BY
		Product.PrdShortName,
		POPUP.SapSuffix DESC
	FOR XML AUTO, ELEMENTS

	RETURN
END

IF(@case = 'PACKAGE_SALEABLEPRODUCT') AND (LTRIM (ISNULL (@param2, '')) <> '')
BEGIN
	SELECT
		POPUP.SapId AS ID,
		Product.PrdShortName + '-' + CAST (POPUP.SapSuffix AS varchar(3)) AS CODE,
		('' + Product.PrdName) AS DESCRIPTION,
		ProductBrand.BrdCode + '-' + ProductBrand.BrdName AS BRAND,
		('' + POPUP.SapStatus) AS STATUS,
		Product.PrdShortName,
		POPUP.SapSuffix
	FROM
		SaleableProduct AS POPUP
		INNER JOIN Product ON POPUP.SapPrdId = Product.PrdId
		INNER JOIN Brand AS ProductBrand ON Product.PrdBrdCode = ProductBrand.BrdCode

		INNER JOIN Package ON POPUP.SapCtyCode = Package.PkgCtyCode
		INNER JOIN Brand AS PackageBrand ON Package.PkgBrdCode = PackageBrand .BrdCode

		INNER JOIN Brand ON (Brand.BrdCode = ProductBrand.BrdCode OR ProductBrand.BrdIsGeneric = 1) AND (Brand.BrdCode = PackageBrand.BrdCode OR PackageBrand.BrdIsGeneric = 1)
		INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
		INNER JOIN UserCompany ON UserCompany.ComCode = Company.ComCode
		INNER JOIN UserInfo ON UserInfo.UsrId = UserCompany.UsrId
	WHERE
		((Product.PrdShortName + '-' + CAST (POPUP.SapSuffix AS varchar(3))) LIKE (@param1 + '%') OR (Product.PrdName LIKE (@param1 + '%')))
		AND Package.PkgId = @Param2
		AND UserInfo.UsrCode = @sUsrCode
	ORDER BY
		Product.PrdShortName,
		POPUP.SapSuffix DESC
	FOR XML AUTO, ELEMENTS

	RETURN
END

IF(@case = 'PACKAGE_AGENTGROUP')
BEGIN
	SELECT
		AgpId AS ID,
		AgpCode	AS CODE,
		AgpDesc	AS DESCRIPTION
	FROM
		AgentGroup AS POPUP
	WHERE
		AgpCode LIKE (@param1 + '%') OR AgpDesc LIKE (@param1 + '%')
	FOR XML AUTO, ELEMENTS

	RETURN
END





GO
