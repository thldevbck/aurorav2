CREATE       PROCEDURE [dbo].[GEN_getMsgToClientNew]
	@sErrorCodes varchar(1000) = ''
AS
/*
SET NOCOUNT ON
DECLARE @start INT,
		@count INT
DECLARE	@TT TABLE (Code varchar(250))
SET @start = 1
SET @count =	CASE
					WHEN ISNULL(@sErrorCodes, '') = '' THEN 0 ELSE dbo.getEntry(@sErrorCodes, ',') + 1
				END

WHILE @start <= @count
BEGIN
	INSERT INTO @TT(Code) VALUES(dbo.getsplitedData(@sErrorCodes, ',', @start))
	SET @start = @start + 1
END

SELECT 	SmsCode	+ ' - ' + SmsMsgText AS	'Msg'
	FROM Messages WITH(NOLOCK)
	WHERE SmsCode IN (SELECT Code FROM @TT)
	FOR XML AUTO,ELEMENTS

*/
SET NOCOUNT ON
DECLARE @start INT,
		@count INT,
		@RntMsg	VARCHAR(5000),
		@MsgCodCurr	VARCHAR(24)

SET @start = 1
SET @count =	CASE
					WHEN ISNULL(@sErrorCodes, '') = '' THEN 0 ELSE dbo.getEntry(@sErrorCodes, ',') + 1
				END

SET @RntMsg = ''--'<Message>'
WHILE @start <= @count
BEGIN
	SET @MsgCodCurr = dbo.getsplitedData(@sErrorCodes, ',', @start)
	SET @RntMsg = @RntMsg + '<Msg>' + (SELECT 	'<Code>' + SmsCode	+ '</Code><Text>' + SmsMsgText + '</Text>'
											FROM Messages WITH(NOLOCK)
											WHERE SmsCode = @MsgCodCurr)+ '</Msg>'
	SET @start = @start + 1
END
--SET @RntMsg = @RntMsg + '</Message>'
SELECT @RntMsg








GO
