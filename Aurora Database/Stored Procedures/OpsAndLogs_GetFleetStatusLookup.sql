CREATE PROCEDURE [dbo].[OpsAndLogs_GetFleetStatusLookup]
	@ComCode AS varchar(64)
AS

-- EXEC OpsAndLogs_GetFleetStatusLookup 'THL'

SELECT
	FlexWeekNumber.FwnTrvYearStart,
	Country.CtyCode,
	Country.CtyName,
	Brand.BrdCode,
	Brand.BrdName
FROM
	(
		SELECT
			FlexWeekNumber.FwnTrvYearStart
		FROM
			FlexWeekNumber
		GROUP BY
			FlexWeekNumber.FwnTrvYearStart
	) AS FlexWeekNumber
	INNER JOIN Country ON (1=1)
	INNER JOIN Brand ON (1=1)
WHERE
	Country.CtyHasProducts = 1
	AND Brand.BrdComCode = @ComCode
	AND EXISTS
	(
		SELECT 1
		FROM
			Product
			INNER JOIN SaleableProduct ON  SaleableProduct.SapPrdId = Product.PrdId
			INNER JOIN PackageProduct ON SaleableProduct.SapId = PackageProduct.PplSapId
			INNER JOIN Package ON PackageProduct.PplPkgId = Package.PkgId
		WHERE
			Product.PrdBrdCode = Brand.BrdCode
			AND Product.PrdIsVehicle = 1
			--AND Product.PrdIsActive = 1
			AND ISNULL (Product.PrdToAppearonFS, 0) = 1
			AND SaleableProduct.SapCtyCode = Country.CtyCode
			AND Package.PkgTravelToDate >= FlexWeekNumber.FwnTrvYearStart
			AND Package.PkgTravelFromDate <  DATEADD (year, 1, FlexWeekNumber.FwnTrvYearStart)
			--AND Package.PkgIsActive = 'ACTIVE'
	)
ORDER BY
	FlexWeekNumber.FwnTrvYearStart DESC,
	Country.CtyCode,
	Brand.BrdCode




GO
