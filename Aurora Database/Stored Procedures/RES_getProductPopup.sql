set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



-- 	QUERY FOR RES_GETPRODUCT.SQL
/*------------------------------------------------------------------------------------------
	Copyright (C) Tourism Holdings Limited as an unpublished work. All rights reserved.

	STORED PROCEDURE:		RES_getProduct

	This stored procedure gets all the records in the feature table mainely used for Vehicle request Screen 

	The SP is passed:
		@param1 = @PrdCode	Product Short name/name
		@param2 = @BrdCode	Brand Code
		@param3 = @ClsId		Class Id
		@param4 = @TypId		Type Id
		@param5 = @ctyTravel	Country of travel
		@ftrId = ''		Feature id in comma seperated list
	The SP returns:
		None
------------------------------------------------------------------------------------------*/
Alter      PROCEDURE [dbo].[RES_getProductPopup] 
	@case	VARCHAR(30)	=	'',
	@param1	VARCHAR(64)	=	'',
	@param2	VARCHAR(64)	=	'',
	@param3	VARCHAR(500)	=	'',
	@param4 VARCHAR(64)	=	'' ,
	@param5 VARCHAR(64)  = '',
	@sUsrCode varchar(64) = ''

--	@PrdCode	varchar(64) = '',
--	@BrdCode	varchar(64) = '',
--	@ClsId		varchar(64) = '',
--	@TypId		varchar(64) = '',
--	--@ctyTravel	varchar(64) = '',
--	@sBranch	varchar(64) = '',
--	@ftrId		varchar(8000)='',
--	@sCkiDate 	VARCHAR(64),

AS

--	LOCAL VARIABLES
SET NOCOUNT ON
DECLARE @RlID		VARCHAR(64)
DECLARE	@start		INT
DECLARE	@count		INT
DECLARE	@productFeatrue TABLE (FtrId VARCHAR(64))
DECLARE @sUvValue			varchar	 (64) 
DECLARE @VhrCkiDate		DATETIME
DECLARE @ctyTravel		VARCHAR(64)
DECLARE @userCty		VARCHAR(64)

-- KX Changes :RKS
DECLARE	@sComCode		VARCHAR(64)
declare @ftrId	varchar(8000)
declare @sCkiDate 	VARCHAR(64)

set @ftrId = ''

IF ISNULL(@param5,'') <> ''
BEGIN
	SELECT 	@ctyTravel = TctCtyCode
	 FROM 	Location WITH(NOLOCK), 
			TownCity WITH(NOLOCK)
	 WHERE 	LocTctCode = TctCode
	 AND 	LocCode = @param5
END
ELSE
BEGIN
	SET @ctyTravel = ''
END

-- KX Changes :RKS
SELECT 	@userCty = UsrCtyCode,
		@sComCode = Company.ComCode
FROM	UserInfo WITH(NOLOCK) 
INNER JOIN dbo.UserCompany (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId
INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = UserCompany.ComCode
WHERE	UsrCode = @sUsrCode

SET @VhrCkiDate = CONVERT(DATETIME, ISNULL(@sCkiDate,''), 103)

SET @sUvValue = 0
SELECT 	@sUvValue = UviValue 
 FROM 	UniversalInfo (NOLOCK) 
 WHERE UviKey  = @userCty + 'MinCheckDays'

IF EXISTS(SELECT @param1 FROM Product WITH(NOLOCK)
			WHERE	PrdShortName = @param1
			AND		PrdIsActive = 1
			-- KX Changes :RKS
			AND PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
									INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = ComCode
									AND ComCode = @sComCode)
		)
BEGIN
	SELECT 	PrdId			AS	'ID',
			PrdName			AS	'DESCRIPTION',
			PrdShortName 	AS	'CODE'
		FROM PRODUCT POPUP WITH (NOLOCK) 
		WHERE	PrdShortName = @param1
		AND		PrdIsActive = 1
		AND		EXISTS(SELECT SapId FROM SaleableProduct WITH (NOLOCK)
						WHERE	SapPrdId = PrdId
						AND		SapStatus = 'Active')
		-- KX Changes :RKS
		AND PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
								INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = ComCode
								AND ComCode = @sComCode)
		FOR XML AUTO,ELEMENTS
	RETURN
END
-- modified for issue 244 by James on 20/06/2003
IF (@ftrId IS NOT NULL AND @ftrId <> '')
BEGIN
	SET @start = 1
	SET @count = dbo.getEntry(@ftrId, ',') + 1
	WHILE @start <= @count
	BEGIN
		INSERT INTO @productFeatrue VALUES(dbo.getSplitedData(@ftrId, ',', @start))
		SET @start = @start + 1
	END -- WHILE @start <= @count

	SELECT 	PrdId			AS	'ID',
			PrdName			AS	'DESCRIPTION',
			PrdShortName 	AS	'CODE'
	 FROM 	PRODUCT POPUP WITH (NOLOCK) 
	 WHERE	(PrdName LIKE @param1 + '%' OR PrdShortName LIKE	@param1 + '%')
	 AND	(PrdBrdCode = @param2 OR ISNULL(@param2,'') = '')  
	 AND	(PrdTypId = @param4 OR ISNULL(@param4,'') = '') 
	 AND	PrdTypId	LIKE	@param4 + '%'
	 AND	PrdIsActive = 1
	 AND	PrdIsVehicle	= 1
	 AND	EXISTS(SELECT	PftPrdId 
					FROM 	ProductFeature WITH(NOLOCK)
					WHERE	PrdId = PftPrdId
					AND		PftFtrId IN (SELECT FtrId FROM @productFeatrue))
	 AND 	EXISTS(SELECT	SapId
					FROM 	SaleableProduct WITH(NOLOCK), 
							PackageProduct 	WITH(NOLOCK), 
							Package 		WITH(NOLOCK) 
					WHERE 	(SapCtyCode = @ctyTravel OR @ctyTravel = '')
					AND 	PplSapId = SapId 

					AND 	PplPkgId = PkgId
					AND 	@VhrCkiDate <= PkgTravelToDate + CAST(@sUvValue AS INT)
					AND 	PkgBookedToDate + CAST(@sUvValue AS INT) > CURRENT_TIMESTAMP
					AND 	PrdId = SapPrdId
					AND		SapStatus = 'Active')

	AND 	(EXISTS(SELECT TypId FROM Type WHERE TypClaId  = @param3 AND PrdTypId = TypId) OR ISNULL(@param3,'') = '')  
	-- KX Changes :RKS
	AND PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
							INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = ComCode
							AND ComCode = @sComCode)
	FOR XML AUTO,ELEMENTS
END -- IF (@ftrId IS NOT NULL AND @ftrId <> '')
ELSE
BEGIN
	SELECT 	PrdId			AS	'ID',
			PrdName 		AS 	'DESCRIPTION',
			PrdShortName 	AS	'CODE'
	 FROM 	PRODUCT POPUP WITH (NOLOCK) 
	 WHERE	(PrdName LIKE @param1 + '%' OR	PrdShortName LIKE @param1 + '%')
	 AND	(PrdBrdCode = @param2 OR ISNULL(@param2,'') = '')  
	 AND	(PrdTypId = @param4 OR ISNULL(@param4,'') = '') 
	 AND	PrdIsVehicle	= 1
	 AND	PrdIsActive 	= 1
	 AND 	EXISTS(SELECT	SapId
					FROM 	SaleableProduct WITH(NOLOCK), 
							PackageProduct 	WITH(NOLOCK), 
							Package 		WITH(NOLOCK)
					WHERE 	(SapCtyCode = @ctyTravel OR @ctyTravel = '')
					AND 	PplSapId = SapId 
					AND 	PplPkgId = PkgId
					AND 	@VhrCkiDate <= PkgTravelToDate + CAST(@sUvValue AS INT)
					AND 	PkgBookedToDate + CAST(@sUvValue AS INT) > CURRENT_TIMESTAMP
					AND 	PrdId = SapPrdId
					AND		SapStatus = 'Active')
	-- KX Changes :RKS
	AND PrdBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) 
							INNER JOIN dbo.Company (NOLOCK) ON Brand.BrdComCode = ComCode
							AND ComCode = @sComCode)

	AND 	(EXISTS(SELECT TypId FROM Type WHERE TypClaId  = @param3  AND PrdTypId = TypId) OR ISNULL(@param3,'') = '')  
	FOR XML AUTO,ELEMENTS
END
-- end modification for issue 244



