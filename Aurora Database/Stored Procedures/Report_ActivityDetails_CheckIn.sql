CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckIn]
	@sCtyCode		VARCHAR(64),
	@sLocCode		VARCHAR(64) = '',
	@sFromDate		VARCHAR(12),
	@sToDate		VARCHAR(12),
	@sBrdCode		VARCHAR(10) = '',
	@sClaCode		VARCHAR(12) = '',
	@sTypCode		VARCHAR(12) = '',
	@sPrdShortName	VARCHAR(8) = ''
AS


SET NOCOUNT ON
/*
DECLARE	@sCtyCode		VARCHAR(64),
		@sLocCode		VARCHAR(64),
		@sFromDate		VARCHAR(12),
		@sToDate		VARCHAR(12),
		@sBrdCode		VARCHAR(10),
		@sClaCode		VARCHAR(12),
		@sTypCode		VARCHAR(12),
		@sPrdShortName	VARCHAR(8)

SET		@sCtyCode = 'NZ'
SET		@sLocCode = ''
SET		@sFromDate = '01/04/2008'
SET		@sToDate = '11/04/2008'
SET		@sBrdCode = ''
SET		@sClaCode = ''
SET		@sTypCode = ''
SET		@sPrdShortName = ''
*/

-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND EXISTS (SELECT Top 1 * FROM Product WHERE Product.PrdTypId = Type.TypId
										AND Product.PrdIsActive = 1 AND Product.PrdIsVehicle = 1)
							AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

--SET DATEFORMAT dmy
DECLARE @AU_THRIFTYPRDS VARCHAR(1000),
		@AU_THRIFTYEFFDT datetime,
		@dRntCkoWhen   	datetime,
		@dRntCkiWhen	datetime

SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

-- Declare check out result variable table
declare @Result_CheckIn table
(
	BookRental		VARCHAR(64),
	CkiLocCode		VARCHAR(64),
	Brand 			VARCHAR(64),
	BkdVehicle		VARCHAR(64),
	UnitNum 		VARCHAR(64),
	SchedVehicle	VARCHAR(64),
	CustName		VARCHAR(64),
	IsVip			VARCHAR(1),
	Pax				VARCHAR(64),
	FromDate		VARCHAR(20),
	ToDate			VARCHAR(20),
	Period			VARCHAR(20),
	PkgCode			VARCHAR(64),
	Note			VARCHAR(4000)
)

-- Create or purge products table
IF NOT EXISTS
(
	SELECT TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = 'Report_ActDetCKIProducts'
)
	BEGIN
		CREATE TABLE Report_ActDetCKIProducts
		(
			BookRentalIn	VARCHAR(64),
			PrdNameIn		VARCHAR(64),
			CurrsIn			VARCHAR(20),
			Amounts			VARCHAR(64)
		)
	END
ELSE
	DELETE reports.Report_ActDetCKIProducts

-- Create or purge payment table
IF NOT EXISTS
(
	SELECT TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = 'Report_ActDetCKIPayment'
)
	BEGIN
		CREATE TABLE Report_ActDetCKIPayment
		(
			BookRentalIn	VARCHAR(64),
			Payment			VARCHAR(64),
			Curr			VARCHAR(64),
	 		CashBondAmount	VARCHAR(64),
	 		CashBondCurr	VARCHAR(20)
		)
	END
ELSE
	DELETE reports.Report_ActDetCKIPayment

-- Variables for Cur_Note
DECLARE @sNote		VARCHAR(2000),
		@sDateTime	VARCHAR(20),
		@Note		VARCHAR(4000)

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME

SET @StartDate	=	CONVERT(DATETIME, @sFromDate, 103)
SET @EndDate	=	CONVERT(DATETIME, @sToDate+' 23:59:59', 103)
SET @WkDATE		=	CONVERT(DATETIME, @sFromDate, 103)

DECLARE @BpdIdIn 		VARCHAR(1000),	@ReturnErrorIn	 	VARCHAR(500),
		@sUnitNumIn 	VARCHAR(64),	@sSchedVehicleIn	VARCHAR(64),
		@sCustNameIn	VARCHAR(64),	@sPaxIn				int,
		@sDeptRefIn		VARCHAR(64),	@sDateIn			VARCHAR(64),
		@sConWhenIn		DATETIME,		@sFromIn			VARCHAR(20),
		@sToIn			VARCHAR(20),	@sRntCodUomIdIn 	VARCHAR(64),
		@sPeriodIn		VARCHAR(20),	@sPkgCodeIn			VARCHAR(64),
		@sBooNumIn		VARCHAR(64),	@sRntNumIn			VARCHAR(64),
		@sBookRentalIn	VARCHAR(64),	@sBookingIdIn		VARCHAR(64),
		@sCurrIdIn 		VARCHAR(64),	@sAmountIn			MONEY,
		@sBrdIn			VARCHAR(10),	@sPrdIn				VARCHAR(64),
		@sRentalIdIn	VARCHAR(64),	@BpdId				VARCHAR(100)

CREATE TABLE #TT_CheckIn( RntId	VARCHAR(64))

DECLARE	@sql VARCHAR(5000)
SELECT @sql = 'INSERT INTO #TT_CheckIn (RntId)
						SELECT RntId FROM Rental WITH(NOLOCK), Package WITH(NOLOCK)
							WHERE	RntPkgId = PkgId
							AND		RntStatus IN (''CO'',''KK'', ''CI'')
							AND		RntCkiWhen BETWEEN ''' + @sFromDate + ''' AND ''' + @sToDate + ' 23:59:59'''

IF @sLocCode <> ''
	SET @sql = @sql + ' AND RntCkiLocCode = ''' + @sLocCode + ''''
ELSE
	SET @sql = @sql + ' AND	EXISTS(SELECT TOP 1 Location.loccode
									FROM Location WITH(NOLOCK), TownCity WITH(NOLOCK)
									WHERE	RntCkiLocCode = LocCode
									AND		LocTctCode = TctCode
									AND		TctCtyCode = ''' + @sCtyCode + ''')'

IF @sBrdCode <> ''
	SET @sql = @sql + ' AND PkgBrdCode = ''' + @sBrdCode + ''''

IF @sClaId <> '' OR @sTypId <> '' OR @sPrdId <> ''
BEGIN
	SET @sql = @sql + ' AND EXISTS(SELECT BpdId FROM dbo.BookedProduct WITH(NOLOCK), dbo.SaleableProduct WITH(NOLOCK), dbo.Product WITH(NOLOCK), dbo.Type WITH(NOLOCK)
										WHERE	BpdSapId = SapId
										AND		PrdId = SapPrdId
										AND		PrdTypId = TypId
										AND		BpdRntId = RntId
										AND		BpdPrdIsVehicle = 1
										AND 	BpdStatus = ''KK'' ' +
										CASE
											WHEN @sTypId <> '' THEN ' AND	PrdTypId = ''' + @sTypId + ''''
											ELSE ''
										END +
										CASE
											WHEN @sClaId <> '' THEN ' AND	TypClaId = ''' + @sClaId + ''''
											ELSE ''
										END +
										CASE
											WHEN @sPrdId <> '' THEN ' AND	PrdId = ''' + @sPrdId + ''''
											ELSE ''
										END +
										')'
END
SET @sql = @sql + ' ORDER BY RntCkiLocCode, RntCkiWhen, RntCkoLocCode, RntCkoWhen'
EXECUTE (@sql)

DECLARE Cur_in CURSOR FAST_FORWARD LOCAL  FOR
	SELECT  RntId FROM #TT_CheckIn

OPEN Cur_in
FETCH NEXT FROM Cur_in INTO @sRentalIdIn
WHILE @@FETCH_STATUS=0
BEGIN

	SELECT	@BpdIdIn 	= NULL,	@ReturnErrorIn	=NULL,
			@sUnitNumIn = '',	@sSchedVehicleIn='',
			@sCustNameIn= '',	@sPaxIn 		=0,
			@sDeptRefIn ='',	@sDateIn 		='',
			@sFromIn 	='',	@sToIn 			='',
			@sRntCodUomIdIn ='',@sPeriodIn 		='',
			@sPkgCodeIn ='',	@sBooNumIn 		='',
			@sRntNumIn 	='',	@sBookingIdIn 	='',
			@sCurrIdIn 	='',	@sBrdIn 		='',
			@sPrdIn 	='', 	@dRntCkiWhen	= null,
			@dRntCkoWhen = null

	-- get book number and rentel number
	SELECT 	@sRntNumIn = RntNum,
			@sBooNumIn = BooNum,
			@sBookRentalIn = BooNum + '/' + RntNum,
			@sPaxIn = ISNULL(ISNULL(RntNumOfAdults, 0) + ISNULL(RntNumOfInfants, 0) + ISNULL(RntNumOfChildren, 0),0),
			@dRntCkiWhen = RntCkiWhen,
			@dRntCkoWhen = RntCkowhen
	FROM	Rental WITH(NOLOCK), Booking WITH(NOLOCK)
	WHERE	RntBooId = BooId
	AND		RntId = @sRentalIdIn

	SELECT 	@sBrdIn = PkgBrdCode,
			@sBookingIdIn = RntBooId
	FROM 	Package WITH(NOLOCK), Rental WITH(NOLOCK)
	WHERE 	PkgId = RntPkgId
	AND		RntId = @sRentalIdIn

	-- get BpdId
		SELECT top 1
				@BpdId = Bpdid
			From dbo.bookedproduct (nolock)
			WHERE 	Bpdrntid = @sBookRentalIn
			AND 	bpdStatus in ('KK','NN','WL','QN','IN')
			AND     bpdprdisvehicle = 1
			Order by BpdCkoWhen

	SET @BpdIdIn = NULL
	SELECT top 1 @BpdIdIn  = Bpdid
			from dbo.bookedproduct (nolock)
			where 	bpdrntid = @sRentalIdIn
			AND 	BpdPrdIsvehicle = 1
			AND     BpdStatus NOT IN ('XX','XN')

	IF @BpdIdIn is null or @BpdIdIn = ''
			GOTO GETNEXTRENTALCKI

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @BpdIdIn
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0
					AND  @dRntCkiWhen >= @AU_THRIFTYEFFDT and @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTRENTALCKI
	END

	--get booked vehicle
	SELECT 	@sPrdIn = product.PrdShortName,
			@sUnitNumIn = ISNULL(BpdUnitNum,'')
	FROM	dbo.Bookedproduct WITH(NOLOCK), dbo.Product WITH(NOLOCK), dbo.rental WITH(NOLOCK)
	WHERE  	Bpdid = @BpdIdIn
	AND 	rntid=BpdrntId
	AND 	PrdId=rntprdrequestedvehicleid

	-- get sched vehicle
     SELECT @sSchedVehicleIn = FleetModelCode
	 FROM 	aimsprod.dbo.FleetModel WITH (NOLOCK) INNER JOIN
			fleetassetview WITH (NOLOCK)
			ON aimsprod.dbo.FleetModel.FleetModelId  =
			fleetassetview.FleetModelId
	 WHERE 	UnitNumber = @sUnitNumIn

	--get hirer
	SELECT	@sCustNameIn = Customer.CusLastName
	FROM	Customer WITH(NOLOCK), Traveller WITH(NOLOCK), Rental WITH(NOLOCK)
	WHERE	Traveller.TrvRntId = Rental.RntId
	AND		Traveller.TrvIsPrimaryHirer = 1
	AND		Customer.CusId = Traveller.TrvCusId
	AND		Rental.RntId = @sRentalIdIn

	-- get check-in location code
	DECLARE @sCkiLocCode VARCHAR(64)
	SELECT 	@sCkiLocCode = LTRIM(RTRIM(RntCkiLocCode))
	FROM 	Rental WITH(NOLOCK)
	WHERE 	RntId = @sRentalIdIn

	-- get Vip
	DECLARE @sRntIsVip VARCHAR(1)
	SELECT	@sRntIsVip = (CASE RntIsVip WHEN '1' THEN 'Y' ELSE 'N' END)
	FROM	Rental WITH (NOLOCK)
	WHERE	RntId = @sRentalIdIn

	--get from
	SELECT 	@sFromIn = RntCkoLocCode + '  ' + CONVERT(char(10),RntCkoWhen,103),
			@sToIn = RntCkiLocCode + '  ' + CONVERT(char(10),RntCkiWhen,103),
			@sRntCodUomIdIn = RntCodUomId,
			@sPeriodIn =  cast(ISNULL(RntNumOfHirePeriods,0) as varchar) + ' Days',
			@sPkgCodeIn = PkgCode
	FROM 	Rental WITH(NOLOCK), Package WITH(NOLOCK)
	WHERE 	Package.PkgId = Rental.RntPkgId
	AND 	RntId = @sRentalIdIn

	-- get note
	SET @Note = ''
	DECLARE Cur_Note CURSOR FOR
		-- Line feed: char(10)
		SELECT 	 ISNULL(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(replace(note.ntedesc,char(150),char(45)),'''',''),'”',''),'á',''),'“',''),'’',''''), char(10), ' '), '') AS NteDesc,
					CONVERT(varchar,Note.AddDATETIME,103) + ' ' + SUBSTRING((CONVERT(varchar,Note.AddDATETIME,108)),1,2)+':' + SUBSTRING((CONVERT(varchar,Note.AddDATETIME,108)),4,2) AS [DATETIME]
		FROM    Note WITH(NOLOCK)
		WHERE  	NteBooId = @sBookingIdIn
		AND (NteRntId = @sRentalIdIn OR  NteRntId is null)
		AND NteIsActive = 1
		ORDER BY Note.AddDATETIME

	OPEN Cur_Note
	FETCH NEXT FROM Cur_Note INTO @sNote, @sDateTime
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @Note = @Note + @sNote + ' '
		FETCH NEXT FROM Cur_Note INTO @sNote, @sDateTime
	END
	CLOSE Cur_Note
	DEALLOCATE Cur_Note

	-- get check in list result
	insert into @Result_CheckIn values
	(
		@sBookRentalIn,
		@sCkiLocCode,
		@sBrdIn,
		ISNULL(@sPrdIn, ''),
		ISNULL(@sUnitNumIn, ''),
		ISNULL(@sSchedVehicleIn,''),
		ISNULL(@sCustNameIn, ''),
		@sRntIsVip,
		CAST(@sPaxIn as VARCHAR),
		ISNULL(@sFromIn, ''),
		ISNULL(@sToIn, ''),
		ISNULL(@sPeriodIn, 0),
		ISNULL(@sPkgCodeIn, ''),
		@Note
	)

	-- get products
	-- 1. Non Security Products
	DECLARE		@NonSecurityBpdIdIn	VARCHAR(8000),
			@docIn			INT
	SET @NonSecurityBpdIdIn = NULL
	--get booked non security products
	EXEC RES_getBookedProducts
			@sRntId = @sRentalIdIn,
			@bSelectCurrentOnly = 1,
			@sIsSecurity = 'N',
			@sExclStatusList = 'XX,XN',
			@dCkiWhenFrom = @StartDate, -- add on 30/09/2002
			@dCkiWhenTo = @EndDate,-- add on 30/09/2002
			@sOrderBy = '',
			@ReturnCSV = 2,
			@sBpdList = @NonSecurityBpdIdIn		OUTPUT,
			@ReturnError = @ReturnErrorIn		OUTPUT

	WHILE	@NonSecurityBpdIdIn IS NOT NULL
	AND	LEN(@NonSecurityBpdIdIn) > 0
	BEGIN
		SET @BpdId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @NonSecurityBpdIdIn,
			@psListEntry		= @BpdId			OUTPUT,
			@psUpdatedList		= @NonSecurityBpdIdIn	OUTPUT

		IF @BpdId is not null and @BpdId <> ''
		BEGIN
			declare		@amountIn	VARCHAR(64),
					@CurrIn		VARCHAR(20),
					@nsPrdNameIn	VARCHAR(64),
					@isPrepaidIn	bit
			set		@amountIn = ''
			set		@CurrIn = ''
			set		@nsPrdNameIn = ''

			--should restrict by AddDate > RntCkoWhen
			DECLARE @CheckInPrd INT
			SELECT 	@CheckInPrd = COUNT(BookedProduct.BpdId)
			FROM    Rental WITH(NOLOCK), BookedProduct WITH(NOLOCK)
			WHERE	Rental.RntId = BookedProduct.BpdRntId
			AND 	BookedProduct.BpdId = @BpdId
			AND 	BookedProduct.AddDATETIME > Rental.RntCkoWhen

			-- get product name
			IF @CheckInPrd > 0
			BEGIN
				SELECT 	@nsPrdNameIn = product.PrdShortName,
						@CurrIn = dbo.getCodCode(BpdCodCurrId),
						@isPrepaidIn = BpdIsCusCharge
				FROM	Bookedproduct  WITH(NOLOCK), Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)
				WHERE  	Bpdid = @BpdId
				AND 	SapId=BpdSapId
				AND 	PrdId=SapPrdId

				IF @isPrepaidIn = 0
					SET @amountIn = 'Prepaid'
				ELSE
					SELECT	@amountIn = CAST((BpdGrossAmt - BpdAgnDisAmt) AS VARCHAR)
					FROM	dbo.BookedProduct WITH(NOLOCK)
					WHERE	BpdId = @BpdId

				insert into reports.Report_ActDetCKIProducts values
				(
					@sBookRentalIn,
					@nsPrdNameIn,
					ISNULL(@CurrIn,''),
					cast(ISNULL(@amountIn,0) as varchar)
				)
			END
		END
	END

	-- 2. Security Products
	DECLARE	@SecurityBpdIdIn	VARCHAR(8000),
			@sReturnErrorIn		VARCHAR(500),
			@sdocIn			INT
	SET @SecurityBpdIdIn = NULL
	EXEC RES_getBookedProducts
			@sRntId = @sRentalIdIn,
			@bSelectCurrentOnly = 1,
			@sIsSecurity = 'Y',
			@sExclStatusList = 'XX,XN',
			@sOrderBy = '',
			@ReturnCSV = 2,
			@sBpdList = @SecurityBpdIdIn		OUTPUT,
			@ReturnError = @sReturnErrorIn		OUTPUT

	WHILE	@SecurityBpdIdIn IS NOT NULL AND	LEN(@SecurityBpdIdIn) > 0
	BEGIN
		SET @BpdId = null
		EXECUTE	GEN_GetNextEntryAndRemoveFromList
			@psCommaSeparatedList	= @SecurityBpdIdIn,
			@psListEntry		= @BpdId			OUTPUT,
			@psUpdatedList		= @SecurityBpdIdIn	OUTPUT

		IF @BpdId is not null and @BpdId <> ''
		BEGIN
			DECLARE		@amountsIn		VARCHAR(64),
					@CurrsIn		VARCHAR(20),
					@ssPrdNameIn		VARCHAR(64),
					@isPrepaidsIn		bit

			set		@amountsIn = ''
			set		@CurrsIn = ''
			set		@ssPrdNameIn = ''

			-- get product name
			DECLARE @sCheckInPrd INT
			SELECT  @sCheckInPrd = COUNT(BookedProduct.BpdId)
			FROM   	Rental WITH(NOLOCK), BookedProduct WITH(NOLOCK)
			WHERE	Rental.RntId = BookedProduct.BpdRntId
			AND 	BookedProduct.BpdId = @BpdId
			AND 	BookedProduct.AddDATETIME > Rental.RntCkoWhen


			IF @sCheckInPrd > 0
			BEGIN
				SELECT 	@ssPrdNameIn = product.PrdShortName,
						@CurrsIn = dbo.getCodCode(BpdCodCurrId),
						@isPrepaidsIn = BpdIsCusCharge
				FROM	Bookedproduct WITH(NOLOCK), Product WITH(NOLOCK), SaleableProduct WITH(NOLOCK)
				WHERE  	Bpdid = @BpdId
				AND 	SapId=BpdSapId
				AND 	PrdId=SapPrdId


				IF @isPrepaidIn = 0
					SET @amountsIn = 'Prepaid'
				ELSE
					SELECT	@amountsIn = CAST((BpdGrossAmt - BpdAgnDisAmt) AS VARCHAR)
					FROM	BookedProduct WITH(NOLOCK)
					WHERE	BpdId = @BpdId

				-- get security products result
				insert into reports.Report_ActDetCKIProducts values
				(
					@sBookRentalIn,
					@ssPrdNameIn,
					ISNULL(@CurrsIn,''),
					cast(ISNULL(@amountsIn,0) as varchar)
				)
			END
		END
	END

	--get Pymt Rcvd
	DECLARE @RcvdSecurityAmount		VARCHAR(64)
	DECLARE @RcvdSecurityCurr		VARCHAR(64)
	SELECT	@RcvdSecurityAmount = CAST(SUM(ISNULL(RentalPayment.RptChargeAmt, 0)) AS varchar),
            @RcvdSecurityCurr = dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)
	FROM  	Rental WITH(NOLOCK) INNER JOIN
            RentalPayment WITH(NOLOCK) ON Rental.RntId = RentalPayment.RptRntId
			AND Rental.RntCkoWhen < RentalPayment.AddDATETIME INNER JOIN
            Payment WITH(NOLOCK) ON RentalPayment.RptPmtId = Payment.PmtId
	WHERE  	Rental.RntId=@sRentalIdIn
	GROUP BY dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)
	ORDER BY dbo.GetCodCode(dbo.RentalPayment.RptChargeCurrId)

	--- get Cash bond
	declare @cashBondAmount VARCHAR(64)
	declare @cashBondCurr VARCHAR(20)
	SELECT 	@cashBondAmount = CAST(ISNULL(SUM(RentalPayment.RptChargeAmt),0) AS varchar),
			@cashBondCurr = ISNULL(dbo.getCodCode(RentalPayment.RptChargeCurrId),'')
	FROM   	RentalPayment  WITH (NOLOCK) INNER JOIN
           	Payment  WITH (NOLOCK) ON RentalPayment.RptPmtId = Payment.PmtId INNER JOIN
           	PaymentMethod  WITH (NOLOCK) ON Payment.PmtPtmId = PaymentMethod.PtmId
	WHERE 	(PaymentMethod.PtmIsCash = 1) --OR PtmIsTravChq = 1 ) --Or PtmIsPersChq = 1)
	AND 	(RentalPayment.RptType = 'B')
	AND 	(RentalPayment.RptRntId = @sRentalIdIn)
	GROUP BY RentalPayment.RptChargeCurrId, RentalPayment.RptType, RentalPayment.RptRntId

	insert into reports.Report_ActDetCKIPayment values
	(
		@sBookRentalIn,
		@RcvdSecurityAmount,
		@RcvdSecurityCurr,
		@cashBondAmount,
		@cashBondCurr
	)

	GETNEXTRENTALCKI:
	FETCH NEXT FROM Cur_in INTO @sRentalIdIn
END

CLOSE Cur_in
DEALLOCATE Cur_in
DROP TABLE #TT_CheckIn

select *
from @Result_CheckIn
where Brand IN (		-- KX fix AJ May 2008
				SELECT	Brand.BrdCode
				FROM	UserInfo INNER JOIN
						UserCompany ON UserInfo.UsrId = UserCompany.UsrId INNER JOIN
						Brand ON UserCompany.ComCode = Brand.BrdComCode
				WHERE   (UserInfo.UsrCode = User)
				)
	AND BkdVehicle <> ''



GO
