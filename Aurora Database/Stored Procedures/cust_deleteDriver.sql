CREATE   PROCEDURE [dbo].[cust_deleteDriver]
	@CusId	varchar(64),
	@RntID varchar(64),
	@sRetirnError varchar(500) OUTPUT
AS
	DECLARE @sError varchar(500)

	IF EXISTS(SELECT BptRntId
				FROM 	BookedProductTraveller  WITH (NOLOCK)
				WHERE	BptRntId = @RntId
				AND		BptCusId = @CusId)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN016', 'Driver', 'BookedProductTraveller ' , @oparam1 = @sError OUTPUT
	    SELECT 'GEN016/' + @sError
		--JL 2008/02/12 set error to  @sRetirnError
		SET @sRetirnError ='GEN016/' + @sError
		RETURN
	END
	DELETE Traveller    WITH (ROWLOCK)
		WHERE 	TrvCusId = @CusId
		AND		TrvRntId = @RntId

	IF @@ERROR <> 0
	BEGIN
		EXECUTE sp_get_Errorstring 	'GEN110',
						'DeleteDriver  ',
						@oparam1 = @sError OUTPUT

		SET @sRetirnError ='GEN110/' + @sError
	END

	RETURN























GO
