set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_insertNewsFlash] 

		@CtyCode		VARCHAR	 (64)	=	'' ,
		@UsrCode		VARCHAR	 (64)	=	'',
		--@NwfId			varchar	(64),
		@PrgmName		VARCHAR	 (64)	=	'',
		@NwfAudienceType VARCHAR	 (20)	=	'' ,
		@NwfEffectiveDate smalldatetime,
		@NwfTerminationDate smalldatetime,
		@NwfText	VARCHAR	 (5000)	=	''

AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@NwfId			varchar	(64),
				@UsrId				VARCHAR	 (64) ,
				@sComCode			VARCHAR(64)

	SELECT	@sComCode = Company.ComCode 
	FROM	dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON  UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @UsrCode
				
	SELECT	@UsrId = UsrId
	 FROM	UserInfo WITH (NOLOCK)
	 WHERE	UsrCode = @UsrCode
	

	set @NwfId = NEWID()

	-- Validation for Date Overlapping
	if not EXISTS ( 
		SELECT 
			NwfID
		FROM 
			NewsFlash		AS	NewsFlash WITH (NOLOCK) 
		WHERE 
			NwfCtyCode=@CtyCode
			AND NwfTerminationDate > = CURRENT_TIMESTAMP
			AND (@UsrCode='' OR NwfComCode = @sComCode)
			and NwfAudienceType = @NwfAudienceType
			and 
			((NwfEffectiveDate < @NwfEffectiveDate and NwfTerminationDate > @NwfEffectiveDate)or
			(NwfEffectiveDate < @NwfTerminationDate and NwfTerminationDate > @NwfTerminationDate)or
			(NwfEffectiveDate > @NwfEffectiveDate and NwfTerminationDate > @NwfTerminationDate))
	)
	begin
		INSERT INTO NewsFlash  WITH (ROWLOCK)(
				NwfID ,
				NwfAudienceType ,
				NwfCtyCode ,
				NwfEffectiveDate ,
				NwfTerminationDate ,
				NwfText ,
				NwfComCode,
				IntegrityNo ,
				AddUsrId ,
				AddDateTime ,
				AddPrgmName
			)
		VALUES
				(
					@NwfId ,
					@NwfAudienceType ,
					@CtyCode ,
					@NwfEffectiveDate ,
					@NwfTerminationDate ,
					@NwfText ,
					@sComCode,
					1 ,
					@UsrId ,
					CURRENT_TIMESTAMP ,
					@PrgmName
				)

		IF @@ERROR <> 0 
			-- Return message to the calling program to indicate failure.
			select N'An error occurred while inserting the NewsFlash record in database.'
		ELSE
			select ''

	end
	else
	begin
		--select 'One or more Date Overlapped for ' + @NwfAudienceType
		SELECT dbo.getMessage('GEN121',@NwfAudienceType,'','','','','')
	end

END

--select * from NewsFlash
--
--sp_help NewsFlash
