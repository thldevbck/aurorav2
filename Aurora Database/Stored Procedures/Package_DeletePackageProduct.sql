CREATE PROCEDURE [dbo].[Package_DeletePackageProduct]
	@PplPkgId	varchar(64),
	@PplSapId	varchar(64)
AS

	DELETE FROM
		PackageProduct
	WHERE
		PackageProduct.PplPkgId = @PplPkgId
		AND PackageProduct.PplSapId = @PplSapId


	DELETE FROM
		FlexLevel
	WHERE
		FlexLevel.FlxPkgId = @PplPkgId
		AND FlexLevel.FlxSapId = @PplSapId



GO
