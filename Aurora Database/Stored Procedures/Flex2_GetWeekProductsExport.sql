set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



ALTER PROCEDURE [dbo].[Flex2_GetWeekProductsExport] 
	@ComCode varchar(64),
	@FwxId int
AS

SELECT 
	FlexWeekProductView.*
FROM 
	FlexWeekExportView
	INNER JOIN FlexWeekProductView
		ON FlexWeekProductView.FbwId = FlexWeekExportView.FbwId
		AND FlexWeekProductView.FlpStatus = 'approved'
		AND FlexWeekProductView.ComCode = FlexWeekExportView.FlxComCode
		AND (FlexWeekProductView.CtyCode = FlexWeekExportView.FlxCtyCode OR FlexWeekExportView.FlxCtyCode IS NULL)
		AND (FlexWeekProductView.BrdCode = FlexWeekExportView.FlxBrdCode OR FlexWeekExportView.FlxBrdCode IS NULL)
		AND (FlexWeekProductView.ClaID = FlexWeekExportView.FlxClaID OR FlexWeekExportView.FlxClaID IS NULL)
		AND (FlexWeekProductView.TypID = FlexWeekExportView.FlxTypID OR FlexWeekExportView.FlxTypID IS NULL)
		AND (FlexWeekProductView.PrdId = FlexWeekExportView.FlxPrdId OR FlexWeekExportView.FlxPrdId IS NULL)
		AND (FlexWeekProductView.TravelYear = FlexWeekExportView.FwxTravelYear OR FlexWeekExportView.FwxTravelYear IS NULL)
WHERE
	FlexWeekExportView.FlxComCode = @ComCode
	AND FlexWeekExportView.FwxId = @FwxId

ORDER BY
	FlexWeekProductView.TravelYear,
	FlexWeekProductView.ComCode,
	FlexWeekProductView.CtyCode,
	FlexWeekProductView.BrdCode,
	FlexWeekProductView.TypCode,
	FlexWeekProductView.PrdShortName
