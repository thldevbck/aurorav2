CREATE	PROCEDURE [dbo].[Report_COReport]
	@sCODate	VARCHAR(10)	=	'',
	@sLocCode	VARCHAR(12)	=	''
AS

/*
DECLARE @sCODate VARCHAR(10)
DECLARE @sLocCode VARCHAR(12)
SET @sCODate = '01/04/2008'
SET @sLocCode = 'AKL'
*/

SET NOCOUNT ON

IF @sCODate = ''
	SET @sCODate = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)
SELECT	''+ExternalRef	AS 'Booking',
		''+LTRIM(RTRIM(FleetModelCode)) + ' - ' + LTRIM(RTRIM(FleetModelDescription)) 	AS 'Fleet Model',
		''+UnitNumber	AS 'Unit No',
		''+RegistrationNumber	AS 'Rego',
		''+StartOdometer	AS 'Odo Out',
		''+/*LTRIM(RTRIM(LocationCode)) + ' - ' +*/ LTRIM(RTRIM(LocationName)) AS 'Location',
		''+(SELECT BooLastName FROM Booking WITH(NOLOCK) WHERE BooNum = dbo.getSplitedData(ExternalRef, '/', 1)) AS 'Hirer',
		''+(SELECT BooId FROM Booking WITH(NOLOCK)
				WHERE	BooNum = dbo.getSplitedData(ExternalRef, '/', 1))	AS	'BooId',
		''+(SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK)
				WHERE	BooId = RntBooId
				AND		BooNum = dbo.getSplitedData(ExternalRef, '/', 1)
				AND		RntNum = dbo.getSplitedData(dbo.getSplitedData(ExternalRef, '/', 2), 'x', 1))	AS	'RntId'

FROM	AIMSPROD.dbo.FleetActivity FAT WITH(NOLOCK), AIMSPROD.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetModel FM WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
WHERE	FAT.FleetAssetId = FA.FleetAssetId
AND		FA.FleetModelId = FM.FleetModelId
AND		FAT.StartLocation = FL.LocationId
AND		FAT.ActivityType = 'Rental'
AND		CAST(CONVERT(VARCHAR(10), StartDateTime, 103) AS DATETIME) = CAST(@sCODate AS DATETIME)
AND		(@sLocCode = '' OR FL.LocationCode = @sLocCode)
ORDER BY FleetModelCode, UnitNumber

GO
