CREATE PROCEDURE [dbo].[report_getAmountsOutstandingMulti]
		@DefaultUsr		VARCHAR(64)='',
		@sBooId			VARCHAR(64)='',
		@paramRntId		VARCHAR(64)='',
		@bAll			BIT=0 ,
		@Currency 		Varchar (128) = '' 	output,
		@TotDue 		Varchar (128) = ''	output,
		@TotalPaidLo 	Varchar (128) = ''	output,
		@BlnDueLoc 		Varchar (128) = ''	output,
		@CustToPay 		Varchar (128) = ''	output
AS
BEGIN
SET NOCOUNT ON


	DECLARE	@bDirectAgent			BIT ,
			@BpdId					VARCHAR	 (64) ,
			@RntId					VARCHAR	 (64) ,
			@iAmtOwing				MONEY ,
			@PkgDepositAmt			MONEY ,
			@PkgDepositPercentage	MONEY ,
			@iDepositAmt			MONEY ,
			@blsSecurity			VARCHAR	 (3) ,
			@sType					VARCHAR	 (1) ,
			@sCurrCode				VARCHAR	 (12) ,
			@ForReceipt				MONEY ,
			@defCurr				VARCHAR(4) ,
			@AmtPaid				MONEY ,
			@FCurr					VARCHAR	(64) ,
			@FTot					MONEY ,
			@PrdList				VARCHAR  (8000),
			@Err					VARCHAR  (8000),
			@doc					INT ,
			@ErrStr					VARCHAR	 (8000) ,
			@DefToSend				VARCHAR	 (64) ,
			@tempDepositAmt			MONEY ,
			@bIsExpense				VARCHAR	 (3) ,
			@Start 					BIGINT ,
			@Count					BIGINT ,
			@sExt					VARCHAR	 (64),
			@sXFrm					VARCHAR	 (64),
			@sXto					VARCHAR	 (64),
			@sLastBpdId				VARCHAR	 (64)

		SELECT @sExt = dbo.GetCodeId(4,'EXT'), @sXFrm = dbo.GetCodeId(4,'XFRM'), @sXto = dbo.GetCodeId(4,'XTO')

		SELECT	@DefToSend = dbo.GetCodCode(Country.CtyCodCurrID)
			FROM 	UserInfo WITH(NOLOCK) INNER JOIN
					Country WITH(NOLOCK) ON UserInfo.UsrCtyCode = Country.CtyCode
			WHERE	UserInfo.UsrCode = @DefaultUsr

		DECLARE @AmountsOutStanding	TABLE(
						RntId		VARCHAR	 (64) ,
						Type		VARCHAR	 (1) ,
						DefCurr		VARCHAR	(64),
						CurrId		VARCHAR	 (64) ,
						Curr		VARCHAR	 (64) ,
						AmtOwing	MONEY  	DEFAULT  0,
						AmtOwingLoc MONEY ,
						AmtPaid		MONEY ,
						CustToPayAmt	AS (ISNULL(AmtOwing,0)-ISNULL(AmtPaid,0)) ,
						CurrentPymtAmt	MONEY
						PRIMARY KEY(RntId, Type, CurrId)
					)

		SELECT	@bDirectAgent = ISNULL(AgentType.AtyIsDirect,0)
			FROM	AgentType WITH(NOLOCK) INNER JOIN
					Agent WITH(NOLOCK) ON AgentType.AtyId = Agent.AgnAtyId INNER JOIN
					Booking WITH(NOLOCK) ON Agent.AgnId = Booking.BooAgnId
			WHERE	Booking.BooID = @sBooId

		SELECT @blsSecurity = 'Y', @bIsExpense = 'N', @PrdList = NULL, @Err = NULL
		IF @bAll = 1
			EXEC RES_getBookedProducts
				@sBooId 			= @sBooId,
				@sExclStatusList 	= 'XN,XX' ,
				@bSelectCurrentOnly = 1,
				@sAgnCusSelection 	= 'Customer' ,
				@sIsSecurity 		= @blsSecurity ,
				@ReturnCSV			= 2 ,
				@sBpdList 			= @PrdList 	OUTPUT,
				@ReturnError 		= @Err 		OUTPUT
		ELSE
			/*EXEC RES_getBookedProducts
				@sBooId 			= @sBooId,
				@sRntId 			= @paramRntId,
				@sExclStatusList 	= 'XN,XX' ,
				@bSelectCurrentOnly = 1,
				@sAgnCusSelection 	= 'Customer' ,
				@sIsSecurity 		= @blsSecurity ,
				@ReturnCSV			= 2 ,
				@sBpdList 			= @PrdList OUTPUT,
				@ReturnError 		= @Err OUTPUT */
			SELECT @PrdList = CASE WHEN @PrdList is null then BpdId else @PrdList + ',' + Bpdid end
				from dbo.bookedproduct with (nolock)
				where 	bpdrntid = @paramRntId
				and 	bpdstatus in ('KK','NN','WL','QN','IN')
				and 	bpdiscuscharge = 1
				and 	DBO.GetProductAttributeValue(BpdSapId, 'Security') IS NOT NULL



		IF @PrdList IS NOT NULL and @PrdList <> ''
		BEGIN
			SET @Start = 1
			SET @Count = dbo.getEntry(@PrdList, ',') + 1

			WHILE @Start <= @Count
			BEGIN
				SELECT @BpdId = dbo.getSplitedData(@PrdList, ',', @Start)
				SELECT @Start = @Start + 1

				SELECT @ForReceipt = 0, @iDepositAmt = 0, @iAmtOwing = 0, @tempDepositAmt  = 0

				IF EXISTS(SELECT BpdId FROM BookedPRoduct WITH(NOLOCK)
							WHERE 	BpdId = @BpdId
							AND 	ISNULL(BpdIsCusCharge, 0) = 1
							AND 	ISNULL(BpdIsCurr, 0) = 1)
				BEGIN
					EXECUTE RES_GetChargeIfXTO
						@BpdId			= @BpdId,
						@sXtoDisplay	= @iAmtOwing OUTPUT
					--SELECT '1st', @BpdId, 	@iAmtOwing
				END

				SELECT	@RntId = BpdRntId,
						-- @iAmtOwing = ISNULL(BpdGrossAmt,0) - ISNULL(BpdAgnDisAmt,0),
						@defCurr = dbo.GetCodCode(BpdCodCurrId),
						@sCurrCode = dbo.getCodCode(BpdCodCurrId)
				FROM	BookedProduct WITH(NOLOCK)
				WHERE	BpdId = @BpdId
				IF @rntid is null or @rntid <> ''
					select @rntid = @paramRntId

				IF @bDirectAgent = 1
				BEGIN
					IF EXISTS (SELECT BpdId FROM  BookedProduct WITH(NOLOCK)
									WHERE BpdId = @BpdId
									AND   BpdPrdIsVehicle = 1 AND (BpdCodTypeId IS NULL
																	OR  BpdCodTypeId NOT IN (@sExt,@sXFrm,@sXto)
																	)
							)
					BEGIN
						SELECT	@PkgDepositAmt = PkgDepositAmt,
								@PkgDepositPercentage = PkgDepositPercentage
							FROM	Rental WITH(NOLOCK) INNER JOIN Package WITH(NOLOCK) ON RntPkgId = PkgId
							WHERE	RntId = @RntId

						IF @PkgDepositAmt>0
							SET @iDepositAmt = @PkgDepositAmt
						ELSE
							SET @iDepositAmt = (@PkgDepositPercentage/100) * @iAmtOwing
					END	--	Product.PrdIsVehicle = 1
					ELSE
						SET @iDepositAmt = 0
				END -- IF @bDirectAgent = 1
				ELSE
					SET @iDepositAmt = 0

				IF @blsSecurity = 'Y'
					SET @sType = 'B'
				ELSE
					IF @bIsExpense = 'Y'
						SET @sType = 'X'
					ELSE
						SET @sType = 'R'

				IF @iDepositAmt > 0
				BEGIN
					SET @AmtPaid = 0
					SELECT @AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'D'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)


					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE 	RntId = @RntId
								AND 	Type = 'D'
								AND 	CurrId = @sCurrCode)
					BEGIN
						--RKSHUKLA
						SET @tempDepositAmt  = @iDepositAmt
						SELECT	@iDepositAmt = @iDepositAmt + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'D'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
								SET  AmtOwing  = @iDepositAmt ,
									AmtOwingLoc = CASE
															WHEN ISNULL(@tempDepositAmt,0) = 0  THEN  @tempDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
															ELSE @iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
														END ,
									-- AmtOwingLoc = @iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)  ,
									AmtPaid = @AmtPaid
						WHERE	RntId = @RntId
						AND		Type = 'D'
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN

						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, 'D', @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iDepositAmt,
								@iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)

					END -- IF EXISTS
				END	-- IF @iDepositAmt > 0

				IF @sType IN ('B','X')
				BEGIN
					SET @AmtPaid = 0
					SELECT @AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType  = @sType
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)



					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type IN ('B','X')
								AND		CurrId = @sCurrCode	)
					BEGIN
						SELECT	@iAmtOwing = @iAmtOwing + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = @sType
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
								SET AmtOwing  = @iAmtOwing,
									AmtOwingLoc = @iAmtOwing * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1),
									AmtPaid	=	@AmtPaid
						WHERE	RntId = @RntId
						AND		Type = @sType
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN

						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, @sType, @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing,
								@iAmtOwing * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)

					END -- IF EXISTS
				END	-- IF @sType IN ('B','X')
				ELSE
				BEGIN
					SET @AmtPaid = 0
					SELECT 	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'R'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)


					IF EXISTS(SELECT RntId
								FROM	@AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = 'R'
								AND		CurrId = @sCurrCode)

					BEGIN
						SELECT	@ForReceipt = @ForReceipt  + AmtOwing + (@iAmtOwing - @iDepositAmt)
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'R'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
								SET AmtOwing  = @ForReceipt ,
									AmtOwingLoc  = @ForReceipt * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1),

									AmtPaid	=	@AmtPaid
						WHERE	RntId = @RntId
						AND		Type = 'R'
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN
						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, 'R', @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing - @iDepositAmt,
								(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)
					END -- IF EXISTS*/
				END -- ELSE
			END	-- WHILE @Start <= @Count
		END -- IF @PrdList IS NOT NULL

		/* 3a(2) Process all BookedProduct 'expense product' records to get summary of charges and costs */
		SELECT @blsSecurity = 'N', @bIsExpense = 'Y', @PrdList = NULL, @Err = NULL

		DECLARE @sNonRevB4XchgBpdId VARCHAR(8000) -- Declare for before exchange, added by James on 07/03/2002

		IF @bAll = 1
			BEGIN
				EXEC RES_getBookedProducts
					@sBooId 			= @sBooId,
					@sExclStatusList 	= 'XN,XX' ,
					@bSelectCurrentOnly = 1,
					@sAgnCusSelection 	= 'Customer' ,
					@sIsSecurity 		= @blsSecurity ,
					@sAttCode			= 'EXPENSE' ,
					@sAttValue			= 1 ,
					@ReturnCSV			= 2 ,
					@sBpdList 			= @PrdList 	OUTPUT,
					@ReturnError 		= @Err 		OUTPUT

				/***************************************************************/
				-- Added for finding before exchange by James on 07/03/2003
				DECLARE @sRntIdList 	VARCHAR(5000)
				DECLARE @sCurrentRntId 	VARCHAR(64)

				SET	@sRntIdList = NULL

				-- for each of the rental in the booking, find any pre exchange details required
				-- find the rentals
				SELECT 	@sRntIdList = ISNULL(@sRntIdList + ',', '') + RntId
				 FROM 	dbo.Rental WITH (NOLOCK)
				 WHERE	RntBooId = @sBooId
				 AND	RntStatus NOT IN ('XX','XN')

				-- for each rental, get before exchange details
				WHILE (@sRntIdList IS NOT NULL) AND (LEN(@sRntIdList) > 0)
				BEGIN
					SET @sCurrentRntId = NULL
					SET	@sNonRevB4XchgBpdId = NULL
					EXECUTE	GEN_GetNextEntryAndRemoveFromList
						@psCommaSeparatedList	= @sRntIdList,
						@psListEntry			= @sCurrentRntId	OUTPUT,
						@psUpdatedList			= @sRntIdList		OUTPUT
					--IF @bDirectAgent = 0
					BEGIN
						EXEC RES_getExchangeBpds
							@sRntId					= @sCurrentRntId,
							@sBpdIdList				= NULL,
							@cNonRevB4XchgBpdIds 	= NULL,
							@cRevB4XchgBpdIds		= @sNonRevB4XchgBpdId OUTPUT ,
							@cRntAgrList 			= NULL,
							@cNonRevB4XchgAmt 		= NULL,
							@cRevB4XchgAmt			= NULL,
							@cXfrmAmt 				= NULL,
							@cXtoAmt 				= NULL
						SET @sLastBpdId = NULL
						IF @sNonRevB4XchgBpdId IS NOT NULL
						BEGIN
							SET @sLastBpdId = dbo.getSplitedData(@sNonRevB4XchgBpdId, ',', dbo.getEntry(@sNonRevB4XchgBpdId, ',') + 1)

							IF Exists(SELECT Bpdid FROM dbo.Bookedproduct (NOLOCK)
										WHERE BpdId = @sLastBpdId AND BpdIsCusCharge = 1)
								SET @sNonRevB4XchgBpdId = NULL
						END -- 	IF @sNonRevB4XchgBpdId IS NOT NULL
						-- added to booked product list
						SET @PrdList = ISNULL(@PrdList + ',','') + @sNonRevB4XchgBpdId
					END
				-- Added end
				/***************************************************************/
				END
			END
		ELSE
			BEGIN
				IF @bIsExpense = 'Y'
				begin
	/*				EXEC RES_getBookedProducts
						@sBooId 			= @sBooId,
						@sRntId 			= @paramRntId,
						@sExclStatusList 	= 'XN,XX' ,
						@bSelectCurrentOnly = 1,
						@sAgnCusSelection 	= 'Customer' ,
						@sIsSecurity 		= @blsSecurity ,
						@sAttCode			= 'EXPENSE' ,
						@sAttValue			= 1 ,
						@ReturnCSV			= 2 ,
						@sBpdList 			= @PrdList OUTPUT,
						@ReturnError 		= @Err OUTPUT
	*/
				SET @prdlist = null
				SELECT @PrdList = case when @PrdList is null then BpdId  else @PrdList +',' + bpdid end
					FROM dbo.BookedProduct with (nolock)
					INNER JOIN dbo.Rental (NOLOCK) ON Rental.RntId = BookedProduct.BpdRntId
					INNER JOIN Booking (NOLOCK) ON Booking.BooID = Rental.RntBooID
				WHERE BooId = @sBooId
				AND RntID= @paramRntId
				AND BpdIsCurr = 1
				AND BpdStatus NOT IN ('XN','XX')
				AND BpdIsCuscharge IS NOT NULL AND BpdIsCuscharge = 1
				AND ISNULL(DBO.GetProductAttributeValue(BpdSapId, 'Security'), 0) = 0
				AND ISNULL(DBO.GetProductAttributeValue(BpdSapId, 'EXPENSE'), 0) = '1'

					-- BEGIN RSM 19/03/03 Added in by ravi to fix the 'X' type issue
					IF @PrdList is null or @PrdList  = ''
						SET @bIsExpense = 'N'
					-- END RSM 19/03/03 Added in by ravi to fix the 'X' type issue
				END
				ELSE
				begin
	/*				EXEC RES_getBookedProducts
						@sBooId 			= @sBooId,
						@sRntId 			= @paramRntId,
						@sExclStatusList 	= 'XN,XX' ,
						@bSelectCurrentOnly = 1,
						@sAgnCusSelection 	= 'Customer' ,
						@sIsSecurity 		= @blsSecurity ,
						@ReturnCSV			= 2 ,
						@sBpdList 			= @PrdList OUTPUT,
						@ReturnError 		= @Err OUTPUT */
					SELECT @PrdList = CASE WHEN @PrdList is null then BpdId else @PrdList + ',' + Bpdid end
						from dbo.bookedproduct with (nolock)
						where 	bpdrntid = @paramRntId
						and 	bpdstatus in ('KK','NN','WL','QN','IN')
						and 	bpdiscuscharge = 1
						AND 	ISNULL(DBO.GetProductAttributeValue(BpdSapId, 'Security'), 0) = 0


				END

				/***************************************************************/
				-- Added for finding before exchange by James on 07/03/2003

				SET	@sNonRevB4XchgBpdId = NULL
				--select '*******************************'
				-- find the before exchange details
				--IF @bDirectAgent = 0
				begin
					EXEC RES_getExchangeBpds
						@sRntId					= @paramRntId,
						@sBpdIdList				= NULL,
						@cNonRevB4XchgBpdIds 	= @sNonRevB4XchgBpdId OUTPUT ,
						@cRevB4XchgBpdIds		= NULL,
						@cRntAgrList 			= NULL,
						@cNonRevB4XchgAmt 		= NULL,
						@cRevB4XchgAmt			= NULL,
						@cXfrmAmt 				= NULL,
						@cXtoAmt 				= NULL
						-- added to booked product list
						SET @sLastBpdId  = NULL
						IF @sNonRevB4XchgBpdId IS NOT NULL
						BEGIN
							SET @sLastBpdId = dbo.getSplitedData(@sNonRevB4XchgBpdId, ',', dbo.getEntry(@sNonRevB4XchgBpdId, ',') + 1)

							IF Exists(SELECT Bpdid FROM dbo.Bookedproduct (NOLOCK)
										WHERE BpdId = @sLastBpdId AND BpdIsCusCharge = 1)
									SET @sNonRevB4XchgBpdId = NULL
						END
						SET @PrdList = ISNULL(@PrdList + ',','') + ISNULL(@sNonRevB4XchgBpdId,'')
				end
				-- Added end
				/***************************************************************/
			END

		IF @PrdList IS NOT NULL and @PrdList <> ''
		BEGIN
			SET @Start = 1
			SET @Count = dbo.getEntry(@PrdList, ',') + 1

			WHILE @Start <= @Count
			BEGIN
				SELECT @BpdId = dbo.getSplitedData(@PrdList, ',', @Start)
				SELECT @Start = @Start + 1
				SELECT @ForReceipt = 0, @iDepositAmt = 0, @iAmtOwing = 0, @tempDepositAmt  = 0

				IF EXISTS(SELECT bpdid FROM dbo.BookedPRoduct WITH(NOLOCK)
								WHERE 	BpdId = @BpdId
								AND 	ISNULL(BpdIsCusCharge, 0) = 1
							--	AND 	ISNULL(BpdIsCurr, 0) = 1 -- is not cuurent one for expense. modified by James on 10/03/2003
							)
				BEGIN
					EXECUTE RES_GetChargeIfXTO
						@BpdId			= @BpdId,
						@sXtoDisplay	= @iAmtOwing OUTPUT
					--SELECT '2nd', @BpdId, 	@iAmtOwing

				END
				SELECT	@RntId = BpdRntId,
						-- @iAmtOwing = ISNULL(BpdGrossAmt,0) - ISNULL(BpdAgnDisAmt,0),
						@defCurr = dbo.GetCodCode(BpdCodCurrId),
						@sCurrCode = dbo.getCodCode(BpdCodCurrId)
					FROM	dbo.BookedProduct WITH(NOLOCK)
					WHERE	BpdId = @BpdId
				if @rntid is null or @rntid = ''
					SET @rntid = @paramRntId

				IF @bDirectAgent = 1
				BEGIN
					IF EXISTS(SELECT BpdId FROM  dbo.BookedProduct WITH(NOLOCK)
									WHERE BpdId = @BpdId
									AND   BpdPrdIsVehicle = 1 AND (BpdCodTypeId IS NULL
																	OR  BpdCodTypeId NOT IN (@sExt,@sXFrm,@sXto)
																)
					)
					BEGIN

						SELECT	@PkgDepositAmt = PkgDepositAmt,
								@PkgDepositPercentage = PkgDepositPercentage
							FROM	dbo.Rental WITH(NOLOCK) INNER JOIN dbo.Package WITH(NOLOCK) ON RntPkgId = PkgId
							WHERE	RntId = @RntId

						IF @PkgDepositAmt>0
							SET @iDepositAmt = @PkgDepositAmt
						ELSE
							SET @iDepositAmt = (@PkgDepositPercentage/100) * @iAmtOwing

					END	--	IF EXISTS(SELECT BpdId FROM  BookedProduct WITH(NOLOCK)
					ELSE
						SET @iDepositAmt = 0
				END -- IF @bDirectAgent = 1
				ELSE
					SET @iDepositAmt = 0

				IF @blsSecurity = 'Y'
					SET @sType = 'B'
				ELSE
					IF @bIsExpense = 'Y'
						SET @sType = 'X'
					ELSE
						SET @sType = 'R'

				IF @iDepositAmt > 0
				BEGIN
					SET @AmtPaid = 0
					SELECT	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	dbo.RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'D'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE 	RntId = @RntId
								AND 	Type = 'D'
								AND 	CurrId = @sCurrCode)
					BEGIN
						--RKSHUKLA
						SET @tempDepositAmt  = @iDepositAmt
						SELECT	@iDepositAmt = @iDepositAmt + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'D'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding

								SET  AmtOwing  = @iDepositAmt ,
									AmtOwingLoc = CASE
															WHEN ISNULL(@tempDepositAmt,0) = 0  THEN  @tempDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
															ELSE @iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
														END ,
								        -- AmtOwingLoc = @iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)  ,
								         AmtPaid = @AmtPaid
						WHERE	RntId = @RntId
						AND		Type = 'D'
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN
						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, 'D', @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iDepositAmt,
								@iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)
					END -- IF EXISTS
				END	-- IF @iDepositAmt > 0

				IF @sType IN ('B','X')
				BEGIN
					SET @AmtPaid = 0
					SELECT	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = @sType
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId
								FROM	@AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = @sType
								AND		CurrId = @sCurrCode)
					BEGIN
						SELECT	@iAmtOwing = @iAmtOwing + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = @sType
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
							SET	AmtOwing  = @iAmtOwing,
								AmtOwingLoc = @iAmtOwing * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1) ,
								AmtPaid	=	@AmtPaid
						WHERE	RntId = @RntId
						AND		Type = @sType
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN
						IF ISNULL(@iAmtOwing, 0) <> 0 OR ISNULL(@AmtPaid, 0) <> 0
						BEGIN
							INSERT INTO @AmountsOutStanding
								(
									RntId, Type, DefCurr,
									CurrId, Curr, AmtOwing,
									AmtOwingLoc, AmtPaid, CurrentPymtAmt
								)
							VALUES
								(
									@RntId, @sType, @defCurr,
									@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing ,
									@iAmtOwing * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
								)
						END -- IF @iAmtOwing IS NOT NULL AND @iAmtOwing <> 0 AND @AmtPaid IS NOT NULL AND @AmtPaid <> 0
					END -- IF EXISTS
				END	-- IF @sType = 'B'
				ELSE
				BEGIN
					SET @AmtPaid = 0
					SELECT	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'R'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId FROM	@AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = 'R'
								AND		CurrId = @sCurrCode)
					BEGIN
						SELECT	@ForReceipt = @ForReceipt  + AmtOwing + (@iAmtOwing - @iDepositAmt)
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'R'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
							SET  	AmtOwing  = @ForReceipt ,
									AmtOwingLoc  = @ForReceipt * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1),
									AmtPaid	=	@AmtPaid
						WHERE	RntId = @RntId
						AND		Type = 'R'
						AND		CurrId = @sCurrCode
					END
					ELSE
					BEGIN
						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, 'R', @defCurr ,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing - @iDepositAmt,
								(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)
					END -- IF EXISTS
				END -- ELSE
			END	-- WHILE @Start <= @Count
		END -- IF @PrdList IS NOT NULL

		/******************************************************/

		SELECT @blsSecurity = 'N', @bIsExpense = 'N', @Err = NULL, @PrdList = NULL
		IF @bAll = 1
			EXEC RES_getBookedProducts
				@sBooId 			= @sBooId,
				@sExclStatusList 	= 'XN,XX' ,
				@bSelectCurrentOnly = 1,
				@sAgnCusSelection 	= 'Customer' ,
				@sIsSecurity 		= @blsSecurity ,
				@sAttCode			= 'EXPENSE' ,
				@sAttValue			= 0 ,
				@ReturnCSV			= 2 ,
				@sBpdList 			= @PrdList OUTPUT,
				@ReturnError 		= @Err OUTPUT
		ELSE
			EXEC RES_getBookedProducts
				@sBooId 			= @sBooId,
				@sRntId 			= @paramRntId,
				@sExclStatusList 	= 'XN,XX' ,
				@bSelectCurrentOnly = 1,
				@sAgnCusSelection 	= 'Customer' ,
				@sIsSecurity 		= @blsSecurity,
				@sAttCode			= 'EXPENSE' ,
				@sAttValue			= 0 ,
				@ReturnCSV			= 2 ,
				@sBpdList 			= @PrdList OUTPUT,
				@ReturnError 		= @Err OUTPUT

		IF @PrdList IS NOT NULL
		BEGIN
			SET @Start = 1
			SET @Count = dbo.getEntry(@PrdList, ',') + 1

			WHILE @Start <= @Count
			BEGIN
				SELECT @BpdId = dbo.getSplitedData(@PrdList, ',', @Start)
				SELECT @Start = @Start + 1
				SELECT @ForReceipt = 0, @iDepositAmt = 0, @iAmtOwing = 0, @tempDepositAmt = 0

				IF EXISTS(SELECT BpdId FROM dbo.BookedPRoduct WITH(NOLOCK)
							WHERE 	BpdId = @BpdId
							AND 	BpdIsCusCharge = 1
							AND 	BpdIsCurr = 1)
				BEGIN
					EXECUTE RES_GetChargeIfXTO
						@BpdId			= @BpdId,
						@sXtoDisplay	= @iAmtOwing OUTPUT
					--SELECT '3rd', @BpdId, 	@iAmtOwing
				END

				SELECT	@RntId = BpdRntId,
						--@iAmtOwing = ISNULL(BpdGrossAmt,0) - ISNULL(BpdAgnDisAmt,0) ,
						@defCurr = dbo.GetCodCode(BpdCodCurrId),
						@sCurrCode = dbo.getCodCode(BpdCodCurrId)
					FROM	dbo.BookedProduct WITH(NOLOCK)
					WHERE	BpdId = @BpdId

				IF @bDirectAgent = 1
				BEGIN
					IF EXISTS (SELECT BpdId
								FROM 	dbo.BookedProduct WITH(NOLOCK) INNER JOIN dbo.SaleableProduct WITH(NOLOCK) ON BpdSapId = SapId
										INNER JOIN dbo.Product WITH(NOLOCK) ON SapPrdId = PrdId
								WHERE	BpdId = @BpdId
								AND		PrdIsVehicle = 1 AND (BpdCodTypeId IS NULL
																OR  BpdCodTypeId NOT IN (@sExt,@sXFrm,@sXto)
															)
					)
					BEGIN
						SELECT	@PkgDepositAmt = PkgDepositAmt,
								@PkgDepositPercentage = PkgDepositPercentage
							FROM	dbo.Rental WITH(NOLOCK) INNER JOIN dbo.Package WITH(NOLOCK) ON RntPkgId = PkgId
							WHERE	RntId = @RntId

						IF @PkgDepositAmt>0
							SET @iDepositAmt = @PkgDepositAmt
						ELSE
							SET @iDepositAmt = (@PkgDepositPercentage/100) * @iAmtOwing
					END	--	IF EXISTS (SELECT Product.IntegrityNo
					ELSE
						SET @iDepositAmt = 0
				END -- IF @bDirectAgent = 1
				ELSE
					SET @iDepositAmt = 0

				IF @blsSecurity = 'Y'
					SET @sType = 'B'
				ELSE
					IF @bIsExpense = 'Y'
						SET @sType = 'X'
					ELSE
						SET @sType = 'R'

				IF @iDepositAmt > 0
				BEGIN
					SET @AmtPaid = 0
					SELECT @AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'D'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = 'D'
								AND		CurrId = @sCurrCode)
					BEGIN
						SET @tempDepositAmt  = @iDepositAmt
						SELECT	@iDepositAmt = @iDepositAmt + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'D'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
							SET	AmtOwing  = @iDepositAmt ,
								AmtOwingLoc =	CASE
													WHEN ISNULL(@tempDepositAmt,0) = 0
														THEN  @tempDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
														ELSE @iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1)
												END,
								AmtPaid = @AmtPaid
						WHERE	RntId = @RntId
						AND		Type = 'D'
						AND		CurrId = @sCurrCode
					END -- IF EXISTS(SELECT RnrntId FROM @AmountsOutStanding
					ELSE
					BEGIN
						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(

								@RntId, 'D', @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iDepositAmt,
								@iDepositAmt * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)
					END -- IF EXISTS
				END	-- IF @iDepositAmt > 0

				IF @sType IN ('B','X')
				BEGIN
					SET @AmtPaid = 0
					SELECT 	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = @sType
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = @sType
								AND		CurrId = @sCurrCode)
					BEGIN
						SELECT	@iAmtOwing = @iAmtOwing + AmtOwing
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type =@sType
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding

							SET AmtOwing  = @iAmtOwing,
								AmtOwingLoc = @iAmtOwing * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1) ,
								AmtPaid	=	@AmtPaid
							WHERE	RntId = @RntId
							AND		Type = @sType
							AND		CurrId = @sCurrCode
					END -- IF EXISTS(SELECT * FROM @AmountsOutStanding
					ELSE
					BEGIN
						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, @sType, @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing,
								@iAmtOwing * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)
					END -- IF EXISTS
				END	-- IF @sType = 'B'
				ELSE IF @sType = 'R'
				BEGIN

					SET @AmtPaid = 0
					SELECT	@AmtPaid = SUM(ISNULL(RptChargeAmt,0))
						FROM	RentalPayment WITH(NOLOCK)
						WHERE	RptRntId = @RntId
						AND		RptType = 'R'
						AND		RptChargeCurrId  = dbo.GetCodeId(23,@DefCurr)

					IF EXISTS(SELECT RntId FROM @AmountsOutStanding
								WHERE	RntId = @RntId
								AND		Type = 'R'
								AND		CurrId = @sCurrCode)
					BEGIN
						IF ISNULL(@tempDepositAmt,0)<>0
							SET @iDepositAmt = @tempDepositAmt
						SELECT @ForReceipt = @ForReceipt  + AmtOwing + (@iAmtOwing - @iDepositAmt)
							FROM	@AmountsOutStanding
							WHERE	RntId = @RntId
							AND		Type = 'R'
							AND		CurrId = @sCurrCode

						UPDATE @AmountsOutStanding
							SET AmtOwing  = @ForReceipt ,
								AmtOwingLoc  = @ForReceipt * ISNULL(dbo.getExchangeRate(@DefCurr,@sCurrCode),1),
								AmtPaid	=	@AmtPaid
							WHERE	RntId = @RntId
							AND		Type = 'R'
							AND		CurrId = @sCurrCode

					END -- IF EXISTS(SELECT * FROM @AmountsOutStanding
					ELSE
					BEGIN
						--select RntId, Type, @RntId, 'R' * FROM @AmountsOutStanding

						INSERT INTO @AmountsOutStanding
							(
								RntId, Type, DefCurr,
								CurrId, Curr, AmtOwing,
								AmtOwingLoc, AmtPaid, CurrentPymtAmt
							)
						VALUES
							(
								@RntId, 'R', @defCurr,
								@sCurrCode, dbo.getCodeId(23,@sCurrCode), @iAmtOwing - @iDepositAmt,
								(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
							)

					END -- IF EXISTS*/
				END -- ELSE IF @sType = 'R'
			END	--	WHILE @Start <= @Count
		END -- IF @PrdList IS NOT NULL

	-- CHECK FOR BOND IF IT Exists In RentalPayment and Not In the temporary Table
	IF @RntId IS NULL	SET @RntId = @paramRntId
	IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)
				WHERE	RptRntId = @RntId
				AND  	RptType = 'B')
	BEGIN
		IF NOT EXISTS(SELECT RntId FROM @AmountsOutStanding
						WHERE	RntId = @RntId
						AND		Type = 'B'
						-- AND		CurrId = @sCurrCode
					)
		BEGIN
			SET @AmtPaid = 0
			SELECT	@iAmtOwing = 0 ,
					@sCurrCode = dbo.getCodCode(RptChargeCurrId) ,
					@AmtPaid =  @AmtPaid + ISNULL(RptChargeAmt, 0)
				FROM	RentalPayment WITH(NOLOCK)
				WHERE	RptRntId = @RntId
				AND  	RptType = 'B'

			IF @AmtPaid IS NOT NULL AND @AmtPaid <> 0
				INSERT INTO @AmountsOutStanding
					(
						RntId, Type, DefCurr,
						CurrId, Curr, AmtOwing,
						AmtOwingLoc, AmtPaid, CurrentPymtAmt
					)
				VALUES
					(
						@RntId, 'B', @defCurr,
						@sCurrCode, dbo.getCodeId(23,@sCurrCode), 0,
						(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
					)
		END -- IF NOT EXISTS(SELECT RntId FROM @AmountsOutStanding
	END -- IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)


	-- CHECK FOR Receipt IF IT Exists In RentalPayment and Not In the temporary Table
	IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)
				WHERE	RptRntId = @RntId
				AND  	RptType = 'R')
	BEGIN
		IF NOT EXISTS(SELECT RntId 	FROM @AmountsOutStanding
						WHERE RntId = @RntId
						AND		Type = 'R'
						-- AND		CurrId = @sCurrCode
					)
		BEGIN
			SET @AmtPaid = 0

			SELECT	@iAmtOwing = 0 ,
					@sCurrCode = dbo.getCodCode(RptChargeCurrId) ,
					@AmtPaid =  @AmtPaid + ISNULL(RptChargeAmt, 0)
				FROM	RentalPayment WITH(NOLOCK)
				WHERE  	RptRntId = @RntId
				AND  	RptType = 'R'


			IF @AmtPaid IS NOT NULL AND @AmtPaid <> 0
				INSERT INTO @AmountsOutStanding
					(
						RntId, Type, DefCurr,
						CurrId,	Curr, AmtOwing,
						AmtOwingLoc, AmtPaid, CurrentPymtAmt
					)
				VALUES
					(
						@RntId, 'R', @defCurr,
						@sCurrCode, dbo.getCodeId(23,@sCurrCode), 0,
						(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
					)
		END -- IF NOT EXISTS(SELECT RntId 	FROM @AmountsOutStanding
	END -- IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)

	-- CHECK FOR Deposit IF IT Exists In RentalPayment and Not In the temporary Table
	IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)
				WHERE	RptRntId = @RntId
				AND  	RptType = 'D')
	BEGIN

		IF NOT EXISTS(SELECT RntId FROM @AmountsOutStanding
						WHERE	RntId = @RntId
						AND		Type = 'D'
						-- AND		CurrId = @sCurrCode
					)
		BEGIN
			SET @AmtPaid = 0
			SELECT	@iAmtOwing = 0 ,
					@sCurrCode = dbo.getCodCode(RptChargeCurrId) ,
					@AmtPaid =  @AmtPaid + ISNULL(RptChargeAmt, 0)
				FROM	RentalPayment WITH(NOLOCK)
				WHERE  	RptRntId = @RntId
				AND  	RptType = 'D'

			IF @AmtPaid IS NOT NULL AND @AmtPaid <> 0
				INSERT INTO @AmountsOutStanding
					(
						RntId, Type, DefCurr,
						CurrId, Curr, AmtOwing,
						AmtOwingLoc, AmtPaid, CurrentPymtAmt
					)
				VALUES
					(
						@RntId, 'D', @defCurr,
						@sCurrCode, dbo.getCodeId(23,@sCurrCode), 0,
						(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
					)
		END -- IF NOT EXISTS(SELECT RntId FROM @AmountsOutStanding
	END -- IF EXISTS(SELECT RptId FROM RentalPayment WITH(NOLOCK)

	-- CHECK FOR Receipt IF IT Exists In RentalPayment and Not In the temporary Table
	IF EXISTS(SELECT rptid FROM RentalPayment WITH(NOLOCK)
				WHERE  	RptRntId = @RntId
				AND  	RptType = 'X')
	BEGIN
		IF NOT EXISTS(SELECT RntId 	FROM @AmountsOutStanding
						WHERE	RntId = @RntId
						AND		Type = 'X'
						-- AND		CurrId = @sCurrCode
					)
		BEGIN
			SET @AmtPaid = 0
			SELECT	@iAmtOwing 	= 0 ,
					@sCurrCode 	= dbo.getCodCode(RptChargeCurrId) ,
					@AmtPaid =  @AmtPaid + ISNULL(RptChargeAmt, 0)
				FROM 	RentalPayment WITH(NOLOCK)
				WHERE	RptRntId = @RntId
				AND  	RptType = 'X'

			IF @AmtPaid IS NOT NULL AND @AmtPaid <> 0
				INSERT INTO @AmountsOutStanding
					(
						RntId, Type, DefCurr,
						CurrId, Curr, AmtOwing,
						AmtOwingLoc, AmtPaid, CurrentPymtAmt
					)
				VALUES
					(
						@RntId, 'X', @defCurr ,
						@sCurrCode, dbo.getCodeId(23,@sCurrCode), 0 ,
						(@iAmtOwing - @iDepositAmt) * ISNULL(dbo.getExchangeRate(@defCurr,@sCurrCode),1), @AmtPaid, NULL
					)
		END -- IF NOT EXISTS(SELECT RntId 	FROM @AmountsOutStanding
	END -- IF EXISTS(SELECT IntegrityNo FROM RentalPayment WITH(NOLOCK)


/*SELECT '<RentalDetail>'
SELECT 	ISNULL(Rental.RntId,'')							AS	RntId ,
		ISNULL(RntNum,'') 								AS	Rntl ,
		ISNULL(Type,'')									AS	Type ,
		ISNULL(CurrId,'')								AS	Curr ,
		ISNULL(Curr,'')									AS	CurrId ,
		ISNULL(CONVERT(VARCHAR,AmtOwing),'0.00')		AS	TotalDue ,
		ISNULL(CONVERT(VARCHAR,AmtOwingLoc),'0.00')		AS	TotalDueLoc ,
		ISNULL(CONVERT(VARCHAR,AmtPaid),'0.00')			AS	TotalPaid ,
		ISNULL(CONVERT(VARCHAR,CustToPayAmt),'0.00')	AS	BlnDue ,
		ISNULL(CONVERT(VARCHAR,CurrentPymtAmt),'0.00')	AS	CustToPay ,
		0												AS	SelToPay
	FROM Rental WITH(NOLOCK), @AmountsOutStanding	AS	RentalPmt
	WHERE Rental.RntId = RentalPmt.RntId
	ORDER BY RntNum, Curr , AmtOwing DESC
	FOR XML AUTO, ELEMENTS
SELECT '</RentalDetail>'

SELECT '<TotalDetail DefCurr="' + @DefToSend + '">'
*/
IF @defCurr IS NULL
	SET @defCurr = @DefToSend

IF EXISTS (SELECT RntId FROM @AmountsOutStanding  WHERE CurrId = @defCurr)
BEGIN
	SELECT 	@Currency 	= case when @Currency 	is null 	or @Currency 	= '' then ISNULL(@defCurr,'') 											else @Currency + ',' + ISNULL(@defCurr,'') 												end,
			@TotDue	 	= case when @TotDue 	is null 	or @TotDue 		= '' then ISNULL(CONVERT(VARCHAR,SUM(ISNULL(AmtOwing,0))),'') 			else @TotDue + ',' + ISNULL(CONVERT(VARCHAR,SUM(ISNULL(AmtOwing,0))),'') 				end,
			@TotalPaidLo = case when @TotalPaidLo is null	or @TotalPaidLo = '' then CONVERT(VARCHAR,ISNULL(SUM(ISNULL(AmtPaid,0)),0)) 			else @TotalPaidLo + ',' + CONVERT(VARCHAR,ISNULL(SUM(ISNULL(AmtPaid,0)),0)) 			end,
			@BlnDueLoc 	= case when @BlnDueLoc 	is null 	or @BlnDueLoc 	= '' then CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CustToPayAmt,0)),0))		else @BlnDueLoc + ',' + CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CustToPayAmt,0)),0)) 			end,
			@CustToPay 	= case when @CustToPay 	is null 	or @CustToPay 	= '' then 	CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CurrentPymtAmt,0)),0))	else @CustToPay + ',' + 	CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CurrentPymtAmt,0)),0))	end
	FROM
		@AmountsOutStanding
	WHERE
		CurrId = @defCurr
END

IF ((SELECT COUNT(DISTINCT CurrId) FROM @AmountsOutStanding)>1)
BEGIN
	SELECT 	@Currency 	= case when @Currency 	is null 	or @Currency 	= '' then ISNULL(CurrId,'') 											else @Currency + ',' + ISNULL(CurrId,'') 												end,
			@TotDue	 	= case when @TotDue 	is null 	or @TotDue 		= '' then ISNULL(CONVERT(VARCHAR,SUM(ISNULL(AmtOwing,0))),'') 			else @TotDue + ',' + ISNULL(CONVERT(VARCHAR,SUM(ISNULL(AmtOwing,0))),'') 				end,
			@TotalPaidLo = case when @TotalPaidLo is null	or @TotalPaidLo = '' then CONVERT(VARCHAR,ISNULL(SUM(ISNULL(AmtPaid,0)),0)) 			else @TotalPaidLo + ',' + CONVERT(VARCHAR,ISNULL(SUM(ISNULL(AmtPaid,0)),0)) 			end,
			@BlnDueLoc 	= case when @BlnDueLoc 	is null 	or @BlnDueLoc 	= '' then CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CustToPayAmt,0)),0))		else @BlnDueLoc + ',' + CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CustToPayAmt,0)),0)) 			end,
			@CustToPay 	= case when @CustToPay 	is null 	or @CustToPay 	= '' then 	CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CurrentPymtAmt,0)),0))	else @CustToPay + ',' + 	CONVERT(VARCHAR,ISNULL(SUM(ISNULL(CurrentPymtAmt,0)),0))	end
	FROM
		@AmountsOutStanding
	WHERE
		CurrId <> @defCurr
	GROUP BY
		CurrId
END
--SELECT '</TotalDetail>'
/*
DECLARE @AllowMod 	BIT,
		@ModBy		VARCHAR(24),
		@bAgentOK	BIT,
		@bCustomerOK	BIT
EXECUTE	CheckPaymentMade
	@sBooId				= @sBooId,
	@sRntId				= @paramRntId,
	@sAgtCliSelection	= '',
	@bAgentOK			= @bAgentOK 	OUTPUT,
	@bCustomerOK		= @bCustomerOK	OUTPUT

IF EXISTS(SELECT CodId FROM Agent WITH(NOLOCK), Booking WITH(NOLOCK), Code WITH(NOLOCK)
			WHERE	AgnId = BooAgnId
			AND		CodId = AgnCodDebtStatId
			AND		CodCode = 'UnApproved'
			AND		BooId = @sBooId)
	AND	@bAgentOK = 0
BEGIN
	SET @AllowMod = dbo.GEN_checkUserRole(@DefaultUsr, '', (SELECT UsrCtyCode FROM UserInfo WITH(NOLOCK) WHERE UsrCode = @DefaultUsr) + 'AUTHAGNCKOROLE')
	SELECT @ModBy = 'By : ' + CASE WHEN ModUsrId IS NOT NULL THEN ModUsrId ELSE AddUsrId END
		FROM PaymentAuth WITH(NOLOCK)
		WHERE	PauRntId = @paramRntId
	SET @ModBy = ISNULL(@ModBy, '')
	IF (SELECT PauIsAuthorised FROM PaymentAuth WITH(NOLOCK)
				WHERE	PauRntId = @paramRntId) = 1
	BEGIN
		SELECT '<RntAut dis="1" chk="1" pp="UnApproved" allowMod="' + CAST(@AllowMod AS VARCHAR) + '" modUsr = "' + @ModBy + '"/>'
	END
	ELSE
		SELECT '<RntAut dis="1" chk="0" pp="UnApproved" allowMod="' + CAST(@AllowMod AS VARCHAR) + '" modUsr = "' + @ModBy + '"/>'
END
ELSE
	SELECT '<RntAut dis="0" chk="0" pp="" allowMod="0" modUsr = ""/>'

*/

Destroy_Cursor:
	RETURN
END








GO
