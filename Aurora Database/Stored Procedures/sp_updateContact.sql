set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go






ALTER    PROCEDURE [dbo].[sp_updateContact]
	@sQueryAction		varchar(2) ='',
	@sConId			varchar(64)='',
	@sConPrntTableName	varchar(64)='',
	@sConPrntId		varchar(64)='',
	@sConCodTypId		varchar(64)='',
        @sConName		varchar(64)='',
	@sConTitle		varchar(64)='',
	@sConFirstName          varchar(128)='',                  
	@sConLastName		varchar(128)='',
	@sConPosition		varchar(64)='',
	@sConComments		varchar(256)='',
	@iIntegrityNo		int,
	@sAddModUsrId		varchar(64),
	@sAddPrgmName		varchar(64),
	@sRetirnError		varchar(500) 	OUTPUT

AS


SET NOCOUNT ON
DECLARE @inumber     int
DECLARE @sError	varchar(100)

DECLARE @Guid varchar(64)
SET @Guid=REPLACE(NewId(),'-','')

SET @sRetirnError = ''
IF(@sQueryAction = 'I')
BEGIN
	INSERT INTO Contact  WITH (ROWLOCK) (
				ConId,
				ConPrntTableName,
				ConPrntId,
				ConCodContactId,
				ConName,
				ConTitle, 
				ConFirstName,
				ConLastName,
				ConPosition,
				ConComments,
				IntegrityNo,
				AddUsrId,
				ModUsrId, 
				AddDateTime,
				ModDateTime,
				AddPrgmName
				)
		VALUES  (
				@Guid,
				@sConPrntTableName,
				@sConPrntId,
				@sConCodTypId,
				@sConName,
				@sConTitle, 
				@sConFirstName,
				@sConLastName,
				@sConPosition,
				@sConComments,
				@iIntegrityNo,
				@sAddModUsrId,
				@sAddModUsrId,
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP,
				@sAddPrgmName
			)
	--JL 2008-05-21 return guid
	SELECT @sRetirnError='GEN000/' + @Guid 
	SELECT 'GEN000/' + @Guid
	RETURN
END

IF(@sQueryAction = 'M')
BEGIN
	SET @inumber = -10
	
	SELECT @inumber =  IntegrityNo FROM Contact  WITH (NOLOCK) 
		WHERE ConId = @sConId
	IF(@inumber = -10 )
	BEGIN
	       SELECT @sError = smsdesc FROM messages WITH (NOLOCK) 
		 WHERE smscode = 'GEN001'
	       SELECT @sRetirnError='GEN001/' + @sError 
	       RETURN
	END
	IF(@inumber <> @iIntegrityNo )
	BEGIN
	       SELECT @sError = smsdesc FROM messages  WITH (NOLOCK) WHERE smscode = 'GEN002'
	       SELECT @sRetirnError='GEN002/' + @sError
	      RETURN 
	END

	UPDATE Contact
	     SET ConPrntTableName	= @sConPrntTableName,
		ConPrntId		= @sConPrntId,
		ConCodContactId	= @sConCodTypId,
		ConName		= @sConName,
		ConTitle			= @sConTitle,
		ConFirstName		= @sConFirstName,
		ConLastName		= @sConLastName,
		ConPosition		= @sConPosition,
		ConComments		= @sConComments,
		IntegrityNo		= @iIntegrityNo + 1,
		ModUsrId		= @sAddModUsrId,
		ModDateTime		= CURRENT_TIMESTAMP
	WHERE 
		ConId = @sConId 
			AND	
		IntegrityNo = @iIntegrityNo
	RETURN
END
IF(@sQueryAction = 'D')
BEGIN
	/******************************* DELETE RECORD FROM PhoneNumber TABLE *****************************/
	DELETE 
		FROM 		
			PhoneNumber WITH (ROWLOCK) 
		WHERE	
			PhnPrntId  = @sConId
				AND
			PhnPrntTableName = 'Contact'
	
	DELETE 
		FROM 
			Contact  WITH (ROWLOCK) 
		WHERE 
			ConId = @sConId  
				AND 
			ConPrntTableName = 'Agent'
	
END
IF(@sQueryAction = 'DC')
BEGIN
	DELETE FROM 	Contact  WITH (ROWLOCK) 
		WHERE ConId = @sConId  
				AND 
			ConPrntTableName = 'Location'
END




























