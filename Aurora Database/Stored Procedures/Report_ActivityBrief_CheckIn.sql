CREATE  PROCEDURE [dbo].[Report_ActivityBrief_CheckIn]
	@sCtyCode		VARCHAR(64),
	@sLocCode		VARCHAR(64) = '',
	@sFromDate		VARCHAR(12),
	@sToDate		VARCHAR(12),
	@sBrdCode		VARCHAR(10) = '',
	@sClaCode		VARCHAR(64) = '',
	@sTypCode		VARCHAR(12) = '',
	@sPrdShortName	VARCHAR(8) = ''
AS
SET NOCOUNT ON

/*
DECLARE	@sCtyCode		VARCHAR(64),
		@sLocCode		VARCHAR(64),
		@sFromDate		VARCHAR(12),
		@sToDate		VARCHAR(12),
		@sBrdCode		VARCHAR(10),
		@sClaCode		VARCHAR(64),
		@sTypCode		VARCHAR(12),
		@sPrdShortName	VARCHAR(8)

SET		@sCtyCode = 'NZ'
SET		@sLocCode = ''
SET		@sFromDate = '01/04/2008'
SET		@sToDate = '11/04/2008'
SET		@sBrdCode = ''
SET		@sClaCode = 'AV'
SET		@sTypCode = '4B'
SET		@sPrdShortName = '4BB'
*/
-- Convert Class, Type and Product parameters "Code" to "ID"
declare @sClaId varchar(64)
set @sClaId = ISNULL((SELECT ClaID FROM dbo.Class WHERE (ClaCode = @sClaCode)), '')

declare @sTypId varchar(64)
set @sTypId = ISNULL((	SELECT TypId
						FROM Type INNER JOIN Class ON Type.TypClaId = Class.ClaId
						WHERE (TypCode = @sTypCode)
							AND EXISTS (SELECT Top 1 * FROM Product WHERE Product.PrdTypId = Type.TypId
										AND Product.PrdIsActive = 1 AND Product.PrdIsVehicle = 1)
							AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
					), '')

declare @sPrdId varchar(64)
set @sPrdId = (	SELECT PrdId
				FROM  Product
					INNER JOIN Type ON Product.PrdTypId = Type.TypId
					INNER JOIN Class ON Type.TypClaId = Class.ClaId
				WHERE (PrdShortName = @sPrdShortName)
					AND Product.PrdIsActive = 1
					AND Product.PrdIsVehicle = 1
					AND (@sBrdCode IS NULL OR Product.PrdBrdCode = @sBrdCode)
					AND (@sTypId IS NULL OR Type.TypId = @sTypId)
					AND (@sClaId IS NULL OR Class.ClaID = @sClaId)
			)

DECLARE 	@sCodIdXfrm	VARCHAR(64),
			@sCodIdExt	VARCHAR(64)

declare @Ckotable table (ttprdshortname varchar(32), ttrntckowhen varchar(10), ttxmltext varchar(2000))
declare @Ckitable table (ttprdshortname varchar(32), ttrntckiwhen varchar(10), ttxmltext varchar(2000))

DECLARE @AU_THRIFTYPRDS VARCHAR(1000)
DECLARE @AU_THRIFTYEFFDT datetime

SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'

declare @Result_CheckIn table
(
	CkiLocCode		VARCHAR(64),
	Brand 			VARCHAR(64),
	BkdVehicle		VARCHAR(64),
	UnitNum 		VARCHAR(64),
	SchedVehicle	VARCHAR(64),
	CustName		VARCHAR(64),
	IsVip			VARCHAR(1),
	Pax				VARCHAR(64),
	DepartureRef	VARCHAR(64),
	ConnecDate		VARCHAR(64),
	FromDate		VARCHAR(20),
	ToDate			VARCHAR(20),
	Period			VARCHAR(20),
	PkgCode			VARCHAR(64),
	CashBond		VARCHAR(64),
	BookRental		VARCHAR(64),
	ActiveNote		VARCHAR(1)
)

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME,
		@sRentalId	VARCHAR(64),
		@ssBpdid    varchar(64)


SET @StartDate= CONVERT(DATETIME, @sFromDate,103)
SET @EndDate=CONVERT(DATETIME, @sToDate + ' 23:59:59' ,103)
SET @WkDATE=CONVERT(DATETIME,@sFromDate,103)

DECLARE @ssBrd 			VARCHAR(64),
		@sBooNum		VARCHAR(64),
		@sBooId			varchar(64),
		@sRntNum		VARCHAR(64),
		@sPrd			VARCHAR(64),
		@sPkgCode		VARCHAR(64),
		@ssCkoLocCode	VARCHAR(64),
		@dRntCkoWhen   	datetime,
		@dRntCkiWhen	datetime,
		@sXMLText		varchar(2000)

SET @sXMLText = null


DECLARE @sRentalIdIN		VARCHAR(64),
		@ssBpdIdIn			VARCHAR(64),
		@sBooNumIn			VARCHAR(64),
		@sBooIdIn			VARCHAR(64),
		@sRntNumIn			VARCHAR(64),
		@sPkgCodeIn			VARCHAR(64),
		@ssBrdIn 			VARCHAR(64),
		@sPrdIn				VARCHAR(64),
		@ssCkiLocCode		VARCHAR(64)
SET @ssBpdIdIn = NULL

DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  RntId
	 FROM 	dbo.Rental WITH(NOLOCK)
	 WHERE	RntStatus IN ('CO','KK', 'CI')
	 AND	RntCkiWhen BETWEEN @StartDate AND @EndDate
	 AND 	(RntCkiLocCode = @sLocCode OR (@sLocCode = ''
											AND		EXISTS(SELECT TOP 1 loccode FROM Location WITH(NOLOCK), TownCity WITH(NOLOCK)
															WHERE	RntCkiLocCode = LocCode
															AND		LocTctCode = TctCode
															AND		TctCtyCode = @sCtyCode)))
OPEN Cur_in

FETCH NEXT FROM Cur_in INTO @sRentalIdIN

WHILE @@FETCH_STATUS=0
BEGIN
	DECLARE @BpdIdIn 			VARCHAR(2000),
			@ReturnErrorIn	 	VARCHAR(500),
			@sUnitNumIn 		VARCHAR(64),
			@sSchedVehicleIn	VARCHAR(64),
			@sCustNameIn		VARCHAR(64),
			@sPaxIn			 	int,
			@sDeptRefIn			VARCHAR(64),
			@sDateIn			VARCHAR(64),
			@sConWhenIn			datetime,
			@sFromIn			VARCHAR(20),
			@sToIn				VARCHAR(20),
			@sRntCodUomIdIn 	VARCHAR(64),
			@sPeriodIn			VARCHAR(20),
			@sBookRentalIn		VARCHAR(64),
			@sBookingIdIn		VARCHAR(64),
			@sCurrIdIn 			VARCHAR(64),
			@sAmountIn			money

	SET 	@sSchedVehicleIn=''
	SET 	@sUnitNumIn=''
	SET		@sCustNameIn=''
	SET		@sPaxIn=0
	SET		@sDeptRefIn=''
	SET		@sDateIn=''
	SET		@sRntCodUomIdIn=''
	SET		@sPeriodIn=''
	SET		@sBookRentalIn=''
	SET 	@sBooNumIn = NULL
	SET		@sBooIdIn	= NULL


	select	@sBooNumIn  = Boonum,
			@sBooidIn	= Booid,
			@ssBrdIn	= PkgBrdCode,
			@sRntNumIn  = RntNum,
			@sPkgCodeIn	= PkgCode,
			@ssCkiLocCode = RntCkiLocCode,
			@dRntCkoWhen   = RntCkoWhen,
			@dRntCkiWhen   = RntCkiWhen
		 From booking with (nolock), dbo.rental with (nolock), dbo.package (nolock)
			where rntid = @sRentalIdIN
			and rntbooid = Booid
			and Rntpkgid = pkgid

	SET		@sBookRentalIn = ISNULL(@sBooNumIn,'') + '/' + ISNULL(@sRntNumIn,'')
	SET 	@BpdIdIn = NULL

	SELECT 	@ssBpdIdIn = BpdId,
			@sUnitNumIn = ISNULL(BpdUnitNum,'')
		From dbo.Bookedproduct (nolock), dbo.saleableproduct (nolock), dbo.product (nolock)
		WHERE bpdRntid = @sRentalIdIN
		AND   BpdPrdIsVehicle = 1
		AND   BpdSapid = Sapid
		AND   SapPrdId = Prdid
		AND   BpdStatus = 'KK'
		AND   (BpdCodTypeid is null OR BpdCodTypeid <>  @sCodIdXfrm)
		Order By BpdCkoWhen

	select 	@sPrdIn = prdshortname
	from dbo.product (nolock), rental (nolock)
	WHERE 	prdid 	= rntprdrequestedvehicleid
	and		rntid	= @sRentalIdIN

	IF @ssBpdIdIn is null or @ssBpdIdIn = ''
		GOTO GETNEXTRENTALCKI

	IF @sPrdId is not null and @sPrdId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock)
					WHERE bpdid = @ssBpdIdIn and bpdsapid = sapid and sapprdid = prdid and prdid <> @sPrdId)
			GOTO GETNEXTRENTALCKI
	END
	IF @sTypId is not null and @sTypId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock)
					WHERE bpdid = @ssBpdIdIn and bpdsapid = sapid and sapprdid = prdid and prdtypid <> @sTypId)
			GOTO GETNEXTRENTALCKI
	END
	IF @sClaId is not null and @sClaId <> ''
	BEGIN
		IF EXISTS(select bpdid from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock)
					WHERE bpdid = @ssBpdIdIn and bpdsapid = sapid and sapprdid = prdid and prdtypid = typid and typclaid <> @sClaId)
			GOTO GETNEXTRENTALCKI
	END

	-- Thrifty Check
	IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> '' and @sCtyCode = 'AU'
	BEGIN
		IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
									dbo.saleableproduct (nolock),
									dbo.product (nolock),
									dbo.type (nolock),
									dbo.class (nolock)
					WHERE bpdid = @ssBpdIdIn
					and bpdsapid = sapid
					and sapprdid = prdid
					and prdtypid = typid
					and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkiWhen >= @AU_THRIFTYEFFDT and @dRntCkoWhen >= @AU_THRIFTYEFFDT
			GOTO GETNEXTRENTALCKI
	END

	IF @sBrdCode is not null and @sBrdCode <> '' AND @ssBrdIn <> @sBrdCode
		GOTO GETNEXTRENTALCKI

	-- get sched vehicle
	SELECT 	@sSchedVehicleIn = FleetModelCode
	 FROM 	aimsprod.dbo.FleetModel WITH (NOLOCK) INNER JOIN
			fleetassetview WITH (NOLOCK)
			ON aimsprod.dbo.FleetModel.FleetModelId  =
			fleetassetview.FleetModelId
	 WHERE 	UnitNumber = @sUnitNumIn

	--get hirer
	SELECT	@sCustNameIn = ISNULL(Customer.CusLastName,'')
	 FROM	Customer WITH (NOLOCK), Traveller WITH (NOLOCK)
	 WHERE	Traveller.TrvRntId = @sRentalIdIN
	 AND	Traveller.TrvIsPrimaryHirer = 1
	 AND	Customer.CusId = Traveller.TrvCusId

	IF ISNULL(@sCustNameIn,'')=''
		SELECT 	@sCustNameIn = ISNULL(BooLastName,'')
		 FROM 	Rental WITH(NOLOCK), Booking WITH(NOLOCK)
		 WHERE	RntBooId = BooID
		 AND	RntId = @sRentalIdIN

	-- get Pax
	SELECT	@sPaxIn = ISNULL(RntNumOfAdults, 0) + ISNULL(RntNumOfInfants, 0) + ISNULL(RntNumOfChildren ,0) ,
			@sDeptRefIn = ISNULL(RntDeptConnectionRef,''),
			@sConWhenIn = ISNULL(RntDeptConnectionWhen,''),
			@sFromIn = RntCkoLocCode + '  ' + CONVERT(CHAR(10),RntCkoWhen,103),
			@sToIn = RntCkiLocCode + '  ' + CONVERT(CHAR(10),RntCkiWhen,103),
			@sRntCodUomIdIn = RntCodUomId,
			@sPeriodIn =  CAST(RntNumOfHirePeriods as VARCHAR) + ' Days'
	 FROM	Rental WITH (NOLOCK)
	 WHERE	RntId = @sRentalIdIN

	SELECT 	@sDateIn = CONVERT(VARCHAR,@sConWhenIn,103) + ' ' + substring((CONVERT(VARCHAR,@sConWhenIn,108)),1,2)+substring((CONVERT(VARCHAR,@sConWhenIn,108)),4,2)
	IF 		@sDAteIN = '0' OR @sDAteIN is null OR @sDateIN like '%1900%'
		SET @sDateIN = ''

	-- Work out tabular results AJ May 2008
	DECLARE @sRntIsVip VARCHAR(1)
	SELECT	@sRntIsVip = (CASE RntIsVip WHEN '1' THEN 'Y' ELSE 'N' END)
	FROM	Rental WITH (NOLOCK)
	WHERE	RntId = @sRentalIdIN

	DECLARE @sCashBond VARCHAR(64)
	SET @sCashBond = ''
	IF exists(select rptid from dbo.RentalPayment WITH (NOLOCK) INNER JOIN
            dbo.Payment WITH (NOLOCK)  ON RentalPayment.RptPmtId = Payment.PmtId INNER JOIN
            dbo.PaymentMethod WITH (NOLOCK) ON Payment.PmtPtmId = PaymentMethod.PtmId
	 WHERE 	(PaymentMethod.PtmIsCash = 1)
	 AND 	(RentalPayment.RptType = 'B')
	 AND 	(RentalPayment.RptRntId = @sRentalIdIN))
	BEGIN
		SELECT  @sCashBond = convert(varchar,ISNULL(SUM(RentalPayment.RptChargeAmt),0)) + '  ' +  ISNULL(dbo.getCodCode(RentalPayment.RptChargeCurrId),'')
		 FROM   RentalPayment WITH (NOLOCK) INNER JOIN
                Payment WITH (NOLOCK)  ON RentalPayment.RptPmtId = Payment.PmtId INNER JOIN
                PaymentMethod WITH (NOLOCK) ON Payment.PmtPtmId = PaymentMethod.PtmId
		 WHERE 	(PaymentMethod.PtmIsCash = 1)
		 AND 	(RentalPayment.RptType = 'B')
		 AND 	(RentalPayment.RptRntId = @sRentalIdIN)
		 GROUP BY 	RentalPayment.RptChargeCurrId, PaymentMethod.PtmIsCash, RentalPayment.RptType, RentalPayment.RptRntId
	END

	DECLARE @sActiveNote VARCHAR(1)
	IF EXISTS(	SELECT  nteid
				 FROM 	Note WITH (NOLOCK)
				 WHERE 	Note.NteRntId = @sRentalIdIN
				 AND	Note.ntebooid = @sBooidIN
				 AND		NteIsActive = 1 )
		SELECT	@sActiveNote  = 'Y'
	ELSE
		SELECT	@sActiveNote = 'N'

	insert into @Result_CheckIn values
	(
		LTRIM(RTRIM(@ssCkiLocCode)),
		@ssBrdIn,
		ISNULL(@sPrdIn, ''),
		ISNULL(@sUnitNumIn, ''),
		ISNULL(@sSchedVehicleIn,''),
		ISNULL(@sCustNameIn, ''),
		@sRntIsVip,
		CAST(@sPaxIn as VARCHAR),
		ISNULL(@sDeptRefIn, ''),
		ISNULL(@sDateIn, ''),
		ISNULL(@sFromIn, ''),
		ISNULL(@sToIn, ''),
		ISNULL(@sPeriodIn, 0),
		ISNULL(@sPkgCodeIn, ''),
		@sCashBond,
		@sBookRentalIn,
		@sActiveNote
	)

	GETNEXTRENTALCKI:
	SET @sPrdIn = null
	SET @dRntCkiWhen = null
	SET @sXmlText = null

	FETCH NEXT FROM Cur_in INTO @sRentalIdIN
END

select *
from @Result_CheckIn
where Brand IN (		-- KX fix AJ May 2008
				SELECT	Brand.BrdCode
				FROM	UserInfo INNER JOIN
						UserCompany ON UserInfo.UsrId = UserCompany.UsrId INNER JOIN
						Brand ON UserCompany.ComCode = Brand.BrdComCode
				WHERE   (UserInfo.UsrCode = User)
				)





GO
