USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Agent_NewId]    Script Date: 06/06/2008 17:05:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Agent_NewId] 
AS

SELECT CAST (NEWID() AS varchar (64))
