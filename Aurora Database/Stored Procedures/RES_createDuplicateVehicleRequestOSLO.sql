CREATE       PROCEDURE [dbo].[RES_createDuplicateVehicleRequestOSLO]
	@tXmlStr		text	= DEFAULT
AS

SET NOCOUNT ON
DECLARE
	@idoc			int,
	@VhrId			varchar(64),
	@choBranch		varchar(24),
	@choDate		varchar(24),
	@choAmPm		varchar(3),
	@chiBranch		varchar(24),
	@chiDate		varchar(24),
	@chiAmPm		varchar(3),
	@prdId			varchar(64),
	@sError			varchar(500),
	@NewVhrId		varchar(64),
	@hirPrd			varchar(4),
	@BkrId			varchar(64),
	@minDays		varchar(24),
	@dMinChkDate	datetime,
	@sMinChkDate	varchar(10)

	-- KX Changes :RKS
	DECLARE		@sComCode	VARCHAR(64)


	SET @sError = ''
	SET @NewVhrId = ''


	EXEC sp_xml_preparedocument @idoc OUTPUT, @tXmlStr

	SELECT  @BkrId			= BkrId,
			@VhrId			= vhrId,
			@choBranch		= choBranch,
			@choDate		= choDate,
			@choAmPm		= choAmPm,
			@chiBranch		= chiBranch,
			@chiDate		= chiDate,
			@chiAmPm		= chiAmPm,
			@prdId			= prdId,
			@hirPrd			= hirPrd
	FROM OPENXML(@idoc,'/Root',3)
	WITH (
			BkrId		varchar(64),
			vhrId		varchar(64),
			choBranch	varchar(24),
			choDate		varchar(24),
			choAmPm		varchar(3),
			chiBranch	varchar(24),
			chiDate		varchar(24),
			chiAmPm		varchar(3),
			prdId		varchar(64),
			hirPrd		varchar(4)
		)
	EXEC sp_xml_removedocument @idoc

	-- KX Changes :RKS
	SELECT @sComCode = dbo.fun_getCompany
				(
					NULL,	--	VARCHAR(64),
					NULL,	--@sLocCode	VARCHAR(64),
					NULL,	--@sBrdCode	VARCHAR(64),
					@prdId,	--@sPrdId		VARCHAR(64),
					NULL,	--@sPkgId		VARCHAR(64),
					NULL,	--@sdummy1	VARCHAR(64),
					NULL,	--@sdummy2	VARCHAR(64),
					NULL	--@sdummy3	VARCHAR(64)
				)



	IF NOT EXISTS (SELECT VhrId FROM VehicleRequest  wITH (NOLOCK)
				WHERE VhrId = @VhrId)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN003', @VhrId, 'VehicleRequest',  @oparam1 = @sError OUTPUT
		SELECT 'GEN003/' + ISNULL(@sError, '')
		GOTO DestroyCursor
		RETURN
	END

	IF NOT EXISTS (SELECT LocCode FROM Location  wITH (NOLOCK)
					WHERE LocCode = @choBranch
					-- KX Changes :RKS
					AND LocComCode = @sComCode)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN003', @choBranch, 'Check-Out Branch',  @oparam1 = @sError OUTPUT
		SELECT 'GEN003/' + ISNULL(@sError, '')
		GOTO DestroyCursor
		RETURN
	END

	IF NOT EXISTS (SELECT LocCode FROM Location  wITH (NOLOCK)
					WHERE LocCode = @chiBranch
					-- KX Changes :RKS
					AND LocComCode = @sComCode)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN003', @chiBranch, 'Check-In Branch',  @oparam1 = @sError OUTPUT
		SELECT 'GEN003/' + ISNULL(@sError, '')
		GOTO DestroyCursor
		RETURN
	END

	IF @prdId <> '' AND NOT EXISTS (SELECT PrdShortName FROM Product  wITH (NOLOCK)
										WHERE PrdId = @prdId)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN003', @prdId, 'ProductId',  @oparam1 = @sError OUTPUT
		SELECT 'GEN003/' + ISNULL(@sError, '')
		GOTO DestroyCursor
		RETURN
	END

	SELECT @minDays = UviValue
		FROM 	UniversalInfo  wITH (NOLOCK)
		WHERE	UviKey = LTRIM(RTRIM(dbo.getCountryForLocation(@choBranch, NULL, ''))) + 'MINCHECKDAYS'

	print 'before : ' + convert(varchar(24),@choDate)
	IF ISNUMERIC(@minDays) = 1
	BEGIN
		SET @dMinChkDate = CAST(CONVERT(VARCHAR(10), (CURRENT_TIMESTAMP - CAST(@minDays AS INT)), 103) AS DATETIME)
		IF CAST(@choDate AS DATETIME) < CAST(CONVERT(VARCHAr(10), @dMinChkDate, 103) AS DATETIME)
		BEGIN
			SET @sMinChkDate = CONVERT(VARCHAR(24), @dMinChkDate, 105)
			EXECUTE	GEN_getErrorString	'GEN090', 'Check-Out Date', 'greater than or equal', @sMinChkDate, @oparam1 = @sError OUTPUT
			SELECT	'GEN090/' + @sError
			GOTO DestroyCursor
			RETURN
		END
	END -- IF ISNUMERIC(@minDays) = 1
	print 'after'

	IF CAST(@choDate AS DATETIME) > CAST(@chiDate AS DATETIME)
	BEGIN
		EXECUTE sp_get_Errorstring 'GEN090', 'CheckIn Date', 'greater than or equal to', 'CheckOutDate',  @oparam1 = @sError OUTPUT
		SELECT 'GEN090/' + ISNULL(@sError, '')
		GOTO DestroyCursor
		RETURN
	END

	IF @prdId = ''
		SET @prdId = NULL
	SET @sError = NULL
	EXECUTE
		GEN_countriesMatchForLocations
				@choBranch,
				@chiBranch,
				NULL,
				@ReturnError	= @sError OUTPUT
	IF (@sError IS NOT NULL)
	BEGIN
		SELECT	@sError
		GOTO DestroyCursor
	END

	SELECT 	@NewVhrId = VhrId
		FROM VehicleRequest  WITH (NOLOCK)
		WHERE	VhrCkoLocCode 	= @choBranch
		AND		CONVERT(VARCHAR(10), VhrCkoDate, 103)	= @choDate
		AND		VhrCkoAmPm		= @choAmPm
		AND		CONVERT(VARCHAR(10), VhrCkiDate, 103)	= @chiDate
		AND		VhrCkiAmPm		= @chiAmPm
		AND		VhrCkiLocCode	= @chiBranch
		AND		VhrPrdId 		= @prdId
		AND		VhrBkrId 		= @BkrId

	IF (@NewVhrId = '')
	BEGIN
		SET @NewVhrId = NEWID()
		print @NewVhrId
		INSERT INTO VehicleRequest   WITH (ROWLOCK) (
			VhrId,			VhrPrdId,			VhrBkrId,			VhrAgnRef,
			VhrCkoLocCode,	VhrCkoDate,			VhrCkoAmPm,			VhrCkiDate,
			VhrCkiAmPm,		VhrCkiLocCode,		VhrHirePeriodUom,	VhrHirePeriod,
			VhrNumOfAdults,	VhrNumOfChildren,	VhrNumOfInfants,	VhrPkgId,
			VhrBrdCode,		VhrClaId,			VhrTypId,			VhrNumOfVehicles,
			VhrRequestType,	VhrCodSourceMocId,	VhrBlockingResult,	IntegrityNo,
			AddUsrId,		AddDateTime,		AddPrgmName
			)
		SELECT
			@NewVhrId,		@prdId,				VhrBkrId,			VhrAgnRef,
			@choBranch,		@choDate,			@choAmPm,			@chiDate,
			@chiAmPm,		@chiBranch,			VhrHirePeriodUom,	@hirPrd,
			VhrNumOfAdults,	VhrNumOfChildren,	VhrNumOfInfants,	VhrPkgId,
			VhrBrdCode,		VhrClaId,			VhrTypId,			VhrNumOfVehicles,
			VhrRequestType,	VhrCodSourceMocId,	VhrBlockingResult,	IntegrityNo,
			AddUsrId,		GETDATE(),			"AddPrgmName"
		FROM VehicleRequest  WITH (NOLOCK)
		WHERE VhrId = @VhrId
	END
EXEC sp_get_ErrorString  'GEN045', @oparam1 = @sError OUTPUT
SELECT 'GEN045/' + ISNULL(@sError, '') + '/' + @NewVhrId
GOTO DestroyCursor

DestroyCursor:




GO
