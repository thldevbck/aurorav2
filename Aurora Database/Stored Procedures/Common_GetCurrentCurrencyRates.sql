CREATE PROCEDURE [dbo].[Common_GetCurrentCurrencyRates]
AS

-- EXEC [Common_GetCurrentCurrencyRates]

-- Get all the current rates and codes
SELECT
	BaseCode.CodCode AS BaseCodCode,
	BaseCode.CodDesc AS BaseCodDesc,
	TransCode.CodCode AS TransCodCode,
	TransCode.CodDesc AS TransCodDesc,
	RateEntry.curr_rate_date AS RateDate,
	CASE
		WHEN RateEntry.rate_input_fmt = 'D' THEN rate_of_exchange
		ELSE 1.0 / rate_of_exchange
	END AS RateOfExchange
INTO
	#CurrentRates
FROM
	(
		SELECT RateEntry.base_curr_code, RateEntry.trans_curr_code, MAX (RateEntry.curr_rate_date) AS curr_rate_date
		FROM RateEntry
		GROUP BY RateEntry.base_curr_code, RateEntry.trans_curr_code
	) AS CurrentRateEntry
	INNER JOIN RateEntry
		ON CurrentRateEntry.base_curr_code = RateEntry.base_curr_code
		AND CurrentRateEntry.trans_curr_code = RateEntry.trans_curr_code
		AND CurrentRateEntry.curr_rate_date = RateEntry.curr_rate_date
	INNER JOIN Code AS BaseCode
		ON RateEntry.base_curr_code = BaseCode.CodCode
		AND BaseCode.CodCdtNum = 23
	INNER JOIN Code AS TransCode
		ON RateEntry.trans_curr_code = TransCode.CodCode
		AND TransCode.CodCdtNum = 23

-- Return all the current rates and codes, and automatically make any reverse rates if it doesnt
-- already exist
SELECT
	*
FROM
	(
		SELECT
			RateEntry.BaseCodCode,
			RateEntry.BaseCodDesc,
			RateEntry.TransCodCode,
			RateEntry.TransCodDesc,
			RateEntry.RateDate,
			RateEntry.RateOfExchange
		FROM
			#CurrentRates AS RateEntry
		UNION
		SELECT
			RateEntryReverse.TransCodCode AS BaseCodCode,
			RateEntryReverse.TransCodDesc AS BaseCodDesc,
			RateEntryReverse.BaseCodCode AS TransCodCode,
			RateEntryReverse.BaseCodDesc AS TransCodDesc,
			RateEntryReverse.RateDate,
			1.0 / RateEntryReverse.RateOfExchange AS RateOfExchange
		FROM
			#CurrentRates AS RateEntryReverse
			LEFT JOIN #CurrentRates AS RateEntry
				ON RateEntryReverse.BaseCodCode = RateEntry.TransCodCode
				AND RateEntryReverse.TransCodCode = RateEntry.BaseCodCode
		WHERE
			RateEntry.BaseCodCode IS NULL
	) AS RateEntry
ORDER BY
	RateEntry.BaseCodDesc,
	RateEntry.TransCodDesc


-- Drop the temporary table
DROP TABLE #CurrentRates



GO
