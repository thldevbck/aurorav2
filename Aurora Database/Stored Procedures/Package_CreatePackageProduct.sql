CREATE PROCEDURE [dbo].[Package_CreatePackageProduct]
	@PplPkgId	varchar(64),
	@PplSapId	varchar(64),
	@PplCopyCreateLink varchar(12) = 'Link',
	@UsrId varchar(64),
	@PrgmName varchar(64)
AS

IF @PplCopyCreateLink = 'Link' EXEC sp_insertLinkedPackageSap @PplPkgId, @PplSapId, @UsrId, @PrgmName, @PplCopyCreateLink
IF @PplCopyCreateLink = 'Create' EXEC sp_createProductForPackage @PplSapId, @PplPkgId, @UsrId, @PrgmName
IF @PplCopyCreateLink = 'Copy' EXEC sp_copySaleableProduct @PplSapId, 1, @PplPkgId, @UsrId, @PrgmName

/*
DECLARE @PplPkgId	varchar(64)
DECLARE @PplSapId	varchar(64)
DECLARE @UsrId varchar(64)
DECLARE @PrgmName varchar(64)
DECLARE @PplCopyCreateLink varchar(12)
SET @PplPkgId = 'C564777D-5DA3-4CF7-823B-9C1FC08BB1AC'
SET @PplSapId = '2CE8CC3BB44B4ACF922BA2A8C81F7228'
SET @UsrId = 'wv1'
SET @PrgmName = 'test'
SET @PplCopyCreateLink = 'Link'
*/

-- Create new FlexLevels for PkgId/SapId/EffectiveDate combinations if its a Flex Package
INSERT INTO FlexLevel
(
	FlxId,
	FlxNum,
	FlxPkgId,
	FlxSapId,
	FlxEffDate,
	FlxTravelYrStart,
	FlxRate,
	FlxComplete,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT
	NEWID() AS FlxId,
	CalculatedFlexLevel.FlxNum AS FlxNum,
	CalculatedFlexLevel.FlxPkgId AS FlxPkgId,
	@PplSapId AS FlxSapId,
	CalculatedFlexLevel.FlxEffDate AS FlxEffDate,
	CalculatedFlexLevel.FlxTravelYrStart AS FlxTravelYrStart,
	CalculatedFlexLevel.FlxRate AS FlxRate,
	0 AS FlxComplete,
	1 AS IntegrityNo,
	@UsrId AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	@PrgmName AS AddPrgmName
FROM
	-- Get the package product just made
	PackageProduct

	-- Inner join to calculate the current rates for a flex package (the aggregates in the GROUP
    -- BY are a hack, it cuts down on an extra join)
	INNER JOIN
	(
		SELECT
			FlexLevel.FlxPkgId,
			FlexLevel.FlxNum,
			FlexLevel.FlxEffDate,
			MIN (FlexLevel.FlxTravelYrStart) AS FlxTravelYrStart,
			MAX (FlexLevel.FlxRate) AS FlxRate
		FROM
			FlexLevel
			INNER JOIN Package ON FlexLevel.FlxPkgId = Package.PkgId
			INNER JOIN Code ON Package.PkgCodTypId = Code.CodId
		WHERE
			Code.CodDesc = 'Flex'
		GROUP BY
			FlexLevel.FlxPkgId,
			FlexLevel.FlxEffDate,
			FlexLevel.FlxNum
	) AS CalculatedFlexLevel ON PackageProduct.PplPkgId = CalculatedFlexLevel.FlxPkgId

	-- Do a left join to check if it exists already
	LEFT JOIN FlexLevel AS ExistingFlexLevel
		ON ExistingFlexLevel.FlxPkgId = CalculatedFlexLevel.FlxPkgId
		AND ExistingFlexLevel.FlxSapId = PackageProduct.PplSapId
		AND ExistingFlexLevel.FlxNum = CalculatedFlexLevel.FlxNum
		AND ExistingFlexLevel.FlxEffDate = CalculatedFlexLevel.FlxEffDate
WHERE
	PackageProduct.PplPkgId = @PplPkgId
	AND PackageProduct.PplSapId = @PplSapId
	AND PackageProduct.PplCopyCreateLink = @PplCopyCreateLink
	AND ExistingFlexLevel.FlxId IS NULL


-- return the PackageProduct just created
SELECT
	PackageProduct.*
FROM
	PackageProduct
WHERE
	PackageProduct.PplPkgId = @PplPkgId
	AND PackageProduct.PplSapId = @PplSapId
	AND PackageProduct.PplCopyCreateLink = @PplCopyCreateLink

GO
