set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================
-- Author:		JackLeong
-- Create date: 2007-10-02
-- Description:	Aurora Log table
-- =============================================
Create PROCEDURE [dbo].AddCategory
	 @categoryName  nvarchar(100), 
	 @logID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE	applicationLog          
	SET		Category = @categoryName          
	WHERE	LogId = @logID 

END

