USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_FinalizeProductRate]    Script Date: 11/19/2007 13:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_FinalizeProductRate] 
	@FlrId AS int,
	@FleetStatus AS varchar(1)
AS

UPDATE FlexBookingWeekRate
SET 
	FlrFleetStatus = @FleetStatus
FROM
	FlexBookingWeekRate
WHERE
	FlexBookingWeekRate.FlrId = @FlrId

SELECT 
	* 
FROM 
	FlexBookingWeekRate 
WHERE 
	FlrId = @FlrId




