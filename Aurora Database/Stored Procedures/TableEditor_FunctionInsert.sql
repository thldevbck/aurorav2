USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_FunctionInsert]    Script Date: 11/19/2007 13:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableEditor_FunctionInsert]
(
	@tefCode AS varchar(64),
    @tefFunCode  AS varchar(12),
    @tefName AS nvarchar(64),
    @tefDescription AS ntext
)
AS

INSERT INTO TableEditorFunction
(
	tefCode,
	tefFunCode,
    tefName,
	tefDescription
)
VALUES
(
	@tefCode,
    @tefFunCode, 
    @tefName, 
    @tefDescription
)

SELECT 
	TableEditorFunction.* 
FROM 
	TableEditorFunction 
WHERE
	TableEditorFunction.tefId = SCOPE_IDENTITY()	
	
	
