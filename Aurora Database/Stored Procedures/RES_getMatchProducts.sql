set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go











ALTER        PROCEDURE [dbo].[RES_getMatchProducts]
	@BooId				VARCHAR	 (64)	=	'',
	@RntId				VARCHAR	 (64)	=	''
AS

-- Local variables
DECLARE @sBpdList 		VARCHAR(5000),
		@ReturnError	VARCHAR(500),
		@bpid			VARCHAR(64),
		@idoc			INT,
		@disBut			varchar

IF @BooId IS NULL OR @BooId = ''
BEGIN
	SELECT @BooId = RntBooId
		FROM Rental WITH(NOLOCK)
		WHERE	RntId = @RntId
END -- IF @BooId IS NULL OR @BooId = ''

SELECT '<BooId>' + @BooId + '</BooId>'
SELECT '<RntId>' + @RntId + '</RntId>'
SELECT '<A>'
IF EXISTS (SELECT	BptId	
			FROM 	BookedProductTraveller  WITH (NOLOCK) 
			WHERE	BptRntId = @RntId
		)
BEGIN	
	SELECT	''+BpdId											AS	BpdId,
			''+CusId											AS	CusId,
			''+PrdShortName + ' - ' + PrdName					AS	PrdDet,
			''+CusLastName + ISNULL(', ' + CusFirstName, '') 	AS	TrvDet,
			0													AS	chk
	FROM 	BookedProductTraveller MP  WITH (NOLOCK) ,
		Customer  WITH (NOLOCK) , 
		BookedProduct  WITH (NOLOCK) , 
		SaleableProduct  WITH (NOLOCK) ,  
		Product  WITH (NOLOCK) 
	WHERE	BptRntId = @RntId
	AND	CusId = BptCusId
	AND	BpdId = BptBpdId
	AND	SapId = BpdSapId
	AND	PrdId = SapPrdId
	FOR XML AUTO, ELEMENTS
END
ELSE
	SELECT 	'<MP>
			<BpdId></BpdId>
			<CusId></CusId>
			<PrdDet> </PrdDet>
			<TrvDet></TrvDet>
			<chk>0</chk>
		</MP>'
SELECT '</A>'

EXEC RES_getBookedProducts 
		--@sBooId 			= @BooId, 
		@sRntId 			= @RntId, 
		@sExclStatusList 	= 'XN,XX' , 
		@bSelectCurrentOnly = 1, 
		@sBpdList 			= @sBpdList 	OUTPUT, 
		@ReturnError 		= @ReturnError 	OUTPUT

EXEC sp_xml_preparedocument @idoc OUTPUT, @sBpdList
-- get Product Match Records
SELECT '<B>'
IF EXISTS(SELECT BpdId 
			FROM BookedProduct WITH (NOLOCK) , SaleableProduct  WITH (NOLOCK) 
			WHERE BpdId IN (SELECT bpid
								FROM OPENXML(@idoc,'/Data/bpid',3)
								WITH ( bpid		varchar(64) 'text()')
							)
			AND	SapId = BpdSapId
			AND	EXISTS(SELECT PatSapId FROM ProductAttribute  WITH (NOLOCK) 
							WHERE	PatSapId = SapId
							AND	PatAttId = (SELECT AttId FROM Attribute  WITH (NOLOCK) 
												WHERE	AttCode = 'MATCH'))
		)
BEGIN
	SET @disBut = 0
	SELECT 	BpdId				AS	BpdId,
			PrdShortName + ' - ' + PrdName	AS	PrdDet,
			CAST(BpdQty AS INT) - (SELECT COUNT(BptBpdId) 
										FROM BookedProductTraveller  WITH (NOLOCK) 
										WHERE BptBpdId = BpdId)	AS	PrdQty,
			0					AS	chk
	FROM	BookedProduct AS PM  WITH (NOLOCK) , 
		SaleableProduct WITH (NOLOCK) , 
		Product  WITH (NOLOCK) 
	WHERE BpdId IN (SELECT bpid
						FROM OPENXML(@idoc,'/Data/bpid',3)
						WITH ( bpid		varchar(64) 'text()')
					)
	AND	SapId = BpdSapId
	AND	PrdId = SapPrdId
	AND	EXISTS(SELECT PatSapId FROM ProductAttribute  WITH (NOLOCK) 
				WHERE	PatSapId = SapId
				AND	PatAttId = (SELECT AttId FROM Attribute  WITH (NOLOCK) 
									WHERE	AttCode = 'MATCH'))
	FOR XML AUTO, ELEMENTS
END
ELSE
BEGIN
	SET @disBut = 1
	SELECT 	'<PM>
			<BpdId></BpdId>
			<PrdQty></PrdQty>
			<PrdDet></PrdDet>
			<chk>0</chk>
		</PM>'
END
SELECT '</B>'

EXEC sp_xml_removedocument @idoc

-- get Traveller Match Records
SELECT '<C>'
IF EXISTS(SELECT TrvRntId 
		FROM Traveller  WITH (NOLOCK) 
		WHERE TrvRntId = @RntId)
BEGIN
	SELECT 	CusId						AS	CusId,
		CusLastName + ISNULL(', '+CusFirstName, '')	AS	TrvDet,
		0						AS	chk
	FROM	Traveller  WITH (NOLOCK) ,
		Customer TM   WITH (NOLOCK) 
	WHERE 	TrvRntId = @RntId
	AND	CusId = TrvCusId
	FOR XML AUTO, ELEMENTS
END
ELSE
BEGIN
	SELECT 	'<TM>
			<CusId></CusId>
			<TrvDet></TrvDet>
			<chk>0</chk>
		</TM>'
	SET @disBut = 1
END
SELECT '</C>'

SELECT '<Dis>' + @disBut + '</Dis>'











