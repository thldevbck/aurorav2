USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetFunction]    Script Date: 11/19/2007 13:57:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[TableEditor_GetFunction] 
	@Code AS varchar(64)
AS

SELECT 
	TableEditorFunction.*
FROM 
	TableEditorFunction
WHERE
	tefCode = @Code


