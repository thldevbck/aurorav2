CREATE PROCEDURE [dbo].[Admin_GetLocationLookups]
AS

SELECT
	Country.*
FROM
	Country
ORDER BY
	Country.CtyCode


SELECT
	TownCity.*
FROM
	TownCity
	INNER JOIN Country ON TownCity.TctCtyCode = Country.CtyCode
ORDER BY
	Country.CtyCode,
	TownCity.TctCode


SELECT
	Code.*
FROM
	Code
	INNER JOIN CodeType ON CodeType.CdtNum = Code.CodCdtNum
ORDER BY
	CodeType.CdtNum,
	Code.CodCode



GO
