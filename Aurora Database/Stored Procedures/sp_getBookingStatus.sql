set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_getBookingStatus] 

		@BookingStatusId			int	 = null

AS
BEGIN
	SET NOCOUNT ON

	select	
			BookingStatusId,
			BookingStatusCode,
			BookingStatusName 
	from	BookingStatus
	where	(BookingStatusId = isnull(@BookingStatusId, 0) or isnull(@BookingStatusId, 0) = 0)
			and IsActive = 1
	
END
