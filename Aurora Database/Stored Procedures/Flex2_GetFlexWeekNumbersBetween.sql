USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetFlexWeekNumbersBetween]    Script Date: 11/19/2007 13:43:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_GetFlexWeekNumbersBetween] 
	@TravelYearFrom AS datetime,
	@TravelYearTo AS datetime
AS

SELECT 
	*
FROM 
	FlexWeekNumber
WHERE
	FwnTrvYearStart BETWEEN @TravelYearFrom AND @TravelYearTo
ORDER BY
	FwnTrvWkStart