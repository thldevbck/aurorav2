CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckIn_Payment]
	@BookRentalIn		VARCHAR(64)
AS

SET NOCOUNT ON

/*
DECLARE	@BookRentalIn		VARCHAR(64)
SET		@BookRentalIn = '3256642/1'
*/

select Payment, Curr, CashBondAmount, CashBondCurr
from reports.Report_ActDetCKIPayment
where BookRentalIn = @BookRentalIn



GO
