USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_DeleteProductException]    Script Date: 11/19/2007 13:42:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Flex2_DeleteProductException] 
	@FleId AS int
AS

DELETE FROM  
	FlexBookingWeekException
WHERE
	FleId = @FleId


