USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetTableNames]    Script Date: 11/19/2007 13:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TableEditor_GetTableNames]

AS

	SELECT 
		CAST (sysobjects.id AS varchar(20)) AS Value,
		sysobjects.name AS Text
	FROM 
		sysobjects
	WHERE 
		sysobjects.xtype = 'U' 
	ORDER BY 
		sysobjects.name