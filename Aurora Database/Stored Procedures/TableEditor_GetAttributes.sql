USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetAttributes]    Script Date: 11/19/2007 13:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[TableEditor_GetAttributes] 
	@Code AS varchar(64)
AS

SELECT 
	TableEditorAttribute.*
FROM 
	TableEditorAttribute
	INNER JOIN TableEditorEntity ON TableEditorAttribute.teeId = TableEditorEntity.teeId
	INNER JOIN TableEditorFunction ON TableEditorEntity.tefId = TableEditorFunction.tefId
WHERE
	TableEditorFunction.tefCode = @Code
ORDER BY
	TableEditorAttribute.teaOrder, 
	TableEditorAttribute.teaId,
	TableEditorEntity.teeOrder, 
	TableEditorEntity.teeId 


