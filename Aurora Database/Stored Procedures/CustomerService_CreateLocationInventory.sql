CREATE PROCEDURE [dbo].[CustomerService_CreateLocationInventory]
	@ComCode AS varchar(64),
	@CtyCode AS varchar(64),
	@LocCode AS varchar(64),
	@UsrId AS varchar(64),
	@PrgrmName AS varchar(64)

AS


DECLARE @LivId AS varchar(64)
SET @LivId = NEWID()


INSERT INTO LocationInventory
(
	LivLocCode,
	LivId,
	LivDateReconciled,
	LivStkChkDate,
	LivOtherItems,
	LivStatus,
	LivCurrentStocktakeCompleted,
	StcStkCountQty,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT
	Location.LocCode AS LivLocCode,
	@LivId AS LivId,
	NULL AS LivDateReconciled,
	GETDATE() AS LivStkChkDate,
	'' AS LivOtherItems,
	'In Progress' AS LivStatus,
	NULL AS LivCurrentStocktakeCompleted,
	0.0 AS StcStkCountQty,
	1 AS IntegrityNo,
	@UsrId AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	@PrgrmName AS AddPrgmName
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
WHERE
	Location.LocComCode = @ComCode
	AND Location.LocCode = @LocCode
	AND TownCity.TctCtyCode = @CtyCode
	AND NOT EXISTS
	(
		SELECT 1
		FROM
			LocationInventory
		WHERE
			LocationInventory.LivLocCode = @LocCode
			AND LocationInventory.LivStatus = 'In Progress'
	)


SELECT
	LocationInventory.*
FROM
	LocationInventory
WHERE
	LocationInventory.LivId = @LivId



GO
