CREATE PROCEDURE [dbo].[CustomerService_GetStockCheckLookups]
	@ComCode AS varchar(64),
	@CtyCode AS varchar(64)
AS

-- EXEC [CustomerService_GetStockCheckLookups] 'THL', 'NZ'

SELECT
	Location.*
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
WHERE
	Location.LocComCode = @ComCode
	AND TownCity.TctCtyCode = @CtyCode


SELECT
	LocationInventory.LivId,
	LocationInventory.LivLocCode,
	LocationInventory.LivStatus,
	LocationInventory.LivStkChkDate,
	LocationInventory.IntegrityNo
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
	INNER JOIN LocationInventory ON Location.LocCode = LocationInventory.LivLocCode
WHERE
	Location.LocComCode = @ComCode
	AND TownCity.TctCtyCode = @CtyCode
ORDER BY
	LocationInventory.LivLocCode,
	LocationInventory.LivStkChkDate DESC


SELECT
	UniversalInfo.*
FROM
	UniversalInfo
WHERE
	UniversalInfo.UviKey = @CtyCode + '_StockTake'

GO
