set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_insertNoteSpec] 

		@UsrCode		VARCHAR	 (64)	=	'',
		@CtyCode VARCHAR	 (12)	=	'' ,
		@NtsType	VARCHAR	 (64)	=	'' ,
		@NtsIsHeader bit,
		@NtsText VARCHAR	 (5000)	=	'' ,
		@NtsOrder int,
		@bIsCus bit, 
		@bIsAgn bit,
		@NtsIsActive bit,
		@PrgmName	VARCHAR	 (64)	=	''

AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@NtsId			varchar	(64),
				@UsrId				VARCHAR	 (64) ,
				@ComCode			VARCHAR(64)

	SELECT	@ComCode = Company.ComCode 
	FROM	dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON  UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @UsrCode
				
	SELECT	@UsrId = UsrId
	 FROM	UserInfo WITH (NOLOCK)
	 WHERE	UsrCode = @UsrCode
	

	set @NtsId = NEWID()

	IF LTRIM(RTRIM(@NtsText)) = ''
		SET @NtsIsActive = 0
	
	insert zzlog (zzlogtext) values (convert(varchar(5),len(@NtsText)))

	INSERT INTO NoteSpec  WITH (ROWLOCK) (
				NtsId ,
				NtsType ,
				NtsCtyCode ,
				NtsIsHeader ,
				NtsText ,
				IntegrityNo ,			
				NtsOrder ,
				NtsIsDefCus ,
				NtsIsDefAgn ,
				NtsComCode,
				AddUsrId ,
				NtsIsActive ,
				AddDateTime ,
				AddPrgmName
			)
	VALUES
			(
				@NtsId ,
				@NtsType ,
				@CtyCode ,
				@NtsIsHeader ,
				@NtsText ,
				1 ,
				@NtsOrder ,
				@bIsCus , 
				@bIsAgn ,
				@ComCode,
				@UsrId ,
				@NtsIsActive ,
				CURRENT_TIMESTAMP ,
				@PrgmName
			)

	IF @@ERROR <> 0 
		-- Return message to the calling program to indicate failure.
		select N'An error occurred while inserting the NoteSpec record in database.'
	ELSE
		select ''

END


