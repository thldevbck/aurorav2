CREATE PROCEDURE [dbo].[OpsAndLogs_GetFleetStatus]
	@ctyCode AS varchar(2),
	@brdCode AS varchar(1),
	@travelYear	AS datetime
AS

-- EXEC OpsAndLogs_GetFleetStatus 'NZ', 'B', '2008-01-04'

-----------------------------------------------------------
-- 0 - FlexWeekNumber
-----------------------------------------------------------
SELECT
	FlexWeekNumber.*
FROM
	FlexWeekNumber
WHERE
	FlexWeekNumber.FwnTrvYearStart = @travelYear

-----------------------------------------------------------
-- 1 - Product
-----------------------------------------------------------
SELECT
	Product.*,
	CAST ((CASE
		WHEN
			Product.PrdIsVehicle = 1
			AND Product.PrdIsActive = 1
			AND ISNULL (Product.PrdToAppearonFS, 0) = 1
			AND EXISTS
			(
				SELECT 1
				FROM
					SaleableProduct
					INNER JOIN PackageProduct ON SaleableProduct.SapId = PackageProduct.PplSapId
					INNER JOIN Package ON PackageProduct.PplPkgId = Package.PkgId
				WHERE
					SaleableProduct.SapPrdId = Product.PrdId
					AND SaleableProduct.SapCtyCode = @ctyCode
					AND Package.PkgTravelToDate >= @travelYear
					AND Package.PkgTravelFromDate <  DATEADD (year, 1, @travelYear)
					AND Package.PkgIsActive = 'ACTIVE'
			)
		THEN 1
		ELSE 0
	END) AS bit) AS IsActive


FROM
	Product
WHERE
	Product.PrdBrdCode = @BrdCode
	AND Product.PrdIsVehicle = 1
	--AND Product.PrdIsActive = 1
	AND ISNULL (Product.PrdToAppearonFS, 0) = 1
	AND EXISTS
	(
		SELECT 1
		FROM
			SaleableProduct
			INNER JOIN PackageProduct ON SaleableProduct.SapId = PackageProduct.PplSapId
			INNER JOIN Package ON PackageProduct.PplPkgId = Package.PkgId
		WHERE
			SaleableProduct.SapPrdId = Product.PrdId
			AND SaleableProduct.SapCtyCode = @ctyCode
			AND Package.PkgTravelToDate >= @travelYear
			AND Package.PkgTravelFromDate <  DATEADD (year, 1, @travelYear)
			--AND Package.PkgIsActive = 'ACTIVE'
	)
ORDER BY
	Product.PrdShortName

-----------------------------------------------------------
-- 2 - FleetStatus
-----------------------------------------------------------
SELECT
	FleetStatus.*
FROM
	Product
	INNER JOIN FleetStatus
		ON Product.PrdShortName = FleetStatus.FstShortName
		AND Product.PrdBrdCode = FleetStatus.FstBrdCode
WHERE
	Product.PrdBrdCode = @BrdCode
	AND Product.PrdIsVehicle = 1
	--AND Product.PrdIsActive = 1
	AND ISNULL (Product.PrdToAppearonFS, 0) = 1
	AND EXISTS
	(
		SELECT 1
		FROM
			SaleableProduct
			INNER JOIN PackageProduct ON SaleableProduct.SapId = PackageProduct.PplSapId
			INNER JOIN Package ON PackageProduct.PplPkgId = Package.PkgId
		WHERE
			SaleableProduct.SapPrdId = Product.PrdId
			AND SaleableProduct.SapCtyCode = @ctyCode
			AND Package.PkgTravelToDate >= @travelYear
			--AND Package.PkgIsActive = 'ACTIVE'
	)
	AND FleetStatus.FstCtyCode = @ctyCode
	AND FleetStatus.FstTravelYear = CAST (DATEPART (yyyy, @travelYear) AS varchar(4))
ORDER BY
	Product.PrdShortName




GO
