CREATE	PROCEDURE [dbo].[Report_R&MReport]
	@sRMDate	VARCHAR(10)	=	'',
	@sStatus	VARCHAR(2)	=	'',
	@sLocCode	VARCHAR(12)	=	''
AS

/*
DECLARE @sRMDate VARCHAR(10)
DECLARE @sStatus VARCHAR(2)
DECLARE @sLocCode VARCHAR(12)

SET @sRMDate = '11/06/2008'
SET @sStatus = 'GE'
SET @sLocCode = 'AKL'
*/
SET NOCOUNT ON

DECLARE	@sFleetCompanyId	VARCHAR(64)
SELECT @sFleetCompanyId = FleetCompanyId FROM dbo.Company (NOLOCK)
INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = user

IF @sRMDate = ''
	SET @sRMDate = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103)

IF @sStatus = 'LE' AND @sRMDate <> ''
	SELECT 	'' + FA.UnitNumber AS 'Unit #',
			''+RegistrationNumber AS 'Rego #',
			''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'Fleet Model',
			''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'Start Date/Time',
			''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'Reason',
			''+ISNULL(StartOdometer, '')  AS 'Start Odo',
			''+ISNULL(EndOdometer, '')  AS 'End Odo',
			''+ISNULL(OtherRepairer, '')  AS 'Other Repairs',
			''+ISNULL(ServiceProvider, '') AS 'Service Provider',
			''+ISNULL(ScheduledService, '') AS 'Schedule Service'
	FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
	WHERE 	FR.FleetAssetId = FA.FleetAssetId
	AND		FM.FleetModelId = FA.FleetModelId
	AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) <= CAST(@sRMDate AS datetime)
	AND		Completed = 0
	AND		FA.LastLocation = FL.LocationId
	AND		(@sLocCode = '' OR FL.LocationCode = @sLocCode)

ELSE IF @sStatus = 'GE'  AND @sRMDate <> ''
	SELECT 	'' + FA.UnitNumber AS 'Unit #',
			''+RegistrationNumber AS 'Rego #',
			''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'Fleet Model',
			''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'Start Date/Time',
			''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'Reason',
			''+ISNULL(StartOdometer, '')  AS 'Start Odo',
			''+ISNULL(EndOdometer, '')  AS 'End Odo',
			''+ISNULL(OtherRepairer, '')  AS 'Other Repairs',
			''+ISNULL(ServiceProvider, '') AS 'Service Provider',
			''+ISNULL(ScheduledService, '') AS 'Schedule Service'
	FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
	WHERE 	FR.FleetAssetId = FA.FleetAssetId
	AND		FM.FleetModelId = FA.FleetModelId
	AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) >= CAST(@sRMDate AS datetime)
	AND		Completed = 0
	AND		FA.LastLocation = FL.LocationId
	AND		(@sLocCode = '' OR FL.LocationCode = @sLocCode)

ELSE IF  @sRMDate <> ''
	SELECT 	'' + FA.UnitNumber AS 'Unit #',
			''+RegistrationNumber AS 'Rego #',
			''+LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' + CONVERT(VARCHAR(15), FM.FleetModelDescription)  AS 'Fleet Model',
			''+CONVERT(VARCHAR(10), FR.StartDateTime, 103) + '-' + CONVERT(VARCHAR(5), FR.StartDateTime, 108)  AS 'Start Date/Time',
			''+ISNULL(CONVERT(VARCHAR(25), ReasonDescription), '') AS 'Reason',
			''+ISNULL(StartOdometer, '')  AS 'Start Odo',
			''+ISNULL(EndOdometer, '')  AS 'End Odo',
			''+ISNULL(OtherRepairer, '')  AS 'Other Repairs',
			''+ISNULL(ServiceProvider, '') AS 'Service Provider',
			''+ISNULL(ScheduledService, '') AS 'Schedule Service'
	FROM 	aimsprod.dbo.FleetRepairs FR WITH(NOLOCK), aimsprod.dbo.FleetModel FM WITH(NOLOCK), aimsprod.dbo.FleetAsset FA WITH(NOLOCK), AIMSPROD.dbo.FleetLocations FL WITH(NOLOCK)
	WHERE 	FR.FleetAssetId = FA.FleetAssetId
	AND		FM.FleetModelId = FA.FleetModelId
	AND		CAST(CONVERT(varchar(10), FR.EndDateTime, 103) AS datetime) = CAST(@sRMDate AS datetime)
	AND		Completed = 0
	AND		FA.LastLocation = FL.LocationId
	AND		(@sLocCode = '' OR FL.LocationCode = @sLocCode)



GO
