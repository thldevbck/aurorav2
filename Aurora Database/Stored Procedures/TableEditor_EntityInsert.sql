USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_EntityInsert]    Script Date: 11/19/2007 13:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableEditor_EntityInsert]
(
	@tefId AS int,
	@teeParentId AS int,
	@teeName AS  nvarchar(64),
	@teeDescription AS  ntext,
	@teeTableName AS nvarchar(64),
	@teeSelectSql AS ntext,
	@teeMustFilter AS bit,
	@teeUpdateable AS bit,
	@teeUpdateSql AS ntext,
	@teeUpdateableFieldName AS nvarchar(64),
	@teeInsertable AS bit,
	@teeInsertSql AS ntext,
	@teeInsertableFieldName AS nvarchar(64),
	@teeDeleteable AS bit,
	@teeDeleteSql AS ntext,
	@teeDeleteableFieldName AS nvarchar(64),
	@teeDefaultSql AS ntext,
	@teeOrder AS int
)
AS

INSERT INTO TableEditorEntity
(
	tefId,
	teeParentId,
	teeName,
	teeDescription,
	teeTableName,
	teeSelectSql,
	teeMustFilter,
	teeUpdateable,
	teeUpdateSql,
	teeUpdateableFieldName,
	teeInsertable,
	teeInsertSql,
	teeInsertableFieldName,
	teeDeleteable,
	teeDeleteSql,
	teeDeleteableFieldName,
	teeDefaultSql,
	teeOrder
)
VALUES
(
	@tefId,
	@teeParentId,
	@teeName,
	@teeDescription,
	@teeTableName,
	@teeSelectSql,
	@teeMustFilter,
	@teeUpdateable,
	@teeUpdateSql,
	@teeUpdateableFieldName,
	@teeInsertable,
	@teeInsertSql,
	@teeInsertableFieldName,
	@teeDeleteable,
	@teeDeleteSql,
	@teeDeleteableFieldName,
	@teeDefaultSql,
	@teeOrder
)

SELECT 
	TableEditorEntity.* 
FROM 
	TableEditorEntity 
WHERE
	TableEditorEntity.teeId = SCOPE_IDENTITY()
	

