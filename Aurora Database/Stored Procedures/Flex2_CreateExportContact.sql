USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_CreateExportContact]    Script Date: 11/19/2007 13:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_CreateExportContact] 
	@FlxId int,
	@AgnId varchar(64) = NULL,
	@UsrId varchar(64) = NULL

AS

/* dont allow both agent and usrid to be set */
IF @AgnId IS NOT NULL AND @UsrId IS NOT NULL RETURN

INSERT INTO FlexExportContact
(
	FxcFlxId,
	FxcAgnId,
	FxcUsrId
)
SELECT
	@FlxId,
	Agent.AgnId,
	UserInfo.UsrId
FROM
	( SELECT 1 AS Root ) AS Root
	LEFT JOIN Agent ON Agent.AgnId = @AgnId
	LEFT JOIN UserInfo ON UserInfo.UsrId = @UsrId
	LEFT JOIN FlexExportContact ON FlexExportContact.FxcFlxId = @FlxId
		AND (FlexExportContact.FxcAgnId = @AgnId OR FlexExportContact.FxcUsrId = @UsrId)
WHERE
	(Agent.AgnId IS NOT NULL OR UserInfo.UsrId IS NOT NULL)
	AND FlexExportContact.FxcId IS NULL

SELECT 
	* 
FROM
	FlexExportContact
WHERE
	FxcFlxId = @FlxId
	AND (FxcAgnId = @AgnId OR FxcUsrId = @UsrId)
