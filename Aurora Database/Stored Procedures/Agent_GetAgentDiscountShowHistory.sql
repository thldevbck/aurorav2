USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Agent_GetAgentDiscountShowHistory]    Script Date: 06/06/2008 17:09:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Agent_GetAgentDiscountShowHistory] 
(
	@ComCode AS varchar(64),
	@AgnId AS varchar(64)
)
AS

/*
DECLARE @AgnId AS varchar(64)
DECLARE @ComCode AS varchar(64)
SET @AgnId = '001B97D3-93D8-49F6-A0C1-8B85FBD5149C' 
SET @ComCode = 'THL'
*/

DECLARE @Agent table 
(
	AgnId varchar(64)
)
INSERT INTO @Agent 
(
	AgnId
)
SELECT 
	Rtrim(Agent.AgnId) AS AgnId
FROM 
	Agent
WHERE
	Agent.AgnId = @AgnId
ORDER BY
	Agent.AgnCode

/*********************************************************************************************/
/* 0 - AgentDiscount */
/*********************************************************************************************/
SELECT     
	AdsID, AdsAgnId, AdsPrdId, AdsChgAgent, AdsBrdCode, 
	ISNULL(CONVERT(VARCHAR(10), AdsEffDate, 103), '') AS AdsEffDate, 
	AgentDiscount.IntegrityNo, AgentDiscount.AddUsrId,       
	ISNULL(AdsIsBookedDateType, '0') as AdsIsBookedDateType,
	AdsCtyCode, AdsBookedFromWhen, AdsTravelFromWhen, AgentDiscount.ModUsrId, 
	ISNULL(AdsPercentage, 0) AS AdsPercentage, 
	AgentDiscount.AddDateTime, AgentDiscount.ModDateTime, AgentDiscount.AddPrgmName
FROM         
	@Agent AS a
	INNER JOIN AgentDiscount ON AgentDiscount.AdsAgnId = a.AgnId
	INNER JOIN Brand ON Brand.BrdCode = AgentDiscount.AdsBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
	LEFT OUTER JOIN Product ON AgentDiscount.AdsPrdId = Product.PrdId
WHERE     
	(Product.PrdIsActive = 1)
	AND	Company.ComCode = @ComCode


