CREATE PROCEDURE [dbo].[OPL_VehicleUndoAssign]
     @BookingNo VARCHAR(64) = '',
     @RentalNo VARCHAR(64) = '',
	 @UserCode VARCHAR(64)
AS

BEGIN
	SET NOCOUNT ON
    -- already null check
	DECLARE @l_RenAllowVehSpec AS VARCHAR(64)

    SELECT @l_RenAllowVehSpec = RenAllowVehSpec
	FROM Rental WITH (NOLOCK)
	WHERE RntId IN (SELECT TOP 1 RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId
			WHERE  BooNum = @BookingNo
			AND  RntNum = @RentalNo)

	IF @l_RenAllowVehSpec IS NULL
	BEGIN
		SELECT 'ERROR/No forced vehicle assignment exists for this rental'
		RETURN
	END


	 UPDATE Rental WITH (ROWLOCK)
	 SET RenAllowVehSpec =   NULL  ,
		 ModUsrId  = @UserCode ,
		 ModDateTime = CURRENT_TIMESTAMP

	 WHERE RntId IN (SELECT TOP 1 RntId FROM Rental WITH (NOLOCK) LEFT OUTER JOIN Booking WITH (NOLOCK) ON BooId = RntBooId
			WHERE  BooNum = @BookingNo
			AND  RntNum = @RentalNo)
	 IF (@@ERROR <> 0)
	 BEGIN
	  SELECT 'ERROR/Error in updating rental (' + @BookingNo + '/' + @RentalNo + ')'
	  RETURN
	 END
	 SELECT 'SUCCESS/' + dbo.getErrorString('GEN046', '(' + @BookingNo + '/' + @RentalNo + ')', '', '', '', '', '')
	 RETURN

END
GO
