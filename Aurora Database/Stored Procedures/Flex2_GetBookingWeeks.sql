USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetBookingWeeks]    Script Date: 11/19/2007 13:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Flex2_GetBookingWeeks] 
	@ComCode varchar(64)            
AS

SELECT 
	*
FROM 
	FlexBookingWeek
WHERE
	FbwComCode = @ComCode
ORDER BY
	FbwBookStart DESC


