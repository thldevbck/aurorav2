CREATE PROCEDURE [dbo].[Admin_GetUser]
	@UsrId AS varchar(64),
	@UsrCode AS varchar(24),
	@UsrName AS varchar(64),
	@UsrComCode AS varchar(64),
	@UsrCtyCode AS varchar(12),
	@UsrLocCode AS varchar(12),
	@UsrIsActive AS bit,
	@UsrIsAgentUser AS bit,
	@ExactUsrCode AS varchar(24),
	@ExactUsrEmail AS varchar(64)
AS

/*
 EXEC Admin_GetUser NULL, 'wv1', NULL, NULL, NULL, NULL, NULL
*/

SELECT
	UserInfo.*
INTO
	#userSearch
FROM
	UserInfo
	LEFT JOIN UserCompany ON UserCompany.UsrId = UserInfo.UsrId
WHERE
	(@UsrId IS NULL OR UserInfo.UsrId = @UsrId)
	AND (@UsrCode IS NULL OR UserInfo.UsrCode LIKE (@UsrCode + '%'))
	AND (@UsrName IS NULL OR UserInfo.UsrName LIKE (@UsrName + '%'))
	AND (@UsrComCode IS NULL OR UserCompany.ComCode = @UsrComCode)
	AND (@UsrCtyCode IS NULL OR UserInfo.UsrCtyCode = @UsrCtyCode)
	AND (@UsrLocCode IS NULL OR UserInfo.UsrLocCode = @UsrLocCode)
	AND (@UsrIsActive IS NULL OR UserInfo.UsrIsActive = @UsrIsActive)
	AND (@UsrIsAgentUser IS NULL OR ISNULL (UserInfo.UsrIsAgentUser, 0) = @UsrIsAgentUser)
	AND (@ExactUsrCode IS NULL OR UserInfo.UsrCode = @ExactUsrCode)
	AND (@ExactUsrEmail IS NULL OR UserInfo.UsrEmail = @ExactUsrEmail)


SELECT
	#userSearch.*
FROM
	#userSearch
ORDER BY
	#userSearch.UsrCode


SELECT
	UserCompany.*
FROM
	#userSearch
	INNER JOIN UserCompany ON UserCompany.UsrId = #userSearch.UsrId
ORDER BY
	#userSearch.UsrCode,
	UserCompany.ComCode


SELECT
	FlexExportContact.*
FROM
	#userSearch
	INNER JOIN FlexExportContact ON FlexExportContact.FxcUsrId = #userSearch.UsrId
ORDER BY
	#userSearch.UsrCode,
	FlexExportContact.FxcId


SELECT
	UserRole.*
FROM
	#userSearch
	INNER JOIN UserRole ON UserRole.UsrId = #userSearch.UsrId
ORDER BY
	#userSearch.UsrCode,
	UserRole.RolId


SELECT
	AgentUserInfo.*
FROM
	#userSearch
	INNER JOIN AgentUserInfo ON AgentUserInfo.AuiUsrId = #userSearch.UsrId
	INNER JOIN Agent ON AgentUserInfo.AuiAgnId = Agent.AgnId
ORDER BY
	Agent.AgnCode


SELECT
	DISTINCT Agent.*
FROM
	#userSearch
	LEFT JOIN AgentUserInfo ON AgentUserInfo.AuiUsrId = #userSearch.UsrId
	INNER JOIN Agent ON AgentUserInfo.AuiAgnId = Agent.AgnId OR #userSearch.UsrAgnDefaultId = Agent.AgnId
ORDER BY
	Agent.AgnCode


DROP TABLE #userSearch



GO
