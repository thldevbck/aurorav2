    
    
    
  ALTER PROCEDURE [dbo].[RES_manageCancelRental]    
 @sBooId     VARCHAR  (64) = '',    
 @sRntId     VARCHAR  (64) = '' ,    
 @sRntCancelWhen   VARCHAR  (20) = '',    
 @sRntCodCancelReasonId VARCHAR  (64) = '' ,    
 @sRntCodCancelMocId  VARCHAR  (64) = '' ,    
 @sRntCodCancelTypeId VARCHAR  (64) = '' ,    
 @bNoteReqd    BIT    = 0,    
 @sUsrCode    VARCHAR  (64) = '' ,    
 @sProgramName   VARCHAR  (256) = ''     
     
AS    
     
 SET NOCOUNT ON    
 DECLARE @IntNo    INT ,    
   @ErrStr    VARCHAR  (2000) ,    
   @RntStatus   VARCHAR  (64) ,    
   @sCkoStatus   VARCHAR  (64) ,    
   @sBpdIdList   VARCHAR  (2000) ,    
   @Err    VARCHAR  (200)  ,    
   @doc    INT ,    
   @bPdId    VARCHAR  (64) ,    
   @RetErrStr   VARCHAR  (500),    
   @RetMsgStr   VARCHAR (500),    
   @RnhId    VARCHAR  (64),    
   @bAlreadyCancelled bit,    
   @sCtyCode   varchar(3),    
   @sUviValue   varchar(24),    
   @sPrdId    varchar(64),    
   @sSapList   varchar(5000),    
   @bResult   bit,    
   @sBpdList   varchar(5000),    
   @ReturnError  varchar(500),    
   @start    int,    
   @count    int,    
   @ReturnSapId  varchar(64),    
   @psReturnCodCurrId varchar(64),    
   @pbReturnPkgPrd  varchar(64),    
   @pbReturnIsBase  bit,    
   @psReturnSapStatus varchar(24),    
   @bCancelFeeReqd  BIT,    
   @UsrId    varchar(64),    
   @noCount    int,    
   @noBpd    int,    
   @bUpdateFleet  BIT,    
   @sBlrIdListReturned VARCHAR(2000),    
   @bCallBookMsg  BIT,    
   @bpdIdListss  VARCHAR(2000),    
   @CurrBpd   varchar(64),    
   @BpdIsCusCharge  BIT,    
   @BooId    varchar(64),    
   @dCkoWhen   datetime ,    
   @RntFollowUpWhen varchar  (10),    
   -- KX Changes :RKS    
   @sRntPkgId   VARCHAR(64),    
   @sComCode   VARCHAR(64),    
   @sAgnId    VARCHAR(64),    
   @pdAdjBookedWhen DATETIME    
    
 SET @pdAdjBookedWhen = CURRENT_TIMESTAMP    
     
 SELECT @sAgnId = BooAgnId FROM dbo.Booking WHERE BooId = @sBooId    
 select @RntFollowUpWhen = convert(varchar(10),RntFollowUpWhen,103),    
   -- KX Changes :RKS    
   @sRntPkgId = RntPkgId    
    from rental (nolock) where RntId = @sRntId    
    
 SELECT @sComCode = dbo.fun_getCompany    
       (    
        NULL,  --@sUserCode VARCHAR(64),    
        NULL,  --@sLocCode VARCHAR(64),    
        NULL,  --@sBrdCode VARCHAR(64),    
        NULL,  --@sPrdId VARCHAR(64),    
        @sRntPkgId, --@sPkgId VARCHAR(64),    
        NULL,  --@sdummy1 VARCHAR(64),    
        NULL,  --@sdummy2 VARCHAR(64),    
        NULL  --@sdummy3 VARCHAR(64)    
       )    
 SET @bCancelFeeReqd = 0    
 SELECT @UsrId = UsrId     
  FROM UserInfo WITH(NOLOCK)    
  WHERE  UsrCode = @sUsrCode    
  OR  UsrId = @sUsrCode    
    
 SELECT  @RntStatus  = RntStatus,    
   @BooId   = RntBooId,    
   @dCkoWhen  = RntCkoWhen,    
   @sCtyCode  = LTRIM(RTRIM(dbo.getCountryForLocation(RntCkoLocCode, NULL, '')))    
  FROM Rental WITH(NOLOCK)    
  WHERE RntID = @sRntId    
    
 UPDATE Rental WITH (ROWLOCK)    
  SET RntCancellationWhen  = @sRntCancelWhen    
 WHERE RntId =  @sRntId    
    
 --  1.IF Rental not previously cancelled, find all BookedProducts on rental and cancel them    
 IF @RntStatus IN ('XX','XN')    
 BEGIN    
  SET @bAlreadyCancelled = 1    
 END    
 ELSE    
 BEGIN    
  SET @bAlreadyCancelled = 0    
    
  -- If a confirmed rental, a CANCEL product may be required to capture a Cancellation fee    
  IF @RntStatus = 'KK'    
  BEGIN    
   -- Get country code for country of travel    
   SET @sUviValue = dbo.getUviValue(@sCtyCode + 'CANCFEEDIRECTAGTONLY')    
       
   -- Get Cancel fee required for direct agent only, see if this rental has a direct agent assigned    
   IF ISNULL(@sUviValue, '') = 'Y'    
   BEGIN    
    IF EXISTS(SELECT AgnId FROM Booking WITH(NOLOCK)     
         LEFT OUTER JOIN Agent WITH(NOLOCK) ON BooAgnId = AgnId    
         LEFT OUTER JOIN AgentType WITH(NOLOCK) ON AtyId = AgnAtyId    
       WHERE BooId = @BooId    
       AND  ISNULL(AtyIsDirect, 0) = 1)    
     SET @bCancelFeeReqd = 1    
   END -- IF ISNULL(@sUviValue, '') = 'Y'    
   ELSE    
    -- Other waise cancellation fee required for all rentals that have 'KK' status    
    SET @bCancelFeeReqd = 1    
  END    
    
  SET @sCkoStatus = dbo.RES_getCheckOutStatus(@sRntId)    
  /*EXEC RES_get BookedProducts     
    @sBooId    = @sBooId,     
    @sRntId    = @sRntId,     
    @bSelectCurrentOnly = 1,     
    @sExclStatusList  = 'XN,XX' ,      
    @ReturnCSV   = 2,    
    @sBpdList    = @sBpdIdList OUTPUT,     
    @ReturnError   = @Err OUTPUT    
  */    
  SELECT    
    @sBpdIdList  = ISNULL(@sBpdIdList + ',','') + BpdId        
  FROM  BookedProduct (NOLOCK)    
  WHERE  BpdRntId  =  @sRntId    
    AND BpdIsCurr  = 1    
    AND BpdStatus  NOT IN('XX','XN')    
    AND NOT exists(select typid from dbo.saleableproduct (nolock), product (nolock), type (nolock)    
           where sapprdid = prdid    
           AND  Prdtypid = typid    
           AND  TypCode  = 'CCSRGE'    
           AND  BpdSapid = Sapid)    
       
  
-- Added by Shoel for Enhancement #28  
-- Part 6 moved up here cos it works when its here :)  
  
  
-- Explaination :   
-- step 1: get the vehicle charges as follows ...  
 DECLARE @l_VehicleCharge AS MONEY  
		,@l_IsCusCharge AS BIT
 SELECT   
  @l_VehicleCharge = CASE   
        WHEN BpdIsCusCharge = 0 THEN SUM(BpdGrossAmt-BpdAgnDisAmt)   
        ELSE  SUM(BpdGrossAmt)   
         END  
,@l_IsCusCharge = BpdIsCusCharge
 FROM BookedProduct WITH (NOLOCK)  
 WHERE BpdRntId = @sRntId AND BpdPrdIsVehicle = 1 AND BpdStatus  = 'KK'  
 GROUP BY BpdIsCusCharge  
  
  
-- step 2: run the old part 6, with the rate as obtained above as an additional param ...  
-- 6. AddBooking cancellation fee if required and a first-time cancel    
 IF  @bCancelFeeReqd = 1 AND @bAlreadyCancelled = 0    
 BEGIN    
  SET @sUviValue = dbo.getUviValue(@sCtyCode + 'CANCPROD')    
    
  SELECT @sPrdId = PrdId    
   FROM Product    
   WHERE PrdShortName = @sUviValue    
  SET @sSapList = ''    
  IF ISNULL(@sPrdId, '') <> ''    
  BEGIN    
   -- Find saleble product id    
   EXECUTE RES_getSaleableProductId      
    @psPkgID   = @sRntPkgId,    
    @psCtyCode   = @sCtyCode, -- VARCHAR(3),    
    @psPrdID   = @sPrdId, -- VARCHAR(64),    
    @psAgnId   = @sAgnId, --NULL,    
    @pdAdjBookedWhen = @pdAdjBookedWhen,    
    @ReturnSapId  = @ReturnSapId OUTPUT,    -- VARCHAR(64) OUTPUT ,    
    @psReturnCodCurrId = @psReturnCodCurrId OUTPUT,  -- VARCHAR(64) OUTPUT ,    
    @pbReturnPkgPrd  = @pbReturnPkgPrd OUTPUT,   -- BIT  OUTPUT ,    
    @pbReturnIsBase  = @pbReturnIsBase OUTPUT,  -- BIT  OUTPUT ,    
    @psReturnSapStatus = @psReturnSapStatus OUTPUT ,  -- VARCHAR(12) OUTPUT     
    @psError   = @ReturnError OUTPUT    
       
   SET @sSapList = @ReturnSapId     
    
   IF ISNULL(@sSapList, '') <> ''    
   BEGIN    
    SET @ReturnError = NULL    
    SET @sBpdList = NULL    
    SET @RetMsgStr = NULL    
    
    -- To delete records from TempTable created by managecalculate rates     
    EXECUTE RES_DeleteTProcessedBpds    
      @sRntId = @sRntId -- VARCHAR(64)=NULL     
  
    EXECUTE RES_CreateBookedProductNonRequest    
     @psRntId   = @sRntId,    
     @psCkoStatus  = NULL,    
     @psSapId   = @sSapList,    
     @psAgnID   = NULL,     
     @psStatus   = NULL,    
     @psCkoLocCode  = NULL,    
     @psCkiLocCode  = NULL,    
     @pdBookedWhen  = NULL,    
     @pbIsFoc   = NULL,    
     @pbApplyOrigRates = NULL,    
     @psUserId   = @UsrId,    
     @psProgramName  = 'RES_ManageCancelRental',    
-- Added by Shoel for Enhancement #28  
  @cRate = @l_VehicleCharge,  
  @bIsCusCharge = @l_IsCusCharge,
-- Added by Shoel for Enhancement #28  
     @sError    = @ReturnError  OUTPUT,    
     @psBpdIds   = @sBpdList  OUTPUT,    
     @psMessage   = @RetMsgStr OUTPUT    
    
    IF ISNULL(@ReturnError, '') <> ''    
    BEGIN    
     SELECT @ReturnError    
     RETURN    
    END -- IF ISNULL(@ReturnError, '') <> ''    
    IF ISNULL(@sBpdList, '') <> ''    
    BEGIN    
     SET @sBpdList = dbo.getSplitedData(@sBpdList, ',', 0)    
     UPDATE BookedProduct WITH(ROWLOCK)    
      SET BpdStatus = 'KK',    
       moddatetime = getdate()        
     WHERE BpdId = @sBpdList    
    END -- IF ISNULL(@sBpdList, '') <> ''    
   END -- IF ISNULL(@sSapIdList, '') <> ''    
  END -- IF ISNULL(@sPrdId, '') <> ''    
 END -- IF  @bCancelFeeReqd = 1 AND @bAlreadyCancelled = 0    
    
  
-- Part 6 moved up here cos it works when its here :)  
-- Added by Shoel for Enhancement #28  
      
  IF ISNULL(@Err, '') = ''    
  BEGIN    
   SET @start = 1    
   SET @count =  CASE    
        WHEN ISNULL(@sBpdIdList, '') = '' THEN 0 ELSE dbo.getEntry(@sBpdIdList, ',') + 1    
       END    
   WHILE @start <= @count    
   BEGIN    
    SET @bPdId = dbo.getSplitedData(@sBpdIdList, ',', @start)    
    SET @start = @start + 1    
    
    SET @RetErrStr = NULL    
    SET @RetMsgStr = NULL    
    EXEC RES_CancelBookedProduct     
     @psBpdId     = @bpdId,    
     @pbIsPartOfModifyProcess = 1,    
     @psCkoStatus    = @sCkoStatus,    
     @psParentBpdID     = @bpdId ,    -- RKS 16/01    
     @psUsrId     = @sUsrCode,    
     @psAddPrgmName    = @sProgramName,      
     @psReturnError    = @RetErrStr OUTPUT,    
     @psReturnMessage   = @RetMsgStr OUTPUT    
        
    IF @RetErrStr IS NOT NULL    
    BEGIN    
     SELECT @RetErrStr    
     GOTO DestroyCursor    
    END        
   END -- WHILE @start <= @count    
  END -- IF ISNULL(@Err, '') = ''    
 END -- IF @RntStatus IN ('XX','XN')    
     
    
 -- 2. Update rental record to record new cancellation details Update rental record    
 UPDATE Rental WITH (ROWLOCK)    
  SET    
   RntCancellationWhen  = @sRntCancelWhen ,    
   RntCodCancelReasonId = @sRntCodCancelReasonId ,    
   RntCodCancelMocId  = @sRntCodCancelMocId ,    
   RntCodCancTypId   = @sRntCodCancelTypeId ,    
   RntStatus    =  CASE    
           WHEN RntStatus IN ('IN','QN','KB','WL','NN') THEN 'XN'    
           WHEN RntStatus IN ('KK') THEN 'XX'    
           ELSE RntStatus    
          END,    
   RntXfrdToFinanwhen  = null,    
   IntegrityNo    = IntegrityNo + 1 ,       
   ModUsrId    = @UsrId,    
   ModDateTime    = CURRENT_TIMESTAMP    
 WHERE RntId =  @sRntId    
     
    
 -- 4. ALTER  Rental History record    
 IF @bAlreadyCancelled = 0    
 BEGIN    
  EXECUTE RES_checkRentalHistory     
    @sBooId   = @sBooId ,     
    @sRntId   = @sRntId ,      
    @sChangeEvent  = 'Cancel',     
    @sUserCode   = @sUsrCode,     
    @sAddPrgmName  = @sProgramName    
 END -- IF bAlreadyCancelled = 0    
    
 -- 5.. ALTER  a rental note    
 IF @bNoteReqd = 1    
 BEGIN    
  INSERT INTO Note WITH (ROWLOCK)    
   (    
   NteId  ,    
   NteBooID ,    
   NteRntId ,    
   NteCodTypId ,    
   NteDesc  ,    
   NteCodAudTypId ,    
   NtePriority ,    
   NteIsActive ,    
   IntegrityNo ,    
   AddUsrId ,    
   AddDateTime ,    
   AddPrgmName ,    
   -- KX Changes :RKS    
   NteComCode    
   )    
  VALUES    
   (    
    NEWID(),    
    @sBooId,    
    @sRntId,    
    dbo.GetCodeId(13,'Rental'),    
    'The provisional rental booking was cancelled as the follow-up date ' + @RntFollowUpWhen + ' has passed',    
    dbo.GetCodeId(26,'Internal'),    
    3,    
    1,    
    1,    
    @UsrId,    
    CURRENT_TIMESTAMP,    
    @sProgramName,    
    -- KX Changes :RKS    
    @sComCode    
   )     
 END    
    
-- -- Part 6 moved up , cos it works when its here :)  
-- -- 6. AddBooking cancellation fee if required and a first-time cancel    
-- IF  @bCancelFeeReqd = 1 AND @bAlreadyCancelled = 0    
-- BEGIN    
--  SET @sUviValue = dbo.getUviValue(@sCtyCode + 'CANCPROD')    
--    
--  SELECT @sPrdId = PrdId    
--   FROM Product    
--   WHERE PrdShortName = @sUviValue    
--  SET @sSapList = ''    
--  IF ISNULL(@sPrdId, '') <> ''    
--  BEGIN    
--   -- Find saleble product id    
--   EXECUTE RES_getSaleableProductId      
--    @psPkgID   = @sRntPkgId,    
--    @psCtyCode   = @sCtyCode, -- VARCHAR(3),    
--    @psPrdID   = @sPrdId, -- VARCHAR(64),    
--    @psAgnId   = @sAgnId, --NULL,    
--    @pdAdjBookedWhen = @pdAdjBookedWhen,    
--    @ReturnSapId  = @ReturnSapId OUTPUT,    -- VARCHAR(64) OUTPUT ,    
--    @psReturnCodCurrId = @psReturnCodCurrId OUTPUT,  -- VARCHAR(64) OUTPUT ,    
--    @pbReturnPkgPrd  = @pbReturnPkgPrd OUTPUT,   -- BIT  OUTPUT ,    
--    @pbReturnIsBase  = @pbReturnIsBase OUTPUT,  -- BIT  OUTPUT ,    
--    @psReturnSapStatus = @psReturnSapStatus OUTPUT ,  -- VARCHAR(12) OUTPUT     
--    @psError   = @ReturnError OUTPUT    
--       
--   SET @sSapList = @ReturnSapId     
--    
--   IF ISNULL(@sSapList, '') <> ''    
--   BEGIN    
--    SET @ReturnError = NULL    
--    SET @sBpdList = NULL    
--    SET @RetMsgStr = NULL    
--    
--    -- To delete records from TempTable created by managecalculate rates     
--    EXECUTE RES_DeleteTProcessedBpds    
--      @sRntId = @sRntId -- VARCHAR(64)=NULL     
--  
--    EXECUTE RES_CreateBookedProductNonRequest    
--     @psRntId   = @sRntId,    
--     @psCkoStatus  = NULL,    
--     @psSapId   = @sSapList,    
--     @psAgnID   = NULL,     
--     @psStatus   = NULL,    
--     @psCkoLocCode  = NULL,    
--     @psCkiLocCode  = NULL,    
--     @pdBookedWhen  = NULL,    
--     @pbIsFoc   = NULL,    
--     @pbApplyOrigRates = NULL,    
--     @psUserId   = @UsrId,    
--     @psProgramName  = 'RES_ManageCancelRental',    
---- Added by Shoel for Enhancement #28  
--  @cRate = @l_VehicleCharge,  
---- Added by Shoel for Enhancement #28  
--     @sError    = @ReturnError  OUTPUT,    
--     @psBpdIds   = @sBpdList  OUTPUT,    
--     @psMessage   = @RetMsgStr OUTPUT    
--    
--    IF ISNULL(@ReturnError, '') <> ''    
--    BEGIN    
--     SELECT @ReturnError    
--     RETURN    
--    END -- IF ISNULL(@ReturnError, '') <> ''    
--    IF ISNULL(@sBpdList, '') <> ''    
--    BEGIN    
--     SET @sBpdList = dbo.getSplitedData(@sBpdList, ',', 0)    
--     UPDATE BookedProduct WITH(ROWLOCK)    
--      SET BpdStatus = 'KK',    
--       moddatetime = getdate()        
--     WHERE BpdId = @sBpdList    
--    END -- IF ISNULL(@sBpdList, '') <> ''    
--   END -- IF ISNULL(@sSapIdList, '') <> ''    
--  END -- IF ISNULL(@sPrdId, '') <> ''    
-- END -- IF  @bCancelFeeReqd = 1 AND @bAlreadyCancelled = 0    
-- -- Part 6 moved up , cos it works when its here :)  
    
 -- 7. Recalculate Booked Product Charges for MultiHire bookings    
 IF @bAlreadyCancelled = 0    
 BEGIN     
  EXECUTE RES_isMultiHire    
   @BookingID   = @sBooId,    
   @SelVehicleID  = NULL,    
   @ExcludeRntID  = @sRntId,    
   @ReturnFlag  = @bResult OUTPUT    
  IF @bResult = 1    
  BEGIN    
   SET @ReturnError = NULL    
   SET @sBpdList = NULL    
    
   SELECT    
     @sBpdList   =  ISNULL(@sBpdList + ',','') + BpdId    
   FROM  BookedProduct (NOLOCK)    
   WHERE  BpdRntId   =  @sRntId    
     AND BpdStatus  NOT IN('XX','XN')    
     AND BpdIsCurr  = 1    
     
    
   IF ISNULL(@ReturnError, '') = '' AND ISNULL(@sBpdList, '') <> ''    
   BEGIN    
    SET @noCount = 1    
    SET @noBpd = dbo.getEntry(@sBpdList, ',') + 1    
    WHILE @noCount <= @noBpd    
    BEGIN    
     SET @CurrBpd = dbo.getSplitedData(@sBpdList, ',', @noCount)    
     SELECT @BpdIsCusCharge = BpdIsCusCharge    
      FROM  BookedProduct WITH(NOLOCK)    
      WHERE BpdId = @CurrBpd    
    
     -- To delete records from TempTable created by managecalculate rates     
     EXECUTE RES_DeleteTProcessedBpds    
       @sRntId = @sRntId -- VARCHAR(64)=NULL     
    
     EXECUTE Res_ManageBookedProductMod    
      @sBpdId   = @CurrBpd,       -- VARCHAR(64),    
      @bIsCusCharge = @BpdIsCusCharge,     -- BIT,    
      @sAgnId   = NULL,        -- VARCHAR(64),    
      @sUserId  = @UsrId,       -- VARCHAR(64) = NULL,    
      @sProgramName   = @sProgramName,     -- VARCHAR(256) = null,    
      @ReturnError = @ErrStr OUTPUT,     -- VARCHAR(2000) output,    
      @ReturnMessage = @RetMsgStr OUTPUT,     -- VARCHAR(2000) OUTPUT,    
      @bUpdateFleet = @bUpdateFleet OUTPUT,    -- bit     OUTPUT,    
      @sBlrIdListReturned = @sBlrIdListReturned OUTPUT, -- VARCHAR(2000) OUTPUT,    
      @bCallManageBlockingMessageReturned = @bCallBookMsg OUTPUT, -- bit output,           
      @bpdIdListss = @bpdIdListss OUTPUT    -- VARCHAR(2000) output      
       
     SET @noCount = @noCount + 1    
    END -- WHILE @noCount < @noBpd    
   END -- IF ISNULL(@ReturnError, '') = '' AND ISNULL(@sBpdList, '') <> ''    
  END -- IF @bResult = 1    
 END -- IF bAlreadyCancelled = 0     
DestroyCursor:      
RETURN 