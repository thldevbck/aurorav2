CREATE          PROCEDURE [dbo].[actReport_getMovementTypeSummary]
	@sCtyCode		varchar(64),
	@sLocCode		varchar(64),
	@sFromDate		varchar(12),
	@sToDate		varchar(12),
	@sMovement		varchar(20),
	@sBrdCode		varchar(10),
	@sClaId			varchar(64),
	@sTypId			varchar(64),
	@sPrdId			varchar(64),
	@sUserCode		varchar(20),
	@sIsTypeReport	bit,
	@sPrintPage		bit

AS
SET NOCOUNT ON

DECLARE 	@sCountry	varchar(64)
DECLARE 	@sBranch	varchar(64)
DECLARE 	@sBrdName	varchar(64)
DECLARE		@sClassCode	varchar(12)
DECLARE 	@sTypCode	varchar(64)
DECLARE 	@sPrdName	varchar(30)
DECLARE		@sRow		int
DECLARE 	@sCodIdEXT  varchar(64)

DECLARE 	@sBpdIdCurrent varchar(64)
DECLARE 	@sBpdSapid		varchar(64)

DECLARE @AU_THRIFTYPRDS VARCHAR(1000)
DECLARE @AU_THRIFTYEFFDT datetime
DECLARE @dRntCkoWhen     datetime
declare @dRntCkiWhen     datetime

SELECT @AU_THRIFTYPRDS = UviValue From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYPRDS'
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From Dbo.UniversalInfo (nolock) WHERE UviKey = @sCtyCode + '_THRIFTYEFFDT'


SET	@sCodIdEXT  = dbo.getCodeId(4,'EXT')

SET	@sRow = 0

DECLARE @tCko TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 varchar(124),
	a6 varchar(54),
	a7 varchar(64)
)

DECLARE @tCki TABLE (
	a1 varchar(64),
	a2 varchar(64),
	a3 varchar(124),
	a4 varchar(124),
	a5 varchar(124),
	a6 varchar(64),
	a7 varchar(64)
)

DECLARE	@sRentalId		VARCHAR(64)
DECLARE	@sRentalIdIn	VARCHAR(64)
declare @sCodidXTO		varchar(64)

SET @sCodidXto = dbo.getCodeid(4,'XTO')

SELECT @sCountry = CtyName  FROM	dbo.Country  WITH (NOLOCK) WHERE CtyCode = @sCtyCode

SELECT	'<Head><country>' + LTRIM(RTRIM(@sCountry)) + '</country>'

IF @sLocCode =''
	SET @sBranch = 'All'
ELSE
	SELECT 	@sBranch = LocName FROM	dbo.location  WITH (NOLOCK) WHERE	LocCode = @sLocCode

SELECT 	'<branch>' + LTRIM(RTRIM(@sBranch)) + '</branch><period>' + @sFromDate + ' to ' + @sToDate + '</period>'

IF @sMovement = ''
	SELECT 	'<movement>All</movement>'
ELSE
	SELECT	'<movement>' + LTRIM(RTRIM(@sMovement)) + '</movement>'

IF @sBrdCode = ''
			SET @sBrdName = 'All'
ELSE
	SELECT	@sBrdName = BrdName FROM dbo.Brand  WITH (NOLOCK) WHERE	BrdCode = @sBrdCode

SELECT	'<brand>' + LTRIM(RTRIM(@sBrdName)) + '</brand>'

IF @sClaId = ''
	SET @sClassCode = 'All'
ELSE
	SELECT	@sClassCode = ClaCode FROM	dbo.Class  WITH (NOLOCK) WHERE	ClaId = @sClaId


SELECT	'<class>' + LTRIM(RTRIM(@sClassCode)) + '</class>'

IF @sTypId = ''
	SET @sTypCode = 'All'
ELSE
	SELECT	@sTypCode = TypCode FROM	dbo.Type WITH (NOLOCK) WHERE 	TypId = @sTypId


SELECT	'<type>' + @sTypCode + '</type>'

IF @sPrdId = ''
	SET @sPrdName = 'All'
ELSE
	SELECT	@sPrdName = PrdShortName FROM dbo.Product  WITH (NOLOCK) WHERE	PrdId = @sPrdId

SELECT	'<product>' + @sPrdName + '</product>'
SELECT	'<IsTypeReport>' + CONVERT(char,@sIsTypeReport) + '</IsTypeReport>'
SELECT	'<TypeReportRow></TypeReportRow>'

IF @sPrintPage=1
	SELECT	'<IsPrintForBranch>True</IsPrintForBranch>'
ELSE
	SELECT	'<IsPrintForBranch>False</IsPrintForBranch>'


SELECT	'<user>User: ' + ISNULL(UsrName,'') + '</user>' FROM 	dbo.UserInfo WITH (NOLOCK)  WHERE	UsrCode = @sUserCode
SELECT 	'<printTime>Printed ' +  substring(CONVERT(VARCHAR,CURRENT_TIMESTAMP,113),0,12) + substring(CONVERT(VARCHAR,CURRENT_TIMESTAMP,109),13,5) + ' ' + substring(CONVERT(VARCHAR,CURRENT_TIMESTAMP,109),25,2)  +  '</printTime>'
SELECT	'</Head>'

DECLARE @StartDate	DATETIME,
		@EndDate	DATETIME,
		@WkDate		DATETIME

SET @StartDate	= CONVERT(DATETIME, @sFromDate,103)
SET @EndDate	= CONVERT(DATETIME,(@sToDate + ' 23:59:59') ,103)
SET @WkDATE		= CONVERT(DATETIME,@sFromDate,103)

SELECT	'<tableData>'
SELECT 	'<Classes>'

IF @sClaId <> ''
BEGIN
	SELECT	ClaId,
			ClaCode,
			ClaDesc
	 FROM	Class  WITH (NOLOCK)
	 WHERE	ClaIsActive = 1
	 AND	ClaIsVehicle = 1
	 AND	ClaId = @sClaId
	FOR XML AUTO, ELEMENTS
END
ELSE IF @sClaId=''
BEGIN
	IF @sPrdId <>''
	BEGIN
		SELECT	ClaId,
				ClaCode,
				ClaDesc
		 FROM   Class  WITH (NOLOCK) INNER JOIN Type  WITH (NOLOCK) ON Class.ClaID = Type.TypClaId INNER JOIN Product  WITH (NOLOCK) ON Type.TypId = Product.PrdTypId
		 WHERE	Product.PrdId = @sPrdId
		FOR XML AUTO, ELEMENTS
	END

	ELSE
	BEGIN
		IF @sTypId <>''
		BEGIN
			SELECT	ClaId,
					ClaCode,
					ClaDesc
			 FROM	Class  WITH (NOLOCK) INNER JOIN Type  WITH (NOLOCK) ON Class.ClaID = Type.TypClaId
			 WHERE	Type.TypId = @sTypId
			FOR XML AUTO, ELEMENTS
		END
		ELSE
		BEGIN
			SELECT	ClaId,
					ClaCode,
					ClaDesc
			 FROM	Class WITH (NOLOCK)
			 WHERE	ClaIsActive = 1
			 AND	ClaIsVehicle = 1
			FOR XML AUTO, ELEMENTS
		END
	END
END

SELECT 	'</Classes><Types>'

IF @sTypId<>''
BEGIN
	SELECT  Type.TypCode 	AS TypeCode,
			Class.ClaCode	AS ClassCode,
			Class.ClaDesc	AS ClassDesc,
			@sRow			AS Row
	 INTO	#TmpType1
	 FROM  	Type  WITH (NOLOCK) INNER JOIN Class  WITH (NOLOCK) ON Type.TypClaId = Class.ClaID
	 WHERE 	(Class.ClaIsVehicle = 1)
	 AND 	(Class.ClaIsActive = 1)
	 AND	(Type.TypId =@sTypId)

	SELECT * FROM #TmpType1 AS Type
	FOR XML AUTO, ELEMENTS
	DROP TABLE #TmpType1
END
ELSE
BEGIN
	IF @sPrdId<>''
	BEGIN
		DECLARE @sSelectTypeId varchar(64)
		SELECT 	@sSelectTypeId = Type.TypId
		 FROM	Type  WITH (NOLOCK) INNER JOIN  Product  WITH (NOLOCK) ON Type.TypId = Product.PrdTypId
		 WHERE	Product.PrdId = @sPrdId

		SELECT 	Type.TypCode 	AS TypeCode,
				Class.ClaCode	AS ClassCode,
				Class.ClaDesc	AS ClassDesc,
				@sRow			AS Row
		 INTO	#TmpType2
		 FROM   Type  WITH (NOLOCK) INNER JOIN Class  WITH (NOLOCK) ON Type.TypClaId = Class.ClaID
		 WHERE 	(Class.ClaIsVehicle = 1) AND
				(Class.ClaIsActive = 1) AND
				(Type.TypId =@sSelectTypeId)
		SELECT * FROM #TmpType2 AS Type
		FOR XML AUTO, ELEMENTS
		DROP TABLE #TmpType2
	END
	ELSE
	BEGIN
		SELECT  Type.TypCode 	AS TypeCode,
				Class.ClaCode	AS ClassCode,
				Class.ClaDesc	AS ClassDesc,
				@sRow		AS Row
		 INTO	#TmpType3
		 FROM  	Type  WITH (NOLOCK) INNER JOIN Class  WITH (NOLOCK) ON Type.TypClaId = Class.ClaID
		 WHERE 	(Class.ClaIsVehicle = 1) AND
				(Class.ClaIsActive = 1) AND
				(Type.TypClaId = @sClaId or @sClaId='')
		SELECT * FROM #TmpType3 AS Type
		FOR XML AUTO, ELEMENTS
		DROP TABLE #TmpType3
	END
END

SELECT 	'</Types>'

SELECT	'<TableDate>'

WHILE @WkDate <= @EndDate
BEGIN
	SELECT	'<Dates><Date>' + CONVERT(VARCHAR,@WKDate,106) + '</Date>'

	SELECT 	'<Day>' + DATENAME(dw,@WKDate) + '</Day></Dates>'

	SET @WKDate = @WKDate +1
END

SELECT	'</TableDate>'
SELECT 	'<locations>'

IF @sLocCode=''
BEGIN

SELECT 	DISTINCT UPPER(Rental.RntCkoLocCode) 		AS  LocCode
 FROM	Rental  WITH (NOLOCK) INNER JOIN
		Location  WITH (NOLOCK) ON Rental.RntCkoLocCode = Location.LocCode  INNER JOIN
      	TownCity  WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
WHERE  	TownCity.TctCtyCode = @sCtyCode

/*
-- KX fix AJ 18 June 2008 (won't work for user 'sa')
SELECT DISTINCT UPPER(dbo.Rental.RntCkoLocCode) AS LocCode
FROM         dbo.Rental WITH (NOLOCK) INNER JOIN
                      dbo.Location WITH (NOLOCK) ON dbo.Rental.RntCkoLocCode = dbo.Location.LocCode INNER JOIN
                      dbo.TownCity WITH (NOLOCK) ON dbo.Location.LocTctCode = dbo.TownCity.TctCode INNER JOIN
                      dbo.Package ON dbo.Rental.RntPkgId = dbo.Package.PkgId
WHERE     (dbo.TownCity.TctCtyCode = @sCtyCode) AND (dbo.Package.PkgBrdCode IN
                          (SELECT     dbo.Brand.BrdCode
                            FROM          dbo.UserInfo INNER JOIN
                                                   dbo.UserCompany ON dbo.UserInfo.UsrId = dbo.UserCompany.UsrId INNER JOIN
                                                   dbo.Brand ON dbo.UserCompany.ComCode = dbo.Brand.BrdComCode
                            WHERE      (dbo.UserInfo.UsrCode = USER)))
*/
FOR XML AUTO,ELEMENTS
END
ELSE
BEGIN
	SELECT 	'<Rental><LocCode>' + LTRIM(RTRIM(@sLocCode)) + '</LocCode></Rental>'
END
SELECT	'</locations>'

SELECT	'<Checkouts>'


------------------------------------
/*for test*/
DECLARE @sBoonum varchar(64)
declare @sRntNum varchar(64)
SET @sBoonum =''
SET @sRntNum = ''
-----------------------------------


IF @sMovement<>'Check In'
BEGIN


DECLARE @ssSapId VARCHAR(64),
		@extValue VARCHAR(64)

SET @ssSapId = NULL
SET @extValue = NULL



IF @sLocCode  <> ''
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   Rental.RntCkoLocCode = @sLocCode
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate
ELSE
	DECLARE Cur_out CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental with (nolock)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkoLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkoWhen BETWEEN @StartDate AND @EndDate

/*GROUP BY Rental.RntCkoLocCode,
		Rental.RntCkiLocCode,
		Rental.RntCkoWhen,
		Rental.RntCkiWhen,
		Rental.RntId,
		RntNum,
		BooNum
ORDER BY Rental.RntCkoLocCode, Rental.RntCkiLocCode, CONVERT(CHAR(10),Rental.RntCkoWhen,103), Rental.RntCkiWhen,Rental.RntId
*/
OPEN Cur_out

FETCH NEXT FROM Cur_out INTO @sRentalId

WHILE @@FETCH_STATUS=0
BEGIN
		SET @sBpdIdCurrent = null
		SET @sBpdSapid = null
		SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
					 @sBpdSapId		 = BpdSapid ,
					 @dRntCkoWhen    = RntCkoWhen
			FROM dbo.Bookedproduct with (nolock), rental (nolock)
			WHERE Bpdrntid = @sRentalId
			and   bpdrntid = rntid
			AND Bpdprdisvehicle = 1
			and Bpdiscurr = 1
			and BpdStatus = 'KK'
			ORder BY BpdCkiWhen

		--CLASS
		IF @sClassCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock), dbo.type with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		Typid = PrdTypId
							AND		@sClaId = typclaid)
				GOTO GetNextRentalCKO
		END

		-- Type
		IF @sTypCode <> 'ALL'
		BEGIN
			IF NOT EXISTS(select Sapid from dbo.Saleableproduct with (nolock), dbo.product with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		PrdTypid = @sTypId)
				GOTO GetNextRentalCKO
		END

		--Brand
		IF @sBrdCode <> ''
		BEGIN
			IF NOT EXISTS(select rntid from dbo.rental with (nolock), dbo.package with (nolock)
							where 	rntid = @sRentalId
							AND		rntpkgid = Pkgid
							AND		PkgBrdCode = @sBrdCode)
				GOTO GetNextRentalCKO
		END

		-- Product
		IF @sPrdName <> 'ALL'
		BEGIN
			IF NOT EXISTS(select sapid from dbo.Saleableproduct with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	SapPrdId = @sPrdId)

				GOTO GetNextRentalCKO
		END

		-- Thrifty Check
		IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
		BEGIN
			IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
										dbo.saleableproduct (nolock),
										dbo.product (nolock),
										dbo.type (nolock),
										dbo.class (nolock)
						WHERE bpdid = @sBpdIdCurrent
						and bpdsapid = sapid
						and sapprdid = prdid
						and prdtypid = typid
						and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0 AND  @dRntCkoWhen >= @AU_THRIFTYEFFDT
				GOTO GetNextRentalCKO
		END

		INSERT INTO @tCko
		SELECT  UPPER(Rental.RntCkoLocCode),
				CONVERT(VARCHAR, Rntckowhen, 106),
				ISNULL(Class.ClaCode, ''),
				Type.TypCode,
				1,
				boonum,
				Rntnum
		FROM	Product  WITH (NOLOCK) ,
               	Type  WITH (NOLOCK) ,
               	Class  WITH (NOLOCK),
               	SaleableProduct  WITH (NOLOCK) ,
               	BookedProduct  WITH (NOLOCK) ,
              	Rental  WITH (NOLOCK) ,
				booking with (nolock)
		WHERE   bpdid = @sBpdIdCurrent
		AND		bpdrntid = Rntid
		AND		rntbooid = Booid
		AND		BpdSapid = Sapid
		AND		SapPrdid = Prdid
		ANd		prdtypid = Typid
		And		TypClaId = Claid

		GetNextRentalCKO:
		FETCH NEXT FROM Cur_out INTO @sRentalId
	END

SELECT	LTRIM(RTRIM(a1))	AS  RntCkoLocCode,
		a2	AS	RntDate,
		a3	AS	Class,
		a4  AS	Code,
		a5	AS	ClCount,
		a6  AS  BooNum,
		a7  AS  RntNum
FROM @tCko AS Checkout
FOR XML AUTO, ELEMENTS


END
close Cur_out
deallocate cur_out


SELECT 	'</Checkouts>'

CKISEC:

SELECT
		'<Checkins>'

IF @sMovement<>'Check Out'
BEGIN


IF @sLocCode  <> ''
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   (Rental.RntCkiLocCode = @sLocCode)
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate

ELSE
	DECLARE Cur_in CURSOR FAST_FORWARD LOCAL FOR
	SELECT  Rental.RntId
	FROM    Rental (NOLOCK)
	WHERE   exists (SELECT LocCode from Location (NOLOCK) , TownCity (NOLOCK), code with (nolock)
						where RntCkiLocCode = LocCode
						AND   LocTctCode 	= TctCode
						AND   TctCtyCode = @sCtyCode
						AND   LocCodTypId  = CodId
						AND	  CodCode in ('AirPort1','AP','Branch','CC','Ferry Terminal','RELOC'))
	AND		RntStatus IN ('KK','CO','CI')
	AND 	RntCkiWhen BETWEEN @StartDate AND @EndDate


/*	GROUP BY Rental.RntCkiLocCode,
			Rental.RntCkoLocCode,
			Rental.RntCkiWhen,
			Rental.RntCkoWhen,
			Rental.RntId,
			RntNum,
			BooNum
	ORDER BY Rental.RntCkiLocCode, Rental.RntCkoLocCode, CONVERT(CHAR(10),Rental.RntCkiWhen,103), Rental.RntCkoWhen,Rental.RntId
*/
	OPEN Cur_in

	FETCH NEXT FROM Cur_in INTO @sRentalIdIn

	WHILE @@FETCH_STATUS=0
	BEGIN

		SET @sBpdIdCurrent = null
		SET @sBpdSapid = null
		SELECT TOP 1 @sBpdIdCurrent  = Bpdid,
					 @sBpdSapId		 = BpdSapid,
					 @dRntCkiWhen    = RntCkiWhen,
					 @dRntCkoWhen 	 = RntCkoWhen
			FROM dbo.Bookedproduct with (nolock) , dbo.rental (nolock)
			WHERE Bpdrntid = @sRentalIdIn
			AND  bpdrntid= rntid
			AND Bpdprdisvehicle = 1
			and Bpdiscurr = 1
			and BpdStatus = 'KK'
			ORder BY BpdCkiWhen desc

		--CLASS
		IF @sClassCode <> 'ALL'
		BEGIN

			IF NOT EXISTS(select sapid from Saleableproduct with (nolock), product with (nolock), type with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		Typid = PrdTypId
							AND		@sClaId = typclaid)
				GOTO GETNEXTCKI
		END

		-- Type
		IF @sTypCode <> 'ALL'
		BEGIN

			IF NOT EXISTS(select Sapid from Saleableproduct with (nolock), product with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	Prdid = SapPrdId
							AND		PrdTypid = @sTypId)
				GOTO GETNEXTCKI
		END

		--Brand
		IF @sBrdCode <> ''
		BEGIN
			IF NOT EXISTS(select rntid from dbo.rental with (nolock), package with (nolock)
							where 	rntid = @sRentalIdIn
							AND		rntpkgid = Pkgid
							AND		PkgBrdCode = @sBrdCode)

				GOTO GETNEXTCKI
		END

		-- Product
		IF @sPrdName <> 'ALL'
		BEGIN

			IF NOT EXISTS(select sapid from Saleableproduct with (nolock)
							WHERE 	Sapid = @sBpdSapId
							AND  	SapPrdId = @sPrdId)

				GOTO GETNEXTCKI
		END

		-- Thrifty Check
		IF @AU_THRIFTYPRDS is not null and @AU_THRIFTYPRDS <> ''
		BEGIN
			IF charindex((select claCode + '-' + Typcode from dbo.bookedproduct (nolock),
										dbo.saleableproduct (nolock),
										dbo.product (nolock),
										dbo.type (nolock),
										dbo.class (nolock)
						WHERE bpdid = @sBpdIdCurrent
						and bpdsapid = sapid
						and sapprdid = prdid
						and prdtypid = typid
						and typclaid = ClaId),@AU_THRIFTYPRDS) <> 0
						AND  @dRntCkiWhen >= @AU_THRIFTYEFFDT and @dRntCkoWhen >= @AU_THRIFTYEFFDT
				GOTO GETNEXTCKI
		END


		INSERT INTO @tCki
		SELECT  UPPER(bpdCkiLocCode),
				CONVERT(VARCHAR, RntCkiWhen, 106),
				Class.ClaCode,
				Type.TypCode,
				1,
				boonum,
				rntnum
		FROM	Product  WITH (NOLOCK) ,
               	Type  WITH (NOLOCK) ,
               	Class  WITH (NOLOCK) ,
               	SaleableProduct  WITH (NOLOCK) ,
               	BookedProduct  WITH (NOLOCK),
				rental with (nolock),
				booking with (nolock)
		WHERE   bpdid = @sBpdIdCurrent
		AND		bpdsapid = Sapid
		AND		SApPrdid = Prdid
		AND		Prdtypid = Typid
		AND		Typclaid = Claid
		AND		bpdrntid = rntid
		AND		rntbooid = booid

		GETNEXTCKI:
		FETCH NEXT FROM Cur_in INTO @sRentalIdIn
	END


SELECT	LTRIM(RTRIM(a1))	AS  RntCkiLocCode,
		a2	AS	RntDate,
		a3	AS	Class,
		a4	AS	Code,
		a5	AS	ClCount,
		a6 AS BooNum,
		a7 AS RntNum
FROM @tCki AS Checkin
--Order by rntdate, boonum, rntnum
FOR XML AUTO, ELEMENTS

END

SELECT	'</Checkins>'
SELECT 	'</tableData>'

Destroy_Cursor:














GO
