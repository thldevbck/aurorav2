USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetWeekProductLocationAvailability]    Script Date: 11/19/2007 13:44:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Flex2_GetWeekProductLocationAvailability] 
	@ComCode varchar(64),
	@FlpId int
AS

SELECT 
	*
FROM 
	FlexWeekProductLocationAvailabilityView
WHERE
	FlexWeekProductLocationAvailabilityView.ComCode = @ComCode
	AND FlexWeekProductLocationAvailabilityView.FlpId = @FlpId
ORDER BY
	FlexWeekProductLocationAvailabilityView.LcaFromLocCode,
	FlexWeekProductLocationAvailabilityView.LcaToLocCode










