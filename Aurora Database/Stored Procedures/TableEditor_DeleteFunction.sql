USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_DeleteFunction]    Script Date: 11/19/2007 13:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[TableEditor_DeleteFunction] 
	@tefId AS int
AS

DELETE
FROM 
	TableEditorFunction
WHERE
	tefId = @tefId



