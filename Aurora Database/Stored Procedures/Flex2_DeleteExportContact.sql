USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_DeleteExportContact]    Script Date: 11/19/2007 13:42:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_DeleteExportContact] 
	@FlxId int,
	@AgnId varchar(64) = NULL,
	@UsrId varchar(64) = NULL

AS

/* dont allow both agent and usrid to be set */
IF @AgnId IS NOT NULL AND @UsrId IS NOT NULL RETURN

DELETE FROM	FlexExportContact
WHERE
	FxcFlxId = @FlxId
	AND (FxcAgnId = @AgnId OR FxcUsrId = @UsrId)
