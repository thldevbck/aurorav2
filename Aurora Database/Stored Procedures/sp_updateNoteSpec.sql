set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


alter PROCEDURE [dbo].[sp_updateNoteSpec] 

		@UsrCode		VARCHAR	 (64)	=	'',
		@CtyCode VARCHAR	 (12)	=	'' ,
		@NtsId			varchar	(64),
		@NtsType	VARCHAR	 (64)	=	'' ,
		@NtsIsHeader bit,
		@NtsText VARCHAR	 (5000)	=	'' ,
		@NtsOrder int,
		@bIsCus bit, 
		@bIsAgn bit,
		@NtsIsActive bit,
		@PrgmName	VARCHAR	 (64)	=	''

AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@UsrId				VARCHAR	 (64) 
			
	SELECT	@UsrId = UsrId
	 FROM	UserInfo WITH (NOLOCK)
	 WHERE	UsrCode = @UsrCode
	
	insert zzlog (zzlogtext) values (convert(varchar(5),len(@NtsText)))

	IF LTRIM(RTRIM(@NtsText)) = ''
		SET @NtsIsActive = 0
	
	UPDATE 	NoteSpec  WITH (ROWLOCK)
	 SET 	NtsType 	=	@NtsType ,
			NtsCtyCode	=	@CtyCode ,
			NtsIsHeader =	@NtsIsHeader ,
			NtsText 	=	@NtsText,
			IntegrityNo =	IntegrityNo + 1 ,			
			NtsOrder 	=	@NtsOrder,
			NtsIsDefCus =	@bIsCus ,
			NtsIsDefAgn =	@bIsAgn ,
			ModUsrId 	=	@UsrId ,
			NtsIsActive =	@NtsIsActive ,
			ModDateTime	=	CURRENT_TIMESTAMP  ,
			AddPrgmName	=	@PrgmName
	 WHERE	NtsId	=  @NtsId

	IF @@ERROR <> 0 
		-- Return message to the calling program to indicate failure.
		select N'An error occurred while updating the NoteSpec record in database.'
	ELSE
		select ''

END


