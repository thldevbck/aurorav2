USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[RES_manageCheckIn]    Script Date: 07/24/2008 14:01:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER	PROCEDURE [dbo].[RES_manageCheckIn]
        @XMLData   TEXT   = DEFAULT,  
	@sUserCode			VARCHAR	 (64)	=	'',
	@sPrgmName			VARCHAR	 (64)	=	'',
	@ScreenValue		varchar	 (2)	=   ''
AS

	--if @sUserCode = 'rs3'
	--	return
	SET NOCOUNT ON
	DECLARE @ErrStr		VARCHAR	 (200)
	SET ANSI_WARNINGS off
	SET @ErrStr = NULL

	DECLARE		@doc				INT ,
				@sCkiLocCode		VARCHAR	 (64) ,
				@sVoucher			VARCHAR	 (64) , 
				@sRntId				VARCHAR	 (64) ,
				@sBpdId				VARCHAR	 (64) ,
				@sUnitNo			VARCHAR	 (64) ,
				@sRego				VARCHAR	 (64) ,
				@mOddometerIn		MONEY ,
				@sRnhId				VARCHAR	 (64) ,
				@sBooId				VARCHAR	 (64) ,
				@sRntCkiWhen 		VARCHAR	 (10) ,
				@sRntCkiTime		VARCHAR	 (5) ,
				@iIntNo				BIGINT ,
				@dCkiWhen			DATETIME ,
				@sRetMsg			VARCHAR	 (2000) ,
				@sCurrCodeListAfter	VARCHAR	 (8000) ,
				@iTotalAmtListAfter	VARCHAR	 (8000) ,
				@sCurrCodeListB4	VARCHAR	 (8000) ,
				@iTotalAmtListB4	VARCHAR	 (8000)
				-- Additions for Enhancement : 28
				,@sRntCkoWhen		VARCHAR	 (10) 
				,@sRntCkoTime		VARCHAR	 (5)
				,@sCountryCode		VARCHAR  (4)
				,@sProductId		VARCHAR  (64)
				,@sPakageId			VARCHAR  (64)
				,@sCkoLocCode       VARCHAR  (64)
				,@l_ApplicableSAPId VARCHAR  (64)
				,@nOddoDiff			VARCHAR  (100)
				,@nOdo100Kms        INT
				,@sAgentId			VARCHAR  (64)

	SELECT
		@sRntId			=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntId>', @XMLData), CHARINDEX('</RntId>', @XMLData) - CHARINDEX('<RntId>', @XMLData)), '<RntId>', ''),
		@sCkiLocCode 	=UPPER(REPLACE(SUBSTRING(@XMLData, CHARINDEX('<UserLocCode>', @XMLData), CHARINDEX('</UserLocCode>', @XMLData) - CHARINDEX('<UserLocCode>', @XMLData)), '<UserLocCode>', '')),
		@sVoucher		=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<Voucher>', @XMLData), CHARINDEX('</Voucher>', @XMLData) - CHARINDEX('<Voucher>', @XMLData)), '<Voucher>', ''),
		@sBpdId			=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<BpdId>', @XMLData), CHARINDEX('</BpdId>', @XMLData) - CHARINDEX('<BpdId>', @XMLData)), '<BpdId>', ''),
		@sUnitNo		=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<UnitNo>', @XMLData), CHARINDEX('</UnitNo>', @XMLData) - CHARINDEX('<UnitNo>', @XMLData)), '<UnitNo>', ''),
		@sRego			=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<Rego>', @XMLData), CHARINDEX('</Rego>', @XMLData) - CHARINDEX('<Rego>', @XMLData)), '<Rego>', ''),
		@mOddometerIn	=CAST(REPLACE(SUBSTRING(@XMLData, CHARINDEX('<OddometerIn>', @XMLData), CHARINDEX('</OddometerIn>', @XMLData) - CHARINDEX('<OddometerIn>', @XMLData)), '<OddometerIn>', '') AS MONEY),
		@sRntCkiWhen	=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntCkiWhen>', @XMLData), CHARINDEX('</RntCkiWhen>', @XMLData) - CHARINDEX('<RntCkiWhen>', @XMLData)), '<RntCkiWhen>', ''),
		@sRntCkiTime	=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntCkiTime>', @XMLData), CHARINDEX('</RntCkiTime>', @XMLData) - CHARINDEX('<RntCkiTime>', @XMLData)), '<RntCkiTime>', ''),
		@iIntNo			=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<IntNo>', @XMLData), CHARINDEX('</IntNo>', @XMLData) - CHARINDEX('<IntNo>', @XMLData)), '<IntNo>', '')

	IF NOT EXISTS(SELECT LocCode FROM dbo.Location WITH(NOLOCK) WHERE LocCode = @sCkiLocCode)
	BEGIN
		SELECT 'Error/' + dbo.getErrorString('GEN003', @sCkiLocCode, ' Check-In Location', NULL, NULL, NULL, NULL)
		RETURN
	END

	IF (SELECT dbo.getCountryForLocation(RntCkoLocCode, NULL, NULL)
			FROM	dbo.Rental WITH(NOLOCK)
			WHERE	RntId = @sRntId) <> dbo.getCountryForLocation(@sCkiLocCode, NULL, NULL)
	BEGIN
		SELECT 'Error/' + dbo.getErrorString('GEN003', @sCkiLocCode, ' Location for ', dbo.getCountryForLocation(RntCkoLocCode, NULL, 3), NULL, NULL, NULL)
			FROM	dbo.Rental WITH(NOLOCK)
			WHERE	RntId = @sRntId
		RETURN
	END



	/*IF NOT EXISTS(SELECT RntId from Rental (NOLOCK) where RntId = @sRntId and IntegrityNo = @iIntNo)
	begin
		declare 		@sRntNum		VARCHAR	(64)
		select @sRntNum = RntNum from rental (nolock) where RntId = @sRntId
		SET @ErrStr = NULL
		EXEC sp_get_Errorstring 'GEN007', 'Rental' , @sRntNum, @oparam1 = @ErrStr OUTPUT
		SELECT 'Error/' + @ErrStr
		GOTO DestroyCursor
	end*/

	SELECT @sBooId = RntBooId 
	FROM 	dbo.RentaL WITH (NOLOCK)
	WHERE 	RntId = @sRntId

	-- Changes for Enhancement #28 : Shoel : 24.7.8
	-- This checks the need for adding DTR and if so adds it 

	SELECT
		@sRntCkoWhen	=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntCkoWhen>', @XMLData), CHARINDEX('</RntCkoWhen>', @XMLData) - CHARINDEX('<RntCkoWhen>', @XMLData)), '<RntCkoWhen>', '')
		,@sRntCkoTime	=REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntCkoTime>', @XMLData), CHARINDEX('</RntCkoTime>', @XMLData) - CHARINDEX('<RntCkoTime>', @XMLData)), '<RntCkoTime>', '')
		,@sCountryCode   =REPLACE(SUBSTRING(@XMLData, CHARINDEX('<CtyCode>', @XMLData), CHARINDEX('</CtyCode>', @XMLData) - CHARINDEX('<CtyCode>', @XMLData)), '<CtyCode>', '')
		,@sCkoLocCode 	=UPPER(REPLACE(SUBSTRING(@XMLData, CHARINDEX('<RntCkoLocCode>', @XMLData), CHARINDEX('</RntCkoLocCode>', @XMLData) - CHARINDEX('<RntCkoLocCode>', @XMLData)), '<RntCkoLocCode>', ''))
		,@nOddoDiff = CAST(REPLACE(SUBSTRING(@XMLData, CHARINDEX('<OddoDiff>', @XMLData), CHARINDEX('</OddoDiff>', @XMLData) - CHARINDEX('<OddoDiff>', @XMLData)), '<OddoDiff>', '') AS INT)
	IF(ISNULL(@sCountryCode,'') = 'NZ')		
	BEGIN
		-- country is NZ, apply the thing

		-- checking if this rental has a DTR product applied already
		IF NOT  EXISTS(
				SELECT     
					1
				FROM         
					BookedProduct WITH (NOLOCK)
					INNER JOIN
					SaleableProduct WITH (NOLOCK)
					ON 
					BookedProduct.BpdSapId = SaleableProduct.SapId
				WHERE     
					SaleableProduct.SapPrdId = dbo.RES_GetDieselRecoveryTaxProductId(@sRntId) 
					AND 
					BookedProduct.BpdStatus NOT IN ('XX', 'XN') 
					AND 
					BookedProduct.BpdIsCurr = 1 
					AND 
					BookedProduct.BpdRntId = @sRntId
			 )
		BEGIN
			-- No Dtr Found, add one!

			 -- Checking if this Rental has a vehicle that requires DTR
			IF EXISTS (	SELECT     
							1
						FROM         
							BookedProduct WITH (NOLOCK)
							INNER JOIN
							SaleableProduct  WITH (NOLOCK)
							ON 
							BookedProduct.BpdSapId = SaleableProduct.SapId 
							INNER JOIN
							Product  WITH (NOLOCK)
							ON 
							SaleableProduct.SapPrdId = Product.PrdId
						WHERE     
							BookedProduct.BpdStatus NOT IN ('XX', 'XN') 
							AND 
							BookedProduct.BpdIsCurr = 1  
							AND 
							BookedProduct.BpdRntId = @sRntId--'3197261-1'
							AND 
							BookedProduct.BpdSapId IN (SELECT SapId FROM SaleableProduct AS SaleableProduct_1 WHERE (ISNULL(dbo.getProductAttributeValue(SapId, 'REQDTR'), 0) = 1))
						)
			BEGIN
				-- This has a vehicle that needs DTR, and no DTR is applied as yet
				
				-- Get Package Id
				SELECT 
					@sPakageId = RntPkgId
				FROM 
					Rental WITH (NOLOCK)
				WHERE
					RntId = @sRntId
			
				-- Get Agent Id
				SELECT @sAgentId = BooAgnId
				FROM Booking WITH (NOLOCK)
				WHERE BooId = @sBooId
				
				-- Get Product Id of the DTR thingie
				SET @sProductId = dbo.RES_GetDieselRecoveryTaxProductId(@sRntId) 

				-- GET the SAP ID for saving it!

				-- temp useless variables
				DECLARE @l_psReturnCodCurrId VARCHAR(64)	,
						@l_pbReturnPkgPrd BIT ,
						@l_pbReturnIsBase BIT ,
						@l_psReturnSapStatus VARCHAR(8000)  ,	
						@l_psError VARCHAR (8000)
				-- temp useless variables

				EXEC RES_getSaleableProductId
						 @psPkgID = @sPakageId
						,@psCtyCode	= @sCountryCode
						,@psPrdID = @sProductId
						,@psAgnId = @sAgentId
						,@psCkoLocCode = @sCkoLocCode
						,@psCkiLocCode = @sCkiLocCode
						,@pdCkoWhen = @sRntCkoWhen
						,@pdCkiWhen = @sRntCkiWhen
						,@ReturnSapId = @l_ApplicableSAPId OUTPUT
						,@psReturnCodCurrId	= @l_psReturnCodCurrId	OUTPUT
						,@pbReturnPkgPrd = @l_pbReturnPkgPrd OUTPUT
						,@pbReturnIsBase = @l_pbReturnIsBase OUTPUT
						,@psReturnSapStatus	= @l_psReturnSapStatus OUTPUT 
						,@psError = @l_psError OUTPUT	
				

						IF ISNULL(@l_psError, '') <> ''
						BEGIN
							SELECT 'Error/' + @l_psError
							GOTO DestroyCursor
						END
				-- Got the SAP ID !

				-- 'might' be needed - to ask!
				-- To delete records from TempTable created by managecalculate rates   
				--EXECUTE RES_DeleteTProcessedBpds  
				--@sRntId = @sRntId -- VARCHAR(64)=NULL   
				-- 'might' be needed - to ask!


				-- Rate Calculation goes here --this works!
				SET @nOdo100Kms =  @nOddoDiff / 100
				IF @nOdo100Kms = 0  SET @nOdo100Kms = 1
				-- Done with rate calculation
				
				-- SAVE THE DTR PRODUCT
				-- Temp variables
				DECLARE @l_sNewBpdIdsList AS VARCHAR(5000)
				DECLARE @l_ReturnError AS VARCHAR(5000)
				DECLARE @l_ReturnMessage AS VARCHAR(5000)
				-- Temp variables				

				EXEC RES_ManageBookedProductAddNonVehicleRequest  
    					 @sRntID = @sRntId 
						,@sSapId = @l_ApplicableSAPId
						--,@cRate = 1.1
						,@iQuantity = @nOdo100Kms
			        	,@bIsCusCharge = 1
        				,@sUserId = @sUserCode
        				,@sProgramName = @sPrgmName
						,@bCreateHistory = 1
						,@dCkoWhen     = @sRntCkoWhen 
						,@sCkoLocCode    = @sCkoLocCode
						,@dCkiWhen    =   @sRntCkiWhen
						,@sCkiLocCode   = @sCkiLocCode   
        				,@sNewBpdIdsList = @l_sNewBpdIdsList OUTPUT 
        				,@ReturnError = @l_ReturnError OUTPUT
        				,@ReturnMessage = @l_ReturnMessage OUTPUT

				IF ISNULL(@l_ReturnError, '') <> ''
				BEGIN
					SELECT 'Error/' + @l_ReturnError
					GOTO DestroyCursor
				END

				IF ISNULL(@l_ReturnMessage, '') <> ''
				BEGIN
					SELECT 'Msg/' + @l_ReturnMessage
					GOTO DestroyCursor
				END

				IF ISNULL(@l_sNewBpdIdsList,'') <> ''
				BEGIN
					SELECT 'Msg/' + 'Diesel Recovery Tax is applicable. Please collect payment from the customer'
					GOTO DestroyCursor
				END
		  
				-- Done saving
			END
			-- Done checking if this Rental has a vehicle that requires DTR
		END

	END

	----------------------------
	-- Done with Enhancement #28

	

	SET @dCkiWhen = @sRntCkiWhen + ' ' + @sRntCkiTime


	/*DECLARE		@bAgentOK				BIT ,
				@bCustomerOK			BIT ,
				@bCustomerOKBeforeCki	BIT ,
				@bCustomerOKAfterCki	BIT

	EXECUTE	CheckPaymentMade
		@sBooId				= @sBooId,
		@sRntId				= @sRntId,
		@sAgtCliSelection	= 'Customer',
		@bAgentOK			= @bAgentOK 	OUTPUT, 
		@bCustomerOK		= @bCustomerOK	OUTPUT

	SET @bCustomerOKBeforeCki	=	 @bCustomerOK
	*/
	
	Exec RES_getCharges
			@sBooId 			=	@sBooId ,
			@sRntId 			=	@sRntId ,
			@sAgtCliSelection	=	'Customer' ,
			@bSecurityOnly 		=	0 ,
			@sCurrCodeList 		=	@sCurrCodeListB4 OUTPUT,
			@iTotalAmtList 		=	@iTotalAmtListB4 OUTPUT,
			@iDepositAmtList	=	NULL

	
	
	IF EXISTS(SELECT RntId FROM dbo.Rental (NOLOCK)
			  WHERE RntId = @sRntId AND  Exists (SELECT BpdId from dbo.BookedProduct (nolock)	
												WHERE BpdId = @sBpdId and isNull(BpdOddometerIn,0)<>0)
			  AND RntCkiWhen>CAST(@dCkiWhen as DATETIME)
	)
	BEGIN
		SELECT 'Error/To change the check-in to an earlier date or time, please cancel the Late Fee or extension just created, and check-in again'
		RETURN
	END	

	--select @sBpdId '@sBpdId' , @dCkiWhen '@dCkiWhen', @sCkiLocCode '@sCkiLocCode', @mOddometerIn '@mOddometerIn'
		
	EXEC RES_ManageBookedProductCki
		-- Mandatory parameters
		@psBpdID				=	@sBpdId  ,
		@pdCkiWhen				=	@dCkiWhen ,
		@psCkiLocCode			=	@sCkiLocCode ,
		-- Optional parameters
		@iToOdo					=	@mOddometerIn ,
		@psUserId				=	@sUserCode ,
		@psProgramName			=	@sPrgmName ,
		-- Return values
		@pReturnError			=	@ErrStr		OUTPUT ,
		@pReturnMessage			=	@sRetMsg	OUTPUT

			
	IF ISNULL(@ErrStr, '') <> ''
	BEGIN
		SELECT 'Error/' + @ErrStr
		GOTO DestroyCursor
	END

	IF ISNULL(@sRetMsg, '') <> ''
	BEGIN
		SELECT 'Msg/' + @sRetMsg
		GOTO DestroyCursor
	END

	
	/*
	SELECT @bAgentOK = 0, @bCustomerOK = 0

	EXECUTE	CheckPaymentMade
		@sBooId				= @sBooId,
		@sRntId				= @sRntId,
		@sAgtCliSelection	= 'Customer',
		@bAgentOK			= @bAgentOK 	OUTPUT, 
		@bCustomerOK		= @bCustomerOK	OUTPUT

	
	SET @bCustomerOKAfterCki = @bCustomerOK

	IF @bCustomerOKBeforeCki = 1 AND @bCustomerOKAfterCki = 0
	BEGIN
		SELECT 'Msg/Customer Charges have altered - please collect payment from the customer.'
		GOTO DestroyCursor
	END
	*/

	Exec RES_getCharges
		@sBooId 			=	@sBooId ,
		@sRntId 			=	@sRntId ,
		@sAgtCliSelection	=	'Customer' ,
		@bSecurityOnly 		=	0 ,
		@sCurrCodeList 		=	@sCurrCodeListAfter OUTPUT,
		@iTotalAmtList 		=	@iTotalAmtListAfter OUTPUT,
		@iDepositAmtList	=	NULL

	IF (@sCurrCodeListB4<>@sCurrCodeListAfter) OR (@iTotalAmtListB4<>@iTotalAmtListAfter)
	BEGIN
		SELECT 'Msg/Customer Charges have altered - please collect payment from the customer.'
		GOTO DestroyCursor
	END



	-- Update Rental table with the following fields 
	UPDATE dbo.Rental WITH (ROWLOCK)
		SET 
			RntStatus 		= 	'CI' ,
			RntCkiLocCode	=	@sCkiLocCode ,
			--RntCkiWhen		=	@sRntCkiWhen + ' ' + @sRntCkiTime ,
			RntVoucNum		=	@sVoucher ,
			IntegrityNo		=	IntegrityNo + 1
	WHERE	RntId = @sRntId


	/***** Update Booked Rental table with the following fields as entered by the user : */
	
	/*UPDATE BookedProduct WITH (ROWLOCK)
		SET 
			BpdUnitNum 		= 	@sUnitNo,	--	NEED TO BE CHANGE
			BpdRego			= 	@sRego ,
			BpdOddometerIn	=	@mOddometerIn ,
			IntegrityNo		=	IntegrityNo + 1
	WHERE	BpdId = @sBpdId*/
	-- Validate Checkout Starts
	EXECUTE	RES_validateCheckInOut
		@sRntId			= @sRntId,			--	VARCHAR(64),
		@sBpdId			= @sBpdId,			--	VARCHAR(64)	= NULL,
		@sUnitNum		= @sUnitNo,			--	varchar(24) = NULL,
		@sRego 			= @sRego,			--	varchar(24) = NULL,
		@sOddoOut 		= @mOddometerIn,	--	int	= NULL,
		@sRntCkoWhen	= NULL,				--	varchar(10) = NULL,
		@sRntCkoTime	= NULL,				--	varchar(10) = NULL,
		@sOddoIn		= @mOddometerIn,	--	INT	= NULL,
		@sRntCkiWhen	= @sRntCkiWhen,		--	varchar(10) = NULL,
		@sRntCkiTime	= @sRntCkiTime,		--	varchar(10) = NULL,
		@sEvent			= 'CKI',			-- 	VARCHAR(12),	
		@sUsrCode		= @sUserCode,		--	VARCHAR(12) = NULL,
		@ScreenVal		= @ScreenValue,		--	VARCHAR(1) = 1,	
		@sRetError		= @ErrStr OUTPUT 	-- 	VARCHAR(2000)	OUTPUT

	IF ISNULL(@ErrStr, '') <> ''
	BEGIN
		SELECT @ErrStr
		GOTO DestroyCursor
	END

	-- Validate Checkout Ends

	-- Update Booking Status is all rental are now Checked Out
	EXECUTE RES_UpdateBookingStatus
			@sBooId	= @sBooId

	EXEC RES_checkRentalHistory
		@sBooId 		= @sBooId , 
		@sRntId 		= @sRntId ,
		@sChangeEvent	= 'CHECKIN' ,
		@sDataType 		= 'RNT',
		@sUserCode 		= @sUserCode ,
		@sAddPrgmName 	= @sPrgmName

DestroyCursor:
RETURN






