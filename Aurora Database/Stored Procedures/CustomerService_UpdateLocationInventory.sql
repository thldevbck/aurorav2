CREATE PROCEDURE [dbo].[CustomerService_UpdateLocationInventory]
	@LivId AS varchar(64),
	@LivStatus AS varchar(64),
	@IntegrityNo AS int,
	@UsrId AS varchar(64),
	@PrgrmName AS varchar(64)
AS


DECLARE @UpdatedLivId AS varchar(64)
SET @UpdatedLivId = NULL


UPDATE
	LocationInventory
SET
	@UpdatedLivId = LocationInventory.LivId,
	LocationInventory.LivStatus = @LivStatus,
	LocationInventory.IntegrityNo = ISNULL (LocationInventory.IntegrityNo, 0) + 1
FROM
	LocationInventory
WHERE
	LocationInventory.LivId = @LivId
	AND LocationInventory.IntegrityNo = @IntegrityNo


SELECT
	LocationInventory.*
FROM
	LocationInventory
WHERE
	LocationInventory.LivId = @UpdatedLivId
GO
