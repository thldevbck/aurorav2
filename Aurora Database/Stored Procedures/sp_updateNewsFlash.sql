set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


Alter PROCEDURE [dbo].[sp_updateNewsFlash] 

		@CtyCode		VARCHAR	 (64)	=	'' ,
		@UsrCode		VARCHAR	 (64)	=	'',
		@NwfId			varchar	(64),
		@NwfAudienceType VARCHAR	 (20)	=	'' ,
		@NwfEffectiveDate datetime,
		@NwfTerminationDate datetime,
		@NwfText	VARCHAR	 (5000)	=	''

AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@UsrId				VARCHAR	 (64) ,
				@sComCode			VARCHAR(64)

	SELECT	@sComCode = Company.ComCode 
	FROM	dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON  UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @UsrCode
				
	SELECT	@UsrId = UsrId
	 FROM	UserInfo WITH (NOLOCK)
	 WHERE	UsrCode = @UsrCode

	-- Validation for Date Overlapping
	if not EXISTS ( 
		SELECT 
			NwfID
		FROM 
			NewsFlash		AS	NewsFlash WITH (NOLOCK) 
		WHERE 
			NwfCtyCode=@CtyCode
			AND NwfTerminationDate > = CURRENT_TIMESTAMP
			AND (@UsrCode='' OR NwfComCode = @sComCode)
			and NwfAudienceType = @NwfAudienceType
			and NwfID <> @NwfId
			and 
			((NwfEffectiveDate < @NwfEffectiveDate and NwfTerminationDate > @NwfEffectiveDate)or
			(NwfEffectiveDate < @NwfTerminationDate and NwfTerminationDate > @NwfTerminationDate)or
			(NwfEffectiveDate > @NwfEffectiveDate and NwfTerminationDate > @NwfTerminationDate))


	)
	begin
		update NewsFlash
		set NwfAudienceType = @NwfAudienceType,
					NwfEffectiveDate = @NwfEffectiveDate,
					NwfTerminationDate = @NwfTerminationDate,
					NwfText = @NwfText,
					ModUsrId = @UsrId ,
					ModDateTime = CURRENT_TIMESTAMP 
		from NewsFlash
		where NwfID = @NwfId

		select ''
	end
	else
	begin
		--select 'The dates are overlapped with other News Flash records'
		SELECT dbo.getMessage('GEN121',@NwfAudienceType,'','','','','')
	end

END

