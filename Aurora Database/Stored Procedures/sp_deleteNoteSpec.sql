CREATE PROCEDURE [dbo].[sp_deleteNoteSpec]

	@NtsId			varchar	(64)

AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM NoteSpec
	WHERE NtsId = @NtsId

	IF @@ERROR <> 0
		-- Return message to the calling program to indicate failure.
		select N'An error occurred while deleting the NoteSpec record in database.'
	ELSE
		select ''

END


GO
