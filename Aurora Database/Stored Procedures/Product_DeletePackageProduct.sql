CREATE PROCEDURE [dbo].[Product_DeletePackageProduct]
	@PplPkgId	varchar(64),
	@PplSapId	varchar(64)
AS

	EXEC Package_DeletePackageProduct @PplPkgId, @PplSapId
GO
