CREATE PROCEDURE [dbo].[Product_GetSaleableProductLookups]
(
	@ComCode AS varchar(64)
)
AS

/*********************************************************************************************/
/* 0 - Attribute */
/*********************************************************************************************/
SELECT
	Attribute.*
FROM
	Attribute
ORDER BY
	Attribute.AttOrder,
	Attribute.AttCode

/*********************************************************************************************/
/* 1 - Code */
/*********************************************************************************************/
SELECT
	Code.*
FROM
	Code
	INNER JOIN CodeType ON CodeType.CdtNum = Code.CodCdtNum
ORDER BY
	CodeType.CdtNum,
	Code.CodOrder,
	Code.CodDesc

/*********************************************************************************************/
/* 2 - Country */
/*********************************************************************************************/
SELECT
	Country.*
FROM
	Country
WHERE
	Country.CtyHasProducts = 1
ORDER BY
	Country.CtyCode

/*********************************************************************************************/
/* 3 - Location */
/*********************************************************************************************/
SELECT
	Location.*
FROM
	Location
ORDER BY
	Location.LocCode

/*********************************************************************************************/
/* 4 - (Vehicle) Products */
/*********************************************************************************************/
SELECT
	Product.*
FROM
	Product
WHERE
	Product.PrdIsVehicle = 1
ORDER BY
	Product.PrdShortName


GO
