USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_ApproveWeekProduct]    Script Date: 11/19/2007 13:39:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[Flex2_ApproveWeekProduct] 
	@FlpId AS int,
	@IntegrityNo AS tinyint,
	@UsrId AS varchar(64)
AS

UPDATE FlexBookingWeekProduct
SET 
	FlpStatus = 'approved',
	IntegrityNo = @IntegrityNo + 1,
	ModUsrId = @UsrId,
	ModDateTime = GETDATE()
FROM
	FlexBookingWeekProduct 
WHERE
	FlexBookingWeekProduct.FlpId = @FlpId
	AND FlexBookingWeekProduct.IntegrityNo = @IntegrityNo

IF @@ROWCOUNT <= 0 RETURN

SELECT 
	* 
FROM 
	FlexBookingWeekProduct 
WHERE 
	FlpId = @FlpId












