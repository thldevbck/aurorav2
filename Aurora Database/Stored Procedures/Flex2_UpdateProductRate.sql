USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_UpdateProductRate]    Script Date: 11/19/2007 13:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Flex2_UpdateProductRate] 
	@FlrId AS int,
	@FlexNum AS int,
	@Changed AS bit
AS

UPDATE FlexBookingWeekRate
SET 
	FlrFlexNum = @FlexNum,
	FlrChanged = @Changed
FROM
	FlexBookingWeekRate
WHERE
	FlexBookingWeekRate.FlrId = @FlrId

SELECT * FROM FlexBookingWeekRate WHERE FlrId = @FlrId



