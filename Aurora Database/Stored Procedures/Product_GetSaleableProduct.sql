CREATE PROCEDURE [dbo].[Product_GetSaleableProduct]
(
	@ComCode AS varchar(64),
	@SapId AS varchar(64)
)
AS

-- EXEC Product_GetSaleableProduct 'THL', '2BB-298'
-- EXEC Product_GetSaleableProduct 'THL', '8CFF996F-3B42-461D-A72C-E9119C067CBF'

DECLARE @SaleableProducts table (
	ComCode varchar(64),
	BrdCode varchar(1),
	PrdId varchar(64),
	SapId varchar(64),
	SapSuffix int
)

INSERT INTO @SaleableProducts
(
	ComCode,
	BrdCode,
	PrdId,
	SapId,
	SapSuffix
)
SELECT
	Brand.BrdComCode AS ComCode,
	Brand.BrdCode AS BrdCode,
	Product.PrdId,
	SaleableProduct.SapId,
	SaleableProduct.SapSuffix
FROM
	Product
	INNER JOIN Brand
		ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN SaleableProduct
		ON SaleableProduct.SapPrdId = Product.PrdId
WHERE
	Brand.BrdComCode = @ComCode
	AND SaleableProduct.SapId = @SapId

/*********************************************************************************************/
/* 0 - Product */
/*********************************************************************************************/
SELECT
	Product.*
FROM
	@SaleableProducts AS s
	INNER JOIN Product ON Product.PrdId = s.PrdId

/*********************************************************************************************/
/* 1 - SaleableProduct */
/*********************************************************************************************/
SELECT
	SaleableProduct.*
FROM
	@SaleableProducts AS s
	INNER JOIN SaleableProduct ON SaleableProduct.SapId = s.SapId

/*********************************************************************************************/
/* 2 - ProductAttribute */
/*********************************************************************************************/
SELECT
	ProductAttribute.*
FROM
	@SaleableProducts AS s
	INNER JOIN ProductAttribute ON ProductAttribute.PatSapId = s.SapId
	INNER JOIN Attribute ON Attribute.AttId = ProductAttribute.PatAttId
ORDER BY
	s.SapSuffix DESC,
	Attribute.AttDataType,
	Attribute.AttOrder,
	Attribute.AttDesc

/*********************************************************************************************/
/* 3 - PersonTypeDiscount */
/*********************************************************************************************/
SELECT
	PersonTypeDiscount.*
FROM
	@SaleableProducts AS s
	INNER JOIN PersonTypeDiscount ON PersonTypeDiscount.PtdSapId = s.SapId
	INNER JOIN Code ON Code.CodId = PersonTypeDiscount.PtdCodPtyId
ORDER BY
	s.SapSuffix DESC,
	Code.CodCode

/*********************************************************************************************/
/* 4 - LocationAvailability */
/*********************************************************************************************/
SELECT
	LocationAvailability.*
FROM
	@SaleableProducts AS s
	INNER JOIN LocationAvailability ON LocationAvailability.LcaSapId = s.SapId
ORDER BY
	s.SapSuffix DESC,
	LocationAvailability.LcaFromLocCode,
	LocationAvailability.LcaToLocCode

/*********************************************************************************************/
/* 5 - NonAvailabilityProfile */
/*********************************************************************************************/
SELECT
	NonAvailabilityProfile.*
FROM
	@SaleableProducts AS s
	INNER JOIN NonAvailabilityProfile ON NonAvailabilityProfile.NapSapId = s.SapId
ORDER BY
	s.SapSuffix DESC,
	NonAvailabilityProfile.NapTravelFromDate,
	NonAvailabilityProfile.NapTravelToDate

/*********************************************************************************************/
/* 6 - Package */
/*********************************************************************************************/
SELECT
	Package.*
FROM
	Package
WHERE
	EXISTS
	(
		SELECT 1
		FROM
			@SaleableProducts AS s
			INNER JOIN PackageProduct ON PackageProduct.PplSapId = s.SapId
		WHERE
			PackageProduct.PplPkgId = Package.PkgId
	)
ORDER BY
	Package.PkgCode

/*********************************************************************************************/
/* 7 - PackageProduct */
/*********************************************************************************************/
SELECT
	PackageProduct.*
FROM
	@SaleableProducts AS s
	INNER JOIN PackageProduct ON PackageProduct.PplSapId = s.SapId
	INNER JOIN Package ON Package.PkgId = PackageProduct.PplPkgId
ORDER BY
	s.SapSuffix DESC,
	Package.PkgCode

/*********************************************************************************************/
/* 8 - TravelBandSet */
/*********************************************************************************************/
SELECT
	TravelBandSet.*
FROM
	@SaleableProducts AS s
	INNER JOIN TravelBandSet ON TravelBandSet.TbsSapId = s.SapId
ORDER BY
	s.SapSuffix DESC,
	TravelBandSet.TbsEffDateTime,
	TravelBandSet.AddDateTime DESC

/*********************************************************************************************/




GO
