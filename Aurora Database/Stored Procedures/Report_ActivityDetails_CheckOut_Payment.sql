CREATE PROCEDURE [dbo].[Report_ActivityDetails_CheckOut_Payment]
	@BookRental		VARCHAR(64)
AS

SET NOCOUNT ON

/*
DECLARE	@BookRental		VARCHAR(64)
SET		@BookRental = '3294349/1'
*/

select Payment, Curr
from reports.Report_ActDetCKOPayment
where BookRental = @BookRental



GO
