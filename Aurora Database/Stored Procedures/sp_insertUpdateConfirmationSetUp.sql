set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


Alter PROCEDURE [dbo].[sp_insertUpdateConfirmationSetUp] 

		@CtyCode		VARCHAR	 (64)	=	'' ,
		@UsrCode		VARCHAR	 (64)	=	'',
		@CrsId			varchar	(64),
		@PrgmName		VARCHAR	 (64)	=	'',
		@Status			varchar(64),
		@CrsText		varchar	(5000)	=''

AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@CrsRntStatus		VARCHAR	 (64) ,
				@UsrId				VARCHAR	 (64) ,
				@sComCode			VARCHAR(64)

	SELECT	@sComCode = Company.ComCode 
	FROM	dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON  UserCompany.UsrId  = UserInfo.UsrId AND UsrCode = @UsrCode
				
	SELECT	@UsrId = UsrId
	 FROM	UserInfo WITH (NOLOCK)
	 WHERE	UsrCode = @UsrCode
	
		IF @Status = 'Confirmed'
			SET @CrsRntStatus = 'KK'
		IF @Status = 'Provisional'
			SET @CrsRntStatus = 'NN'
		IF @Status = 'Waitlist'
			SET @CrsRntStatus = 'WL'
		IF @Status = 'Knockback'
			SET @CrsRntStatus = 'KB'
		IF @Status = 'Quote'
			SET @CrsRntStatus = 'QN'
		IF @Status = 'Cancelled (after confirmation)'
			SET @CrsRntStatus = 'XX'
		IF @Status = 'Cancelled (before confirmation)'
			SET @CrsRntStatus = 'XN'		

		IF RTRIM(LTRIM(@CrsId))=''
		BEGIN				
			set @CrsId = NEWID()
			
			INSERT INTO ConfirmationRentalStatus  WITH (ROWLOCK)
					(
						CrsId ,
						CrsRntStatus ,
						CrsCtyCode ,
						IntegrityNo ,
						CrsText ,
						CrsComCode,
						AddUsrId ,
						AddDateTime ,
						AddPrgmName
					)
				VALUES
					(
						@CrsId,
						@CrsRntStatus, 
						@CtyCode ,
						1 ,
						@CrsText ,
						@sComCode,
						@UsrId,
						CURRENT_TIMESTAMP,
						@PrgmName
					)
		END	--	END OF IF RTRIM(LTRIM(@CrsId))=''
		ELSE
		BEGIN	--	START OF ELSE  IF RTRIM(LTRIM(@CrsId))=''
			UPDATE  ConfirmationRentalStatus  WITH (ROWLOCK)
			 SET 	CrsRntStatus	=	@CrsRntStatus,
					CrsCtyCode		=	@CtyCode  ,
					IntegrityNo 	=	IntegrityNo + 1,
					CrsText			=	@CrsText  ,
					ModUsrId 		=	@UsrId,
					ModDateTime 	=	CURRENT_TIMESTAMP
			 WHERE	CrsId  = @CrsId
					
		END	--	END OF ELSE  IF RTRIM(LTRIM(@CrsId))=''

		select @CrsId as CrsId
	
END

