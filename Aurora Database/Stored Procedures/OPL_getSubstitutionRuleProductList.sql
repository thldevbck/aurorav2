CREATE     PROCEDURE [dbo].[OPL_getSubstitutionRuleProductList]
	@DefCountry 	varchar(24) = '',
	@UsrCode 	varchar(24) = '',
	@PrdId 		varchar(64) = '',
	@new		varchar(24) = ''
AS

--SET @DefCountry='IN'
SET NOCOUNT ON
-- Local Variable Declaration Start
DECLARE @sError 	varchar(500)
DECLARE @SubId 		varchar(64)
DECLARE @SubPrdId 	varchar(64)
DECLARE @PrdDesc	varchar(64)
DECLARE @SubIntNo	varchar(64)
DECLARE @SrpId		varchar(64)
DECLARE @SrpFlmId 	varchar(64)
DECLARE @FlmDesc	varchar(64)
DECLARE @SrpStDt	varchar(64)
DECLARE @SrpEnDt	varchar(64)
DECLARE @SrpFxCst	varchar(64)
DECLARE @SrpVrCst	varchar(64)
DECLARE @SrpVsLvl	varchar(64)
DECLARE @SrpAvl		varchar(64)
DECLARE @SrpActive	varchar(64)
DECLARE @SrpIntNo	varchar(64)
DECLARE @tacId 		varchar(500)
DECLARE @tacDesc 	varchar(500)
DECLARE @tacIntNo 	varchar(500)
-- KX Changes :RKS
DECLARE @sComCode	varchar(64)
-- Local Variable Declaration End

SET @tacId = ''
SET @tacDesc=''
SET @tacIntNo = ''


IF (@DefCountry <> '')
BEGIN
	IF NOT EXISTS(SELECT CtyCode
			FROM Country WITH (NOLOCK)
			WHERE CtyCode = @DefCountry
			AND   CtyIsActive = 1)
	BEGIN
		EXEC sp_get_ErrorString  'GEN003', @DefCountry, 'Country', @oparam1 = @sError OUTPUT
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>GEN003</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @sError + '</ErrDescription></Error>'
		GOTO DestroyCursor
	END
	IF NOT EXISTS(SELECT CtyCode
			FROM Country WITH (NOLOCK)
			WHERE CtyCode = @DefCountry
			AND   CtyHasProducts = 1)
	BEGIN
		EXEC sp_get_ErrorString  'GEN059', @oparam1 = @sError OUTPUT
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>GEN059</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @sError + '</ErrDescription></Error>'
		GOTO DestroyCursor
	END
	--SET @UsrCode = ''
END
ELSE
BEGIN
	IF EXISTS(SELECT UsrCode
			FROM UserInfo WITH (NOLOCK)
			WHERE UsrCode = @UsrCode
			AND   UsrIsActive = 0)
	BEGIN
		EXEC sp_get_ErrorString  'GEN003', @UsrCode, 'User', @oparam1 = @sError OUTPUT
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>GEN003</ErrNumber><ErrType>Application</ErrType><ErrDescription>' + @sError + '</ErrDescription></Error>'
		GOTO DestroyCursor
	END
	IF EXISTS(SELECT CtyCode
			FROM Country WITH (NOLOCK)
			WHERE CtyCode = (SELECT UsrCtyCode FROM UserInfo  WITH (NOLOCK) WHERE UsrCode = @UsrCode)
			AND   CtyIsActive = 0)
	BEGIN
		EXEC sp_get_ErrorString  'GEN003', @oparam1 = @sError OUTPUT
		SELECT '<Error><ErrStatus>True</ErrStatus><ErrNumber>GEN003</ErrNumber><ErrType>Application</ErrType><ErrDescription>User dosen''t have Valid Country Of operation</ErrDescription></Error>'
		GOTO DestroyCursor
	END
	SELECT @DefCountry = UsrCtyCode
		FROM UserInfo  WITH (NOLOCK)
		WHERE UsrCode = @UsrCode
END

-- KX Changes :RKS
SELECT @sComCode = Company.ComCode FROM dbo.Company (NOLOCK)
INNER JOIN dbo.UserCompany (NOLOCK) ON Company.comCode = UserCompany.ComCode
INNER JOIN dbo.UserInfo (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId AND UsrCode = @UsrCode
	DECLARE	@sRetValue	VARCHAR(256)
	SELECT @sRetValue = dbo.fun_ValidateCompanyCountry(@DefCountry,@sComCode)

	IF @sRetValue<>''
	BEGIN
		SELECT @sRetValue
		RETURN
	END
--select @sComCode


SELECT '<Cop code="' + @DefCountry + '" name="' + (SELECT CtyName FROM Country  WITH (NOLOCK) WHERE CtyCode = @DefCountry )+ '" />'
SELECT '<Prd id="' + @PrdId + '" />'
SELECT '<SubRuleList>'
IF @new <> 'YES' AND EXISTS(SELECT SubstitutionRule.SubId
				FROM
		SubstituteRulePeriod WITH (NOLOCK) , SubstitutionRule WITH (NOLOCK) , Product WITH (NOLOCK) , Brand WITH (NOLOCK) , class WITH (NOLOCK) , type WITH (NOLOCK)
	WHERE	SubId 		= SrpSubId
	AND   SubPrdId	LIKE	@PrdId+'%'
	AND	SubPrdId 	= PrdId
	AND	Product.PrdIsVehicle = 1
	AND	CAST(CONVERT(VARCHAR(10), SrpEndDate, 103) AS DATETIME) 	>= 	CAST(CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103) AS DATETIME)
	AND	BrdCode	 	= PrdBrdCode
	AND	SubCtyCode 	= @DefCountry
	AND   prdtypid = typid
	AND   typclaid = claid
	-- KX Changes :RKS
	AND	(@UsrCode = '' OR BrdComCode  = @sComCode)

			)
BEGIN
	DECLARE cur_Sub CURSOR SCROLL FOR
	SELECT
		''+ISNULL(SubId,'')							AS	SubId,
		''+ISNULL(SubPrdId,'')							AS	SubPrdId,
		''+ISNULL(PrdShortName + ' - ' + Brand.BrdName + ' ' + PrdName, '')	AS	PrdDesc,
		''+SubstitutionRule.IntegrityNo						AS	SubIntNo,
		''+SrpId								AS	SrpId,
		''+SrpFlmId								AS	SrpFlmId,
		''+dbo.getFleetModelDesc(SrpFlmId,1)					AS	FlmDesc,
		''+ISNULL(CONVERT(VARCHAR(10), SrpStartDate, 103), '')			AS	SrpStDt,
		''+ISNULL(CONVERT(VARCHAR(10), SrpEndDate, 103), '')			AS	SrpEnDt,
		''+ISNULL(CAST(SrpFixedCost AS VARCHAR), '')				AS	SrpFxCst,
		''+ISNULL(CAST(SrpVariableDailyCost AS VARCHAR), '')			AS	SrpVrCst,
		''+ISNULL(SrpCodVisLvlId, '')						As	SrpVsLvl,
		''+ SrpAppliesToAvailability	AS	SrpAvl,
		''+ISNULL(CAST(SrpIsActive AS VARCHAR),'0')				AS	SrpActive,
		''+CONVERT(VARCHAR, SubstituteRulePeriod.IntegrityNo)			AS	SrpIntNo
	FROM
		SubstituteRulePeriod WITH (NOLOCK) , SubstitutionRule WITH (NOLOCK) , Product WITH (NOLOCK) , Brand WITH (NOLOCK) , class WITH (NOLOCK) , type WITH (NOLOCK)
	WHERE	SubId 		= SrpSubId
	AND   SubPrdId	LIKE	@PrdId+'%'
	AND	SubPrdId 	= PrdId
	AND	Product.PrdIsVehicle = 1
	AND	CAST(CONVERT(VARCHAR(10), SrpEndDate, 103) AS DATETIME) 	>= 	CAST(CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 103) AS DATETIME)
	AND	BrdCode	 	= PrdBrdCode
	AND	SubCtyCode 	= @DefCountry
	AND   prdtypid = typid
	AND   typclaid = claid
	-- KX Changes :RKS
	AND	(@UsrCode = '' OR BrdComCode  = @sComCode)
	ORDER BY claorder, typorder, prdshortname

	OPEN cur_Sub

	IF(@@ERROR <> 0 ) GOTO DestroyCursor
	FETCH FIRST FROM cur_Sub INTO @SubId, @SubPrdId, @PrdDesc, @SubIntNo, @SrpId, @SrpFlmId, @FlmDesc, @SrpStDt, @SrpEnDt, @SrpFxCst, @SrpVrCst, @SrpVsLvl, @SrpAvl, @SrpActive, @SrpIntNo
	WHILE(@@FETCH_STATUS = 0)
	BEGIN
		SET @tacId = ''
		SET @tacDesc=''
		SET @tacIntNo = ''
		SELECT	@tacId = @tacId + ',' + TacId,
			@tacDesc = @tacDesc + ',' +
				CONVERT(VARCHAR,ISNULL(TacNumOfHalfDays,0)) +
				'-$' +
				CONVERT(VARCHAR,ISNULL(TacCost,0)),
			@tacIntNo = @tacIntNo + ',' + CAST(IntegrityNo AS VARCHAR)
		FROM TurnAroundCost WITH (NOLOCK)
		WHERE TacSrpId = @SrpId
		ORDER BY TacCost DESC
		SET @tacId = SUBSTRING(@tacId,2,LEN(@tacId))
		SET @tacDesc = SUBSTRING(@tacDesc,2,LEN(@tacDesc))
		SET @tacIntNo = SUBSTRING(@tacIntNo,2,LEN(@tacIntNo))

		SELECT '<SubRulePeriod>
			<SubId>'	+	@SubId		+	'</SubId>
			<SubPrdId>'	+	@SubPrdId 	+ 	'</SubPrdId>
			<PrdDesc>'	+	@PrdDesc	+	'</PrdDesc>
			<SubIntNo>'	+	@SubIntNo	+	'</SubIntNo>
			<SrpId>'	+	@SrpId		+	'</SrpId>
			<SrpFlmId>' 	+ 	@SrpFlmId 	+ 	'</SrpFlmId>
			<FlmDesc>' 	+ 	@FlmDesc 	+ 	'</FlmDesc>
			<SrpStDt>' 	+ 	@SrpStDt 	+ 	'</SrpStDt>
			<SrpEnDt>' 	+ 	@SrpEnDt 	+ 	'</SrpEnDt>
			<SrpFxCst>' 	+ 	@SrpFxCst 	+ 	'</SrpFxCst>
			<SrpVrCst>' 	+ 	@SrpVrCst 	+ 	'</SrpVrCst>
			<SrpVsLvl>' 	+ 	@SrpVsLvl 	+ 	'</SrpVsLvl>
			<SrpAvl>' 	+ 	@SrpAvl 	+ 	'</SrpAvl>
			<SrpActive>' 	+ 	@SrpActive 	+ 	'</SrpActive>
			<SrpIntNo>' 	+ 	@SrpIntNo 	+ 	'</SrpIntNo>
			<TctId>' 	+ 	@tacId 		+ 	'</TctId>
			<TctDesc>' 	+ 	@tacDesc 	+ 	'</TctDesc>
			<TctIntNo>' 	+ 	@tacIntNo 	+ 	'</TctIntNo>
			<Rem>'		+	'0'		+	'</Rem>
			</SubRulePeriod>'
		FETCH NEXT FROM cur_Sub INTO @SubId, @SubPrdId, @PrdDesc, @SubIntNo, @SrpId, @SrpFlmId, @FlmDesc, @SrpStDt, @SrpEnDt, @SrpFxCst, @SrpVrCst, @SrpVsLvl, @SrpAvl, @SrpActive, @SrpIntNo
	END
	IF (@PrdId <> '')
	BEGIN
		SELECT  '<SubRulePeriod>
				<SubId>'+@SubId+'</SubId>
				<SubPrdId>' + @PrdId + '</SubPrdId>
				<PrdDesc></PrdDesc>
				<SubIntNo></SubIntNo>
				<SrpId></SrpId>
				<SrpFlmId></SrpFlmId>
				<FlmDesc></FlmDesc>
				<SrpStDt></SrpStDt>
				<SrpEnDt></SrpEnDt>
				<SrpFxCst></SrpFxCst>
				<SrpVrCst></SrpVrCst>
				<SrpVsLvl></SrpVsLvl>
				<SrpAvl>Available</SrpAvl>
				<SrpActive>1</SrpActive>
				<SrpIntNo></SrpIntNo>
				<TctId></TctId>
				<TctDesc></TctDesc>
				<TctIntNo></TctIntNo>
				<Rem>0</Rem>
			</SubRulePeriod>'
	END
END
ELSE
BEGIN
	SELECT  '<SubRulePeriod>
			<SubId></SubId>
			<SubPrdId></SubPrdId>
			<PrdDesc></PrdDesc>
			<SubIntNo></SubIntNo>
			<SrpId></SrpId>
			<SrpFlmId></SrpFlmId>
			<FlmDesc></FlmDesc>
			<SrpStDt></SrpStDt>
			<SrpEnDt></SrpEnDt>
			<SrpFxCst></SrpFxCst>
			<SrpVrCst></SrpVrCst>
			<SrpVsLvl></SrpVsLvl>
			<SrpAvl>Available</SrpAvl>
			<SrpActive>1</SrpActive>
			<SrpIntNo></SrpIntNo>
			<TctId></TctId>
			<TctDesc></TctDesc>
			<TctIntNo></TctIntNo>
			<Rem>0</Rem>
		</SubRulePeriod>'

END
SELECT '</SubRuleList>'
GOTO DestroyCursor

DestroyCursor:
	IF( (SELECT COUNT(cursor_handle) FROM master.dbo.syscursors  WITH (NOLOCK) WHERE cursor_name = 'cur_Sub') != 0 )
	BEGIN
		CLOSE cur_Sub
		DEALLOCATE cur_Sub
	END
	RETURN


GO
