set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

alter PROCEDURE [dbo].[sp_deleteNewsFlash] 

	@NwfId			varchar	(64)

AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM NewsFlash
	WHERE NwfID = @NwfId

	IF @@ERROR <> 0 
		-- Return message to the calling program to indicate failure.
		select N'An error occurred while deleting the NewsFlash record in database.'
	ELSE
		select ''
	
END

