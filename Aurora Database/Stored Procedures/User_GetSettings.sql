set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go





ALTER PROCEDURE [dbo].[User_GetSettings] 
	@usrCode AS varchar(64)            
AS

/*
EXEC [User_GetSettings] 'jl3'
*/


DECLARE @UviValue AS varchar(128)
SELECT @UviValue=UviValue FROM UniversalInfo WHERE UviKey = 'SYSIDENT'

SELECT 
	ISNULL (UserInfo.UsrId, '') AS UsrId,
	ISNULL (UserInfo.UsrCode, '') AS UsrCode,
	ISNULL (UserInfo.UsrName, '') AS UsrName,
	ISNULL (Company.ComName, '') AS ComName,
	ISNULL (Company.ComCode, '') AS ComCode,
	ISNULL (CompanySettings.ComCulture, 'en-nz') AS ComCulture,
	ISNULL (CompanySettings.ComDateFormat, 'dd/MM/yyyy') AS ComDateFormat,
	ISNULL (Location.LocCode, '') AS LocCode,
	ISNULL (Location.LocName, '') AS LocName,
	ISNULL (Location.LocHoursBehindServerLocation, 0.00) AS LocHoursBehindServerLocation,
	ISNULL (TownCity.TctCode, '') AS TctCode,
	ISNULL (TownCity.TctName, '') AS TctName,
	ISNULL (Country.CtyCode, '') AS CtyCode,
	ISNULL (Country.CtyName, '') AS CtyName,
	ISNULL (Country.CtyDefCurrCode, '') AS CtyDefCurrCode,
	ISNULL (@UviValue, '') AS SystemIdentifier,
	ISNULL (Functions.FunFileName, '') AS FunFileName
FROM 
	UserInfo 
	INNER JOIN UserCompany ON UserCompany.UsrId = UserInfo.UsrId
	INNER JOIN Company ON Company.ComCode = UserCompany.ComCode 
	INNER JOIN CompanySettings ON Company.ComCode = CompanySettings.ComCode
	INNER JOIN Location ON Location.LocCode = userinfo.UsrLocCode
	INNER JOIN TownCity ON TownCity.TctCode = Location.LocTctCode
	INNER JOIN Country ON Country.CtyCode = userinfo.UsrCtyCode
	LEFT JOIN Menu ON UserInfo.UsrDefaultMenId = Menu.MenId
	LEFT JOIN Functions ON Menu.MenFunId = Functions.FunId		
WHERE
	@usrCode = UserInfo.UsrCode 


