set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



ALTER	PROCEDURE [dbo].[RES_getMaintainBookedProduct]
	@sBpdId	VARCHAR(64)=''
AS
BEGIN
	SET NOCOUNT ON
	/*==========>> Initial Screen Display <<========================*/		
		DECLARE 		@sRntId			VARCHAR	 (64) ,
						@sBooId			VARCHAR	 (64) ,
						@sPkgId			VARCHAR	 (64) ,
						@PrdList		VARCHAR	 (200) ,
						@Err			VARCHAR	 (200) ,
						@sCkoLoc		VARCHAR	 (10) ,
						@sCtyCode		VARCHAR 	 (64) ,
						@bShowCkoL		BIT ,
						@bShowCkiL		BIT ,
						@bShowCkoD		BIT ,
						@bShowCkiD		BIT ,
						@bIsVehicle		BIT ,
						@sSapId			VARCHAR	 (64) ,
						@bBpdIsCurr		BIT ,
						@sBpdStatus		VARCHAR	 (64) ,
						@sBpdType		VARCHAR	 (64) ,
						@bDispCancelBtn	BIT ,
						@bDispReclChg	BIT ,
						@sAimsAssetType	VARCHAR	 (64) ,
						@sValue			VARCHAR	 (64) ,
						@sUviValue		VARCHAR	 (64) ,
						@bBkgRef		BIT ,
						@bApplyDisc 	BIT ,
						@dBpdCkoWhen 	DATETIME ,
						@dBpdCkiWhen	DATETIME,
						@IsXFRM			VARCHAR ,
						@bXchgRnt		BIT ,
						@bNewModifiable	CHAR(1) ,
						@cNonRevB4XchgBpdIds	VARCHAR	 (8000) ,
						@cRevB4XchgBpdIds		VARCHAR	 (8000),
	-- Issue 879: RKS : 18-MAY-2006
	-- START
						@sRnhId					VARCHAR(64),
						@sXFRDTpFin			VARCHAR(1)			-- added : RKS 21 SEP 2005 Issue  - 810
						

		SET @sRnhId = NEWID()
		
		
	
		-- END added : RKS 21 SEP 2005 Issue  - 810
	-- END

		
						
		SELECT
			@sRntId 		= Rental.RntId ,
			@sBooId 		= Rental.RntBooId ,
			@sPkgId 		= Rental.RntPkgId ,
			@sCkoLoc 		= Rental.RntCkoLocCode ,
			@sSapId 		= BookedProduct.BpdSapId ,
			@bBpdIsCurr 	= ISNULL(BookedProduct.BpdIsCurr,0) ,
			@sBpdStatus 	= BookedProduct.BpdStatus ,
			@sBpdType 		= dbo.getCodCode(BookedProduct.BpdCodTypeId), 
			@dBpdCkoWhen 	= BpdCkoWhen ,
			@dBpdCkiWhen 	= BpdCkiWhen 
		FROM         
			BookedProduct (nolock) INNER JOIN
                  Rental (nolock) ON BookedProduct.BpdRntId = Rental.RntId
		WHERE
			BookedProduct.BpdId = @sBpdId

		-- START added : RKS 21 SEP 2005 Issue  - 810
		IF EXISTS(Select BpdId FROM dbo.BookedProduct (NOLOCK) WHERE BpdRntId = @sRntId AND BpdRntXfrdToFinanWhen IS NOT NULL)
			SET @sXFRDTpFin = 'Y'
		ELSE
			SET @sXFRDTpFin = ''

		exec RES_GetExchangeBpds
				@sRntId					=	@sRntId ,
				@sBpdIdList				= 	NULL ,
				@cNonRevB4XchgBpdIds	=	@cNonRevB4XchgBpdIds 	OUTPUT,
				@cRevB4XchgBpdIds		=	@cRevB4XchgBpdIds		OUTPUT,		--	Added: RKS 26-03-2003
				@cRntAgrList			=	NULL ,
				@cNonRevB4XchgAmt		=	NULL ,
				@cRevB4XchgAmt			=	NULL ,
				@cXfrmAmt				=	NULL ,
				@cXtoAmt				=	NULL 
		
		SET @bXchgRnt = 0		
		IF 	isnull(@cNonRevB4XchgBpdIds,'')<>''
			and isnull(@cRevB4XchgBpdIds,'')<>''
			SET @bXchgRnt = 1

				
		if @sBpdStatus NOT IN ('XX','XN') OR (@bXchgRnt = 1 and (@sBpdId = dbo.getSplitedData(isnull(@cNonRevB4XchgBpdIds,''), ',' , dbo.getEntry(isnull(@cNonRevB4XchgBpdIds,''), ',') + 1))
			OR (@sBpdId = dbo.getSplitedData(isnull(@cRevB4XchgBpdIds,''), ',' , dbo.getEntry(isnull(@cRevB4XchgBpdIds,''), ',') + 1)))
			SET @bNewModifiable = '1'
		ELSE
			SET @bNewModifiable = '0'

		IF UPPER(@sBpdType) = 'XFRM'
			SET @IsXFRM	 = 	'1'
		
		SELECT @PrdList = NULL
		SELECT @PrdList = CASE WHEN @PrdList is null then BpdID ELSE @PrdList + ',' + Bpdid END
				FROM Bookedproduct WITH (nolock)
				WHERE bpdid 	= @sBpdId
				AND   Bpdstatus not in ('XX','XN')
				AND   BpdIsCurr = 1
				AND   BpdPrdisVehicle = 1
		
		IF @PrdList IS NOT NULL
			SET @bIsVehicle = 1
		ELSE
			SET @bIsVehicle = 0		
		/*===============>> GET THE COUNTRY OF Travel <<===============*/
		SELECT @sCtyCode = dbo.getCountryForLocation (@sCkoLoc , @sPkgId , 1)
		/*===> Set Logical Working Variables to Show whether to Display Check-Out/Check_in dates 
							and Locations <=======*/
		/*==> This is only required if the product is not a vehicle, as vehicle will show these columns by default. <===*/
		IF @bIsVehicle=0 
		BEGIN
			/* -- Test Check-Out location. */
			SET @bShowCkoL = 0	--	SET Value to 0
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'PromptCkoL')
			IF @sValue = 1
				SET @bShowCkoL = 1
			
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'DefCkoLoc')
			
			IF @sValue = 1
				SET @bShowCkoL = 1

			SET @bShowCkiL = 0		-- Set Value to 0
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'PromptCkiL')
			IF @sValue = 1
				SET @bShowCkiL = 1
			
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'DefCkiLoc')
			
			IF @sValue = 1
				SET @bShowCkiL = 1			
			
			SET @bShowCkoD = 0	-- Set Value to 0
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'PromptCkoD')
			IF @sValue = 1
				SET @bShowCkoD = 1
			
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId,	'DefCkoDate')
			IF @sValue = 1
				SET @bShowCkoD = 1
			
			SET @bShowCkiD = 0	-- Set Value to 0
			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'PromptCkiD')
			IF @sValue = 1
				SET @bShowCkiD = 1

			SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'DefCkiDate')
			IF @sValue = 1
				SET @bShowCkiD = 1
		END -- IF @bIsVehicle=0 

		/*===========>> Cancel Processing ( VISIBILITY Of Button)*/
		
		SELECT @sValue = ISNULL(dbo.getProductAttributeValue (@sSapId, 'MANDATORY'),0)
		IF 	(@sValue  <>  '1' AND @bIsVehicle = 0 AND @bBpdIsCurr = 1 AND @sBpdStatus NOT IN ('XX','XN') AND UPPER(@sBpdType) <> 'XFRM')
		OR	(@bIsVehicle = 1 and UPPER(@sBpdType) = 'EXT' and @dBpdCkoWhen>CURRENT_TIMESTAMP and @dBpdCkiWhen>CURRENT_TIMESTAMP)
			SET @bDispCancelBtn = 1
		ELSE
			SET @bDispCancelBtn = 0
		
		/*====> Visible?? Recalculation Charges */
		IF (@bBpdIsCurr = 0 AND @sBpdStatus NOT IN ('XX','XN'))
			SET @bDispReclChg = 0
		ELSE
			SET @bDispReclChg = 1 					
								
		/*====> Rego AND Unit */
		IF   @bIsVehicle = 1                                  
		BEGIN
			SELECT	@sUviValue = UviValue
				FROM 	UniversalInfo (nolock) 
				WHERE 	UviKey = @sCtyCode + 'DefUnit' 
			IF @sUviValue = 'Unit'
				SET @sAimsAssetType = 'Unit'
			ELSE
				SET @sAimsAssetType = 'Rego'
		END
		ELSE
			SET @sAimsAssetType = 'Serial'
		
		/*===>> Visibility?? */
		SET @bBkgRef = 0		-- Set Value to 0
		SELECT @sValue = dbo.getProductAttributeValue (@sSapId, 'BkgRef')
		IF @sValue = 1
			SET @bBkgRef = 1
				
		--==========>> For the Rental Table <<=======================		
		SELECT	
			RntId																			AS	RntId,
			--JL 2008/06/19 change name
			RntNum																		AS	RentalNum,
			CONVERT(VARCHAR(10),RntCkoWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkoWhen,'D') + ' ' +  RntCkoLocCode		AS	'Check-Out',
			CONVERT(VARCHAR(10),RntCkiWhen, 103) + ' ' + dbo. GetDateTimeForWeeklyProfile(RntCkiWhen,'D') + ' ' +  RntCkiLocCode		AS	'Check-In' ,
			ISNULL(RntNumOfHirePeriods, 0)																AS	HirePd ,
			ISNULL(Package.PkgCode,'')																	AS	Package ,
			''+ ISNULL(BrdName,'')																		AS	Brand ,
			''+ ISNULL(PrdShortName,'')																	AS	Vehicle ,
			RntStatus																					AS	Status ,
			''+ BooNum																					AS	BooNum ,
			''+ AgnCode + ' - ' + AgnName + ' - ' + dbo.GEN_getCodDesc(AgnCodDebtStatId,NULL,NULL)		AS	AgnName ,
			''+ ISNULL(BooLastName,'') + ', ' + ISNULL(BooTitle,'') +  ' ' + ISNULL(BooFirstName,'')	AS Hirer ,
			 '' + ISNULL(BooStatus,'')																	AS	BookingSt ,
			'' + BooId																					AS	BooId ,
			left(DateName(dw,RntCkoWhen),3)																AS	CkoDay ,
			left(DateName(dw,RntCkiWhen),3)																AS	CkiDay ,
			--Added by Shoel on 6/11/7 > for the Integrity number issue defect #23
			'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo,
            '' + CAST(Booking.IntegrityNo AS VARCHAR) AS BooIntNo
		FROM
			Rental (nolock),
			Package (nolock) ,
			Brand (nolock) ,
			Product (nolock) ,
			Booking (nolock) ,
			Agent (nolock) 
		WHERE 	BooAgnId = AgnId
		AND		RntBooId = BooId
		AND		Package.PkgId = Rental.RntPkgId
		AND		Brand.BrdCode = Package.PkgBrdCode
		AND		Product.PrdId =* Rental.RntPrdRequestedVehicleId
		AND		Rental.RntId = @sRntId
		FOR XML AUTO, ELEMENTS
	
	
		--==========>> For the Booked Product Detail <<=======================	
		IF EXISTS(
					SELECT     
						bpdid
					FROM         
						BookedProduct (nolock) INNER JOIN
                  				Charge (nolock) ON BookedProduct.BpdId = Charge.ChgBpdId INNER JOIN
                  				ChargeDiscount (nolock) ON Charge.ChgId = ChargeDiscount.ChdChgId
					WHERE     
						(BookedProduct.BpdId = @sBpdId)
							AND
						ChargeDiscount.ChdPtdId IS NOT NULL
		)
		BEGIN
			SET @bApplyDisc = 1
		END

		SELECT     
			ISNULL(BookedProduct.BpdId,'') 					AS BpdId, 
			ISNULL(BookedProduct.BpdRntId,'') 				AS RntId,
			CASE 
				WHEN isnull(BpdExtRef,0) = 0 THEN BooNum + '/' + RntNum 
				ELSE BooNum + '/' + RntNum  + 'x' + BpdExtRef
			END												AS ExternalRef ,
			ISNULL(dbo.RES_getCheckOutStatus(BpdRntId),'')	AS CheckOutStatus,
			ISNULL(Rental.RntPkgId,'')						AS PkgId ,
			ISNULL(Product.PrdDvassSeq,'') 					AS PrdDvassSeq ,
			ISNULL(BookedProduct.BpdSapId,'') 				AS SapId,
			/*CASE 
				WHEN BookedProduct.BpdAgnId IS NOT NULL THEN BookedProduct.BpdAgnId
				ELSE ISNULL(Booking.BooAgnId,'')						
			END 											AS AgnId ,*/
			''												AS AgnId ,
			ISNULL((Product.PrdShortName + ' - ' + CAST(SapSuffix AS VARCHAR) + ' - ' + Brand.BrdName + ' - ' + Product.PrdName),'') AS PrdName, 
			ISNULL(@sCtyCode,'')							AS CtyCode,
			'' + ISNULL(PrdId,'')							AS PrdId, 							
			ISNULL((CASE @sAimsAssetType
				WHEN 'Unit' THEN BookedProduct.BpdUnitNum
				WHEN 'Rego' THEN BookedProduct.BpdRego
				-- GPS
				ELSE BookedProduct.BpdPlannedSerialCode
			END),'')										AS UnitNum ,
			ISNULL(BookedProduct.BpdIniStatus,'')			AS IniSt, 
			ISNULL((CONVERT(VARCHAR,BookedProduct.BpdQty)),'0.00')	AS	BpdQty ,
			ISNULL(BookedProduct.BpdActualSerialCode,'')	AS SrCd ,
			ISNULL(BpdUnitNum,'')							AS UnitNumber ,
			ISNULL(BpdUnitNum,'')							AS OldUnitNumber ,
			ISNULL(BpdRego,'')								AS RegoNumber ,
			ISNULL(BpdRego,'')								AS OldRegoNumber ,
			ISNULL((SELECT UviValue From UniversalInfo with (nolock) where UviKey = 'CROSSHIRE_' + dbo.getCountryForLocation(RntCkoLocCode,Null,Null)),'') AS CrossHireUnitNum ,	
			ISNULL(BookedProduct.BpdCodTypeId,'')			AS TypeId ,
            ISNULL(BookedProduct.BpdStatus,'') 				AS St, 
			ISNULL((BookedProduct.BpdCkoLocCode + ' - ' +
		      (SELECT     
				Location.LocName
		        FROM          
				LOCATION (nolock) 
		        WHERE      
				LocCode = BookedProduct.BpdCkoLocCode)),'') AS CkoLoc , 
			ISNULL((BookedProduct.BpdCkiLocCode + ' - ' +
		       (SELECT     
				Location.LocName
		          FROM          
					LOCATION (nolock) 
		          WHERE      
					LocCode = BookedProduct.BpdCkiLocCode)),'') AS CkiLoc , 
			ISNULL(BookedProduct.BpdFrom,'')				AS Fr, 
			ISNULL(CONVERT(VARCHAR(10), BookedProduct.BpdCkoWhen, 103),'') AS BpdCkoDt, 
			ISNULL(LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdCkoWhen, 108), 5),'') AS BpdCkoTm, 
			ISNULL(BookedProduct.BpdTo,'')					AS 'To', 
			ISNULL(CONVERT(VARCHAR(10),BookedProduct.BpdCkiWhen, 103),'') AS BpdCkiDt, 
			ISNULL(LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdCkiWhen, 108), 5),'') AS BpdCkiTm , 
			ISNULL(CONVERT(VARCHAR(10), BookedProduct.BpdChargeFromWhen, 103),'') AS ChgFrDt, 
			ISNULL(LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdChargeFromWhen, 108),5),'') AS ChgFrTm,
			ISNULL(CONVERT(VARCHAR(10), BookedProduct.BpdChargeToWhen, 103),'') AS ChgToDt, 
			ISNULL(LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdChargeToWhen, 108),5),'') AS ChgToTm,
		    ISNULL(CONVERT(VARCHAR,CONVERT(BIGINT,BookedProduct.BpdOddometerOut)),'0')	AS OddOut, 
			ISNULL(CONVERT(VARCHAR,CONVERT(BIGINT,BookedProduct.BpdOddometerIn)),'0') AS OddIn, 
			ISNULL(CONVERT(VARCHAR,BookedProduct.BpdHirePeriod ),'') +  ' ' + ISNULL((SELECT CodCode FROM Code with (nolock) WHERE CodId = BookedProduct.BpdCodUOMId),'') AS UomId, 
		      CASE ISNULL(BookedProduct.BpdIsCurr,0)
				WHEN 1 THEN 'Current'
				ELSE 'Not Current'
			END + ' '  + CASE ISNULL(BookedProduct.BpdIsOriginal,0)
						WHEN 1 THEN 'Original'
						ELSE 'Not Original'
						END  +  CASE 
									WHEN BpdCodTypeId IS NOT NULL OR LEN(BpdCodTypeId)>0 THEN ', ' 
									ELSE ''
								 END +
					CASE 
						WHEN BpdCodTypeId IS NOT NULL OR LEN(BpdCodTypeId)>0 
							THEN dbo.GEN_GetCodDesc(BpdCodTypeId,NULL,NULL)
							ELSE ''
					END 							AS CurrOrg , 
			ISNULL(BookedProduct.BpdIsCusCharge,0)  AS Cus,
			CASE 
				WHEN /*BpdStatus IN ('XX', 'XN') OR*/ (dbo.getCodCode(BpdCodTypeId) IN ('XFRM', 'XTO') AND ISNULL(BpdIsCusCharge,0) = 1) THEN 0
				ELSE 1
			END										AS	CusMod ,
			ISNULL(BookedProduct.BpdThirdPartyReference,'') AS BooRef ,
			ISNULL(CONVERT(VARCHAR(10),BookedProduct.BpdRntXfrdToFinanWhen,103),'') +  
				CASE ISNULL(BpdSSBatchNo,'')
					WHEN '' 
						THEN ''
						ELSE ', ' 
				END + 
				ISNULL(BpdSSBatchNo,'') 		AS Trans, 
			ISNULL(BookedProduct.BpdIsFoc,0) 	AS	Foc, 
			ISNULL(BookedProduct.BpdApplyOrigRates,0)  AS AppOrgRates, 
			ISNULL((CONVERT(VARCHAR(10),BookedProduct.BpdCancellationWhen,103)  + ' ' + LEFT((CONVERT(VARCHAR(10),BookedProduct.BpdCancellationWhen,108)),5)),'')  AS CancelWhen ,
			'' + CONVERT(VARCHAR,ISNULL(Product.prdIsVehicle,0))							AS	IsVehicle ,
			'' + ISNULL(@IsXFRM, '0') 	AS IsXFRM,
			ISNULL(@bDispCancelBtn,0)	AS	DispCancel ,
			ISNULL(@bDispReclChg,0)		AS	DispReclChg ,
			ISNULL(@bShowCkoL,0)		AS	ShowCol ,
			ISNULL(@sAimsAssetType,'')	AS	AimsAssetType ,
			ISNULL(@bShowCkoL,0) 		AS 	CkoL,
			ISNULL(@bShowCkiL,0)		AS 	CkiL ,
			ISNULL(@bShowCkoD,0)		AS 	CkoD ,
			ISNULL(@bShowCkiD,0)		AS 	CkiD ,
			CASE 
				WHEN ISNULL(Product.PrdIsVehicle,0) = '1' 
						AND Exists(Select rntid From Rental with (nolock) Where RntId = BookedProduct.BpdRntId And RntStatus IN('CO','CI','XX'))
						THEN '1'
				ELSE '0'
			END 						AS OddoD,
			ISNULL(@bBkgRef,0)			AS BkgRef	,
			ISNULL(@bApplyDisc,0)		AS AppDisc ,
			ISNULL(BpdIsCusCharge,0)	AS OrigCusCharge ,
			ISNULL(BpdStatus,'')		AS OldRntStatus ,
			isnull(Rental.IntegrityNo,0) AS IntNo ,
			CASE 
				WHEN isnull(BpdExtRef,0) = 0 THEN BooNum + '/' + RntNum 
				ELSE BooNum + '/' + RntNum  + 'x' + BpdExtRef
			END							AS ExtRef ,
			ISNULL(CONVERT(VARCHAR(10),BookedProduct.BpdCkoWhen, 103) + ' ' + LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdCkoWhen, 108), 5),'') AS OldCkoWhen ,
			ISNULL(CONVERT(VARCHAR(10),BookedProduct.BpdCkiWhen, 103) + ' ' + LEFT(CONVERT(VARCHAR(10), BookedProduct.BpdCkiWhen, 108), 5),'') AS OldCkiWhen ,
			ISNULL(BookedProduct.BpdCkoLocCode,'')		AS	OldCkoLocCode ,
			ISNULL(BookedProduct.BpdCkiLocCode,'')		AS	OldCkiLocCode ,
			@bNewModifiable								AS	NewModStatus,
			-- Issue 879: RKS : 18-MAY-2006
			-- START
			'' + RntStatus									AS  Status,
			@sRnhId											AS	RnhId,
			ISNULL(@sXFRDTpFin,'')						AS TrToFin		-- added : RKS 21 SEP 2005 Issue  - 810
			-- END
		FROM         
			BookedProduct (nolock)  INNER JOIN
		    SaleableProduct (nolock)  ON BookedProduct.BpdSapId = SaleableProduct.SapId INNER JOIN
		    Product (nolock)  ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN
		    Brand (nolock) ON Product.PrdBrdCode = Brand.BrdCode INNER JOIN
			Rental on Rntid = Bpdrntid inner join
			booking on booid = rntbooid				
		WHERE BpdId = @sBpdId  -- '202F9956-211D-458B-B425-F1CD3C51F706'
		FOR XML AUTO, ELEMENTS

		--==========>> For the Booked Product Charges <<=======================							
		exec tab_getChargeSummary @sBpdId
END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON


