set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

      ALTER   PROCEDURE [dbo].[REs_getBookingDetail]  
 @sBooId  VARCHAR  (64)  = '',  
 @sUsrCode VARCHAR  (64) = ''  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE  @sUsrId   VARCHAR  (64) ,  
    @sUviKey  VARCHAR  (64) ,  
    @bAgnIsDirect BIT  
  
 SELECT  
  @sUsrId    =  UsrId ,  
  @sUviKey = UPPER(UsrCtyCode) + 'AGDIRSUPROLE'  
 FROM  
  dbo.UserInfo (nolock)  
 WHERE  
  UsrCode = @sUsrCode  
  
    
 SELECT   
  @bAgnIsDirect = ISNULL(AtyIsDirect,0)  
 FROM   
  dbo.Agent (NOLOCK),   
  dbo.AgentType (NOLOCK) ,  
  dbo.Booking (NOLOCK)  
 WHERE   
  AgnAtyId = AtyId   
  AND AgnId = BooAgnId  
  AND BooId = @sBooId  
    
  
 SELECT       
   ISNULL(Booking.BooId,'')     AS BooId ,  
   ISNULL(Booking.BooFirstName,'')   AS FName,   
   ISNULL(Booking.BooLastName,'')   AS LName,   
   ISNULL(Booking.BooTitle,'')     AS Title,   
   ISNULL(Booking.BooAgnId,'')   AS AgnId ,  
   ISNULL(Booking.BooAgnId,'')   AS OLD_AgnId ,  
   ISNULL(AgnIsMisc,0)     AS AgnIsMisc,  
   CASE ISNULL(AgnIsMisc,0)  
   WHEN 0 THEN Agent.AgnCode + ' - ' +  Agent.AgnName +  CASE ISNULL(Agent.AgnIsVip,0)   
            WHEN 0 THEN ''  
            ELSE ' - '   
           END + CASE ISNULL(Agent.AgnIsVip,0)  
             WHEN 0  THEN ''  
             ELSE 'VIP'  
            END  
   ELSE Agent.AgnCode + ' - ' + BooMiscAgentName  
         END     AS  AgnDet,   
   ISNULL(dbo.GEN_GetCodDesc('',42,BooStatus),'')     AS  ST,   
   ISNULL(CONVERT(VARCHAR,Booking.BooEarliestRentalCkoDate,103),'')  AS  CkoDt,   
   ISNULL(CONVERT(VARCHAR,Booking.BooLatestRentalCkiDate,103),'')  AS  CkiDt,   
   ISNULL(CONVERT(VARCHAR,Booking.BooXfrdToFinanWhen,103),'')  AS  FinWhen ,  
   ISNULL(CONVERT(VARCHAR, Booking.BooCancelWhen,103),'')   AS  CanWhen,   
   ISNULL(Booking.BooOldBooNum,'')       AS  OldBooNum ,  
   ISNULL(BooCodReferralTypId,'')       AS  RefType ,  
   Booking.IntegrityNo          AS  IntNo ,  
   CASE   
    WHEN (@bAgnIsDirect = 1 AND dbo.GEN_checkUserRole(@sUsrId,NULL,@sUviKey) = 1) THEN 0  
    WHEN @bAgnIsDirect = 0  THEN 0  
    ELSE 1  
   END   AS AgnIsDirect  ,
	--Added by Shoel on 6/11/7 > for the Integrity number issue
	'' + CAST(Rental.IntegrityNo AS VARCHAR) AS RntIntNo,
    '' + CAST(Booking.IntegrityNo AS VARCHAR) AS BooIntNo,

	--JL 2008/07/30 return booNum
	BooNum as BooNum
            
  FROM         
		Booking WITH (NOLOCK) INNER JOIN                      
		Agent WITH (NOLOCK) 
		ON Booking.BooAgnId = Agent.AgnId  
		INNER JOIN Rental WITH (NOLOCK)
		ON RntBooId = Booking.BooId
  WHERE  
   BooId = @sBooId  
  FOR XML AUTO, ELEMENTS   
END  

