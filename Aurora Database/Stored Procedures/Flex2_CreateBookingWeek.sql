set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROCEDURE [dbo].[Flex2_CreateBookingWeek] 
	@ComCode varchar(64),
	@BookStart datetime,
	@TravelYearStart datetime,
	@TravelYearEnd datetime,
	@UsrId varchar(64),
	@PrgmName varchar(64)

AS

/* ===========================================================================*/
/* Calculate Previous Booking Week Id */
/* ===========================================================================*/

PRINT 'Calculate Previous Booking Week Id'

DECLARE @PrevFbwId AS int

SELECT TOP 1
	@PrevFbwId = PrevFlexBookingWeek.FbwId
FROM 
	FlexBookingWeek AS PrevFlexBookingWeek
WHERE
	PrevFlexBookingWeek.FbwBookStart < @BookStart
	AND PrevFlexBookingWeek.FbwComCode = @ComCode
ORDER BY 
	PrevFlexBookingWeek.FbwBookStart DESC

/* ===========================================================================*/
/* Create Booking Week */
/* ===========================================================================*/

PRINT 'Create Booking Week'

INSERT INTO FlexBookingWeek
(
	FbwComCode,
	FbwBookStart,
	FbwTravelYearStart,
	FbwTravelYearEnd,
	FbwVersion,
	FbwStatus,
	FbwEmailText,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT
	Params.ComCode AS FbwComCode,
	Params.BookStart AS FbwBookStart,
	Params.TravelYearStart AS FbwTravelYearStart,
	Params.TravelYearEnd AS FbwTravelYearEnd,
	0 AS FbwVersion,
	'created' AS FbwStatus,
	ISNULL (PrevFlexBookingWeek.FbwEmailText, NULL) AS FbwEmailText,
	1 AS IntegrityNo,
	Params.UsrId AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	Params.PrgmName AS AddPrgmName
FROM
	(SELECT 	
		@ComCode AS ComCode,
		@BookStart AS BookStart,
		@TravelYearStart AS TravelYearStart,
		@TravelYearEnd AS TravelYearEnd,
		@UsrId AS UsrId,
		@PrgmName AS PrgmName
	) Params
	LEFT JOIN FlexBookingWeek AS PrevFlexBookingWeek ON PrevFlexBookingWeek.FbwId = @PrevFbwId
	LEFT JOIN FlexBookingWeek ON FlexBookingWeek.FbwBookStart = Params.BookStart AND FlexBookingWeek.FbwComCode = Params.ComCode
WHERE
	DATENAME (weekday, Params.BookStart) = 'Monday'
	AND FlexBookingWeek.FbwId IS NULL

DECLARE @FbwId AS int
SET @FbwId = SCOPE_IDENTITY()

IF @FbwId IS NULL RETURN

/* ===========================================================================*/
/* Create Products */
/* ===========================================================================*/

PRINT 'Create Products'

INSERT INTO FlexBookingWeekProduct
(
	FlpFbwId,
	FlpComCode,
	FlpCtyCode,
	FlpBrdCode,
	FlpTravelYear,
	FlpPrdId,
	FlpStatus,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
) 
SELECT 
	Params.FbwId AS FlpFbwId,
	FlexProductsView.ComCode AS FlpComCode,
	Country.CtyCode AS FlpCtyCode,
	FlexProductsView.BrdCode AS FlpBrdCode,
	FlexTravelYearsView.TravelYear AS FlpTravelYear,
	FlexProductsView.PrdId AS FlpPrdId,
	'unapproved' AS FlpStatus,
	1 AS IntegrityNo,
	Params.UsrId AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	Params.PrgmName AS AddPrgmName
FROM 
	(SELECT 	
		@FbwId AS FbwId,
		@UsrId AS UsrId,
		@PrgmName AS PrgmName
	) Params
	INNER JOIN FlexBookingWeek
		ON FlexBookingWeek.FbwId = Params.FbwId
	INNER JOIN FlexTravelYearsView
		ON FlexTravelYearsView.TravelYear BETWEEN FlexBookingWeek.FbwTravelYearStart AND FlexBookingWeek.FbwTravelYearEnd
	INNER JOIN FlexProductsView 
		ON FlexProductsView.ComCode = FlexBookingWeek.FbwComCode
		AND FlexProductsView.IsActive = 1
	INNER JOIN Country
		ON (Country.CtyHasProducts = 1)
	INNER JOIN FlexProduct
		ON FlexProduct.FxpComCode = FlexProductsView.ComCode
		AND FlexProduct.FxpCtyCode = Country.CtyCode
		AND FlexProduct.FxpBrdCode = FlexProductsView.BrdCode
		AND FlexProduct.FxpPrdId = FlexProductsView.PrdId
		AND FlexProduct.FxpParentId IS NULL
		AND FlexProduct.FxpIsActive = 1
WHERE
	/* make sure flex rates are specified for this product too */
	EXISTS 
	(
		SELECT 1 
		FROM 
			FlexPackageView
		WHERE
			FlexPackageView.PrdId = FlexProductsView.PrdId 
			AND FlexPackageView.CtyCode = Country.CtyCode  	
			AND FlexPackageView.IsActive = 1 
			AND FlexPackageView.BrdCode = FlexProductsView.BrdCode
			AND FlexPackageView.PkgBookedFromDate < DATEADD (d, 7, FlexBookingWeek.FbwBookStart)
			AND FlexPackageView.PkgBookedToDate >= FlexBookingWeek.FbwBookStart
			AND FlexPackageView.PkgTravelFromDate < DATEADD (y, 1, FlexTravelYearsView.TravelYear)
			AND FlexPackageView.PkgTravelToDate >= FlexTravelYearsView.TravelYear
	)

/* ===========================================================================*/
/* Create Product Rates (from previous week, or else default to A1) */
/* ===========================================================================*/

PRINT 'Create Product Rates'

INSERT INTO FlexBookingWeekRate
(
	FlrFlpId,
	FlrWkNo,
	FlrTravelFrom,
	FlrFlexNum,
	FlrChanged
)
SELECT 
	FlexBookingWeekProduct.FlpId AS FlrFlpId,
	FlexWeekNumber.FwnWkNo AS FlrWkNo,
	FlexWeekNumber.FwnTrvWkStart AS FlrTravelFrom,
	CASE WHEN PrevFlexBookingWeekRate.FlrId IS NOT NULL THEN PrevFlexBookingWeekRate.FlrFlexNum ELSE 11 END AS FlrFlexNum,
	CASE WHEN PrevFlexBookingWeekRate.FlrId IS NOT NULL THEN 0 ELSE 1 END AS FlrChanged
FROM 
	(SELECT 	
		@FbwId AS FbwId,
		@PrevFbwId AS PrevFbwId,
		@UsrId AS UsrId,
		@PrgmName AS PrgmName
	) Params
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = Params.FbwId
	INNER JOIN FlexWeekNumber
		ON FlexWeekNumber.FwnTrvYearStart = FlexBookingWeekProduct.FlpTravelYear
	LEFT JOIN FlexBookingWeekProduct AS PrevFlexBookingWeekProduct
		ON PrevFlexBookingWeekProduct.FlpFbwId = @PrevFbwId
		AND PrevFlexBookingWeekProduct.FlpComCode = FlexBookingWeekProduct.FlpComCode
		AND PrevFlexBookingWeekProduct.FlpCtyCode = FlexBookingWeekProduct.FlpCtyCode
		AND PrevFlexBookingWeekProduct.FlpBrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND PrevFlexBookingWeekProduct.FlpTravelYear = FlexBookingWeekProduct.FlpTravelYear
		AND PrevFlexBookingWeekProduct.FlpPrdId = FlexBookingWeekProduct.FlpPrdId  
	LEFT JOIN FlexBookingWeekRate AS PrevFlexBookingWeekRate
		ON PrevFlexBookingWeekRate.FlrFlpId = PrevFlexBookingWeekProduct.FlpId
		AND PrevFlexBookingWeekRate.FlrWkNo = FlexWeekNumber.FwnWkNo
ORDER BY
	FlexBookingWeekProduct.FlpId,
	FlexWeekNumber.FwnWkNo

/* ===========================================================================*/
/* Create Exceptions (from previous week) */
/* ===========================================================================*/

PRINT 'Create Exceptions'

INSERT INTO FlexBookingWeekException
(
	FleFlpId,
	FleBookStart,
	FleBookEnd,
	FleTravelStart,
	FleTravelEnd,
	FleLocCodeFrom,
	FleLocCodeTo,
	FleDelta
)
SELECT 
	FlexBookingWeekProduct.FlpId AS FleFlpId,
	PrevFlexBookingWeekException.FleBookStart AS FleBookStart,  
	PrevFlexBookingWeekException.FleBookEnd AS FleBookEnd,    
	PrevFlexBookingWeekException.FleTravelStart AS FleTravelStart,
	PrevFlexBookingWeekException.FleTravelEnd AS FleTravelEnd,  
	PrevFlexBookingWeekException.FleLocCodeFrom AS FleLocCodeFrom,
	PrevFlexBookingWeekException.FleLocCodeTo AS FleLocCodeTo,  
	PrevFlexBookingWeekException.FleDelta AS FleDelta
FROM 
	(SELECT 	
		@FbwId AS FbwId,
		@PrevFbwId AS PrevFbwId,
		@UsrId AS UsrId,
		@PrgmName AS PrgmName
	) Params
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = Params.FbwId
	INNER JOIN FlexBookingWeekProduct AS PrevFlexBookingWeekProduct
		ON PrevFlexBookingWeekProduct.FlpFbwId = Params.PrevFbwId
		AND PrevFlexBookingWeekProduct.FlpComCode = FlexBookingWeekProduct.FlpComCode
		AND PrevFlexBookingWeekProduct.FlpCtyCode = FlexBookingWeekProduct.FlpCtyCode
		AND PrevFlexBookingWeekProduct.FlpBrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND PrevFlexBookingWeekProduct.FlpTravelYear = FlexBookingWeekProduct.FlpTravelYear
		AND PrevFlexBookingWeekProduct.FlpPrdId = FlexBookingWeekProduct.FlpPrdId  
	INNER JOIN FlexBookingWeekException AS PrevFlexBookingWeekException
		ON PrevFlexBookingWeekException.FleFlpId = PrevFlexBookingWeekProduct.FlpId
ORDER BY 
	FlexBookingWeekProduct.FlpId,
	PrevFlexBookingWeekException.FleLocCodeFrom,
	PrevFlexBookingWeekException.FleLocCodeTo


/* ===========================================================================*/
/* Create Exports */
/* ===========================================================================*/

PRINT 'Create Exports'

INSERT INTO FlexBookingWeekExport
(
	FwxFbwId,
	FwxFlxId,
	FwxStatus,
	FwxTravelYear,
	FwxScheduledTime,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT 
	FlexBookingWeek.FbwId AS FwxFbwId,
	FlexExportType.FlxId AS FwxFlxId,
    'created' AS FwxStatus, 
	FlexTravelYearsView.TravelYear AS FwxTravelYear,
	DATEADD (hh, -FlexExportType.FlxScheduleHoursBefore, FlexBookingWeek.FbwBookStart) AS FwxScheduledTime,
	1 AS IntegrityNo,
	@UsrId AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	@PrgmName AS AddPrgmName
FROM 
	FlexBookingWeek
	INNER JOIN FlexExportType 
		ON (1=1)
	LEFT JOIN FlexTravelYearsView
		ON FlexTravelYearsView.TravelYear BETWEEN FlexBookingWeek.FbwTravelYearStart AND FlexBookingWeek.FbwTravelYearEnd
		AND FlexExportType.FlxPerTravelYear = 1
WHERE
	FlexBookingWeek.FbwId = @FbwId
	AND FlexExportType.FlxIsActive = 1
	-- only make an export type if valid products exist within that travel year, or overall if not per year
	AND EXISTS
	(
		SELECT 1 
		FROM FlexBookingWeekProduct 
		WHERE FlexBookingWeekProduct.FlpFbwId = @FbwId
		AND 
		(
			FlexExportType.FlxPerTravelYear = 0
			OR FlexBookingWeekProduct.FlpTravelYear = FlexTravelYearsView.TravelYear 
		) 
	)
ORDER BY
	FlexExportType.FlxCode,
	FlexTravelYearsView.TravelYear


/* ===========================================================================*/
/* Return Booking Week */
/* ===========================================================================*/

PRINT 'Return Booking Week'

SELECT 
	* 
FROM
	FlexBookingWeek
WHERE
	FbwId = @FbwId


