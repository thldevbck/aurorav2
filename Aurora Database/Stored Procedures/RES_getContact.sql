set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




/*
	Copyright (C) Tourism Holdings Limited as an unpublished work. All rights reserved.

	STORED PROCEDURE:		RES_getContact

	This stored procedure gets all the records in the feature table mainely used for Vehicle request Screen 

	The SP is passed:
		@parentTable		Parent Table name in Contact table
		@parentId		Parent Id
		@contactType		Contact types
		@searcgField		on which record need to be searched

	The SP returns:
		None
*/
ALTER      PROCEDURE [dbo].[RES_getContact]
	@parentTable		VARCHAR(64)	= '',
	@parentId		VARCHAR(64)	= '',
	@contactType		VARCHAR(64)	= '',
	@searcgField		VARCHAR(64)	= ''
AS

--	LOCAL VARIABLES

--========================================
--	START PROCESSING SECTION
--========================================

SELECT 	ConId,
	ConName,
	ConFirstName,
	ConLastName,
	ConTitle,
	ConPosition,
	ConLastName + ', ' + ConFirstName as 'ConName'
	FROM Contact WITH(NOLOCK)
	WHERE 	Contact.ConPrntTableName = @parentTable
	AND	Contact.ConPrntId = @parentId
	AND	Contact.ConCodContactId IN (SELECT CodId FROM Code WITH(NOLOCK)
							WHERE 	CodCdtNum = 25
							AND	CodCode = @contactType
							)
	AND	Contact.ConLastName LIKE @searcgField + '%'
	ORDER BY ConLastName
	FOR XML AUTO,ELEMENTS

--========================================
--	END PROCESSING SECTION
--========================================

--	**************************************** End RES_getContact ****************************************


