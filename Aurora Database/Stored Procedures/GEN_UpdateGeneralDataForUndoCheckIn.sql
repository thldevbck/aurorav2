CREATE  PROCEDURE [dbo].[GEN_UpdateGeneralDataForUndoCheckIn]
	@BookingNumber		VARCHAR(24),
	@usrCode		VARCHAR(24)
AS

SET NOCOUNT ON
DECLARE		@sType				VARCHAR(24),
		@FleetAssetId			VARCHAR(64),
		@BooNum				VARCHAR(24),
		@RntNum				VARCHAR(3),
		@sRntId				VARCHAR(64),
		@sBooId				VARCHAR(64)


if @usrcode = ''
   set @usrcode = 'rs3'

set @sType = 'UNDOCHECKIN'

	------------------------------------------------------------------------------------------
	-- start: REV:MIA April 22, 2008 - Add new trapping here for invalid booking rental format
	------------------------------------------------------------------------------------------

	-- checking for empty booking reference
	if @BookingNumber = ''
	   begin
			SELECT 'ERROR/Please enter Booking Reference Number!'
			GOTO Error
	   end

	-- checking for forward slash
	if patindex('%/%',@BookingNumber) = 0
	   begin
			SELECT 'ERROR/Booking Ref is not in correct format. i.e. 3000140/1 format.'
			GOTO Error
	   end
	------------------------------------------------------------------------------------------
	-- end: REV:MIA April 22, 2008 - Add new trapping here for invalid booking rental format
	------------------------------------------------------------------------------------------


	-- split the @BookingNumber into two parts, one is the BoookingNumber and the rest will be the RentalNumber
	SET @BooNum = dbo.getSplitedData(@BookingNumber, '/', 1)
	SET @RntNum = dbo.getSplitedData(@BookingNumber, '/', 2)


	IF NOT EXISTS(SELECT BooId
					FROM	Booking WITH(NOLOCK)
					INNER JOIN Rental WITH(NOLOCK) ON BooId = RntBooId
					INNER JOIN dbo.Package (NOLOCK) ON RntPkgId = PkgId
					WHERE	BooNum = @BooNum
					AND		RntNum = @RntNum
					AND PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK)
										INNER JOIN dbo.Company ON Brand.BrdComCode = Company.ComCode
										INNER JOIN dbo.UserCompany ON UserCompany.ComCode = Company.ComCode
										INNER JOIN dbo.UserInfo ON UserCompany.UsrId = UserInfo.UsrId
										AND UsrCode = @usrCode)
			)
					BEGIN
						SELECT 'ERROR/Booking Ref "' + @BookingNumber + '" is not a valid Booking'
						GOTO Error
					END



	SELECT 	@sRntId = RntId,
		@sBooId = BooId
			FROM	Booking WITH(NOLOCK) INNER JOIN Rental WITH(NOLOCK) ON BooId = RntBooId
				WHERE 	BooNum = @BooNum
				AND	RntNum = @RntNum

		--select @sRntId, @sBooId

	IF 	@sType = 'UNDOCHECKIN' AND
		NOT EXISTS(SELECT RntId
					FROM	Rental WITH(NOLOCK)
					WHERE	RntId = @sRntId
					AND		RntStatus = 'CI')
						BEGIN
							SELECT 'ERROR/Booking Ref "' + @BookingNumber + '" is not Checked In'
							GOTO Error
						END


	IF @sType = 'UNDOCHECKIN'

		BEGIN
			UPDATE Booking WITH(ROWLOCK) SET BooStatus = 'CO' WHERE	(BooNum = @BooNum and BooStatus = 'CI')

			UPDATE Rental WITH(ROWLOCK) SET RntStatus = 'CO' WHERE 	RntId = @sRntId AND	RntBooId = @sBooId

	                UPDATE BookedProduct WITH(ROWLOCK)  SET BpdOddometerIn = NULL   WHERE BpdRntId = @sRntId   AND  BpdStatus NOT IN ('XX','XN') AND  BpdPrdIsVehicle = 1

			SELECT top 1 @FleetAssetId = ActivityId FROM  AIMSPROD.dbo.FleetActivity  WHERE
					(ExternalRef LIKE @BooNum + '/' + @RntNum +  'x%' OR  ExternalRef = @BooNum + '/' + @RntNum ) ORDER BY StartDateTime DESC


			UPDATE	AIMSPROD.dbo.FleetAsset WITH(ROWLOCK) SET LastActivity = NULL WHERE	FleetAssetId = @FleetAssetId

			UPDATE AIMSPROD.dbo.FleetActivity WITH(ROWLOCK) SET COMPLETED = 0 WHERE ActivityId = @FleetAssetId

		END


	EXECUTE RES_checkRentalHistory
		@sBooId 		= @sBooId,
		@sRntId 		= @sRntId,
		@sBpdId			= NULL,
		@sChangeEvent		= @sType,
		@sDataType 		= 'RNT',
		@sUserCode 		= @usrCode,
		@sAddPrgmName 		= 'UNDOCHECKIN.ASPX'
	GOTO SUCCESS




SUCCESS:
	SELECT 'SUCCESS/' + DBO.getErrorString('GEN046', NULL, NULL, NULL, NULL, NULL, NULL)
	RETURN

ERROR:
	RETURN






GO
