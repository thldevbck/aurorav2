USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetFunctionCodes]    Script Date: 11/19/2007 13:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableEditor_GetFunctionCodes] 
AS

SELECT DISTINCT 
	FunCode AS Value, 
	FunCode + ' - ' + FunTitle AS Text 
FROM 
	Functions 
ORDER BY 
	FunCode