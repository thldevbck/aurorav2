set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go




ALTER    PROCEDURE [dbo].[sp_getConfirmationText] 
	@DefaultUser	AS	VARCHAR	 (64)	=	'',
	@bActive		AS	BIT				=	1,
	@CtyCode		AS	VARCHAR	 (64)	=	NULL,
	@NtsId			as  VARCHAR	 (64)	=	NULL
AS
BEGIN
	SET NOCOUNT ON
--	DECLARE		@CtyCode		VARCHAR	 (64)

/*	SELECT
		@CtyCode = CtyCode 
	FROM
		Country,
		UserInfo
	WHERE
		UsrCtyCode = CtyCode
		AND UsrCode = @DefaultUser
*/	


	IF IsNull(@CtyCode,'')=''
		SELECT
			@CtyCode = CtyCode 
		FROM
			Country WITH (NOLOCK) ,
			UserInfo WITH (NOLOCK) 
		WHERE
			UsrCtyCode = CtyCode
			AND UsrCode = @DefaultUser

	-- KX Changes :RKS
	DECLARE @sComCode VARCHAR(12)
	IF @DefaultUser<>''
		SELECT @sComCode = Company.ComCode FROM dbo.Company (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON Company.ComCode = UserCompany.ComCode
		INNER JOIN dbo.UserInfo (NOLOCK) ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @DefaultUser
	ELSE
		SET @sComCode = ''
	

	SELECT '<ConfText>'
	IF @bActive = 1
	BEGIN
			IF NOT EXISTS(
						SELECT 
							NtsId
		 				FROM 
							NoteSpec 
						WHERE 
							NtsIsActive = 1	AND NtsCtyCode = @CtyCode 
							-- KX Changes :RKS
							AND (@DefaultUser = '' OR NtsComCode = @sComCode)
							-- JL Changes : To have the ability to retrieve the NoteSpec by id 
							and (isnull(@NtsId, '') = '' or @NtsId = NtsId)
			)
				SELECT '<Conf><NtsId/><ConfActive>1</ConfActive><ConfType></ConfType><ConfText></ConfText><ConfHeader>1</ConfHeader><ConfFooter>0</ConfFooter><ConfOrder></ConfOrder><IsCus></IsCus><IsAgn></IsAgn></Conf>'
			ELSE
			BEGIN
					SELECT 
						NtsId						AS	NtsId ,
						ISNULL(NtsIsActive,0)		AS	ConfActive ,
						ISNULL(NtsType,'')			AS	ConfType ,
						ISNULL(NtsText,'')			AS	ConfText ,
						ISNULL(NtsIsHeader,0)		AS	ConfHeader,
						CASE ISNULL(NtsIsHeader,0)
						WHEN 0 THEN '1'
						ELSE '0'
						END							AS	ConfFooter,
						NtsOrder					AS	ConfOrder,
						IsNull(NtsIsDefCus,0)		AS IsCus,
						IsNull(NtsIsDefAgn,0)		AS IsAgn
			 		FROM 
						NoteSpec 					AS	Conf
					WHERE 
						NtsIsActive = 1	AND NtsCtyCode = @CtyCode
						-- KX Changes :RKS
						AND (@DefaultUser = '' OR NtsComCode = @sComCode)
						-- JL Changes : To have the ability to retrieve the NoteSpec by id 
						and (isnull(@NtsId, '') = '' or @NtsId = NtsId)
					ORDER BY NtsOrder, NtsType
					FOR XML AUTO, ELEMENTS
				SELECT '<Conf><NtsId/><ConfActive>1</ConfActive><ConfType></ConfType><ConfText></ConfText><ConfHeader>1</ConfHeader><ConfFooter>0</ConfFooter><ConfOrder></ConfOrder><IsCus></IsCus><IsAgn></IsAgn></Conf>'
			END
	END
	ELSE
	BEGIN
			IF NOT EXISTS(
						SELECT 
							NtsId
		 				FROM 
							NoteSpec 
						WHERE 
							NtsCtyCode = @CtyCode
							-- KX Changes :RKS
							AND (@DefaultUser = '' OR NtsComCode = @sComCode)
							-- JL Changes : To have the ability to retrieve the NoteSpec by id 
							and (isnull(@NtsId, '') = '' or @NtsId = NtsId)
			)
				SELECT '<Conf><NtsId/><ConfActive>1</ConfActive><ConfType></ConfType><ConfText></ConfText><ConfHeader>1</ConfHeader><ConfFooter>0</ConfFooter><ConfOrder></ConfOrder><IsCus></IsCus><IsAgn></IsAgn></Conf>'
			ELSE
			BEGIN
					SELECT 
						NtsId						AS	NtsId ,
						ISNULL(NtsIsActive,0)		AS	ConfActive ,
						ISNULL(NtsType,'')			AS	ConfType ,
						ISNULL(NtsText,'')			AS	ConfText ,
						ISNULL(NtsIsHeader,0)		AS	ConfHeader,
						CASE ISNULL(NtsIsHeader,0)
						WHEN 0 THEN '1'
						ELSE '0'
						END							AS	ConfFooter,
						NtsOrder					AS	ConfOrder,
						IsNull(NtsIsDefCus,0)		AS IsCus,
						IsNull(NtsIsDefAgn,0)		AS IsAgn
			 		FROM 
						NoteSpec 					AS	Conf
					WHERE 
						NtsCtyCode = @CtyCode
						-- KX Changes :RKS
						AND (@DefaultUser = '' OR NtsComCode = @sComCode)
						-- JL Changes : To have the ability to retrieve the NoteSpec by id 
						and (isnull(@NtsId, '') = '' or @NtsId = NtsId)
					ORDER BY NtsOrder, NtsType
					FOR XML AUTO, ELEMENTS
				SELECT '<Conf><NtsId/><ConfActive>1</ConfActive><ConfType></ConfType><ConfText></ConfText><ConfHeader>1</ConfHeader><ConfFooter>0</ConfFooter><ConfOrder></ConfOrder><IsCus></IsCus><IsAgn></IsAgn></Conf>'
			END

	END
	SELECT '</ConfText>'	
END
	
--sp_getConfirmationText 'TEST'


