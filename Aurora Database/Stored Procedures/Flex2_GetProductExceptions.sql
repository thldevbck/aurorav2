USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetProductExceptions]    Script Date: 11/19/2007 13:44:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Flex2_GetProductExceptions] 
	@FlpId AS int
AS

SELECT 
	*
FROM 
	FlexBookingWeekException
WHERE
	FlexBookingWeekException.FleFlpId = @FlpId
ORDER BY
	FlexBookingWeekException.FleId






