USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_GetFunctions]    Script Date: 11/19/2007 13:57:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TableEditor_GetFunctions] 
AS
	SELECT 
		TableEditorFunction.*
	FROM 
		TableEditorFunction
	ORDER BY
		TableEditorFunction.tefId


