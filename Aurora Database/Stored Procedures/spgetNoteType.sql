CREATE    PROCEDURE [dbo].[spgetNoteType]
 @sCodcdtnum         int  = default, /*Mandatory*/
 @sCode    VARCHAR(24) =''
AS
SET NOCOUNT ON
DECLARE @soryBy VARCHAR(60)

 /*Get the order of display*/
 SELECT @soryBy = CdtSortBy FROM CodeType WHERE CdtNum = @sCodcdtnum
 IF (@soryBy = NULL)
  SET @soryBy = ''

 IF (@soryBy = '' OR @soryBy = 'CodCode')
 BEGIN
  SELECT CodId    AS 'ID',
    CodCode  AS 'CODE',
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'
    FROM Code WITH (NOLOCK)
   WHERE Codcdtnum      = @sCodcdtnum
   AND  (CodCode LIKE @sCode + '%' OR
      CodDesc LIKE @sCode + '%')
   AND  CodIsActive = 1
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodCode
   FOR XML AUTO,ELEMENTS
  RETURN
 END

 IF (@soryBy = 'CodDesc' )
 BEGIN
  SELECT CodId    AS 'ID',
    CodCode  AS 'CODE',
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'
    FROM Code WITH (NOLOCK)
   WHERE Codcdtnum      = @sCodcdtnum
   AND  (CodCode LIKE @sCode + '%' OR
      CodDesc LIKE @sCode + '%')
   AND  CodIsActive = 1
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodDesc
   FOR XML AUTO,ELEMENTS
  RETURN
 END

 IF (@soryBy = 'CodOrder' )
 BEGIN
  SELECT CodId    AS 'ID',
    CodCode  AS 'CODE',
    ISNULL(CodDesc,'')  AS 'DESCRIPTION'
    FROM Code WITH (NOLOCK)
   WHERE Codcdtnum      = @sCodcdtnum
   AND  (CodCode LIKE @sCode + '%' OR
      CodDesc LIKE @sCode + '%')
   AND  CodIsActive = 1
   AND  ( CodDesc LIKE 'Booking%' OR CodDesc LIKE 'Rental%' OR CodDesc LIKE 'Product%' OR CodDesc LIKE 'Package%' OR CodDesc LIKE 'Credit%')  --REV:MIA add CodDesc LIKE 'Credit%'
   ORDER BY CodOrder
   FOR XML AUTO,ELEMENTS

  RETURN
 END
    RETURN




GO
