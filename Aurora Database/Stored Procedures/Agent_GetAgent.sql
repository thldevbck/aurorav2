CREATE PROCEDURE [dbo].[Agent_GetAgent]
(
	@ComCode AS varchar(64),
	@AgnId AS varchar(64)
)
AS

/*
DECLARE @AgnId AS varchar(64)
DECLARE @ComCode AS varchar(64)
SET @AgnId = '001B97D3-93D8-49F6-A0C1-8B85FBD5149C'
SET @ComCode = 'THL'
*/

DECLARE @Agent TABLE
(
	AgnId varchar(64)
)
INSERT INTO @Agent
(
	AgnId
)
SELECT
	Agent.AgnId AS AgnId
FROM
	Agent
WHERE
	Agent.AgnId = @AgnId
ORDER BY
	Agent.AgnCode

/*********************************************************************************************/
/* 0 - Agent */
/*********************************************************************************************/
SELECT
	Agent.*
FROM
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId

/*********************************************************************************************/
/* 1 - PhoneNumber */
/*********************************************************************************************/
SELECT
	PhoneNumber.*
FROM
	@Agent AS a
	INNER JOIN PhoneNumber ON PhoneNumber.PhnPrntId = a.AgnId
WHERE
	PhoneNumber.PhnPrntTableName = 'Agent'
UNION
SELECT
	PhoneNumber.*
FROM
	@Agent AS a
	INNER JOIN Contact ON Contact.ConPrntId = a.AgnId
	INNER JOIN PhoneNumber ON PhoneNumber.PhnPrntId = Contact.ConId
WHERE
	PhoneNumber.PhnPrntTableName = 'Contact'

/*********************************************************************************************/
/* 2 - AgentSubGroup */
/*********************************************************************************************/
SELECT
	AgentSubGroup.*
FROM
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId
	INNER JOIN AgentSubGroup ON Agent.AgnAsgId = AgentSubGroup.AsgId

/*********************************************************************************************/
/* 3 - AgentGroup */
/*********************************************************************************************/
SELECT
	AgentGroup.*
FROM
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId
	INNER JOIN AgentSubGroup ON Agent.AgnAsgId = AgentSubGroup.AsgId
	INNER JOIN AgentGroup ON AgentSubGroup.AsgAgpId = AgentGroup.AgpId

/*********************************************************************************************/
/* 4 - AddressDetails */
/*********************************************************************************************/
SELECT
	AddressDetails.*
FROM
	@Agent AS a
	INNER JOIN AddressDetails ON AddressDetails.AddPrntID = a.AgnId
	INNER JOIN Code ON AddressDetails.AddCodAddressId = Code.CodId
WHERE
	AddressDetails.AddPrntTableName = 'Agent'
	AND Code.CodCdtNum = 1
	AND Code.CodCode IN ('Billing', 'Physical', 'Postal')

/*********************************************************************************************/
/* 5 - Product */
/*********************************************************************************************/
SELECT DISTINCT
	Product.*
FROM
	@Agent AS a
	INNER JOIN AgentDiscount ON AgentDiscount.AdsAgnId = a.AgnId
	INNER JOIN Brand ON Brand.BrdCode = AgentDiscount.AdsBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
	INNER JOIN Product ON AgentDiscount.AdsPrdId = Product.PrdId
WHERE
	Company.ComCode = @ComCode

/*********************************************************************************************/
/* 6 - Note */
/*********************************************************************************************/
SELECT
	Note.*
FROM
	@Agent AS a
	INNER JOIN Note ON Note.NteAgnId = a.AgnId
WHERE
	Note.NteBooID IS NULL
ORDER BY
	Note.ModDateTime DESC,
	Note.AddDateTime DESC

/*********************************************************************************************/
/* 7 - Contact */
/*********************************************************************************************/
SELECT
	Contact.*
FROM
	@Agent AS a
	INNER JOIN Contact ON Contact.ConPrntId = a.AgnId
WHERE
	ConPrntTableName = 'Agent'
ORDER BY
	Contact.AddDateTime

/*********************************************************************************************/
/* 8 - FlexExportContact */
/*********************************************************************************************/
SELECT
	FlexExportContact.*
FROM
	@Agent AS a
	INNER JOIN FlexExportContact ON FlexExportContact.FxcAgnId = a.AgnId

/*********************************************************************************************/
/* 9 - AgentDiscount */
/*********************************************************************************************/
SELECT
	AgentDiscount.*
FROM
	@Agent AS a
	INNER JOIN AgentDiscount ON AgentDiscount.AdsAgnId = a.AgnId
	INNER JOIN Brand ON Brand.BrdCode = AgentDiscount.AdsBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
	INNER JOIN Product ON AgentDiscount.AdsPrdId = Product.PrdId
WHERE
	Company.ComCode = @ComCode
ORDER BY
	a.AgnId,
	Product.PrdShortName,
	Brand.BrdCode,
	AgentDiscount.AdsEffDate DESC

/*********************************************************************************************/
/* 10 - AgentCompany (for all companies) */
/*********************************************************************************************/
SELECT
	AgentCompany.*
FROM
	@Agent AS a
	INNER JOIN AgentCompany ON AgentCompany.AgcAgnId = a.AgnId
	INNER JOIN Company ON AgentCompany.AgcComCode = Company.ComCode
ORDER BY
	a.AgnId,
	Company.ComCode


GO
