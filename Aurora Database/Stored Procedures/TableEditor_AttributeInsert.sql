USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[TableEditor_AttributeInsert]    Script Date: 11/19/2007 13:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[TableEditor_AttributeInsert]
(
	@tefId AS int,
	@teeId AS int,
	@teaParentId AS int,
	@teaName AS nvarchar(64),
	@teaDescription AS ntext,
	@teaFieldName AS nvarchar(64),
	@teaIsPrimaryKey AS bit,
	@teaIsFilterable AS bit,
	@teaIsListable AS bit,
	@teaIsSortable AS bit,
	@teaListWidth AS nvarchar(12),
	@teaListAlign AS nvarchar(12),
	@teaIsViewable AS bit,
	@teaIsUpdateable AS bit,
	@teaIsInsertable AS bit,
	@teaIsNullable AS bit,
	@teaIsBoolean AS bit,
	@teaLookupSql AS ntext,
	@teaTextRowCount AS int,
	@teaTextWrap AS bit,
	@teaDefaultValue AS ntext,
	@teaOrder AS int
)
AS

INSERT INTO TableEditorAttribute
(
	tefId,
	teeId,
	teaParentId,
	teaName,
	teaDescription,
	teaFieldName,
	teaIsPrimaryKey,
	teaIsFilterable,
	teaIsListable,
	teaIsSortable,
	teaListWidth,
	teaListAlign,
	teaIsViewable,
	teaIsUpdateable,
	teaIsInsertable,
	teaIsNullable,
	teaIsBoolean,
	teaLookupSql,
	teaTextRowCount,
	teaTextWrap,
	teaDefaultValue,
	teaOrder 
)
VALUES
(
	@tefId,
	@teeId,
	@teaParentId,
	@teaName,
	@teaDescription,
	@teaFieldName,
	@teaIsPrimaryKey,
	@teaIsFilterable,
	@teaIsListable,
	@teaIsSortable,
	@teaListWidth,
	@teaListAlign,
	@teaIsViewable,
	@teaIsUpdateable,
	@teaIsInsertable,
	@teaIsNullable,
	@teaIsBoolean,
	@teaLookupSql,
	@teaTextRowCount,
	@teaTextWrap,
	@teaDefaultValue,
	@teaOrder
)

SELECT 
	TableEditorAttribute.* 
FROM 
	TableEditorAttribute 
WHERE
	TableEditorAttribute.teaId = SCOPE_IDENTITY()



