CREATE PROCEDURE [dbo].[OpsAndLogs_GetBlockingRuleLookups]
	@comCode AS varchar(64),
	@ctyCode AS varchar(2)
AS

SELECT
	Code.*
FROM
	Code
	INNER JOIN CodeType ON CodeType.CdtNum = Code.CodCdtNum
ORDER BY
	CodeType.CdtNum,
	Code.CodOrder,
	Code.CodDesc

SELECT
	Country.*
FROM
	Country
ORDER BY
	Country.CtyCode

SELECT
	Brand.*
FROM
	Brand
WHERE
	Brand.BrdComCode = @comCode
ORDER BY
	Brand.BrdCode

SELECT
	Location.*
FROM
	Location
	INNER JOIN TownCity ON Location.LocTctCode = TownCity.TctCode
WHERE
	Location.LocComCode = @comCode
	AND TownCity.TctCtyCode = @ctyCode
ORDER BY
	Location.LocCode

SELECT
	Product.*
FROM
	Product
	INNER JOIN Brand ON Product.PrdBrdCode = Brand.BrdCode
WHERE
	Product.PrdIsVehicle = 1
	AND Brand.BrdComCode = @comCode
ORDER BY
	Product.PrdShortName



GO
