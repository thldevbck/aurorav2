ALTER PROCEDURE [dbo].[Flex2_GetLocationsExport] 
	@ComCode varchar(64),         
	@FwxId AS int
AS

SELECT 
	Location.LocComCode	AS ComCode,
	TownCity.TctCtyCode AS CtyCode,
	Location.LocCode,
	Location.LocName
FROM 
	FlexWeekExportView
	INNER JOIN FlexWeekProductView
		ON FlexWeekProductView.FbwId = FlexWeekExportView.FbwId
		AND FlexWeekProductView.FlpStatus = 'approved'
		AND FlexWeekProductView.ComCode = FlexWeekExportView.FlxComCode
		AND (FlexWeekProductView.CtyCode = FlexWeekExportView.FlxCtyCode OR FlexWeekExportView.FlxCtyCode IS NULL)
		AND (FlexWeekProductView.BrdCode = FlexWeekExportView.FlxBrdCode OR FlexWeekExportView.FlxBrdCode IS NULL)
		AND (FlexWeekProductView.ClaID = FlexWeekExportView.FlxClaID OR FlexWeekExportView.FlxClaID IS NULL)
		AND (FlexWeekProductView.TypID = FlexWeekExportView.FlxTypID OR FlexWeekExportView.FlxTypID IS NULL)
		AND (FlexWeekProductView.PrdId = FlexWeekExportView.FlxPrdId OR FlexWeekExportView.FlxPrdId IS NULL)
		AND (FlexWeekProductView.TravelYear = FlexWeekExportView.FwxTravelYear OR FlexWeekExportView.FwxTravelYear IS NULL)
	INNER JOIN Location
		ON Location.LocComCode = FlexWeekProductView.ComCode
		AND Location.LocIsActive = 1
	INNER JOIN Code 
		ON Code.CodId = Location.LocCodTypId
		AND Code.CodCode = 'BRANCH'
	INNER JOIN TownCity 
		ON TownCity.TctCode = Location.LocTctCode 
		AND TownCity.TctCtyCode = FlexWeekProductView.CtyCode
WHERE
	FlexWeekExportView.FlxComCode = @ComCode
	AND FlexWeekExportView.FwxId = @FwxId

GROUP BY
	Location.LocComCode,
	TownCity.TctCtyCode,
	Location.LocCode,
	Location.LocName

ORDER BY 	
	TownCity.TctCtyCode,
	Location.LocName



