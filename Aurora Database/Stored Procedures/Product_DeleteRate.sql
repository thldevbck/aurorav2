CREATE PROCEDURE [dbo].[Product_DeleteRate]
(
	@PtbId AS varchar(64),
	@RateId AS varchar(64)
)
AS

-- Delete Location Dependent Rate children and discounts if we are deleteing a ProductRateBookingBand
DELETE ProductRateDiscount
FROM
	ProductRateBookingBand
	INNER JOIN LocationDependentRate ON ProductRateBookingBand.BapId = LocationDependentRate.LdrBapId
	INNER JOIN ProductRateDiscount ON LocationDependentRate.LdrId = ProductRateDiscount.PdsParentId
WHERE
	ProductRateBookingBand.BapPtbId = @PtbId
	AND ProductRateBookingBand.BapId = @RateId

DELETE LocationDependentRate
FROM
	ProductRateBookingBand
	INNER JOIN LocationDependentRate ON ProductRateBookingBand.BapId = LocationDependentRate.LdrBapId
WHERE
	ProductRateBookingBand.BapPtbId = @PtbId
	AND ProductRateBookingBand.BapId = @RateId

-- Kill Discount
DELETE ProductRateDiscount FROM ProductRateDiscount WHERE PdsPtbId = @PtbId AND PdsParentId = @RateId

-- Kill Rates
DELETE AgentDependentRate FROM AgentDependentRate WHERE AdrPtbId = @PtbId AND AdrId = @RateId
DELETE PersonRate FROM PersonRate WHERE PrrPtbId = @PtbId AND PrrId = @RateId
DELETE PersonDependentRate FROM PersonDependentRate WHERE PdrPtbId = @PtbId AND PdrId = @RateId

DELETE
	LocationDependentRate
FROM
	ProductRateBookingBand
	INNER JOIN LocationDependentRate ON ProductRateBookingBand.BapId = LocationDependentRate.LdrBapId
WHERE
	ProductRateBookingBand.BapPtbId = @PtbId
	AND LocationDependentRate.LdrId = @RateId

DELETE ProductRateBookingBand FROM ProductRateBookingBand WHERE BapPtbId = @PtbId AND BapId = @RateId
DELETE VehicleDependentRate FROM VehicleDependentRate WHERE VdrPtbId = @PtbId AND VdrPtbId = @RateId
DELETE FixedRate FROM FixedRate WHERE FfrPtbId = @PtbId AND FfrId = @RateId



GO
