set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go







ALTER PROCEDURE [dbo].[Agent_SearchAgent] 
(
	@AgnId AS varchar(64) = NULL,
	@AgnCode AS varchar(10) = NULL,
	@AgnName AS varchar(64) = NULL,
	@AgnAgpCode AS varchar(24) = NULL,
	@AgnAtyId AS varchar(64) = NULL,
	@AgnDebtStatId AS varchar(64) = NULL,
	@AgnPhnCountryCode AS varchar(6) = NULL,
	@AgnPhnAreaCode AS varchar(6) = NULL,
	@AgnPhnNumber AS varchar(24) = NULL,
	@AgnAddAddress1 AS varchar(64) = NULL,
	@AgnAddAddress2 AS varchar(64) = NULL,
	@AgnAddAddress3 AS varchar(64) = NULL,
	@AgnAddState AS varchar(24) = NULL,
	@AgnAddPostcode AS varchar(15) = NULL,
	@AgnAddCtyCode AS varchar(12) = NULL,
	@AgnIsActive AS bit = NULL,
	@AgnCodeExact AS varchar(10) = NULL,
	@AgnSoundEx AS bit = 0
)
AS

DECLARE @Agent TABLE 
(
	AgnId varchar(64)
)

/*********************************************************************************************/
/* Search */
/*********************************************************************************************/
INSERT INTO @Agent 
(
	AgnId
)
SELECT DISTINCT
	Agent.AgnId AS AgnId
FROM         
	Agent
WHERE
	(@AgnId IS NULL OR Agent.AgnId = @AgnId)
	AND (@AgnCode IS NULL OR Agent.AgnCode LIKE (@AgnCode + '%'))
	AND 
	(
		@AgnName IS NULL 
--		OR (@AgnSoundEx = 0 AND Agent.AgnName LIKE ('%' + @AgnName + '%'))
--		OR (@AgnSoundEx = 1 AND SOUNDEX(Agent.AgnName) = SOUNDEX(@AgnName))
		OR (@AgnSoundEx = 0 AND (Agent.AgnName LIKE ('%' + @AgnName + '%') OR Agent.AgnCode LIKE (@AgnName + '%')))
		OR (@AgnSoundEx = 1 AND (SOUNDEX(Agent.AgnName) = SOUNDEX(@AgnName) OR SOUNDEX(Agent.AgnCode) = SOUNDEX(@AgnName)))
	)

	AND (@AgnAgpCode IS NULL OR EXISTS
	(
		SELECT 
			1
		FROM 
			AgentSubGroup
			INNER JOIN AgentGroup ON AgentSubGroup.AsgAgpId = AgentGroup.AgpId
		WHERE
			AgentSubGroup.AsgId = Agent.AgnAsgId
			AND AgentGroup.AgpCode = @AgnAgpCode 
	))

	AND (@AgnAtyId IS NULL OR Agent.AgnAtyId = @AgnAtyId)
	AND (@AgnDebtStatId IS NULL OR Agent.AgnCodDebtStatId = @AgnDebtStatId)

	AND ((@AgnPhnCountryCode IS NULL AND @AgnPhnAreaCode IS NULL AND @AgnPhnNumber IS NULL) OR EXISTS
	(
		SELECT 
			1
		FROM         
			PhoneNumber 
			INNER JOIN Code ON PhoneNumber.PhnCodPhoneId = Code.CodId
		WHERE     
			PhoneNumber.PhnPrntId = Agent.AgnId 
			AND PhoneNumber.PhnPrntTableName = 'Agent'
			AND Code.CodCdtNum = 24
			AND Code.CodCode = 'Day'
			AND (@AgnPhnCountryCode IS NULL OR PhoneNumber.PhnCountryCode = @AgnPhnCountryCode) 
			AND (@AgnPhnAreaCode IS NULL OR PhoneNumber.PhnAreaCode = @AgnPhnAreaCode)			
			AND (@AgnPhnNumber IS NULL OR PhoneNumber.PhnNumber = @AgnPhnNumber)
	 ))

	AND ((@AgnAddAddress1 IS NULL AND @AgnAddAddress2 IS NULL AND @AgnAddAddress3 IS NULL AND @AgnAddState IS NULL AND @AgnAddPostcode IS NULL AND @AgnAddCtyCode IS NULL) OR EXISTS
	(
		SELECT 
			1
		FROM         
			AddressDetails 
			INNER JOIN Code ON AddressDetails.AddCodAddressId = Code.CodId
		WHERE     
			AddressDetails.AddPrntID = Agent.AgnId 
			AND AddressDetails.AddPrntTableName = 'Agent'
			AND Code.CodCdtNum = 1
			AND (Code.CodCode = 'Billing' OR Code.CodCode = 'Physical' OR Code.CodCode = 'Postal') 
			AND (@AgnAddAddress1 IS NULL OR AddressDetails.AddAddress1 LIKE (@AgnAddAddress1 + '%'))
			AND (@AgnAddAddress2 IS NULL OR AddressDetails.AddAddress2 LIKE (@AgnAddAddress2 + '%'))
			AND (@AgnAddAddress3 IS NULL OR AddressDetails.AddAddress3 LIKE (@AgnAddAddress3 + '%'))
			AND (@AgnAddState IS NULL OR AddressDetails.AddState LIKE (@AgnAddState + '%'))
			AND (@AgnAddPostcode IS NULL OR AddressDetails.AddPostcode LIKE (@AgnAddPostcode + '%'))
			AND (@AgnAddCtyCode IS NULL OR AddressDetails.AddCtyCode LIKE (@AgnAddCtyCode + '%'))
	 ))

	AND (@AgnIsActive IS NULL OR Agent.AgnIsActive = @AgnIsActive)
	AND (@AgnCodeExact IS NULL OR Agent.AgnCode = @AgnCodeExact)


/*********************************************************************************************/
/* 0 - Agent */
/*********************************************************************************************/
SELECT 
	Agent.*
FROM 
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId
ORDER BY
	Agent.AgnCode


/*********************************************************************************************/
/* 1 - AgentSubGroup */
/*********************************************************************************************/
SELECT DISTINCT
	AgentSubGroup.* 
FROM 
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId
	INNER JOIN AgentSubGroup ON Agent.AgnAsgId = AgentSubGroup.AsgId

/*********************************************************************************************/
/* 2 - AgentGroup */
/*********************************************************************************************/
SELECT DISTINCT 
	AgentGroup.* 
FROM 
	@Agent AS a
	INNER JOIN Agent ON Agent.AgnId = a.AgnId
	INNER JOIN AgentSubGroup ON Agent.AgnAsgId = AgentSubGroup.AsgId
	INNER JOIN AgentGroup ON AgentSubGroup.AsgAgpId = AgentGroup.AgpId

/*********************************************************************************************/
/* 3 - AddressDetails */
/*********************************************************************************************/
SELECT     
	AddressDetails.*
FROM   
	@Agent AS a      
	INNER JOIN AddressDetails ON AddressDetails.AddPrntID = a.AgnId
	INNER JOIN Code ON AddressDetails.AddCodAddressId = Code.CodId
WHERE     
	(AddressDetails.AddPrntTableName = 'Agent') 
	AND (Code.CodCdtNum = 1)
	AND (Code.CodCode = 'Billing')




