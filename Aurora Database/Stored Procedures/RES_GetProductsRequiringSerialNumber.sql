USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[RES_GetProductsRequiringSerialNumber]    Script Date: 05/20/2008 12:24:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[RES_GetProductsRequiringSerialNumber] --'3294372-1' , 1
@RentalId AS VARCHAR(64) ,
@ShowAll AS BIT
AS  
BEGIN  
 SET NOCOUNT ON     
  
 SELECT '<Products>'  
 
 IF @ShowAll = 0
 BEGIN 
 SELECT  BpdId,  
    '' + PrdShortName As PrdName,   
    '' + IsNull(BpdPlannedSerialCode,'') AS SerialNum   
    FROM BookedProduct Prd WITH (NOLOCK), product WITH (NOLOCK), SaleableProduct WITH (NOLOCK)  
  
 WHERE BpdSapId = SapId   
    AND SapPrdId = PrdId   
    AND BpdRntId = @RentalId   
    AND BpdStatus NOT IN ('XX','XN')   
    AND BpdIsCurr =1   
 AND IsNull(dbo.getProductAttributeValue(BpdsapId,'REQSERNUM'),0) = 1   
    AND IsNull(BpdPlannedSerialCode,'')=''  
  
 FOR XML AUTO  
 END
 ELSE
 BEGIN
	 SELECT  BpdId,  
    '' + PrdShortName As PrdName,   
    '' + IsNull(BpdPlannedSerialCode,'') AS SerialNum   
    FROM BookedProduct Prd WITH (NOLOCK), product WITH (NOLOCK), SaleableProduct WITH (NOLOCK)  
  
 WHERE BpdSapId = SapId   
    AND SapPrdId = PrdId   
    AND BpdRntId = @RentalId   
    AND BpdStatus NOT IN ('XX','XN')   
    AND BpdIsCurr =1   
  AND IsNull(dbo.getProductAttributeValue(BpdsapId,'REQSERNUM'),0) = 1   
     FOR XML AUTO  
 END
  
 SELECT '</Products>'  
  
END  

