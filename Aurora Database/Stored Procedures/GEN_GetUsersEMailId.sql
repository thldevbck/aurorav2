CREATE PROC GEN_GetUsersEMailId 
	@UserCode AS VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON
	SELECT     
		ISNULL(UsrEmail, '') AS EmailId
	FROM         
		UserInfo WITH (NOLOCK)
	WHERE     
		UsrCode = @UserCode
	FOR XML AUTO, ELEMENTS
END
