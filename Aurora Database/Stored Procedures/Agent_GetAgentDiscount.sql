USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Agent_GetAgentDiscount]    Script Date: 06/06/2008 17:10:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Agent_GetAgentDiscount] 
(
	@ComCode AS varchar(64),
	@AgnId AS varchar(64)
)
AS

/*
DECLARE @AgnId AS varchar(64)
DECLARE @ComCode AS varchar(64)
SET @AgnId = '001B97D3-93D8-49F6-A0C1-8B85FBD5149C' 
SET @ComCode = 'THL'
*/

DECLARE @Agent table 
(
	AgnId varchar(64)
)
INSERT INTO @Agent 
(
	AgnId
)
SELECT 
	Rtrim(Agent.AgnId) AS AgnId
FROM 
	Agent
WHERE
	Agent.AgnId = @AgnId
ORDER BY
	Agent.AgnCode

/*********************************************************************************************/
/* 0 - AgentDiscount */
/*********************************************************************************************/

-- Work out agent discount for the latest effective date of booked 
-- (AdsIsBookedDateType = 1) on each PrdId of specific AgnId
------------------------------------------------------------------
SELECT     
	AdsID, AdsAgnId, AdsPrdId, AdsChgAgent, AdsBrdCode, 
	ISNULL(CONVERT(VARCHAR(10), AdsEffDate, 103), '') AS AdsEffDate, 
	AgentDiscount.IntegrityNo, AgentDiscount.AddUsrId,       
	ISNULL(AdsIsBookedDateType, '0') as AdsIsBookedDateType,
	AdsCtyCode, AdsBookedFromWhen, AdsTravelFromWhen, AgentDiscount.ModUsrId, 
	ISNULL(AdsPercentage, 0) AS AdsPercentage, 
	AgentDiscount.AddDateTime, AgentDiscount.ModDateTime, AgentDiscount.AddPrgmName
INTO
	#AgentDiscountBooked
FROM         
	@Agent AS a
	INNER JOIN AgentDiscount ON AgentDiscount.AdsAgnId = a.AgnId
	INNER JOIN Brand ON Brand.BrdCode = AgentDiscount.AdsBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
	LEFT OUTER JOIN Product ON AgentDiscount.AdsPrdId = Product.PrdId
WHERE     
	(Product.PrdIsActive = 1)
	AND	Company.ComCode = @ComCode
	AND (AdsIsBookedDateType = 1) 
	
SELECT     
	adb1.*
INTO
	#AgentDiscountBookedMaxEffDate
FROM         
	#AgentDiscountBooked adb1
WHERE	
	adb1.AdsEffDate = (SELECT DISTINCT MAX(adb2.AdsEffDate) FROM #AgentDiscountBooked adb2 WHERE adb2.AdsPrdId = adb1.AdsPrdId)

DROP TABLE #AgentDiscountBooked

-- Work out agent discount for the latest effective date of travel 
-- (AdsIsBookedDateType = 0 OR AdsIsBookedDateType IS NULL) on
-- each PrdId of specific AgnId
------------------------------------------------------------------
SELECT     
	AdsID, AdsAgnId, AdsPrdId, AdsChgAgent, AdsBrdCode, 
	ISNULL(CONVERT(VARCHAR(10), AdsEffDate, 103), '') AS AdsEffDate, 
	AgentDiscount.IntegrityNo, AgentDiscount.AddUsrId,       
	ISNULL(AdsIsBookedDateType, '0') as AdsIsBookedDateType,
	AdsCtyCode, AdsBookedFromWhen, AdsTravelFromWhen, AgentDiscount.ModUsrId, 
	ISNULL(AdsPercentage, 0) AS AdsPercentage, 
	AgentDiscount.AddDateTime, AgentDiscount.ModDateTime, AgentDiscount.AddPrgmName
INTO
	#AgentDiscountTravel
FROM         
	@Agent AS a
	INNER JOIN AgentDiscount ON AgentDiscount.AdsAgnId = a.AgnId
	INNER JOIN Brand ON Brand.BrdCode = AgentDiscount.AdsBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
	LEFT OUTER JOIN Product ON AgentDiscount.AdsPrdId = Product.PrdId
WHERE     
	(Product.PrdIsActive = 1)
	AND	Company.ComCode = @ComCode
	AND (AdsIsBookedDateType = 0 OR AdsIsBookedDateType IS NULL) 

SELECT     
	adt1.*
INTO
	#AgentDiscountTravelMaxEffDate
FROM         
	#AgentDiscountTravel adt1
WHERE	
	adt1.AdsEffDate = (SELECT DISTINCT MAX(adt2.AdsEffDate) FROM #AgentDiscountTravel adt2 WHERE adt2.AdsPrdId = adt1.AdsPrdId)

DROP TABLE #AgentDiscountTravel

-- Union the two typed (booked and travel) agent discounts
----------------------------------------------------------
SELECT
	#AgentDiscountBookedMaxEffDate.*
FROM
	#AgentDiscountBookedMaxEffDate

UNION

SELECT
	#AgentDiscountTravelMaxEffDate.*
FROM
	#AgentDiscountTravelMaxEffDate

DROP TABLE #AgentDiscountBookedMaxEffDate
DROP TABLE #AgentDiscountTravelMaxEffDate