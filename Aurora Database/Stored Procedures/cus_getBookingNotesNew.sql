set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


--JL 2007-11-21 change the column name of the return result. 'Note' to 'NoteDesc'

alter            PROCEDURE [dbo].[cus_getBookingNotesNew]
	@BooId			VARCHAR	 (64)	=	'',
	@RntId			VARCHAR	 (64)	=	'',
	@bAllNote		BIT		=	1


AS
BEGIN
DEclare @BooCodId Varchar(36), @rntCodID  Varchar(36), @IcdCodID Varchar(36), @infCodID varchar(36)
	SELECT @BooCodId = codId from code WITH (nolock) where CodCdtNum =13 and CodCode = 'Booking'
	SELECT @rntCodID = codId from code WITH (nolock) where CodCdtNum =13 and CodCode = 'Rental'
	SELECT @IcdCodID = codId from code WITH (nolock) where CodCdtNum =13 and CodCode = 'Incident'
	SELECT @infCodID = codId from code WITH (nolock) where CodCdtNum =13 and CodCode = 'Infringement'
	

SET NOCOUNT ON
DECLARE		@ErrStr		VARCHAR	 (8000)
IF @bAllNote=1
BEGIN
	IF EXISTS(SELECT Nteid FROM    Note WITH (NOLOCK)
				WHERE	NteIsActive = 1	
				AND 	NteBooID = @BooId )					
	BEGIN
		SELECT '<BkNotes>'			
			SELECT	
				''											AS		NoteId, 
				NteId 										AS		NId,
				ISNULL(NteRntId,'')							AS		RId,
				ISNULL(dbo.GetCodCode(Note.NteCodTypId),'')	AS		Type,
				ISNULL(NteDesc,'')							AS		NoteDesc,
				ISNULL(dbo.GetCodCode(NteCodAudTypId),'')	AS		Audience,
				ISNULL(NtePriority,0) 						AS		Priority,
				CASE NteIsActive 
					WHEN 0 THEN  'N'
					WHEN 1 THEN 'Y'
				END 										AS		Active,
				ISNULL((SELECT UsrName FROM UserInfo WITH (NOLOCK)
						WHERE 	UserInfo.UsrId = Note.AddUsrId
						OR 		UserInfo.UsrCode = Note.AddUsrId),'')				AS		'By',
				dbo.getdatetime(Note.AddDateTime,0)			AS		AddDtTm,
				dbo.getdatetime(Note.AddDateTime,3)			AS		ISOADT ,
				'' + ISNULL((SELECT RntNum FROM Rental WITH (NOLOCK)
						WHERE RntId = NteRntId),'ALL')		AS		RntNo
			FROM    Note WITH (NOLOCK)
			WHERE	NteCodTypId is not null and NteCodTypId IN (@BooCodId, @rntCodID, @IcdCodID,  @infCodID)
			AND		NteIsActive = 1	
			AND 	NteBooid = @BooId
			ORDER BY NtePriority, dbo.getdatetime(Note.AddDateTime,3) desc
			FOR XML AUTO, ELEMENTS			
		SELECT '</BkNotes>'
	END
	ELSE
	BEGIN
		SELECT 
			@ErrStr = SmsMsgText 
		FROM 
			Messages WITH (NOLOCK)
		WHERE 
			SmsCode = 'GEN049' 
		SELECT '<Msg>GEN049/GEN049 - ' + @ErrStr + '</Msg>'
	END
END -- end of @bAllNote=1
ELSE
BEGIN
	IF EXISTS(SELECT Note.nteid
				FROM	Note WITH (NOLOCK)
				WHERE	Ntebooid = @booid 
				and 	NteRntId = @RntId
				AND		NteIsActive = 1)
		 OR	exists(select Note.nteid
				FROM	Note WITH (NOLOCK)
				WHERE	ntecodTypid is not null 
				and ntecodtypid = @BooCodId 
				and NteBooId = @BooId
				AND	NteIsActive = 1	)
	BEGIN
		SELECT '<BkNotes>'
			SELECT	''											AS		NoteId, 
				NteId 											AS		NId,
				ISNULL(NteRntId,'')								AS		RId,
				ISNULL(dbo.GetCodCode(Note.NteCodTypId),'')  	AS		Type,
				ISNULL(NteDesc,'')								AS		NoteDesc,
				ISNULL(dbo.GetCodCode(NteCodAudTypId),'')		AS		Audience,
				ISNULL(NtePriority,0) 							AS		Priority,
				CASE NteIsActive 
					WHEN 0 THEN  'N'
					WHEN 1 THEN 'Y'
				END 											AS		Active,
				ISNULL((SELECT UsrName FROM UserInfo WITH (NOLOCK)
							WHERE	UserInfo.UsrId = Note.AddUsrId 
							OR 		UserInfo.UsrCode = Note.AddUsrId),'')	AS	'By',
				dbo.getdatetime(Note.AddDateTime,0)				AS		AddDtTm,
				dbo.getdatetime(Note.AddDateTime,3)				AS		ISOADT ,
				'' + ISNULL((SELECT RntNum FROM Rental WITH (NOLOCK)
						WHERE	RntId = NteRntId), 'ALL')		AS		RntNo
			FROM	Note WITH (NOLOCK)
			WHERE	Ntebooid = @BooId 
			AND		(NteRntId IS NULL OR NteRntId = @RntId)
			AND		NteIsActive = 1
			ORDER BY NtePriority, dbo.getdatetime(Note.AddDateTime,3) desc
			FOR XML AUTO, ELEMENTS
			
			SELECT '</BkNotes>'
	END
	ELSE
	BEGIN		
		SELECT 	@ErrStr = SmsMsgText 
			FROM 	Messages WITH (NOLOCK)
			WHERE 	SmsCode = 'GEN049' 
		SELECT '<Msg>GEN049/GEN049 - ' + @ErrStr + '</Msg>'
	END
END
END















