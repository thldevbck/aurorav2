CREATE PROCEDURE [dbo].[Product_GetMaxSapSuffix]
(
	@ComCode AS varchar(64),
	@PrdId AS varchar(64)
)
AS

SELECT
	ISNULL (MAX (SaleableProduct.SapSuffix), 0) AS MaxSapSuffix
FROM
	SaleableProduct
	INNER JOIN Product ON Product.PrdId = SaleableProduct.SapPrdId
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
WHERE
	Brand.BrdComCode = @ComCode
	AND Product.PrdId = @PrdId
GO
