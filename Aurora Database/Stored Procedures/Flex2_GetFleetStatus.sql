set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go





CREATE PROCEDURE [dbo].[Flex2_GetFleetStatus] 
	@ComCode AS varchar(64),
	@CtyCode AS varchar(64),
	@BrdCode AS varchar(64),
	@TravelYear AS datetime,
	@PrdId AS varchar(64)
AS

/*
DECLARE @ComCode AS varchar(64)
DECLARE @TravelYear AS datetime
DECLARE @CtyCode AS varchar(64)
DECLARE @BrdCode AS varchar(64)
DECLARE @PrdId AS varchar(64)

SET @ComCode = 'THL'
SET @TravelYear = '1/04/2006'
SET @CtyCode = 'AU'
SET @BrdCode = 'B'
SET @PrdId = '3307685E-1F3C-454C-8A81-DF2A63F7C94C'
*/

SELECT 
	@ComCode AS ComCOde,
	@TravelYear AS TravelYear,
	@CtyCode AS CtyCode,
	@BrdCode AS BrdCode,
	Product.PrdId AS PrdId,
	FleetStatus.FstFleetStatus1,
	FleetStatus.FstFleetStatus2,
	FleetStatus.FstFleetStatus3,
	FleetStatus.FstFleetStatus4,
	FleetStatus.FstFleetStatus5,
	FleetStatus.FstFleetStatus6,
	FleetStatus.FstFleetStatus7,
	FleetStatus.FstFleetStatus8,
	FleetStatus.FstFleetStatus9,
	FleetStatus.FstFleetStatus10,
	FleetStatus.FstFleetStatus11,
	FleetStatus.FstFleetStatus12,
	FleetStatus.FstFleetStatus13,
	FleetStatus.FstFleetStatus14,
	FleetStatus.FstFleetStatus15,
	FleetStatus.FstFleetStatus16,
	FleetStatus.FstFleetStatus17,
	FleetStatus.FstFleetStatus18,
	FleetStatus.FstFleetStatus19,
	FleetStatus.FstFleetStatus20,
	FleetStatus.FstFleetStatus21,
	FleetStatus.FstFleetStatus22,
	FleetStatus.FstFleetStatus23,
	FleetStatus.FstFleetStatus24,
	FleetStatus.FstFleetStatus25,
	FleetStatus.FstFleetStatus26,
	FleetStatus.FstFleetStatus27,
	FleetStatus.FstFleetStatus28,
	FleetStatus.FstFleetStatus29,
	FleetStatus.FstFleetStatus30,
	FleetStatus.FstFleetStatus31,
	FleetStatus.FstFleetStatus32,
	FleetStatus.FstFleetStatus33,
	FleetStatus.FstFleetStatus34,
	FleetStatus.FstFleetStatus35,
	FleetStatus.FstFleetStatus36,
	FleetStatus.FstFleetStatus37,
	FleetStatus.FstFleetStatus38,
	FleetStatus.FstFleetStatus39,
	FleetStatus.FstFleetStatus40,
	FleetStatus.FstFleetStatus41,
	FleetStatus.FstFleetStatus42,
	FleetStatus.FstFleetStatus43,
	FleetStatus.FstFleetStatus44,
	FleetStatus.FstFleetStatus45,
	FleetStatus.FstFleetStatus46,
	FleetStatus.FstFleetStatus47,
	FleetStatus.FstFleetStatus48,
	FleetStatus.FstFleetStatus49,
	FleetStatus.FstFleetStatus50,
	FleetStatus.FstFleetStatus51,
	FleetStatus.FstFleetStatus52
FROM 
	Product 
	INNER JOIN FleetStatus
		ON FleetStatus.FstBrdCode = Product.PrdBrdCode
		AND FleetStatus.FstShortName = Product.PrdShortName
WHERE
	Product.PrdId = @PrdId
	AND Product.PrdIsVehicle = 1
	AND FleetStatus.FstTravelYear = DATEPART (year, @TravelYear)
	AND FleetStatus.FstCtyCode = @CtyCode
	



