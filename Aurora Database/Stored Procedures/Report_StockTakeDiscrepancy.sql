set ANSI_NULLS ON਍ഀ
set QUOTED_IDENTIFIER ON਍ഀ
go਍ഀ
਍ഀ
ALTER PROCEDURE [dbo].[Report_StockTakeDiscrepancy]਍ഀ
	@sLocCode	varchar(24) = '',਍ഀ
	@sLivId		varchar(64) = ''਍ഀ
AS਍ഀ
਍ഀ
/* Rewrite of RPT_stocktakeDiscrepancy for report in Oslo਍ഀ
Returns 5 tables:਍ഀ
- Basic header information (LocationInventory and Location data)਍ഀ
- Missing Vehicles਍ഀ
- Vehicles not expected in this location਍ഀ
- Vehicles on Rental਍ഀ
- Vehicles off Fleet਍ഀ
*/਍ഀ
/*਍ഀ
DECLARE @sLocCode	varchar(24)਍ഀ
DECLARE @sLivId		varchar(64)਍ഀ
SET @sLocCode = ''਍ഀ
--SET @sLivId = '008DE352-BFF4-4396-B9EA-2883905B362B'਍ഀ
SET @sLivId = 'E18D90C6-FAEC-4570-812D-C368A17978EF'਍ഀ
*/਍ഀ
਍ഀ
SET NOCOUNT ON਍ഀ
਍ഀ
-- Basic header information (LocationInventory and Location data)਍ഀ
--SELECT	਍ഀ
--	LocationInventory.LivStkChkDate,਍ഀ
--	LocationInventory.LivStatus,਍ഀ
--	Location.LocCode,਍ഀ
--	Location.LocName਍ഀ
--FROM 	਍ഀ
--	LocationInventory ਍ഀ
--	INNER JOIN Location ON LocationInventory.LivLocCode = Location.LocCode਍ഀ
--WHERE	਍ഀ
--	LocationInventory.LivId = @sLivId਍ഀ
਍ഀ
DECLARE @Branch	varchar(64)਍ഀ
SET @Branch = ਍ഀ
(਍ഀ
	SELECT RTrim(Location.LocCode) + ' - ' + Location.LocName਍ഀ
	FROM 	਍ഀ
		LocationInventory ਍ഀ
		INNER JOIN Location ON LocationInventory.LivLocCode = Location.LocCode਍ഀ
	WHERE	਍ഀ
		LocationInventory.LivId = @sLivId਍ഀ
)਍ഀ
਍ഀ
DECLARE @Status	varchar(64)਍ഀ
SET @Status = ਍ഀ
(਍ഀ
SELECT LocationInventory.LivStatus਍ഀ
FROM 	਍ഀ
	LocationInventory ਍ഀ
	INNER JOIN Location ON LocationInventory.LivLocCode = Location.LocCode਍ഀ
WHERE	਍ഀ
	LocationInventory.LivId = @sLivId਍ഀ
)਍ഀ
਍ഀ
/*਍ഀ
	Find all vehicles from aims where the nextlocation is null, and nextdatetime is null, and lastactivity is not null,਍ഀ
	and on the particular location and date, and not in the data the user entered.਍ഀ
	Used to go into <VehAims />਍ഀ
*/਍ഀ
਍ഀ
-- Missing Vehicles਍ഀ
SELECT 	਍ഀ
	ai_FleetModel.FleetModelId,  ਍ഀ
	RTrim(ai_FleetModel.FleetModelCode) + ' - ' + ai_FleetModel.FleetModelDescription AS ModelDescription,਍ഀ
	ai_FleetAsset.FleetAssetId,਍ഀ
	ai_FleetAsset.UnitNumber, ਍ഀ
	ai_FleetAsset.Rego,਍ഀ
	'Missing' AS Vehicles,਍ഀ
	@Branch AS Branch,਍ഀ
	@Status AS Status਍ഀ
FROM ਍ഀ
	LocationInventory਍ഀ
	INNER JOIN aimsprod.dbo.FleetLocations AS FleetLocations ON LocationInventory.LivLocCode = FleetLocations.LocationCode਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetAsset AS ai_FleetAsset ON FleetLocations.LocationId = ai_FleetAsset.LastLocation਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetModel AS ai_FleetModel ON ai_FleetAsset.FleetModelId = ai_FleetModel.FleetModelId਍ഀ
	LEFT JOIN CheckedProductItem ਍ഀ
		ON LocationInventory.LivId = CheckedProductItem.PriLivId਍ഀ
		AND ai_FleetAsset.UnitNumber = CheckedProductItem.PriUnitNum਍ഀ
WHERE 	਍ഀ
	LocationInventory.LivId = @sLivId ਍ഀ
	AND ai_FleetAsset.LastDateTime <= LocationInventory.LivStkChkDate਍ഀ
	AND ai_FleetAsset.NextLocation IS NULL ਍ഀ
	AND ai_FleetAsset.NextDateTime IS NULL਍ഀ
	AND CheckedProductItem.PriUnitNum IS NULL਍ഀ
--ORDER BY ai_FleetModel.FleetModelCode,  ai_FleetAsset.UnitNumber਍ഀ
਍ഀ
UNION਍ഀ
਍ഀ
/*਍ഀ
	Find all the records from the data user entered but doesn't match the records in aims਍ഀ
	Change Requested by Damien to trackdown all offfleets which are outside the branch਍ഀ
	Used to go into <VehAurora />਍ഀ
*/਍ഀ
਍ഀ
-- Vehicles not expected in this location਍ഀ
SELECT਍ഀ
	ai_FleetModel.FleetModelId,਍ഀ
	RTrim(ai_FleetModel.FleetModelCode) + ' - ' + ai_FleetModel.FleetModelDescription AS ModelDescription,਍ഀ
	ai_FleetAsset.FleetAssetId,਍ഀ
	ai_FleetAsset.UnitNumber, ਍ഀ
	ai_FleetAsset.Rego,਍ഀ
	'Not expected in this location' AS Vehicles,਍ഀ
	@Branch AS Branch,਍ഀ
	@Status AS Status਍ഀ
FROM ਍ഀ
	CheckedProductItem ਍ഀ
	INNER JOIN LocationInventory ON CheckedProductItem.PriLivId = LocationInventory.LivId਍ഀ
	INNER JOIN aimsprod.dbo.FleetLocations AS FleetLocations ON LocationInventory.LivLocCode = FleetLocations.LocationCode਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetAsset AS ai_FleetAsset ON CheckedProductItem.PriUnitNum = ai_FleetAsset.UnitNumber਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetModel AS ai_FleetModel ON ai_FleetAsset.FleetModelId = ai_FleetModel.FleetModelId਍ഀ
WHERE	਍ഀ
	LocationInventory.LivId = @sLivId ਍ഀ
	AND NOT EXISTS ਍ഀ
	(਍ഀ
		SELECT 	਍ഀ
			UnitNumber ਍ഀ
		FROM 	਍ഀ
			aimsprod.dbo.ai_FleetAsset AS ai_FleetAssetCheck਍ഀ
		WHERE	਍ഀ
			ai_FleetAssetCheck.UnitNumber = CheckedProductItem.PriUnitNum਍ഀ
			AND ai_FleetAssetCheck.LastLocation = FleetLocations.LocationId਍ഀ
			AND ਍ഀ
			(਍ഀ
				(ai_FleetAssetCheck.NextLocation IS NULL AND ai_FleetAssetCheck.NextDateTime IS NULL)਍ഀ
				OR (ai_FleetAssetCheck.NextLocation IS NOT NULL AND ai_FleetAssetCheck.NextDateTime IS NOT NULL AND ai_FleetAssetCheck.LastActivity IS NULL)਍ഀ
			)਍ഀ
			AND ai_FleetAssetCheck.LastDateTime <= LocationInventory.LivStkChkDate਍ഀ
	)਍ഀ
--ORDER BY ai_FleetModel.FleetModelCode,  ai_FleetAsset.UnitNumber਍ഀ
਍ഀ
UNION ਍ഀ
਍ഀ
/*਍ഀ
	Vehicles on Rental਍ഀ
	The vehicles user entered should be on rental based on Database ਍ഀ
	Used to go into <OnRental />਍ഀ
*/਍ഀ
਍ഀ
-- Vehicles on Rental਍ഀ
SELECT ਍ഀ
	ai_FleetModel.FleetModelId,਍ഀ
	RTrim(ai_FleetModel.FleetModelCode) + ' - ' +	ai_FleetModel.FleetModelDescription AS ModelDescription,਍ഀ
	ai_FleetAsset.FleetAssetId,਍ഀ
	ai_FleetAsset.UnitNumber, ਍ഀ
	ai_FleetAsset.Rego,਍ഀ
	'On Rental' AS Vehicles,਍ഀ
	@Branch AS Branch,਍ഀ
	@Status AS Status਍ഀ
FROM 	਍ഀ
	CheckedProductItem ਍ഀ
	INNER JOIN LocationInventory ON CheckedProductItem.PriLivId = LocationInventory.LivId਍ഀ
	INNER JOIN aimsprod.dbo.FleetLocations AS FleetLocations ON LocationInventory.LivLocCode = FleetLocations.LocationCode਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetAsset AS ai_FleetAsset ON CheckedProductItem.PriUnitNum = ai_FleetAsset.UnitNumber਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetModel AS ai_FleetModel ON ai_FleetAsset.FleetModelId = ai_FleetModel.FleetModelId਍ഀ
WHERE	਍ഀ
	LocationInventory.LivId = @sLivId ਍ഀ
	AND ai_FleetAsset.LastActivity IS NOT NULL਍ഀ
	AND ai_FleetAsset.NextDateTime IS NOT NULL਍ഀ
	AND ai_FleetAsset.NextLocation IS NOT NULL਍ഀ
--ORDER BY ai_FleetModel.FleetModelCode, ai_FleetAsset.UnitNumber਍ഀ
਍ഀ
UNION ਍ഀ
਍ഀ
/*਍ഀ
	Vehicles off fleet਍ഀ
	The vehicles user entered should be off fleet਍ഀ
	Used to go into <offFleet />਍ഀ
*/਍ഀ
਍ഀ
-- Vehicles off Fleet਍ഀ
SELECT DISTINCT਍ഀ
	ai_FleetModel.FleetModelId, ਍ഀ
	RTrim(ai_FleetModel.FleetModelCode) + ' - ' + ai_FleetModel.FleetModelDescription AS ModelDescription,਍ഀ
	ai_FleetAsset.FleetAssetId,਍ഀ
	ai_FleetAsset.UnitNumber, ਍ഀ
	ai_FleetAsset.Rego,਍ഀ
	'Off Fleet' AS Vehicles,਍ഀ
	@Branch AS Branch,਍ഀ
	@Status AS Status਍ഀ
FROM 	਍ഀ
	LocationInventory਍ഀ
	INNER JOIN aimsprod.dbo.FleetLocations AS FleetLocations ON LocationInventory.LivLocCode = FleetLocations.LocationCode਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetAsset AS ai_FleetAsset ON FleetLocations.LocationId = ai_FleetAsset.LastLocation਍ഀ
	INNER JOIN aimsprod.dbo.ai_FleetModel AS ai_FleetModel ON ai_FleetAsset.FleetModelId = ai_FleetModel.FleetModelId਍ഀ
	LEFT JOIN CheckedProductItem ਍ഀ
		ON LocationInventory.LivId = CheckedProductItem.PriLivId਍ഀ
		AND ai_FleetAsset.UnitNumber = CheckedProductItem.PriUnitNum਍ഀ
WHERE	਍ഀ
	LocationInventory.LivId = @sLivId ਍ഀ
	AND ai_FleetAsset.NextLocation IS NOT NULL਍ഀ
	AND ai_FleetAsset.NextDateTime IS NOT NULL਍ഀ
	AND ai_FleetAsset.LastActivity IS NULL਍ഀ
	AND CheckedProductItem.PriUnitNum IS NULL਍ഀ
ORDER BY Vehicles, ModelDescription, ai_FleetAsset.UnitNumber਍ഀ
਍ഀ
਍ഀ
਍ഀ
਍ഀ
਍ഀ
਍ഀ
਍ഀ
