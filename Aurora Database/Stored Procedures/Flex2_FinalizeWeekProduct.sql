USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_FinalizeWeekProduct]    Script Date: 11/19/2007 13:42:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Flex2_FinalizeWeekProduct] 
	@FlpId AS int,
	@LocationsInclude AS varchar(256),
	@LocationsExclude AS varchar(256),
	@IntegrityNo int,
	@UsrId AS varchar(64)
AS

UPDATE FlexBookingWeekProduct
SET 
	FlpLocationsInclude = @LocationsInclude,
	FlpLocationsExclude = @LocationsExclude,
	IntegrityNo = @IntegrityNo + 1,
	ModUsrId = @UsrId,
	ModDateTime = GETDATE()
FROM
	FlexBookingWeekProduct 
WHERE
	FlexBookingWeekProduct.FlpId = @FlpId
	AND FlexBookingWeekProduct.IntegrityNo = @IntegrityNo

IF @@ROWCOUNT <= 0 RETURN

SELECT 
	* 
FROM 
	FlexBookingWeekProduct 
WHERE 
	FlpId = @FlpId














