USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_ApproveBookingWeek]    Script Date: 11/19/2007 13:41:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Flex2_ApproveBookingWeek] 
	@FbwId int,
	@FbwEmailText ntext,
	@IntegrityNo tinyint,
	@UsrId varchar(64)

AS

UPDATE FlexBookingWeek SET
	FlexBookingWeek.FbwVersion = FlexBookingWeek.FbwVersion + 1,
	FlexBookingWeek.FbwStatus = 'approved',
	FlexBookingWeek.FbwEmailText = @FbwEmailText,
	FlexBookingWeek.IntegrityNo = @IntegrityNo + 1,
	FlexBookingWeek.ModUsrId = @UsrId,
	FlexBookingWeek.ModDateTime = GETDATE()
WHERE
	FlexBookingWeek.FbwId = @FbwId
	AND FlexBookingWeek.IntegrityNo = @IntegrityNo
	AND FlexBookingWeek.FbwStatus IN ('created', 'rejected')

IF @@ROWCOUNT <= 0 RETURN

UPDATE FlexBookingWeekExport SET
	FlexBookingWeekExport.FwxStatus = 'approved',  -- dont update the version, thats used for scheduler logic
	FlexBookingWeekExport.ModUsrId = @UsrId,
	FlexBookingWeekExport.ModDateTime = GETDATE()
WHERE
	FlexBookingWeekExport.FwxFbwId = @FbwId

SELECT 
	* 
FROM
	FlexBookingWeek
WHERE
	FbwId = @FbwId







