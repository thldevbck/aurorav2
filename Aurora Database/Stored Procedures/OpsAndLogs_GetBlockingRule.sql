CREATE PROCEDURE [dbo].[OpsAndLogs_GetBlockingRule]
	@comCode AS varchar(64),
	@ctyCode AS varchar(2),
	@blrId AS varchar(64) = NULL
AS

/*
	EXEC OpsAndLogs_GetBlockingRule 'THL', 'NZ', NULL
*/

SELECT
	BlockingRule.*
FROM
	BlockingRule
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC


SELECT
	BlockingRuleAgent.*
FROM
	BlockingRuleAgent
	INNER JOIN BlockingRule ON BlockingRuleAgent.BraBlrId = BlockingRule.BlrID
	INNER JOIN Agent ON BlockingRuleAgent.BraAgnId = Agent.AgnId
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	Agent.AgnCode



SELECT
	DISTINCT Agent.*
FROM
	BlockingRuleAgent
	INNER JOIN BlockingRule ON BlockingRuleAgent.BraBlrId = BlockingRule.BlrID
	INNER JOIN Agent ON BlockingRuleAgent.BraAgnId = Agent.AgnId
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)


SELECT
	BlockingRuleAgentCategory.*
FROM
	BlockingRuleAgentCategory
	INNER JOIN BlockingRule ON BlockingRuleAgentCategory.BacBlrId = BlockingRule.BlrID
	INNER JOIN Code ON BlockingRuleAgentCategory.BacCodAgentCtgyId = Code.CodId
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	Code.CodCode


SELECT
	BlockingRuleAgentCountry.*
FROM
	BlockingRuleAgentCountry
	INNER JOIN BlockingRule ON BlockingRuleAgentCountry.BrcBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	BlockingRuleAgentCountry.BrcCtyCode


SELECT
	BlockingRuleBrand.*
FROM
	BlockingRuleBrand
	INNER JOIN BlockingRule ON BlockingRuleBrand.BlbBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	BlockingRuleBrand.BlbBrdCode


SELECT
	BlockingRuleLocation.*
FROM
	BlockingRuleLocation
	INNER JOIN BlockingRule ON BlockingRuleLocation.BrlBlrId = BlockingRule.BlrID
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	BlockingRuleLocation.BrlLocCode


SELECT
	BlockingRulePackage.*
FROM
	BlockingRulePackage
	INNER JOIN BlockingRule ON BlockingRulePackage.BrpBlrId = BlockingRule.BlrID
	INNER JOIN Package ON BlockingRulePackage.BrpPkgId = Package.PkgId
	INNER JOIN Brand ON Package.PkgBrdCode = Brand.BrdCode
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
	AND Package.PkgCtyCode = @ctyCode
	AND Brand.BrdComCode = @comCode
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	Package.PkgCode


SELECT
	DISTINCT Package.*
FROM
	BlockingRulePackage
	INNER JOIN BlockingRule ON BlockingRulePackage.BrpBlrId = BlockingRule.BlrID
	INNER JOIN Package ON BlockingRulePackage.BrpPkgId = Package.PkgId
	INNER JOIN Brand ON Package.PkgBrdCode = Brand.BrdCode
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
	AND Package.PkgCtyCode = @ctyCode
	AND Brand.BrdComCode = @comCode


SELECT
	BlockingRuleProduct.*
FROM
	BlockingRuleProduct
	INNER JOIN BlockingRule ON BlockingRuleProduct.BlpBlrId = BlockingRule.BlrID
	INNER JOIN Product ON BlockingRuleProduct.BlpPrdId = Product.PrdId
	INNER JOIN Brand ON Product.PrdBrdCode = Brand.BrdCode
WHERE
	BlockingRule.BlrComCode = @comCode
	AND BlockingRule.BlrCtyCode = @ctyCode
	AND (@blrId IS NULL OR BlockingRule.BlrID = @blrId)
	AND Product.PrdIsVehicle = 1
	AND Brand.BrdComCode = @comCode
ORDER BY
	BlockingRule.BlrDateFrom DESC,
	BlockingRule.BlrDateTo DESC,
	Product.PrdShortName





GO
