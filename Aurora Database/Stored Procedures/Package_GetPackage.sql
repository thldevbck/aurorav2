CREATE PROCEDURE [dbo].[Package_GetPackage]
(
	@ComCode AS varchar(64),
	@PkgId AS varchar(64)
)
AS

/*
DECLARE @ComCode AS varchar(64)
DECLARE @PkgId AS varchar(64)

SET @ComCode = 'THL'
SET @PkgId = 'E672F3F5-2063-4150-A853-61B595DE4358'
*/

DECLARE @Package table (
	ComCode varchar(64),
	BrdCode varchar(1),
	PkgId varchar(64),
	PkgAgpId varchar(64)
)

INSERT INTO @Package
(
	ComCode,
	BrdCode,
	PkgId,
	PkgAgpId
)
SELECT
	Company.ComCode AS ComCode,
	Brand.BrdCode AS BrdCode,
	Package.PkgId AS PkgId,
	Package.PkgAgpId AS PkgAgpId
FROM
	Package
	INNER JOIN Brand ON Brand.BrdCode = Package.PkgBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
WHERE
	Company.ComCode = @ComCode
	AND Package.PkgId = @PkgId
ORDER BY
	Package.PkgCode


DECLARE @SaleableProduct table (
	ComCode varchar(64),
	BrdCode varchar(1),
	SapId varchar(64),
	SapPrdId varchar(64)
)

INSERT INTO @SaleableProduct
(
	ComCode,
	BrdCode,
	SapId,
	SapPrdId
)
SELECT DISTINCT
	Company.ComCode AS ComCode,
	Brand.BrdCode AS BrdCode,
	SaleableProduct.SapId AS SapId,
	SaleableProduct.SapPrdId AS SapPrdId
FROM
	@Package AS p
	INNER JOIN PackageProduct ON PackageProduct.PplPkgId = p.PkgId
	INNER JOIN SaleableProduct ON SaleableProduct.SapId = PackageProduct.PplSapId
	INNER JOIN Product ON Product.PrdId = SaleableProduct.SapPrdId
	INNER JOIN Brand ON Brand.BrdCode = Product.PrdBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
WHERE
	Company.ComCode = @ComCode

/*********************************************************************************************/
/* 0 - Package (All) */
/*********************************************************************************************/
SELECT
	Package.*
FROM
	Package
	INNER JOIN Brand ON Brand.BrdCode = Package.PkgBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
WHERE
	Company.ComCode = @ComCode
ORDER BY
	Package.PkgCode

/*********************************************************************************************/
/* 1 - PackageProduct */
/*********************************************************************************************/
SELECT
	PackageProduct.*
FROM
	@SaleableProduct AS s
	INNER JOIN SaleableProduct ON SaleableProduct.SapId = s.SapId
	INNER JOIN Product ON Product.PrdId = SaleableProduct.SapPrdId
	INNER JOIN PackageProduct ON PackageProduct.PplSapId = SaleableProduct.SapId
	INNER JOIN Package ON Package.PkgId = PackageProduct.PplPkgId
	INNER JOIN Brand ON Brand.BrdCode = Package.PkgBrdCode
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode
WHERE
	Company.ComCode = @ComCode
ORDER BY
	Package.PkgId,
	Product.PrdShortName,
	SaleableProduct.SapSuffix DESC


/*********************************************************************************************/
/* 2 - SaleableProduct */
/*********************************************************************************************/
SELECT
	SaleableProduct.*
FROM
	@SaleableProduct AS s
	INNER JOIN SaleableProduct ON SaleableProduct.SapId = s.SapId
	INNER JOIN Product ON SaleableProduct.SapPrdId = Product.PrdId
ORDER BY
	Product.PrdShortName,
	SaleableProduct.SapSuffix

/*********************************************************************************************/
/* 3 - Product */
/*********************************************************************************************/
SELECT
	DISTINCT Product.*
FROM
	@SaleableProduct AS s
	INNER JOIN Product ON Product.PrdId = s.SapPrdId
ORDER BY
	Product.PrdShortName

/*********************************************************************************************/
/* 4 - LocationAvailability */
/*********************************************************************************************/
SELECT
	LocationAvailability.*
FROM
	@Package AS p
	INNER JOIN LocationAvailability ON LocationAvailability.LcaPkgId = p.PkgId
ORDER BY
	LocationAvailability.LcaFromLocCode,
	LocationAvailability.LcaToLocCode

/*********************************************************************************************/
/* 5 - NonAvailabilityProfile */
/*********************************************************************************************/
SELECT
	NonAvailabilityProfile.*
FROM
	@Package AS p
	INNER JOIN NonAvailabilityProfile ON NonAvailabilityProfile.NapPkgId = p.PkgId
ORDER BY
	NonAvailabilityProfile.NapTravelFromDate,
	NonAvailabilityProfile.NapTravelToDate

/*********************************************************************************************/
/* 6 - WeeklyProfile */
/*********************************************************************************************/
SELECT
	WeeklyProfile.*
FROM
	@Package AS p
	INNER JOIN WeeklyProfile ON WeeklyProfile.WkpPkgId = p.PkgId
ORDER BY
	WeeklyProfile.WkpFromDayOfWeek,
	WeeklyProfile.WkpToDayOfWeek

/*********************************************************************************************/
/* 7 - PackageMarketingRegion */
/*********************************************************************************************/
SELECT
	PackageMarketingRegion.*
FROM
	@Package AS p
	INNER JOIN PackageMarketingRegion ON PackageMarketingRegion.PmrPkgId = p.PkgId

/*********************************************************************************************/
/* 8 - Note */
/*********************************************************************************************/
SELECT
	Note.*
FROM
	@Package AS p
	INNER JOIN Note ON Note.NtePkgId = p.PkgId
WHERE
	Note.NteBooID IS NULL
ORDER BY
	Note.ModDateTime DESC, Note.AddDateTime DESC

/*********************************************************************************************/
/* 9 - (Note) UserInfo */
/*********************************************************************************************/
SELECT
	DISTINCT UserInfo.*
FROM
	@Package AS p
	INNER JOIN Note ON Note.NtePkgId = p.PkgId
	INNER JOIN UserInfo ON UserInfo.UsrId = Note.AddUsrId OR UserInfo.UsrId = Note.ModUsrId
WHERE
	Note.NteBooID IS NULL

/*********************************************************************************************/
/* 10 - AgentPackage */
/*********************************************************************************************/
SELECT
	AgentPackage.*
FROM
	@Package AS p
	INNER JOIN AgentPackage ON AgentPackage.ApkPkgId = p.PkgId
	INNER JOIN Agent ON Agent.AgnId = AgentPackage.ApkAgnId
ORDER BY
	AgnCode

/*********************************************************************************************/
/* 11 - Agent */
/*********************************************************************************************/
SELECT
	DISTINCT Agent.*
FROM
	@Package AS p
	INNER JOIN AgentPackage ON AgentPackage.ApkPkgId = p.PkgId
	INNER JOIN Agent ON Agent.AgnId = AgentPackage.ApkAgnId
ORDER BY
	AgnCode

/*********************************************************************************************/
/* 12 - AgentGroup */
/*********************************************************************************************/
SELECT
	AgentGroup.*
FROM
	@Package AS p
	INNER JOIN AgentGroup ON AgentGroup.AgpId = p.PkgAgpId

/*********************************************************************************************/
/* 13 - FlexLevel */
/*********************************************************************************************/
SELECT
	FlexLevel.*
FROM
	@Package AS p
	INNER JOIN FlexLevel ON FlexLevel.FlxPkgId = p.PkgId
ORDER BY
	p.PkgId,
	FlexLevel.FlxSapId,
	FlexLevel.FlxEffDate DESC,
	FlexLevel.FlxNum

/*********************************************************************************************/


GO
