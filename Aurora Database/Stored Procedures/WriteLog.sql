set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================
-- Author:		JackLeong
-- Create date: 2007-10-02
-- Description:	Aurora Log table
-- =============================================
Create PROCEDURE [dbo].[WriteLog]
	 @EventID int, 
	 @Priority int, 
	 @Severity nvarchar(32), 
	 @Title nvarchar(256), 
	 @Timestamp datetime,
	 @MachineName nvarchar(32), 
	 @AppDomainName nvarchar(512),
	 @ProcessID nvarchar(256),
	 @ProcessName nvarchar(512),
	 @ThreadName nvarchar(512),
	 @Win32ThreadId nvarchar(128),
	 @Message nvarchar(1500),
	 @FormattedMessage ntext,
	 @LogId int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- INSERT the new record
	INSERT INTO ApplicationLog
	VALUES( 
		@EventID , 
		'',
		@Priority , 
		@Severity , 
		@Title , 
		@Timestamp ,
		@MachineName , 
		@AppDomainName ,
		@ProcessID ,
		@ProcessName,
		@ThreadName ,
		@Win32ThreadId ,
		@Message ,
		@FormattedMessage
	)

	-- Now return the InventoryID of the newly inserted record
	SELECT @LogId = SCOPE_IDENTITY()


END

