USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_GetBookingWeeksScheduler]    Script Date: 11/19/2007 13:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Flex2_GetBookingWeeksScheduler] 

AS

SELECT 
	*
FROM 
	FlexBookingWeek
WHERE
	FbwStatus = 'approved'
ORDER BY
	FbwBookStart 




