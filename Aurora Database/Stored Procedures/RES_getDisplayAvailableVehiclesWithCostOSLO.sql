CREATE           PROCEDURE [dbo].[RES_getDisplayAvailableVehiclesWithCostOSLO]
	@vhrId 		varchar(64) = '',
	@booId		varchar(64) = '',
	@rntId		varchar(64) = '',
	@sUsrCode	varchar(64) = '',
	@nxtBtn		varchar(32) = ''
AS
	SET NOCOUNT ON
	DECLARE @costdef 	AS 	VARCHAR(2),
			@CtyCode	AS	VARCHAR(3)
	SELECT	@CtyCode = dbo.getCountryForLocation(UsrLocCode, NULL, NULL)
		FROM	UserInfo with (nolock)
		WHERE	UsrCode = @sUsrCode
	SELECT @costdef = CASE
						WHEN UviValue = 'PerPerson' THEN 'pp'
						WHEN UviValue = 'PerDay' THEN 'pd'
						WHEN UviValue = 'Total' THEN 'tc'
						WHEN UviValue = 'Vehicle' THEN 'vc'
						ELSE 'tc'
					  END
		FROM UniversalInfo WITH(NOLOCK) WHERE UviKey = @CtyCode + 'CostDef'

	SELECT
		VhrId															AS	VhrId,
		ISNULL(@booId, '')												AS	booid,
		ISNULL((SELECT BooNum FROM Booking   WITH (NOLOCK)
					WHERE BooId = ISNULL(@booId, '')), '')				AS	bookingnum,
		ISNULL(VhrBkrId, '')											AS	bkrid,
		ISNULL((BkrId), '')						AS	bkrIntegrityNo,
		ISNULL((BkrAgnId), '')						AS	agnId,
		ISNULL((SELECT AgnCode+' - '+AgnName
					FROM Agent   WITH (NOLOCK)
					WHERE AgnId = (BkrAgnId)), '')	AS	agent,
		ISNULL((SELECT AgnIsMisc
					FROM Agent  WITH (NOLOCK)
					WHERE AgnId = BkrAgnId), 0)	AS	agentIsMisc,
		ISNULL((BkrMiscAgentName), '')						AS	miscAgnName,
		ISNULL((BkrConId),'')							AS	contact,
		ISNULL(VhrAgnRef, '')											AS	referance,
		ISNULL(VhrPrdId, '')											AS	productid,
		ISNULL((SELECT PrdShortName + ' - ' + PrdName
					FROM Product WITH (NOLOCK)
					WHERE PrdId = VhrPrdId), '') +
		ISNULL((SELECT ' - ' + PkgCode + ' - ' + PkgName
					FROM Package WITH (NOLOCK)
					WHERE PkgId = VhrPkgId), '')						AS	productname,
		ISNULL(VhrNumOfVehicles, '')									AS 	numvehicle,
		ISNULL(VhrBrdCode, '')											AS	brand,
		ISNULL(VhrClaId, '')											AS	class,
		ISNULL(VhrTypId, '')											AS	type,
		''																AS	commonfeature,
		''																AS	noncommonfeature,
		ISNULL(	VhrCkoLocCode +' - '+ (SELECT LocName
											FROM Location  WITH (NOLOCK)
											WHERE LocCode = VhrCkoLocCode), '')	AS	checkoutbranch,
		ISNULL(LEFT(DATENAME(DW, VhrCkoDate), 3), '')					AS 	coDw,
		CONVERT(varchar(10), VhrCkoDate, 103) 							AS	checkoutdate,
		(CONVERT(varchar(5), VhrCkoDate, 108) + ' ' +  ISNULL(VhrCkoAmPm, ''))											AS	checkoutampm,
		ISNULL(	VhrCkiLocCode +' - '+(SELECT LocName
										FROM Location WITH (NOLOCK)
										WHERE LocCode = VhrCkiLocCode), '')		AS	checkinbranch,
		ISNULL(LEFT(DATENAME(DW, VhrCkiDate), 3), '')					AS 	ciDw,
		CONVERT(varchar(10), VhrCkiDate, 103) 							AS	checkindate,
		(CONVERT(varchar(5), VhrCkiDate, 108) + ' ' + ISNULL(VhrCkiAmPm, ''))											As	checkinampm,
		ISNULL(VhrHirePeriod, '')										AS	hireperiod,
		ISNULL(VhrHirePeriodUom, '')									As	hiretype,
		ISNULL((BkrLastName), '')						AS	hirersurname,
		ISNULL((BkrFirstName), '')						As	hirerfirstname,
		ISNULL((BkrTitle), '')						AS	hirertitle,
		ISNULL(RTRIM(CAST(VhrNumOfAdults AS CHAR)), '')					AS	adult,
		ISNULL(RTRIM(CAST(VhrNumOfChildren AS CHAR)), '') 				AS	childern,
		ISNULL(RTRIM(CAST(VhrNumOfInfants AS CHAR)), '') 				AS	infants,
		ISNULL(VhrPkgId,'')												AS	pkgid,
		ISNULL((SELECT PkgCode + ' - ' + PkgName
					FROM Package WITH (NOLOCK)
					WHERE PkgId = VhrPkgId), '')						AS	package,
		ISNULL(VhrCodSourceMocId, '')									AS	requestsource,
		''																AS	buttonclicked,
		ISNULL(@costdef, 'tc')											AS 	costdef,
		LTRIM(RTRIM(dbo.getCountryForLocation(VhrCkoLocCode, NULL, NULL))) AS	rntCty
	FROM VehicleRequest  WITH (NOLOCK) , bookingrequest (nolock)
	WHERE VhrId = @vhrId
	and   vhrbkrid = bkrid
	FOR XML AUTO,ELEMENTS








GO
