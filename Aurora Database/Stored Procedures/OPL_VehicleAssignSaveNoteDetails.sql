CREATE PROCEDURE [dbo].[OPL_VehicleAssignSaveNoteDetails]
     @BookingNo VARCHAR(64) = '',
     @RentalNo VARCHAR(64) = '',
     @NoteId VARCHAR(64) = '',
     @NoteDesc VARCHAR(1000) = '',
     @NotePriority INT = 1,
     @NoteAudienceType VARCHAR(64) = '',
     @NoteActiveFlag BIT = 0,
     @NoteIntegrityNo INT = 1,
     @UserCode VARCHAR(64) = ''
AS

BEGIN
	SET NOCOUNT ON

	DECLARE @sComCode AS VARCHAR(64)


	 IF @NoteId = ''
	 BEGIN
		  SELECT @sComCode = dbo.fun_getCompany
			  (
			   @UserCode, -- @sUserCode VARCHAR(64),
			   NULL,  -- @sLocCode VARCHAR(64),
			   NULL,  -- @sBrdCode VARCHAR(64),
			   NULL,  -- @sPrdId  VARCHAR(64),
			   NULL,  -- @sPkgId  VARCHAR(64),
			   NULL,  -- @sdummy1 VARCHAR(64),
			   NULL,  -- @sdummy2 VARCHAR(64),
			   NULL  -- @sdummy3 VARCHAR(64)
			  )

		  INSERT INTO Note ( NteId,
			   NteBooID,
			   NteRntId,
			   NteCodTypId,
			   NteDesc,
			   NteCodAudTypId,
			   NtePriority,
			   NteIsActive,
			   IntegrityNo,
			   AddUsrId,
			   AddDateTime,
			   AddPrgmName,
			   -- KX Changes :RKS
			   NteComCode
			  )
		   SELECT  NEWID(),
			 (SELECT BooId FROM Booking WITH(NOLOCK) WHERE BooNum = @BookingNo),
			 (SELECT RntId FROM Booking WITH(NOLOCK), Rental WITH(NOLOCK) WHERE BooId = RntBooId AND BooNum = @BookingNo AND RntNum = @RentalNo),
			 dbo.getCodeId(13, 'Veh_Assign'),
			 @NoteDesc,--dbo.getSplitedData(@param3, '~', 2),
			 dbo.getCodeId(26, @NoteAudienceType),--dbo.getSplitedData(@param3, '~', 4)),
			 @NotePriority,--dbo.getSplitedData(@param3, '~', 3),
			 @NoteActiveFlag,--dbo.getSplitedData(@param3, '~', 5),
			 1,
			 (SELECT UsrId FROM UserInfo with (nolock) WHERE UsrCode = @UserCode),--,dbo.getSplitedData(@param3, '~', 7)),
			 CURRENT_TIMESTAMP,
			 'Vehicle_Assign',
			 -- KX Changes :RKS
			 @sComCode
		  IF (@@ERROR <> 0)
		  BEGIN
		   SELECT 'ERROR/ErrorIn Updating Note For Vehicle Assign(' + @BookingNo + '/' + @RentalNo + ')'
		   RETURN
		  END
		  SELECT 'SUCCESS/' + dbo.getErrorString('GEN046', '(' + @BookingNo + '/' + @RentalNo + ')', '', '', '', '', '')
		  RETURN
	 END -- IF dbo.getSplitedData(@param3, '~', 1) = ''
	 ELSE
	 BEGIN

		  --Integrity Check -- added by shoel , cos its missing!
	      IF EXISTS ( SELECT COUNT(1) FROM Note WITH (NOLOCK) WHERE NteId = @NoteId AND IntegrityNo = @NoteIntegrityNo)
	      --Match found, update
	      BEGIN
			Update Note WITH(ROWLOCK)
			SET NteDesc = @NoteDesc,--dbo.getSplitedData(@param3, '~', 2),
				NteCodAudTypId = dbo.getCodeId(26, @NoteAudienceType),--dbo.getSplitedData(@param3, '~', 4)),
				NtePriority = @NotePriority,--dbo.getSplitedData(@param3, '~', 3),
				NteIsActive = @NoteActiveFlag,--dbo.getSplitedData(@param3, '~', 5),
				IntegrityNo = IntegrityNo + 1,
				ModUsrId = (SELECT UsrId FROM UserInfo  with (nolock) WHERE UsrCode = @UserCode),--dbo.getSplitedData(@param3, '~', 7)),
				ModDateTime = CURRENT_TIMESTAMP
			WHERE NteId = @NoteId--dbo.getSplitedData(@param3, '~', 1)
			IF (@@ERROR <> 0)
			BEGIN
				SELECT 'ERROR/ErrorIn Updating Note For Vehicle Assign(' + @BookingNo + '/' + @RentalNo + ')'
				RETURN
			END
			SELECT 'SUCCESS/' + dbo.getErrorString('GEN046', '(' + @BookingNo + '/' + @RentalNo + ')', '', '', '', '', '')
			RETURN
	      END
		  ELSE --no match!
          BEGIN
			SELECT 'ERROR/Error In Updating Note For Vehicle Assign(' + @BookingNo + '/' + @RentalNo + '), this note has been modified by another user. Please refresh your screen and try again.'
			RETURN
		  END

	 END
	 RETURN

END




GO
