set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go





ALTER   PROCEDURE [dbo].[header_get_userName] 
	@sUserCode	varchar(64)            
AS

/*
EXEC [header_get_userName] 'jl3'
*/


SET NOCOUNT ON
IF NOT EXISTS(SELECT UsrId FROM UserInfo WITH (NOLOCK) WHERE UsrCode = @sUserCode)
BEGIN
    SELECT 'GEN049 - ' + smsMsgText FROM Messages  WITH (NOLOCK) WHERE  smscode = 'GEN049'
    RETURN
END	
ELSE
BEGIN
	DECLARE @UviValue AS varchar(128)
	SELECT @UviValue=UviValue FROM UniversalInfo WHERE UviKey = 'SYSIDENT'

	SELECT 
		UserInfo.UsrId AS UsrId,
		'' + UserInfo.UsrName AS UsrName,
		'' + UserInfo.UsrCtyCode AS UsrCity,
		'' + Company.ComName AS ComName,
		'' + Company.ComCode AS ComCode,
		'' + Location.LocCode AS UsrLoc,
		'' + Location.LocName AS LocName,
		ISNULL (Location.LocHoursBehindServerLocation, 0.00) AS LocHoursBehindServerLocation,
		ISNULL (TownCity.TctCode, '') AS TctCode,
		ISNULL (TownCity.TctName, '') AS TctName,
		ISNULL (Country.CtyCode, '') AS CtyCode,
		ISNULL (Country.CtyName, '') AS CtyName,
		ISNULL (@UviValue, '') AS SysIdent
	FROM 
		UserInfo WITH (NOLOCK) 
		INNER JOIN dbo.UserCompany (NOLOCK) ON UserCompany.UsrId = UserInfo.UsrId
		INNER JOIN dbo.Company (NOLOCK) ON Company.ComCode = UserCompany.ComCode 
		LEFT JOIN dbo.Location (NOLOCK) ON Location.LocCode = userinfo.UsrLocCode
		LEFT JOIN dbo.TownCity (NOLOCK) ON TownCity.TctCode = Location.LocTctCode
		LEFT JOIN dbo.Country (NOLOCK) ON Country.CtyCode = userinfo.UsrCtyCode
		
	WHERE 
		UsrCode = @sUserCode
	FOR XML AUTO,ELEMENTS
END



