set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/*
SET DATEFORMAT DMY
EXEC CustomerService_GetDailyActivity 'wv1', 'akl', '28/03/2008', 1, 2
*/

ALTER PROCEDURE [dbo].[CustomerService_GetDailyActivity] 
	@UsrCode AS varchar(100),
	@LocCode AS varchar(50), 
	@ActivityFor AS datetime,
	@IncludeOverdues AS bit,
	@SortBy AS int = 0 -- 0 - BooName (customer), 1 - RntCkoWhen/RntCkiWhen (Date), 2 - BrdCode,PrdShortName (Vehicle)

AS

SET NOCOUNT ON

-- Security Check
IF NOT @LocCode IN (SELECT LocCode FROM Location  
		INNER JOIN Company ON Location.LocComCode = Company.ComCode
		INNER JOIN UserCompany ON UserCompany.ComCode = Company.ComCode
		INNER JOIN UserInfo ON UserInfo.UsrId = UserCompany.UsrId AND UsrCode = @UsrCode)
BEGIN
	RETURN
END

DECLARE @ActivityForTo AS datetime
SET @ActivityForTo = @ActivityFor + 1

DECLARE @CtyCode AS varchar(3)
SELECT @CtyCode = dbo.GetCountryForLocation(@LocCode,'',1)

DECLARE @AU_THRIFTYPRDS AS varchar(1000)
SELECT @AU_THRIFTYPRDS = UviValue From UniversalInfo WHERE UviKey = @CtyCode + '_THRIFTYPRDS'

DECLARE @AU_THRIFTYEFFDT AS datetime
SELECT @AU_THRIFTYEFFDT = CAST(UviValue AS DATETIME) From UniversalInfo WHERE UviKey = @CtyCode + '_THRIFTYEFFDT'

DECLARE @XfrmId AS varchar(64)
SELECT @XfrmId = dbo.GetCodeID(4, 'XFRM') -- Set the Xfrm id From the Code Table

-- **************************************
-- ** Checkouts                        **
-- **************************************
SELECT 
	Rental.RntId,
	Booking.BooId,
	CAST (ISNULL (Rental.RntIsError, 0) AS bit) AS RntIsError,
	LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, '')) AS BooName,
	Booking.BooNum + '/' + Rental.RntNum + 
	CASE 
		WHEN Rental.RenAllowVehSpec IS NULL OR Rental.RenAllowVehSpec = '' THEN '' 
		ELSE ' F' 
	END AS BooNum,
	Rental.RntCkoWhen,
	Rental.RntCkiWhen,
	Rental.RntCkiLocCode + '-' + SUBSTRING(CONVERT(varchar,Rental.RntCkiWhen,103),1,5) AS Ckidet,
	PkgBrdCode,
	Rental.RntIsVip,
	Rental.RntStatus, 
	Product.PrdBrdCode + '-' + Product.PrdShortName AS ProductName,
	Class.ClaCode +'-'+ Type.TypCode AS ClaTypCode,
	CAST ((CASE 
		WHEN (RntCkoWhen < @ActivityFor AND RntStatus = 'KK') THEN 1
		ELSE 0
	END) AS bit) AS IsOverdue,
	CAST ((CASE 
		WHEN (RntCkoWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus = 'KK') THEN 1
		ELSE 0
	END) AS bit) AS IsDue,
	CAST ((CASE 
		WHEN (RntCkoWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus in ('CO','CI')) THEN 1
		ELSE 0
	END) AS bit) AS IsDone,
	CASE 
		WHEN Aimsprod.dbo.fleetmodel.FleetModelCode IS NOT NULL THEN Package.PkgBrdCode + ' - ' + ISNULL (Aimsprod.dbo.fleetmodel.FleetModelCode, '') 
		ELSE ''
	END AS FleetModelCode
FROM 
	Rental
	INNER JOIN Package ON Rental.RntPkgId = Package.PkgId
	INNER JOIN Booking ON Rental.RntBooId = Booking.BooId 
	INNER JOIN Product ON Rental.RntPrdRequestedVehicleId = Product.PrdId
	INNER JOIN Type ON Product.PrdTypId = Type.TypId
	INNER JOIN Class ON Type.TypClaId = Class.ClaId
	LEFT JOIN dvs_RentalAssign 
		ON dvs_RentalAssign.Ctycode = @CtyCode 
		AND dvs_RentalAssign.DraRntNum = (Booking.BooNum + '/' + Rental.RntNum + CASE WHEN Rental.RenAllowVehSpec IS NULL OR Rental.RenAllowVehSpec = '' THEN '' ELSE ' F' END)
	LEFT JOIN FleetAssetView ON dvs_RentalAssign.DraUnitNum = FleetAssetView.Unitnumber
	LEFT JOIN Aimsprod.dbo.fleetmodel ON FleetAssetView.FleetModelId = Aimsprod.dbo.fleetmodel.FleetModelId
WHERE 
	Rental.RntCkoLocCode = @LocCode
	-- Thrifty Check
	AND NOT
	(
		@AU_THRIFTYPRDS IS NOT NULL 
		AND @AU_THRIFTYPRDS <> '' 
		AND @CtyCode = 'AU'
		AND charindex(Class.ClaCode +'-'+ Type.TypCode, @AU_THRIFTYPRDS) <> 0 
		AND  RntCkoWhen >= @AU_THRIFTYEFFDT
	)
	AND 
	(
		(
			(@IncludeOverdues = 1)
			AND 
			(
				(RntCkoWhen < @ActivityForTo AND RntStatus = 'KK')
				OR (RntCkoWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus in ('CO','CI'))
			)
		)
		OR
		(
			NOT (@IncludeOverdues = 1)
			AND
			(
				(RntCkoWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus = 'KK')
				OR (RntCkoWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus in ('CO','CI'))
			)
		)
	)
ORDER BY
	CASE @SortBy
		WHEN 0 THEN LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, ''))
		WHEN 1 THEN CONVERT(varchar,Rental.RntCkoWhen,103)
		ELSE Product.PrdBrdCode + ' - ' + Product.PrdShortName
	END,
	LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, '')),
	Booking.BooNum + '/' + Rental.RntNum
	
-- **************************************
-- ** Checkins                         **
-- **************************************
SELECT 
	Rental.RntId,
	Booking.BooId,
	CAST (ISNULL (Rental.RntIsError, 0) AS bit) AS RntIsError,
	LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, '')) AS BooName,
	Booking.BooNum + '/' + RntNum + 
	CASE 
		WHEN Rental.RenAllowVehSpec IS NULL OR Rental.RenAllowVehSpec = '' THEN '' 
		ELSE ' F' 
	END AS BooNum,
	Rental.RntCkoWhen,
	Rental.RntCkiWhen,
	Rental.RntCkiLocCode + '-' + SUBSTRING(CONVERT(varchar,Rental.RntCkiWhen,103),1,5) AS Ckidet,
	PkgBrdCode,
	Rental.RntIsVip,
	Rental.RntStatus, 
	Product.PrdBrdCode + ' - ' + Product.PrdShortName AS ProductName,
	Class.ClaCode +'-'+ Type.TypCode AS ClaTypCode,
	CAST ((CASE 
		WHEN (RntCkiWhen < @ActivityFor AND RntStatus in ('KK','CO')) THEN 1
		ELSE 0
	END) AS bit) AS IsOverdue,
	CAST ((CASE 
		WHEN (RntCkiWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus in ('KK','CO')) THEN 1
		ELSE 0
	END) AS bit) AS IsDue,
	CAST ((CASE 
		WHEN (RntStatus = 'CI') THEN 1
		ELSE 0
	END) AS bit) AS IsDone,
	ISNULL((
		SELECT TOP 1 BpdUnitNum
		FROM BookedProduct 
		WHERE	
			BpdPrdIsVehicle = 1
			AND BpdRntId = Rental.RntId
			AND	BpdStatus = 'KK'
			AND (BpdCodTypeId IS NULL OR BpdCodTypeId <> @XfrmId)
		ORDER BY BpdCkiWhen DESC
	), '') AS BpdUnitNum
FROM 
	Rental
	INNER JOIN Package ON Rental.RntPkgId = Package.PkgId
	INNER JOIN Booking ON Rental.RntBooId = Booking.BooId 
	INNER JOIN Product ON Rental.RntPrdRequestedVehicleId = Product.PrdId
	INNER JOIN Type ON Product.PrdTypId = Type.TypId
	INNER JOIN Class ON Type.TypClaId = Class.ClaId
WHERE 
	RntCkiLocCode = @LocCode
	-- Thrifty Check
	AND NOT
	(
		@AU_THRIFTYPRDS IS NOT NULL 
		AND @AU_THRIFTYPRDS <> '' 
		AND @CtyCode = 'AU'
		AND charindex(Class.ClaCode +'-'+ Type.TypCode,@AU_THRIFTYPRDS) <> 0 
		AND  RntCkiWhen >= @AU_THRIFTYEFFDT 
		AND RntCkoWhen >= @AU_THRIFTYEFFDT
	)
	AND 
	(
		(
			(@IncludeOverdues = 1)
			AND 
			(
				(RntCkiWhen < @ActivityForTo AND RntStatus in ('KK','CO'))
				OR (RntCkiWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus = 'CI')
			)
		)
		OR
		(
			NOT (@IncludeOverdues = 1)
			AND
			(
				(RntCkiWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus in ('KK','CO'))
				OR (RntCkiWhen BETWEEN @ActivityFor AND @ActivityForTo AND RntStatus = 'CI')
			)
		)
	)
ORDER BY
	CASE @SortBy
		WHEN 0 THEN LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, ''))
		WHEN 1 THEN CONVERT(varchar,Rental.RntCkiWhen,103)
		ELSE ISNULL((
			SELECT TOP 1 BpdUnitNum
			FROM BookedProduct 
			WHERE	
				BpdPrdIsVehicle = 1
				AND BpdRntId = Rental.RntId
				AND	BpdStatus = 'KK'
				AND (BpdCodTypeId IS NULL OR BpdCodTypeId <> @XfrmId)
			ORDER BY BpdCkiWhen DESC
		), '')
	END,
	LTRIM (ISNULL(Booking.BooLastName, '') + ', ' + ISNULL(Booking.BooFirstName, '')),
	Booking.BooNum + '/' + Rental.RntNum
	


