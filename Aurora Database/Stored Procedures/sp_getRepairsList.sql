CREATE           PROCEDURE [dbo].[sp_getRepairsList]
		@sSearchField				VARCHAR	 (64)	=	'',
		@sFleetAssetId				VARCHAR	 (64)	=	'',
		-- KX Chnages :RKS
		@sUsrCode					VARCHAR	 (64)	=	''
AS
	SET NOCOUNT ON
	DECLARE	@sComCode		VARCHAR(64),
			@sFleeCompanyId	VARCHAR(64)

	-- KX Chnages :RKS
	IF @sUsrCode<>''
		SELECT	@sComCode		= Company.ComCode,
				@sFleeCompanyId = FleetCompanyId
		FROM	dbo.UserInfo (NOLOCK), dbo.UserCompany (NOLOCK), dbo.Company (NOLOCK)
		WHERE	UserCompany.ComCode = Company.ComCode AND UserCompany.UsrId = UserInfo.UsrId
		AND UsrCode = @sUsrCode


	IF NOT EXISTS(SELECT FleetAssetId FROM AimsProd.dbo.FleetAsset (NOLOCK)
			  WHERE
				-- KX Chnages :RKS
				(@sUsrCode='' OR CHARINDEX(cast(CompanyId as varchar),@sFleeCompanyId)>0)
				AND ((UnitNumber = @sSearchField OR RegistrationNumber = @sSearchField)
				 OR
				  (FleetAssetId = @sFleetAssetId)
				)
	)
	BEGIN
		SELECT 'GEN049 - ' + dbo.getErrorString('GEN049', '', '', '', '', '', '')
		RETURN
	END -- IF EXISTS(SELECT FleetAssetId FROM...

	IF  @sFleetAssetId = '' AND @sSearchField = ''	RETURN
	IF  @sFleetAssetId <> ''
	BEGIN
		IF NOT EXISTS(SELECT	FA.FleetAssetId
						FROM 	fleetassetview AS FA WITH(NOLOCK)
						WHERE	FA.FleetAssetId = @sFleetAssetId)
		BEGIN
			SELECT 'GEN049 - ' + dbo.getErrorString('GEN049', '', '', '', '', '', '')
			RETURN
		END	-- IF NOT EXISTS(SELECT	FA.FleetAssetId
	END -- IF  @sFleetAssetId <> ''
	ELSE
	BEGIN
		IF NOT EXISTS(SELECT	FA.UnitNumber
						FROM fleetassetview AS FA WITH(NOLOCK)
						WHERE	FA.UnitNumber = @sSearchField)	AND
		   NOT EXISTS(SELECT	FA.UnitNumber
						FROM fleetassetview AS FA WITH(NOLOCK)
						WHERE	FA.Rego = @sSearchField)
		BEGIN
			SELECT 'GEN049 - ' + dbo.getErrorString('GEN049', '', '', '', '', '', '')
			RETURN
		END -- IF NOT EXISTS(SELECT	FA.UnitNumber

		SELECT	@sFleetAssetId  = FA.FleetAssetId
			FROM 	fleetassetview AS  FA WITH(NOLOCK)
			WHERE	FA.UnitNumber = @sSearchField
			OR		FA.Rego = @sSearchField
	END

	SELECT '<Root>'
	SELECT	ISNULL(ModelInfo.FleetAssetId, '')					AS	FleetAssetId,
			ModelInfo.UnitNumber								AS	UnitNo,
			LTRIM(RTRIM(FM.FleetModelCode)) + ' - ' +
				LTRIM(RTRIM(FM.FleetModelDescription))			AS	'Desc',
			--Fix by shoel to avoid missing RegBo Column!! 14/3/8
			ISNULL(ModelInfo.Rego,'')							AS	RegBo,
			--Fix by shoel to avoid missing RegBo Column!! 14/3/8
			ISNULL(CONVERT(VARCHAR(10), RUCRenewDate, 103), '')	AS	RUCExp,
			ISNULL(CONVERT(VARCHAR(10), RCCofEndDate, 103), '')	AS	COFExp,
			ISNULL(CONVERT(VARCHAR(10), RegEndDate, 103), '')	AS	RegoExp,
			ISNULL(IdRadioCode, '')								AS	RadioCode,
			ISNULL(IdEngineNumber, '')							AS	EngNum,
			ISNULL(IdChassisNumber, '')							AS	VinNum,
			ISNULL(CAST(IdCompliancePlateYear AS VARCHAR), '')	AS	YrOfMan,
			ISNULL(CONVERT(VARCHAR(10), RegFirstDate, 103), '')	AS	FstRegDt
		FROM	fleetassetview  	AS	ModelInfo WITH(NOLOCK),
				AimsProd.dbo.AI_FleetModel		AS 	FM WITH(NOLOCK)
		WHERE	FM.FleetModelId = ModelInfo.FleetModelId
		AND		ModelInfo.FleetAssetId =  @sFleetAssetId
		FOR XML AUTO

	SELECT '<RepairList>'
	IF NOT EXISTS (SELECT	TOP 1 FR.RepairId
					FROM	AimsProd.dbo.AI_FleetRepair AS FR WITH(NOLOCK)
							LEFT OUTER JOIN AimsProd.dbo.AI_Location AS LO WITH(NOLOCK) on LO.LocationId = FR.LocationId
							LEFT OUTER JOIN AimsProd.dbo.AI_ServiceScheduleService AS SS WITH(NOLOCK) on FR.ServiceId = SS.ServiceScheduleServiceId
							LEFT OUTER JOIN AimsProd.dbo.AI_Repairer AS RR WITH(NOLOCK) on FR.ServiceProvider = RR.RepairerCode
							LEFT OUTER JOIN fleetassetview AS FA WITH(NOLOCK) on FA.FleetAssetId = FR.FleetAssetId
							WHERE  FA.FleetAssetId =  @sFleetAssetId)
	BEGIN
		SELECT '<FR RepairId="" Branch="" FrDt="" FrTm="" ToDt="" ToTm="" OdoSt="" ServPt="" Reason="" Provider="" ComplDt="" ComplTm="" OdoEnd="" PONo="" AvlSch=""/>'
	END -- IF NOT EXISTS (SELECT	TOP 1 FR.RepairId
	ELSE
	BEGIN
		SELECT	'' + FR.RepairId											AS		RepairId,
				'' + LO.LocationCode										AS		Branch,
				'' + ISNULL(CONVERT(VARCHAR(10),FR.StartDateTime,103),'')	AS		FrDt,
				'' + ISNULL(DBO.GetDateTime(StartDateTime,1),'')			AS		FrTm,
				'' + ISNULL(CONVERT(VARCHAR(10),FR.EndDateTime,103),'')		AS		ToDt,
				'' + ISNULL(DBO.GetDateTime(EndDateTime,1),'')				AS		ToTm,
				ISNULL(StartOdometer,0)										AS		OdoSt,
				'' + ISNULL(CONVERT(VARCHAR,SS.OdometerDue)+'K','')			AS		ServPt,
				'' + ISNULL(FR.ReasonCode,'')								AS		Reason,
				'' + ISNULL(RR.RepairerName,'')								AS		Provider,
				'' + ISNULL(CONVERT(VARCHAR(10), EndDateTime,103),'')		AS		ComplDt,
				'' + ISNULL(DBO.GetDateTime(EndDateTime,1),'')				AS		ComplTm,
				ISNULL(EndOdometer,0)										AS		OdoEnd,
				'' + ISNULL(Pon,'')											AS		PONo,
				CASE OffFleetFlag WHEN 1 THEN 'Y' ELSE 'N' END				AS		AvlSch
		FROM	AimsProd.dbo.AI_FleetRepair AS FR WITH(NOLOCK)

				LEFT OUTER JOIN AimsProd.dbo.AI_Location AS LO WITH(NOLOCK) on LO.LocationId = FR.LocationId
				LEFT OUTER JOIN AimsProd.dbo.AI_ServiceScheduleService AS SS WITH(NOLOCK) on FR.ServiceId = SS.ServiceScheduleServiceId
				LEFT OUTER JOIN AimsProd.dbo.AI_Repairer AS RR WITH(NOLOCK) on FR.ServiceProvider = RR.RepairerCode
				LEFT OUTER JOIN fleetassetview AS FA WITH(NOLOCK) on FA.FleetAssetId = FR.FleetAssetId
				WHERE  FA.FleetAssetId =  @sFleetAssetId
		ORDER BY StartDateTime DESC
		FOR XML AUTO
	END
	SELECT '</RepairList>'
	SELECT '</Root>'



GO
