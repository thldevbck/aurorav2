USE [Aurora]
GO
/****** Object:  StoredProcedure [dbo].[Flex2_CreateUpdateProductException]    Script Date: 11/19/2007 13:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Flex2_CreateUpdateProductException] 
	@FleId AS int,
	@FlpId AS int,
	@BookStart AS datetime,
	@BookEnd AS datetime,
	@TravelStart AS datetime,
	@TravelEnd AS datetime,
	@LocCodeFrom AS varchar(12),
	@LocCodeTo AS varchar(12),
	@Delta AS int
AS

IF @FleId IS NULL 
BEGIN
	INSERT INTO FlexBookingWeekException
	(
		FleFlpId,
		FleBookStart,
		FleBookEnd,
		FleTravelStart,
		FleTravelEnd,
		FleLocCodeFrom,
		FleLocCodeTo,
		FleDelta
	)
	VALUES
	(
		@FlpId,
		@BookStart,
		@BookEnd,
		@TravelStart,
		@TravelEnd,
		@LocCodeFrom,
		@LocCodeTo,
		@Delta
	)

	SET @FleId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	UPDATE FlexBookingWeekException
	SET 
		FleFlpId = @FlpId,
		FleBookStart = @BookStart,
		FleBookEnd = @BookEnd,
		FleTravelStart = @TravelStart,
		FleTravelEnd = @TravelEnd,
		FleLocCodeFrom = @LocCodeFrom,
		FleLocCodeTo = @LocCodeTo,
		FleDelta = @Delta
	WHERE
		FleId = @FleId
END

SELECT * FROM FlexBookingWeekException WHERE FleId = @FleId

