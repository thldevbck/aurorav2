
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[FlexWeekProductPackageView] AS
SELECT 
	FlexBookingWeek.FbwId,
	FlexBookingWeekProduct.FlpId,
	FlexProductsView.ComCode,
	Country.CtyCode,
	FlexProductsView.BrdCode,
	FlexBookingWeekProduct.FlpTravelYear AS TravelYear,
	FlexProductsView.PrdId,
	FlexProductsView.PrdShortName,

	FlexPackageView.SapId,
	FlexPackageView.SapStatus,
	FlexPackageView.SapSuffix,

	FlexPackageView.PkgId,
	FlexPackageView.PkgCode,
	FlexPackageView.PkgName,
	FlexPackageView.PkgDesc,
	FlexPackageView.PkgIsActive,
	FlexPackageView.PkgBookedFromDate,
	FlexPackageView.PkgBookedToDate,
	FlexPackageView.PkgTravelFromDate,
	FlexPackageView.PkgTravelToDate,

	CASE WHEN FlexPackageView.IsActive = 1 AND FlexProductsView.IsActive = 1 THEN 1 ELSE 0 END AS IsActive
FROM 
	FlexBookingWeek
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = FlexBookingWeek.FbwId
	INNER JOIN FlexProductsView 
		ON FlexProductsView.ComCode = FlexBookingWeekProduct.FlpComCode
		AND FlexProductsView.BrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND FlexProductsView.PrdId = FlexBookingWeekProduct.FlpPrdId 
	INNER JOIN Country
		ON Country.CtyCode = FlexBookingWeekProduct.FlpCtyCode  
	INNER JOIN FlexPackageView
		ON FlexPackageView.PrdId = FlexProductsView.PrdId 
		AND FlexPackageView.CtyCode = Country.CtyCode  	
		AND FlexPackageView.BrdCode = FlexProductsView.BrdCode
WHERE
	--FlexBookingWeek.FbwId = 212 AND 
	(1=1)
	AND FlexPackageView.PkgBookedFromDate < DATEADD (d, 7, FlexBookingWeek.FbwBookStart)
	AND FlexPackageView.PkgBookedToDate >= FlexBookingWeek.FbwBookStart
	AND FlexPackageView.PkgTravelFromDate < DATEADD (y, 1, FlexBookingWeekProduct.FlpTravelYear)
	AND FlexPackageView.PkgTravelToDate >= FlexBookingWeekProduct.FlpTravelYear


/*
ORDER BY
	FlexBookingWeek.FbwBookStart,
	FlexTravelYearsView.TravelYear,
	FlexProductsView.ComCode,
	Country.CtyCode,
	FlexProductsView.BrdCode,
	FlexProductsView.PrdShortName,
	FlexPackageView.PkgCode
*/
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

