USE [Aurora]
GO
/****** Object:  View [dbo].[FlexAgentEmailView]    Script Date: 11/19/2007 13:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FlexAgentEmailView] AS
SELECT 
	Agent.AgnId,
	Agent.AgnCode,
	Agent.AgnName,
	Agent.AgnIsActive,
	AgentCompany.AgcComCode,
	AgentCompany.AgcAgnId,
	AgentCompany.AgcEmail0 AS AgcEmail
FROM 
	Agent
	INNER JOIN AgentCompany
		ON AgentCompany.AgcAgnId = Agent.AgnId
WHERE
	RTRIM (ISNULL (AgentCompany.AgcEmail0, '')) <> ''

UNION

SELECT 
	Agent.AgnId,
	Agent.AgnCode,
	Agent.AgnName,
	Agent.AgnIsActive,
	AgentCompany.AgcComCode,
	AgentCompany.AgcAgnId,
	AgentCompany.AgcEmail1 AS AgcEmail
FROM 
	Agent
	INNER JOIN AgentCompany
		ON AgentCompany.AgcAgnId = Agent.AgnId
WHERE
	RTRIM (ISNULL (AgentCompany.AgcEmail1, '')) <> ''

UNION

SELECT 
	Agent.AgnId,
	Agent.AgnCode,
	Agent.AgnName,
	Agent.AgnIsActive,
	AgentCompany.AgcComCode,
	AgentCompany.AgcAgnId,
	AgentCompany.AgcEmail2 AS AgcEmail
FROM 
	Agent
	INNER JOIN AgentCompany
		ON AgentCompany.AgcAgnId = Agent.AgnId
WHERE
	RTRIM (ISNULL (AgentCompany.AgcEmail2, '')) <> ''
