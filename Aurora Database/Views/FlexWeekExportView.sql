USE [Aurora]
GO
/****** Object:  View [dbo].[FlexWeekExportView]    Script Date: 11/19/2007 13:35:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FlexWeekExportView] AS
SELECT 
	FlexBookingWeek.FbwId,
	FlexBookingWeek.FbwComCode,
	FlexBookingWeek.FbwBookStart,
	FlexBookingWeek.FbwTravelYearStart,
	FlexBookingWeek.FbwTravelYearEnd,
	FlexBookingWeek.FbwVersion,
	FlexBookingWeek.FbwStatus,
	FlexBookingWeek.FbwEmailText,

	FlexExportType.FlxId,
	FlexExportType.FlxCode,
	FlexExportType.FlxName,
	FlexExportType.FlxFileName,
	FlexExportType.FlxEmailText,
	FlexExportType.FlxPerTravelYear,
	FlexExportType.FlxComCode,
	FlexExportType.FlxCtyCode,
	FlexExportType.FlxBrdCode,
	FlexExportType.FlxClaID,
	FlexExportType.FlxTypID,
	FlexExportType.FlxPrdId,
	FlexExportType.FlxScheduleHoursBefore,
	FlexExportType.FlxIsFtp,
	FlexExportType.FlxFtpUri,
	FlexExportType.FlxFtpUserName,
	FlexExportType.FlxFtpPassword,
	FlexExportType.FlxIsAurora,
	FlexExportType.FlxIsActive,

	FlexBookingWeekExport.FwxId,
	FlexBookingWeekExport.FwxFbwId,
	FlexBookingWeekExport.FwxFlxId,
	FlexBookingWeekExport.FwxStatus,
	FlexBookingWeekExport.FwxTravelYear,
	FlexBookingWeekExport.FwxText,
	FlexBookingWeekExport.FwxContactsDescription,
	FlexBookingWeekExport.FwxScheduledTime,
	FlexBookingWeekExport.IntegrityNo,
	FlexBookingWeekExport.AddUsrId,
	FlexBookingWeekExport.ModUsrId,
	FlexBookingWeekExport.AddDateTime,
	FlexBookingWeekExport.ModDateTime,
	FlexBookingWeekExport.AddPrgmName,

	FlexBookingWeekExportCounts.ProductCount
FROM
	FlexBookingWeek
	INNER JOIN FlexExportType 
		ON FlexExportType.FlxComCode = FlexBookingWeek.FbwComCode
	INNER JOIN FlexBookingWeekExport
		ON FlexBookingWeekExport.FwxFlxId = FlexExportType.FlxId
		 AND FlexBookingWeekExport.FwxFbwId = FlexBookingWeek.FbwId
	INNER JOIN 
	(
		SELECT 
			FlexBookingWeekExport.FwxId,
			COUNT (FlexWeekProductView.FlpId) AS ProductCount
		FROM 
			FlexBookingWeek
			INNER JOIN FlexExportType 
				ON FlexExportType.FlxComCode = FlexBookingWeek.FbwComCode
			INNER JOIN FlexBookingWeekExport
				ON FlexBookingWeekExport.FwxFlxId = FlexExportType.FlxId
				 AND FlexBookingWeekExport.FwxFbwId = FlexBookingWeek.FbwId
			lEFT JOIN FlexWeekProductView
				ON FlexWeekProductView.FbwId = FlexBookingWeekExport.FwxFbwId
				AND FlexWeekProductView.FlpStatus = 'approved'
				AND FlexWeekProductView.ComCode = FlexExportType.FlxComCode
				AND (FlexWeekProductView.CtyCode = FlexExportType.FlxCtyCode OR FlexExportType.FlxCtyCode IS NULL)
				AND (FlexWeekProductView.BrdCode = FlexExportType.FlxBrdCode OR FlexExportType.FlxBrdCode IS NULL)
				AND (FlexWeekProductView.ClaID = FlexExportType.FlxClaID OR FlexExportType.FlxClaID IS NULL)
				AND (FlexWeekProductView.TypID = FlexExportType.FlxTypID OR FlexExportType.FlxTypID IS NULL)
				AND (FlexWeekProductView.PrdId = FlexExportType.FlxPrdId OR FlexExportType.FlxPrdId IS NULL)
				AND (FlexWeekProductView.TravelYear = FlexBookingWeekExport.FwxTravelYear OR FlexBookingWeekExport.FwxTravelYear IS NULL)
		GROUP BY
			FlexBookingWeekExport.FwxId
	) AS FlexBookingWeekExportCounts ON FlexBookingWeekExportCounts.FwxId = FlexBookingWeekExport.FwxId



/*
ORDER BY
	FlexExportType.FlxCode,
	FlexWeekProductView.CtyCode,
	FlexWeekProductView.ComCode,
	FlexWeekProductView.BrdCode,
	FlexWeekProductView.PrdShortName
*/
