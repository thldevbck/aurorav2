USE [Aurora]
GO
/****** Object:  View [dbo].[FlexWeekProductLocationAvailabilityView]    Script Date: 11/19/2007 13:35:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[FlexWeekProductLocationAvailabilityView] AS

SELECT 
	FlexBookingWeek.FbwId,
	FlexBookingWeekProduct.FlpId,
	FlexProductsView.ComCode,
	Country.CtyCode,
	FlexProductsView.BrdCode,
	FlexBookingWeekProduct.FlpTravelYear AS TravelYear,
	FlexProductsView.PrdId,
	FlexProductsView.PrdShortName,

	LocationAvailability.LcaFromLocCode,
	LocationAvailability.LcaToLocCode,
	LocationAvailability.LcaIsNotAvailable
FROM 
	FlexBookingWeek
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = FlexBookingWeek.FbwId
	INNER JOIN FlexProductsView 
		ON FlexProductsView.ComCode = FlexBookingWeekProduct.FlpComCode
		AND FlexProductsView.BrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND FlexProductsView.PrdId = FlexBookingWeekProduct.FlpPrdId 
	INNER JOIN Country
		ON Country.CtyCode = FlexBookingWeekProduct.FlpCtyCode  
	INNER JOIN FlexPackageView
		ON FlexPackageView.PrdId = FlexProductsView.PrdId 
		AND FlexPackageView.CtyCode = Country.CtyCode  	
		AND FlexPackageView.BrdCode = FlexProductsView.BrdCode
	INNER JOIN LocationAvailability 
		ON LocationAvailability.LcaSapId = FlexPackageView.SapId
WHERE
	--FlexBookingWeek.FbwId = 212 AND 
	(1=1)
	AND FlexPackageView.PkgBookedFromDate < DATEADD (d, 7, FlexBookingWeek.FbwBookStart)
	AND FlexPackageView.PkgBookedToDate >= FlexBookingWeek.FbwBookStart
	AND FlexPackageView.PkgTravelFromDate < DATEADD (y, 1, FlexBookingWeekProduct.FlpTravelYear)
	AND FlexPackageView.PkgTravelToDate >= FlexBookingWeekProduct.FlpTravelYear

GROUP BY
	FlexBookingWeek.FbwId,
	FlexBookingWeekProduct.FlpId,
	FlexProductsView.ComCode,
	Country.CtyCode,
	FlexProductsView.BrdCode,
	FlexBookingWeekProduct.FlpTravelYear,
	FlexProductsView.PrdId,
	FlexProductsView.PrdShortName,

	LocationAvailability.LcaFromLocCode,
	LocationAvailability.LcaToLocCode,
	LocationAvailability.LcaIsNotAvailable
