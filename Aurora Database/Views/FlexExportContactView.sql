USE [Aurora]
GO
/****** Object:  View [dbo].[FlexExportContactView]    Script Date: 11/19/2007 13:34:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FlexExportContactView] AS
SELECT 
	FlexExportType.FlxComCode AS ComCode,

	FlexExportType.FlxId,
	FlexExportType.FlxCode,
	FlexExportType.FlxName,
	FlexExportType.FlxEmailText,
	FlexExportType.FlxIsActive,

	FlexExportContact.FxcId,

	FlexAgentEmailView.AgnId,
	FlexAgentEmailView.AgnCode,
	FlexAgentEmailView.AgnName,
	FlexAgentEmailView.AgnIsActive,
	FlexAgentEmailView.AgcEmail,

	UserInfo.UsrId,
	UserInfo.UsrCode,
	UserInfo.UsrName,
	UserInfo.UsrIsActive,
	UserInfo.UsrEmail,

	CASE 
		WHEN FlexAgentEmailView.AgnId IS NOT NULL THEN FlexAgentEmailView.AgnCode
		WHEN UserInfo.UsrId IS NOT NULL THEN UserInfo.UsrCode
		ELSE NULL
	END AS Code,
	CASE 
		WHEN FlexAgentEmailView.AgnId IS NOT NULL THEN FlexAgentEmailView.AgnName
		WHEN UserInfo.UsrId IS NOT NULL THEN UserInfo.UsrName
		ELSE NULL
	END AS Name,
	CASE 
		WHEN FlexAgentEmailView.AgnId IS NOT NULL THEN FlexAgentEmailView.AgcEmail
		WHEN UserInfo.UsrId IS NOT NULL THEN UserInfo.UsrEmail
		ELSE NULL
	END AS Email,
	CASE 
		WHEN FlexExportType.FlxIsActive = 1 AND FlexAgentEmailView.AgnId IS NOT NULL THEN FlexAgentEmailView.AgnIsActive 
		WHEN FlexExportType.FlxIsActive = 1 AND UserInfo.UsrId IS NOT NULL THEN UserInfo.UsrIsActive
		ELSE 0
	END AS IsActive
	
FROM
	FlexExportType 
	INNER JOIN FlexExportContact
		ON FlexExportContact.FxcFlxId = FlexExportType.FlxId
	LEFT JOIN FlexAgentEmailView 
		ON FlexAgentEmailView.AgcComCode = FlexExportType.FlxComCode
		AND FlexAgentEmailView.AgcAgnId = FlexExportContact.FxcAgnId
	LEFT JOIN UserCompany 
		ON UserCompany.ComCode = FlexExportType.FlxComCode
		AND UserCompany.UsrId = FlexExportContact.FxcUsrId
	LEFT JOIN UserInfo
		ON UserInfo.UsrId = UserCompany.UsrId
WHERE
	(FlexAgentEmailView.AgnId IS NOT NULL OR UserInfo.UsrId IS NOT NULL)
