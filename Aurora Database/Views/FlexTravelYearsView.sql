USE [Aurora]
GO
/****** Object:  View [dbo].[FlexTravelYearsView]    Script Date: 11/19/2007 13:35:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FlexTravelYearsView] AS

SELECT 
	FlexWeekNumber.FwnTrvYearStart AS TravelYear
FROM	
	FlexWeekNumber 
GROUP BY 
	FlexWeekNumber.FwnTrvYearStart
