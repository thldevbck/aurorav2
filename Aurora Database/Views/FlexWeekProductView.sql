USE [Aurora]
GO
/****** Object:  View [dbo].[FlexWeekProductView]    Script Date: 11/19/2007 13:36:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[FlexWeekProductView] AS
SELECT 
	FlexBookingWeek.FbwId,
	FlexBookingWeek.FbwBookStart,
	FlexBookingWeek.FbwTravelYearStart,
	FlexBookingWeek.FbwTravelYearEnd,
	FlexBookingWeek.FbwVersion,
	FlexBookingWeek.FbwStatus,
	FlexBookingWeekProduct.FlpId,
	FlexProductsView.ComCode,
	FlexProductsView.ComName,
	Country.CtyCode,
	Country.CtyName,
	FlexProductsView.BrdCode,
	FlexProductsView.BrdName,
	FlexProductsView.ClaId,
	FlexProductsView.ClaCode,
	FlexProductsView.ClaDesc,
	FlexProductsView.TypId,
	FlexProductsView.TypCode,
	FlexProductsView.TypDesc,
	FlexBookingWeekProduct.FlpTravelYear AS TravelYear,
	FlexProductsView.PrdId,
	FlexProductsView.PrdShortName,
	FlexProductsView.PrdName,
	FlexProductsView.PrdDesc,
	FlexProductsView.IsActive AS IsActive,
	FlexBookingWeekProduct.FlpStatus,
	FlexBookingWeekProduct.FlpLocationsInclude,
	FlexBookingWeekProduct.FlpLocationsExclude,
	FlexBookingWeekProduct.IntegrityNo,
	FlexBookingWeekProduct.AddUsrId,
	FlexBookingWeekProduct.ModUsrId,
	FlexBookingWeekProduct.AddDateTime,
	FlexBookingWeekProduct.ModDateTime,
	FlexBookingWeekProduct.AddPrgmName
FROM 
	FlexBookingWeek
	INNER JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = FlexBookingWeek.FbwId
	INNER JOIN FlexProductsView 
		ON FlexProductsView.ComCode = FlexBookingWeekProduct.FlpComCode
		AND FlexProductsView.BrdCode = FlexBookingWeekProduct.FlpBrdCode
		AND FlexProductsView.PrdId = FlexBookingWeekProduct.FlpPrdId 
	INNER JOIN Country
		ON Country.CtyCode = FlexBookingWeekProduct.FlpCtyCode  

/*
ORDER BY
	FlexBookingWeek.FbwBookStart,
	FlexProductsView.ComCode,
	FlexTravelYearsView.TravelYear,
	Country.CtyCode,
	FlexProductsView.BrdCode,
	FlexProductsView.PrdShortName
*/
