USE [Aurora]
GO
/****** Object:  View [dbo].[FlexProductsView]    Script Date: 11/19/2007 13:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FlexProductsView]
AS
SELECT 
	Company.ComCode,
	Company.ComName,
	Brand.BrdCode,
	Brand.BrdName,
	Class.ClaId,
	Class.ClaCode,
	Class.ClaDesc,
	Type.TypId,
	Type.TypCode,
	Type.TypDesc,
	Product.PrdId,
	Product.PrdShortName,
	Product.PrdName,
	Product.PrdShortName + ' - ' + Product.PrdName AS PrdDesc,
	Product.PrdIsActive AS PrdIsActive,
	Product.PrdToAppearOnFS AS PrdToAppearOnFS,
	CASE WHEN Product.PrdIsActive = 1 AND Product.PrdToAppearOnFS = 1 THEN 1 ELSE 0 END AS IsActive
FROM
	Product 
	INNER JOIN Brand AS ProductBrand ON Product.PrdBrdCode = ProductBrand.BrdCode
	INNER JOIN Brand ON Brand.BrdCode = ProductBrand.BrdCode OR ProductBrand.BrdIsGeneric = 1
	INNER JOIN Company ON Company.ComCode = Brand.BrdComCode 
	INNER JOIN Type ON Type.TypId = Product.PrdTypId
	INNER JOIN Class ON Class.ClaID = Type.TypClaId
WHERE
	Product.PrdIsVehicle = 1
