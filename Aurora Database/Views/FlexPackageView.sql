
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[FlexPackageView] AS
SELECT 
	SaleableProduct.SapPrdId AS PrdId,
	SaleableProduct.SapId,
	SaleableProduct.SapCtyCode AS CtyCode,
	SaleableProduct.SapSuffix,
	SaleableProduct.SapStatus,

	Package.PkgBrdCode AS BrdCode,
	Package.PkgId,
	Package.PkgCode,
	Package.PkgName,
	Package.PkgCode + ' - ' + Package.PkgName AS PkgDesc,
	Package.PkgIsActive,
    Package.PkgBookedFromDate,
    Package.PkgBookedToDate,
    Package.PkgTravelFromDate,
    Package.PkgTravelToDate,

	CASE WHEN SaleableProduct.SapStatus = 'Active' AND Package.PkgIsActive = 'Active' THEN 1 ELSE 0 END AS IsActive -- more friendly and standard, bit value
FROM
	SaleableProduct 
	INNER JOIN PackageProduct 
		ON PackageProduct.PplSapId = SaleableProduct.SapId 
	INNER JOIN Package 
		ON Package.PkgId = PackageProduct.PplPkgId
	INNER JOIN Code 
		ON Code.CodId = Package.PkgCodTypId
		AND Code.CodCode = 'Flex'
--WHERE
--	EXISTS (SELECT 1 FROM FlexLevel WHERE FlexLevel.FlxPkgId = Package.PkgId AND FlexLevel.FlxSapId = SaleableProduct.SapId)
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

