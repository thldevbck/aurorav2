---------------------------------------------------------------------------------------------------------------------
-- this script is intended for credit card security enhancement
-- REV:MIA 01Aug - created
---------------------------------------------------------------------------------------------------------------------




---------------------------------------------------------------------------------------------------------------------


/* 
			Summary: this will retrieve the permission of a user when navigating the NoteUsercontrol
					This is primarily use in the Note Usercontrol

*/




begin transaction


declare @newID uniqueidentifier
set @newID = newid()

declare @CodCode varchar(64)
declare @CodDesc varchar(64)

--Note: Change these when deploying
set @CodCode = 'Card'				       -- change this to 'Card'
set @CodDesc = 'Credit Card'                           -- change this 'Credit Card'

---------------------------------------------------------------------------------------------------------------------

/* 
			Summary: Add credit card to the code table w/c will appear on the Note Usercontrol popup
					 
*/

INSERT INTO CODE 
			( 
				CodId,
				CodCdtNum,
				CodCode,
				CodDesc,
				IntegrityNo,
				CodOrder,
				CodIsActive,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES 
			(
				newid(),
				13,
				@CodCode,
				@CodDesc,
				1,
				12, 
				1,
				'ma2', 
				getDate(),
				'Backend'
			) 			 	
   	     --Go

---------------------------------------------------------------------------------------------------------------------

set @CodCode = 'Hold Imprint'		
set @CodDesc = 'Hold Imprint'		


insert into code 
			(
			    CodId, 
				CodCdtNum, 
				CodCode,
				CodDesc,
				IntegrityNo,
				CodOrder,
				CodIsActive,
				AddUsrId, 
				ModUsrId,
				AddPrgmName,
				AddDateTime, ModDateTime
			 )
             values	
			 (
					@newID,
				    39, 
					@CodCode, 
					@CodDesc, 
					1, 
					80,
					1,
					'ma2',
					'ma2',
				    'Backend',
					getDate(),
					getDate()
			)
			--Go

--insert MasterCardHoldImprint
--Note: Change these when deploying
declare @MasterCardHoldImprint varchar(64)
set     @MasterCardHoldImprint = 'MasterCard Hold Imprint'     -- change this to 'MasterCard Hold Imprint'

INSERT INTO PaymentMethod
			(
				PtmId,
				PtmCloseoffDispSeq,
				PtmName,
				PtmCodCtgyId,
				PtmIsLocal,
				PtmIsCreditCard,
				PtmIsCash,
				PtmIsTravChq,
				PtmIsPersChq,
				PtmIsPostedToFinanc,
				PtmIsRefund,
				PtmPrdShortName,
				PtmIsActive,
				IntegrityNo,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES
			(
				newid(),			   
				0,                     
				@MasterCardHoldImprint,
				@newID,
				1,
				1,
				0,
				0,
				0,
				1,
				0,
				'',
				1,
				1,
				'ma2',
				getDate(),			  
				'Backend'
			)
			--Go

--insert DinersHoldImprint
declare @DinersHoldImprint     varchar(64)
set     @DinersHoldImprint =     'Diners Hold Imprint'         -- change this to 'Diners Hold Imprint'


INSERT INTO PaymentMethod
			(
				PtmId,
				PtmCloseoffDispSeq,
				PtmName,
				PtmCodCtgyId,
				PtmIsLocal,
				PtmIsCreditCard,
				PtmIsCash,
				PtmIsTravChq,
				PtmIsPersChq,
				PtmIsPostedToFinanc,
				PtmIsRefund,
				PtmPrdShortName,
				PtmIsActive,
				IntegrityNo,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES
			(
				newid(),			   
				0,                     
				@DinersHoldImprint,
				@newID,
				1,
				1,
				0,
				0,
				0,
				1,
				0,
				'',
				1,
				1,
				'ma2',
				getDate(),			  
				'Backend'
			)
			--Go

--insert VisaHoldImprint
declare @VisaHoldImprint       varchar(64)
set     @VisaHoldImprint =        'Visa Hold Imprint'           -- change this to 'Visa Hold Imprint'


INSERT INTO PaymentMethod
			(
				PtmId,
				PtmCloseoffDispSeq,
				PtmName,
				PtmCodCtgyId,
				PtmIsLocal,
				PtmIsCreditCard,
				PtmIsCash,
				PtmIsTravChq,
				PtmIsPersChq,
				PtmIsPostedToFinanc,
				PtmIsRefund,
				PtmPrdShortName,
				PtmIsActive,
				IntegrityNo,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES
			(
				newid(),			   
				0,                     
				@VisaHoldImprint,
				@newID,
				1,
				1,
				0,
				0,
				0,
				1,
				0,
				'',
				1,
				1,
				'ma2',
				getDate(),			  
				'Backend'
			)
			--Go

--insert BankcardHoldImprintAU
declare @BankcardHoldImprintAU varchar(64)
set     @BankcardHoldImprintAU =  'Bankcard Hold Imprint (AU)'  -- change this to 'Bankcard Hold Imprint (AU)'

INSERT INTO PaymentMethod
			(
				PtmId,
				PtmCloseoffDispSeq,
				PtmName,
				PtmCodCtgyId,
				PtmIsLocal,
				PtmIsCreditCard,
				PtmIsCash,
				PtmIsTravChq,
				PtmIsPersChq,
				PtmIsPostedToFinanc,
				PtmIsRefund,
				PtmPrdShortName,
				PtmIsActive,
				IntegrityNo,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES
			(
				newid(),			   
				0,                     
				@BankcardHoldImprintAU,
				@newID,
				1,
				1,
				0,
				0,
				0,
				1,
				0,
				'',
				1,
				1,
				'ma2',
				getDate(),			  
				'Backend'
			)
			--Go


--insert AmexHoldImprint
declare @AmexHoldImprint	   varchar(64)
set     @AmexHoldImprint =  'Amex Hold Imprint'                 -- change this to 'Amex Hold Imprint'
INSERT INTO PaymentMethod
			(
				PtmId,
				PtmCloseoffDispSeq,
				PtmName,
				PtmCodCtgyId,
				PtmIsLocal,
				PtmIsCreditCard,
				PtmIsCash,
				PtmIsTravChq,
				PtmIsPersChq,
				PtmIsPostedToFinanc,
				PtmIsRefund,
				PtmPrdShortName,
				PtmIsActive,
				IntegrityNo,
				AddUsrId,
				AddDateTime,
				AddPrgmName
			)
			VALUES
			(
				newid(),			   
				0,                     
				@AmexHoldImprint,
				@newID,
				1,
				1,
				0,
				0,
				0,
				1,
				0,
				'',
				1,
				1,
				'ma2',
				getDate(),			  
				'Backend'
			)
			--Go

---------------------------------------------------------------------------------------------------------------------

IF @@ERROR <> 0
  -- There's an error b/c @ERROR is not 0, rollback
  ROLLBACK
ELSE
  COMMIT   -- 