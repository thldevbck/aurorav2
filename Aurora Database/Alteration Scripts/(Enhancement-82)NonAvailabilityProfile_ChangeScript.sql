/*
   Thursday, 7 August 200811:13:16 a.m.
   User: sa
   Server: AKL-KXDEV-001
   Database: Aurora
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.NonAvailabilityProfile ADD
	NapPrdId varchar(64) NULL
GO
COMMIT
