USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexBookingWeekException]    Script Date: 11/19/2007 13:29:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexBookingWeekException](
	[FleId] [int] IDENTITY(1,1) NOT NULL,
	[FleFlpId] [int] NOT NULL,
	[FleBookStart] [datetime] NOT NULL,
	[FleBookEnd] [datetime] NOT NULL,
	[FleTravelStart] [datetime] NOT NULL,
	[FleTravelEnd] [datetime] NOT NULL,
	[FleLocCodeFrom] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FleLocCodeTo] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FleDelta] [int] NOT NULL,
 CONSTRAINT [PK_FlexBookingWeekException] PRIMARY KEY CLUSTERED 
(
	[FleId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexBookingWeekException]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekException_FlexBookingWeekProduct] FOREIGN KEY([FleFlpId])
REFERENCES [dbo].[FlexBookingWeekProduct] ([FlpId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexBookingWeekException] CHECK CONSTRAINT [FK_FlexBookingWeekException_FlexBookingWeekProduct]