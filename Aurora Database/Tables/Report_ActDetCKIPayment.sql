USE [Aurora]
GO
/****** Object:  Table [reports].[Report_ActDetCKIPayment]    Script Date: 06/09/2008 17:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [reports].[Report_ActDetCKIPayment](
	[BookRentalIn] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Payment] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Curr] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CashBondAmount] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CashBondCurr] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF