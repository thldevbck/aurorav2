USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexProduct]    Script Date: 02/25/2008 09:57:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexProduct](
	[FxpId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FxpComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FxpCtyCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FxpBrdCode] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FxpPrdId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FxpParentId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FxpIsActive] [bit] NOT NULL CONSTRAINT [DF_FlexProduct_FxpIsActive]  DEFAULT (1),
	[FxpFTICode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FxpCAFCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FxpDERCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FxpDescription] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FlexProduct] PRIMARY KEY CLUSTERED 
(
	[FxpId] ASC
) ON [PRIMARY],
 CONSTRAINT [IX_FlexProduct] UNIQUE NONCLUSTERED 
(
	[FxpComCode] ASC,
	[FxpCtyCode] ASC,
	[FxpBrdCode] ASC,
	[FxpPrdId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexProduct_FlexProduct] FOREIGN KEY([FxpParentId])
REFERENCES [dbo].[FlexProduct] ([FxpId])
GO
ALTER TABLE [dbo].[FlexProduct] CHECK CONSTRAINT [FK_FlexProduct_FlexProduct]