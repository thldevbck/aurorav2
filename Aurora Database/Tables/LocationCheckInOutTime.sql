CREATE TABLE [dbo].[LocationCheckInOutTime](
	[lioLocCode] [varchar](12) NOT NULL,
	[lioDayOfWeek] [int] NOT NULL,
	[lioCkiFromTime] [datetime] NULL,
	[lioCkiToTime] [datetime] NULL,
	[lioCkoFromTime] [datetime] NULL,
	[lioCkoToTime] [datetime] NULL,
	[IntegrityNo] [tinyint] NOT NULL,
	[AddUsrId] [varchar](64) NOT NULL,
	[ModUsrId] [varchar](64) NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) NOT NULL,
 CONSTRAINT [PK_LocationCheckInOutTime] PRIMARY KEY CLUSTERED
(
	[lioLocCode] ASC,
	[lioDayOfWeek] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[LocationCheckInOutTime]  WITH NOCHECK ADD  CONSTRAINT [FK_LocationCheckInOutTime_Location] FOREIGN KEY([lioLocCode])
REFERENCES [dbo].[Location] ([LocCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LocationCheckInOutTime] CHECK CONSTRAINT [FK_LocationCheckInOutTime_Location]
GO
