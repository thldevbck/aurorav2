USE [Aurora]
GO
/****** Object:  Table [dbo].[TableEditorEntity]    Script Date: 11/19/2007 13:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableEditorEntity](
	[teeId] [int] IDENTITY(1,1) NOT NULL,
	[tefId] [int] NOT NULL,
	[teeParentId] [int] NULL,
	[teeName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[teeDescription] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeTableName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[teeSelectSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeMustFilter] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeMustFilter]  DEFAULT (1),
	[teeUpdateable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeCanUpdate]  DEFAULT (1),
	[teeUpdateSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeUpdateableFieldName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeInsertable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeInsertable]  DEFAULT (1),
	[teeInsertSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeInsertableFieldName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeDeleteable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeDeleteable]  DEFAULT (1),
	[teeDeleteSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeDeleteableFieldName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeDefaultSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teeOrder] [int] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeOrder]  DEFAULT (100),
 CONSTRAINT [PK_TableEditorEntity] PRIMARY KEY CLUSTERED 
(
	[teeId] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
USE [Aurora]
GO
ALTER TABLE [dbo].[TableEditorEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorEntity_TableEditorEntity] FOREIGN KEY([teeParentId])
REFERENCES [dbo].[TableEditorEntity] ([teeId])
GO
ALTER TABLE [dbo].[TableEditorEntity] CHECK CONSTRAINT [FK_TableEditorEntity_TableEditorEntity]
GO
ALTER TABLE [dbo].[TableEditorEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorEntity_TableEditorFunction] FOREIGN KEY([tefId])
REFERENCES [dbo].[TableEditorFunction] ([tefId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TableEditorEntity] CHECK CONSTRAINT [FK_TableEditorEntity_TableEditorFunction]