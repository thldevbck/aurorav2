
/****** Object:  Table [dbo].[BookingStatus]    Script Date: 11/09/2007 16:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookingStatus](
	[BookingStatusId] [int] IDENTITY(1,1) NOT NULL,
	[BookingStatusCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BookingStatusName] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsActive] [bit] NOT NULL,
	[AddUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_BookingStatus] PRIMARY KEY CLUSTERED 
(
	[BookingStatusId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('IN','Inquiry',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('QN','Quotation',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('KB','Knock Back',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('WL','Wait Listed',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('NN','Provisional',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('KK','Confirmed',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('CO','Checked Out',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('CI','Checked In',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('XN','Cancelled Before Confirmation',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('XX','Cancelled After Confirmation',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')
INSERT INTO [BookingStatus]
           ([BookingStatusCode],[BookingStatusName],[IsActive],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
     VALUES('NC','Non-Cancelled',1,'D0E492C2-C947-449C-AE13-55C561B59497',null,getdate(),null,'SAVE')

