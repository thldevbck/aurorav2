USE [Aurora]
GO
/****** Object:  Table [dbo].[TableEditorAttribute]    Script Date: 11/19/2007 13:53:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableEditorAttribute](
	[teaId] [int] IDENTITY(1,1) NOT NULL,
	[tefId] [int] NOT NULL,
	[teeId] [int] NOT NULL,
	[teaParentId] [int] NULL,
	[teaName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[teaDescription] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teaFieldName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[teaIsPrimaryKey] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsPrimaryKey]  DEFAULT (0),
	[teaIsFilterable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsFilter]  DEFAULT (1),
	[teaIsListable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaList]  DEFAULT (1),
	[teaIsSortable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsSort]  DEFAULT (1),
	[teaListWidth] [nvarchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teaListAlign] [nvarchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teaIsViewable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanEdit]  DEFAULT (1),
	[teaIsUpdateable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanUpdate]  DEFAULT (1),
	[teaIsInsertable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanInsert]  DEFAULT (1),
	[teaIsNullable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsNullable]  DEFAULT (1),
	[teaIsBoolean] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsBoolean]  DEFAULT (0),
	[teaLookupSql] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teaTextRowCount] [int] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaTextRowCount]  DEFAULT (1),
	[teaTextWrap] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaTextWrap]  DEFAULT (1),
	[teaDefaultValue] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[teaOrder] [int] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaOrder]  DEFAULT (100),
 CONSTRAINT [PK_TableEditorAttribute] PRIMARY KEY CLUSTERED 
(
	[teaId] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
USE [Aurora]
GO
ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorAttribute] FOREIGN KEY([teaParentId])
REFERENCES [dbo].[TableEditorAttribute] ([teaId])
GO
ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorAttribute]
GO
ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorEntity] FOREIGN KEY([teeId])
REFERENCES [dbo].[TableEditorEntity] ([teeId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorEntity]
GO
ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorFunction] FOREIGN KEY([tefId])
REFERENCES [dbo].[TableEditorFunction] ([tefId])
GO
ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorFunction]