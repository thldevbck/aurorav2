USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexBookingWeek]    Script Date: 11/19/2007 13:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexBookingWeek](
	[FbwId] [int] IDENTITY(1,1) NOT NULL,
	[FbwComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FlexBooki__FbwCo__6329C8FC]  DEFAULT ('THL'),
	[FbwBookStart] [datetime] NOT NULL,
	[FbwTravelYearStart] [datetime] NOT NULL,
	[FbwTravelYearEnd] [datetime] NOT NULL,
	[FbwVersion] [int] NOT NULL,
	[FbwStatus] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FbwEmailText] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IntegrityNo] [tinyint] NOT NULL,
	[AddUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FlexBookingWeek] PRIMARY KEY CLUSTERED 
(
	[FbwId] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexBookingWeek]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeek_Company] FOREIGN KEY([FbwComCode])
REFERENCES [dbo].[Company] ([ComCode])
GO
ALTER TABLE [dbo].[FlexBookingWeek] CHECK CONSTRAINT [FK_FlexBookingWeek_Company]