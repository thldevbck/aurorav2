CREATE TABLE [dbo].[CompanySettings](
	[ComCode] [varchar](64) NOT NULL,
	[ComCulture] [varchar](12) NULL,
	[ComDateFormat] [varchar](24) NULL,
 CONSTRAINT [PK_CompanySettings] PRIMARY KEY CLUSTERED
(
	[ComCode] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CompanySettings]  WITH CHECK ADD  CONSTRAINT [FK_CompanySettings_Company] FOREIGN KEY([ComCode])
REFERENCES [dbo].[Company] ([ComCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
