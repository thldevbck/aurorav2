USE [Aurora]
GO
/****** Object:  Table [reports].[Report_ActDetCKOPayment]    Script Date: 06/06/2008 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [reports].[Report_ActDetCKOPayment](
	[BookRental] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Payment] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Curr] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF