USE [Aurora]
GO
/****** Object:  Table [reports].[Report_ActDetCKIProducts]    Script Date: 06/09/2008 17:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [reports].[Report_ActDetCKIProducts](
	[BookRentalIn] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrdNameIn] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrsIn] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Amounts] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF