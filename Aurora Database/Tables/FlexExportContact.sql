USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexExportContact]    Script Date: 11/19/2007 13:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexExportContact](
	[FxcId] [int] IDENTITY(1,1) NOT NULL,
	[FxcFlxId] [int] NOT NULL,
	[FxcAgnId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FxcUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_FlexExportContact] PRIMARY KEY CLUSTERED 
(
	[FxcId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexExportContact]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportContact_Agent] FOREIGN KEY([FxcAgnId])
REFERENCES [dbo].[Agent] ([AgnId])
GO
ALTER TABLE [dbo].[FlexExportContact] CHECK CONSTRAINT [FK_FlexExportContact_Agent]
GO
ALTER TABLE [dbo].[FlexExportContact]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportContact_FlexExportType] FOREIGN KEY([FxcFlxId])
REFERENCES [dbo].[FlexExportType] ([FlxId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexExportContact] CHECK CONSTRAINT [FK_FlexExportContact_FlexExportType]
GO
ALTER TABLE [dbo].[FlexExportContact]  WITH CHECK ADD  CONSTRAINT [FK_FlexExportContact_UserInfo] FOREIGN KEY([FxcUsrId])
REFERENCES [dbo].[UserInfo] ([UsrId])
ON DELETE CASCADE