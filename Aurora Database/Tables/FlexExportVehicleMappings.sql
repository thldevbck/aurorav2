USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexExportVehicleMappings]    Script Date: 11/19/2007 13:31:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexExportVehicleMappings](
	[FvmId] [int] IDENTITY(1,1) NOT NULL,
	[FvmFlxCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmCtyCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmBrdCode] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmTravelYear] [int] NULL,
	[FvmPrdShortName] [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmVehicleCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FvmDescription] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_FlexExportVehicleMappings] PRIMARY KEY CLUSTERED 
(
	[FvmId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF