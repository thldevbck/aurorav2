
CREATE TABLE ApplicationLog 
(
	 LogId int IDENTITY (1,1) PRIMARY KEY, 
	 EventID int, 
	 Category nvarchar(100) NULL,
	 Priority int, 
	 Severity nvarchar(32), 
	 Title nvarchar(256), 
	 Timestamp datetime,
	 MachineName nvarchar(32), 
	 AppDomainName nvarchar(512),
	 ProcessID nvarchar(256),
	 ProcessName nvarchar(512),
	 ThreadName nvarchar(512),
	 Win32ThreadId nvarchar(128),
	 Message nvarchar(1500),
	 FormattedMessage ntext
)
