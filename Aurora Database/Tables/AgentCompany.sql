USE [Aurora]
GO
/****** Object:  Table [dbo].[AgentCompany]    Script Date: 11/19/2007 13:33:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgentCompany](
	[AgcComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AgcAgnId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AgcEmail0] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AgcEmail1] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AgcEmail2] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_AgentCompany] PRIMARY KEY CLUSTERED 
(
	[AgcComCode] ASC,
	[AgcAgnId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF