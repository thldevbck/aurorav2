USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexBookingWeekExport]    Script Date: 11/19/2007 13:30:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexBookingWeekExport](
	[FwxId] [int] IDENTITY(1,1) NOT NULL,
	[FwxFbwId] [int] NOT NULL,
	[FwxFlxId] [int] NOT NULL,
	[FwxStatus] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FwxTravelYear] [datetime] NULL,
	[FwxText] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FwxContactsDescription] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FwxScheduledTime] [datetime] NOT NULL,
	[IntegrityNo] [tinyint] NOT NULL,
	[AddUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FlexBookingWeekExport] PRIMARY KEY CLUSTERED 
(
	[FwxId] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexBookingWeekExport]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekExport_FlexBookingWeek] FOREIGN KEY([FwxFbwId])
REFERENCES [dbo].[FlexBookingWeek] ([FbwId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexBookingWeekExport] CHECK CONSTRAINT [FK_FlexBookingWeekExport_FlexBookingWeek]
GO
ALTER TABLE [dbo].[FlexBookingWeekExport]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekExport_FlexExportType] FOREIGN KEY([FwxFlxId])
REFERENCES [dbo].[FlexExportType] ([FlxId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexBookingWeekExport] CHECK CONSTRAINT [FK_FlexBookingWeekExport_FlexExportType]