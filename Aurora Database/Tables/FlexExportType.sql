USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexExportType]    Script Date: 11/19/2007 13:31:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexExportType](
	[FlxId] [int] IDENTITY(1,1) NOT NULL,
	[FlxCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlxName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlxFileName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlxEmailText] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlxPerTravelYear] [bit] NOT NULL CONSTRAINT [DF_FlexExportType_FlxPerTravelYear]  DEFAULT (1),
	[FlxComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlxCtyCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxBrdCode] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxClaID] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxTypID] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxPrdId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxScheduleHoursBefore] [int] NOT NULL,
	[FlxIsFtp] [bit] NOT NULL CONSTRAINT [DF_FlexExportType_FlxIsFtp]  DEFAULT (0),
	[FlxFtpUri] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxFtpUserName] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxFtpPassword] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlxIsAurora] [bit] NOT NULL CONSTRAINT [DF_FlexExportType_FtpIsAurora]  DEFAULT (0),
	[FlxIsActive] [bit] NOT NULL CONSTRAINT [DF_FlexExportType_FlxIsActive]  DEFAULT (1),
	[IntegrityNo] [tinyint] NOT NULL,
	[AddUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FlexExportType] PRIMARY KEY CLUSTERED 
(
	[FlxId] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Brand] FOREIGN KEY([FlxBrdCode])
REFERENCES [dbo].[Brand] ([BrdCode])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Brand]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Class] FOREIGN KEY([FlxClaID])
REFERENCES [dbo].[Class] ([ClaID])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Class]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Company] FOREIGN KEY([FlxComCode])
REFERENCES [dbo].[Company] ([ComCode])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Company]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Country] FOREIGN KEY([FlxCtyCode])
REFERENCES [dbo].[Country] ([CtyCode])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Country]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Product] FOREIGN KEY([FlxPrdId])
REFERENCES [dbo].[Product] ([PrdId])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Product]
GO
ALTER TABLE [dbo].[FlexExportType]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexExportType_Type] FOREIGN KEY([FlxTypID])
REFERENCES [dbo].[Type] ([TypId])
GO
ALTER TABLE [dbo].[FlexExportType] CHECK CONSTRAINT [FK_FlexExportType_Type]