USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexBookingWeekRate]    Script Date: 11/19/2007 13:30:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexBookingWeekRate](
	[FlrId] [int] IDENTITY(1,1) NOT NULL,
	[FlrFlpId] [int] NOT NULL,
	[FlrWkNo] [int] NOT NULL,
	[FlrTravelFrom] [datetime] NOT NULL,
	[FlrFlexNum] [int] NOT NULL,
	[FlrChanged] [bit] NOT NULL CONSTRAINT [DF_FlexBookingWeekRate_FlrFlexChange]  DEFAULT (0),
	[FlrFleetStatus] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_FlexBookingWeekRate] PRIMARY KEY CLUSTERED 
(
	[FlrId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexBookingWeekRate]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekRate_FlexBookingWeekProduct] FOREIGN KEY([FlrFlpId])
REFERENCES [dbo].[FlexBookingWeekProduct] ([FlpId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexBookingWeekRate] CHECK CONSTRAINT [FK_FlexBookingWeekRate_FlexBookingWeekProduct]