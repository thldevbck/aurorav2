USE [Aurora]
GO
/****** Object:  Table [dbo].[FlexBookingWeekProduct]    Script Date: 11/19/2007 13:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlexBookingWeekProduct](
	[FlpId] [int] IDENTITY(1,1) NOT NULL,
	[FlpFbwId] [int] NOT NULL,
	[FlpComCode] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlpCtyCode] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlpBrdCode] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlpTravelYear] [datetime] NOT NULL,
	[FlpPrdId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FlpStatus] [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlpLocationsInclude] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlpLocationsExclude] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IntegrityNo] [tinyint] NOT NULL,
	[AddUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModUsrId] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddDateTime] [smalldatetime] NOT NULL,
	[ModDateTime] [smalldatetime] NULL,
	[AddPrgmName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FlexBookingWeekProduct] PRIMARY KEY CLUSTERED 
(
	[FlpId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [Aurora]
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekProduct_Brand] FOREIGN KEY([FlpBrdCode])
REFERENCES [dbo].[Brand] ([BrdCode])
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct] CHECK CONSTRAINT [FK_FlexBookingWeekProduct_Brand]
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekProduct_Company] FOREIGN KEY([FlpComCode])
REFERENCES [dbo].[Company] ([ComCode])
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct] CHECK CONSTRAINT [FK_FlexBookingWeekProduct_Company]
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekProduct_Country] FOREIGN KEY([FlpCtyCode])
REFERENCES [dbo].[Country] ([CtyCode])
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct] CHECK CONSTRAINT [FK_FlexBookingWeekProduct_Country]
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekProduct_FlexBookingWeek] FOREIGN KEY([FlpFbwId])
REFERENCES [dbo].[FlexBookingWeek] ([FbwId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct] CHECK CONSTRAINT [FK_FlexBookingWeekProduct_FlexBookingWeek]
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct]  WITH NOCHECK ADD  CONSTRAINT [FK_FlexBookingWeekProduct_Product] FOREIGN KEY([FlpPrdId])
REFERENCES [dbo].[Product] ([PrdId])
GO
ALTER TABLE [dbo].[FlexBookingWeekProduct] CHECK CONSTRAINT [FK_FlexBookingWeekProduct_Product]