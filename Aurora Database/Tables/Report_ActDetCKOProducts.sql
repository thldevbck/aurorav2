USE [Aurora]
GO
/****** Object:  Table [reports].[Report_ActDetCKOProducts]    Script Date: 06/06/2008 16:56:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [reports].[Report_ActDetCKOProducts](
	[BookRental] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrdName] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Currs] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Amounts] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF