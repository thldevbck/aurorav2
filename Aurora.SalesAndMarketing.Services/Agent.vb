''rev:mia jan 07 2013 Call : 37671 - Change request - Section to search for agents to exclude from the bulk allocation in Aurora
Imports System.Xml
Imports Aurora.SalesAndMarketing.Data
Imports Aurora.Common

Public Class Agent

    ''rev:mia Jan 7 2013 - change for this year. addtion of AgnCodName, PackageId
    Public Shared Function FindBulkAgent(ByVal Country As String, ByVal Group As String, ByVal MarketCode As String, ByVal AgentType As String, ByVal Category As String, _
                                         Optional AgnCodName As String = "", Optional PackageId As String = "") As XmlDocument
        If (Not String.IsNullOrEmpty(AgnCodName)) Then
            If (AgnCodName.Contains(",") = True) Then
                Dim codes() As String = AgnCodName.Split(",")
                Dim item As String = ""
                For Each code As String In codes
                    item = String.Concat(item, code.Split("-")(0).Trim, ",")
                Next
                If (item.EndsWith(",") = True) Then
                    item = item.Remove(item.Length - 1, 1)
                End If
                AgnCodName = item
            Else
                AgnCodName = AgnCodName.Split("-")(0).Trim
            End If

        End If
        Return DataRepository.FindBulkAgent(Country, Group, MarketCode, AgentType, Category, AgnCodName, PackageId)
    End Function


    Public Shared Function GetClassTypes() As DataTable
        Return DataRepository.GetClassTypes()
    End Function

    Public Shared Function GetBrands(ByVal UserCode As String) As DataTable
        Dim dstResults As DataSet

        dstResults = Aurora.Common.Data.GetData("BRAND", "", "", UserCode)
        If dstResults.Tables.Count > 0 Then
            Return dstResults.Tables(0)
        Else
            Return (New DataTable)
        End If
    End Function


#Region "PRODUCT STUFF"

    Public Shared Function GetBulkProduct(ByVal AllProduct As Boolean, ByVal ProductShortName As String, ByVal Brand As String, ByVal [Class] As String, ByVal Type As String, ByVal UserCode As String) As XmlDocument
        Return DataRepository.GetBulkProduct(AllProduct, ProductShortName, Brand, [Class], Type, UserCode)
    End Function

    Public Shared Function SaveBulkAgentProducts(ByVal ScreenData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String) As XmlDocument

        Dim oResult As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                'main part
                Dim sReturnedMessage As String = Nothing
                For Each oAgentDetailNode As XmlNode In ScreenData.DocumentElement.SelectSingleNode("AgentDetail").ChildNodes
                    Dim sAgentProductXml As String
                    sAgentProductXml = "<Root>" & _
                                            ScreenData.DocumentElement.SelectSingleNode("PD").OuterXml & _
                                            ScreenData.DocumentElement.SelectSingleNode("CrDt").OuterXml & _
                                            "<AgnId>" & oAgentDetailNode.Attributes("id").InnerText & "</AgnId>" & _
                                            "<AddModUserId>" & UserCode & "</AddModUserId>" & _
                                            "<AddProgramName>" & ProgramName & "</AddProgramName>" & _
                                            ScreenData.DocumentElement.SelectSingleNode("ProductDetail").OuterXml & _
                                       "</Root>"

                    sReturnedMessage = DataRepository.SaveBulkAgentProducts(sAgentProductXml)
                    'If Not sReturnedMessage.Equals(String.Empty) Then
                    If InStr(1, sReturnedMessage, "GEN106") = 0 Then
                        Throw (New Exception(sReturnedMessage))
                    End If
                    'If sReturnedMessage.Contains("GEN106") Then

                    'End If
                Next

                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                oResult.LoadXml("<Error><ErrorMessage>" & sReturnedMessage & "</ErrorMessage><ErrorType>" & "GEN106" & "</ErrorType></Error>")

                'main part

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Dim sErrorType, sErrorMessage As String
                If ex.Message.Contains("Application") Then
                    'application
                    sErrorMessage = ex.Message.Replace("Application/", "")
                    sErrorType = "Application"
                Else
                    sErrorMessage = ex.Message
                    sErrorType = "Other"
                End If
                oResult.LoadXml("<Error><ErrorMessage>" & sErrorMessage & "</ErrorMessage><ErrorType>" & sErrorType & "</ErrorType></Error>")
            End Try
        End Using


        Return oResult


    End Function

#End Region

#Region "PACKAGE STUFF"
    ''rev:mia Jan 7 2013 - change for this year. addtion of AgnCodName
    Public Shared Function GetBulkPackages(ByVal AllPackages As Boolean, _
                                            ByVal PackageCode As String, _
                                            ByVal CountryCode As String, _
                                            ByVal BrandCode As String, _
                                            ByVal Type As String, _
                                            ByVal TravelFromDate As String, _
                                            ByVal TravelToDate As String, _
                                            ByVal BookedFromDate As String, _
                                            ByVal BookedToDate As String, _
                                            ByVal UserCode As String, _
                                            Optional AgnCodName As String = "") As XmlDocument

        If (Not String.IsNullOrEmpty(AgnCodName)) Then
            AgnCodName = AgnCodName.Split("-")(0).Trim
        End If
        Return DataRepository.GetBulkPackages(AllPackages, PackageCode, CountryCode, BrandCode, Type, TravelFromDate, TravelToDate, BookedFromDate, BookedToDate, UserCode, AgnCodName)

    End Function
    'exec sp_getBulkAvailablePackages 
    '@bAllPackages = '0', 
    '@pkgCode = '', 
    '@pkgCtyCode = 'NZ', 
    '@pkgBrdCode = 'B', 
    '@PkgCodTypId = '6ABB1B42-2066-42C8-97FD-2B71B6CEE182', 
    '@pkgTravelFromDate = '01/01/2001', 
    '@pkgTravelToDate = '02/02/2002', 
    '@pkgBookedFromDate = '03/03/2003', 
    '@pkgBookedToDate = '04/04/2004', 
    '@sUsrCode = 'sp7'

    Public Shared Function GetPackageTypes() As DataTable
        Dim dstTypes As DataSet
        dstTypes = Aurora.Common.Data.GetCodecodetype(14, "")
        If dstTypes.Tables.Count > 0 Then
            Return dstTypes.Tables(0)
        Else
            Return (New DataTable)
        End If
    End Function

    Public Shared Function GetCountriesOfOperation() As DataTable
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.GetCountriesOfOperation()
        Dim dstCountries As New DataSet
        dstCountries.ReadXml(New XmlNodeReader(xmlDoc))
        If dstCountries.Tables.Count > 0 Then
            Return (dstCountries.Tables(0))
        Else
            Return (New DataTable)
        End If


    End Function


    Public Shared Function GetPackageDetails(ByVal PackageId As String, ByVal UserCode As String) As XmlDocument
        Dim xmlDoc As XmlDocument
        xmlDoc = DataRepository.GetPackageDetails(PackageId, UserCode)

        xmlDoc.LoadXml(xmlDoc.DocumentElement.SelectSingleNode("Packages/Package").OuterXml)
        'returned xml <Package Code="25ANNI" Name="25TH ANNIVERSARY SPECIAL FOR MEIERS" Brand="B" Type="" 
        'AgnType="" AgnGroup="" AgnCode="" Country="NZ" Currency="NZD" Relocation="0" DisPrice="1" Status="Active" 
        'Comments="SPECIAL RATES FOR 2BB FOR MEIERS CUSTOMERS ONLY.  MINIMUM HIRE 18 DAYS. NO LONG HIRE DISCOUNT APPLIES.  ADDED VALUE:  1 PICNIC TABLE, 2 PICNIC CHAIRS, 1 X HAMNER SPRINGS ADULT PASS, 1 X KELLY TARLTONS ADULT PASS, 1 X WAITOMO CAVES ADULT PASS." 
        'BkFrom="23/10/2004" BkTo="14/03/2006" TrFrom="01/04/2005" TrTo="31/03/2006" Extra="0.0000" AgDiscOvr="" DepoAmt="0.0000" 
        'DepoPerc="0.0000" ExtBaseRate="0" ChargeAtCoRate="0" PkgDesc="SPECIAL RATES FOR 2BB ONLY" LumpSumDesc="" 
        'IsSortB4Veh="1" IntegrityNo="7" />

        'to be formatted as 
        '<Packages>
        '  <Package>
        '   <PkgId>8748A7CE-8284-4F19-931F-75221CDB1AAB</PkgId> 
        '   <PkgDesc>STDM - Standard Motorhome Rates - NZ</PkgDesc> 
        '   <BkFr>01/03/2001</BkFr> 
        '   <BkTo>27/03/2008</BkTo> 
        '   <TrFr>01/03/2001</TrFr> 
        '   <TrTo>31/03/2008</TrTo> 
        '   <Remove>0</Remove> 
        '  </Package>
        '</Packages>
        xmlDoc.LoadXml("<Packages>" & _
                                        "<Package>" & _
                                            "<PkgId>" & PackageId & "</PkgId>" & _
                                            "<PkgDesc>" & xmlDoc.SelectSingleNode("Package").Attributes("Name").InnerText & "</PkgDesc>" & _
                                            "<BkFr>" & xmlDoc.SelectSingleNode("Package").Attributes("BkFrom").InnerText & "</BkFr>" & _
                                            "<BkTo>" & xmlDoc.SelectSingleNode("Package").Attributes("BkTo").InnerText & "</BkTo>" & _
                                            "<TrFr>" & xmlDoc.SelectSingleNode("Package").Attributes("TrFrom").InnerText & "</TrFr>" & _
                                            "<TrTo>" & xmlDoc.SelectSingleNode("Package").Attributes("TrTo").InnerText & "</TrTo>" & _
                                            "<Remove>0</Remove> " & _
                                        "</Package>" & _
                                  "</Packages>")

        Return xmlDoc

    End Function

    Public Shared Function SaveBulkAgentPackages(ByVal ScreenData As XmlDocument) As XmlDocument

        Dim oResult As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                'main part
                Dim sReturnedMessage As String

                sReturnedMessage = DataRepository.SaveBulkAgentPackages(ScreenData.OuterXml)
                'If Not sReturnedMessage.Equals(String.Empty) Then
                If InStr(1, sReturnedMessage, "GEN106") = 0 Then
                    Throw (New Exception("Application/" & sReturnedMessage))
                End If
         
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                oResult.LoadXml("<Error><ErrorMessage>" & sReturnedMessage & "</ErrorMessage><ErrorType>" & "GEN106" & "</ErrorType></Error>")

                'main part

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Dim sErrorType, sErrorMessage As String
                If ex.Message.Contains("Application") Then
                    'application
                    sErrorMessage = ex.Message.Replace("Application/", "")
                    sErrorType = "Application"
                Else
                    sErrorMessage = ex.Message
                    sErrorType = "Other"
                End If
                oResult.LoadXml("<Error><ErrorMessage>" & sErrorMessage & "</ErrorMessage><ErrorType>" & sErrorType & "</ErrorType></Error>")
            End Try
        End Using


        Return oResult


    End Function

#End Region

#Region "rev:mia Feb. 1 2013 - part of agent bulk loading"
    Public Shared Function GetPopUpDataForAgentBulkloading(ByVal poptype As String, _
                                                    ByVal userCode As String _
                                                    ) As DataSet
        Dim pop As Popups.Data.PopupType
        Dim agentPopup As Boolean = False
        Select Case poptype
            Case "AGNENTCOUNTRY"
                pop = Popups.Data.PopupType.AGNENTCOUNTRY
            Case "MARKETCODE"
                ''Agent_GetPopUpData 'MARKETCODE', '', '', '', '', '', 'ma2'
                pop = Popups.Data.PopupType.MARKETCODE
                agentPopup = True
            Case "PACKAGEBULK"
                pop = Popups.Data.PopupType.PACKAGEBULK
        End Select

        If (agentPopup = False) Then
            Return Aurora.Common.Data.GetPopUpData(poptype, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               userCode)
        Else
            Dim ds As New DataSet
            ds.ReadXml(New XmlNodeReader(Aurora.Common.Data.GetPopUpData("Agent_GetPopUpData", _
                                                  pop, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               String.Empty, _
                                               userCode)))

            Return ds
        End If
        
    End Function
#End Region

End Class
