﻿Imports System.Xml
Imports Aurora.SalesAndMarketing.Data
Imports Aurora.Common
Imports Aurora.Common.Logging


Public Class SlotManagement
    Public Shared Function GetSlotManagementData(SlotCtyCode As String, LocSlotLocCode As String, sUsrCode As String) As DataSet

        Dim result As New DataSet
        Try
            Logging.LogDebug("GetSlotManagementData", String.Format("SlotCtyCode={0},LocSlotLocCode={1},sUsrCode={2}", SlotCtyCode, LocSlotLocCode, sUsrCode))
            result = DataRepository.Get_SlotManagementData(SlotCtyCode, LocSlotLocCode, sUsrCode)
        Catch ex As Exception
            Logging.LogError("GetSlotManagementData ERROR", ex.Message & "," & ex.StackTrace)
            Throw New Exception("ERROR: Retrieving Slot Management Data")
        End Try
        Return result
    End Function

    Public Shared Function DeleteUpdateSlotNumber(updatedata As DataSet, usercode As String) As String
        Dim Slotid As Integer = 0
        Dim LocSlotid As Integer = 0
        Dim SlotIsActive As Boolean = True
        Dim LocSlotNumOfCko As Integer = 0
        Dim result As String = "OK"

        Try
            For Each row As DataRow In updatedata.Tables(0).Rows
                Slotid = row("slotid")
                LocSlotid = row("locslotid")
                LocSlotNumOfCko = row("locslotnumofcko")
                SlotIsActive = row("slotisactive")
                Logging.LogDebug("DeleteUpdateSlotNumber", String.Format("Slotid={0},LocSlotid={1},SlotIsActive={2},LocSlotNumOfCko={3},LocSlotNumOfCko={4}", Slotid, LocSlotid, SlotIsActive, LocSlotNumOfCko, LocSlotNumOfCko))
                result = DataRepository.DeleteUpdate_SlotNumber(Slotid, LocSlotid, SlotIsActive, LocSlotNumOfCko, usercode)
            Next

        Catch ex As Exception
            Logging.LogError("DeleteUpdateSlotNumber ERROR", ex.Message & "," & ex.StackTrace)
            Throw New Exception("ERROR: Saving Slot Management Data")
        End Try

        Return "OK"
    End Function
End Class
