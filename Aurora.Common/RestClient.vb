﻿Imports Microsoft.VisualBasic

Imports System.IO
Imports System.Net
Imports System.Text
''Imports System.Web.Script.Serialization

Namespace Aurora.Common


    Public Enum HttpVerb
        [GET]
        POST
        PUT
        DELETE
    End Enum

    Public Class RestClient
        Public Property EndPoint() As String
            Get
                Return m_EndPoint
            End Get
            Set(value As String)
                m_EndPoint = value
            End Set
        End Property
        Private m_EndPoint As String
        Public Property Method() As HttpVerb
            Get
                Return m_Method
            End Get
            Set(value As HttpVerb)
                m_Method = value
            End Set
        End Property
        Private m_Method As HttpVerb
        Public Property ContentType() As String
            Get
                Return m_ContentType
            End Get
            Set(value As String)
                m_ContentType = value
            End Set
        End Property
        Private m_ContentType As String
        Public Property PostData() As String
            Get
                Return m_PostData
            End Get
            Set(value As String)
                m_PostData = value
            End Set
        End Property
        Private m_PostData As String

        Public Sub New()
            EndPoint = ""
            Method = HttpVerb.[GET]
            ContentType = "text/xml"
            PostData = ""
        End Sub
        Public Sub New(endpoint__1 As String)
            EndPoint = endpoint__1
            Method = HttpVerb.[GET]
            ContentType = "text/xml"
            PostData = ""
        End Sub
        Public Sub New(endpoint__1 As String, method__2 As HttpVerb)
            EndPoint = endpoint__1
            Method = method__2
            ContentType = "text/xml"
            PostData = ""
        End Sub

        Public Sub New(endpoint__1 As String, method__2 As HttpVerb, postData__3 As String)
            EndPoint = endpoint__1
            Method = method__2
            ContentType = "text/xml"
            PostData = postData__3
        End Sub


        Public Function MakeRequest() As String
            Return MakeRequest("")
        End Function

        Public Function MakeRequest(parameters As String) As String
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(EndPoint & parameters), HttpWebRequest)

            request.Method = Method.ToString()
            request.ContentLength = 0
            request.ContentType = ContentType

            If Not String.IsNullOrEmpty(PostData) AndAlso Method = HttpVerb.POST Then
                Dim encoding__1 = New UTF8Encoding()
                Dim bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData)
                request.ContentLength = bytes.Length

                Using writeStream = request.GetRequestStream()
                    writeStream.Write(bytes, 0, bytes.Length)
                End Using
            End If

            Using response = DirectCast(request.GetResponse(), HttpWebResponse)
                Dim responseValue = String.Empty

                If response.StatusCode <> HttpStatusCode.OK Then
                    Dim message = [String].Format("Request failed. Received HTTP {0}", response.StatusCode)
                    Throw New ApplicationException(message)
                End If

                ' grab the response
                Using responseStream = response.GetResponseStream()
                    If responseStream IsNot Nothing Then
                        Using reader = New StreamReader(responseStream.ToString())
                            responseValue = reader.ReadToEnd()
                        End Using
                    End If
                End Using

                Return responseValue
            End Using
        End Function

        Sub MakeRequestNew(dataValue As String)



            Using webCli As New WebClient
                Dim responseValue As Byte()
                Try

                    Dim dataVal() As Byte = New Byte(1) {}
                    responseValue = webCli.UploadData(EndPoint + dataValue, "PUT", dataVal)

                    Dim returnmessage As String = responseValue.ToString()
                Catch ex As Exception
                    Dim s As String = ex.Message
                End Try
            End Using

        End Sub

    End Class


End Namespace