Imports System.Data
Imports System.Xml
Imports Aurora.Common.Data

Namespace Aurora.Common

    Partial Public Class Data

        Public Shared Function GetUniversalInfo(ByVal key As String) As XmlDocument
            Return Data.ExecuteSqlXmlSPDoc("sp_get_UniversalInfo", 0, "", key, "", "", "", "")
        End Function

        Public Shared Function GetFunctionPermission(ByVal userCode As String, ByVal functionCode As String) As Boolean
            If AuroraFunctionPermissionOverride.Instance.HasEntry(userCode, functionCode) IsNot Nothing Then
                Return AuroraFunctionPermissionOverride.Instance.HasEntry(userCode, functionCode).Allow
            Else
                Return Convert.ToBoolean(Data.ExecuteScalarSP("GetFunctionPermission", userCode, functionCode))
            End If
        End Function

        Public Shared Function GetComboData(ByVal caseType As String, ByVal param1 As String, ByVal param2 As String, ByVal userCode As String) As DataSet
            Return Data.ExecuteSqlXmlSPDataSet("GEN_getComboData", caseType, param1, param2, userCode)
        End Function

        Public Shared Function GetData(ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal userCode As String) As DataSet
            Return Data.ExecuteSqlXmlSPDataSet("GEN_get_data", param1, param2, param3, userCode)
        End Function

        Public Shared Function GetPopUpData(ByVal caseType As String, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String, ByVal userCode As String) As DataSet
            Return Data.ExecuteSqlXmlSPDataSet("GEN_GetPopUpData", caseType, param1, param2, param3, param4, param5, userCode)
        End Function

        Public Shared Function GetPopUpData(ByVal storedProcedureName As String, ByVal popupType As Aurora.Popups.Data.PopupType, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String, ByVal userCode As String) As XmlDocument
            Return Data.ExecuteSqlXmlSPDoc(storedProcedureName, _
                popupType.ToString(), _
                "" & param1, _
                "" & param2, _
                "" & param3, _
                "" & param4, _
                "" & param5, _
                "" & userCode)
        End Function

        Public Shared Function GetPopUpData(ByVal popupType As Aurora.Popups.Data.PopupType, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String, ByVal userCode As String) As XmlDocument
            Return Data.ExecuteSqlXmlSPDoc(Aurora.Popups.Data.PopupInformation.DefaultStoredProcedureName, _
                popupType.ToString(), _
                "" & param1, _
                "" & param2, _
                "" & param3, _
                "" & param4, _
                "" & param5, _
                "" & userCode)
        End Function

        Public Shared Function GetUserInitFunction(ByVal userCode As String) As DataSet
            Return Data.ExecuteSqlXmlSPDataSet("sp_get_UserInit_Fun", userCode)
        End Function

        Public Shared Function GetCodecodetype(ByVal codcdtnum As String, ByVal code As String) As DataSet
            Return Data.ExecuteSqlXmlSPDataSet("sp_get_codecodetype", codcdtnum, code, "")
        End Function

        Public Shared Function GetErrorMessage(ByVal errorCode As String) As String
            Dim ds As DataSet = Data.ExecuteSqlXmlSPDataSet("GEN_getMsgToClient", errorCode)
            If ds.Tables.Count = 1 Then
                Return ds.Tables(0).Rows(0)(0)
            Else
                Return ""
            End If
        End Function

        Public Shared Function GetMsgToClient(ByVal errorCode As String) As Collection
            Dim col As New Collection
            Dim ds As DataSet = Data.ExecuteSqlXmlSPDataSet("GEN_getMsgToClientNew", errorCode)
            For Each dr As DataRow In ds.Tables(0).Rows
                col.Add(dr(1), dr(0))
            Next
            Return col
        End Function

        Public Shared Function GetNewId() As String
            Dim ds As DataSet = Data.ExecuteSqlXmlSPDataSet("sp_get_newId ")
            If ds.Tables.Count = 1 Then
                Return ds.Tables(0).Rows(0)(0)
            Else
                Throw New Exception("Unexpected error calling sp_get_newId")
            End If
        End Function

        Public Shared Function CheckUser(ByVal UserCode As String, ByVal Password As String, ByVal RoleCode As String) As XmlDocument
            'dialog_checkUser
            '@sUserCode varchar(64),  
            '@sPsword varchar(64),  
            '@sRoleCode  varchar(6000)  
            Return Data.ExecuteSqlXmlSPDoc("dialog_checkUser", UserCode, Password, RoleCode)
        End Function

        Public Shared Function GetCountriesOfOperation() As XmlDocument
            Return Data.ExecuteSqlXmlSPDoc("sp_getCountriesOfOperation")
        End Function

        Public Shared Function GetUserSettings(ByVal usrCode As String) As DataTable
            Return Data.ExecuteTableSP("User_GetSettings", New DataTable("UserSettings"), usrCode)
        End Function

        Public Shared Function GetCurrentCurrencyRates() As DataTable
            Return Data.ExecuteTableSP("Common_GetCurrentCurrencyRates", New DataTable("CurrentCurrencyRates"))
        End Function

        Public Shared Sub UpdateUserPassword(ByVal usrCode As String, ByVal usrPsswd As String, ByVal modUsrId As String)
            'CREATE PROCEDURE [dbo].[userInfo_updatePassword]
            '	@sUsrCode 	varchar(64),
            '	@sPswd		varchar(64),
            '	@sParam		varchar(64), -- not used
            '	@sAddModuserId	varchar(64),
            '	@sProgram	varchar(64), -- not used
            '	@sAction 	varchar(10) -- not used

            Data.ExecuteScalarSP("userInfo_updatePassword", usrCode, usrPsswd, DBNull.Value, modUsrId, DBNull.Value, DBNull.Value)
        End Sub

        Public Shared Function ToggleUserCompany(ByVal UserCode As String) As String
            Dim oXml As XmlDocument
            oXml = Data.ExecuteSqlXmlSPDoc("GEN_ToggleUserCompany", UserCode)
            Return oXml.InnerText
        End Function

        Public Shared Function UserAllowedToToggleLocation(ByVal UserCode As String) As Boolean
            Dim oXml As XmlDocument
            oXml = Data.ExecuteSqlXmlSPDoc("GEN_CheckToggleLocationPermission", UserCode)
            If oXml.InnerText.Trim().ToUpper().Equals("YES") Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class

End Namespace