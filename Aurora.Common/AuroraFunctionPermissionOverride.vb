Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration
Imports System.Collections.Generic

Namespace Aurora.Common

    Public Class AuroraFunctionPermissionOverrideEntry
        Public UserCode As String = ""
        Public FunctionCode As String = ""
        Public Allow As Boolean
    End Class

    Public Class AuroraFunctionPermissionOverride
        Public Entries() As AuroraFunctionPermissionOverrideEntry = {}

        Public Function HasEntry(ByVal userCode As String, ByVal functionCode As String) As AuroraFunctionPermissionOverrideEntry
            For Each entry As AuroraFunctionPermissionOverrideEntry In Entries
                If ((("" & userCode).Trim().ToUpper() = ("" & entry.UserCode).Trim().ToUpper()) _
                 AndAlso (("" & functionCode).Trim().ToUpper() = ("" & entry.FunctionCode).Trim().ToUpper())) Then
                    Return entry
                End If
            Next
            Return Nothing
        End Function

        Private Shared _instance As AuroraFunctionPermissionOverride
        Public Shared ReadOnly Property Instance() As AuroraFunctionPermissionOverride
            Get
                If _instance Is Nothing Then _instance = CType(ConfigurationManager.GetSection("AuroraFunctionPermissionOverride"), AuroraFunctionPermissionOverride)
                If _instance Is Nothing Then _instance = New AuroraFunctionPermissionOverride()
                Return _instance
            End Get
        End Property

    End Class


    Public Class AuroraFunctionPermissionOverrideSectionHandler
        Implements IConfigurationSectionHandler

        Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
            ByVal section As System.Xml.XmlNode) As Object _
            Implements System.Configuration.IConfigurationSectionHandler.Create

            Dim xs As XmlSerializer = New XmlSerializer(GetType(AuroraFunctionPermissionOverride))
            Return xs.Deserialize(New XmlNodeReader(section))
        End Function

    End Class

End Namespace