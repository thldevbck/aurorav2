Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Text

Namespace Aurora.Common

    Public Class LoggingCategory
        Public Const Errors As String = "Errors"
        Public Const Warning As String = "Warning"
        Public Const Information As String = "Information"
        Public Const Debug As String = "Debug"
    End Class

    ' Struct containing integer constants used to identify the Priority level for log messages
    Public Enum LoggingPriority
        Low = 2
        Medium = 3
        High = 4
    End Enum

    Public Class Logging

        Public Shared Sub Log(ByVal functionCode As String, ByVal category As String, ByVal priority As LoggingPriority, ByVal severity As TraceEventType, ByVal message As String)
            Try
                ' Creates and fills the log entry with user information
                Dim logEntry As New LogEntry()
                logEntry.EventId = 0
                logEntry.Priority = priority
                logEntry.Title = functionCode
                logEntry.Message = message
                logEntry.Categories.Clear()
                logEntry.Categories.Add(category)
                logEntry.Severity = severity

                ' Writes the log entry.
                Logger.Write(logEntry)

            Catch ex As Exception

            End Try
        End Sub

        Public Shared Sub LogError(ByVal functionCode As String, ByVal description As String)
            Log(functionCode, LoggingCategory.Errors, LoggingPriority.High, TraceEventType.Error, description)
        End Sub

        Public Shared Sub LogError(ByVal functionCode As String, ByVal description As String, ByVal ex As Exception)
            Log(functionCode, LoggingCategory.Errors, LoggingPriority.High, TraceEventType.Error, description + vbCrLf + vbCrLf + ReportException(ex))
        End Sub

        Public Shared Sub LogException(ByVal functionCode As String, ByVal ex As Exception)
            Log(functionCode, LoggingCategory.Errors, LoggingPriority.High, TraceEventType.Critical, ReportException(ex))
        End Sub

        Public Shared Sub LogWarning(ByVal functionCode As String, ByVal description As String)
            Log(functionCode, LoggingCategory.Warning, LoggingPriority.Medium, TraceEventType.Warning, description)
        End Sub

        Public Shared Sub LogWarning(ByVal functionCode As String, ByVal description As String, ByVal ex As Exception)
            Log(functionCode, LoggingCategory.Warning, LoggingPriority.Medium, TraceEventType.Warning, description + vbCrLf + vbCrLf + ReportException(ex))
        End Sub

        Public Shared Sub LogInformation(ByVal functionCode As String, ByVal description As String)
            Log(functionCode, LoggingCategory.Information, LoggingPriority.Medium, TraceEventType.Information, description)
        End Sub

        Public Shared Sub LogDebug(ByVal functionCode As String, ByVal description As String)
            Log(functionCode, LoggingCategory.Debug, LoggingPriority.Low, TraceEventType.Verbose, description)
        End Sub

        'Report an exception and all its inner exceptions information
        Private Shared Function ReportException(ByVal ex As Exception) As String
            Dim exceptionInfo As StringBuilder = New StringBuilder()
            exceptionInfo.Append("--------- Outer Exception Data ---------" & Environment.NewLine)

            ReportExceptionInfo(ex, exceptionInfo)
            While Not ex.InnerException Is Nothing
                exceptionInfo.Append("--------- Inner Exception Data ---------" & Environment.NewLine)
                ReportExceptionInfo(ex.InnerException, exceptionInfo)
                ex = ex.InnerException
            End While
            Return exceptionInfo.ToString()
        End Function

        'Report an exception information
        Private Shared Sub ReportExceptionInfo(ByVal ex As Exception, ByVal exceptionInfo As StringBuilder)

            exceptionInfo.Append("Exception Message: " & ex.Message & Environment.NewLine)
            exceptionInfo.Append("Exception Type: " & ex.GetType().FullName & Environment.NewLine)
            exceptionInfo.Append("Source: " & ex.Source & Environment.NewLine)
            exceptionInfo.Append("StrackTrace: " & ex.StackTrace & Environment.NewLine)
            exceptionInfo.Append("TargetSite: " & ex.TargetSite.Name & Environment.NewLine)

        End Sub

    End Class

End Namespace