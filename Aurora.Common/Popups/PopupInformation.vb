Imports System.Xml
Imports System.Web.UI.WebControls

Namespace Aurora.Popups.Data

    Partial Public Class PopupInformation

        Public Const DefaultStoredProcedureName As String = "GEN_GetPopUpData"

        Public Const DefaultDataXPath As String = "/data/POPUP"
        Public Const DefaultIdXPath As String = "ID"
        Public Const DefaultCodeXPath As String = "CODE"
        Public Const DefaultDescXPath As String = "DESCRIPTION"
        Public Const DefaultTextParamNumber As Integer = 1

        Private _popupType As PopupType
        Public ReadOnly Property PopupType() As PopupType
            Get
                Return _popupType
            End Get
        End Property

        Private _name As String
        Public ReadOnly Property Name() As String
            Get
                Return _name
            End Get
        End Property

        Private _pluralName As String
        Public ReadOnly Property PluralName() As String
            Get
                If _pluralName IsNot Nothing Then Return _pluralName Else Return _name & "s"
            End Get
        End Property

        Private _storedProcedureName As String = DefaultStoredProcedureName
        Public ReadOnly Property StoredProcedureName() As String
            Get
                Return _storedProcedureName
            End Get
        End Property

        Private _dataXPath As String = DefaultDataXPath
        Public ReadOnly Property DataXPath() As String
            Get
                Return _dataXPath
            End Get
        End Property

        Private _textParamNumber As Integer = DefaultTextParamNumber
        Public ReadOnly Property TextParamNumber() As Integer
            Get
                Return _textParamNumber
            End Get
        End Property

        Private _idEnabled As Boolean = False
        Public ReadOnly Property IdEnabled() As Boolean
            Get
                Return _idEnabled
            End Get
        End Property

        Private _idColumn As PopupInformationColumn = New PopupInformationColumn(caption:="Id", XPath:=DefaultIdXPath, visible:=False)
        Public ReadOnly Property IdColumn() As PopupInformationColumn
            Get
                Return _idColumn
            End Get
        End Property

        Private _codeColumn As PopupInformationColumn = New PopupInformationColumn(caption:="Code", XPath:=DefaultCodeXPath, width:="100px")
        Public ReadOnly Property CodeColumn() As PopupInformationColumn
            Get
                Return _codeColumn
            End Get
        End Property

        Private _descEnabled As Boolean = True
        Public ReadOnly Property DescEnabled() As Boolean
            Get
                Return _descEnabled
            End Get
        End Property

        Private _descColumn As PopupInformationColumn = New PopupInformationColumn(caption:="Description", XPath:=DefaultDescXPath)
        Public ReadOnly Property DescColumn() As PopupInformationColumn
            Get
                Return _descColumn
            End Get
        End Property

        Private _customColumns() As PopupInformationColumn = {}
        Public ReadOnly Property CodeColumns() As PopupInformationColumn()
            Get
                Return _customColumns
            End Get
        End Property

        Public ReadOnly Property AllColumns() As PopupInformationColumn()
            Get
                Dim result As New List(Of PopupInformationColumn)
                result.Add(_idColumn)
                result.Add(_codeColumn)
                result.Add(_descColumn)
                If _customColumns IsNot Nothing Then result.AddRange(_customColumns)
                Return result.ToArray()
            End Get
        End Property

        Private Sub New( _
          ByVal popupType As PopupType, _
          Optional ByVal name As String = Nothing, _
          Optional ByVal storedProcedureName As String = Nothing, _
          Optional ByVal pluralName As String = Nothing, _
          Optional ByVal dataXPath As String = Nothing, _
          Optional ByVal textParamNumber As Integer = DefaultTextParamNumber, _
          Optional ByVal idEnabled As Boolean = False, _
          Optional ByVal idColumn As PopupInformationColumn = Nothing, _
          Optional ByVal idXPath As String = Nothing, _
          Optional ByVal codeColumn As PopupInformationColumn = Nothing, _
          Optional ByVal codeXPath As String = Nothing, _
          Optional ByVal codeWidth As String = Nothing, _
          Optional ByVal descEnabled As Boolean = True, _
          Optional ByVal descColumn As PopupInformationColumn = Nothing, _
          Optional ByVal descXPath As String = Nothing, _
          Optional ByVal customColumns() As PopupInformationColumn = Nothing)
            _popupType = popupType
            If name IsNot Nothing Then _name = name Else _name = popupType.ToString()
            If storedProcedureName IsNot Nothing Then _storedProcedureName = storedProcedureName
            If pluralName IsNot Nothing Then _pluralName = pluralName
            If dataXPath IsNot Nothing Then _dataXPath = dataXPath
            _textParamNumber = textParamNumber

            If idColumn IsNot Nothing Then _idColumn = idColumn
            If idXPath IsNot Nothing AndAlso _idColumn IsNot Nothing Then _idColumn.XPath = idXPath
            _idEnabled = idEnabled
            If Not _idEnabled Then
                _idColumn.XPath = Nothing
                _idColumn.Visible = False
            End If

            If codeColumn IsNot Nothing Then _codeColumn = codeColumn
            If codeXPath IsNot Nothing AndAlso _codeColumn IsNot Nothing Then _codeColumn.XPath = codeXPath
            If codeWidth IsNot Nothing AndAlso _codeColumn IsNot Nothing Then _codeColumn.Width = Unit.Parse(codeWidth)

            If descColumn IsNot Nothing Then _descColumn = descColumn
            If descXPath IsNot Nothing AndAlso _descColumn IsNot Nothing Then _descColumn.XPath = descXPath
            _descEnabled = descEnabled
            If Not _descEnabled Then
                _descColumn.XPath = Nothing
                _descColumn.Visible = False
            End If

            If customColumns IsNot Nothing Then _customColumns = customColumns
        End Sub


        Public Function GetPopUpData(ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String, ByVal userCode As String, ByVal text As String) As XmlDocument

            If _textParamNumber = 1 AndAlso String.IsNullOrEmpty(param1) Then
                param1 = text
            ElseIf _textParamNumber = 2 AndAlso String.IsNullOrEmpty(param2) Then
                param2 = text
            ElseIf _textParamNumber = 3 AndAlso String.IsNullOrEmpty(param3) Then
                param3 = text
            ElseIf _textParamNumber = 4 AndAlso String.IsNullOrEmpty(param4) Then
                param4 = text
            ElseIf _textParamNumber = 5 AndAlso String.IsNullOrEmpty(param5) Then
                param5 = text
            End If

            Return Aurora.Common.Data.GetPopUpData(_storedProcedureName, _popupType, param1, param2, param3, param4, param5, userCode)
        End Function


        Public Function ValidatePopUpDataText(ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String, ByVal userCode As String, ByVal text As String) As String

            Dim popupXml As XmlDocument = GetPopUpData(param1, param2, param3, param4, param5, userCode, text)
            If Aurora.Common.Data.HasSqlXmlSPDocError(popupXml) Then Return Nothing

            Dim nodes As XmlNodeList = popupXml.SelectNodes(DataXPath)
            If nodes.Count <> 1 Then Return Nothing

            Dim node As XmlNode = nodes(0)
            If IdEnabled Then
                Return node.SelectSingleNode(IdColumn.XPath).InnerText
            Else
                Return node.SelectSingleNode(CodeColumn.XPath).InnerText
            End If

        End Function

    End Class

End Namespace