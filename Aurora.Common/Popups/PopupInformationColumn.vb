Imports System.Web.UI.WebControls

Namespace Aurora.Popups.Data

    ''' <summary>
    ''' PopupInformation holds information on the popup columns
    ''' </summary>
    Public Class PopupInformationColumn

        Private _caption As String
        Public Property Caption() As String
            Get
                Return _caption
            End Get
            Set(ByVal value As String)
                _caption = value
            End Set
        End Property

        Private _xPath As String
        Public Property XPath() As String
            Get
                Return _xPath
            End Get
            Set(ByVal value As String)
                _xPath = value
            End Set
        End Property

        Public _width As Unit = Unit.Empty
        Public Property Width() As Unit
            Get
                Return _width
            End Get
            Set(ByVal value As Unit)
                _width = value
            End Set
        End Property

        Public _visible As Boolean
        Public Property Visible() As Boolean
            Get
                Return _visible
            End Get
            Set(ByVal value As Boolean)
                _visible = False
            End Set
        End Property

        Public _horizontalAlign As HorizontalAlign
        Public Property HorizontalAlign() As HorizontalAlign
            Get
                Return _horizontalAlign
            End Get
            Set(ByVal value As HorizontalAlign)
                _horizontalAlign = False
            End Set
        End Property

        Public Sub New( _
          ByVal caption As String, _
          ByVal xpath As String, _
          Optional ByVal width As String = Nothing, _
          Optional ByVal visible As Boolean = True, _
          Optional ByVal horizontalAlign As HorizontalAlign = HorizontalAlign.NotSet)
            _caption = caption
            _xPath = xpath
            If width IsNot Nothing Then _width = New Unit(width)
            _visible = visible
            _horizontalAlign = horizontalAlign
        End Sub

    End Class

End Namespace