Imports System.Web.UI.WebControls

Namespace Aurora.Popups.Data

    Partial Public Class PopupInformation

        Private Shared _information As PopupInformation() = { _
            New PopupInformation(PopupType.AGENT, Name:="Agent", IdEnabled:=True, _
             customColumns:=New PopupInformationColumn() {New PopupInformationColumn(Caption:="Misc", xpath:="''", width:="50px", HorizontalAlign:=HorizontalAlign.Right)}) _
            , New PopupInformation(PopupType.PACKAGE, Name:="Package", IdEnabled:=True) _
            , New PopupInformation(PopupType.LOCATION, Name:="Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.LOCATIONFORCOUNTRY, Name:="Location", TextParamNumber:=2, CodeWidth:="50px") _
            , New PopupInformation(PopupType.CHKOLOC, Name:="Check-Out Location", DataXPath:="/data/Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.CHKILOC, Name:="Check-In Location", DataXPath:="/data/Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.POPUPCOUNTRY, Name:="Country", PluralName:="Countries", DataXPath:="/data/Country", CodeWidth:="50px") _
            , New PopupInformation(PopupType.PRODUCT, Name:="Product", IdEnabled:=True) _
            , New PopupInformation(PopupType.PRODUCTVEHICLE, Name:="Vehicle", IdEnabled:=True, DataXPath:="/data/PRODUCT", idXPath:="Id", CodeXPath:="ShortName", DescXPath:="Name") _
            , New PopupInformation(PopupType.Product4BookingSearch, Name:="Product") _
            , New PopupInformation(PopupType.FleetModel4BookingSearch, Name:="Fleet Model") _
            , New PopupInformation(PopupType.BOOKINGAGN, Name:="Agent", DataXPath:="/data/Agent") _
            , New PopupInformation(PopupType.GETDATAFORUSER, Name:="User", DataXPath:="/data/UserInfo", CodeXPath:="DESCRIPTION", DescXPath:="CODE") _
            , New PopupInformation(PopupType.ADMIN_GETUSERINFO, Name:="User", IdEnabled:=True, StoredProcedureName:="Admin_GetPopUpData") _
            , New PopupInformation(PopupType.AGENTPACKAGE, TextParamNumber:=2, IdEnabled:=True) _
            , New PopupInformation(PopupType.POPUPSALEABLEPRODUCT, Name:="Saleable Product") _
 _
            , New PopupInformation(PopupType.PRODUCT_PACKAGE, Name:="Package", IdEnabled:=True, StoredProcedureName:="Product_GetPopUpData", customColumns:=New PopupInformationColumn() { _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="BRAND", HorizontalAlign:=HorizontalAlign.Right, width:="100px"), _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="STATUS", HorizontalAlign:=HorizontalAlign.Right, width:="50px")}) _
 _
            , New PopupInformation(PopupType.PACKAGE_SALEABLEPRODUCT, Name:="Saleable Product", IdEnabled:=True, StoredProcedureName:="Package_GetPopUpData", customColumns:=New PopupInformationColumn() { _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="BRAND", HorizontalAlign:=HorizontalAlign.Right, width:="100px"), _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="STATUS", HorizontalAlign:=HorizontalAlign.Right, width:="50px")}) _
            , New PopupInformation(PopupType.PACKAGE_AGENTGROUP, Name:="Agent Group", IdEnabled:=True, StoredProcedureName:="Package_GetPopUpData") _
            , New PopupInformation(PopupType.PRD4CTY, Name:="Product", IdEnabled:=True, DataXPath:="/data/Product", idXPath:="ID", CodeXPath:="CODE", DescXPath:="DESCRIPTION") _
            , New PopupInformation(PopupType.AGENTPACKAGE4BOOKINGREQ, Name:="Agent Package", IdEnabled:=True) _
            , New PopupInformation(PopupType.MARKETCODE, Name:="Market Codes", IdEnabled:=True, StoredProcedureName:="Agent_GetPopUpData", DataXPath:="/data/Code", TextParamNumber:=1) _
            , New PopupInformation(PopupType.PRODUCT4SUBSTITUTIONRULE, Name:="Package", StoredProcedureName:="RES_getProductPopup") _
             , New PopupInformation(PopupType.OPLOGGETFLEETMODEL, Name:="Fleet Mode", DataXPath:="/data/POPUP", IdEnabled:=True, idXPath:="SrpFlmId", CodeXPath:="CODE", DescXPath:="DESCRIPTION") _
             , New PopupInformation(PopupType.PRODUCT4EXCHANGE, Name:="Product", DataXPath:="/data/Product") _
 _
            , New PopupInformation(PopupType.AGENTFORSUBGROUP, Name:="Agent Subgroup", IdEnabled:=True) _
            , New PopupInformation(PopupType.BookedProduct, Name:="Booked Products", DataXPath:="/data/Product", CodeWidth:="100px") _
            , New PopupInformation(PopupType.BookedProductClass, Name:="Booked Products Class", DataXPath:="/data/Class", CodeWidth:="100px") _
            , New PopupInformation(PopupType.BookedProductType, Name:="Booked Products Type", DataXPath:="/data/Type", CodeWidth:="100px") _
 _
            , New PopupInformation(PopupType.SUBGROUP, Name:="Agent Subgroup", IdEnabled:=True, _
                CodeColumn:=New PopupInformationColumn(Caption:="Group", xpath:="GrpCode", HorizontalAlign:=HorizontalAlign.Left, width:="100px"), _
                DescColumn:=New PopupInformationColumn(Caption:="Sub Group", xpath:="SubGrpDesc", HorizontalAlign:=HorizontalAlign.Left, width:="100px"), _
                customColumns:=New PopupInformationColumn() { _
                    New PopupInformationColumn(Caption:="&nbsp;", xpath:="concat (GrpDesc, '. ', SubGrpDesc)")}) _
                     , New PopupInformation(PopupType.SELBOOPRD, Name:="Product", DataXPath:="data/PRODUCT", idXPath:="Id", CodeXPath:="ShortName", DescXPath:="Name") _
            , New PopupInformation(PopupType.PRODUCT4BPD, Name:="Product") _
            , New PopupInformation(PopupType.LocForBPD, Name:="Location") _
             , New PopupInformation(PopupType.GETPRODUCT, Name:="Product", IdEnabled:=True, DataXPath:="/data/PRODUCT", idXPath:="Id", CodeXPath:="ShortName", DescXPath:="Name") _
             , New PopupInformation(PopupType.GetVehicleData, Name:="Product", IdEnabled:=True) _
             , New PopupInformation(Data.PopupType.AGNENTCOUNTRY, Name:="Agent Country", IdEnabled:=False) _
             , New PopupInformation(Data.PopupType.AGNENTSUBGROUP, Name:="Agent Sub-Group", IdEnabled:=True) _
            , New PopupInformation(PopupType.LOCATIONFORVEHICLEREQ, Name:="Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.GETLOCATIONEXACT, Name:="Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.GETVALIDLOCATION, Name:="Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.PACKAGEBULK, Name:="Package", CodeWidth:="50px", IdEnabled:=True) _
            , New PopupInformation(Data.PopupType.CLASSTYPEBULK, Name:="Type", CodeWidth:="50px") _
            , New PopupInformation(PopupType.FSXLOCATION, Name:="Location", CodeWidth:="50px") _
            , New PopupInformation(PopupType.GETCOMPLAINTHANDLERS, Name:="User", DataXPath:="/data/UserInfo", CodeXPath:="DESCRIPTION", DescXPath:="CODE") _
          , New PopupInformation(PopupType.NonVehicleProduct, Name:="Non-Vehicle Product", DataXPath:="/data/Product", IdEnabled:=True, idXPath:="ID", CodeXPath:="CODE", DescXPath:="DESCRIPTION") _
              , New PopupInformation(PopupType.DISCOUNTONPACKAGEPROFILE, Name:="Package", IdEnabled:=True, StoredProcedureName:="Product_GetPopUpData", customColumns:=New PopupInformationColumn() { _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="BRAND", HorizontalAlign:=HorizontalAlign.Right, width:="100px"), _
                New PopupInformationColumn(Caption:="&nbsp;", xpath:="STATUS", HorizontalAlign:=HorizontalAlign.Right, width:="50px")}) _
        }


        Public Shared Function GetInformationByType(ByVal popupType As PopupType) As PopupInformation
            If Not [Enum].IsDefined(GetType(PopupType), popupType) Then Throw New ArgumentException()

            For Each info As PopupInformation In _information
                If info.PopupType = popupType Then Return info
            Next

            Return New PopupInformation(popupType, popupType.ToString()) ' return a default
        End Function

    End Class

End Namespace