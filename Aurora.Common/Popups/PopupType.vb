Namespace Aurora.Popups.Data

    ''' <summary>
    ''' Enumaration of all the "Popup Types" (the value of the @case parameter to the GEN_GetPopUpData SP)
    ''' </summary>
    Public Enum PopupType
        Undefined = -1
        AGENT
        AGENTFORSUBGROUP
        AGENTGROUP
        AGENTPACKAGE
        AGENTPACKAGE4BOOKINGREQ
        AGENTPACKAGE4BooMgt
        AGNENTCOUNTRY
        AGNENTSUBGROUP
        BONDREPORT
        BONDREPORT1
        BookedProduct
        BookedProductClass
        BookedProductType
        BOOKINGAGN
        ChangeUserCompany
        checkLocation
        CHKBOORENT
        CHKILOC
        CHKOLOC
        [CLASS]
        CLASSTYPE
        CODCODE
        CODE
        CONTACTPOPUP
        COREPORT
        CTYOFOPR
        CUSTOMERCOUNTRY
        FleetModel4BookingSearch
        FLEXAGENTEMAIL
        getBpdStatus
        GETCOUNTRY_NEW
        GETDATAFORUSER
        GETUVI
        GetVehicleData
        GST
        IncentiveScheme
        LOCATION
        LOCATIONFORCOUNTRY
        LOCATIONFORCOUNTRY_NEW
        LOCATIONFORSTOCK
        LOCATIONFORVEHICLEREQ
        LOCATIONFORVEHICLEREQ1
        LOCATIONFORVEHICLEREQ2
        LocForBPD
        NEWAGENTGROUP
        OPLOGBRANCH
        OPLOGFLEETREPBRANCH
        OPLOGFLEETREPPROVIDER
        OPLOGFLEETREPPROVIDERID
        OPLOGGETFLEETMODEL
        PACKAGE
        PACKAGE_AGENT_PACKAGETYPE
        PACKAGEPRODUCT
        POPUPCOUNTRY
        POPUPSALEABLEPRODUCT
        POPUPSALEABLEPRODUCTID
        PRD4CTY
        PRDTYPE4EXCH
        PRDTYPE4LATEFEE
        PRODUCT
        Product4BookingSearch
        PRODUCT4BPD
        PRODUCT4EXCHANGE
        PRODUCTCOUNTRY
        PRODUCTVEHICLE
        RENTADD
        SALEABLEPRODUCT
        SELBOOPRD
        SUBGROUP
        tmpreport
        tmpRMreport
        TYPE
        UNIVERSALINFO
        UPDTALLVEHSP
        VALIDATEBOOKING
        VALIDATEVEHICLE
        VEHASSGET_GRID
        VEHASSUPDT

        PRODUCT_PACKAGE

        PACKAGE_SALEABLEPRODUCT
        PACKAGE_AGENTGROUP
        MARKETCODE
        PRODUCT4SUBSTITUTIONRULE

        ADMIN_GETUSERINFO
        GETPRODUCT           ''REV:0908 MIA
        GETLOCATIONEXACT     ''REV:1410 MIA
        GETVALIDLOCATION     ''REV:1510 MIA   
        PACKAGEBULK ' Added by Shoel - 25.11.8 - to prevent invalid packages being picked up on bulk assign page
        CLASSTYPEBULK ' Added by Shoel - 25.11.8 - to fix the class type issue in bulk assign page
        FSXLOCATION ' Added by Shoel - 20.3.9 - PHOENIX Change - needed for FSX
        GETCOMPLAINTHANDLERS ' Added by Shoel - 9.11.9 - Complaints Tab

        VehicleProduct 'Added by Raj - 15 March, 2011 - For Inclusive Products Maintenance Screen
        NonVehicleProduct 'Added by Raj - 15 March, 2011 - For Inclusive Products Maintenance Screen

        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        AGENTCOUNTRYMULTIPLE

        ''rev:mia March. 14 2013 - addition of a field to allow package discount profile
        DISCOUNTONPACKAGEPROFILE
    End Enum


End Namespace