Imports System.Data
Imports System.Data.Common
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO

Imports Microsoft.VisualBasic

Imports Microsoft.Practices.EnterpriseLibrary.Data

Namespace Aurora.Common

    Partial Public Class Data

#Region "CallBlock"

        Public Class SPCallBlock
            Implements IDisposable

            Private storedProcedureName As String
            Private params() As Object
            Private startTime As Date = Date.Now

            Public ReadOnly Property SPParamsText()
                Get
                    Dim result As String = "EXEC " + storedProcedureName + " "
                    Dim first As Boolean = True
                    For Each p As Object In params
                        If first Then
                            first = False
                        Else
                            result += ", "
                        End If
                        If p Is Nothing OrElse p Is DBNull.Value Then
                            result += "NULL"
                        ElseIf TypeOf p Is Date Then
                            Dim v As Date = CType(p, Date)
                            result += "'" + v.ToString("dd/MM/yyyy") + "'"
                        ElseIf TypeOf p Is Boolean Then
                            Dim v As Boolean = CType(p, Boolean)
                            result += IIf(v, "1", "0")
                        Else
                            result += "'" + p.ToString() + "'"
                        End If
                    Next
                    Return result
                End Get
            End Property

            Sub New(ByVal storedProcedureName As String, ByVal ParamArray params() As Object)
                Me.storedProcedureName = storedProcedureName
                Me.params = params
            End Sub

            Public Sub Dispose() Implements System.IDisposable.Dispose
                Dim ts As TimeSpan = Date.Now - startTime
                Logging.LogDebug("SP", SPParamsText + vbCrLf + "--Time: " + CStr(ts.TotalSeconds))
            End Sub

        End Class

        Private Class SqlCallBlock
            Implements IDisposable

            Private startTime As Date = Date.Now
            Private sql As String

            Sub New(ByVal sql As String)
                Me.sql = sql
            End Sub

            Public Sub Dispose() Implements System.IDisposable.Dispose
                Dim ts As TimeSpan = Date.Now - startTime
                Logging.LogDebug("SQL", sql + vbCrLf + "--Time: " + CStr(ts.TotalSeconds))
            End Sub

        End Class

#End Region

#Region "Low Level Database Calls"

        Public Const DefaultCommandTimeout As Integer = 300

#Region " Rev:mia Dec 9"
        Public Enum TrapMode
            InsertMode = 1
            EditMode = 2
        End Enum

        Private Shared Function ModifySQL(ByVal sql As String, ByVal objTrapMode As Integer) As String
            Dim tempSQL As String = ""
            Dim tempDummyColumn As String = "@Param5 -- VdrRate=9999999999"
            Dim tempDummyColumnValue As String = "@Param5 -- VdrRate=NULL"
            Select Case objTrapMode
                Case 1
                    If sql.Contains("INSERT INTO [VehicleDependentRate]") Then
                        If sql.Contains("@Param5 -- VdrRate=9999999999") Then
                            tempSQL = sql.Replace(tempDummyColumn, tempDummyColumnValue)
                        End If

                        tempDummyColumn = "@Param7 -- VdrMinChg=9999999999"
                        tempDummyColumnValue = "@Param7 -- VdrMinChg=NULL"
                        If sql.Contains(tempDummyColumn) Then
                            tempSQL = sql.Replace(tempDummyColumn, tempDummyColumnValue)
                        End If

                        tempDummyColumn = "@Param8 -- VdrMaxChg=9999999999"
                        tempDummyColumnValue = "@Param8 -- VdrMaxChg=NULL"
                        If sql.Contains(tempDummyColumn) Then
                            tempSQL = sql.Replace(tempDummyColumn, tempDummyColumnValue)
                        End If
                    End If
                Case 2
                    ''UPDATE
                    If sql.Contains("UPDATE [VehicleDependentRate]") Then
                        tempDummyColumn = "[VdrRate] = @NewParam5 -- 9999999999"
                        tempDummyColumnValue = "[VdrRate] = @NewParam5 -- NULL"
                        If sql.Contains(tempDummyColumn) Then
                            tempSQL = sql.Replace(tempDummyColumn, tempDummyColumnValue)
                        End If
                    End If
            End Select
            Return IIf(String.IsNullOrEmpty(tempSQL), sql, tempSQL)
        End Function

        Public Shared Function ExecuteInsertDataInsertVehicleDependentRate(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing, Optional ByVal PrgmName As String = Nothing) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")

            If dataRow.Table.Columns.Contains("IntegrityNo") Then dataRow("IntegrityNo") = 1
            If Not String.IsNullOrEmpty(UsrId) AndAlso dataRow.Table.Columns.Contains("AddUsrId") Then dataRow("AddUsrId") = UsrId
            If dataRow.Table.Columns.Contains("AddDateTime") Then dataRow("AddDateTime") = Date.Now
            If Not String.IsNullOrEmpty(PrgmName) AndAlso dataRow.Table.Columns.Contains("AddPrgmName") Then dataRow("AddPrgmName") = PrgmName

            Dim sql As String = "INSERT INTO [" & dataRow.Table.TableName & "] WITH  (ROWLOCK) " & vbCrLf

            Dim first As Boolean
            Dim index As Integer

            sql = sql.ToUpper()

            sql += "(" & vbCrLf
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If (dataColumn.ColumnName.ToUpper().Trim().Equals("NTEDESC") Or dataColumn.ColumnName.ToUpper().Trim().Equals("NTEID")) _
                And dataColumn.Table.TableName.ToUpper().Trim().Equals("VEHICLEDEPENDENTRATE") Then Continue For

                If Not dataColumn.AutoIncrement Then
                    If first Then first = False Else sql += "," & vbCrLf

                    If sql.Contains("FleetStatus".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("ExceptionList") Then
                        ' Get rid of extra comma
                        sql = Left(sql, sql.LastIndexOf(","c))
                    Else
                        sql += vbTab & "[" & dataColumn.ColumnName & "]"
                    End If

                End If
            Next
            sql += ")" & vbCrLf

            sql += "VALUES" & vbCrLf
            sql += "(" & vbCrLf & vbTab
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If (dataColumn.ColumnName.ToUpper().Trim().Equals("NTEDESC") Or dataColumn.ColumnName.ToUpper().Trim().Equals("NTEID")) _
                And dataColumn.Table.TableName.ToUpper().Trim().Equals("VEHICLEDEPENDENTRATE") Then Continue For

                If Not dataColumn.AutoIncrement Then
                    If first Then first = False Else sql += vbTab & ","
                    index += 1

                    If sql.Contains("FleetStatus".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("ExceptionList") Then
                        ' Get rid of extra comma
                        sql = Left(sql, sql.LastIndexOf(","c))
                    Else
                        sql += "@Param" & CStr(dataColumn.Ordinal)
                    End If

                    If Not dataRow.IsNull(dataColumn) Then
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataColumn.ColumnName & "=" & dataRow(dataColumn).ToString()) & vbCrLf
                    Else
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataColumn.ColumnName & "=NULL") & vbCrLf
                    End If
                End If
            Next
            sql += vbCrLf & ");" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"
            Dim temp As String = ModifySQL(sql, 1)
            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(temp)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns
                    If (dataColumn.ColumnName.ToUpper().Trim().Equals("NTEDESC") Or dataColumn.ColumnName.ToUpper().Trim().Equals("NTEID")) _
                    And dataColumn.Table.TableName.ToUpper().Trim().Equals("VEHICLEDEPENDENTRATE") Then Continue For

                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = "@Param" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn) Then
                        p.Value = dataRow(dataColumn)
                    Else
                        p.Value = DBNull.Value
                    End If

                    'If p.ParameterName = "@Param5" Or p.ParameterName = "@Param7" Or p.ParameterName = "@Param8" Then
                    If Not IsDBNull(p.Value) Then
                        If CStr(p.Value) = "9999999999" Then
                            p.Value = DBNull.Value
                        End If
                    End If
                    'End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function

        Public Shared Function ExecuteDataRowUpdateVehicleDependentRate(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")
            If dataRow.Table.PrimaryKey Is Nothing OrElse dataRow.Table.PrimaryKey.Length <= 0 Then Throw New ArgumentException("DataTable must have a PrimaryKey defined")

            'Dim changed As Boolean = False
            'For Each dataColumn As DataColumn In dataRow.Table.Columns
            '    If Not dataColumn.AutoIncrement Then
            '        Dim originalValue As Object
            '        If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
            '            originalValue = dataRow(dataColumn, DataRowVersion.Original)
            '        Else
            '            originalValue = Nothing
            '        End If

            '        Dim currentValue As Object
            '        If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
            '            currentValue = dataRow(dataColumn, DataRowVersion.Current)
            '        Else
            '            currentValue = Nothing
            '        End If

            '        If currentValue IsNot Nothing And originalValue Is Nothing Then
            '            changed = True
            '            Exit For
            '        End If

            '        If currentValue Is Nothing And originalValue IsNot Nothing Then
            '            changed = True
            '            Exit For
            '        End If

            '        If currentValue <> originalValue Then
            '            changed = True
            '            Exit For
            '        End If
            '    End If
            'Next
            'If Not changed Then Return 0 ' Return nothing if no data value has changed.  Cant use RowState because it says modified if value is just set, even if not modified

            If dataRow.Table.Columns.Contains("IntegrityNo") AndAlso dataRow("IntegrityNo", DataRowVersion.Current) = dataRow("IntegrityNo", DataRowVersion.Original) Then dataRow("IntegrityNo") = dataRow("IntegrityNo") + 1
            If Not String.IsNullOrEmpty(UsrId) AndAlso dataRow.Table.Columns.Contains("ModUsrId") Then dataRow("ModUsrId") = UsrId
            If dataRow.Table.Columns.Contains("ModDateTime") Then dataRow("ModDateTime") = Date.Now

            Dim sql As String = "UPDATE [" & dataRow.Table.TableName & "] WITH  (ROWLOCK) SET" & vbCrLf & vbTab

            Dim first As Boolean = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns

                If (dataColumn.ColumnName.ToUpper().Trim().Equals("NTEDESC") Or dataColumn.ColumnName.ToUpper().Trim().Equals("NTEID")) _
                And dataColumn.Table.TableName.ToUpper().Trim().Equals("VEHICLEDEPENDENTRATE") Then Continue For

                If Not dataColumn.AutoIncrement Then
                    Dim originalValue As Object
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        originalValue = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        originalValue = Nothing
                    End If

                    Dim currentValue As Object
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        currentValue = dataRow(dataColumn, DataRowVersion.Current)
                    Else
                        currentValue = Nothing
                    End If

                    sql = sql.ToUpper
                    'If currentValue = originalValue Then Continue For
                    If sql.Contains("ProductRateTravelBand".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("PtbId") Then
                        ''do nothing
                    ElseIf sql.Contains("Agentdependentrate".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("AdrId") Then
                        ''do nothing
                    ElseIf sql.Contains("VehicleDependentRate".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("VdrId") Then
                        ''do nothing
                    ElseIf sql.Contains("ProductRateBookingBand".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("BapId") Then
                        ''do nothing
                    ElseIf sql.Contains("PersonDependentrate".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("PtbId") Then
                        ''do nothing
                    ElseIf sql.Contains("Fixedrate".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("FfrId") Then
                        ''do nothing
                    ElseIf sql.Contains("Personrate".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("PrrId") Then
                        ''do nothing
                    ElseIf sql.Contains("Package".ToUpper) AndAlso UCase(dataColumn.ColumnName) = UCase("PkgId") Then ''** to avoid update for Package ID
                        ''do nothing
                    ElseIf sql.Contains("Location".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("LocCode") Then
                        '' do nothing!!
                    ElseIf sql.Contains("PhoneNumber".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("PhnId") Then
                        '' do nothing!!
                    ElseIf sql.Contains("Agent".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("AgnId") Then
                        '' do nothing!!
                    ElseIf sql.Contains("AddressDetails".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("AddId") Then
                        '' do nothing!!
                    ElseIf sql.Contains("FleetStatus".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("ExceptionList") Then
                        '' do nothing!!
                    ElseIf sql.Contains("Product".ToUpper()) AndAlso UCase(dataColumn.ColumnName) = UCase("PrdId") Then
                        '' do nothing!!
                    ElseIf sql.Contains("VehicleNumberAvailable".ToUpper()) AndAlso (UCase(dataColumn.ColumnName) = UCase("VnaId") OrElse UCase(dataColumn.ColumnName) = UCase("VnaSapId")) Then
                        '' do nothing!!
                    Else
                        If first Then first = False Else sql += vbTab & ","
                        sql += "[" & dataColumn.ColumnName & "] = @NewParam" & CStr(dataColumn.Ordinal)
                    End If


                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Current).ToString()) & vbCrLf
                    Else
                        sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                    End If
                End If
            Next
            sql += vbCrLf

            sql += "WHERE" & vbCrLf & vbTab
            first = True

            For Each dataColumn As DataColumn In dataRow.Table.PrimaryKey
                If first Then first = False Else sql += vbTab & "AND "

                sql += "[" & dataColumn.ColumnName & "] = @OldParam" & CStr(dataColumn.Ordinal)
                If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                    sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Original).ToString()) & vbCrLf
                Else
                    sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                End If
            Next
            'If dataRow.Table.Columns.Contains("IntegrityNo") Then
            '    sql += vbCrLf & vbTab & "AND [IntegrityNo] = " & dataRow("IntegrityNo", DataRowVersion.Original)
            'End If

            sql += ";" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"

            Dim temp As String = ModifySQL(sql, 2)
            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(temp)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns

                    If (dataColumn.ColumnName.ToUpper().Trim().Equals("NTEDESC") Or dataColumn.ColumnName.ToUpper().Trim().Equals("NTEID")) _
                    And dataColumn.Table.TableName.ToUpper().Trim().Equals("VEHICLEDEPENDENTRATE") Then Continue For

                    Dim p As DbParameter

                    p = command.CreateParameter()
                    p.ParameterName = "@NewParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Current)
                    Else
                        p.Value = DBNull.Value
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening


                    'If p.ParameterName = "@NewParam5" Or p.ParameterName = "@NewParam7" Or p.ParameterName = "@NewParam8" Then
                    If Not IsDBNull(p.Value) Then
                        If CStr(p.Value) = "9999999999" Then
                            p.Value = DBNull.Value
                        End If
                    End If
                    'End If

                    command.Parameters.Add(p)

                    p = command.CreateParameter()
                    p.ParameterName = "@OldParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        p.Value = DBNull.Value
                    End If

                    If Not IsDBNull(p.Value) Then
                        If CStr(p.Value) = "9999999999" Then
                            p.Value = DBNull.Value
                        End If
                    End If

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function

        Public Shared Function ExecuteDataRowInsertVehicleDependentRate(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing, Optional ByVal PrgmName As String = Nothing) As Integer
            If dataRow Is Nothing Then Throw New ArgumentNullException()

            If dataRow.RowState = DataRowState.Added Then
                Return ExecuteInsertDataInsertVehicleDependentRate(dataRow, UsrId, PrgmName)
            ElseIf dataRow.RowState = DataRowState.Modified Then
                Return ExecuteDataRowUpdateVehicleDependentRate(dataRow, UsrId)
            ElseIf dataRow.RowState = DataRowState.Deleted Then
                Return ExecuteDataRowDelete(dataRow)
            Else
                Return 0
            End If
        End Function
#End Region

#Region "Rev:mia Jan 5"
        Public Shared Function ExecuteDataRowAgentMaintenance(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing, Optional ByVal PrgmName As String = Nothing) As Integer
            If dataRow Is Nothing Then Throw New ArgumentNullException()

            If dataRow.RowState = DataRowState.Added Then
                Return ExecuteInsertDataInsert(dataRow, UsrId, PrgmName)
            ElseIf dataRow.RowState = DataRowState.Modified Then
                Return ExecuteDataRowUpdateAgentMaintenance(dataRow, UsrId)
            ElseIf dataRow.RowState = DataRowState.Deleted Then
                Return ExecuteDataRowDelete(dataRow)
            Else
                Return 0
            End If
        End Function

        Public Shared Function ExecuteDataRowUpdateAgentMaintenance(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")
            If dataRow.Table.PrimaryKey Is Nothing OrElse dataRow.Table.PrimaryKey.Length <= 0 Then Throw New ArgumentException("DataTable must have a PrimaryKey defined")


            If dataRow.Table.Columns.Contains("IntegrityNo") AndAlso dataRow("IntegrityNo", DataRowVersion.Current) = dataRow("IntegrityNo", DataRowVersion.Original) Then dataRow("IntegrityNo") = dataRow("IntegrityNo") + 1
            If Not String.IsNullOrEmpty(UsrId) AndAlso dataRow.Table.Columns.Contains("ModUsrId") Then dataRow("ModUsrId") = UsrId
            If dataRow.Table.Columns.Contains("ModDateTime") Then dataRow("ModDateTime") = Date.Now

            Dim sql As String = "UPDATE [" & dataRow.Table.TableName & "] SET" & vbCrLf & vbTab

            Dim first As Boolean = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If Not dataColumn.AutoIncrement Then

                    If first Then first = False Else sql += vbTab & ","
                    sql += "[" & dataColumn.ColumnName & "] = @NewParam" & CStr(dataColumn.Ordinal)

                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Current).ToString()) & vbCrLf
                    Else
                        sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                    End If
                End If
            Next
            sql += vbCrLf

            sql += "WHERE" & vbCrLf & vbTab
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.PrimaryKey
                If first Then first = False Else sql += vbTab & "AND "
                sql += "[" & dataColumn.ColumnName & "] = @OldParam" & CStr(dataColumn.Ordinal)

                If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                    sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Original).ToString()) & vbCrLf
                Else
                    sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                End If
            Next
            If dataRow.Table.Columns.Contains("IntegrityNo") Then
                sql += vbCrLf & vbTab & "AND [IntegrityNo] = " & dataRow("IntegrityNo", DataRowVersion.Original)
            End If

            sql += ";" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"

            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(sql)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns
                    Dim p As DbParameter

                    p = command.CreateParameter()
                    p.ParameterName = "@NewParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Current)
                    Else
                        p.Value = DBNull.Value
                    End If


                    If Not IsDBNull(p.Value) Then
                        If CStr(p.Value) = "9999999999" Then
                            p.Value = DBNull.Value
                        End If
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening


                    command.Parameters.Add(p)

                    p = command.CreateParameter()
                    p.ParameterName = "@OldParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        p.Value = DBNull.Value
                    End If

                    If Not IsDBNull(p.Value) Then
                        If CStr(p.Value) = "9999999999" Then
                            p.Value = DBNull.Value
                        End If
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function
#End Region


        Public Shared ReadOnly Property CommandTimeout()
            Get
                Dim result As Integer
                Dim s As String = ConfigurationManager.AppSettings("CommandTimeout")
                If String.IsNullOrEmpty(s) OrElse Not Integer.TryParse(s, result) Then result = DefaultCommandTimeout
                Return result
            End Get
        End Property

        Public Shared Function GetDatabase() As Database
            If DatabaseTransaction.Current Is Nothing Then
                Return DatabaseFactory.CreateDatabase()
            Else
                Return DatabaseTransaction.Current.Database
            End If
        End Function

        Public Shared Function GetConnection() As IDbConnection
            Dim db As Database = GetDatabase()

            If db IsNot Nothing Then
                Return db.CreateConnection()
            End If

            Return Nothing
        End Function

        Public Shared Sub ExecuteCheckParams(ByVal database As Database, ByVal command As DbCommand)
            If database Is Nothing OrElse command Is Nothing Then
                Throw New ArgumentNullException()
            ElseIf DatabaseTransaction.Current IsNot Nothing AndAlso database IsNot DatabaseTransaction.Current.Database Then
                Throw New ArgumentException("Invalid database argument")
            End If
        End Sub

        Public Shared Function ExecuteScalar(ByVal database As Database, ByVal command As DbCommand) As Object
            ExecuteCheckParams(database, command)
            Dim ts As TimeSpan = New TimeSpan(Date.Now.Ticks)

            Try
                If DatabaseTransaction.Current Is Nothing Then
                    Return database.ExecuteScalar(command)
                Else
                    Return database.ExecuteScalar(command, DatabaseTransaction.Current.Transaction)
                End If
            Catch ex As Exception
                Throw ex
            Finally
                Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + CStr((New TimeSpan(Now.Ticks) - ts).TotalSeconds))
            End Try

        End Function

        Public Shared Function ExecuteNonQuery(ByVal database As Database, ByVal command As DbCommand) As Object
            ExecuteCheckParams(database, command)
            Dim ts As TimeSpan = New TimeSpan(Date.Now.Ticks)
            Try
                If DatabaseTransaction.Current Is Nothing Then
                    Return database.ExecuteNonQuery(command)
                Else
                    Return database.ExecuteNonQuery(command, DatabaseTransaction.Current.Transaction)
                End If
            Catch ex As Exception
                Throw ex
            Finally
                Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + CStr((New TimeSpan(Now.Ticks) - ts).TotalSeconds))
            End Try
        End Function

        Public Shared Function GetDataAdapter(ByVal database As Database, ByVal command As DbCommand) As DbDataAdapter
            ExecuteCheckParams(database, command)

            Dim dataAdapter As DbDataAdapter = database.GetDataAdapter()
            dataAdapter.SelectCommand = command
            If DatabaseTransaction.Current Is Nothing Then
                dataAdapter.SelectCommand.Connection = database.CreateConnection()
            Else
                dataAdapter.SelectCommand.Connection = DatabaseTransaction.Current.Connection
                dataAdapter.SelectCommand.Transaction = DatabaseTransaction.Current.Transaction
            End If
            Return dataAdapter
        End Function

        ''' <summary>
        ''' Call sql in the default database and returns a scalar result
        ''' </summary>
        Public Shared Function ExecuteScalarSQL(ByVal sql As String) As Object
            Using New SqlCallBlock(sql)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(sql)
                    command.CommandTimeout = CommandTimeout
                    Return ExecuteScalar(database, command)
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the default database and returns a scalar result
        ''' </summary>
        Public Shared Function ExecuteScalarSP(ByVal storedProcedureName As String, ByVal ParamArray params() As Object) As Object
            Using New SPCallBlock(storedProcedureName, params)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName, params)
                    command.CommandTimeout = CommandTimeout
                    Return ExecuteScalar(database, command)
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Calls a stored procedure with output parameters
        ''' </summary>
        Public Shared Sub ExecuteOutputSP(ByVal storedProcedureName As String, ByRef params As Parameter())
            Dim paramsList As New ArrayList()
            For Each param As Parameter In params
                paramsList.Add(param.Value)
            Next

            Using New SPCallBlock(storedProcedureName, paramsList.ToArray())
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName)
                    command.CommandTimeout = CommandTimeout

                    For Each param As Parameter In params
                        Select Case param.ParamType
                            Case Parameter.ParameterType.AddInParameter
                                database.AddInParameter(command, param.Name, param.Type, param.Value)
                            Case Parameter.ParameterType.AddOutParameter
                                database.AddOutParameter(command, param.Name, param.Type, param.Value)
                        End Select
                    Next

                    ExecuteNonQuery(database, command)

                    For Each param As Parameter In params
                        Select Case param.ParamType
                            Case Parameter.ParameterType.AddOutParameter
                                param.Value = Utility.ParseString(database.GetParameterValue(command, param.Name), "")
                        End Select
                    Next
                End Using
            End Using
        End Sub

        ''' <summary>
        ''' Calls a stored procedure with output parameters and dataset
        ''' </summary>
        Public Shared Function ExecuteOutputDatasetSP(ByVal storedProcedureName As String, ByRef params As Parameter()) As DataSet
            Dim result As New DataSet

            Dim paramsList As New ArrayList()
            For Each param As Parameter In params
                paramsList.Add(param.Value)
            Next

            Using New SPCallBlock(storedProcedureName, paramsList.ToArray())
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName)
                    command.CommandTimeout = CommandTimeout

                    For Each param As Parameter In params
                        Select Case param.ParamType
                            Case Parameter.ParameterType.AddInParameter
                                database.AddInParameter(command, param.Name, param.Type, param.Value)
                            Case Parameter.ParameterType.AddOutParameter
                                database.AddOutParameter(command, param.Name, param.Type, param.Value)
                        End Select
                    Next

                    'ExecuteNonQuery(database, command)

                    Using dataAdapter As DbDataAdapter = GetDataAdapter(database, command)
                        dataAdapter.Fill(result)
                    End Using

                    For Each param As Parameter In params
                        Select Case param.ParamType
                            Case Parameter.ParameterType.AddOutParameter
                                param.Value = Utility.ParseString(database.GetParameterValue(command, param.Name), "")
                        End Select
                    Next
                End Using
            End Using
            Return result
        End Function

        ''' <summary>
        ''' Call sql in the default database and returns a table result
        ''' </summary>
        Public Shared Function ExecuteTableSql(ByVal sql As String, ByVal result As DataTable) As DataTable
            Using New SqlCallBlock(sql)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(sql)
                    command.CommandTimeout = CommandTimeout

                    Using dataAdapter As DbDataAdapter = GetDataAdapter(database, command)
                        Dim ts As TimeSpan = New TimeSpan(Date.Now.Ticks)
                        dataAdapter.Fill(result)
                        Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + CStr((New TimeSpan(Now.Ticks) - ts).TotalSeconds))
                        Return result
                    End Using
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the default database and returns a table result
        ''' </summary>
        Public Shared Function ExecuteTableSP(ByVal storedProcedureName As String, ByVal result As DataTable, ByVal ParamArray params() As Object) As DataTable
            Using New SPCallBlock(storedProcedureName, params)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName, params)
                    command.CommandTimeout = CommandTimeout

                    Using dataAdapter As DbDataAdapter = GetDataAdapter(database, command)
                        dataAdapter.Fill(result)
                        Return result
                    End Using
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the default database and returns a dataset result
        ''' </summary>
        Public Shared Function ExecuteDataSetSP(ByVal storedProcedureName As String, ByVal result As DataSet, ByVal ParamArray params() As Object) As DataSet
            Using New SPCallBlock(storedProcedureName, params)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName, params)
                    command.CommandTimeout = CommandTimeout

                    Using dataAdapter As DbDataAdapter = GetDataAdapter(database, command)
                        Dim ts As TimeSpan = New TimeSpan(Date.Now.Ticks)
                        dataAdapter.Fill(result)
                        Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + CStr((New TimeSpan(Now.Ticks) - ts).TotalSeconds))
                        Return result
                    End Using
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Call sql in the default database and returns a dataset result
        ''' </summary>
        Public Shared Function ExecuteDataSetSql(ByVal sql As String, ByVal result As DataSet) As DataSet
            Using New SqlCallBlock(sql)
                Dim database As Database = GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(sql)
                    command.CommandTimeout = CommandTimeout

                    Using dataAdapter As DbDataAdapter = GetDataAdapter(database, command)
                        Dim ts As TimeSpan = New TimeSpan(Date.Now.Ticks)
                        dataAdapter.Fill(result)
                        Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + CStr((New TimeSpan(Now.Ticks) - ts).TotalSeconds))
                        Return result
                    End Using
                End Using
            End Using
        End Function

        Public Shared Function ExecuteOutputObjectSP(ByVal storedProcedureName As String, ByRef params As Parameter()) As Object
            'Try
            ExecuteOutputSP(storedProcedureName, params)
            Return "SUCCESS"
            'Catch ex As Exception
            '    Return "FAILED"
            '    Try
            '        'Logging.LogException("DATALAYER Exception" & storedProcedureName & " failed with exception ", ex)
            '        Aurora.Common.Logging.LogException("ExecuteOutputObjectSP", ex)
            '        Aurora.Common.Logging.LogInformation("ExecuteOutputObjectSP", "Exception Stack Trace = " & ex.StackTrace)
            '    Catch
            '    End Try
            'End Try

        End Function

        Protected Shared Function GetCurrentConnection(ByVal db As Database) As DbConnection
            If DatabaseTransaction.Current Is Nothing Then
                Return db.CreateConnection()
            End If



            Return DatabaseTransaction.Current.Connection
        End Function
#End Region

#Region "DataRow-based Low Level Database Calls"

        Public Shared Sub CopyDataTable(ByVal source As DataTable, ByVal target As DataTable)
            For Each dataRow As DataRow In source.Rows

                Try
                    ''rev:mia July 19 2012 - add a trapping here to maxlenght violation error in BpdUnitNum
                    If ((target.TableName.ToUpper = "Checkout".ToUpper) Or (target.TableName.ToUpper = "CheckIn".ToUpper)) And source.Columns.Contains("BpdUnitNum") = True Then

                        If (target.Columns("BpdUnitNum").MaxLength = 16) Then target.Columns("BpdUnitNum").MaxLength = 64
                        ''System.Diagnostics.Debug.WriteLine(dataRow("BpdUnitNum").ToString.Length.ToString + " " + dataRow("BpdUnitNum").ToString)
                        ''Logging.LogDebug("checking", dataRow("BpdUnitNum").ToString.Length)
                    End If
                Catch ex As Exception
                End Try


                target.ImportRow(dataRow)
            Next
        End Sub

        Private Shared Function ConvertToDbType(ByVal t As Type) As DbType
            If t Is GetType(System.Boolean) Then Return DbType.Boolean
            If t Is GetType(System.Int16) Then Return DbType.Int16
            If t Is GetType(System.Int32) Then Return DbType.Int32
            If t Is GetType(System.Int64) Then Return DbType.Int64
            If t Is GetType(System.Double) Then Return DbType.Double
            If t Is GetType(System.Single) Then Return DbType.Single
            If t Is GetType(System.Decimal) Then Return DbType.Decimal
            If t Is GetType(System.String) Then Return DbType.String
            If t Is GetType(System.DateTime) Then Return DbType.DateTime
            If t Is GetType(System.Byte()) Then Return DbType.Binary
            If t Is GetType(System.Guid) Then Return DbType.Guid
            Return DbType.String
        End Function

        Public Shared Function ExecuteInsertDataInsert(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing, Optional ByVal PrgmName As String = Nothing) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")

            If dataRow.Table.Columns.Contains("IntegrityNo") Then dataRow("IntegrityNo") = 1
            If Not String.IsNullOrEmpty(UsrId) AndAlso dataRow.Table.Columns.Contains("AddUsrId") Then dataRow("AddUsrId") = UsrId
            If dataRow.Table.Columns.Contains("AddDateTime") Then dataRow("AddDateTime") = Date.Now
            If Not String.IsNullOrEmpty(PrgmName) AndAlso dataRow.Table.Columns.Contains("AddPrgmName") Then dataRow("AddPrgmName") = PrgmName

            Dim sql As String = "INSERT INTO [" & dataRow.Table.TableName & "]" & vbCrLf

            Dim first As Boolean
            Dim index As Integer

            sql += "(" & vbCrLf
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If Not dataColumn.AutoIncrement Then
                    If first Then first = False Else sql += "," & vbCrLf
                    sql += vbTab & "[" & dataColumn.ColumnName & "]"
                End If
            Next
            sql += ")" & vbCrLf

            sql += "VALUES" & vbCrLf
            sql += "(" & vbCrLf & vbTab
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If Not dataColumn.AutoIncrement Then
                    If first Then first = False Else sql += vbTab & ","
                    index += 1

                    sql += "@Param" & CStr(dataColumn.Ordinal)

                    If Not dataRow.IsNull(dataColumn) Then
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataColumn.ColumnName & "=" & dataRow(dataColumn).ToString()) & vbCrLf
                    Else
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataColumn.ColumnName & "=NULL") & vbCrLf
                    End If
                End If
            Next
            sql += vbCrLf & ");" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"

            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(sql)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns
                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = "@Param" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn) Then
                        p.Value = dataRow(dataColumn)
                    Else
                        p.Value = DBNull.Value
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function

        Public Shared Function ExecuteDataRowUpdate(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")
            If dataRow.Table.PrimaryKey Is Nothing OrElse dataRow.Table.PrimaryKey.Length <= 0 Then Throw New ArgumentException("DataTable must have a PrimaryKey defined")

            'Dim changed As Boolean = False
            'For Each dataColumn As DataColumn In dataRow.Table.Columns
            '    If Not dataColumn.AutoIncrement Then
            '        Dim originalValue As Object
            '        If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
            '            originalValue = dataRow(dataColumn, DataRowVersion.Original)
            '        Else
            '            originalValue = Nothing
            '        End If

            '        Dim currentValue As Object
            '        If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
            '            currentValue = dataRow(dataColumn, DataRowVersion.Current)
            '        Else
            '            currentValue = Nothing
            '        End If

            '        If currentValue IsNot Nothing And originalValue Is Nothing Then
            '            changed = True
            '            Exit For
            '        End If

            '        If currentValue Is Nothing And originalValue IsNot Nothing Then
            '            changed = True
            '            Exit For
            '        End If

            '        If currentValue <> originalValue Then
            '            changed = True
            '            Exit For
            '        End If
            '    End If
            'Next
            'If Not changed Then Return 0 ' Return nothing if no data value has changed.  Cant use RowState because it says modified if value is just set, even if not modified

            If dataRow.Table.Columns.Contains("IntegrityNo") AndAlso dataRow("IntegrityNo", DataRowVersion.Current) = dataRow("IntegrityNo", DataRowVersion.Original) Then dataRow("IntegrityNo") = dataRow("IntegrityNo") + 1
            If Not String.IsNullOrEmpty(UsrId) AndAlso dataRow.Table.Columns.Contains("ModUsrId") Then dataRow("ModUsrId") = UsrId
            If dataRow.Table.Columns.Contains("ModDateTime") Then dataRow("ModDateTime") = Date.Now

            Dim sql As String = "UPDATE [" & dataRow.Table.TableName & "] SET" & vbCrLf & vbTab

            Dim first As Boolean = True
            For Each dataColumn As DataColumn In dataRow.Table.Columns
                If Not dataColumn.AutoIncrement Then
                    Dim originalValue As Object
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        originalValue = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        originalValue = Nothing
                    End If

                    Dim currentValue As Object
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        currentValue = dataRow(dataColumn, DataRowVersion.Current)
                    Else
                        currentValue = Nothing
                    End If

                    'If currentValue = originalValue Then Continue For

                    If first Then first = False Else sql += vbTab & ","
                    sql += "[" & dataColumn.ColumnName & "] = @NewParam" & CStr(dataColumn.Ordinal)

                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Current).ToString()) & vbCrLf
                    Else
                        sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                    End If
                End If
            Next
            sql += vbCrLf

            sql += "WHERE" & vbCrLf & vbTab
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.PrimaryKey
                If first Then first = False Else sql += vbTab & "AND "
                sql += "[" & dataColumn.ColumnName & "] = @OldParam" & CStr(dataColumn.Ordinal)

                If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                    sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Original).ToString()) & vbCrLf
                Else
                    sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                End If
            Next
            If dataRow.Table.Columns.Contains("IntegrityNo") Then
                sql += vbCrLf & vbTab & "AND [IntegrityNo] = " & dataRow("IntegrityNo", DataRowVersion.Original)
            End If

            sql += ";" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"

            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(sql)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns
                    Dim p As DbParameter

                    p = command.CreateParameter()
                    p.ParameterName = "@NewParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Current) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Current)
                    Else
                        p.Value = DBNull.Value
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)

                    p = command.CreateParameter()
                    p.ParameterName = "@OldParam" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        p.Value = DBNull.Value
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function

        Public Shared Function ExecuteDataRowDelete(ByVal dataRow As DataRow) As Integer

            If dataRow Is Nothing Then Throw New ArgumentNullException()
            If dataRow.RowState = DataRowState.Detached Then Throw New ArgumentException("DataRow may not be detached")
            If dataRow.Table Is Nothing OrElse String.IsNullOrEmpty(dataRow.Table.TableName) Then Throw New ArgumentException("DataRow must have a valid table name defined")
            If dataRow.Table.PrimaryKey Is Nothing OrElse dataRow.Table.PrimaryKey.Length <= 0 Then Throw New ArgumentException("DataTable must have a PrimaryKey defined")

            Dim sql As String = "DELETE FROM [" & dataRow.Table.TableName & "] " & vbCrLf

            Dim first As Boolean

            sql += "WHERE" & vbCrLf & vbTab
            first = True
            For Each dataColumn As DataColumn In dataRow.Table.PrimaryKey
                If first Then first = False Else sql += vbTab & "AND "
                sql += "[" & dataColumn.ColumnName & "] = @Param" & CStr(dataColumn.Ordinal)

                If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                    sql += " " & Utility.EscapeSqlSingleLineComment(dataRow(dataColumn, DataRowVersion.Original).ToString()) & vbCrLf
                Else
                    sql += " " & Utility.EscapeSqlSingleLineComment("NULL") & vbCrLf
                End If
            Next
            If dataRow.Table.Columns.Contains("IntegrityNo") Then
                sql += vbTab & "AND [IntegrityNo] = " & dataRow("IntegrityNo", DataRowVersion.Original) & vbCrLf
            End If

            sql += ";" & vbCrLf & vbCrLf & "SELECT @@ROWCOUNT"

            Dim database As Database = GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(sql)
                command.CommandTimeout = CommandTimeout

                For Each dataColumn As DataColumn In dataRow.Table.Columns
                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = "@Param" & CStr(dataColumn.Ordinal)
                    p.DbType = ConvertToDbType(dataColumn.DataType)
                    If Not dataRow.IsNull(dataColumn, DataRowVersion.Original) Then
                        p.Value = dataRow(dataColumn, DataRowVersion.Original)
                    Else
                        p.Value = DBNull.Value
                    End If

                    ' 3.3.9 - Shoel - hack to stop weird and time consuming varchar to nvarchar conversions from happening
                    If p.DbType = DbType.String Then
                        CType(p, System.Data.SqlClient.SqlParameter).SqlDbType = SqlDbType.VarChar
                    End If
                    ' 3.3.9 - Shoel - hack to stop weird and time consuming conversions from happening

                    command.Parameters.Add(p)
                Next

                Return CType(ExecuteScalar(database, command), Integer)
            End Using
        End Function

        Public Shared Function ExecuteDataRow(ByVal dataRow As DataRow, Optional ByVal UsrId As String = Nothing, Optional ByVal PrgmName As String = Nothing) As Integer
            If dataRow Is Nothing Then Throw New ArgumentNullException()

            If dataRow.RowState = DataRowState.Added Then
                Return ExecuteInsertDataInsert(dataRow, UsrId, PrgmName)
            ElseIf dataRow.RowState = DataRowState.Modified Then
                Return ExecuteDataRowUpdate(dataRow, UsrId)
            ElseIf dataRow.RowState = DataRowState.Deleted Then
                Return ExecuteDataRowDelete(dataRow)
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetAddModDescription(ByVal dataRow As DataRow) As String
            If dataRow Is Nothing Then Throw New ArgumentNullException()

            Dim result As String = ""

            If dataRow.Table.Columns.Contains("AddDateTime") AndAlso dataRow.Table.Columns.Contains("AddUsrId") _
             AndAlso Not dataRow.IsNull("AddDateTime") AndAlso Not dataRow.IsNull("AddUsrId") Then
                result += "Created " & CType(dataRow("AddDateTime"), Date).ToString(Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") & " by " & dataRow("AddUsrId").ToString()
            End If

            If dataRow.Table.Columns.Contains("ModDateTime") AndAlso dataRow.Table.Columns.Contains("ModUsrId") _
             AndAlso Not dataRow.IsNull("ModDateTime") AndAlso Not dataRow.IsNull("ModUsrId") Then
                If Not String.IsNullOrEmpty(result) Then
                    result += ", modified "
                Else
                    result += "Modified "
                End If
                result += " " & CType(dataRow("ModDateTime"), Date).ToString(Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") & " by " & dataRow("ModUsrId").ToString()
            End If

            Return result
        End Function

#End Region

#Region "Parameter"

        Public Class Parameter

            Public Enum ParameterType
                AddInParameter = 1
                AddOutParameter = 2
            End Enum

            Sub New(ByVal name As String, ByVal type As String, ByVal value As Object, ByVal paramType As Parameter.ParameterType)
                _name = Name
                _type = Type
                _value = Value
                _paramType = ParamType
            End Sub

            Private _name As String
            Public ReadOnly Property Name()
                Get
                    Return _name
                End Get
            End Property

            Private _type As DbType
            Public ReadOnly Property Type()
                Get
                    Return _type
                End Get
            End Property

            Private _value As Object
            Public Property Value()
                Get
                    Return _value
                End Get
                Set(ByVal value)
                    _value = value
                End Set
            End Property

            Private _paramType As Integer
            Public ReadOnly Property ParamType()
                Get
                    Return _paramType
                End Get
            End Property
        End Class

#End Region

#Region "Database Transactions"

        ''' <summary>
        ''' Simple Transaction wrapper using the IDisposable and Singleton Pattern.  A transaction is cached in the 
        ''' current web request (or globally for non-web applications) and only one transaction may be created at a time.
        ''' The low level database calls in this module use the transaction if one is current.
        ''' </summary>
        ''' <example>
        '''Using transaction As New DatabaseTransaction()
        '''    Data.ExecuteScalarSP("UpdateSomething", param1, param2)
        '''    transaction.CommitTransaction()
        '''End Using
        ''' </example>
        Public Class DatabaseTransaction
            Implements IDisposable

            Public Const CurrentKey As String = "Aurora.Common.Data.DatabaseTransaction.Current"

            Private Shared _current As DatabaseTransaction = Nothing
            ''' <summary>
            ''' The current transaction (if a transaction is currently running in the web request or application)
            ''' </summary>
            Public Shared ReadOnly Property Current() As DatabaseTransaction
                Get
                    If System.Web.HttpContext.Current Is Nothing Then
                        Return _current
                    ElseIf System.Web.HttpContext.Current.Items.Contains(CurrentKey) Then
                        Return CType(System.Web.HttpContext.Current.Items(CurrentKey), DatabaseTransaction)
                    Else
                        Return Nothing
                    End If
                End Get
            End Property

            Private Shared Sub SetCurrent(ByVal value As DatabaseTransaction)
                If System.Web.HttpContext.Current Is Nothing Then
                    _current = value
                ElseIf System.Web.HttpContext.Current.Items.Contains(CurrentKey) Then
                    System.Web.HttpContext.Current.Items(CurrentKey) = value
                Else
                    System.Web.HttpContext.Current.Items.Add(CurrentKey, value)
                End If
            End Sub

            Private _database As Database = Nothing
            ''' <summary>
            ''' The current database (if not already closed)
            ''' </summary>
            Public ReadOnly Property Database() As Database
                Get
                    Return _database
                End Get
            End Property

            Private _connection As DbConnection = Nothing
            ''' <summary>
            ''' The current connection (if not already closed)
            ''' </summary>
            Public ReadOnly Property Connection() As DbConnection
                Get
                    Return _connection
                End Get
            End Property

            Private _transaction As DbTransaction = Nothing
            ''' <summary>
            ''' The current connection (if not already closed)
            ''' </summary>
            Public ReadOnly Property Transaction() As DbTransaction
                Get
                    Return _transaction
                End Get
            End Property

            Private _isClosed As Boolean = False
            ''' <summary>
            ''' Has the transaction been closed? (committed or rolled back)
            ''' </summary>
            Public ReadOnly Property IsClosed() As Boolean
                Get
                    Return _isClosed
                End Get
            End Property

            Public Sub New()
                If Not Current Is Nothing Then Throw New InvalidOperationException("Transaction already exists")

                _database = DatabaseFactory.CreateDatabase()
                _connection = _database.CreateConnection()
                _connection.Open()
                _transaction = _connection.BeginTransaction()

                SetCurrent(Me)
            End Sub

            Public Sub Dispose() Implements System.IDisposable.Dispose
                If Not _isClosed Then
                    RollbackTransaction()
                End If
            End Sub

            ''' <summary>
            ''' Commit the transaction.
            ''' </summary>
            Public Sub CommitTransaction()
                If _isClosed Then Throw New InvalidOperationException("Transaction already closed")

                _isClosed = True
                SetCurrent(Nothing)

                _transaction.Commit()
                _connection.Close()

                _transaction.Dispose() : _transaction = Nothing
                _connection.Dispose() : _connection = Nothing
                _database = Nothing
            End Sub

            ''' <summary>
            ''' Rollback the transaction.  Called automatically if Dispose is called before the 
            ''' transaction has been Committed or Rolled Back
            ''' </summary>
            Public Sub RollbackTransaction()
                If _isClosed Then Throw New InvalidOperationException("Transaction already closed")

                _isClosed = True
                SetCurrent(Nothing)

                _transaction.Rollback()
                _connection.Close()

                _transaction.Dispose() : _transaction = Nothing
                _connection.Dispose() : _connection = Nothing
                _database = Nothing
            End Sub

        End Class
#End Region

#Region "SQL XML provider methods"
        Private Shared Function ConvertDataSetToXml(ByVal dataSet As DataSet) As String
            Using result As New StringWriter()
                result.Write("<data>")
                For Each dataTable As DataTable In dataSet.Tables
                    For Each dataRow As DataRow In dataTable.Rows
                        result.Write(dataRow(0).ToString())
                    Next
                Next
                result.Write("</data>")
                Return result.ToString()
            End Using
        End Function

        ''' <summary>
        ''' Executes sql in the "Aurora" database using "SQLOLEDB" and returns a Xml 
        ''' formatted string result ("<data></data>")
        ''' </summary>
        Public Shared Function ExecuteSqlXml(ByVal sql As String) As String
            Dim dataSet As DataSet = ExecuteDataSetSql(sql, New DataSet())
            Return ConvertDataSetToXml(dataSet)
        End Function

        ''' <summary>
        ''' Executes sql in the "Aurora" database using "SQLOLEDB" and returns a Xml 
        ''' formatted string result ("<data></data>")
        ''' </summary>
        Public Shared Function ExecuteSqlXmlDoc(ByVal sql As String) As XmlDocument
            Dim xml As String = ExecuteSqlXml(sql)
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(xml)
            Return xmlDoc
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the "Aurora" database using "SQLOLEDB" and returns a Xml 
        ''' formatted string result ("<data></data>")
        ''' </summary>
        Public Shared Function ExecuteSqlXmlSP(ByVal storedProcedureName As String, ByVal ParamArray params() As Object) As String
            Dim dataSet As DataSet = ExecuteDataSetSP(storedProcedureName, New DataSet(), params)
            Return ConvertDataSetToXml(dataSet)
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the "Aurora" database using "SQLOLEDB" and returns a Xml DOM result.
        ''' Returns Nothing if a string error message was returns, or the xml could not be parsed.
        ''' </summary>
        Public Shared Function ExecuteSqlXmlSPDoc(ByVal storedProcedureName As String, ByVal ParamArray params() As Object) As XmlDocument
            Dim xml As String = ExecuteSqlXmlSP(storedProcedureName, params)
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(xml)
            Return xmlDoc
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the "Aurora" database using "SQLOLEDB" and returns a DataSet result.
        ''' Returns Nothing if a string error message was returns, or the xml could not be parsed.
        ''' </summary>
        Public Shared Function ExecuteSqlXmlSPDataSet(ByVal storedProcedureName As String, ByVal ParamArray params() As Object) As DataSet
            Dim result As New DataSet()
            result.ReadXml(New XmlNodeReader(Data.ExecuteSqlXmlSPDoc(storedProcedureName, params)))
            Return result
        End Function

        ''' <summary>
        ''' Calls a stored procedure in the "Aurora" database using "SQLOLEDB" and returns a Xml DOM result.
        ''' Will attempt to correct any escaping errors in the xml if it finds any e.g. unescaped ampersands.
        ''' Returns Nothing if a string error message was returns, or the xml could not be parsed.
        ''' </summary>
        Public Shared Function ExecuteSqlXmlSPDocCorrecting(ByVal storedProcedureName As String, ByVal ParamArray params() As Object) As XmlDocument
            Dim result As XmlDocument = Nothing
            Dim xml As String = ExecuteSqlXmlSP(storedProcedureName, params)

            If Not String.IsNullOrEmpty(xml) Then
                Try
                    result = New XmlDocument()
                    result.LoadXml(xml)
                Catch
                    Try
                        xml = xml.Replace("&", "&amp;")

                        result = New XmlDocument()
                        result.LoadXml(xml)
                    Catch
                        result = Nothing
                    End Try
                End Try
            End If
            Return result
        End Function


        Public Const SqlXmlSPDocErrorXpath As String = "/data/Error"
        Public Const SqlXmlSPDocErrorStatusXpath As String = "ErrStatus"
        Public Const SqlXmlSPDocErrorCodeXpath As String = "ErrCode"
        Public Const SqlXmlSPDocErrorDescXpath As String = "ErrDesc"
        Public Const SqlXmlSPDocErrorDescXpath2 As String = "ErrDescription"

        Public Shared Function HasSqlXmlSPDocError(ByVal doc As XmlDocument, ByRef status As String, ByRef code As String, ByRef desc As String) As Boolean
            status = Nothing
            code = Nothing
            desc = Nothing

            If doc Is Nothing Then Throw (New ArgumentNullException())

            Dim errorNode As XmlNode = doc.SelectSingleNode(SqlXmlSPDocErrorXpath)
            If errorNode Is Nothing Then Return False

            Dim statusNode As XmlNode = errorNode.SelectSingleNode(SqlXmlSPDocErrorStatusXpath)
            If statusNode IsNot Nothing Then status = statusNode.InnerText

            Dim codeNode As XmlNode = errorNode.SelectSingleNode(SqlXmlSPDocErrorCodeXpath)
            If codeNode IsNot Nothing Then code = codeNode.InnerText

            Dim descNode As XmlNode = errorNode.SelectSingleNode(SqlXmlSPDocErrorDescXpath)
            Dim descNode2 As XmlNode = errorNode.SelectSingleNode(SqlXmlSPDocErrorDescXpath2)
            If descNode IsNot Nothing Then
                desc = descNode.InnerText
            ElseIf descNode2 IsNot Nothing Then
                desc = descNode2.InnerText
            End If

            Return True
        End Function

        Public Shared Function HasSqlXmlSPDocError(ByVal doc As XmlDocument) As Boolean
            Dim status As String = Nothing
            Dim code As String = Nothing
            Dim desc As String = Nothing
            Return HasSqlXmlSPDocError(doc, status, code, desc)
        End Function

#End Region

#Region "ExecuteListMethods"
        ''' <summary>
        ''' Executes an SP that populates one list-based resultset.
        ''' </summary>
        ''' <param name="storedProcedureName">The name of the stored procedure to execute.</param>
        ''' <param name="dataset">Dataset object instance used to create and populate the result object with data.</param>
        ''' <param name="parameters">Stored procedure parameters.</param>
        ''' <remarks></remarks>
        Public Shared Sub ExecuteListSP(ByVal storedProcedureName As String, ByVal dataset As IListDataSet, _
                                                   ByVal ParamArray parameters() As ParamInfo)
            ExecuteListSP(storedProcedureName, _
                                 {dataset}, _
                                 parameters)
        End Sub

        ''' <summary>
        ''' Executes an SP that populates mutliple list-based resultsets.
        ''' </summary>
        ''' <param name="storedProcedureName">The name of the stored procedure to execute.</param>
        ''' <param name="dataset">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="dataset2">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="parameters">Stored procedure parameters.</param>
        ''' <remarks></remarks>
        Public Shared Sub ExecuteListSP(ByVal storedProcedureName As String, ByVal dataset As IListDataSet, _
                                                   ByVal dataset2 As IListDataSet, ByVal ParamArray parameters() As ParamInfo)

            ExecuteListSP(storedProcedureName, _
                                 {dataset, dataset2}, _
                                 parameters)
        End Sub

        ''' <summary>
        ''' Executes an SP that populates mutliple list-based resultsets.
        ''' </summary>
        ''' <param name="storedProcedureName">The name of the stored procedure to execute.</param>
        ''' <param name="dataset">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="dataset2">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="dataset3">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="parameters">Stored procedure parameters.</param>
        ''' <remarks></remarks>
        Public Shared Sub ExecuteListSP(ByVal storedProcedureName As String, ByVal dataset As IListDataSet, _
                                                  ByVal dataset2 As IListDataSet, _
                                                  ByVal dataset3 As IListDataSet, _
                                                  ByVal ParamArray parameters() As ParamInfo)

            ExecuteListSP(storedProcedureName, _
                                 {dataset, dataset2, dataset3}, _
                                 parameters)

        End Sub

        ''' <summary>
        ''' Executes an SP that populates mutliple list-based resultsets
        ''' </summary>
        ''' <param name="storedProcedureName">The name of the stored procedure to execute.</param>
        ''' <param name="datasets">Dataset object instance used to create and populate the result objects with data.</param>
        ''' <param name="parameters">Stored procedure parameters.</param>
        ''' <remarks></remarks>
        Public Shared Sub ExecuteListSP(ByVal storedProcedureName As String, ByVal dataSets() As IListDataSet, ByVal ParamArray parameters() As ParamInfo)
            If dataSets Is Nothing Then
                Throw New ArgumentNullException("datasets")
            End If

            Dim database As Database = GetDatabase()

            Using command As DbCommand = database.GetStoredProcCommand(storedProcedureName)

                command.CommandTimeout = CommandTimeout

                Dim connection As DbConnection
                Dim transaction As DbTransaction = Nothing

                'Get the request-level connection if available, otherwise create a new one.
                If DatabaseTransaction.Current Is Nothing Then
                    connection = database.CreateConnection()
                Else
                    Dim transactionWrapper As DatabaseTransaction = DatabaseTransaction.Current

                    connection = transactionWrapper.Connection
                    transaction = transactionWrapper.Transaction
                End If


                If parameters IsNot Nothing Then
                    For Each param As ParamInfo In parameters
                        Dim commandParam As IDataParameter = command.CreateParameter()

                        commandParam.Value = param.Value
                        commandParam.ParameterName = param.Name
                        command.Parameters.Add(commandParam)
                    Next
                End If


                Dim handleConnectionState As Boolean = connection.State = ConnectionState.Closed OrElse connection.State = ConnectionState.Broken


                'Measure the time taken to execute the SP
                Dim sw As New System.Diagnostics.Stopwatch()
                sw.Start()

                Dim reader As DbDataReader = Nothing

                Try
                    If handleConnectionState Then
                        connection.Open()
                    End If

                    command.Connection = connection

                    reader = command.ExecuteReader()

                    For Each dataset As IListDataSet In dataSets

                        If dataset IsNot Nothing Then
                            While reader.Read()
                                dataset.AddItemFromReader(reader)
                            End While

                            If Not reader.NextResult() Then
                                Exit For
                            End If

                        End If
                    Next


                Catch ex As Exception
                    If transaction IsNot Nothing Then
                        transaction.Rollback()
                    End If

                    Throw ex
                Finally

                    If reader IsNot Nothing Then
                        reader.Close()
                    End If

                    If handleConnectionState AndAlso connection IsNot Nothing Then
                        connection.Close()
                    End If

                    sw.Stop()
                    Logging.LogDebug("SQL", command.CommandText + vbCrLf + "--Time: " + sw.Elapsed.TotalSeconds.ToString())
                End Try
            End Using
        End Sub
#End Region

    End Class


    Public Class ParamInfo
        Private mValue As Object
        Private mName As String

        Public Property Value As Object
            Get
                Return mValue
            End Get
            Set(ByVal value As Object)
                mValue = value
            End Set
        End Property

        Public Property Name As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Sub New(ByVal value As Object, ByVal name As String)
            mValue = value
            mName = name
        End Sub
    End Class


    Public Interface IListDataSet
        Sub AddItemFromReader(ByVal reader As DbDataReader)
    End Interface

    Public Interface IListDataSet(Of T)
        Inherits IListDataSet

        ReadOnly Property ResultSet As List(Of T)

        Function CreateItemFromReader(ByVal reader As DbDataReader) As T
    End Interface

    Public Class ListDataSet(Of T)
        Implements IListDataSet(Of T)

        Protected mList As List(Of T) = New List(Of T)

        Public ReadOnly Property ResultSet As System.Collections.Generic.List(Of T) Implements IListDataSet(Of T).ResultSet
            Get
                Return mList
            End Get
        End Property



        Public Overridable Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As T Implements IListDataSet(Of T).CreateItemFromReader

        End Function



        Friend Sub AddItemFromReader(ByVal reader As System.Data.Common.DbDataReader) Implements IListDataSet.AddItemFromReader
            Dim item As T = CreateItemFromReader(reader)

            mList.Add(item)
        End Sub



        Public Shared Function GetDbValue(Of TVal)(ByVal reader As IDataReader, ByVal field As String) As TVal
            Dim value As Object = reader(field)

            If value Is Nothing OrElse Object.ReferenceEquals(value, DBNull.Value) Then
                Return Nothing
            End If

            Return CType(value, TVal)
        End Function

        Public Shared Function GetDbValue(Of TVal)(ByVal value As Object) As TVal
            If value Is Nothing OrElse Object.ReferenceEquals(value, DBNull.Value) Then
                Return Nothing
            End If

            Return CType(value, TVal)
        End Function
    End Class

End Namespace