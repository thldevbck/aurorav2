Imports System
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Text
Imports System.IO
Imports System.Diagnostics
Imports Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners
Imports Microsoft.Practices.EnterpriseLibrary.Common.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Formatters
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Namespace Aurora.Common

    <ConfigurationElementType(GetType(CustomTraceListenerData))> _
    Public Class DateFlatFileTraceListener
        Inherits CustomTraceListener

        Public Const DATETOKEN As String = "{Date}"

        Public Overrides Sub TraceData(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal data As Object)
            If (TypeOf data Is LogEntry) And Me.Formatter IsNot Nothing Then
                WriteLine(Me.Formatter.Format(DirectCast(data, LogEntry)))
            Else
                WriteLine(data.ToString())
            End If
        End Sub

        Public Overrides Sub Write(ByVal message As String)
            Dim filename As String = MyBase.Attributes("fileName")
            If Not Path.IsPathRooted(filename) Then filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename)
            filename = filename.Replace(DATETOKEN, DateTime.Today.ToString("yyyyMMdd"))

            Dim directoryName As String = Path.GetDirectoryName(filename)
            If Not String.IsNullOrEmpty(directoryName) AndAlso Not System.IO.Directory.Exists(directoryName) Then
                System.IO.Directory.CreateDirectory(directoryName)
            End If

            Using stream As New FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Using writer As StreamWriter = New StreamWriter(stream)
                    writer.Write(message)
                End Using
            End Using
        End Sub

        Public Overrides Sub WriteLine(ByVal message As String)
            Write(message + vbCrLf)
        End Sub

    End Class

End Namespace