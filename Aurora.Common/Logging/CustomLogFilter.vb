Imports System
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Text
Imports System.Diagnostics
Imports Microsoft.Practices.EnterpriseLibrary.Common.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Namespace Aurora.Common

    <ConfigurationElementType(GetType(CustomLogFilterData))> _
    Public Class CustomLogFilter : Inherits LogFilter

        Private enabled As Boolean
        Private titles As List(Of String) = New List(Of String)

        Public Sub New(ByVal values As NameValueCollection)
            MyBase.New(values.Item("name"))

            enabled = Convert.ToBoolean(values.Item("enabled"))
            For Each title As String In values.Item("titles").Split(";")
                titles.Add(Trim(UCase(title)))
            Next
        End Sub

        Public Overrides Function Filter(ByVal logEntry As LogEntry) As Boolean
            If enabled Then
                Return titles.Contains(Trim(UCase(logEntry.Title)))
            Else
                Return True
            End If
        End Function

    End Class

End Namespace