Imports Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners
Imports Microsoft.Practices.EnterpriseLibrary.Common.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Namespace Aurora.Common

    ''' <summary>
    ''' Trace listener that writes formatted messages to the Console
    ''' </summary>
    <ConfigurationElementType(GetType(CustomTraceListenerData))> _
    Public Class ConsoleTraceListener
        Inherits CustomTraceListener

        Public Overrides Sub TraceData(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal data As Object)
            If (TypeOf data Is LogEntry) And Me.Formatter IsNot Nothing Then
                WriteLine(Me.Formatter.Format(DirectCast(data, LogEntry)))
            Else
                WriteLine(data.ToString())
            End If
        End Sub

        ''' <summary>
        ''' Writes a message to the console
        ''' </summary>
        ''' <param name="message">The string to write to the debug window</param>
        Public Overrides Sub Write(ByVal message As String)
            Console.Write(message)
        End Sub

        ''' <summary>
        ''' Writes a message to the console
        ''' </summary>
        ''' <param name="message">The string to write to the debug window</param>
        Public Overrides Sub WriteLine(ByVal message As String)
            Console.WriteLine(message)
        End Sub

    End Class

End Namespace