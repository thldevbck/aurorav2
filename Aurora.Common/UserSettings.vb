Namespace Aurora.Common

    Public Class UserSettings

#Region "Defaults"
        Public Const DefaultCulture As String = "en-nz"
        Public Const DefaultDateFormat As String = "dd/MM/yyyy"
#End Region

#Region "Properties"
        Private _usrId As String = String.Empty
        Public ReadOnly Property UsrId() As String
            Get
                Return _usrId
            End Get
        End Property

        Private _usrCode As String = String.Empty
        Public ReadOnly Property UsrCode() As String
            Get
                Return _usrCode
            End Get
        End Property

        Private _usrName As String = String.Empty
        Public ReadOnly Property UsrName() As String
            Get
                Return _usrName
            End Get
        End Property

        Private _comCode As String = String.Empty
        Public ReadOnly Property ComCode() As String
            Get
                Return _comCode
            End Get
        End Property

        Private _comName As String = String.Empty
        Public ReadOnly Property ComName() As String
            Get
                Return _comName
            End Get
        End Property

        Private _locCode As String = String.Empty
        Public ReadOnly Property LocCode() As String
            Get
                Return _locCode
            End Get
        End Property

        Private _locName As String = String.Empty
        Public ReadOnly Property LocName() As String
            Get
                Return _locName
            End Get
        End Property

        Private _locHoursBehindServerLocation As Double = 0
        Public ReadOnly Property LocHoursBehindServerLocation() As Double
            Get
                Return _locHoursBehindServerLocation
            End Get
        End Property

        Private _tctCode As String = String.Empty
        Public ReadOnly Property TctCode() As String
            Get
                Return _tctCode
            End Get
        End Property

        Private _tctName As String = String.Empty
        Public ReadOnly Property TctName() As String
            Get
                Return _tctName
            End Get
        End Property

        Private _ctyCode As String = String.Empty
        Public ReadOnly Property CtyCode() As String
            Get
                Return _ctyCode
            End Get
        End Property

        Private _ctyDefCurrCode As String = String.Empty
        Public ReadOnly Property CtyDefCurrCode() As String
            Get
                Return _ctyDefCurrCode
            End Get
        End Property

        Private _ctyName As String = String.Empty
        Public ReadOnly Property CtyName() As String
            Get
                Return _ctyName
            End Get
        End Property

        Private _systemIdentifier As String = String.Empty
        Public ReadOnly Property SystemIdentifier() As String
            Get
                Return _systemIdentifier
            End Get
        End Property

        Private _funFileName As String = String.Empty
        Public ReadOnly Property FunFileName() As String
            Get
                Return _FunFileName
            End Get
        End Property

        Private _comCulture As String = DefaultCulture
        Public ReadOnly Property ComCulture() As String
            Get
                Return _comCulture
            End Get
        End Property

        Private _comDateFormat As String = DefaultDateFormat
        Public ReadOnly Property ComDateFormat() As String
            Get
                Return _comDateFormat
            End Get
        End Property

        Public ReadOnly Property PrgmName() As String
            Get
                If System.Web.HttpContext.Current Is Nothing Then
                    Return System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName
                Else
                    Return System.Web.HttpContext.Current.Request.Url.LocalPath
                End If
            End Get
        End Property
#End Region

#Region "Current Instance property and Load Method"
        Public Const CurrentKey As String = "Aurora.Common.UserSettings.Current"
        Public Shared ReadOnly DefaultSettings = New UserSettings()

        Private Shared _current As UserSettings = DefaultSettings
        ''' <summary>
        ''' The current user settings (if a web application, for the current HttpRequest, else from a global variable)
        ''' </summary>
        Public Shared ReadOnly Property Current() As UserSettings
            Get
                If System.Web.HttpContext.Current Is Nothing Then
                    If _current IsNot Nothing Then
                        Return _current
                    Else
                        Return DefaultSettings
                    End If
                Else
                    If System.Web.HttpContext.Current.Items.Contains(CurrentKey) Then
                        Return CType(System.Web.HttpContext.Current.Items(CurrentKey), UserSettings)
                    Else
                        Return DefaultSettings
                    End If
                End If
            End Get
        End Property

        Public Shared Function Load(ByVal userCode As String) As UserSettings
            If String.IsNullOrEmpty(userCode) Then Throw New ArgumentException()

            Dim result As UserSettings = Nothing
            Try
                Dim dataTable As DataTable = Data.GetUserSettings(userCode)
                If dataTable.Rows.Count <> 1 Then Throw New Exception()
                Dim dataRow As DataRow = dataTable.Rows(0)

                result = New UserSettings()
                result._usrId = CType(dataRow("UsrId"), String)
                result._usrCode = CType(dataRow("UsrCode"), String)
                result._usrName = CType(dataRow("UsrName"), String)
                result._comCode = CType(dataRow("ComCode"), String)
                result._comName = CType(dataRow("ComName"), String)
                result._comCulture = CType(dataRow("ComCulture"), String)
                result._comDateFormat = CType(dataRow("ComDateFormat"), String)
                result._locCode = CType(dataRow("LocCode"), String)
                result._locName = CType(dataRow("LocName"), String)
                result._locHoursBehindServerLocation = CType(dataRow("LocHoursBehindServerLocation"), Double)
                result._tctCode = CType(dataRow("TctCode"), String)
                result._tctName = CType(dataRow("TctName"), String)
                result._ctyCode = CType(dataRow("CtyCode"), String)
                result._ctyName = CType(dataRow("CtyName"), String)
                result._ctyDefCurrCode = CType(dataRow("CtyDefCurrCode"), String)
                result._systemIdentifier = CType(dataRow("SystemIdentifier"), String)
                result._funFileName = CType(dataRow("FunFileName"), String)
            Catch
                Return Nothing
            End Try

            If System.Web.HttpContext.Current Is Nothing Then
                _current = result
            ElseIf System.Web.HttpContext.Current.Items.Contains(CurrentKey) Then
                System.Web.HttpContext.Current.Items(CurrentKey) = result
            Else
                System.Web.HttpContext.Current.Items.Add(CurrentKey, result)
            End If

            Return result
        End Function
#End Region

    End Class

End Namespace