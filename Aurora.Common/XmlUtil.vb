Imports System.IO
Imports System.Xml
Imports System.Xml.Schema

Namespace Aurora.Common

    ''' <summary>
    ''' Simple Xml utility functions.  One validates an Xml document against a scheme, the reset
    ''' are simplified xpath queries and dom creation functions
    ''' </summary>
    ''' <remarks></remarks>
    Public Module XmlUtil

        ''' <summary>
        ''' Validate an xml document according to a scheme
        ''' </summary>
        Public Sub ValidateXmlDoc(ByVal xmlDoc As XmlDocument, ByVal targetNamespace As String, ByVal schemaFileName As String)
            Dim sc As XmlSchemaSet = New XmlSchemaSet()
            sc.Add(targetNamespace, schemaFileName)

            Dim settings As XmlReaderSettings = New XmlReaderSettings()
            settings.ValidationType = ValidationType.Schema
            settings.Schemas = sc

            Using nodeReader As New XmlNodeReader(xmlDoc.DocumentElement)
                Using reader As XmlReader = XmlReader.Create(nodeReader, settings)
                    Try
                        While reader.Read()
                        End While
                    Catch ex As Exception
                        Try
                            ex = New Exception("Xml Validation Error at " + reader.Name + " : " + ex.Message, ex)
                        Catch ex0 As Exception
                        End Try
                        Throw ex
                    End Try
                End Using
            End Using
        End Sub

        '''<summary>
        ''' Takes a simplified XPath expression 
        ''' e.g. "/a/b/c" or "a/@b"
        ''' and makes it so it ignore namespaces
        ''' e.g. "/*[local-name()="a"]/*[local-name()="b"]/*[local-name()="c"] or "*[local-name()="a"]/@b]"
        '''</summary>
        Public Function ConvertSimpleXPath(ByVal simpleXPath As String)
            If String.IsNullOrEmpty(simpleXPath) Then Throw New ArgumentNullException()

            Dim paths As String() = simpleXPath.Split("/")

            Dim result As String = ""
            For i As Integer = 0 To paths.Length - 1
                Dim path As String = paths(i).Trim()
                If (i > 0) Then result += "/"
                If String.IsNullOrEmpty(path) Then
                ElseIf path.StartsWith("@") Then
                    result += path
                Else
                    result += "*[local-name()=""" + path + """]"
                End If
            Next
            Return result
        End Function

        ''' <summary>
        ''' Selects a list of nodes using a simple xpath expression
        ''' </summary>
        Public Function SelectNodes(ByVal node As XmlNode, ByVal simpleXPath As String) As XmlNodeList
            If node Is Nothing OrElse String.IsNullOrEmpty(simpleXPath) Then Throw New ArgumentNullException()

            Dim xpath As String = ConvertSimpleXPath(simpleXPath)
            Return node.SelectNodes(xpath)
        End Function

        ''' <summary>
        ''' Selects a single node using a simple xpath expression
        ''' </summary>
        Public Function SelectSingleNode(ByVal node As XmlNode, ByVal simpleXPath As String) As XmlNode
            If node Is Nothing OrElse String.IsNullOrEmpty(simpleXPath) Then Throw New ArgumentNullException()

            Dim xpath As String = ConvertSimpleXPath(simpleXPath)
            Return node.SelectSingleNode(xpath)
        End Function

        ''' <summary>
        ''' This function (and its overloaded versions) returns a single ("InnerText") node value
        ''' </summary>
        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String) As String
            Dim result As XmlNode = SelectSingleNode(node, simpleXPath)

            If result IsNot Nothing Then
                Return result.InnerText
            Else
                Return Nothing
            End If
        End Function

        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String, ByVal defaultValue As String) As String
            Try
                Dim result As String = SelectSingleNodeValue(node, simpleXPath)
                If result IsNot Nothing Then
                    Return result
                Else
                    Return defaultValue
                End If
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String, ByVal defaultValue As Date) As Date
            Try
                Dim result As String = SelectSingleNodeValue(node, simpleXPath)
                If result IsNot Nothing Then
                    Return XmlConvert.ToDateTime(result, XmlDateTimeSerializationMode.Unspecified)
                Else
                    Return defaultValue
                End If
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String, ByVal defaultValue As Integer) As Integer
            Try
                Dim result As String = SelectSingleNodeValue(node, simpleXPath)
                If result IsNot Nothing Then
                    Return XmlConvert.ToInt32(result)
                Else
                    Return defaultValue
                End If
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String, ByVal defaultValue As Nullable(Of Boolean)) As Nullable(Of Boolean)
            Try
                Dim result As String = SelectSingleNodeValue(node, simpleXPath)
                If result IsNot Nothing Then
                    Return New Nullable(Of Boolean)(XmlConvert.ToBoolean(result))
                Else
                    Return defaultValue
                End If
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function SelectSingleNodeValue(ByVal node As XmlNode, ByVal simpleXPath As String, ByVal defaultValue As Double)
            Try
                Dim result As String = SelectSingleNodeValue(node, simpleXPath)
                If result IsNot Nothing Then
                    Return XmlConvert.ToDouble(result)
                Else
                    Return defaultValue
                End If
            Catch
                Return defaultValue
            End Try
        End Function

        ''' <summary>
        ''' Creates a new element with an optional innerText argument
        ''' </summary>
        Public Function CreateElement(ByVal parentElement As XmlElement, ByVal name As String, Optional ByVal innerText As String = Nothing) As XmlElement
            If parentElement Is Nothing OrElse String.IsNullOrEmpty(name) Then Throw New ArgumentNullException()

            Dim result As XmlElement = parentElement.OwnerDocument.CreateElement(name)
            parentElement.AppendChild(result)
            If Not String.IsNullOrEmpty(innerText) Then result.InnerText = innerText

            Return result
        End Function

        ''' <summary>
        ''' Creates a new element only if one with the same name does not already exist.  If innerText is nothing, it will *not* be set
        ''' </summary>
        Public Function CreateSingleElement(ByVal parentElement As XmlElement, ByVal name As String, Optional ByVal innerText As String = Nothing) As XmlElement
            If parentElement Is Nothing OrElse String.IsNullOrEmpty(name) Then Throw New ArgumentNullException()

            Dim result As XmlElement = SelectSingleNode(parentElement, name)
            If result Is Nothing Then
                result = CreateElement(parentElement, name, innerText)
            ElseIf Not String.IsNullOrEmpty(innerText) Then
                result.InnerText = innerText
            End If

            Return result
        End Function

        ''' <summary>
        ''' Copy the contents (elements and attributes) from a source element to a target element ignoring namespace defintions.  
        ''' Avoids the namespace issue when done instead using innerxml/outerxml
        ''' </summary>
        Public Sub CopyNodes(ByVal sourceNode As XmlNode, ByVal targetNode As XmlNode)
            If sourceNode Is Nothing OrElse targetNode Is Nothing Then Throw New ArgumentNullException()

            If sourceNode.HasChildNodes Then
                For Each sourceChildNode As XmlNode In sourceNode.ChildNodes
                    Dim targetChildNode As XmlNode = targetNode.OwnerDocument.CreateNode(sourceChildNode.NodeType, sourceChildNode.Name, Nothing)
                    targetNode.AppendChild(targetChildNode)

                    CopyNodes(sourceChildNode, targetChildNode)
                Next
            Else
                targetNode.InnerText = sourceNode.InnerText
            End If
        End Sub

        ''' <summary>
        ''' Make a nicely formatted string version of this Xml Node (like OuterXml but with indenting)
        ''' </summary>
        Public Function ToString(ByVal node As XmlNode) As String
            If node Is Nothing Then Throw New ArgumentNullException()

            Using stringWriter As New StringWriter()
                Using xmlWriter As New XmlTextWriter(stringWriter)
                    xmlWriter.Formatting = Formatting.Indented
                    node.WriteTo(xmlWriter)
                End Using

                Return stringWriter.ToString()
            End Using
        End Function

    End Module

End Namespace