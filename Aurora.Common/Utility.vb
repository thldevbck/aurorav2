Imports System.Text
Imports System.Globalization
Imports System.Reflection
Imports System.Web
Imports System.Web.Configuration
Imports System.Configuration

Namespace Aurora.Common

    Public Module Utility

        Public Const SystemCulture = "en-NZ"

        Public Function ObjectValueToString(ByVal value As Object) As String
            If value Is Nothing Then
                Return Nothing
            ElseIf TypeOf value Is Guid Then
                Return CType(value, Guid).ToString() ' Guid doesnt have an overriden ToString!!!! (?)
            ElseIf TypeOf value Is Date Then
                Dim d As Date = CType(value, Date)
                Return d.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) & IIf(d.TimeOfDay.Ticks > 0, d.ToString(" HH:mm"), "")
            ElseIf TypeOf value Is Decimal Then
                Return CType(value, Decimal).ToString("0.#")
            Else
                Return value.ToString()
            End If
        End Function

        Public Function ObjectValueParse(ByVal dataType As Type, ByVal value As String) As Object
            If value Is Nothing Then
                Return Nothing
            ElseIf dataType Is GetType(Guid) Then
                Return New Guid(value)
            ElseIf dataType Is GetType(Date) Then
                Try
                    Return Date.ParseExact(value, Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm", System.Globalization.CultureInfo.InvariantCulture)
                Catch
                    Return Date.ParseExact(value, Aurora.Common.UserSettings.Current.ComDateFormat, System.Globalization.CultureInfo.InvariantCulture)
                End Try
            ElseIf dataType Is GetType(String) Then
                Return value
            Else
                Return dataType.InvokeMember("Parse", BindingFlags.Public Or BindingFlags.Static Or BindingFlags.InvokeMethod, Nothing, Nothing, New Object() {value})
            End If
        End Function

        Public Function ParseString(ByVal sourceObject As Object) As String
            Try
                Return ParseString(sourceObject, String.Empty)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function ParseString(ByVal sourceObject As Object, ByVal defaultValue As String) As String
            Try
                If sourceObject Is System.DBNull.Value Then Return defaultValue
                Return Convert.ToString(sourceObject)
            Catch ex As Exception
                Return defaultValue
            End Try
        End Function

        Public Function ParseInt(ByVal sourceObject As Object) As Integer
            Try
                Return ParseInt(sourceObject, Integer.MinValue)
            Catch
                Return Integer.MinValue
            End Try
        End Function

        Public Function ParseInt(ByVal sourceObject As Object, ByVal defaultValue As Integer) As Integer
            Try
                Dim value As Integer = 0
                If Not Integer.TryParse(ParseString(sourceObject), value) Then Return defaultValue
                Return value
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function ParseDecimal(ByVal sourceObject As Object, ByVal defaultValue As Decimal) As Decimal
            Try
                Dim value As Decimal = 0
                If Not Decimal.TryParse(ParseString(sourceObject), value) Then Return defaultValue
                Return value
            Catch
                Return defaultValue
            End Try
        End Function

        Public Function ParseDateTime(ByVal value As String, ByVal fromCulture As String) As Date
            'fromCulture = "en-nz"
            Return DateTime.Parse(value, New CultureInfo(fromCulture, True), DateTimeStyles.NoCurrentDateDefault)
        End Function

        Public Function ParseDateTime(ByVal value As String) As Date
            Try
                Return Convert.ToDateTime(value)
            Catch
                Return Date.MinValue
            End Try
        End Function

        Public Function DateTimeToString(ByVal value As DateTime, ByVal toCulture As String) As String
            'toCulture = "en-nz"
            'formate = "d"
            Return Convert.ToString(value, New CultureInfo(toCulture, True))
        End Function

        ''' <summary>
        ''' Convert a string in user setting date format to date 
        ''' </summary>
        Public Function DateUIParse(ByVal value As String) As Date
            Try
                Return Date.ParseExact(value, UserSettings.Current.ComDateFormat, System.Globalization.CultureInfo.InvariantCulture)
            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function

        ''' <summary>
        ''' Convert a date to string in user setting date format 
        ''' </summary>
        Public Function DateUIToString(ByVal value As Date) As String
            'Dim s As String
            's = value.ToString(UserSettings.Current.ComDateFormat)
            'Return s.Substring(0, s.IndexOf(" "))
            Return value.ToString(UserSettings.Current.ComDateFormat)
        End Function

        ''' <summary>
        ''' Convert a string in DB date format to date 
        ''' </summary>
        Public Function DateDBParse(ByVal value As String) As Date
            Return ParseDateTime(value, Utility.SystemCulture)
        End Function

        ''' <summary>
        ''' Convert a date to string in DB date format
        ''' </summary>
        Public Function DateDBToString(ByVal value As Date) As String
            Dim s As String
            s = DateTimeToString(value, Utility.SystemCulture)
            If s.IndexOf(" ") > 0 And value.Hour = 0 And value.Minute = 0 And value.Second = 0 Then
                Return s.Substring(0, s.IndexOf(" "))
            Else
                Return s
            End If
            'Return DateTimeToString(value, Utility.SystemCulture)
        End Function

        ''' <summary>
        ''' Convert a string in user setting date format to string in DB date format
        ''' </summary>
        Public Function DateUIDBConvert(ByVal value As String) As String
            If Not String.IsNullOrEmpty(value) Then
                Dim d As Date
                d = DateUIParse(value)
                Return DateDBToString(d)
            Else
                Return value
            End If
        End Function

        ''' <summary>
        ''' Convert a string in DB date format to string in user setting date format
        ''' </summary>
        Public Function DateDBUIConvert(ByVal value As String) As String
            If Not String.IsNullOrEmpty(value) Then
                Dim d As Date = DateDBParse(value)
                Return DateUIToString(d)
            Else
                Return value
            End If
        End Function



        Public Function ParseBoolean(ByVal sourceObject As Object, ByVal defaultValue As Boolean) As Boolean
            Try
                If sourceObject Is System.DBNull.Value Then Return defaultValue
                Return Convert.ToBoolean(sourceObject)
            Catch
                Return defaultValue
            End Try
        End Function

        ''' <summary>
        ''' Does an integer value intersect with a integer range?
        ''' </summary>
        Public Function DoesIntersect(ByVal value As Long, ByVal min As Long, ByVal max As Long) As Boolean
            Return value >= min AndAlso value <= max
        End Function

        ''' <summary>
        ''' Does an integer range intersect with a integer range?
        ''' </summary>
        Public Function DoesRangeIntersect(ByVal min0 As Long, ByVal max0 As Long, ByVal min1 As Long, ByVal max1 As Long) As Boolean
            Return Not (max1 < min0 OrElse min1 > max0 OrElse max0 < min1 OrElse min0 > max1)
        End Function

        ''' <summary>
        ''' Ensures a integer value falls within the modulus range
        ''' </summary>
        Public Function ModulaWithin(ByVal value As Long, ByVal range As Long) As Long
            If range <= 0 Then Throw New ArgumentException("Invalid range value")
            While value < 0
                value += range
            End While
            While value >= range
                value -= range
            End While
            Return value
        End Function

        ''' <summary>
        ''' Does an integer modulas value intersect with a integer modulus range?
        ''' </summary>
        Public Function DoesModulaIntersect(ByVal value As Long, ByVal min As Long, ByVal max As Long, ByVal range As Long) As Boolean
            min = ModulaWithin(min, range)
            max = ModulaWithin(max, range)
            If max < min Then max += range
            If value < min Then value += range
            Return DoesIntersect(value, min, max)
        End Function

        ''' <summary>
        ''' Does an integer modulas range intersect with a integer modulus range?
        ''' </summary>
        Public Function DoesModulaRangeIntersect(ByVal min0 As Long, ByVal max0 As Long, ByVal min1 As Long, ByVal max1 As Long, ByVal range As Long) As Boolean
            min0 = ModulaWithin(min0, range)
            max0 = ModulaWithin(max0, range)
            min1 = ModulaWithin(min1, range)
            max1 = ModulaWithin(max1, range)

            If max0 < min0 Then
                Return DoesModulaRangeIntersect(0, max0, min1, max1, range) _
                 OrElse DoesModulaRangeIntersect(min0, range - 1, min1, max1, range)
            ElseIf max1 < min1 Then
                Return DoesModulaRangeIntersect(min0, max0, 0, max1, range) _
                 OrElse DoesModulaRangeIntersect(min0, max0, min1, range - 1, range)
            Else
                Return DoesRangeIntersect(min0, max0, min1, max1)
            End If
        End Function

        ''' <summary>
        ''' Do two date ranges intersect?
        ''' </summary>
        Public Function DoesDateRangeIntersect(ByVal min0 As Date, ByVal max0 As Date, ByVal min1 As Date, ByVal max1 As Date) As Boolean
            Return DoesRangeIntersect(min0.Ticks, max0.Ticks, min1.Ticks, max1.Ticks)
        End Function

        ''' <summary>
        ''' Do two time spans intersect?
        ''' </summary>
        Public Function DoesTimeSpanRangeIntersect(ByVal min0 As TimeSpan, ByVal max0 As TimeSpan, ByVal min1 As TimeSpan, ByVal max1 As TimeSpan) As Boolean
            Return DoesRangeIntersect(min0.Ticks, max0.Ticks, min1.Ticks, max1.Ticks)
        End Function


        Private Const ValidSqlCharacters As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.<>/?:;""[{]}\|`~!@#$%^&*()-_=+"


        ''' <summary>
        ''' Converts a string value to an escaped SQL expression
        ''' e.g. "abc" -> "'abc'"
        ''' Will turn any characters it does not recognise into CHAR() function calls
        ''' e.g.
        ''' "hello,\nworld!" -> "'hello,' + CHAR(13) + 'world!'"
        ''' Function is conservative and works on a whitelist of characters it trusts so it should always 
        ''' make a safe expression
        ''' </summary>
        Public Function EscapeSqlString(ByVal str As String)
            If String.IsNullOrEmpty(str) Then Return "''"

            Dim result As New StringBuilder()
            Dim inQuote As Boolean = False

            For Each c As Char In str
                If ValidSqlCharacters.Contains(c.ToString()) Then
                    If Not inQuote Then
                        If result.Length > 0 Then result.Append("+")
                        result.Append("'")
                    End If
                    inQuote = True
                    result.Append(c)
                Else
                    If inQuote Then result.Append("'")
                    inQuote = False
                    If result.Length > 0 Then result.Append("+")
                    result.Append("CHAR(" & Asc(c) & ")")
                End If
            Next
            If inQuote Then result.Append("'")

            Return result.ToString()
        End Function

        ''' <summary>
        ''' Creates a safe single line comment for use in programmatically generated SQL scripts
        ''' e.g. "abc" -> "-- abc"
        ''' Will ignore or replace any characters or sequances it doesnt trust
        ''' "hello,\nworld!" -> "-- hello, world!"
        ''' Function is conservative and works on a whitelist of characters it trusts so it should always 
        ''' make a safe comment
        ''' </summary>
        Public Function EscapeSqlSingleLineComment(ByVal str As String) As String

            If String.IsNullOrEmpty(str) Then Return "--"

            While str.IndexOf("--") > 0 : str = str.Replace("--", "-") : End While
            While str.IndexOf(vbCr) > 0 : str = str.Replace(vbCr, "") : End While
            While str.IndexOf(vbLf) > 0 : str = str.Replace(vbLf, " ") : End While

            Dim result As New StringBuilder()
            result.Append("-- ")

            For Each c As Char In str
                If ValidSqlCharacters.Contains(c.ToString()) Then
                    result.Append(c)
                End If
            Next

            Return result.ToString()
        End Function

        Public Function BuildScript(ByVal name As String, ByVal value As String)
            Dim output As StringBuilder
            output = New StringBuilder()
            output.Append("<script id='" & name & "Script' language='javascript'>" & Environment.NewLine)
            output.Append("var " & name & Environment.NewLine)
            output.Append(name & " = """ & value & """" & Environment.NewLine)
            output.Append("</script>")
            Return output
        End Function

        Public Function GetAge(ByVal dob As Date) As Integer
            Dim years As Integer = DateTime.Now.Year - dob.Year
            ' subtract another year if we're before the
            ' birth day in the current year
            If DateTime.Now.Month < dob.Month Or (DateTime.Now.Month = dob.Month And DateTime.Now.Day < dob.Day) Then
                years = years - 1
            End If
            Return years
        End Function

        Public Sub ExtractFirstLastName(ByVal name As String, ByRef firstName As String, ByRef lastName As String)
            name = ("" & name).Trim()
            firstName = ""
            lastName = ""
            Dim index As Integer = name.IndexOf(" ")
            If index >= 0 Then
                firstName = name.Substring(0, index).Trim()
                lastName = name.Substring(index).Trim()
            End If
        End Sub



#Region "REV:mia Feb. 21 2013 Part of Edit Profile Package"
        Public Function EncodeConfirmationHTML(decodedhtml As String) As String
            Return HttpContext.Current.Server.HtmlEncode(decodedhtml)
        End Function
        Public Function DecodeConfirmationHTML(encodedhtml As String) As String
            Return HttpContext.Current.Server.HtmlDecode(encodedhtml)
        End Function

        Public Function EncodeText(ByVal oldText As String) As String
            Return HttpUtility.HtmlEncode(oldText)
        End Function

        Public Function DecodeText(ByVal oldText As String) As String
            Return HttpUtility.HtmlDecode(oldText)
        End Function

        Public Function DateWith2359(ByVal input As DateTime) As DateTime
            Return New DateTime(input.Year, input.Month, input.Day, 23, 59, 0)
        End Function

        'Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
        '    Get
        '        If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
        '            Return Aurora.ProductConstants.FutureColor
        '        ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
        '            Return ProductConstants.CurrentColor
        '        ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
        '            Return ProductConstants.PastColor
        '        Else
        '            Return ProductConstants.InactiveColor
        '        End If
        '    End Get
        'End Property
#End Region


        Public Function IIFX(Of T)(ByVal condition As Boolean, ByVal resultIfTrue As T, ByVal resultIfFalse As T) As T
            If condition Then
                Return resultIfTrue
            End If

            Return resultIfFalse
        End Function

#Region "REV:mia Oct. 30 2013 O'connor and O'Brien mystery"
        Public Function EncodeTextForSqlPassing(ByVal oldText As String) As String
            oldText = HttpUtility.HtmlEncode(oldText)

            Try
                Dim hasApos As Boolean = False
                Dim aryText() As String = {}
                If (oldText.Contains("'") = True) Then
                    aryText = oldText.Split("'")
                    hasApos = True
                Else
                    hasApos = False
                End If

                If (hasApos = True) Then
                    oldText = ""
                    For Each item As String In aryText
                        If item.Length > 0 And Not item.Contains("'") Then
                            oldText = oldText & item & "''"
                        End If
                    Next
                    oldText = oldText.Substring(0, oldText.LastIndexOf("''"))
                End If

            Catch ex As Exception
            End Try

            Return oldText
        End Function
#End Region


    End Module

End Namespace