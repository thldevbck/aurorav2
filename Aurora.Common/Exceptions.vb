Namespace Aurora.Common

    Public Class AuroraException
        Inherits Exception

        Private _tag As Object
        Public ReadOnly Property Tag() As Object
            Get
                Return _tag
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal tag As Object)
            MyBase.New(message)

            Me._tag = tag
        End Sub

    End Class

    Public Class ValidationException
        Inherits AuroraException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal tag As Object)
            MyBase.New(message, tag)
        End Sub

    End Class

    Public Class SaveException
        Inherits AuroraException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal tag As Object)
            MyBase.New(message, tag)
        End Sub
    End Class

End Namespace