Namespace Aurora.Common

    Public Class Service

        Public Shared Function GetPopUpData(ByVal caseType As String, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, _
        ByVal param4 As String, ByVal param5 As String, ByVal userCode As String) As DataTable
            Dim ds As DataSet
            ds = Aurora.Common.Data.GetPopUpData(caseType, param1, param2, param3, param4, param5, userCode)
            Dim dt As DataTable = New DataTable
            If ds.Tables.Count = 1 Then
                dt = ds.Tables(0)
            End If
            Return dt
        End Function
        ' Search  a popup item by text
        Public Shared Function SearchPopupItem(ByVal caseType As String, ByVal caption As String, ByVal text As String, ByRef errorMessage As String, ByVal userCode As String) As DataRow

            Dim dr As DataRow = Nothing

            Dim agentId As String = ""

            Dim dt As DataTable
            dt = GetPopUpData(caseType, text, "", "", "", "", userCode)

            If dt.Rows.Count > 1 Then
                errorMessage = "More Than one " & caption & " found with '" & text & "'. Use Popup"
            ElseIf dt.Rows.Count = 0 Then
                errorMessage = "No " & caption & " found with '" & text & "''."
            ElseIf dt.Rows(0)(0) = "GEN022" Then
                errorMessage = dt.Rows(0)(2)
            Else
                dr = dt.Rows(0)
            End If
            Return dr

        End Function


        Public Shared Function CheckUser(ByVal UserCode As String, ByVal Password As String, ByVal RoleCode As String) As String
            'dialog_checkUser
            Dim sReturnMsg As String = "Error"
            Dim oXmlDoc As System.Xml.XmlDocument
            oXmlDoc = Aurora.Common.Data.CheckUser(UserCode, Password, RoleCode)
            If Not oXmlDoc.DocumentElement.SelectSingleNode("Error") Is Nothing Then
                sReturnMsg = oXmlDoc.DocumentElement.SelectSingleNode("Error").InnerText
            Else
                If Not oXmlDoc.DocumentElement.SelectSingleNode("isUser") Is Nothing Then
                    If CBool(oXmlDoc.DocumentElement.SelectSingleNode("isUser").InnerText) Then
                        sReturnMsg = "True"
                    End If
                End If
            End If
            Return sReturnMsg

        End Function

        Public Shared Function CheckUser(ByVal UserCode As String, ByVal Password As String, ByVal RoleCode As String, ByRef errorMessage As String) As Boolean

            errorMessage = "Error"

            Dim xmlDoc As System.Xml.XmlDocument = Aurora.Common.Data.CheckUser(UserCode, Password, RoleCode)
            If xmlDoc.DocumentElement.SelectSingleNode("Error") IsNot Nothing Then
                errorMessage = xmlDoc.DocumentElement.SelectSingleNode("Error").InnerText
                Return False
            ElseIf xmlDoc.DocumentElement.SelectSingleNode("isUser") IsNot Nothing _
             AndAlso CBool(xmlDoc.DocumentElement.SelectSingleNode("isUser").InnerText) Then
                errorMessage = ""
                Return True
            Else
                Return False
            End If

        End Function

    End Class

End Namespace