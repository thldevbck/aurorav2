Imports System.Drawing
Imports System.Configuration
Imports System

Namespace Aurora.Common
    Public Class DataConstants

#Region "Status Colors"
        Public Shared ReadOnly CurrentColor As Color = ColorTranslator.FromHtml("#DDFFDD")
        Public Shared ReadOnly FutureColor As Color = ColorTranslator.FromHtml("#FFFFDD")
        Public Shared ReadOnly PastColor As Color = ColorTranslator.FromHtml("#DDDDFF")
        Public Shared ReadOnly ActiveColor As Color = ColorTranslator.FromHtml("#DDFFDD")
        Public Shared ReadOnly InactiveColor As Color = ColorTranslator.FromHtml("#DDDDDD")
        Public Shared ReadOnly ErrorColor As Color = ColorTranslator.FromHtml("#FFDDDD")
        Public Shared ReadOnly NeutralColor As Color = ColorTranslator.FromHtml("#FFFFFF")

        ''rev:mia May 26 2011 - added required field
        Private Shared requiredfieldcolor As String = System.Configuration.ConfigurationManager.AppSettings("RequiredColor").ToString
        Public Shared ReadOnly RequiredColor As Color = ColorTranslator.FromHtml(requiredfieldcolor)

        Public Shared ReadOnly TimeLineCurrentColor As Color = ColorTranslator.FromHtml("#FFFF88")
        Public Shared ReadOnly TimeLineFutureColor As Color = ColorTranslator.FromHtml("#FFFFFF")
        Public Shared ReadOnly TimeLinePastColor As Color = ColorTranslator.FromHtml("#CCCCCC")

        Public Shared ReadOnly IncludeColor As Color = ActiveColor
        Public Shared ReadOnly ExcludeColor As Color = ErrorColor
        Public Shared ReadOnly IncludeForeColor As Color = ColorTranslator.FromHtml("#000000")
        Public Shared ReadOnly ExcludeForeColor As Color = ColorTranslator.FromHtml("#880000")
#End Region

#Region "Fleet Status"
        Public Const FleetStatus_FreeSale As String = "F"
        Public Const FleetStatus_Request As String = "R"
        Public Const FleetStatus_StopSale As String = "S"
        Public Const FleetStatus_Unavailable As String = "X"
        Public Const FleetStatus_None As String = " "

        Public Shared ReadOnly FleetStatus_All As String() = {FleetStatus_FreeSale, FleetStatus_Request, FleetStatus_StopSale, FleetStatus_Unavailable, FleetStatus_None}
        Public Shared ReadOnly FleetStatus_Colors As Color() = { _
            ColorTranslator.FromHtml("#CCFFCC"), _
            ColorTranslator.FromHtml("#FFEEEE"), _
            ColorTranslator.FromHtml("#FFCCCC"), _
            ColorTranslator.FromHtml("#DD9999"), _
            ColorTranslator.FromHtml("#DDDDDD")}

        Public Shared Function FleetStatusToColor(ByVal status As String) As Color
            Dim index As Integer = Array.IndexOf(FleetStatus_All, status)
            If index = -1 Then
                Return FleetStatus_Colors(Array.IndexOf(FleetStatus_All, FleetStatus_None))
            Else
                Return FleetStatus_Colors(index)
            End If
        End Function
#End Region

#Region "Travel Year/Weeks"
        ''' <summary>
        ''' Function to calculate the travel dates for a specific year.
        ''' </summary>
        ''' <remarks>
        ''' Evil evil evil evil.....but taken verbatim from the old VB code, but hesitant to re-invent something.
        ''' </remarks>
        Public Shared Function CalculateTravelYearDates(ByVal year As Integer) As Date()
            Dim travelYearStart As New Date(year, 4, 1)
            Dim travelYearEnd As New Date(year + 1, 3, 31)
            Dim result(51) As Date
            result(0) = travelYearStart
            Dim count As Integer = 1
            Dim last As Integer = CInt((travelYearEnd - travelYearStart).TotalDays())
            For day As Integer = 4 To last
                Dim d As Date = travelYearStart.AddDays(day)
                If d.DayOfWeek = DayOfWeek.Monday Then
                    result(count) = d
                    count += 1
                    If count > 51 Then
                        Exit For
                    End If
                End If
            Next
            Return result
        End Function

        ''' <summary>
        ''' Looks for the next travel week start date after the specified date.
        ''' </summary>
        Public Shared Function NextTravelStart(ByVal travelStart As Date) As Date
            Dim travelYearDates As New List(Of Date)
            If travelStart.Month >= 4 Then
                travelYearDates.AddRange(CalculateTravelYearDates(travelStart.Year))
                travelYearDates.Add(New Date(travelStart.Year + 1, 4, 1))
            Else
                travelYearDates.AddRange(CalculateTravelYearDates(travelStart.Year - 1))
                travelYearDates.Add(New Date(travelStart.Year, 4, 1))
            End If

            For Each d As Date In travelYearDates
                If d > travelStart Then
                    Return d
                End If
            Next

            Throw New Exception("Could not find next travel date") ' should *never* happen
        End Function

        ''' <summary>
        ''' Make sure the date falls on a valid travel start date (usually a monday)
        ''' </summary>
        Public Shared Function ValidateTravelStart(ByVal travelStart As Date) As Boolean
            Return travelStart = NextTravelStart(travelStart.AddDays(-1))
        End Function

        ''' <summary>
        ''' Make sure the date falls on a valid travel end date (usually a sunday), which is the day before the next
        ''' travel start
        ''' </summary>
        Public Shared Function ValidateTravelEnd(ByVal travelStart As Date) As Boolean
            Return travelStart.AddDays(1) = NextTravelStart(travelStart)
        End Function

#End Region

#Region "Booking Status"
        Public Const BookingStatus_IN As String = "IN" 'IN	Inquiry
        Public Const BookingStatus_QN As String = "QN" 'QN	Quotation
        Public Const BookingStatus_KB As String = "KB" 'KB	Knock Back
        Public Const BookingStatus_WL As String = "WL" 'WL	Wait Listed
        Public Const BookingStatus_NN As String = "NN" 'NN	Provisional
        Public Const BookingStatus_KK As String = "KK" 'KK	Confirmed
        Public Const BookingStatus_CO As String = "CO" 'CO	Checked Out
        Public Const BookingStatus_CI As String = "CI" 'CI	Checked In
        Public Const BookingStatus_XN As String = "XN" 'XN	Cancelled Before Confirmation
        Public Const BookingStatus_XX As String = "XX" 'XX	Cancelled After Confirmation
        Public Const BookingStatus_NC As String = "NC" 'NC	Non-Cancelled
        Public Const BookingStatus_None As String = ""
        Public Shared ReadOnly BookingStatus_All As String() = {"IN", "QN", "KB", "WL", "NN", "KK", "CO", "CI", "XN", "XX", "NC", ""}

        Public Shared ReadOnly BookingStatusColor_IN As Color = NeutralColor
        Public Shared ReadOnly BookingStatusColor_QN As Color = NeutralColor
        Public Shared ReadOnly BookingStatusColor_KB As Color = NeutralColor
        Public Shared ReadOnly BookingStatusColor_WL As Color = NeutralColor
        Public Shared ReadOnly BookingStatusColor_NN As Color = ErrorColor
        Public Shared ReadOnly BookingStatusColor_KK As Color = FutureColor
        Public Shared ReadOnly BookingStatusColor_CO As Color = ActiveColor
        Public Shared ReadOnly BookingStatusColor_CI As Color = PastColor
        Public Shared ReadOnly BookingStatusColor_XN As Color = InactiveColor
        Public Shared ReadOnly BookingStatusColor_XX As Color = InactiveColor
        Public Shared ReadOnly BookingStatusColor_NC As Color = NeutralColor
        Public Shared ReadOnly BookingStatusColor_None As Color = ErrorColor
        Public Shared ReadOnly BookingStatusColor_All As Color() = {BookingStatusColor_IN, BookingStatusColor_QN, BookingStatusColor_KB, BookingStatusColor_WL, BookingStatusColor_NN, BookingStatusColor_KK, BookingStatusColor_CO, BookingStatusColor_CI, BookingStatusColor_XN, BookingStatusColor_XX, BookingStatusColor_NC, BookingStatusColor_None}

        Public Shared Function GetBookingStatusColor(ByVal bookingStatus As String) As Color
            Dim index As Integer = Array.IndexOf(BookingStatus_All, bookingStatus)
            If index <> -1 Then
                Return BookingStatusColor_All(index)
            Else
                Return BookingStatusColor_None
            End If
        End Function
#End Region

#Region "Code Types"
        Public Const CodeType_Address_Type As Integer = 1
        Public Const CodeType_Agent_Type As Integer = 3
        Public Const CodeType_Booked_Product_Type As Integer = 4
        Public Const CodeType_Incident_Type As Integer = 8
        Public Const CodeType_Location_Type As Integer = 9
        Public Const CodeType_Marketing_Region As Integer = 10
        Public Const CodeType_Mode_Of_Communication As Integer = 11
        Public Const CodeType_Note_Type As Integer = 13
        Public Const CodeType_Package_Type As Integer = 14
        Public Const CodeType_Person_Type As Integer = 16
        Public Const CodeType_Product_Rate_Discount_type As Integer = 17
        Public Const CodeType_Unit_of_Measure As Integer = 18
        Public Const CodeType_Referral_Type As Integer = 19
        Public Const CodeType_Specification_Unit_Of_Value As Integer = 20
        Public Const CodeType_Traveller_Type As Integer = 22
        Public Const CodeType_Currency As Integer = 23
        Public Const CodeType_Phone_Number_Type As Integer = 24
        Public Const CodeType_Contact_Type As Integer = 25
        Public Const CodeType_Audience_Type As Integer = 26
        Public Const CodeType_Cancellation_Reason As Integer = 27
        Public Const CodeType_Cancellation_Type As Integer = 28
        Public Const CodeType_Knock_Back_Type As Integer = 30
        Public Const CodeType_Note_Spec_Type As Integer = 31
        Public Const CodeType_Repairs_Maintenance_Reasons As Integer = 32
        Public Const CodeType_Relocation_Rule_Type As Integer = 33
        Public Const CodeType_Agent_Category As Integer = 35
        Public Const CodeType_Vis_Level As Integer = 36
        Public Const CodeType_Activity_Type As Integer = 37
        Public Const CodeType_Administration_Fee As Integer = 38
        Public Const CodeType_Payment_Method As Integer = 39
        Public Const CodeType_Booking_Request_Unit_of_Measure As Integer = 40
        Public Const CodeType_Person_Rate As Integer = 41
        Public Const CodeType_Status_Code As Integer = 42
        Public Const CodeType_Fleet_Override_Reasons As Integer = 43
        Public Const CodeType_AGENT_DEBTOR_STATUS As Integer = 44
#End Region

#Region "UOM's"
        Public Const UOM_FixedFee As String = ""  ' Fixed Fee"
        Public Const UOM_24HR As String = "24HR"  ' 24 Hour Day"
        Public Const UOM_DAY As String = "DAY"    ' Calendar Day"
        Public Const UOM_HOUR As String = "HOUR"  ' Hour"
        Public Const UOM_KM As String = "KM"      ' Kilometre"
#End Region

#Region "Payment"
        ''REV:MIA july 19 2011
        Public Shared SurchargeDecimalIfError As String = "-1"
        Public Shared SurchargeDecimalIfErrorFirstLoad As String = "0"
#End Region


    End Class
End Namespace